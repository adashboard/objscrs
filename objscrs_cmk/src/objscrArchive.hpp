#ifndef  OBJSCR_ARCHIVING_H
#define OBJSCR_ARCHIVING_H

// objscrs
#include "singleton.h"
#include "objscrScore.hpp"
#include "objscrDialogue.hpp"
#include "objscrFinder.hpp"
#include "objscrCommands.hpp"
#include "objscrFiles.hpp"
#include "objscrLang.hpp"
#include "textscorereader.h"


// shared_ptr
#include <tr1/memory>

#include <string>
#include <list>
#include <map>
#include <exception>
using std::string;
using std::list;
using std::map;
using std::exception;


// xml
#include "tinyxml.h"



namespace Objsc
{
	////////////////////////////////////////
	// WRITER
	////////////////////////////////////////
	class Writer
	{
		private:
			void encodeScore(TiXmlElement *parentNode, Objsc::ScorePtr score);
			void encodeZoneMap(TiXmlElement *parentNode, Objsc::ZoneMapPtr zonemap);
			void encodeZone(TiXmlElement *parentNode, Objsc::ZonePtr zone);
			void encodeRect(TiXmlElement *parentNode, Objsc::Rect& rect);
			void encodeRect(TiXmlElement *parentNode, const Objsc::Rect& rect);
			void encodeGrid(TiXmlElement *parentNode, const int& cols, const int& rows);
			void encodeColour(TiXmlElement *parentNode, Objsc::Colour& colour);
			void encodeLangPatch(TiXmlElement *parentNode, Objsc::LangPatchPtr patch);
			void encodeLangObject(TiXmlElement *parentNode, Objsc::LangObjectPtr object, int encID=-1);
			void encodeLangCord(TiXmlElement *parentNode, Objsc::LangObjectPtr object, Objsc::LangObjectEncodingMap& objectIDs);
			void encodeLangAtom(TiXmlElement *parentNode, Objsc::LangAtom* atom);
			void encodeLangAtomInt(TiXmlElement *parentNode, Objsc::LangAtom* atom);
			void encodeLangAtomFloat(TiXmlElement *parentNode, Objsc::LangAtom* atom);
			void encodeLangAtomString(TiXmlElement *parentNode, Objsc::LangAtom* atom);
			void encodeLangAtomVar(TiXmlElement *parentNode, Objsc::LangAtom* atom);
			void encodeLangAtomList(TiXmlElement *parentNode, Objsc::LangAtom* atom);
		
		public:
			Writer();
			~Writer() {}
				
			void write(Objsc::ScorePtr score);
	};
	
	
	////////////////////////////////////////
	// READER
	////////////////////////////////////////
	class Reader
	{
		private:
			Objsc::ScorePtr score;
		
			void decodeScore(TiXmlElement *parentNode, Objsc::ScorePtr score);
			void decodeZoneMap(TiXmlElement *parentNode, Objsc::ZoneMapPtr zonemap);
			void decodeZone(TiXmlElement *parentNode, Objsc::ZonePtr zone);
			void decodeRect(TiXmlElement *parentNode, Objsc::Rect& rect);
			void decodeGrid(TiXmlElement *parentNode, Objsc::ZonePtr zone);
			void decodeColour(TiXmlElement *parentNode, Objsc::Colour& colour);
			void decodeLangPatch(TiXmlElement *parentNode, Objsc::LangPatchPtr patch);
			void decodeLangObject(TiXmlElement *parentNode, Objsc::LangObjectPtr object);
			void decodeLangCord(TiXmlElement *parentNode, Objsc::LangObjectPtr object, Objsc::LangObjectDecodingMap& objectIDs);
			void decodeLangReceiver(TiXmlElement *parentNode, Objsc::LangPatchPtr patch, Objsc::LangObjectDecodingMap& objectIDs);
			void decodeLangAtom(TiXmlElement *parentNode, Objsc::LangAtom* atom);
			void decodeLangAtomInt(TiXmlElement *parentNode, Objsc::LangAtom* atom);
			void decodeLangAtomFloat(TiXmlElement *parentNode, Objsc::LangAtom* atom);
			void decodeLangAtomString(TiXmlElement *parentNode, Objsc::LangAtom* atom);
			void decodeLangAtomVar(TiXmlElement *parentNode, Objsc::LangAtom* atom);
			void decodeLangAtomList(TiXmlElement *parentNode, Objsc::LangAtom* atom);
		
		public:
			Reader() : score() {}
			~Reader() {}
				
			void read(const string& filename);
	};
	
	
	////////////////////////////////////////
	// ARCHIVIST
	////////////////////////////////////////
	class Archivist : public Singleton<Archivist>, public Objsc::Finder, public Objsc::CommandReceiver
	{
		private:
			enum arch_mode {
				SAVE_MODE,
				OPEN_MODE
			};
			
		private:
			Objsc::FileResource		scorefiles;
			Objsc::Writer 			writer;
			Objsc::Reader			reader;
			Objsc::TextScoreReader		textReader;
			Objsc::DialoguePtr		dialogue;
			Objsc::ItemNames 		items;
			arch_mode			mode;
		
			void initDialogue(); 
		
		public:
			Archivist();
			~Archivist() {}
				
			void save();
			void saveAs(const string& filename);
			void open(const string& filename);
				
			void openFileDialogue();
			void saveFileDialogue(const string& filename="");
				
			// commands
			void doCommand(Objsc::CommandPtr& cmd);
			void undoCommand(Objsc::CommandPtr& cmd);
				
			// finder
			void handleInput(const string& input);
			void selectItem(const string& itemname);
			void selectItem(int index){}
			const Objsc::ItemNames& getItemNames();
	};
	
}

#endif // OBJSCR_ARCHIVING_H
