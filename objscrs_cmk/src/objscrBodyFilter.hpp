#ifndef  OBJSCR_BODYFILTER_H
#define OBJSCR_BODYFILTER_H


// objscrs
#include "objscrShapeFilter.hpp"
#include "objscrBody.hpp"

// STL
#include <vector>
using std::vector;

// shared_ptr
#include <tr1/memory>

/*
- reads shapes
- compares against bodies
- creates body where necessary
- updates existign bodies
*/

namespace Objsc
{
	
	typedef vector < int > ShapeNumVec;
	
	////////////////////////////////////////
	// BODY FILTER
	////////////////////////////////////////
	class BodyFilter
	{
		protected:
			Objsc::ShapeFilterPtr 	shapes;
			Objsc::BodyPtrList		bodies;
			Objsc::ShapeNumVec	matchedShapes;
			bool				debug;
		
			void process();
			void processOLD();
			void createBody(const ofxCvBlob& shape);
			void cull();
			void prune();
		
		public:
			BodyFilter() : shapes(), 
					bodies(), 
					matchedShapes(),
#ifdef OBJSCRS_DEBUG
					debug(true){}
#else
					debug(false){}
#endif
			virtual ~BodyFilter() {}
				
			void setShapeFilter(Objsc::ShapeFilterPtr shapeFilterPtr) {shapes=shapeFilterPtr;}
			void update();
			
			bool hasBodies() const { return !bodies.empty();}
			// I unconst everything to quiet gcc and considering these methods are not used at all
			Objsc::BodyPtrIter beginBodies() { return bodies.begin();}
			Objsc::BodyPtrIter endBodies() { return bodies.end(); }
			const Objsc::BodyPtrList& getBodies() const { return bodies; }
			Objsc::BodyPtr getBody(unsigned int uid);
			
			void clear();
			
			void velocitySmoothingReset();
			
			static std::tr1::shared_ptr<Objsc::BodyFilter> create()
			{
				return std::tr1::shared_ptr<Objsc::BodyFilter>(new BodyFilter());
			}
			
			
	};
	
	typedef std::tr1::shared_ptr<Objsc::BodyFilter> BodyFilterPtr;
}

#endif // OBJSCR_BODYFILTER_H
