#include "objscrZoneMapFinder.hpp"

// objscrs
#include "objscrApp.hpp"
#include "objscrZoneMap.hpp"
#include "objscrCommands.hpp"


// STL
#include <algorithm>

namespace Objsc
{
	
	
	void ZoneMapFinder::addItem(const string& itemname)
	{
		Objsc::CommandPtr cmd = Objsc::CommandDispatcher::createCommand("_zonemapeditor_", "new_map");
		Objsc::PacketPtr data = cmd->getData();
		data->addArg(itemname);
		Objsc::CommandDispatcher::Current()->doCommand(cmd);
	}
	
	void ZoneMapFinder::selectItem(const string& itemname)
	{
		Objsc::CommandPtr cmd = Objsc::CommandDispatcher::createCommand("_zonemapeditor_", "set_map");
		Objsc::PacketPtr data = cmd->getData();
		data->addArg(itemname);
		Objsc::CommandDispatcher::Current()->doCommand(cmd);
	}
	
	void ZoneMapFinder::removeItem(const string& itemname)
	{
		Objsc::CommandPtr cmd = Objsc::CommandDispatcher::createCommand("_zonemapeditor_", "remove_map");
		Objsc::PacketPtr data = cmd->getData();
		data->addArg(itemname);
		Objsc::CommandDispatcher::Current()->doCommand(cmd);
	}
	
	
	const Objsc::ItemNames& ZoneMapFinder::getItemNames()
	{
		items.clear();
		const Objsc::ZoneMapMap zonemaps = Objsc::App::Current()->getScore()->getZoneMaps();
		for(Objsc::ZoneMapMap::const_iterator zmap = zonemaps.begin(); zmap != zonemaps.end();  ++zmap)	{
			items.push_back(zmap->second->getName());
		}
		sort(items.begin(),items.end());
		return items;
	}

	
}


