/***************************************************************************
 *   Copyright (C) 2012 by Pierre Marchand   *
 *   pierre@oep-h.com   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#ifndef OBJSC_LANGPATCHCANVAS_H
#define OBJSC_LANGPATCHCANVAS_H

#include "../objscrLangViews.hpp"

#include "LangCordView.h"

namespace Objsc {
	
	class LangPatchCanvas : public Objsc::LangViewBase
	{
		protected:
			Objsc::LangPatchPtr 		patch;
			Objsc::LangViewVector 		objviews;
			Objsc::LangCordViewPtr		cord;
			Objsc::LangCordViewVector 	cordviews;
			//Objsc::TextField			namefield;
			int						bgAlpha;

		public:
			LangPatchCanvas();
			~LangPatchCanvas() {}

			void update();
			void draw();

			// geom
			virtual void setLocation(float x, float y);
			virtual void setDimensions(float width, float height);
			virtual void setBounds(float x, float y, float width, float height);

			// events
			void keyPressed(int key);
			void keyReleased(int key);
			void mouseMoved(int x, int y );
			void mouseDragged(int x, int y, int button);
			void mousePressed(int x, int y, int button);
			void mouseReleased(int x, int y, int button);
			void windowResized(int w, int h);

			void setPatch(Objsc::LangPatchPtr patchPtr);

			static std::tr1::shared_ptr<Objsc::LangPatchCanvas> create()
			{
				return std::tr1::shared_ptr<Objsc::LangPatchCanvas>(new LangPatchCanvas());
			}

			static const string stylename;

			// lang
			void addObjectView(Objsc::LangViewPtr view);
			void linkAllViews();
			void linkFromView(Objsc::LangViewPtr viewFrom, bool linkobject);
			void linkViews(Objsc::LangViewPtr viewFrom, const std::string& outlet, Objsc::LangViewPtr viewTo, const std::string& inlet, bool linkobject);
			Objsc::LangViewPtr& viewForObject(Objsc::LangObjectPtr objPtr);
			void createObject(float x, float y);
			void createMessage(float x, float y);
			void createNumber(float x, float y);
			void createBang(float x, float y);
			void createToggle(float x, float y);
			void startCord(Objsc::LangViewPtr view, const std::string& socket);
			void endCord(Objsc::LangViewPtr view, const std::string& socket);
			void dropCord();

			void clear();
			void removeObject(Objsc::LangViewPtr objview);
			void removeCord(Objsc::LangCordViewPtr cordview);

			// parsing
			void parseStringToObject(Objsc::LangViewPtr objview, const string& str);

			// display
			void toggleBG();
			void solidBG() { bgAlpha=255; }
			void transBG() { bgAlpha=120; }
	};

	typedef std::tr1::shared_ptr<Objsc::LangPatchCanvas> LangPatchCanvasPtr;
	typedef vector < Objsc::LangPatchCanvasPtr > LangPatchCanvasVector;

	
} // namespace Objsc

#endif // OBJSC_LANGPATCHCANVAS_H
