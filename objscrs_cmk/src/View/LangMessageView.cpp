/***************************************************************************
 *   Copyright (C) 2012 by Pierre Marchand   *
 *   pierre@oep-h.com   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#include "LangMessageView.h"

#include "../objscrGUIStyles.hpp"
#include "../objscrLangEditor.hpp"

namespace Objsc {
	
	LangMessageView::LangMessageView() : inlet(Objsc::LANG_INLET), outlet(Objsc::LANG_OUTLET), dodrag(false), scrX(0), scrY(0), actionParse()
	{
		Objsc::StylePalette::Current()->applyStyle(Objsc::LangViewBase::stylename, this);
		Objsc::StylePalette::Current()->applyStyle(Objsc::LangViewBase::stylename, &text);
		setBounds(0,0, Objsc::LANGVIEW_DFTWIDTH, Objsc::LANGVIEW_DFTHEIGHT);
		text.sizeWithText(true);
		startEdit();
		actionParse = Objsc::CommandDispatcher::createCommand(Objsc::LangPatchEditor::cmdlabel  , "lobj_parsemsg");
	}

	void LangMessageView::setObject(Objsc::LangObjectPtr objectPtr)
	{
		object=objectPtr;
		updateText();
	}

	void LangMessageView::initSockets(std::tr1::weak_ptr<Objsc::LangView> weakPtr)
	{
		inlet.setAction(weakPtr, "0");
		outlet.setAction(weakPtr, "0");
	}

	void LangMessageView::initActions(std::tr1::weak_ptr<Objsc::LangView> weakPtr)
	{
		actionParse->getData()->addArg(weakPtr);
	}

	void LangMessageView::update()
	{

	}

	void LangMessageView::draw()
	{
		if (visible)	{
			ofSetColor(bgColour.r, bgColour.g, bgColour.b, bgColour.a);
			ofFill();
			ofRect(bounds.getX(), bounds.getY(), bounds.getWidth(), bounds.getHeight());
			ofSetColor(fgColour.r, fgColour.g, fgColour.b);
			text.draw();
			ofSetColor(fgColour.r, fgColour.g, fgColour.b, fgColour.a);
			ofLine(bounds.getX(), bounds.getY(), bounds.getRight(), bounds.getY());
			ofLine(bounds.getX(), bounds.getBottom(), bounds.getRight(), bounds.getBottom());
			ofLine(bounds.getX(), bounds.getY(), bounds.getX(), bounds.getBottom());
			ofLine(bounds.getRight(), bounds.getY(), bounds.getRight()-Objsc::LANGVIEW_BEVEL, bounds.getY()+Objsc::LANGVIEW_BEVEL);
			ofLine(bounds.getRight()-Objsc::LANGVIEW_BEVEL, bounds.getY()+Objsc::LANGVIEW_BEVEL, bounds.getRight()-Objsc::LANGVIEW_BEVEL, bounds.getBottom()-Objsc::LANGVIEW_BEVEL);
			ofLine(bounds.getRight()-Objsc::LANGVIEW_BEVEL, bounds.getBottom()-Objsc::LANGVIEW_BEVEL, bounds.getRight(), bounds.getBottom());
			inlet.draw();
			outlet.draw();
		}
	}

	void LangMessageView::_setLocation(float x, float y)
	{
		bounds.setLocation(x,y);
		inrect.setLocation(bounds.getX()+Objsc::LANGVIEW_MARGIN, bounds.getY()+Objsc::LANGVIEW_MARGIN);
		text.setLocation(x,y);
		inlet.setLocation(x,y);
		outlet.setLocation(x,bounds.getBottom()-Objsc::LANGVIEW_SOCKETHEIGHT);
	}

	void LangMessageView::_setBounds(float x, float y, float width, float height)
	{
		bounds.set(x,y,width,height);
		inrect.set(bounds.getX()+Objsc::LANGVIEW_MARGIN, bounds.getY()+Objsc::LANGVIEW_MARGIN, bounds.getWidth()-Objsc::LANGVIEW_MARGIN*2, bounds.getHeight()-Objsc::LANGVIEW_MARGIN*2);
		text.setBounds(x,y,width-Objsc::LANGVIEW_BEVEL,height);
		inlet.setLocation(x,y);
		outlet.setLocation(x,bounds.getBottom()-Objsc::LANGVIEW_SOCKETHEIGHT);
	}

	void LangMessageView::fitToText()
	{
		if(text.getWidth()+Objsc::LANGVIEW_RIGHTMARGIN > bounds.getWidth())	{
			bounds.setWidth(text.getWidth()+Objsc::LANGVIEW_RIGHTMARGIN+Objsc::LANGVIEW_MARGIN);
		}
	}

	void LangMessageView::updateText()
	{
		text.setText(object->getDisplayString());
		fitToText();
		if(text.getText().empty())	{
			startEdit();
		} else {
			endEdit();
		}
	}

	void LangMessageView::startEdit()
	{
		text.editOn();
		text.setTextColour(255,0,0);
	}

	void LangMessageView::endEdit()
	{
		text.editOff();
		text.setTextColour(0,0,0);
	}

	void LangMessageView::keyPressed(int key)
	{
		consumed = false;
		if(hasFocus) 	{
			if(key == OF_KEY_RETURN)	{
				object->setDisplayString(text.getText());
				updateText();
				if(object) {
					object->activate();
				}
			} else {
				text.keyPressed(key);
				fitToText();
			}
			consumed = true;
		}
	}

	void LangMessageView::keyReleased(int key)
	{
		if(hasFocus) 	{
			text.keyReleased(key);
		}
	}

	void LangMessageView::mouseMoved(int x, int y )
	{
		if(withinBounds(x,y) && visible)	{
			if(!hasFocus && displayState!=Objsc::DISABLED) {
				gainFocus();
			}
		} else {
			if(hasFocus && displayState!=Objsc::DISABLED) {
				looseFocus();
				if(dodrag)
					dodrag=false;
			}
		}
		text.mouseMoved(x,y);
		inlet.mouseMoved(x,y);
		outlet.mouseMoved(x,y);
	}

	void LangMessageView::mouseDragged(int x, int y, int button)
	{
		consumed = false;
		float dX = x-scrX;
		float dY = y-scrY;
		if(dodrag)	{
			bounds.moveCentre(dX,dY);
			inrect.moveCentre(dX,dY);
			text.setLocation(bounds.getX(), bounds.getY());
			inlet.setLocation(bounds.getX(), bounds.getY());
			outlet.setLocation(bounds.getX(), bounds.getBottom()-Objsc::LANGVIEW_SOCKETHEIGHT);
			mapToStage();
			consumed = true;
		}
		scrX = x;
		scrY = y;
	}

	void LangMessageView::mousePressed(int x, int y, int button)
	{
		consumed = false;
		if(withinBounds(x,y))	{
			consumed = true;
			scrX = x;
			scrY = y;
			switch(button)	{
				case 0:
					inlet.mousePressed(x,y,button);
					if(inlet.eventConsumed())	{
						return;
					}
					outlet.mousePressed(x,y,button);
					if(outlet.eventConsumed())	{
						return;
					}
					if(onInside(x,y))	{
						if(text.isEditable())	{
							text.mousePressed(x,y,button);
						} else if(object) {
							object->activate();
						}
					} else if(onMargin(x,y))	{
						dodrag = true;
					}
					break;
				case 1:
					break;
				case 2:
					openMenu(x,y);
					break;
			}
		}
	}

	void LangMessageView::mouseReleased(int x, int y, int button)
	{
		if(dodrag) {
			dodrag=false;
		}
	}

	
} // namespace Objsc
