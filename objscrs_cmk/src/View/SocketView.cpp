/***************************************************************************
 *   Copyright (C) 2012 by Pierre Marchand   *
 *   pierre@oep-h.com   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#include "SocketView.h"

#include "../objscrGUIStyles.hpp"

namespace Objsc {

	Objsc::WidgetStyle* SocketView::style = 0;

	SocketView::SocketView(socket_type ntype) :
		socketName(),
		type(ntype)
	{
		Objsc::StylePalette::Current()->applyStyle(Objsc::LangViewBase::stylename, this);
		setBounds(0,0, Objsc::LANGVIEW_SOCKETWIDTH, Objsc::LANGVIEW_SOCKETHEIGHT);

		if(style == 0)
		{
			style = Objsc::StylePalette::Current()->createStyle("_SocketView_");
			style->setTextColour(0,0,255,132);
			style->setBackColour(0,0,0,0);
			style->setFont(FontConfig::Current()->GetFont(FONT_BOLD), 20);
		}
		style->apply(&socketName);
		socketName.clearText();
		socketName.setLocation(Objsc::WINDOWMARGIN_WIDTH, float(ofGetHeight()) - (Objsc::WINDOWMARGIN_HEIGHT + 32));
//		socketName.drawBackground(false);
		socketName.hide();
	}

	void SocketView::setAction(Objsc::LangViewWeakPtr view, const std::string& socketnum)
	{
		std::string socketPrefix;
		switch(type)
		{
			case Objsc::LANG_INLET:
				action = Objsc::CommandDispatcher::createCommand("_langeditor_", "end_cord");
				socketPrefix = "Inlet: ";
				break;
			case Objsc::LANG_OUTLET:
				action = Objsc::CommandDispatcher::createCommand("_langeditor_", "start_cord");
				socketPrefix = "Outlet: ";
				break;
		}
		if(action)
		{
			Objsc::PacketPtr data = action->getData();
			data->addArg(socketnum);
			data->addArg(view);
			socketPrefix.append(socketnum);
			socketName.setText(socketPrefix);
		}
	}

	void SocketView::draw()
	{
		if (visible)
		{
			ofSetColor(fgColour.r, fgColour.g, fgColour.b);
			if(hasFocus)	{
				ofFill();
				ofRect(bounds.getX(), bounds.getY(), bounds.getWidth(), bounds.getHeight());
				ofNoFill();
			} else {
				outlineBox(bounds);
			}

			socketName.draw();
		}

	}

	void SocketView::mousePressed(int x, int y, int button)
	{
		consumed = false;
		if(withinBounds(x,y))	{
			consumed = true;
			Objsc::CommandDispatcher::Current()->doCommand(action);
		}
	}

	void SocketView::gainFocus()
	{
		LangViewBase::gainFocus();
		socketName.show();
	}

	void SocketView::looseFocus()
	{
		LangViewBase::looseFocus();
		socketName.hide();
	}

	
} // namespace Objsc
