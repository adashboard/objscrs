/***************************************************************************
 *   Copyright (C) 2012 by Pierre Marchand   *
 *   pierre@oep-h.com   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#ifndef OBJSC_LANGTOGGLEVIEW_H
#define OBJSC_LANGTOGGLEVIEW_H

#include "../objscrLangViews.hpp"

#include "SocketView.h"

namespace Objsc {

	class LangToggleView : public Objsc::LangView, public Objsc::LangObserver
	{
		protected:
			Objsc::SocketView		inlet;
			Objsc::SocketView		outlet;
			bool				dodrag;
			int					scrX;
			int					scrY;
			bool				drawon;

			void _setLocation(float x, float y);
			void _setBounds(float x, float y, float width, float height);

		public:
			LangToggleView();
			~LangToggleView();

			void update();
			void draw();

			void initSockets(std::tr1::weak_ptr<Objsc::LangView> weakPtr);

			void setObject(Objsc::LangObjectPtr objectPtr);

			const float getOutletX(const std::string& index) const { return outlet.getOutletX(); }
			const float getOutletY(const std::string& index) const { return outlet.getOutletY(); }
			const float getInletX(const std::string& index) const { return inlet.getInletX(); }
			const float getInletY(const std::string& index) const { return inlet.getInletY(); }

			void objActivated();

			void keyPressed(int key);
			void keyReleased(int key);
			void mouseMoved(int x, int y );
			void mouseDragged(int x, int y, int button);
			void mousePressed(int x, int y, int button);
			void mouseReleased(int x, int y, int button);
			void windowResized(int w, int h);

			static std::tr1::shared_ptr<Objsc::LangToggleView> create()
			{
				std::tr1::shared_ptr<Objsc::LangToggleView> ptr(new LangToggleView());
				ptr->initSockets(std::tr1::weak_ptr<Objsc::LangView>(ptr));
				return ptr;
			}
	};

	typedef std::tr1::shared_ptr<Objsc::LangToggleView> LangToggleViewPtr;
	typedef vector < Objsc::LangToggleViewPtr > LangToggleViewVector;

	
} // namespace Objsc

#endif // OBJSC_LANGTOGGLEVIEW_H
