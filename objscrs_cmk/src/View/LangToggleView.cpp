/***************************************************************************
 *   Copyright (C) 2012 by Pierre Marchand   *
 *   pierre@oep-h.com   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#include "LangToggleView.h"
#include "../objscrGUIStyles.hpp"
#include "../Lang/ToggleObject.hpp"

namespace Objsc {
	

	LangToggleView::LangToggleView() : inlet(Objsc::LANG_INLET), outlet(Objsc::LANG_OUTLET), dodrag(false), scrX(0), scrY(0), drawon(false)
	{
		Objsc::StylePalette::Current()->applyStyle(Objsc::LangViewBase::stylename, this);
		setBounds(0,0, Objsc::LANGVIEW_DFTHEIGHT, Objsc::LANGVIEW_DFTHEIGHT);
	}

	LangToggleView::~LangToggleView()
	{
		//if(object)
			//object->removeObserver();
	}

	void LangToggleView::setObject(Objsc::LangObjectPtr objectPtr)
	{
		object=objectPtr;
		if(object)
			drawon = std::tr1::static_pointer_cast<Objsc::ToggleObject>(object)->isOn();
	}


	void LangToggleView::initSockets(std::tr1::weak_ptr<Objsc::LangView> weakPtr)
	{
		inlet.setAction(weakPtr, "0");
		outlet.setAction(weakPtr, "0");
	}

	void LangToggleView::update()
	{

	}

	void LangToggleView::objActivated()
	{
		//printf("LangToggleView::objActivated\n");
		if(object)
			drawon = std::tr1::static_pointer_cast<Objsc::ToggleObject>(object)->isOn();
	}

	void LangToggleView::draw()
	{
		if (visible)	{
			ofSetColor(bgColour.r, bgColour.g, bgColour.b, bgColour.a);
			ofFill();
			ofRect(bounds.getX(), bounds.getY(), bounds.getWidth(), bounds.getHeight());
			ofSetColor(fgColour.r, fgColour.g, fgColour.b);
			if(drawon)	{
				crossXLines(bounds);
			}
			outlineBox(bounds);
			inlet.draw();
			outlet.draw();
		}
	}

	void LangToggleView::_setLocation(float x, float y)
	{
		bounds.setLocation(x,y);
		inrect.setLocation(bounds.getX()+Objsc::LANGVIEW_MARGIN, bounds.getY()+Objsc::LANGVIEW_MARGIN);
		inlet.setLocation(x,y);
		outlet.setLocation(x,bounds.getBottom()-Objsc::LANGVIEW_SOCKETHEIGHT);
	}

	void LangToggleView::_setBounds(float x, float y, float width, float height)
	{
		bounds.set(x,y,width,height);
		inrect.set(bounds.getX()+Objsc::LANGVIEW_MARGIN, bounds.getY()+Objsc::LANGVIEW_MARGIN, bounds.getWidth()-Objsc::LANGVIEW_MARGIN*2, bounds.getHeight()-Objsc::LANGVIEW_MARGIN*2);
		inlet.setLocation(x,y);
		outlet.setLocation(x,bounds.getBottom()-Objsc::LANGVIEW_SOCKETHEIGHT);
	}

	void LangToggleView::keyPressed(int key)
	{
		consumed = false;
	}

	void LangToggleView::keyReleased(int key)
	{
		consumed = false;
	}

	void LangToggleView::mouseMoved(int x, int y )
	{
		if(withinBounds(x,y) && visible)	{
			if(!hasFocus && displayState!=Objsc::DISABLED) {
				if(onInside(x,y))	{
					gainFocus();
				}
			}
		} else {
			if(hasFocus && displayState!=Objsc::DISABLED) {
				looseFocus();
				if(dodrag)
					dodrag=false;
			}
		}
		inlet.mouseMoved(x,y);
		outlet.mouseMoved(x,y);
	}

	void LangToggleView::mouseDragged(int x, int y, int button)
	{
		consumed = false;
		float dX = x-scrX;
		float dY = y-scrY;
		if(dodrag)	{
			bounds.moveCentre(dX,dY);
			inrect.moveCentre(dX,dY);
			inlet.setLocation(bounds.getX(), bounds.getY());
			outlet.setLocation(bounds.getX(), bounds.getBottom()-Objsc::LANGVIEW_SOCKETHEIGHT);
			mapToStage();
			consumed = true;
		}
		scrX = x;
		scrY = y;
	}

	void LangToggleView::mousePressed(int x, int y, int button)
	{
		consumed = false;
		if(withinBounds(x,y))	{
			scrX = x;
			scrY = y;
			consumed = true;
			switch(button)	{
				case 0:
					inlet.mousePressed(x,y,button);
					if(inlet.eventConsumed())	{
						return;
					}
					outlet.mousePressed(x,y,button);
					if(outlet.eventConsumed())	{
						return;
					}
					if(onInside(x,y))	{
						if(object)
							object->activate();
					} else if(onMargin(x,y))	{
						dodrag = true;
					}
					break;
				case 1:
					break;
				case 2:
					openMenu(x,y);
					break;
			}
		}
	}

	void LangToggleView::mouseReleased(int x, int y, int button)
	{
		if(dodrag) {
			dodrag=false;
		}
	}

	void LangToggleView::windowResized(int w, int h)
	{

	}


	
} // namespace Objsc
