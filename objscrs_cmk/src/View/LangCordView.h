/***************************************************************************
 *   Copyright (C) 2012 by Pierre Marchand   *
 *   pierre@oep-h.com   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#ifndef OBJSC_LANGCORDVIEW_H
#define OBJSC_LANGCORDVIEW_H

#include "../objscrLangViews.hpp"

namespace Objsc {
	
	class LangCordView : public Objsc::LangViewBase
	{
		protected:
			Objsc::LangViewPtr		outletView;
			Objsc::LangViewPtr		inletView;
			std::string					outlet;
			std::string					inlet;
			float				endX;
			float				endY;
			bool				editing;
			Objsc::CommandPtr 	actionDelete;


		public:
			LangCordView();
			~LangCordView() {}

			void update();
			void draw();

			void start(Objsc::LangViewPtr viewPtr, std::string socket);
			void connect(Objsc::LangViewPtr viewPtr, std::string socket, bool linkobject=true);
			bool connectsWith(Objsc::LangViewPtr viewPtr) const { return viewPtr == outletView || viewPtr == inletView; }
			void print() { printf("LangCordView\n");}

			void unlink();

			void keyPressed(int key);
			void keyReleased(int key);
			void mouseMoved(int x, int y );
			void mouseDragged(int x, int y, int button);
			void mousePressed(int x, int y, int button);
			void mouseReleased(int x, int y, int button);
			void windowResized(int w, int h);

			bool withinBounds(float x, float y);

			void initActions(std::tr1::weak_ptr<Objsc::LangCordView> weakPtr);

			static std::tr1::shared_ptr<Objsc::LangCordView> create()
			{
				std::tr1::shared_ptr<Objsc::LangCordView> ptr(new LangCordView());
				ptr->initActions(std::tr1::weak_ptr<Objsc::LangCordView>(ptr));
				return ptr;
			}

			static const string stylename;
	};

	typedef std::tr1::shared_ptr<Objsc::LangCordView> LangCordViewPtr;
	typedef vector < Objsc::LangCordViewPtr > LangCordViewVector;
	typedef std::tr1::weak_ptr<Objsc::LangCordView> LangCordViewWeakPtr;


	
} // namespace Objsc

#endif // OBJSC_LANGCORDVIEW_H
