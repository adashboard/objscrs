/***************************************************************************
 *   Copyright (C) 2012 by Pierre Marchand   *
 *   pierre@oep-h.com   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#ifndef OBJSC_SOCKETVIEW_H
#define OBJSC_SOCKETVIEW_H

#include "../objscrLangViews.hpp"

namespace Objsc {
	
	class SocketView : public Objsc::LangViewBase
	{
		private:
			SocketView(){}
			static Objsc::WidgetStyle* style;
			Objsc::Label	socketName;
			socket_type 			type;
			Objsc::CommandPtr 	action;
			void setType(socket_type ntype) { type=ntype;}

		public:
			SocketView(socket_type ntype);
			~SocketView() {}

			void setAction(Objsc::LangViewWeakPtr view, const std::string& socketnum);
			void draw();
			void mousePressed(int x, int y, int button);

			void gainFocus();
			void looseFocus();

			static std::tr1::shared_ptr<Objsc::SocketView> createInlet()
			{
				return std::tr1::shared_ptr<Objsc::SocketView>(new SocketView(Objsc::LANG_INLET));
			}

			static std::tr1::shared_ptr<Objsc::SocketView> createOutlet()
			{
				return std::tr1::shared_ptr<Objsc::SocketView>(new SocketView(Objsc::LANG_OUTLET));
			}

			void moveCentre(float dX, float dY) { bounds.moveCentre(dX,dY);}

			const float getOutletX() const { return bounds.getCentreX(); }
			const float getOutletY() const { return bounds.getBottom(); }
			const float getInletX() const { return bounds.getCentreX(); }
			const float getInletY() const { return bounds.getY(); }
	};

	typedef std::tr1::shared_ptr<Objsc::SocketView> SocketViewPtr;
	typedef vector < Objsc::SocketViewPtr > SocketViewVector;
	typedef std::map < std::string, Objsc::SocketViewPtr > SocketViewMap;

	
} // namespace Objsc

#endif // OBJSC_SOCKETVIEW_H
