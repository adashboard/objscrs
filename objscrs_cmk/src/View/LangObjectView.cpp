/***************************************************************************
 *   Copyright (C) 2012 by Pierre Marchand   *
 *   pierre@oep-h.com   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#include "LangObjectView.h"

#include "../objscrGUIStyles.hpp"
#include "../objscrLangEditor.hpp"

namespace Objsc {

	const string Objsc::LangViewBase::stylename = "langobject";
	
	LangObjectView::LangObjectView() : dodrag(false), scrX(0), scrY(0), actionParse(), inlets(), outlets()
	{
		Objsc::StylePalette::Current()->applyStyle(Objsc::LangViewBase::stylename, this);
		Objsc::StylePalette::Current()->applyStyle(Objsc::LangViewBase::stylename, &text);
		setBounds(0,0, Objsc::LANGVIEW_DFTWIDTH, Objsc::LANGVIEW_DFTHEIGHT);
		text.sizeWithText(true);
		text.setTextColour(255,0,0);
		actionParse = Objsc::CommandDispatcher::createCommand(Objsc::LangPatchEditor::cmdlabel  , "lobj_parse");
	}

	LangObjectView::~LangObjectView()
	{
		inlets.clear();
		outlets.clear();
	}


	void LangObjectView::setObject(Objsc::LangObjectPtr objectPtr)
	{
		object=objectPtr;
		updateText();
	}

	void LangObjectView::reset()
	{
		bounds.setWidth(Objsc::LANGVIEW_DFTWIDTH);
		text.clearText();
		text.setTextColour(255,0,0);
		text.editOn();
	}

	void LangObjectView::setText(const string& str)
	{
		text.setText(str);
		fitToText();
	}

	void LangObjectView::updateText()
	{
		reset();
		if(object)	{
			text.setText(object->getDisplayString());
			fitToText();
			text.setTextColour(0,0,0);
			text.editOff();
		}
	}

	void LangObjectView::fitToText()
	{
		if(text.getWidth()+Objsc::LANGVIEW_RIGHTMARGIN > bounds.getWidth())	{
			bounds.setWidth(text.getWidth()+Objsc::LANGVIEW_RIGHTMARGIN);
			placeSockets();
		} else if(text.getWidth()+Objsc::LANGVIEW_RIGHTMARGIN < Objsc::LANGVIEW_DFTWIDTH) {
			bounds.setWidth(Objsc::LANGVIEW_DFTWIDTH);
			placeSockets();
		}
	}

	void LangObjectView::initSockets(std::tr1::weak_ptr<Objsc::LangView> weakPtr)
	{
		if(object)
		{
			for(InOutLetList::const_iterator itIn =  object->getInlets().begin();
			    itIn != object->getInlets().end();
			    itIn++)
			{
				std::tr1::shared_ptr<Objsc::SocketView> sv(Objsc::SocketView::createInlet());
				sv->setAction(weakPtr, *itIn);
				inlets.insert( std::pair< std::string, std::tr1::shared_ptr<Objsc::SocketView> >(*itIn, sv));
			}

			for(InOutLetList::const_iterator itOut =  object->getOutlets().begin();
			    itOut != object->getOutlets().end();
			    itOut++)
			{
				std::tr1::shared_ptr<Objsc::SocketView> sv(Objsc::SocketView::createOutlet());
				sv->setAction(weakPtr, *itOut);
				outlets.insert( std::pair< std::string, std::tr1::shared_ptr<Objsc::SocketView> >(*itOut, sv));
			}
			placeSockets();
		}
	}

	void LangObjectView::placeSockets()
	{
		float inwidth = 0.0f;
		float outwidth = 0.0f;
		float sktwidth = 0.0f;

		if(!inlets.empty())	{
			inwidth = (inlets.size()*Objsc::LANGVIEW_SOCKETWIDTH) + ((inlets.size()-1)*2.0f);
		}
		if(!outlets.empty())	{
			outwidth = (outlets.size()*Objsc::LANGVIEW_SOCKETWIDTH) + ((outlets.size()-1)*2.0f);
		}
		sktwidth = outwidth>inwidth?outwidth:inwidth;
		if(sktwidth > bounds.getWidth())	{
			bounds.setWidth(sktwidth);
		}

		float inspace = 0.0f;
		float outspace = 0.0f;

		float i = 0.0f;

		if(inlets.size() > 1)
			inspace = (bounds.getWidth() - (inlets.size() * Objsc::LANGVIEW_SOCKETWIDTH)) / (inlets.size() - 1);

		for(Objsc::SocketViewMap::iterator skt = inlets.begin(); skt != inlets.end(); ++skt)
		{
			skt->second->setLocation( bounds.getX() + (i * (Objsc::LANGVIEW_SOCKETWIDTH + inspace) ),
						  bounds.getY());
			i += 1.0f;
		}

		if(outlets.size() > 1)
			outspace = (bounds.getWidth() - (outlets.size() * Objsc::LANGVIEW_SOCKETWIDTH)) / (outlets.size()-1);
		i = 0;
		for(Objsc::SocketViewMap::iterator skt = outlets.begin(); skt != outlets.end(); ++skt)
		{
			skt->second->setLocation(bounds.getX() + (i * (Objsc::LANGVIEW_SOCKETWIDTH + outspace)),
						 bounds.getBottom() - Objsc::LANGVIEW_SOCKETHEIGHT);
			i += 1.0f;
		}



	}

	void LangObjectView::initActions(std::tr1::weak_ptr<Objsc::LangView> weakPtr)
	{
		actionParse->getData()->addArg(weakPtr);
	}

	void LangObjectView::update()
	{

	}

	void LangObjectView::draw()
	{
		if (visible)	{
			ofSetColor(bgColour.r, bgColour.g, bgColour.b, bgColour.a);
			ofFill();
			ofRect(bounds.getX(), bounds.getY(), bounds.getWidth(), bounds.getHeight());
			text.draw();
			ofSetColor(fgColour.r, fgColour.g, fgColour.b, fgColour.a);
			outlineBox(bounds);
			for(SocketViewMap::iterator in = inlets.begin(); in != inlets.end(); ++in)
			{
				in->second->draw();
			}
			for(SocketViewMap::iterator out = outlets.begin(); out != outlets.end(); ++out)	{
				out->second->draw();
			}
		}
	}

	void LangObjectView::_setLocation(float x, float y)
	{
		bounds.setLocation(x,y);
		inrect.setLocation(bounds.getX()+Objsc::LANGVIEW_MARGIN, bounds.getY()+Objsc::LANGVIEW_MARGIN);
		text.setLocation(x,y);
		placeSockets();
	}

	void LangObjectView::_setBounds(float x, float y, float width, float height)
	{
		bounds.set(x,y,width,height);
		inrect.set(bounds.getX()+Objsc::LANGVIEW_MARGIN, bounds.getY()+Objsc::LANGVIEW_MARGIN, bounds.getWidth()-Objsc::LANGVIEW_MARGIN*2, bounds.getHeight()-Objsc::LANGVIEW_MARGIN*2);
		text.setBounds(x,y,width,height);
		placeSockets();
	}


	void LangObjectView::keyPressed(int key)
	{
		consumed = false;
		if(hasFocus) 	{
			if(key == OF_KEY_RETURN)	{
				actionParse->getData()->setKwArg("str", text.getText());
				Objsc::CommandDispatcher::Current()->doCommand(actionParse);
			} else {
				text.keyPressed(key);
				fitToText();
			}
			consumed = true;
		}
	}

	void LangObjectView::keyReleased(int key)
	{
		consumed = false;
		if(hasFocus) 	{
			text.keyReleased(key);
		}
	}

	void LangObjectView::mouseMoved(int x, int y )
	{
		if(withinBounds(x,y) && visible)	{
			if(!hasFocus && displayState!=Objsc::DISABLED) {
				gainFocus();
			}
			text.mouseMoved(x,y);
		} else {
			if(hasFocus && displayState!=Objsc::DISABLED) {
				looseFocus();
				if(dodrag)
					dodrag=false;
			}
		}
		for(SocketViewMap::iterator in = inlets.begin(); in != inlets.end(); ++in)	{
			in->second->mouseMoved(x,y);
		}
		for(SocketViewMap::iterator out = outlets.begin(); out != outlets.end(); ++out)	{
			out->second->mouseMoved(x,y);
		}
	}

	void LangObjectView::mouseDragged(int x, int y, int button)
	{
		consumed = false;
		float dX = x-scrX;
		float dY = y-scrY;
		if(dodrag)	{
			bounds.moveCentre(dX,dY);
			inrect.moveCentre(dX,dY);
			text.setLocation(bounds.getX(), bounds.getY());
			for(SocketViewMap::iterator in = inlets.begin(); in != inlets.end(); ++in)	{
				in->second->moveCentre(dX,dY);
			}
			for(SocketViewMap::iterator out = outlets.begin(); out != outlets.end(); ++out)	{
				out->second->moveCentre(dX,dY);
			}
			mapToStage();
			consumed = true;
		}
		scrX = x;
		scrY = y;
	}

	void LangObjectView::mousePressed(int x, int y, int button)
	{
		consumed = false;
		if(withinBounds(x,y))	{
			scrX = x;
			scrY = y;
			consumed = true;
			switch(button)	{
				case 0:
					for(SocketViewMap::iterator in = inlets.begin(); in != inlets.end(); ++in)	{
						in->second->mousePressed(x,y,button);
						if(in->second->eventConsumed())	{
							return;
						}
					}
					for(SocketViewMap::iterator out = outlets.begin(); out != outlets.end(); ++out)	{
						out->second->mousePressed(x,y,button);
						if(out->second->eventConsumed())	{
							return;
						}
					}
					if(onInside(x,y))	{
						text.mousePressed(x,y,button);
					} else if(onMargin(x,y))	{
						dodrag = true;
					}
					break;
				case 1:
					break;
				case 2:
					openMenu(x,y);
					break;
			}

		}
	}

	void LangObjectView::mouseReleased(int x, int y, int button)
	{
		if(dodrag) {
			dodrag=false;
		}
	}

	void LangObjectView::windowResized(int w, int h)
	{

	}

	
} // namespace Objsc
