/***************************************************************************
 *   Copyright (C) 2012 by Pierre Marchand   *
 *   pierre@oep-h.com   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#ifndef OBJSC_LANGOBJECTVIEW_H
#define OBJSC_LANGOBJECTVIEW_H

#include "../objscrLangViews.hpp"

#include "SocketView.h"

namespace Objsc {
	
	class LangObjectView : public Objsc::LangView
	{
		protected:
			Objsc::TextField		text;
			bool				dodrag;
			int					scrX;
			int					scrY;
			Objsc::CommandPtr 	actionParse;
			Objsc::SocketViewMap	inlets;
			Objsc::SocketViewMap	outlets;

			void fitToText();
			void updateText();
			void placeSockets();
			void _setLocation(float x, float y);
			void _setBounds(float x, float y, float width, float height);

		public:
			LangObjectView();
			~LangObjectView();

			void setObject(Objsc::LangObjectPtr objectPtr);
			void initSockets(std::tr1::weak_ptr<Objsc::LangView> weakPtr);
			void initActions(std::tr1::weak_ptr<Objsc::LangView> weakPtr);
			void setText(const string& str);
			void reset();

			void update();
			void draw();

			void keyPressed(int key);
			void keyReleased(int key);
			void mouseMoved(int x, int y );
			void mouseDragged(int x, int y, int button);
			void mousePressed(int x, int y, int button);
			void mouseReleased(int x, int y, int button);
			void windowResized(int w, int h);

			const float getOutletX(const std::string& index) const { return (outlets.find(index))->second->getOutletX(); }
			const float getOutletY(const std::string& index) const { return (outlets.find(index))->second->getOutletY(); }
			const float getInletX(const std::string& index) const { return (inlets.find(index))->second->getInletX(); }
			const float getInletY(const std::string& index) const { return (inlets.find(index))->second->getInletY(); }

			static std::tr1::shared_ptr<Objsc::LangObjectView> create()
			{
				std::tr1::shared_ptr<Objsc::LangObjectView> ptr(new LangObjectView());
				ptr->initSockets(std::tr1::weak_ptr<Objsc::LangView>(ptr));
				ptr->initActions(std::tr1::weak_ptr<Objsc::LangView>(ptr));
				return ptr;
			}
	};

	typedef std::tr1::shared_ptr<Objsc::LangObjectView> LangObjectViewPtr;
	typedef vector < Objsc::LangObjectViewPtr > LangObjectViewVector;

	
} // namespace Objsc

#endif // OBJSC_LANGOBJECTVIEW_H
