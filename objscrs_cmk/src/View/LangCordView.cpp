/***************************************************************************
 *   Copyright (C) 2012 by Pierre Marchand   *
 *   pierre@oep-h.com   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#include "LangCordView.h"

#include "../objscrGUIStyles.hpp"
#include "../objscrLangEditor.hpp"

namespace Objsc {

	const string Objsc::LangCordView::stylename = "langcord";
	
	LangCordView::LangCordView() : outletView(), inletView(), outlet(), inlet(), endX(), endY(), editing(false)
	{
		Objsc::StylePalette::Current()->applyStyle(Objsc::LangCordView::stylename, this);
	}


	void LangCordView::initActions(std::tr1::weak_ptr<Objsc::LangCordView> weakPtr)
	{
		actionDelete = Objsc::CommandDispatcher::createCommand(Objsc::LangPatchEditor::cmdlabel  , "cord_delete");
		actionDelete->getData()->addArg(weakPtr);
	}


	void LangCordView::start(Objsc::LangViewPtr viewPtr, std::string socket)
	{
		outletView=viewPtr;
		outlet=socket;
		endX = outletView->getOutletX(outlet);
		endY = outletView->getOutletY(outlet);
		editing = true;
		visible = true;
	}
	void LangCordView::connect(Objsc::LangViewPtr viewPtr, std::string socket, bool linkobject)
	{
		inletView=viewPtr;
		inlet=socket;
		endX = inletView->getInletX(inlet);
		endY = inletView->getInletY(inlet);
		editing = false;
		if(linkobject)
			outletView->link(outlet, inletView, inlet);
	}

	void LangCordView::unlink()
	{
		if(outletView && inletView)	{
			outletView->getObject()->unlink(outlet, inletView->getObject(), inlet);
			Objsc::CommandDispatcher::Current()->doCommand(actionDelete);
		}
	}

	void LangCordView::update()
	{

	}

	void LangCordView::draw()
	{
		if (visible)	{
			if(hasFocus) {
				ofSetColor(hiColour.r, hiColour.g, hiColour.b);
			} else {
				ofSetColor(fgColour.r, fgColour.g, fgColour.b);
			}
			if(editing)	{
				ofLine(outletView->getOutletX(outlet), outletView->getOutletY(outlet), endX, endY);
			} else {
				ofLine(outletView->getOutletX(outlet), outletView->getOutletY(outlet), inletView->getInletX(inlet), inletView->getInletY(inlet));
			}
		}
	}

	void LangCordView::keyPressed(int key)
	{
		consumed = false;
		if(!editing && hasFocus)	{
			if(key == OF_KEY_BACKSPACE)	{
				unlink();
				consumed = true;
			}
		}
	}

	void LangCordView::keyReleased(int key)
	{

	}

	bool LangCordView::withinBounds(float x, float y)
	{
		if(editing)	{
			return true;
		} else {
			if(x < outletView->getOutletX(outlet) &&  x < inletView->getInletX(inlet))
				return false;
			if(x > outletView->getOutletX(outlet) &&  x > inletView->getInletX(inlet))
				return false;
			if(y < outletView->getOutletY(outlet) &&  y < inletView->getInletY(inlet))
				return false;
			if(y > outletView->getOutletY(outlet) &&  y > inletView->getInletY(inlet))
				return false;
			return fabs((outletView->getOutletX(outlet)*(inletView->getInletY(inlet)-y))+(inletView->getInletX(inlet)*(y-outletView->getOutletY(outlet)))+(x*(outletView->getOutletY(outlet)-inletView->getInletY(inlet)))) < 160.0f;
		}
	}

	void LangCordView::mouseMoved(int x, int y )
	{
		if(editing)	{
			endX = x;
			endY = y;
		} else {
			if(withinBounds(x,y) && visible)	{
				if(!hasFocus) {
					gainFocus();
				}
			} else {
				if(hasFocus) {
					looseFocus();
				}
			}
		}
	}

	void LangCordView::mouseDragged(int x, int y, int button)
	{

	}

	void LangCordView::mousePressed(int x, int y, int button)
	{
		consumed = false;
		if(!editing && withinBounds(x,y))	{
			unlink();
			consumed = true;
		}
	}

	void LangCordView::mouseReleased(int x, int y, int button)
	{

	}

	void LangCordView::windowResized(int w, int h)
	{

	}


	
} // namespace Objsc
