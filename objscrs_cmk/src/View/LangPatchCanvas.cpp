/***************************************************************************
 *   Copyright (C) 2012 by Pierre Marchand   *
 *   pierre@oep-h.com   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#include "LangBangView.h"
#include "LangMessageView.h"
#include "LangNumberView.h"
#include "LangObjectView.h"
#include "LangPatchCanvas.h"
#include "LangToggleView.h"


#include "../objscrGUIStyles.hpp"

namespace Objsc {
	const string Objsc::LangPatchCanvas::stylename = "langpatch";

	struct cordConnects : std::binary_function< Objsc::LangCordViewPtr, Objsc::LangViewPtr, bool >	{
		bool operator() (const Objsc::LangCordViewPtr& ptr, const Objsc::LangViewPtr& objview) const	{
			return ptr->connectsWith(objview);
		}
	};

	
	LangPatchCanvas::LangPatchCanvas() : patch(), objviews(), cord(), cordviews(),  bgAlpha(120)//, namefield()
	{
		Objsc::StylePalette::Current()->applyStyle(Objsc::LangPatchCanvas::stylename, this);
		//Objsc::StylePalette::Current()->applyStyle(Objsc::LangPatchCanvas::stylename, &//namefield);
		//~ //namefield.sizeWithText(true);
		//~ //namefield.clearText();
		//~ //namefield.drawBackground(false);
	}

	void LangPatchCanvas::setPatch(Objsc::LangPatchPtr patchPtr)
	{
		if(patch)	{
			clear();
		}
		patch=patchPtr;
		//~ //namefield.setText(patch->getName());
		//~ //namefield.setLocation(bounds.getRight()-//namefield.getWidth()-Objsc::WINDOWMARGIN_WIDTH, bounds.getY());
	}

	void LangPatchCanvas::update()
	{

	}

	void LangPatchCanvas::draw()
	{
		ofSetColor(bgColour.r, bgColour.g, bgColour.b, bgAlpha);
		ofFill();
		ofRect(bounds.getX(), bounds.getY(), bounds.getWidth(), bounds.getHeight());
		//namefield.draw();
		for(LangCordViewVector::iterator crd = cordviews.begin(); crd != cordviews.end(); ++crd)	{
			(*crd)->draw();
		}
		for(LangViewVector::iterator obj = objviews.begin(); obj != objviews.end(); ++obj)	{
			(*obj)->draw();
		}
		if(cord)	{
			cord->draw();
		}
	}

	void LangPatchCanvas::setLocation(float x, float y)
	{
		bounds.setLocation(x,y);
		//namefield.setLocation(bounds.getRight()-//namefield.getWidth()-Objsc::WINDOWMARGIN_WIDTH, bounds.getY());
	}

	void LangPatchCanvas::setDimensions(float width, float height)
	{
		bounds.setDimensions(width,height);
		//namefield.setLocation(bounds.getRight()-//namefield.getWidth()-Objsc::WINDOWMARGIN_WIDTH, bounds.getY());
	}

	void LangPatchCanvas::setBounds(float x, float y, float width, float height)
	{
		bounds.set(x,y,width,height);
		//namefield.setLocation(bounds.getRight()-//namefield.getWidth()-Objsc::WINDOWMARGIN_WIDTH, bounds.getY());
	}

	void LangPatchCanvas::keyPressed(int key)
	{
		for(LangViewVector::iterator obj = objviews.begin(); obj != objviews.end(); ++obj)	{
			(*obj)->keyPressed(key);
			if((*obj)->eventConsumed())	{
				consumed = true;
				return;
			}
		}
		for(LangCordViewVector::iterator crd = cordviews.begin(); crd != cordviews.end(); ++crd)	{
			(*crd)->keyPressed(key);
			if((*crd)->eventConsumed())	{
				consumed = true;
				return;
			}
		}
	}

	void LangPatchCanvas::keyReleased(int key)
	{
		for(LangViewVector::iterator obj = objviews.begin(); obj != objviews.end(); ++obj)	{
			(*obj)->keyReleased(key);
		}
	}

	void LangPatchCanvas::mouseMoved(int x, int y )
	{
		for(LangCordViewVector::iterator crd = cordviews.begin(); crd != cordviews.end(); ++crd)	{
			(*crd)->mouseMoved(x,y);
		}
		for(LangViewVector::iterator obj = objviews.begin(); obj != objviews.end(); ++obj)	{
			(*obj)->mouseMoved(x,y);
		}
		if(cord)	{
			cord->mouseMoved(x,y);
		}
	}

	void LangPatchCanvas::mouseDragged(int x, int y, int button)
	{
		consumed = false;
		for(LangViewVector::iterator obj = objviews.begin(); obj != objviews.end(); ++obj)	{
			(*obj)->mouseDragged(x,y,button);
			if((*obj)->eventConsumed())	{
				consumed = true;
				return;
			}
		}
	}

	void LangPatchCanvas::mousePressed(int x, int y, int button)
	{
		consumed = false;
		for(LangViewVector::iterator obj = objviews.begin(); obj != objviews.end(); ++obj)	{
			(*obj)->mousePressed(x,y,button);
			if((*obj)->eventConsumed())	{
				consumed = true;
				return;
			}
		}
		for(LangCordViewVector::iterator crd = cordviews.begin(); crd != cordviews.end(); ++crd)	{
			(*crd)->mousePressed(x,y,button);
			if((*crd)->eventConsumed())	{
				consumed = true;
				return;
			}
		}
		if(cord && !consumed)	{
			consumed = true;
			dropCord();
		}
	}

	void LangPatchCanvas::mouseReleased(int x, int y, int button)
	{
		for(LangCordViewVector::iterator crd = cordviews.begin(); crd != cordviews.end(); ++crd)	{
			(*crd)->mouseReleased(x,y,button);
		}
		for(LangViewVector::iterator obj = objviews.begin(); obj != objviews.end(); ++obj)	{
			(*obj)->mouseReleased(x,y,button);
		}
		if(cord)	{
			cord->mouseReleased(x,y,button);
		}
	}

	void LangPatchCanvas::windowResized(int w, int h)
	{

	}

	void LangPatchCanvas::addObjectView(Objsc::LangViewPtr view)
	{
		view->setCanvasRect(bounds);
		view->mapToScreen();
		view->initSockets(std::tr1::weak_ptr<Objsc::LangView>(view));
		objviews.push_back(view);
	}

	void LangPatchCanvas::linkAllViews()
	{
		for(LangViewVector::iterator view = objviews.begin(); view != objviews.end(); ++view)	{
			if((*view)->getObject()->getNumOutlets()  > 0)	{
				linkFromView((*view), false);
			}
		}
	}

	void LangPatchCanvas::linkFromView(Objsc::LangViewPtr viewFrom, bool linkobject)
	{
		const Objsc::LangCordMap outlets = viewFrom->getObject()->getOutletsMap();
		for(Objsc::LangCordMap::const_iterator link = outlets.begin(); link != outlets.end(); ++link)	{
			const Objsc::LangAddressVector addrs = link->second->getAddresses();
			for(Objsc::LangAddressVector::const_iterator addr = addrs.begin(); addr != addrs.end(); ++addr)	{
				linkViews(viewFrom, link->first, viewForObject((*addr).first), (*addr).second, linkobject);
			}
		}
	}

	void LangPatchCanvas::linkViews(Objsc::LangViewPtr viewFrom, const std::string& outlet, Objsc::LangViewPtr viewTo, const std::string& inlet, bool linkobject)
	{
		if(viewFrom && viewTo)	{
			cordviews.push_back(Objsc::LangCordView::create());
			cordviews.back()->start(viewFrom,outlet);
			cordviews.back()->connect(viewTo,inlet,linkobject);
		}
	}

	Objsc::LangViewPtr& LangPatchCanvas::viewForObject(Objsc::LangObjectPtr objPtr)
	{
		for(LangViewVector::iterator view = objviews.begin(); view != objviews.end(); ++view)	{
			if((*view)->getObject() == objPtr)
				return (*view);
		}
	}

	// lang
	void LangPatchCanvas::createObject(float x, float y)
	{
		Objsc::LangObjectViewPtr view = Objsc::LangObjectView::create();
		view->setCanvasRect(bounds);
		view->setLocation(x,y);
		objviews.push_back(std::tr1::static_pointer_cast<Objsc::LangView>(view));
	}

	void LangPatchCanvas::createMessage(float x, float y)
	{
		Objsc::LangMessageViewPtr view = Objsc::LangMessageView::create();
		view->setObject(patch->createObject("message"));
		view->setCanvasRect(bounds);
		view->setLocation(x,y);
		objviews.push_back(std::tr1::static_pointer_cast<Objsc::LangView>(view));
	}

	void LangPatchCanvas::createNumber(float x, float y)
	{
		Objsc::LangNumberViewPtr view = Objsc::LangNumberView::create();
		Objsc::LangObjectPtr obj = patch->createObject("number");
		view->setObject(obj);
		view->setCanvasRect(bounds);
		view->setLocation(x,y);
		obj->setObserver(std::tr1::static_pointer_cast<Objsc::LangObserver>(view));
		objviews.push_back(std::tr1::static_pointer_cast<Objsc::LangView>(view));
	}

	void LangPatchCanvas::createBang(float x, float y)
	{
		Objsc::LangBangViewPtr view = Objsc::LangBangView::create();
		Objsc::LangObjectPtr obj = patch->createObject("bang");
		view->setObject(obj);
		view->setCanvasRect(bounds);
		view->setLocation(x,y);
		obj->setObserver(std::tr1::static_pointer_cast<Objsc::LangObserver>(view));
		objviews.push_back(std::tr1::static_pointer_cast<Objsc::LangView>(view));
	}

	void LangPatchCanvas::createToggle(float x, float y)
	{
		Objsc::LangToggleViewPtr view = Objsc::LangToggleView::create();
		Objsc::LangObjectPtr obj = patch->createObject("toggle");
		view->setObject(obj);
		view->setCanvasRect(bounds);
		view->setLocation(x,y);
		obj->setObserver(std::tr1::static_pointer_cast<Objsc::LangObserver>(view));
		objviews.push_back(std::tr1::static_pointer_cast<Objsc::LangView>(view));
	}

	void LangPatchCanvas::startCord(Objsc::LangViewPtr view, const std::string& socket)
	{
		cord = Objsc::LangCordView::create();
		cord->start(view,socket);
	}

	void LangPatchCanvas::endCord(Objsc::LangViewPtr view, const std::string& socket)
	{
		if(cord)	{
			cord->connect(view,socket);
			cordviews.push_back(cord);
			cord.reset();
		}
	}

	void LangPatchCanvas::dropCord()
	{
		cord.reset();
	}

	void LangPatchCanvas::clear()
	{
		if(cord)	{
			cord.reset();
		}
		cordviews.clear(); // MIGHT LEAK!
		objviews.clear(); // MIGHT LEAK!
	}

	void LangPatchCanvas::removeObject(Objsc::LangViewPtr objview)
	{
		cordviews.erase(remove_if(cordviews.begin(), cordviews.end(), bind2nd(cordConnects(), objview)), cordviews.end());
		objviews.erase(find(objviews.begin(), objviews.end(), objview));
		if(objview->hasObject())	{
			patch->removeObject(objview->getObject());
		}
	}


	void LangPatchCanvas::removeCord(Objsc::LangCordViewPtr cordview)
	{
		cordviews.erase(find(cordviews.begin(), cordviews.end(),cordview));
	}


	// parsing
	void LangPatchCanvas::parseStringToObject(Objsc::LangViewPtr objview, const string& str)
	{
		Objsc::LangObjectPtr objPtr = patch->parseStringToObject(str);
		if(objPtr)	{
			patch->addObject(objPtr);
			objview->setObject(objPtr);
			objview->mapToStage();
			objview->initSockets(std::tr1::weak_ptr<Objsc::LangView>(objview));
		} else {
			objview->reset();
		}
	}

	// display
	void LangPatchCanvas::toggleBG()
	{
		if(bgAlpha==255)	{
			bgAlpha=120;
		} else {
			bgAlpha=255;
		}
	}
	
} // namespace Objsc
