/***************************************************************************
 *   Copyright (C) 2012 by Pierre Marchand   *
 *   pierre@oep-h.com   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#include "LangBangView.h"

#include "../objscrGUIStyles.hpp"

namespace Objsc {
	
	LangBangView::LangBangView() : inlet(Objsc::LANG_INLET), outlet(Objsc::LANG_OUTLET), dodrag(false), scrX(0), scrY(0), showbang(false)
	{
		Objsc::StylePalette::Current()->applyStyle(Objsc::LangViewBase::stylename, this);
		setBounds(0,0, Objsc::LANGVIEW_DFTHEIGHT, Objsc::LANGVIEW_DFTHEIGHT);
	}

	LangBangView::~LangBangView()
	{
		//~ if(object)
			//~ object->removeObserver();
	}

	void LangBangView::setObject(Objsc::LangObjectPtr objectPtr)
	{
		object=objectPtr;
		//object->setObserver(); // cast to weak pointer
	}


	void LangBangView::initSockets(std::tr1::weak_ptr<Objsc::LangView> weakPtr)
	{
		inlet.setAction(weakPtr, "0");
		outlet.setAction(weakPtr, "0");
	}

	void LangBangView::update()
	{

	}

	void LangBangView::draw()
	{
		if (visible)	{
			ofSetColor(bgColour.r, bgColour.g, bgColour.b, bgColour.a);
			ofFill();
			ofRect(bounds.getX(), bounds.getY(), bounds.getWidth(), bounds.getHeight());
			if(showbang)	{
				ofSetColor(fgColour.r, fgColour.g, fgColour.b, fgColour.a);
				ofCircle(bounds.getCentreX(), bounds.getCentreY(), Objsc::LANGVIEW_DFTHEIGHT/2.0f);
				showbang=false;
			} else if(hasFocus)	{
				ofSetColor(hiColour.r, hiColour.g, hiColour.b, hiColour.a);
				ofCircle(bounds.getCentreX(), bounds.getCentreY(), Objsc::LANGVIEW_DFTHEIGHT/2.0f);
			}
			ofSetColor(fgColour.r, fgColour.g, fgColour.b);
			ofNoFill();
			ofCircle(bounds.getCentreX(), bounds.getCentreY(), Objsc::LANGVIEW_DFTHEIGHT/2.0f);
			ofFill();
			outlineBox(bounds);
			inlet.draw();
			outlet.draw();
		}
	}

	void LangBangView::_setLocation(float x, float y)
	{
		bounds.setLocation(x,y);
		inrect.setLocation(bounds.getX()+Objsc::LANGVIEW_MARGIN, bounds.getY()+Objsc::LANGVIEW_MARGIN);
		inlet.setLocation(x,y);
		outlet.setLocation(x,bounds.getBottom()-Objsc::LANGVIEW_SOCKETHEIGHT);
	}

	void LangBangView::_setBounds(float x, float y, float width, float height)
	{
		bounds.set(x,y,width,height);
		inrect.set(bounds.getX()+Objsc::LANGVIEW_MARGIN, bounds.getY()+Objsc::LANGVIEW_MARGIN, bounds.getWidth()-Objsc::LANGVIEW_MARGIN*2, bounds.getHeight()-Objsc::LANGVIEW_MARGIN*2);
		inlet.setLocation(x,y);
		outlet.setLocation(x,bounds.getBottom()-Objsc::LANGVIEW_SOCKETHEIGHT);
	}

	void LangBangView::keyPressed(int key)
	{
		consumed = false;
	}

	void LangBangView::keyReleased(int key)
	{
		consumed = false;
	}

	void LangBangView::mouseMoved(int x, int y )
	{
		if(withinBounds(x,y) && visible)	{
			if(!hasFocus && displayState!=Objsc::DISABLED) {
				if(onInside(x,y))	{
					gainFocus();
				}
			}
		} else {
			if(hasFocus && displayState!=Objsc::DISABLED) {
				looseFocus();
				if(dodrag)
					dodrag=false;
			}
		}
		inlet.mouseMoved(x,y);
		outlet.mouseMoved(x,y);
	}

	void LangBangView::mouseDragged(int x, int y, int button)
	{
		consumed = false;
		float dX = x-scrX;
		float dY = y-scrY;
		if(dodrag)	{
			bounds.moveCentre(dX,dY);
			inrect.moveCentre(dX,dY);
			inlet.setLocation(bounds.getX(), bounds.getY());
			outlet.setLocation(bounds.getX(), bounds.getBottom()-Objsc::LANGVIEW_SOCKETHEIGHT);
			mapToStage();
			consumed = true;
		}
		scrX = x;
		scrY = y;
	}

	void LangBangView::mousePressed(int x, int y, int button)
	{
		consumed = false;
		if(withinBounds(x,y))	{
			scrX = x;
			scrY = y;
			consumed = true;
			switch(button)	{
				case 0:
					inlet.mousePressed(x,y,button);
					if(inlet.eventConsumed())	{
						return;
					}
					outlet.mousePressed(x,y,button);
					if(outlet.eventConsumed())	{
						return;
					}
					if(onInside(x,y))	{
						if(object)
							object->activate();
					} else if(onMargin(x,y))	{
						dodrag = true;
					}
					break;
				case 1:
					break;
				case 2:
					openMenu(x,y);
					break;
			}
		}
	}

	void LangBangView::mouseReleased(int x, int y, int button)
	{
		if(dodrag) {
			dodrag=false;
		}
	}

	void LangBangView::windowResized(int w, int h)
	{

	}

	
} // namespace Objsc
