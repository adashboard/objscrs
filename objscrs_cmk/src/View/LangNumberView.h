/***************************************************************************
 *   Copyright (C) 2012 by Pierre Marchand   *
 *   pierre@oep-h.com   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#ifndef OBJSC_LANGNUMBERVIEW_H
#define OBJSC_LANGNUMBERVIEW_H

#include "../objscrLangViews.hpp"
#include "SocketView.h"

namespace Objsc {
	
	class LangNumberView : public Objsc::LangView, public Objsc::LangObserver
	{
		protected:
			Objsc::TextField		text;
			Objsc::SocketView		inlet;
			Objsc::SocketView		outlet;
			bool				dodrag;
			int					scrX;
			int					scrY;
			Objsc::CommandPtr 	actionParse;

			void fitToText();
			void updateText();
			void _setLocation(float x, float y);
			void _setBounds(float x, float y, float width, float height);

		public:
			LangNumberView();
			~LangNumberView();

			void update();
			void draw();

			const float getOutletX(const std::string& index) const { return outlet.getOutletX(); }
			const float getOutletY(const std::string& index) const { return outlet.getOutletY(); }
			const float getInletX(const std::string& index) const { return inlet.getInletX(); }
			const float getInletY(const std::string& index) const { return inlet.getInletY(); }

			void objActivated();

			void initSockets(std::tr1::weak_ptr<Objsc::LangView> weakPtr);
			void initActions(std::tr1::weak_ptr<Objsc::LangView> weakPtr);

			void keyPressed(int key);
			void keyReleased(int key);
			void mouseMoved(int x, int y );
			void mouseDragged(int x, int y, int button);
			void mousePressed(int x, int y, int button);
			void mouseReleased(int x, int y, int button);

			void setObject(Objsc::LangObjectPtr objectPtr);

			static std::tr1::shared_ptr<Objsc::LangNumberView> create()
			{
				std::tr1::shared_ptr<Objsc::LangNumberView> ptr(new LangNumberView());
				ptr->initSockets(std::tr1::weak_ptr<Objsc::LangView>(ptr));
				ptr->initActions(std::tr1::weak_ptr<Objsc::LangView>(ptr));
				return ptr;
			}
	};

	typedef std::tr1::shared_ptr<Objsc::LangNumberView> LangNumberViewPtr;

	
} // namespace Objsc

#endif // OBJSC_LANGNUMBERVIEW_H
