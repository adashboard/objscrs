#ifndef  OBJSCR_DVVIDEOSOURCE_H
#define OBJSCR_DVVIDEOSOURCE_H

#include "objscrFrames.hpp"
#include "objscrConstants.hpp"

#include "ofDVUtils.h"

// shared_ptr
#include <tr1/memory>

namespace Objsc
{
	////////////////////////////////////////
	// FRAME SOURCE
	////////////////////////////////////////
	class DVVideoSource : public FrameSource
	{
		protected:
			bool				active;
			//ofVideoGrabber		grabber;
			ofDVUtils				grabber;
		
		public:
			DVVideoSource():
				active(false),
				grabber()
			{}
				
			virtual ~DVVideoSource() {}
				
			void setFrameSize(int nwidth, int nheight)
			{
				width=PAL_WIDTH; //nwidth;
				height=PAL_HEIGHT; //nheight;
				//grabber.initGrabber(width,height);
				grabber.initGrabber(false, ofDVUtils::OF_DV_AVERAGE_DEINTERLACE, ofDVUtils::OF_DV_PAL);
			}
			unsigned char* getFrame() { return grabber.getPixels();}
			bool hasNewFrame() {return grabber.isFrameNew();}
			void start() { active=true; }
			void stop() { active=false; }
			void update() { if(active) {grabber.grabFrame();}}
			
			static Objsc::FrameSourcePtr create()
			{
				return Objsc::FrameSourcePtr(new DVVideoSource());
			}
	};
}

#endif // OBJSCR_DVVIDEOSOURCE_H
