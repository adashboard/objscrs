#include "objscrLangEditor.hpp"

// objscrs
#include "objscrGUIRoot.hpp"
#include "objscrFiles.hpp"


#include "Lang/MessageObject.hpp"
#include "Lang/BangObject.hpp"
#include "Lang/NumberObject.hpp"
#include "Lang/ToggleObject.hpp"

#include "View/LangBangView.h"
#include "View/LangMessageView.h"
#include "View/LangNumberView.h"
#include "View/LangObjectView.h"
#include "View/LangPatchCanvas.h"
#include "View/LangToggleView.h"

//#include "objscrLang.hpp"

// openframeworks
#include "ofMain.h"

namespace Objsc
{
	
	////////////////////////////////////////
	// DECLARATIONS
	////////////////////////////////////////
	const string Objsc::LangPatchEditor::cmdlabel   = "_langeditor_";
	
	
	////////////////////////////////////////
	// FUNCTIONS
	////////////////////////////////////////
	Objsc::LangViewPtr viewFromObject(Objsc::LangObjectPtr object)
	{
		//printf("viewFromObject <%s>\n", object->getLangName().c_str());
		if(object->getLangName() == Objsc::BangObject::langname)	{
			Objsc::LangBangViewPtr bangview = Objsc::LangBangView::create();
			bangview->setObject(object);
			object->setObserver(std::tr1::static_pointer_cast<Objsc::LangObserver>(bangview));
			return std::tr1::static_pointer_cast<Objsc::LangView>(bangview);
		} else if(object->getLangName() == Objsc::MessageObject::langname)	{
			Objsc::LangMessageViewPtr msgview = Objsc::LangMessageView::create();
			msgview->setObject(object);
			return std::tr1::static_pointer_cast<Objsc::LangView>(msgview);
		} else if(object->getLangName() == Objsc::NumberObject::langname)	{
			Objsc::LangNumberViewPtr numview = Objsc::LangNumberView::create();
			numview->setObject(object);
			object->setObserver(std::tr1::static_pointer_cast<Objsc::LangObserver>(numview));
			return std::tr1::static_pointer_cast<Objsc::LangView>(numview);
		} else if(object->getLangName() == Objsc::ToggleObject::langname)	{
			Objsc::LangToggleViewPtr tglview = Objsc::LangToggleView::create();
			tglview->setObject(object);
			object->setObserver(std::tr1::static_pointer_cast<Objsc::LangObserver>(tglview));
			return std::tr1::static_pointer_cast<Objsc::LangView>(tglview);
		} else {
			Objsc::LangObjectViewPtr objview = Objsc::LangObjectView::create();
			objview->setObject(object);
			//printf("getDisplayString() <%s>\n", object->getDisplayString().c_str());
			objview->setText(object->getDisplayString());
			return std::tr1::static_pointer_cast<Objsc::LangView>(objview);
		}
		return Objsc::LangViewPtr();
	}
	
	////////////////////////////////////////
	// PATCH EDITOR
	////////////////////////////////////////
	LangPatchEditor::LangPatchEditor() : 
			score(), 
			patchcanvas(), 
			objmenu(), 
			patchmenu(),
			patchdialogue(),
			stylename("langeditor"), 
			scrX(0), 
			scrY()
	{
		patchcanvas = Objsc::LangPatchCanvas::create();
		autosize();
		// commands
		Objsc::CommandDispatcher::Current()->registerCommandReceiver(Objsc::LangPatchEditor::cmdlabel , this);
		initMenus();
		patchcanvas->setPatch(Objsc::LangPatch::create("test_patch"));
		patchdialogue = Objsc::Dialogue::create("_langeditdia_");
	}
	
	void LangPatchEditor::initMenus()
	{
		// object menu
		objmenu = Objsc::ContextMenu::create();
		objmenu->addItem("object", Objsc::CommandDispatcher::createCommand(Objsc::LangPatchEditor::cmdlabel , "new_object"));
		objmenu->addItem("message", Objsc::CommandDispatcher::createCommand(Objsc::LangPatchEditor::cmdlabel , "new_message"));
		objmenu->addItem("number", Objsc::CommandDispatcher::createCommand(Objsc::LangPatchEditor::cmdlabel , "new_number"));
		objmenu->addItem("bang", Objsc::CommandDispatcher::createCommand(Objsc::LangPatchEditor::cmdlabel , "new_bang"));
		objmenu->addItem("toggle", Objsc::CommandDispatcher::createCommand(Objsc::LangPatchEditor::cmdlabel , "new_toggle"));
		// patch menu
		patchmenu = Objsc::ContextMenu::create();
		//~ patchmenu->addItem("new patch", Objsc::CommandDispatcher::createCommand(Objsc::LangPatchEditor::cmdlabel , "new_patch"));
		//~ patchmenu->addItem("open patch", Objsc::CommandDispatcher::createCommand(Objsc::LangPatchEditor::cmdlabel , "open_patch"));
		//~ patchmenu->addItem("save patch", Objsc::CommandDispatcher::createCommand(Objsc::LangPatchEditor::cmdlabel , "save_patch"));
		//~ patchmenu->addItem("save patch as", Objsc::CommandDispatcher::createCommand(Objsc::LangPatchEditor::cmdlabel , "rename_patch"));
		patchmenu->addItem("close", Objsc::CommandDispatcher::createCommand("_gui_", "close_patch"));
	}
	
	void LangPatchEditor::update()
	{
		patchcanvas->update();
	}
	
	void LangPatchEditor::draw()
	{
		if(active)	{
			patchcanvas->draw();
		}
	}
	
	void LangPatchEditor::disable()
	{
		active = false;
	}
	
	void LangPatchEditor::toggleBG()
	{
		if(active)	{
			patchcanvas->toggleBG();
		}
	}
	
	// patch
	void LangPatchEditor::setPatch(Objsc::LangPatchPtr patchPtr) 
	{
		patchcanvas->setPatch(patchPtr);
		for(Objsc::LangObjectListIter obj = patchPtr->objectsBegin(); obj != patchPtr->objectsEnd(); ++obj)	{
			patchcanvas->addObjectView(viewFromObject((*obj)));
		}
		patchcanvas->linkAllViews();
		Objsc::GUIRoot::Current()->setZonePath(patchPtr->getName());
	}
	// commands
	void LangPatchEditor::doCommand(Objsc::CommandPtr& cmd)
	{
		//std::cout << "LangPatchEditor::doCommand: " << cmd->getName() << std::endl;
		string cmdname = cmd->getName();
		if(cmdname ==  "new_object")	{
			patchcanvas->createObject(scrX,scrY);
			Objsc::GUIRoot::Current()->closeRootMenu();
		} else if(cmdname ==  "new_message")	{
			patchcanvas->createMessage(scrX,scrY);
			Objsc::GUIRoot::Current()->closeRootMenu();
		} else if(cmdname ==  "new_number")	{
			patchcanvas->createNumber(scrX,scrY);
			Objsc::GUIRoot::Current()->closeRootMenu();
		} else if(cmdname ==  "new_bang")	{
			patchcanvas->createBang(scrX,scrY);
			Objsc::GUIRoot::Current()->closeRootMenu();
		} else if(cmdname ==  "new_toggle")	{
			patchcanvas->createToggle(scrX,scrY);
			Objsc::GUIRoot::Current()->closeRootMenu();
		} else if(cmdname ==  "start_cord")	{
			std::string socket;
			Objsc::LangViewWeakPtr view;
			Objsc::PacketPtr data = cmd->getData();
			data->getArg(0,&socket);
			data->getArg(1,&view);
			patchcanvas->startCord(Objsc::LangViewPtr(view),socket);
		} else if(cmdname ==  "end_cord")	{
			std::string socket;
			Objsc::LangViewWeakPtr view;
			Objsc::PacketPtr data = cmd->getData();
			data->getArg(0,&socket);
			data->getArg(1,&view);
			patchcanvas->endCord(Objsc::LangViewPtr(view),socket);
		} else if(cmdname ==  "cord_delete")	{
			Objsc::LangCordViewWeakPtr view;
			Objsc::PacketPtr data = cmd->getData();
			data->getArg(0,&view);
			patchcanvas->removeCord(Objsc::LangCordViewPtr(view));
		} else if(cmdname ==  "new_patch")	{
			patchdialogue->setText("new_patch");
			Objsc::GUIRoot::Current()->openDialogue(patchdialogue);
		} else if(cmdname ==  "open_patch")	{
			patchdialogue->setText("open_patch");
			Objsc::GUIRoot::Current()->openDialogue(patchdialogue);
		} else if(cmdname ==  "save_patch")	{
			patchdialogue->setText("save_patch");
			Objsc::GUIRoot::Current()->openDialogue(patchdialogue);
		} else if(cmdname ==  "rename_patch")	{
			patchdialogue->setText("rename_patch");
			Objsc::GUIRoot::Current()->openDialogue(patchdialogue);
		} else if(cmdname ==  "lobj_edit")	{
			Objsc::GUIRoot::Current()->closeRootMenu();
			Objsc::LangViewPtr view;
			cmd->getData()->getArg(0,&view);
			if(view)	{
				view->startEdit();
			}
		} else if(cmdname ==  "lobj_delete")	{
			Objsc::LangViewPtr view;
			cmd->getData()->getArg(0,&view);
			patchcanvas->removeObject(view);
			Objsc::GUIRoot::Current()->closeRootMenu();
		} else if(cmdname ==  "lobj_help")	{
			Objsc::LangViewPtr view;
			cmd->getData()->getArg(0,&view);
			if(view)	{
				ofLaunchBrowser(Objsc::ProjectFiles::Current()->makeObjectDocFilePath(view->getObject()->getLangName()));
			}
			Objsc::GUIRoot::Current()->closeRootMenu();
		}  else if(cmdname ==  "lobj_parse")	{
			Objsc::LangViewPtr view;
			string pstr = "";
			cmd->getData()->getArg(0,&view);
			cmd->getData()->getKwArg("str",&pstr);
			patchcanvas->parseStringToObject(view, pstr);
		}		
	}
	
	void LangPatchEditor::undoCommand(Objsc::CommandPtr& cmd)
	{
		std::cout << "LangPatchEditor::undoCommand: " << cmd->getName() << std::endl;
	}
	
	
	void LangPatchEditor::keyPressed(int key)
	{
		patchcanvas->keyPressed(key);
	}
	
	void LangPatchEditor::keyReleased(int key)
	{
		patchcanvas->keyReleased(key);
	}
	
	void LangPatchEditor::mouseMoved(int x, int y )
	{
		patchcanvas->mouseMoved(x,y);
	}
	
	void LangPatchEditor::mouseDragged(int x, int y, int button)
	{
		patchcanvas->mouseDragged(x,y,button);
	}
	
	void LangPatchEditor::mousePressed(int x, int y, int button)
	{
		consumed = false;
		patchcanvas->mousePressed(x,y,button);
		if(patchcanvas->eventConsumed())	{
			consumed = true;
			return;
		}
		switch(button)	{
			case 0:
				consumed = true;
				scrX = x;
				scrY = y;
				Objsc::GUIRoot::Current()->openRootMenu(objmenu, x, y);
				break;
			case 1:
				//openRootMenu("main", x, y);
				break;
			case 2:
				consumed = true;
				scrX = x;
				scrY = y;
				Objsc::GUIRoot::Current()->openRootMenu(patchmenu, x, y);
				break;
		}
	}
	
	void LangPatchEditor::mouseReleased(int x, int y, int button)
	{
		patchcanvas->mouseReleased(x,y,button);
	}
	
	void LangPatchEditor::windowResized(int w, int h)
	{
		patchcanvas->windowResized(w,h);
	}
	
	void LangPatchEditor::autosize()
	{
		//setBounds(WINDOWMARGIN_WIDTH,WINDOWMARGIN_HEIGHT, ofGetWidth()-(WINDOWMARGIN_WIDTH*2),ofGetHeight()-(WINDOWMARGIN_HEIGHT*2));
		setBounds(0,0,ofGetWidth(),ofGetHeight());
		patchcanvas->setBounds(bounds.getX(), bounds.getY(), bounds.getWidth(), bounds.getHeight());
	}
}

