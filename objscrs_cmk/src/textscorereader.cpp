/***************************************************************************
 *   Copyright (C)  2012 by Pierre Marchand                   *
 *   pierre@oep-h.com   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "textscorereader.h"

#include "objscrApp.hpp"
#include "objscrGUIRoot.hpp"
#include "objscrFiles.hpp"

#include "Lang/LangObjectFactory.h"
#include "Lang/MessageObject.hpp"

// boost
#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>

#include <fstream>
#include <cmath>

namespace Objsc
{
	const std::string TextScoreReader::startMap = std::string("map");
	const std::string TextScoreReader::endMap = std::string("endmap");
	const std::string TextScoreReader::startZone = std::string("zone");
	const std::string TextScoreReader::endZone = std::string("endzone");
	const std::string TextScoreReader::keyLink = std::string("link");
	const std::string TextScoreReader::keyReceive = std::string("receiver");

	TextScoreReader::TextScoreReader()
	{
	}

	void TextScoreReader::read(const string &filename)
	{
		std::string filepath(Objsc::ProjectFiles::Current()->makeScorePath(filename));
		std::ifstream infile(filepath.c_str());
		if(infile.is_open())
		{
			std::string str((std::istreambuf_iterator<char>(infile)), std::istreambuf_iterator<char>());
			load(str);
			infile.close();
		}
		else
			std::cerr << "Failed to open: "<< filename << std::endl;

	}

	void TextScoreReader::load(const std::string &text)
	{
		lines.clear();
		stringList slines;
		boost::algorithm::split(slines, text, boost::algorithm::is_any_of("\n"), boost::algorithm::token_compress_on);
		for(stringList::const_iterator lit(slines.begin()); lit != slines.end(); lit++)
		{
			if(boost::algorithm::starts_with(*lit, "#"))
				continue;
			stringList toks;
			boost::algorithm::split(toks, *lit, boost::algorithm::is_space(), boost::algorithm::token_compress_on);
			lines.push_back(toks);
		}

		score = Objsc::App::Current()->newScore();
		readText();
		score->removeZoneMap("map_0");
		Objsc::GUIRoot::Current()->newScoreLoaded();

	}


	void TextScoreReader::readText()
	{
		lineList::const_iterator liter(lines.begin());
		for(;liter != lines.end(); liter++)
		{
			if(liter->at(0) == startMap)
			{
				readMap(liter);
			}
		}
	}

	void TextScoreReader::readMap(lineList::const_iterator &lineIterator)
	{
		Objsc::ZoneMapPtr m(score->createZoneMap(lineIterator->at(1)));
		score->setZoneMap(lineIterator->at(1));
		lineIterator++;
		for(;lineIterator != lines.end(); lineIterator++)
		{
			if(lineIterator->at(0) == endMap)
				return;
			if(Objsc::ZoneFactory::Current()->hasCreator( lineIterator->at(0) ))
			{
				readZone(lineIterator, m);
			}
		}
	}

	void TextScoreReader::readZone(lineList::const_iterator &lineIterator, ZoneMapPtr &omap)
	{
		objects.clear();
		Objsc::ZonePtr z(omap->createZone(lineIterator->at(0)));
		if(lineIterator->size() > 4)
		{
			float x(boost::lexical_cast<float>(lineIterator->at(1)));
			float y(boost::lexical_cast<float>(lineIterator->at(2)));
			float w(boost::lexical_cast<float>(lineIterator->at(3)));
			float h(boost::lexical_cast<float>(lineIterator->at(4)));
			z->setBounds(x,y,w,h);
		}

//		Objsc::LangPatchPtr p(score->createPatch(z->getName()));
		Objsc::LangPatchPtr p(Objsc::LangPatch::create());
		z->setPatch(p);
		lineIterator++;
		readObjects(lineIterator, p);
		layout();

	}

	void TextScoreReader::readObjects(lineList::const_iterator &lineIterator, LangPatchPtr &opatch)
	{
		for(;lineIterator != lines.end(); lineIterator++)
		{
			if(lineIterator->at(0) == endMap || lineIterator->at(0) == endZone )
				return;
			else if(lineIterator->at(0) == keyLink)
			{
				linkObjects(lineIterator);
			}
			else if(lineIterator->at(0) == keyReceive)
			{
				if(lineIterator->size() > 1 && objects.count(lineIterator->at(1)) > 0)
					opatch->addObjectToReceiver(lineIterator->at(1), objects[lineIterator->at(1)]);
			}
			else
			{
				if(LangObjectFactory::Current()->hasCreator(lineIterator->at(0)) && lineIterator->size() > 1)
				{
					LangObjectPtr o(opatch->createObject(lineIterator->at(0), false));
					if(lineIterator->size() > 2)
					{
						Objsc::LangList l;
						std::string args;
						for(stringList::const_iterator sit(lineIterator->begin() + 2); sit != lineIterator->end(); sit++)
						{
							args.append(*sit);
							args.append(" ");
						}
						Objsc::tokenizeString(args, l);
						o->parseArgs(&l);
						// pff, exceptions
						if(o->getLangName() == Objsc::MessageObject::langname)
							o->setDisplayString(args);
					}
					o->setIdentifier(lineIterator->at(1));
					objects[lineIterator->at(1)] = o;
				}
			}

		}
	}

	void TextScoreReader::linkObjects(lineList::const_iterator &lineIterator)
	{
		if(lineIterator->size() < 3)
			return;

		stringList source;
		boost::algorithm::split(source, lineIterator->at(1), boost::algorithm::is_any_of("."), boost::algorithm::token_compress_on);
		stringList target;
		boost::algorithm::split(target, lineIterator->at(2), boost::algorithm::is_any_of("."), boost::algorithm::token_compress_on);

		if(source.size() == 2
				&& target.size() == 2
				&& objects.count(source.at(0)) > 0
				&& objects.count(target.at(0)) > 0)
		{
			objects[source.at(0)]->link(source.at(1), objects[target.at(0)], target.at(1));
		}
	}

	void TextScoreReader::layout()
	{
		const int nObj(objects.size());
		const float cellArea((1000.0f * 1000.0f) / float(nObj));
		const float edge(sqrt(cellArea));
		const int byRow(1000.0f / edge);
		const int numRow((nObj / byRow) + 1);
		const float rowHeight(1000.0f / float(numRow));
		float x(10);
		float y(10);
		int count(0);
		for(std::map<std::string, Objsc::LangObjectPtr>::const_iterator oit(objects.begin()); oit != objects.end(); oit++)
		{
			if(count >= byRow)
			{
				y += rowHeight;
				x = 10;
				count = 0;
			}
			oit->second->setLocation(x / 1000.0f, y / 1000.0f);
			x += edge;
			++count;
		}
	}

} // namespace Objsc
