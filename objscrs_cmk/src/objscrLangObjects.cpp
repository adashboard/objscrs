#include "objscrLangObjects.hpp"


#include <stdlib.h>

// for printscreen
#include "objscrGUIRoot.hpp"

// STL
#include <algorithm>
#include <functional>

#include <boost/lexical_cast.hpp>

namespace Objsc
{
	
	////////////////////////////////////////
	// DECLARATIONS
	////////////////////////////////////////
	const string Objsc::LangObject::langname = "_undefined_";
	
	
	/////////////////////////////////////
	// FUNCTIONS
	/////////////////////////////////////

	
	string makeReceiverName(const string& langname)
	{
		char cstr[256];
		sprintf(cstr, "_%s_", langname.c_str());
		return string(cstr);
	}
	

	struct objectIsLinked : std::binary_function< Objsc::LangAddress, Objsc::LangObjectPtr, bool >	
	{
		bool operator() (const Objsc::LangAddress& addr, const Objsc::LangObjectPtr& obj) const	{
			return (addr.first == obj);
		}
	};
	

	
	/////////////////////////////////////
	// LANG CORD
	/////////////////////////////////////
	void LangCord::send(LangAtom* atom)
	{
		for(Objsc::LangAddressVector::iterator obj = objects.begin(); obj != objects.end(); ++obj)	{
			obj->first.get()->receive(obj->second, atom);
		}
	}
	
	void LangCord::unlinkAll(Objsc::LangObjectPtr objectPtr)
	{
		objects.erase(find_if(objects.begin(), objects.end(), bind2nd(Objsc::objectIsLinked(), objectPtr)));
	}
	
	bool LangCord::connectsTo(Objsc::LangObjectPtr objectPtr)
	{
		for(Objsc::LangAddressVector::iterator obj = objects.begin(); obj != objects.end(); ++obj)	{
			if(obj->first == objectPtr)	{
				return true;
			} 
		}
		return false;
	}
	
	/////////////////////////////////////
	// LANG RECEIVER
	/////////////////////////////////////
	void LangReceiver::add(LangObjectPtr objectPtr)
	{
		//printf("LangReceiver::add <%s>\n", objectPtr->getLangName().c_str());
		objects.push_back(objectPtr);
	}
	
	void LangReceiver::remove(LangObjectPtr objectPtr)
	{
		//printf("LangReceiver::remove <%s>\n", objectPtr->getLangName().c_str());
		objects.erase(find(objects.begin(), objects.end(), objectPtr));
	}
			
	void LangReceiver::receive(LangAtom* atom)
	{
		//printf("LangReceiver::receive A %d\n", atom->getType());
		for(Objsc::LangObjectList::iterator obj = objects.begin(); obj != objects.end(); ++obj)	{
			//printf("LangReceiver::receive B <%s>\n", (*obj)->getLangName().c_str());
			(*obj)->receive("0", atom);
		}
	}
	
	/////////////////////////////////////
	// LANG OBJECT
	/////////////////////////////////////
	LangObject::LangObject() : 
		num_inlets(), 
		num_outlets(), 
		outlets(), 
		x(0.0f), 
		y(0.0f), 
		show_inlets(true), 
		show_outlets(true), 
		displaystr(), 
		min_args(0), 
		max_args(0) 
	{}
			
	void LangObject::setInlets(InOutLetList inlts)
	{
		num_inlets = inlts.size();
		named_inlets = inlts;
	}

	void LangObject::setOutlets(InOutLetList outlts)
	{
		num_outlets = outlts.size();
		named_outlets = outlts;
		outlets.clear();
		for(InOutLetList::const_iterator out = outlts.begin(); out != outlts.end() ; out++)
		{
			std::string so(*out);
			outlets.insert(LangCordEntry(so, new LangCord()));
		}
	}

	void LangObject::setInlets(int size)
	{
		InOutLetList in;
		for(int i(0); i < size; ++i)
		{
			in.push_back(intToString(i));
		}
		setInlets(in);
	}

	void LangObject::setOutlets(int size) 
	{ 
		InOutLetList out;
		for(int i(0); i < size; ++i)
		{
			out.push_back(intToString(i));
		}
		setOutlets(out);
	}
	
	const bool LangObject::checkInlet(const std::string &in) const
	{
		const size_t iSize(named_inlets.size());
		if(iSize > 0)
		{
			for(InOutLetList::const_iterator it(named_inlets.begin()); it != named_inlets.end(); it++)
			{
				if((*it) == in)
					return true;
			}
		}
		return false;
	}

	const int LangObject::definedOutlets() const
	{
		if(!show_outlets)	{
			return 0;
		} else {
			int sum = 0;
			for(LangCordMap::const_iterator out = outlets.begin(); out != outlets.end(); ++out) {
				sum += out->second->numAddresses();
			}
			return sum;
		}
	}
	
	
	void LangObject::link(std::string outlet, LangObjectPtr objectPtr, std::string inlet)
	{
		if(outlets.find(outlet) != outlets.end())
		{
			outlets[outlet]->link(objectPtr, inlet);
		}
	}
	
	void LangObject::unlink(std::string outlet, LangObjectPtr objectPtr, std::string inlet)
	{
		if(outlets.find(outlet) != outlets.end())
		{
			outlets[outlet]->unlink(objectPtr, inlet);
		}
	}
			
	void LangObject::unlinkAll(Objsc::LangObjectPtr objectPtr)
	{
		for(LangCordMap::iterator out = outlets.begin(); out != outlets.end(); ++out)
		{
			if(out->second->connectsTo(objectPtr))	{
				out->second->unlinkAll(objectPtr);
			} 
		}
	}
	
	bool LangObject::connectsTo(Objsc::LangObjectPtr objectPtr)
	{
		for(LangCordMap::iterator out = outlets.begin(); out != outlets.end(); ++out)
		{
			if(out->second->connectsTo(objectPtr))
			{
				return true;
			}
		}
		return false;
	}
	
	void LangObject::clearOutlet(std::string outlet)
	{
		if(outlets.find(outlet) != outlets.end())
		{
			outlets[outlet]->clear();
		} 
	}
	
	void LangObject::clearOutlets()
	{
		for(LangCordMap::iterator out = outlets.begin(); out != outlets.end(); ++out) {
			out->second->clear();
		}
		outlets.clear();
	}
	
	void LangObject::setLocation(float nx, float ny)
	{
		x = nx;
		y = ny;
	}
	
	const string& LangObject::getDisplayString()
	{
		if(displaystr.empty())	{
			return getLangName();
		}
		return displaystr;
	}
	
	void LangObject::receive(std::string inlet, Objsc::LangAtom* atom)
	{
		if(checkInlet(inlet))	{
			switch(atom->getType())	{
				case LANG_NULL:
					break;
				case LANG_BANG:
					receiveBang(inlet);
					break;
				case LANG_INT:
					receiveInt(inlet, parseInt(atom));
					break;
				case LANG_FLOAT:
					receiveFloat(inlet, parseFloat(atom));
					break;
				case LANG_STRING:
					receiveString(inlet, parseString(atom));
					break;
				case LANG_LIST:
					receiveList(inlet, dynamic_cast<Objsc::LangList*>(atom));
					break;
				case LANG_VAR:
					break;
			}
		}
	}
	
	void LangObject::send(std::string index, LangAtom* atom)
	{
		if(outlets.find(index) != outlets.end())
		{
			outlets[index]->send(atom);
		} 
	}

	void LangObject::send(int idx, LangAtom *atom)
	{
		send(intToString(idx), atom);
	}
	
	void LangObject::parseValue(LangAtom* atom)
	{
		switch(atom->getType())	{
			case LANG_NULL:
				break;
			case LANG_BANG:
				break;
			case LANG_INT:
				//std::cout << parseInt(atom) << std::endl;
			send("2", new Objsc::LangInt(parseInt(atom)));
				break;
			case LANG_FLOAT:
				//std::cout << parseFloat(atom) << std::endl;
			send("2", new Objsc::LangFloat(parseFloat(atom)));
				break;
			case LANG_STRING:
				//std::cout << parseString(atom) << std::endl;
			send("2", new Objsc::LangString(parseString(atom)));
				break;
			case LANG_LIST:
				break;
			case LANG_VAR:
				break;
		}
	}
	
	void LangObject::receiveBang(std::string inlet)
	{ 
		//std::cout << "bang!" << std::endl; 
		send("0", new Objsc::LangBang());
	}
	void LangObject::receiveInt(std::string inlet, int value)
	{
		//std::cout << "int: " << value << std::endl;
		send("1", new Objsc::LangInt(value));
	}
	void LangObject::receiveFloat(std::string inlet, float value)
	{
		//std::cout << "float: " << value << std::endl; 
		send("1", new Objsc::LangFloat(value));
	}
	void LangObject::receiveString(std::string inlet, string value)
	{
		//std::cout << "string: " << value << std::endl;
		send("1", new Objsc::LangString(value));
	}
	void LangObject::receiveList(std::string inlet, LangList* value)
	{	
		//std::cout << "list: " << std::endl; 
		for(LangAtomIter atom = value->begin(); atom != value->end(); ++atom) {
			parseValue((*atom)); 
		}
	}

}



