#ifndef  OBJSCR_PACKETS_H
#define OBJSCR_PACKETS_H

// STL
#include <iostream>
#include <string>
#include <vector>
#include <map>
#include <exception>
using std::string;
using std::vector;
using std::map;
using std::exception;

// shared_ptr
#include <tr1/memory>


namespace Objsc
{
	class AbstractPacketData
	{
		public:
			virtual ~AbstractPacketData() {}
				
			//virtual void print() {std::cout << "AbstractPacketData" << std::endl;}
			virtual void* get() {return NULL;}
	};
	
	typedef std::tr1::shared_ptr<Objsc::AbstractPacketData> APDPtr;
	typedef vector < Objsc::APDPtr > APDVector;
	typedef map < string, Objsc::APDPtr > APDMap;
	typedef APDMap::value_type APDMapEntry;
	
	template <typename T>
	class PacketData : public AbstractPacketData
	{
		private:
			T		data;
		
		public:
			PacketData() : data() {}
			PacketData(T value) : data(value) {}
			~PacketData() {}
			void set(T value) { data = value; }
			void* get() { return (void*)&data;}
			//void print() {std::cout << "PacketData: " << data << std::endl;}
	};
	
	class Packet
	{
		private:
			Objsc::APDVector	args;
			Objsc::APDMap	kwargs;
		
		public:
			Packet() : args(), kwargs() {}
			~Packet()
			{
				args.clear();
				kwargs.clear();
			}
			
			//~ void printArgs()
			//~ {
				//~ for(Objsc::APDVector::iterator it = args.begin(); it != args.end(); ++it)	{
					//~ (*it).get()->print();
				//~ }
			//~ }
			
			//~ void printKwArgs()
			//~ {
				//~ for(Objsc::APDMap::iterator it = kwargs.begin(); it != kwargs.end(); ++it)	{
					//~ std::cout << "key: " << it->first << std::endl;
					//~ it->second.get()->print();
				//~ }
			//~ }
			
			template <typename T>
			void addArg(T value) { args.push_back(APDPtr(new Objsc::PacketData<T>(value))); }
			
			APDPtr getArg(int i){return args[i];}
			
			template <typename T> 
			void getArg(int i, T* value)
			{
				if(i > -1 && i < args.size())	{
					(*value) = *(static_cast< T* >(args[i].get()->get()));
				}
			}
			
			unsigned int sizeArgs() const {return args.size();}
			void clearArgs() {args.clear();}
			
			template <typename T>
			void addKwArg(string key, T value) { kwargs.insert(Objsc::APDMapEntry(key, APDPtr(new Objsc::PacketData<T>(value)))); }
			
			template <typename T>
			void setKwArg(string key, T value) 
			{ 
				if(kwargs.count(key) > 0)	{
					kwargs[key] = APDPtr(new Objsc::PacketData<T>(value)); 
				} else {
					kwargs.insert(Objsc::APDMapEntry(key, APDPtr(new Objsc::PacketData<T>(value))));
				}
			}
			
			APDPtr getKwArg(string key){return kwargs[key];}
			
			template <typename T> 
			void getKwArg(string key, T* value)
			{
				if(kwargs.count(key) > 0)	{
					(*value) = *(static_cast< T* >(kwargs[key].get()->get()));
				}
			}
			
			unsigned int sizeKwArgs() const {return kwargs.size();}
			void clearKwArgs() {kwargs.clear();}
			
			void clear()
			{
				clearArgs();
				clearKwArgs();
			}
	};
	
	typedef std::tr1::shared_ptr<Objsc::Packet> PacketPtr;
	
}

#endif // OBJSCR_PACKETS_H
