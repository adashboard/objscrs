#ifndef OBJSCR_BODYVIEWS_H
#define OBJSCR_BODYVIEWS_H


// objscrs
#include "objscrConstants.hpp"
#include "objscrGUI.hpp"
#include "objscrGeom.hpp"
#include "objscrCommands.hpp"
#include "objscrBody.hpp"

// STL
#include <string>
#include <vector>
#include <map>
#include <list>
using std::string;
using std::vector;
using std::map;
using std::list;

// shared_ptr
#include <tr1/memory>


namespace Objsc
{
	////////////////////////////////////////
	// BODY VIEW
	////////////////////////////////////////
	class BodyView : public Objsc::Widget
	{
		protected:
			Objsc::BodyPtr 		body;
			ofPoint             		centroid;
			int					scrX;
			int					scrY;
			Objsc::TextField		labelfield;
			Objsc::TextField		stagenumfield;
			Objsc::ContextMenuPtr 	editmenu;
			bool				drawBox;
			bool				drawLoc;
			bool				drawLabel;
			bool				drawContour;
	
			//~ void mapToScreen();
			//~ void mapToStage();
			void openMenu(int x, int y);

			std::list<vector<ofPoint> > storePoints;
		
		public:
			BodyView();
			virtual ~BodyView() {}
			static const string stylename;
			void setBody(Objsc::BodyPtr bodyPtr);
			Objsc::BodyPtr getBody() {return body;}
			const bool matchesBody(Objsc::BodyPtr bodyPtr) const { return body == bodyPtr; }
			// geom
			virtual void setLocation(float x, float y);
			virtual void setDimensions(float width, float height);
			virtual void setBounds(float x, float y, float width, float height);
			// runtime
			void update();
			void draw();
			// events
			void keyPressed(int key);
			void keyReleased(int key);
			void mouseMoved(int x, int y);
			void mouseDragged(int x, int y, int button);
			void mousePressed(int x, int y, int button);
			void mouseReleased(int x, int y, int button);
			void windowResized(int w, int h);
			void boundsResized();
			// display
			void setDrawBox(bool value) { drawBox=value; }
			const bool getDrawBox() const { return drawBox; }
			void setDrawLoc(bool value) { drawLoc=value; }
			const bool getDrawLoc() const { return drawLoc; }
			void setDrawContour(bool value) { drawContour=value; }
			const bool getDrawContour() const { return drawContour; }
			// label
			void updateLabel();
			void setLabel(const string &labelStr);
			void clearLabel();
			
			static std::tr1::shared_ptr<Objsc::BodyView> create()
			{
				return std::tr1::shared_ptr<Objsc::BodyView>(new BodyView());
			}
	};
	
	typedef std::tr1::shared_ptr<Objsc::BodyView> BodyViewPtr;
	typedef vector < Objsc::BodyViewPtr > BodyViewVec;
	

	
	
}

#endif // OBJSCR_BODYVIEWS_H
