#ifndef  OBJSCR_LANGATOMS_H
#define OBJSCR_LANGATOMS_H


// STL
#include <iostream>
#include <string>
#include <list>
#include <vector>
using std::string;
using std::list;
using std::vector;

// shared_ptr
#include <tr1/memory>


namespace Objsc
{
	const int LANG_UNDEFINED = -1;
	const string LANG_BANGSTRING = "bang!";
	const string LANG_NULLSTRING = "<NULL>";
	
	/////////////////////////////////////
	// LANG ATOMS
	/////////////////////////////////////
	enum lang_type
	{
		LANG_NULL,
		LANG_BANG,
		LANG_INT,
		LANG_FLOAT,
		LANG_STRING,
		LANG_VAR,
		LANG_LIST,
		//~ LANG_ZONE,
		//~ LANG_BODY
	};
	
	class LangAtom
	{
		public:
			LangAtom() {}
			virtual ~LangAtom() {}
			virtual const lang_type getType() const { return LANG_NULL; }
			virtual bool operator== (LangAtom &rhs) { return false;}
	};
	
	typedef std::tr1::shared_ptr<Objsc::LangAtom> LangAtomPtr;
	typedef list < LangAtom* > LangAtomList;
	typedef vector < LangAtom* > LangAtomVec;
	typedef LangAtomVec::iterator LangAtomIter;
	
	class LangBang : public LangAtom
	{
		public:
			LangBang() {}
			~LangBang() {}
			const lang_type getType() const { return LANG_BANG; }
			int get() const { return 1; }
			static Objsc::LangAtomPtr create() { return Objsc::LangAtomPtr(new LangBang());}
			virtual bool operator== (LangAtom &rhs) { return rhs.getType() == LANG_BANG;}
	};
	
	class LangNull : public LangAtom
	{
		public:
			LangNull() {}
			~LangNull() {}
			const lang_type getType() const { return LANG_NULL; }
			int get() const { return 0; }
			static Objsc::LangAtomPtr create() { return Objsc::LangAtomPtr(new LangNull());}
			bool operator== (LangAtom &rhs) { return rhs.getType() == LANG_NULL;}
	};
	
	class LangInt : public LangAtom
	{
		private:
			int value;
		
		public:
			LangInt() : value(0) {}
			LangInt(int nvalue) : value(nvalue) {}
			~LangInt() {}
			const lang_type getType() const { return LANG_INT; }
			
			void set(int nvalue) { value = nvalue; }
			int get() const { return value; }
			static Objsc::LangAtomPtr create() { return Objsc::LangAtomPtr(new LangInt());}
			static Objsc::LangAtomPtr create(int nvalue) { return Objsc::LangAtomPtr(new LangInt(nvalue));}
			bool operator== (LangAtom &rhs) 
			{ 
				if (rhs.getType() == LANG_INT) {
					return dynamic_cast<LangInt&>(rhs).get() == value;
				}
				return false;
			}
	};
	
	
	class LangFloat : public LangAtom
	{
		private:
			float value;
		
		public:
			LangFloat() : value(0.0f) {}
			LangFloat(float nvalue) : value(nvalue) {}
			~LangFloat() {}
			const lang_type getType() const { return LANG_FLOAT; }
			
			void set(float nvalue) { value = nvalue; }
			float get() const { return value; }
			static Objsc::LangAtomPtr create() { return Objsc::LangAtomPtr(new LangFloat());}
			static Objsc::LangAtomPtr create(float nvalue) { return Objsc::LangAtomPtr(new LangFloat(nvalue));}
			bool operator== (LangAtom &rhs) 
			{ 
				if (rhs.getType() == LANG_FLOAT) {
					return dynamic_cast<LangFloat&>(rhs).get() == value;
				}
				return false;
			}
	};
	
	class LangString : public LangAtom
	{
		private:
			string value;
		
		public:
			LangString() : value() {}
			LangString(string nvalue) : value(nvalue) {}
			~LangString() {}
			const lang_type getType() const { return LANG_STRING; }
			
			void set(string nvalue) { value = nvalue; }
			string get() const { return value; }
			static Objsc::LangAtomPtr create() { return Objsc::LangAtomPtr(new LangString());}
			static Objsc::LangAtomPtr create(string nvalue) { return Objsc::LangAtomPtr(new LangString(nvalue));}
			
			bool operator== (LangAtom &rhs) 
			{ 
				//printf("LangString == <%s>\n", value.c_str());
				if (rhs.getType() == LANG_STRING) {
					//printf("rhs is string: <%s>\n", dynamic_cast<LangString&>(rhs).get().c_str());
					return dynamic_cast<LangString&>(rhs).get() == value;
				}
				return false;
			}
	};
	
	class LangVar : public LangAtom
	{
		private:
			int id;
			Objsc::LangAtomPtr value;
		
		public:
			LangVar(int nid=LANG_UNDEFINED) : id(nid), value(new LangNull()) {}
			LangVar(LangVar& other)
			{
				id = other.getID();
				value = other.getValue();
			}
			~LangVar() {}
			const lang_type getType() const { return LANG_VAR; }
			void setID(int nid) {id=nid;}
			const int getID() const { return id;}
			void setValue(Objsc::LangAtomPtr nvalue);
			/*
			{ 
				if(nvalue->getType() != LANG_VAR)	{
					value=nvalue; 
				}
			}*/
			Objsc::LangAtomPtr getValue() const { return value; }
			Objsc::LangAtomPtr get() const { return value; }
			const lang_type getValueType() const { return value->getType(); }
			static Objsc::LangAtomPtr create() { return Objsc::LangAtomPtr(new LangVar());}
			static Objsc::LangAtomPtr create(int nvalue) { return Objsc::LangAtomPtr(new LangVar(nvalue));}
			static Objsc::LangAtomPtr create(LangVar nvalue) { return Objsc::LangAtomPtr(new LangVar(nvalue));}
	};
	
	class LangList : public LangAtom
	{
		private:
			LangAtomVec values;
		
		public:
			LangList() : values() {}
			LangList(LangList& other) 
			{
				values.clear();
				for(Objsc::LangAtomIter atom = other.begin(); atom != other.end(); ++atom) {
					values.push_back((*atom));
				}
			}
			~LangList() {}
			const lang_type getType() const { return LANG_LIST; }
			
			static Objsc::LangAtomPtr create() { return Objsc::LangAtomPtr(new LangList());}
			
			void addBang() { values.push_back(new LangBang()); }
			void add(int nvalue) { values.push_back(new LangInt(nvalue)); }
			void add(float nvalue) { values.push_back(new LangFloat(nvalue)); }
			void add(string nvalue) { values.push_back(new LangString(nvalue)); }
			void add(Objsc::LangList nvalue) { values.push_back(new LangList(nvalue)); }
			void add(Objsc::LangVar nvalue) { values.push_back(new LangVar(nvalue)); }
			void add(Objsc::LangAtom* atom);
			void set(int index, Objsc::LangAtom* atom);
			
			void copy(LangList* other, int start=0, int end=0);
			
			Objsc::LangAtomPtr get(int index);
			Objsc::LangAtom* idx(int index) { return values[index]; }
			const int getItemType(int index) { return values[index]->getType(); }
			
			const int find(Objsc::LangAtom* atom);
			
			void setVar(int index, Objsc::LangAtomPtr value);
			const int size() const { return values.size(); }
			const bool empty() const { return values.empty(); }
			void clear() { values.clear();}
			
			Objsc::LangAtomIter begin() { return values.begin();}
			Objsc::LangAtomIter end() { return values.end();}
	};

	
	// parse atoms
	int parseInt(LangAtom* atom);
	float parseFloat(LangAtom* atom);
	bool parseBool(LangAtom* atom);
	string parseString(LangAtom* atom);
	
	int parseInt(LangAtom* atom, int dft);
	float parseFloat(LangAtom* atom, float dft);
	bool parseBool(LangAtom* atom, bool dft);
	string parseString(LangAtom* atom, const string& dft);
	
	LangAtomIter getListBegin(LangAtom* atom);
	LangAtomIter getListEnd(LangAtom* atom);
	
	// tokenize string
	void tokenizeString(const string& str, LangList& list, const string& delimiters = " ");
	string getFirstAsString(LangList* list);
	string getItemAsString(LangList* list, int index);
	int getItemAsInt(LangList* list, int index);
	float getItemAsFloat(LangList* list, int index);
	bool getItemAsBool(LangList* list, int index);
	
	// string formatting
	string intToString(int value);
	string floatToString(float value);
	string varToString(Objsc::LangVar* atom);
	string listToString(Objsc::LangList* listAtom);
	
	// var values
	string getVarString(Objsc::LangVar* atom);
	int getVarInt(Objsc::LangVar* atom);
	float getVarFloat(Objsc::LangVar* atom);
	
	// debug
	void printAtom(Objsc::LangAtom* atom);
	
	// deep copy
	Objsc::LangAtom* cloneAtom(Objsc::LangAtom* atom);
	
	// comparison
	bool equalsAtom(Objsc::LangAtom* atom, Objsc::LangAtom* other);
	bool equalsValue(Objsc::LangAtom* atom, Objsc::LangAtom* other);
	
	// lists
	Objsc::LangList* cloneList(Objsc::LangList* other, int start=0, int end=0);
}

#endif // OBJSCR_LANGATOMS_H
