#include "objscrColours.hpp"
#include "objscrConstants.hpp"
#include <math.h>

// openframeworks
#include "ofMain.h"


namespace Objsc
{
	
	static float rndDegrees = ofRandom(0,360);

	int clipColourValue(int value)	{
		if(value > 255) return 255;
		if(value < 0) return 0;
		return value;
	}

	bool isNullColour(Colour col)	{
		return col.r < 0;
	}

	bool isWhiteColour(Colour col)	{
		return (col.r == 255) && (col.g == 255) && (col.b == 255);
	}

	bool isGreyColour(Colour col)	{
		return col.r == col.g == col.b;
	}

	bool coloursMatch(Colour a, Colour b)	{
		return (a.r == b.r) && (a.g == b.g) && (a.b == b.b);
	}

	Colour makeNullColour()	
	{
		Colour nullcolour;
		nullcolour.r = -1;
		nullcolour.g = -1;
		nullcolour.b = -1;
		nullcolour.a = -1;
		return nullcolour;
	}

	Colour makeWhiteColour()	
	{
		Colour colour;
		colour.r = 255;
		colour.g = 255;
		colour.b = 255;
		colour.a = 255;
		return colour;
	}


	Colour makeBlackColour()	
	{
		Colour colour;
		colour.r = 0;
		colour.g = 0;
		colour.b = 0;
		colour.a = 255;
		return colour;
	}

	Colour makeColour(int red, int green, int blue)
	{
		Colour colour;
		colour.r = red;
		colour.g = green;
		colour.b = blue;
		colour.a = 255;
		return colour;
	}

	Colour makeColour(int red, int green, int blue, int alpha)
	{
		Colour colour;
		colour.r = red;
		colour.g = green;
		colour.b = blue;
		colour.a = alpha;
		return colour;
	}


	Colour hsi2rgb(float H, float S, float I)	
	{
		float R,G,B;
		Colour colour;

		if (H<0)	{
			H = 0;
		} else if(H>1) {
			H = 1;
		}
		if(S<0) { 
			S = 0;
		} else if(S>1) {
			S = 1;
		}

		if (I==0.0) {
			// black image
			R = G = B = 0;
		} else {
			if (S==0.0) {
				// grayscale image
				R = G = B = I;
			} else {
				double domainOffset = 0.0;
				if (H<1.0/6.0) { // red domain; green acending
					domainOffset = H;
					R = I;
					B = I * (1-S);
					G = B + (I-B)*domainOffset*6;
				} else {
					if (H<2.0/6) { // yellow domain; red acending
						domainOffset = H - 1.0/6.0;
						G = I;
						B = I * (1-S);
						R = G - (I-B)*domainOffset*6;
					} else {
						if (H<3.0/6) { // green domain; blue descending
							domainOffset = H-2.0/6;
							G = I;
							R = I * (1-S);
							B = R + (I-R)*domainOffset * 6;
						} else {
							if (H<4.0/6) { // cyan domain, green acsending
								domainOffset = H - 3.0/6;
								B = I;
								R = I * (1-S);
								G = B - (I-R) * domainOffset * 6;
							} else {
								if (H<5.0/6) { // blue domain, red ascending
									domainOffset = H - 4.0/6;
									B = I;
									G = I * (1-S);
									R = G + (I-G) * domainOffset * 6;
								} else { // magenta domain, blue descending
									domainOffset = H - 5.0/6;
									R = I;
									G = I * (1-S);
									B = R - (I-G) * domainOffset * 6;
								}
							}
						}
					}
				}
			}
		}

		colour.r = RINT(R*255);
		colour.g = RINT(G*255);
		colour.b= RINT(B*255);
		colour.a=255;

		return colour;
	}

	void rgb2hsi(int r, int g, int b, float* hue, float* saturation, float* intensity)
	{
		double H, S, I, min, mid, max;
		if (r>g && r>b) {
			max = r;
			mid = MAX(g, b);
			min = MIN(g, b);
		}else {
			if (g>b) {
				max = g;
				mid = MAX(r, b);
				min = MIN(r, b);
			}else {
				max = b;
				mid = MAX(r, g);
				min = MIN(r, g);
			}
		}

		I = max / 255;
		S = 0;
		H = 0;
		if (I==0 || max==min) {
			// this is a black image or grayscale image
			S = 0;
			H = 0;
		}else {
			S = (I - min/255) / I;
			// domains are 60 degrees of
			// red, yellow, green, cyan, blue or magenta
			double domainBase = 0.0;
			double oneSixth = 1.0/6.0;
			double domainOffset = ( (mid-min) / (max-min) ) / 6.0;

			if (r==max) {
				if (mid==g) { // green is ascending
					domainBase = 0; // red domain
				}else { // blue is descending
					domainBase = 5.0/6.0; // magenta domain
					domainOffset = oneSixth - domainOffset;
				}
			}else {
				if (g==max) {
					if (mid==b) { // blue is ascending
						domainBase = 2.0/6.0; // green domain
					}else { // red is ascending
						domainBase = 1.0/6.0; // yellow domain
						domainOffset = oneSixth - domainOffset;
					}
				}else {
					if (mid==r) { // red is ascending
						domainBase = 4.0/6.0; // blue domain
					}else { // green is descending
						domainBase = 3.0/6.0; // cyan domain
						domainOffset = oneSixth - domainOffset;
					}
				}
			}
			H = domainBase + domainOffset;
		}
		(*hue)=H;
		(*saturation)=S;
		(*intensity)=I;
	}


	Colour makeRandomColour()
	{
		rndDegrees = (int)(rndDegrees+ofRandom(30,90))%360;
		return hsi2rgb(rndDegrees/360.0f,0.5f,0.5f);
	}

}
