#include "objscrScore.hpp"

// objscrs
#include "objscrGUIRoot.hpp"
//#include "tokenize.hpp"


namespace Objsc
{
	
	
	string createMapName(int id)
	{
		char cstr[64];
		sprintf(cstr, "map_%d", id);
		return string(cstr);
	}
	
	////////////////////////////////////////
	// SCORE PATH
	////////////////////////////////////////
	Objsc::ScorePath parseScorePath(const string& pathstr)
	{	
		Objsc::ScorePath path;
		char *cstr = new char [pathstr.size()+1];
		strcpy (cstr, pathstr.c_str());
		char *p = strtok(cstr, "/");
		while (p) {
			path.push_back(string(p));
			p = strtok(NULL, " ");
		}
		return path;
	}
	
	////////////////////////////////////////
	// SCORE
	////////////////////////////////////////
	Score::Score(const string& nname) :
				name(nname),
				zonemaps(),
				zonemap(),
				patches(),
				zonenums(0),
				active(true)
	{
		createZoneMapAsCurrent();
	}
	
	void Score::setName(const string& nname) 
	{ 
		name = nname;
		Objsc::GUIRoot::Current()->setScorePath(name);
	}
	
	Objsc::LangPatchPtr Score::createPatch(const string& patchname)
	{
		patches.insert(LangPatchMapEntry(patchname, Objsc::LangPatch::create(patchname)));
		return patches[patchname];
	}
	
	void Score::update(int timeElapsed)
	{
		if(active)	{
			for(Objsc::ZoneMapMap::iterator zonemap = zonemaps.begin(); zonemap != zonemaps.end(); ++zonemap)	{
				zonemap->second->update(timeElapsed);
			}
		}
	}
	
	Objsc::ZoneMapPtr Score::createZoneMap()
	{
		return createZoneMap(createMapName(zonenums++));
	}
	
	Objsc::ZoneMapPtr Score::createZoneMapAsCurrent(const string& mapname)
	{
		if(mapname.empty())	{
			zonemap = createZoneMap();
		} else {
			zonemap = createZoneMap(mapname);
		}
		return zonemap;
	}
	
	Objsc::ZoneMapPtr Score::makeFirstMapCurrent()
	{
		zonemap = zonemaps.begin()->second;
		return zonemap;
	}
	
	Objsc::ZoneMapPtr Score::createZoneMap(string mapname)
	{
		if(zonemaps.count(mapname) == 0)	{
			zonemaps.insert(ZoneMapEntry(mapname, Objsc::ZoneMap::create(mapname)));
		}
		return zonemaps[mapname];
	}
	
	Objsc::ZoneMapPtr Score::removeZoneMap(const string& mapname)
	{
		bool replace = false;
		if(zonemaps.count(mapname) > 0)	{
			if(zonemap == zonemaps[mapname])	{
				replace = true;
			}
			zonemaps.erase(mapname);
		}
		if(zonemaps.empty())	{
			zonenums=0;
			zonemap = createZoneMap();
		} else if(replace) {
			zonemap = zonemaps.begin()->second;
		}
		return zonemap;
	}
	
	const bool Score::hasZoneMap(const string& mapname) const
	{
		return zonemaps.count(mapname) > 0;
	}
	
	Objsc::ZoneMapPtr Score::getZoneMap(const string& mapname)
	{
		if(zonemaps.count(mapname) > 0)	{
			return zonemaps[mapname];
		}
		return Objsc::ZoneMapPtr();
	}
	
	Objsc::ZoneMapPtr Score::setZoneMap(const string& mapname)
	{
		//printf("Score::setZoneMap <%s> A\n", mapname.c_str());
		if(zonemaps.count(mapname) > 0)	{
			//printf("Score::setZoneMap <%s> B\n", mapname.c_str());
			zonemap = zonemaps[mapname];
		}
		return zonemap;
	}
	
	Objsc::ZonePtr Score::getZone(const string& zonepath)
	{
		Objsc::ScorePath path = parseScorePath(zonepath);
		if (path.size() > 1)	{
			Objsc::ZoneMapPtr tmpZoneMap = getZoneMap(path[0]);
			if (tmpZoneMap)	{
				return tmpZoneMap->getZone(path[1]);
			} 
		} else {
			return zonemap->getZone(path[0]);
		}
		return Objsc::ZonePtr();
	}
	
	void Score::sendToCurrentZoneMap(const string& label, Objsc::LangAtomPtr atom)
	{
		//printf("Score::sendToCurrentZoneMap <%s> A\n", label.c_str());
		if(zonemap) {
			//printf("Score::sendToCurrentZoneMap <%s> B\n", label.c_str());
			zonemap->sendToAllZones(label, atom);
		}
	}
}
