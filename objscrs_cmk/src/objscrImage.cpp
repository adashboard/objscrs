#include "objscrImage.hpp"



namespace Objsc
{
	////////////////////////////////////////
	// IMAGE PRODUCER
	////////////////////////////////////////
	void ImageProducer::load(string imgname, const char* filepath)
	{
		images.insert(ImageMapEntry(imgname, new ofImage()));
		ofImage* img = images[imgname];
		img->setUseTexture(false);
		img->loadImage(filepath);
		img->setImageType(OF_IMAGE_GRAYSCALE);
	}

	const int ImageProducer::getWidth(string imgname)
	{
		if(images.count(imgname) > 0)	{
			return images[imgname]->width;
		} else {
			return 0;
		}
	}

	const int ImageProducer::getHeight(string imgname)
	{
		if(images.count(imgname) > 0)	{
			return images[imgname]->height;
		} else {
			return 0;
		}
	}

	const int ImageProducer::totalPixels(string imgname)
	{
		if(images.count(imgname) > 0)	{
			return images[imgname]->width*images[imgname]->height;
		} else {
			return 0;
		}
	}
				
	void ImageProducer::fillPixels(string imgname, unsigned char* pixels)
	{
		if(images.count(imgname) > 0)	{
			ofImage* srcimg = images[imgname];
			unsigned char* srcpixels = srcimg->getPixels();
			for (int i = 0; i < srcimg->width; i++){
				for (int j = 0; j < srcimg->height; j++){
					pixels[j*srcimg->width+i] = srcpixels[j*srcimg->width+i];
				}
			}
		}
	}

	void ImageProducer::fillPixels(string imgname, unsigned char* pixels, int width, int height)
	{
		if(images.count(imgname) > 0)	{
			ofImage* srcimg = images[imgname];
			unsigned char* srcpixels;
			if(width==0)
				width = srcimg->width;
			if(height==0)
				height = srcimg->height;
			if(width != srcimg->width || height != srcimg->height)	{
				ofImage imgsized;
				imgsized.setUseTexture(false);
				imgsized.clone((*srcimg));
				imgsized.setImageType(OF_IMAGE_GRAYSCALE);
				imgsized.resize(width,height);
				srcpixels = imgsized.getPixels();
			} else {
				srcpixels = srcimg->getPixels();
			}
			for (int i = 0; i < width; i++){
				srcpixels[i]=(unsigned char)0;
			}
			for (int i = 0; i < width; i++){
				for (int j = 0; j < height; j++){
					pixels[j*width+i] = srcpixels[j*width+i];
				}
			}
		}
	}


	void ImageProducer::fillPixels(string imgname, unsigned char* pixels, float widthPerc, float heightPerc)
	{
		if(images.count(imgname) > 0)	{
			ofImage* srcimg = images[imgname];
			unsigned char* srcpixels;
			int width = srcimg->width*widthPerc;
			int height = srcimg->height*heightPerc;
			if(width != srcimg->width || height != srcimg->height)	{
				ofImage imgsized;
				imgsized.setUseTexture(false);
				imgsized.clone((*srcimg));
				imgsized.setImageType(OF_IMAGE_GRAYSCALE);
				imgsized.resize(width,height);
				srcpixels = imgsized.getPixels();
			} else {
				srcpixels = srcimg->getPixels();
			}
			for (int i = 0; i < width; i++){
				srcpixels[i]=(unsigned char)0;
			}
			for (int i = 0; i < width; i++){
				for (int j = 0; j < height; j++){
					pixels[j*width+i] = srcpixels[j*width+i];
				}
			}
		}
	}

}
