#ifndef NITT_SOUNDMIXER
#define NITT_SOUNDMIXER

// project
#include "objscrSoundPerformers.hpp"
#include "objscrSound.hpp"


// shared_ptr
#include <tr1/memory>



namespace Objsc
{
	////////////////////////////////////////
	// SOUND MIXER
	////////////////////////////////////////
	class SoundMixer : public Singleton<SoundMixer>
	{
	
		private:
			Objsc::SoundPerformerMap	sounds;
			float					sampleL;
			float					sampleR;
			
		public:
			SoundMixer();
			~SoundMixer();
		
			Objsc::SoundPerformerPtr createSound(const string& label, const string& soundname);
			
			void audioRequested(float* output, int bufferSize, int nChannels);
			
			void update(int timeframe);
			
	};

}

#endif
