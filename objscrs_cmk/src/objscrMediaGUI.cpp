#include "objscrMediaGUI.hpp"

// onjscrs
#include "objscrGUIStyles.hpp"
//#include "objscrGUIRoot.hpp"

namespace Objsc
{
	
	////////////////////////////////////////
	// MEDIA PREVIEW
	////////////////////////////////////////
	MediaPreview::MediaPreview() : text(), mrect(), loaded(false) 
	{
		Objsc::StylePalette::Current()->applyStyle("mediapreview", this);
		Objsc::StylePalette::Current()->applyStyle("mediapreview", &text);
		text.clearText();
		setBounds(0,0,Objsc::MEDIAWIDTH, Objsc::MEDIAHEIGHT);
	}
	
	void MediaPreview::setLocation(float x, float y)
	{
		bounds.setLocation(x,y);
		text.setLocation(x,bounds.getBottom()-text.getHeight());
		resizeMedia();
	}
	
	void MediaPreview::setDimensions(float width, float height)
	{
		bounds.setDimensions(width,height);
		text.setWidth(width);
		resizeMedia();
	}
	void MediaPreview::setBounds(float x, float y, float width, float height)
	{
		bounds.set(x,y,width,height);
		text.setLocation(x,bounds.getBottom()-text.getHeight());
		text.setWidth(width);
		resizeMedia();
	}
	
	
	////////////////////////////////////////
	// IMAGE PREVIEW
	////////////////////////////////////////
	ImagePreview::ImagePreview() : image()
	{
		Objsc::StylePalette::Current()->applyStyle("mediapreview", this);
		Objsc::StylePalette::Current()->applyStyle("mediapreview", &text);
		text.clearText();
		setBounds(0,0,Objsc::MEDIAWIDTH, Objsc::MEDIAHEIGHT);
	}
	
	// image
	void ImagePreview::setMedia(const string& filepath, const string& filename)
	{
		image.loadImage(filepath);
		loaded = true;
		text.setText(filename);
		resizeMedia();
	}
	// geom
	void ImagePreview::resizeMedia()
	{
		if(loaded)	{
			if(image.width > bounds.getWidth() || image.height > bounds.getHeight()-text.getHeight())	{
				if(image.width > image.height)	{
					mrect.setWidth(bounds.getWidth());
					mrect.setHeight(mrect.getWidth()*((float)image.height/(float)image.width));
				} else {
					mrect.setHeight(bounds.getHeight()-text.getHeight());
					mrect.setWidth(mrect.getHeight()*((float)image.width/(float)image.height));
				}
				image.resize(mrect.getWidth(),mrect.getHeight());
			}
		} else {
			mrect.set(bounds.getX(), bounds.getY(), bounds.getWidth(), bounds.getHeight()-text.getHeight());
		}
		mrect.setCentre(bounds.getCentreX(),bounds.getCentreY()-text.getHeight());
	}
	// runtime
	void ImagePreview::draw()
	{
		if (visible)	{
			ofSetColor(bgColour.r, bgColour.g, bgColour.b, bgColour.a);
			ofRect(bounds.getX(), bounds.getY(), bounds.getWidth(), bounds.getHeight());
			if(loaded)	{
				ofSetColor(0xFFFFFF);
				image.draw(mrect.getX(), mrect.getY());
				text.draw();
			}
		}
	}
	
	////////////////////////////////////////
	// SOUND PREVIEW
	////////////////////////////////////////
	SoundPreview::SoundPreview() : snd(), playing(false)
	{
		Objsc::StylePalette::Current()->applyStyle("mediapreview", this);
		Objsc::StylePalette::Current()->applyStyle("mediapreview", &text);
		text.clearText();
		setBounds(0,0,Objsc::MEDIAWIDTH, Objsc::MEDIAHEIGHT);
		ofEnableDataPath();
		playicon.loadImage(Objsc::PLAYICON);
		stopicon.loadImage(Objsc::STOPICON);
		ofDisableDataPath();
	}
	SoundPreview::~SoundPreview()
	{
		snd.unloadSound();
	}
	// sound
	void SoundPreview::setMedia(const string& filepath, const string& filename)
	{
		//snd.loadSound(filepath, true);
		snd.loadSound(filepath);
		loaded = true;
		text.setText(filename);
		resizeMedia();
	}
	void SoundPreview::playMedia()
	{
		if(loaded)	{
			snd.play();
			playing=true;
		} else {
			playing=false;
		}
	}
	void SoundPreview::stopMedia()
	{
		if(loaded)	{
			snd.stop();
			playing=false;
		}
	}
	// geom
	void SoundPreview::resizeMedia()
	{
		if(loaded)	{
			if(playicon.width > bounds.getWidth() || playicon.height > bounds.getHeight()-text.getHeight())	{
				if(playicon.width > playicon.height)	{
					mrect.setWidth(bounds.getWidth());
					mrect.setHeight(mrect.getWidth()*((float)playicon.height/(float)playicon.width));
				} else {
					mrect.setHeight(bounds.getHeight()-text.getHeight());
					mrect.setWidth(mrect.getHeight()*((float)playicon.width/(float)playicon.height));
				}
				playicon.resize(mrect.getWidth(),mrect.getHeight());
				stopicon.resize(mrect.getWidth(),mrect.getHeight());
			}
		} else {
			mrect.set(bounds.getX(), bounds.getY(), bounds.getWidth(), bounds.getHeight()-text.getHeight());
		}
		mrect.setCentre(bounds.getCentreX(),bounds.getCentreY()-text.getHeight());
	}
	// events
	void SoundPreview::mousePressed(int x, int y, int button)
	{
		if(mrect.containsPoint(x,y))	{
			if(playing)	{
				stopMedia();
			} else {
				playMedia();
			}
		}
	}
	// runtime
	void SoundPreview::draw()
	{
		if (visible)	{
			ofSetColor(bgColour.r, bgColour.g, bgColour.b, bgColour.a);
			ofRect(bounds.getX(), bounds.getY(), bounds.getWidth(), bounds.getHeight());
			if(loaded)	{
				if(playing)	{
					stopicon.draw(mrect.getX(), mrect.getY());
				} else {
					playicon.draw(mrect.getX(), mrect.getY());
				}
				text.draw();
			} 
		}
	}
	
	
	////////////////////////////////////////
	// VIDEO PREVIEW
	////////////////////////////////////////
	VideoPreview::VideoPreview() : video(), playing(false)
	{
		Objsc::StylePalette::Current()->applyStyle("mediapreview", this);
		Objsc::StylePalette::Current()->applyStyle("mediapreview", &text);
		text.clearText();
		setBounds(0,0,Objsc::MEDIAWIDTH, Objsc::MEDIAHEIGHT);
	}
	VideoPreview::~VideoPreview()
	{
		video.closeMovie();
	}
	// sound
	void VideoPreview::setMedia(const string& filepath, const string& filename)
	{
		video.loadMovie(filepath);
		loaded = true;
		text.setText(filename);
		resizeMedia();
	}
	void VideoPreview::playMedia()
	{
		if(loaded)	{
			video.play();
			playing=true;
		} else {
			playing=false;
		}
	}
	void VideoPreview::stopMedia()
	{
		if(loaded)	{
			video.stop();
			playing=false;
		}
	}
	// geom
	void VideoPreview::resizeMedia()
	{
		if(loaded)	{
			if(video.width > bounds.getWidth() || video.height > bounds.getHeight()-text.getHeight())	{
				if(video.width > video.height)	{
					mrect.setWidth(bounds.getWidth());
					mrect.setHeight(mrect.getWidth()*((float)video.height/(float)video.width));
				} else {
					mrect.setHeight(bounds.getHeight()-text.getHeight());
					mrect.setWidth(mrect.getHeight()*((float)video.width/(float)video.height));
				}
			}
		} else {
			mrect.set(bounds.getX(), bounds.getY(), bounds.getWidth(), bounds.getHeight()-text.getHeight());
		}
		mrect.setCentre(bounds.getCentreX(),bounds.getCentreY()-text.getHeight());
	}
	// events
	void VideoPreview::mousePressed(int x, int y, int button)
	{
		if(mrect.containsPoint(x,y))	{
			if(playing)	{
				stopMedia();
			} else {
				playMedia();
			}
		}
	}
	// runtime
	void VideoPreview::draw()
	{
		if (visible)	{
			ofSetColor(bgColour.r, bgColour.g, bgColour.b, bgColour.a);
			ofRect(bounds.getX(), bounds.getY(), bounds.getWidth(), bounds.getHeight());
			if(loaded)	{
				ofSetColor(0xFFFFFF);
				video.draw(mrect.getX(), mrect.getY(), mrect.getWidth(), mrect.getHeight());
				text.draw();
			}
		}
	}

	
}