#ifndef  OBJSCR_SHAPEFILTER_H
#define OBJSCR_SHAPEFILTER_H


// objscrs
#include "objscrFrames.hpp"
#include "objscrCommands.hpp"


// openframeworks
#include "ofxOpenCv.h"

// openframeworks
#include "ofMain.h"

// shared_ptr
#include <tr1/memory>

#include <list>
#include <map>
#include <set>

typedef vector<ofxCvBlob>::iterator ofxCvBlobIterator;

namespace Objsc
{
	////////////////////////////////////////
	// SHAPE FILTER
	////////////////////////////////////////
	class ShapeFilter : public CommandReceiver
	{
		protected:
			Objsc::FrameSourcePtr 	input;
			bool				active;
			ofxCvColorImage		inputImg;
			ofxCvColorImage		colorImg;
			ofxCvGrayscaleImage 	grayImage;
			ofxCvGrayscaleImage 	grayBg;
			ofxCvGrayscaleImage 	grayDiff;

			std::map<int, ofxCvGrayscaleImage> background;
			std::map<int, ofxCvGrayscaleImage> image;
			std::map<int, ofxCvGrayscaleImage> diff;

			ofxCvContourFinder 		contourFinder;
			ofImage 				maskimg;
			unsigned char*		maskpix;
			unsigned char*  		diffpix;
			bool				domask;
			int 					threshold;
			int					inWidth;
			int					inHeight;
			int					pixTotal;
			bool				continuous;
			bool				hasRef;
//			bool				resize;
			int					minRadius;
			int					maxRadius;
			int					blur;
			std::set<ofxCvColorImage::ColorChannel>	colorChannels;

			std::list<ofxCvGrayscaleImage*> diffList;

		
			void process();
			void filterImage();
		
		public:
			ShapeFilter();
			virtual ~ShapeFilter() {}
				
			void setInput(Objsc::FrameSourcePtr inputPtr);
			void setThreshold(int value) { threshold = value>255?255:(value<0?0:value);}
			
			void start();
            void stop() ;
			void update(){if(active) {process();}}
			void draw();
			
			ofxCvBlobIterator shapesBegin() { return contourFinder.blobs.begin();}
			ofxCvBlobIterator shapesEnd() { return contourFinder.blobs.end();}
			const bool hasShapes() const {return !contourFinder.blobs.empty();}
			const ofxCvBlob& getShape(int idx);
			const int totalShapes() const { return contourFinder.nBlobs; }
			
			void takeRef();
			void startRef();
			void stopRef();
			
			void setMinRadius(int value) { minRadius = value<0?0:value; }
			void setMaxRadius(int value) { maxRadius = value<0?0:value; }
			void setShapesBlur(int value) { blur = value<0?0:value; }

			void setColorChannel(const ofxCvColorImage::ColorChannel& cc, bool add=true);
			bool hasColorChannel(const ofxCvColorImage::ColorChannel& cc){return colorChannels.find(cc) != colorChannels.end();}
			
			void setMaskImage(const string& filename);
			void applyMask();
			void useMask();
			void clearMask();
			const bool hasMask() const { return domask; }
			
			// commands
			void doCommand(Objsc::CommandPtr& cmd);
			void undoCommand(Objsc::CommandPtr& cmd);
			
			static std::tr1::shared_ptr<Objsc::ShapeFilter> create()
			{
				return std::tr1::shared_ptr<Objsc::ShapeFilter>(new ShapeFilter());
			}
			static const string	cmdlabel;
	};
	
	typedef std::tr1::shared_ptr<Objsc::ShapeFilter> ShapeFilterPtr;
}

#endif // OBJSCR_SHAPEFILTER_H
