#include "objscrArchiveLang.hpp"
#include "Lang/MessageObject.hpp"


// objscrs
//#include "objscrLangExtensions.hpp"
#include "Lang/LangMediaObject.hpp"

// STL
#include <algorithm>

namespace Objsc
{
	/////////////////////////////////////
	// FUNCTIONS
	/////////////////////////////////////
	void initLangArchivers()
	{
		Objsc::LangObjectArchiverFactory* factory = Objsc::LangObjectArchiverFactory::Current();
		factory->addCreator("default", new Objsc::DefaultLangObjectArchiverCreator());
		factory->addCreator(Objsc::MessageObject::langname, new Objsc::LangMessageArchiverCreator());
		//~ factory->addCreator(Objsc::SoundObject::langname, new Objsc::LangMediaObjectArchiverCreator());
		//~ factory->addCreator(Objsc::VideoObject::langname, new Objsc::LangMediaObjectArchiverCreator());
		//factory->addCreator(Objsc::ImageObject::langname, new Objsc::LangMediaObjectArchiverCreator());
	}
	
	
	////////////////////////////////////////
	// DEFAULT LANG OBJECT ARCHIVER
	////////////////////////////////////////
	void DefaultLangObjectArchiver::encode(TiXmlElement *parentNode, Objsc::LangObjectPtr object, int encID)
	{
		TiXmlElement *langObjNode = new TiXmlElement("object");
		langObjNode->SetAttribute("langname", object->getLangName());
		langObjNode->SetDoubleAttribute("x", object->getX());
		langObjNode->SetDoubleAttribute("y", object->getY());
		if(encID>-1)	{
			langObjNode->SetAttribute("encid", encID);
		}
		
		if(object->getLabel() != Objsc::NULL_STRING)	{
			TiXmlElement *labelEle = new TiXmlElement("label");
			labelEle->LinkEndChild(new TiXmlText(object->getLabel()));
			langObjNode->LinkEndChild(labelEle);
		}
		
		if(object->hasArgs())	{
			TiXmlElement *argsEle = new TiXmlElement("args");
			argsEle->LinkEndChild(new TiXmlText(object->getArgString()));
			langObjNode->LinkEndChild(argsEle);
		}
		
		parentNode->LinkEndChild(langObjNode);
	}
	
	void DefaultLangObjectArchiver::decode(TiXmlElement *parentNode, Objsc::LangObjectPtr object)
	{
		TiXmlElement *labelEle = parentNode->FirstChildElement("label");
		if(labelEle) {
			object->setLabel(string(labelEle->GetText()));
		}
		
		TiXmlElement *argsEle = parentNode->FirstChildElement("args");
		if(argsEle) {
			Objsc::LangList argList;
			Objsc::tokenizeString(string(argsEle->GetText()), argList);
			object->parseArgs(&argList);
		}
	}
	
	////////////////////////////////////////
	// LANG MESSAGE ARCHIVER
	////////////////////////////////////////
	void LangMessageArchiver::encode(TiXmlElement *parentNode, Objsc::LangObjectPtr object, int encID)
	{
		TiXmlElement *langObjNode = new TiXmlElement("object");
		langObjNode->SetAttribute("langname", object->getLangName());
		langObjNode->SetDoubleAttribute("x", object->getX());
		langObjNode->SetDoubleAttribute("y", object->getY());
		if(encID>-1)	{
			langObjNode->SetAttribute("encid", encID);
		}
		
		if(object->getLabel() != Objsc::NULL_STRING)	{
			TiXmlElement *labelEle = new TiXmlElement("label");
			labelEle->LinkEndChild(new TiXmlText(object->getLabel()));
			langObjNode->LinkEndChild(labelEle);
		}
		
		if(!object->getDisplayString().empty())	{
			TiXmlElement *messageEle = new TiXmlElement("message");
			messageEle->LinkEndChild(new TiXmlText(object->getDisplayString()));
			langObjNode->LinkEndChild(messageEle);
		}
		
		parentNode->LinkEndChild(langObjNode);
	}
	
	void LangMessageArchiver::decode(TiXmlElement *parentNode, Objsc::LangObjectPtr object)
	{
		TiXmlElement *labelEle = parentNode->FirstChildElement("label");
		if(labelEle) {
			object->setLabel(string(labelEle->GetText()));
		}
		
		TiXmlElement *messageEle = parentNode->FirstChildElement("message");
		if(messageEle) {
			object->setDisplayString(string(messageEle->GetText()));
		}
	}
	
	////////////////////////////////////////
	// LANG MEDIA OBJECT ARCHIVER
	////////////////////////////////////////
	void LangMediaObjectArchiver::encode(TiXmlElement *parentNode, Objsc::LangObjectPtr object, int encID)
	{
		Objsc::LangMediaObjectPtr mediObj = std::tr1::static_pointer_cast<Objsc::LangMediaObject>(object);
		
		TiXmlElement *langObjNode = new TiXmlElement("object");
		langObjNode->SetAttribute("langname", mediObj->getLangName());
		langObjNode->SetDoubleAttribute("x", mediObj->getX());
		langObjNode->SetDoubleAttribute("y", mediObj->getY());
		if(encID>-1)	{
			langObjNode->SetAttribute("encid", encID);
		}
		
		if(mediObj->getLabel() != Objsc::NULL_STRING)	{
			TiXmlElement *labelEle = new TiXmlElement("label");
			labelEle->LinkEndChild(new TiXmlText(mediObj->getLabel()));
			langObjNode->LinkEndChild(labelEle);
		}
		
		//~ if(!mediObj->getFilename().empty())	{
			//~ TiXmlElement *mediaEle = new TiXmlElement("media");
			//~ string filename(mediObj->getFilename());
			//~ //std::replace(filename.begin(), filename.end(), " ", "%20");
			//~ replace(filename.begin(), filename.end(), ' ', '_'); // should do html escape
			//~ mediaEle->SetAttribute("filename", filename);
			//~ mediaEle->SetAttribute("layer", mediObj->getMediaLayer());
			//~ langObjNode->LinkEndChild(mediaEle);
		//~ }
		
		parentNode->LinkEndChild(langObjNode);
	}
	
	void LangMediaObjectArchiver::decode(TiXmlElement *parentNode, Objsc::LangObjectPtr object)
	{
		
	}
	
	
}

