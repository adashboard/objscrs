#include "objscrBodyViews.hpp"

// openframeworks
#include "ofMain.h"

// objscrs
#include "objscrGUIStyles.hpp"
#include "objscrStage.hpp"
#include "objscrGUIRoot.hpp"
#include "objscrSettings.hpp"

// STL
#include <algorithm>
#include <iostream>

#include <stdio.h>


namespace Objsc
{
	
	////////////////////////////////////////
	// DECLARATIONS
	////////////////////////////////////////
	const string Objsc::BodyView::stylename = "body";
	
	
	////////////////////////////////////////
	// ZONE VIEW
	////////////////////////////////////////
	BodyView::BodyView() : 
		body(), 
		centroid(),
		scrX(0), 
		scrY(0), 
		labelfield(),
		stagenumfield(),
		editmenu(),
		drawBox(true),
		drawLoc(true),
		drawLabel(true),
		drawContour(false)
	{
		Objsc::StylePalette::Current()->applyStyle(Objsc::BodyView::stylename, &labelfield);
		labelfield.clearText();
		Objsc::StylePalette::Current()->applyStyle(Objsc::BodyView::stylename, &stagenumfield);
		stagenumfield.clearText();
		Objsc::StylePalette::Current()->applyStyle(Objsc::BodyView::stylename, this);
	}
	
	void BodyView::setBody(Objsc::BodyPtr bodyPtr) 
	{
		body=bodyPtr;
		//printf("BodyView::setBody %u\n", body->getID());
		//char cstr[256];
		//sprintf(cstr, "body %u", body->getID());
		//sprintf(cstr, "body %u", body->getStageNum());
		//labelfield.setText(string(cstr));
		stagenumfield.setText(ofToString((int)(body->getStageNum())));
		//labelfield.setText(body->getName());
		//mapToScreen();
	}
	
	//~ void BodyView::mapToScreen()
	//~ {
		//~ if(body)	{
			//~ int x=0;
			//~ int y=0;
			//~ int w=0;
			//~ int h=0;
			//~ const Objsc::Rect zrect = body->getBounds();
			//~ Objsc::Stage::Current()->stageToScreen(zrect.getX(), zrect.getY(), &x,&y);
			//~ Objsc::Stage::Current()->stageToScreen(zrect.getWidth(), zrect.getHeight(),&w,&h);
			//~ bounds.set(x,y,w,h);
			//~ labelfield.setLocation(bounds.getX(), bounds.getY());
		//~ }
	//~ }
	
	
	//~ void BodyView::mapToStage()
	//~ {
		//~ if(body)	{
			//~ float x=0.0f;
			//~ float y=0.0f;
			//~ float w=0.0f;
			//~ float h=0.0f;
			//~ Objsc::Stage::Current()->screenToStage(bounds.getX(), bounds.getY(), &x,&y);
			//~ Objsc::Stage::Current()->screenToStage(bounds.getWidth(), bounds.getHeight(),&w,&h);
			//~ body->setBounds(x,y,w,h);
		//~ }
	//~ }
	
	
	void BodyView::setLocation(float x, float y)
	{
		bounds.setLocation(x,y);
		boundsResized();
	}
	
	void BodyView::setDimensions(float width, float height)
	{
		
	}
	
	void BodyView::setBounds(float x, float y, float width, float height)
	{
		
	}
	
	void BodyView::boundsResized()
	{
		//mapToStage();
		stagenumfield.setLocation(bounds.getX(), bounds.getY());
		labelfield.setLocation(bounds.getX(), bounds.getCentreY());		
	}
	
	void BodyView::update()
	{
		const Objsc::Rect& sr  = body->getStageRect();
		float HScale(float(ofGetWidth()) / Objsc::STAGE_WIDTH);
		float VScale(float(ofGetHeight()) / Objsc::STAGE_HEIGHT);
//		bounds.set(stageToScreen(sr.getX(), ofGetWidth()),
//					stageToScreen(sr.getY(), ofGetHeight()),
//					stageToScreen(sr.getWidth(), ofGetWidth()),
//					stageToScreen(sr.getHeight(), ofGetHeight()));

		bounds = sr.scaled(HScale, VScale);
		centroid.x = body->getStageX() * HScale;
		centroid.y = body->getStageY() * VScale;

//		centroid.x = stageToScreen(body->getStageX(), ofGetWidth());
//		centroid.y = stageToScreen(body->getStageY(), ofGetHeight());
		
		stagenumfield.setLocation(bounds.getX(), bounds.getY());
		updateLabel();
		labelfield.setLocation(bounds.getX(), bounds.getCentreY());

		switch(body->getState())	{
			case Body::UNBORN:
				stagenumfield.setTextColour(0,255,0);
				break;
			case Body::BECOMING:
				stagenumfield.setTextColour(0,255,0);
				break;
			case Body::PRESENT:
				stagenumfield.setTextColour(255,120,0);
				break;
			case Body::ABSENT:
				stagenumfield.setTextColour(0,120,255); 
				break;
			case Body::DEAD:
				stagenumfield.setTextColour(120,120,120); 
				break;
		}
	}
	
	void BodyView::draw()
	{
		if (visible)
		{
			if(drawBox)
			{
				ofFill();
				if(hasFocus)
				{
					ofSetColor(bgColour.r, bgColour.g, bgColour.b, 30);
					ofRect(bounds.getX(), bounds.getY(), bounds.getWidth(), bounds.getHeight());
					ofSetColor(bgColour.r, bgColour.g, bgColour.b);
				}
				ofSetColor(bgColour.r, bgColour.g, bgColour.b, 120);
				ofRect(bounds.getX(), bounds.getY(), bounds.getWidth(), bounds.getHeight());
				ofSetColor(bgColour.r, bgColour.g, bgColour.b, 255);
				ofNoFill();
			}
			
			if(drawLoc)
			{
				ofLine(centroid.x, bounds.getY(), centroid.x, bounds.getBottom());
				ofLine(bounds.getX(), centroid.y, bounds.getRight(), centroid.y);
				ofEnableSmoothing();
				ofCircle(centroid.x, centroid.y, 10.0f);
				ofDisableSmoothing();
			}
			
			if(drawLabel)
            {
				ofFill();
				stagenumfield.draw();
				if (labelfield.hasText())
					labelfield.draw();
				//ofNoFill();
            }

            if(drawContour)
			{
				ofSetColor(bgColour.r, bgColour.g, bgColour.b, 255);
				ofFill();
				int vSize(body->getBodyPoints().size());
				float HScale(float(ofGetWidth())
					     / float(Objsc::Settings::Current()->getCameraWidth()));
				float VScale(float(ofGetHeight())
					     / float(Objsc::Settings::Current()->getCameraHeight()));

				for(int p(0); p < vSize ; ++p)
				{
					ofPoint cP(body->getBodyPoints().at(p));
					//                            std::cout << "Point[" << p <<"] "<< cP.x << " ; " << cP.y << std::endl;
					ofCircle(float(cP.x) * HScale, float(cP.y) * VScale, 1.0 * HScale);
				}
				ofPoint curP;
				bool firstP(true);
				for(int p(0); p < vSize ; ++p)
				{
					ofPoint cP(body->getBodyPoints().at(p));
					if(firstP)
						firstP = false;
					else
						ofLine(float(curP.x) * HScale, float(curP.y) * VScale,
						       float(cP.x) * HScale, float(cP.y) * VScale);
					curP = cP;
				}
			}

			// SY: marginalia lab disabled for CCA lab work
			/*
			if(Settings::Current()->hasSetting("track_view_marginalia3")
					&& (Settings::Current()->getSetting("track_view_marginalia3") != std::string("none")))
			{
				if(storePoints.size() < 2)
				{
					storePoints.push_back(body->getBodyPoints());
					return;
				}

				vector<ofPoint> v0(storePoints.front());
				vector<ofPoint> v1(storePoints.back());
				storePoints.push_back(body->getBodyPoints());
				storePoints.pop_front();

				ofSetColor(bgColour.r, bgColour.g, bgColour.b, 255);
				ofFill();
				int vSize(v0.size());
				float HScale(float(ofGetWidth())
					     / float(Objsc::Settings::Current()->getCameraWidth()));
				float VScale(float(ofGetHeight())
					     / float(Objsc::Settings::Current()->getCameraHeight()));
				//			std::cout<<"HScale: " << HScale << " | VScale: " << VScale << std::endl;
//				for(int p(0); p < vSize ; ++p)
//				{
//					ofPoint cP(body->getBodyPoints().at(p));
//					//                            std::cout << "Point[" << p <<"] "<< cP.x << " ; " << cP.y << std::endl;
//					ofCircle(float(cP.x) * HScale, float(cP.y) * VScale, 1.0 * HScale);
//				}
				ofPoint curP;
				ofSetColor( 0,0 ,255 , 255);
				ofFill();
				bool firstP(true);
				for(int p(0); p < vSize ; ++p)
				{
					ofPoint cP(v0.at(p));
					if(firstP)
						firstP = false;
					else
						ofLine(float(curP.x) * HScale, float(curP.y) * VScale,
						       float(cP.x) * HScale, float(cP.y) * VScale);
					curP = cP;
				}

				curP = ofPoint() ;
				ofSetColor( 255,0 ,0 , 255);
				ofFill();
				firstP = true;
				vSize = v1.size();
				for(int p(0); p < vSize ; ++p)
				{
					ofPoint cP(v1.at(p));
					if(firstP)
						firstP = false;
					else
						ofLine(float(curP.x ) * HScale, float(curP.y) * VScale,
						       float(cP.x ) * HScale, float(cP.y) * VScale);
					curP = cP;
				}


			}
			*/
			
			/*
			// SY: added new setting to replace hardcoded file path
			if(Settings::Current()->hasSetting("track_view_marginalia2")
					&& (Settings::Current()->getSetting("track_view_marginalia2") != std::string("none"))
					&& Settings::Current()->hasSetting("track_view_marginalia2_text")
					&& (Settings::Current()->getSetting("track_view_marginalia2") != std::string("none"))
			)
			{
				// OLD: FILE * pFile(fopen("/home/pierre/marginalia2", "r"));
				FILE * pFile(fopen(Settings::Current()->getSetting("track_view_marginalia2_text").c_str(), "r"));
				std::string theText;
				if (pFile!=NULL)
				{
					long lSize;
					char * buffer;
					size_t result;
					fseek (pFile , 0 , SEEK_END);
					lSize = ftell (pFile);
					rewind (pFile);
					// allocate memory to contain the whole file:
					buffer = (char*) malloc (sizeof(char)*lSize);
					if (buffer != NULL)
					{

					// copy the file into the buffer:
					result = fread (buffer,1,lSize,pFile);
					if (result == lSize)
					{
						theText = std::string(buffer, lSize);
					}
					free (buffer);
					}
					// terminate
					fclose (pFile);

				}
				if(theText.size() > 0)
				{
					ofTrueTypeFont * tFont(labelfield.getFont());
					tFont->drawString(theText, centroid.x,centroid.y);
				}


			}
			*/
			
			/*
			if(Settings::Current()->hasSetting("track_view_marginalia0")
					&& (Settings::Current()->getSetting("track_view_marginalia0") != std::string("none")))
			{
				ofSetColor(bgColour.r, bgColour.g, bgColour.b, 255);
				ofFill();
				int vSize(body->getBodyPoints().size());
				float HScale(float(ofGetWidth())
					     / float(Objsc::Settings::Current()->getCameraWidth()));
				float VScale(float(ofGetHeight())
					     / float(Objsc::Settings::Current()->getCameraHeight()));
				//			std::cout<<"HScale: " << HScale << " | VScale: " << VScale << std::endl;
				for(int p(0); p < vSize ; ++p)
				{
					ofPoint cP(body->getBodyPoints().at(p));
					//                            std::cout << "Point[" << p <<"] "<< cP.x << " ; " << cP.y << std::endl;
					ofCircle(float(cP.x) * HScale, float(cP.y) * VScale, 1.0 * HScale);
				}
				ofPoint curP;
				bool firstP(true);
				for(int p(0); p < vSize ; ++p)
				{
					ofPoint cP(body->getBodyPoints().at(p));
					if(firstP)
						firstP = false;
					else
						ofLine(float(curP.x) * HScale, float(curP.y) * VScale,
						       float(cP.x) * HScale, float(cP.y) * VScale);
					curP = cP;
				}

				/// just here
				int vel(body->getVelocity() / 10);
				//std::cerr << std::fixed << "Velocity: "<<body->getVelocity()<<std::endl;
				float centerX(body->getShapeRect().getCentreX());
				float centerY(body->getShapeRect().getCentreY());
				for(int i(2); i<= vel; i+=2 )
				{
					firstP = true;
					for(int p(0); p < vSize ; ++p)
					{
						ofPoint cP(body->getBodyPoints().at(p));

						if(cP.x > centerX && cP.y < centerY)
						{
							if(firstP)
								firstP = false;
							else
								ofLine(float(curP.x + i) * HScale, float(curP.y + i) * VScale,
								       float(cP.x + i ) * HScale, float(cP.y + i) * VScale);
						}
						if(cP.x > centerX && cP.y > centerY)
						{
							if(firstP)
								firstP = false;
							else
								ofLine(float(curP.x + i) * HScale, float(curP.y - i) * VScale,
								       float(cP.x + i ) * HScale, float(cP.y - i) * VScale);
						}
						if(cP.x < centerX && cP.y > centerY)
						{
							if(firstP)
								firstP = false;
							else
								ofLine(float(curP.x - i) * HScale, float(curP.y - i) * VScale,
								       float(cP.x - i ) * HScale, float(cP.y - i) * VScale);
						}
						if(cP.x < centerX && cP.y < centerY)
						{
							if(firstP)
								firstP = false;
							else
								ofLine(float(curP.x + i) * HScale, float(curP.y + i) * VScale,
								       float(cP.x + i ) * HScale, float(cP.y + i) * VScale);
						}
						curP = cP;
					}
				}
				
			}*/
		}
	}
	
	// label
	void BodyView::updateLabel()
	{
		if(body->getLabel() != labelfield.getText())	
			labelfield.setText(body->getLabel());
	}
	
	void BodyView::setLabel(const string &labelStr)
	{
		labelfield.setText(labelStr);
	}
	
	void BodyView::clearLabel()
	{
		labelfield.clearText();
	}
	
	
	void BodyView::keyPressed(int key)
	{
		
	}
	
	void BodyView::keyReleased(int key)
	{
		
	}
	
	void BodyView::mouseMoved(int x, int y )
	{
		if(withinBounds(x,y))	{
			if(!hasFocus) {
				gainFocus();
			}
		} else {
			if(hasFocus) {
				looseFocus();
			}
		}
	}
	
	void BodyView::mouseDragged(int x, int y, int button)
	{
		consumed = false;
		float dX = x-scrX;
		float dY = y-scrY;
		
		scrX = x;
		scrY = y;
	}
	
	void BodyView::mousePressed(int x, int y, int button)
	{
		consumed = false;
		if(withinBounds(x,y))	{
			scrX = x;
			scrY = y;
			switch(button)	{
				case 0:
					consumed = true;
					break;
				case 1:
					break;
				case 2:
					consumed = true;
					openMenu(x, y);
					break;
			}
		}
	}
	
	void BodyView::mouseReleased(int x, int y, int button)
	{
		
	}
	
	void BodyView::windowResized(int w, int h)
	{
		
	}
	
	void BodyView::openMenu(int x, int y)
	{
		//~ if(!editmenu)	{
			//~ editmenu = Objsc::ContextMenu::create();
			//~ Objsc::CommandPtr actionPatch = Objsc::CommandDispatcher::createCommand(Objsc::BodyMapEditor::cmdlabel , "body_patch");
			//~ actionPatch->getData()->addArg(Objsc::BodyViewPtr(this));
			//~ editmenu->addItem("patch", actionPatch);
			//~ Objsc::CommandPtr actionRename = Objsc::CommandDispatcher::createCommand(Objsc::BodyMapEditor::cmdlabel , "body_rename");
			//~ actionRename->getData()->addArg(Objsc::BodyViewPtr(this));
			//~ editmenu->addItem("rename", actionRename);
			//~ Objsc::CommandPtr actionDelete = Objsc::CommandDispatcher::createCommand(Objsc::BodyMapEditor::cmdlabel , "body_delete");
			//~ actionDelete->getData()->addArg(Objsc::BodyViewPtr(this));
			//~ editmenu->addItem("delete", actionDelete);
		//~ }
		//~ Objsc::GUIRoot::Current()->openRootMenu(editmenu, x, y);
	}
	
}

