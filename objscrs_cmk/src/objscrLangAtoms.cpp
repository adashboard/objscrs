#include "objscrLangAtoms.hpp"


#include <ctype.h>
#include <stdlib.h>
#include <cstdio>
#include <string.h>

namespace Objsc
{
	// parse atoms
	int parseInt(LangAtom* atom) { return dynamic_cast<LangInt*>(atom)->get(); }
	float parseFloat(LangAtom* atom) { return dynamic_cast<LangFloat*>(atom)->get(); }
	bool parseBool(LangAtom* atom)  { return (dynamic_cast<LangInt*>(atom)->get() != 0);}
	string parseString(LangAtom* atom) { return dynamic_cast<LangString*>(atom)->get(); }
	LangAtomIter getListBegin(LangAtom* atom) { return dynamic_cast<LangList*>(atom)->begin();}
	LangAtomIter getListEnd(LangAtom* atom) { return dynamic_cast<LangList*>(atom)->end();}
	
	int parseInt(LangAtom* atom, int dft)
	{
		if(atom->getType() == LANG_INT)	{
			return dynamic_cast<LangInt*>(atom)->get();
		} else if(atom->getType() == LANG_FLOAT)	{
			return dynamic_cast<LangFloat*>(atom)->get();
		} else {
			return dft;
		}
	}
	
	float parseFloat(LangAtom* atom, float dft)
	{
		if(atom->getType() == LANG_FLOAT)	{
			return dynamic_cast<LangFloat*>(atom)->get();
		} else if(atom->getType() == LANG_INT)	{
			return dynamic_cast<LangInt*>(atom)->get();
		} else {
			return dft;
		}
	}
	
	
	bool parseBool(LangAtom* atom, bool dft)
	{
		if(atom->getType() == LANG_INT)	{
			return (dynamic_cast<LangInt*>(atom)->get() != 0);
		} else if(atom->getType() == LANG_FLOAT)	{
			return (dynamic_cast<LangFloat*>(atom)->get() != 0);
		} else if(atom->getType() == LANG_STRING)	{
			return (dynamic_cast<LangString*>(atom)->get() == "on");
		} else {
			return dft;
		}
	}
	
	string parseString(LangAtom* atom, const string& dft)
	{
		if(atom->getType() == LANG_STRING)	{
			return dynamic_cast<LangString*>(atom)->get(); 
		} else {
			return dft;
		}
	}
	
	
	// string formatting
	string intToString(int value)
	{
		char cstr[64];
		sprintf(cstr, "%d", value);
		return string(cstr);
	}
	
	string floatToString(float value)
	{
		char cstr[64];
		sprintf(cstr, "%.3f", value);
		return string(cstr);
	}
	
	string varToString(Objsc::LangVar* atom)
	{
		
		switch(atom->getValueType())	{
			case LANG_NULL:
				char cstr[64];
				//sprintf(cstr, "<$%d>", atom->getID());
				sprintf(cstr, "$%d", atom->getID());
				return string(cstr);
				break;
			case LANG_BANG:
				return LANG_BANGSTRING;
				break;
			case LANG_INT:
				return intToString(parseInt(atom->getValue().get()));
				break;
			case LANG_FLOAT:
				return floatToString(parseFloat(atom->getValue().get()));
				break;
			case LANG_STRING:
				return parseString(atom->getValue().get());
				break;
			case LANG_VAR:
				return varToString(dynamic_cast<Objsc::LangVar*>(atom->getValue().get()));
				break;
			case LANG_LIST:
				return listToString(dynamic_cast<Objsc::LangList*>(atom->getValue().get()));
				break;
		}
	}
	
	string getVarString(Objsc::LangVar* atom)
	{
		if(atom->getValueType() == LANG_STRING)	{
			return parseString(atom->getValue().get());
		} else {
			return "";
		}
	}
	
	int getVarInt(Objsc::LangVar* atom)
	{
		if(atom->getValueType() == LANG_INT)	{
			return parseInt(atom->getValue().get());
		} else {
			return 0;
		}
	}
	
	float getVarFloat(Objsc::LangVar* atom)
	{
		if(atom->getValueType() == LANG_FLOAT)	{
			return parseFloat(atom->getValue().get());
		} else {
			return 0.0f;
		}
	}
	
	string listToString(Objsc::LangList* listAtom)
	{
		string buffer;
		for(LangAtomIter atom = listAtom->begin(); atom != listAtom->end(); ++atom) {
			switch((*atom)->getType())	{
				case LANG_NULL:
					break;
				case LANG_BANG:
					buffer.append("b");
					break;
				case LANG_INT:
					buffer.append(intToString(parseInt((*atom))));
					break;
				case LANG_FLOAT:
					buffer.append(floatToString(parseFloat((*atom))));
					break;
				case LANG_STRING:
					buffer.append(parseString((*atom)));
					break;
				case LANG_LIST:
					buffer.append(listToString(dynamic_cast<Objsc::LangList*>((*atom))));
					break;
				case LANG_VAR:
					buffer.append(varToString(dynamic_cast<Objsc::LangVar*>((*atom))));
					break;
			}
			buffer.append(" ");
		}
		return buffer;
	}
	
	// tokenize string
	bool strIsInt(const char* cstr) 
	{ 
		if(isdigit(cstr[0])==0 && cstr[0] != '-') 
			return false;
		if(strlen(cstr)==1 && cstr[0] == '-')
			return false;
		bool valid = true;
		int i = 1;
		while(valid && cstr[i]!='\0')	{
			valid = (isdigit(cstr[i]) !=0 && cstr[i]!='.');
			++i;
		}
		return valid;
	}
	bool strIsFloat(const char* cstr) 
	{ 
		if(isdigit(cstr[0])==0 && cstr[0] != '-' && cstr[0] !='.') 
			return false;
		if(strlen(cstr)==1 && (cstr[0] == '-' || cstr[0] =='.'))
			return false;
		bool valid = true;
		bool haspoint = (cstr[0] =='.');
		int i = 1;
		while(valid && cstr[i]!='\0')	{
			if(cstr[i] == '.')	{
				if(haspoint)
					return false;
				else
					haspoint = true;
			} else {
				valid = (isdigit(cstr[i])!=0);
			}
			++i;
		}
		return (valid && haspoint);
	}
	
	bool strIsVar(string& str) { return str.size() > 1 && str.find("$") == 0 && strIsInt(str.substr(1).c_str());}
	
	bool strIsBang(string& str) { return str == "b" || str == "bang"; }
	
	bool strIsNull(string& str) { return str == "a" || str == "null"; }
	
	void tokenizeString(const string& str, LangList& list, const string& delimiters)
	{
		list.clear();
		string::size_type lastPos = str.find_first_not_of(delimiters, 0);
		string::size_type pos = str.find_first_of(delimiters, lastPos);
		while (string::npos != pos || string::npos != lastPos) {
			string tokstr = str.substr(lastPos, pos - lastPos);
			if(strIsVar(tokstr))	{
				Objsc::LangVar var(atoi(tokstr.substr(1).c_str()));
				list.add(var);
			} else if (strIsInt(tokstr.c_str())) {
				list.add(atoi(tokstr.c_str()));
			} else if (strIsFloat(tokstr.c_str())) {
				list.add((float)atof(tokstr.c_str()));
			} else if (strIsBang(tokstr)) {
				list.addBang();
			} else { // plain string
				list.add(tokstr);
			}
			lastPos = str.find_first_not_of(delimiters, pos);
			pos = str.find_first_of(delimiters, lastPos);
		}
	}
	
	string getFirstAsString(LangList* list)
	{
		LangAtomIter atom = list->begin();
		if((*atom)->getType() == LANG_STRING)	{
			return parseString((*atom));
		} else if((*atom)->getType() == LANG_VAR)	{
			return getVarString(dynamic_cast<Objsc::LangVar*>((*atom)));
		} else if((*atom)->getType() == LANG_INT)	{
			return intToString(parseInt((*atom)));
		} else if((*atom)->getType() == LANG_FLOAT)	{
			return floatToString(parseFloat((*atom)));
		} else {
			return "";
		}
	}
	
	string getItemAsString(LangList* list, int index)
	{
		if(index > -1 && index < list->size())	{
			Objsc::LangAtomPtr atom = list->get(index);
			if(atom->getType() == LANG_STRING)	{
				return parseString(atom.get());
			} else if(atom->getType() == LANG_VAR)	{
				return getVarString(dynamic_cast<Objsc::LangVar*>(atom.get()));
			} else {
				return "";
			}
		} else {
			return "";
		}
	}
	
	
	int getItemAsInt(LangList* list, int index)
	{
		if(index > -1 && index < list->size())	{
			Objsc::LangAtomPtr atom = list->get(index);
			if(atom->getType() == LANG_INT)	{
				return parseInt(atom.get());
			} else if(atom->getType() == LANG_FLOAT)	{
				return (int)parseFloat(atom.get());
			} else if(atom->getType() == LANG_VAR)	{
				return getVarInt(dynamic_cast<Objsc::LangVar*>(atom.get()));
			} else {
				return 0;
			}
		} else {
			return 0;
		}
	}
	
	float getItemAsFloat(LangList* list, int index)
	{
		if(index > -1 && index < list->size())	{
			Objsc::LangAtomPtr atom = list->get(index);
			if(atom->getType() == LANG_FLOAT)	{
				return parseFloat(atom.get());
			} else if(atom->getType() == LANG_INT)	{
				return (float)parseInt(atom.get());
			} else if(atom->getType() == LANG_VAR)	{
				return getVarFloat(dynamic_cast<Objsc::LangVar*>(atom.get()));
			} else {
				return 0.0f;
			}
		} else {
			return 0.0f;
		}
	}
	
	bool getItemAsBool(LangList* list, int index)
	{
		if(index > -1 && index < list->size())	{
			Objsc::LangAtomPtr atom = list->get(index);
			if(atom->getType() == LANG_INT)	{
				return parseInt(atom.get()) != 0;
			} else if(atom->getType() == LANG_VAR)	{
				return getVarInt(dynamic_cast<Objsc::LangVar*>(atom.get())) != 0;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
	
	
	// debug
	void printAtom(Objsc::LangAtom* atom)
	{
		switch(atom->getType())	{
			case LANG_NULL:
				//printf("atom: NULL\n");
				break;
			case LANG_BANG:
				//printf("atom: BANG\n");
				break;
			case LANG_INT:
				//printf("atom: INT %d\n", parseInt(atom));
				break;
			case LANG_FLOAT:
				//printf("atom: FLOAT %f\n", parseFloat(atom));
				break;
			case LANG_STRING:
				//printf("atom: STRING <%s>\n", parseString(atom).c_str());
				break;
			case LANG_LIST:
				//printf("atom: LIST %s\n", listToString(dynamic_cast<Objsc::LangList*>(atom)).c_str());
				break;
			case LANG_VAR:
				//printf("atom: VAR %s\n", varToString(dynamic_cast<Objsc::LangVar*>(atom)).c_str());
				break;
		}
	}
	
	// deep copy
	Objsc::LangAtom* cloneAtom(Objsc::LangAtom* atom)
	{
		if(atom->getType() == LANG_LIST)	{
			Objsc::LangList* list = new Objsc::LangList(); // needs special copy
			list->copy(dynamic_cast<Objsc::LangList*>(atom));
			return list;
		} else {
			switch(atom->getType())	{
				case LANG_NULL:
					return new LangNull(); 
					break;
				case LANG_BANG:
					return new LangBang(); 
					break;
				case LANG_INT:
					return new LangInt(parseInt(atom)); 
					break;
				case LANG_FLOAT:
					return new LangFloat(parseFloat(atom)); 
					break;
				case LANG_STRING:
					return new LangString(parseString(atom).c_str()); 
					break;
				case LANG_LIST:
					break;
				case LANG_VAR:
					return new LangVar(); // needs special copy
					break;
			}
		}
	}
	
	Objsc::LangList* cloneList(Objsc::LangList* other, int start, int end)
	{
		Objsc::LangList* list = new Objsc::LangList(); // needs special copy
		list->copy(other, start, end);
		return list;
	}
	
	bool equalsAtom(Objsc::LangAtom* atom, Objsc::LangAtom* other)
	{
		if(atom->getType() == other->getType())	{
			//printf("types match\n");
			switch(atom->getType())	{
				case LANG_NULL:
					return true;
					break;
				case LANG_BANG:
					return true;
					break;
				case LANG_INT:
					return (parseInt(atom) == parseInt(other)); 
					break;
				case LANG_FLOAT:
					return (parseFloat(atom) == parseFloat(other)); 
					break;
				case LANG_STRING:
					return (parseString(atom) == parseString(other)); 
					break;
				case LANG_LIST:
					return true; // needs special 
					break;
				case LANG_VAR:
					return true; // needs special 
					break;
			}
		}
		return false;
	}
	
	bool equalsValue(Objsc::LangAtom* atom, Objsc::LangAtom* other)
	{
		switch(atom->getType())	{
			case LANG_NULL:
				return atom->getType() == other->getType();
				break;
			case LANG_BANG:
				return atom->getType() == other->getType();
				break;
			case LANG_INT:
				if(other->getType() == LANG_INT)	{
					return (parseInt(atom) == parseInt(other)); 
				} else if(other->getType() == LANG_FLOAT)	{
					return (parseInt(atom) == parseFloat(other)); 
				}
				break;
			case LANG_FLOAT:
				if(other->getType() == LANG_INT)	{
					return (parseFloat(atom) == parseInt(other)); 
				} else if(other->getType() == LANG_FLOAT)	{
					return (parseFloat(atom) == parseFloat(other)); 
				}
				break;
			case LANG_STRING:
				if(atom->getType() == other->getType())
					return (parseString(atom) == parseString(other)); 
				break;
			case LANG_LIST:
				return atom->getType() == other->getType(); // needs special 
				break;
			case LANG_VAR:
				return atom->getType() == other->getType(); // needs special 
				break;
		}
		return false;
	}
	
	
	////////////////////////////////
	// LANG VAR
	////////////////////////////////
	void LangVar::setValue(Objsc::LangAtomPtr nvalue)
	{ 
		//printf("LangVar::setValue A\n");
		if(nvalue->getType() != LANG_VAR)	{
			//printf("LangVar::setValue B %d\n", nvalue->getType());
			if(nvalue->getType() == LANG_LIST)	{
				//printf("LangVar::setValue C %d\n", dynamic_cast<Objsc::LangList*>(nvalue.get())->getItemType(0));
				switch(dynamic_cast<Objsc::LangList*>(nvalue.get())->getItemType(0))	{
					case LANG_NULL:
						value=LangNull::create();
						break;
					case LANG_BANG:
						value=LangBang::create();
						break;
					case LANG_INT:
						value=dynamic_cast<Objsc::LangList*>(nvalue.get())->get(0); 
						break;
					case LANG_FLOAT:
						value=dynamic_cast<Objsc::LangList*>(nvalue.get())->get(0);
						break;
					case LANG_STRING:
						value=dynamic_cast<Objsc::LangList*>(nvalue.get())->get(0);
						break;
					case LANG_LIST:
						setValue(dynamic_cast<Objsc::LangList*>(nvalue.get())->get(0));
						break;
					case LANG_VAR:
						//value=dynamic_cast<Objsc::LangVar*>(dynamic_cast<Objsc::LangList*>(nvalue.get())->get(0))->getValue();
						break;
				}
			} else {
				value=nvalue; 
			}
		}
		//printf("LangVar::setValue DONE\n");
	}
	
	////////////////////////////////
	// LANG LIST
	////////////////////////////////
	void LangList::add(Objsc::LangAtom* atom) 
	{ 
		values.push_back(cloneAtom(atom)); 
	}
	
	void LangList::set(int index, Objsc::LangAtom* atom)
	{
		if(index > -1 && index < values.size())	{
			values[index] = cloneAtom(atom);
		}
	}
	
	void LangList::copy(LangList* other, int start, int end)
	{
		clear();
		if(end == 0)
			end = other->size();
		for(int i = start; i < end; ++i)	{
			values.push_back(cloneAtom(other->idx(i)));
		}
	}
	
	Objsc::LangAtomPtr LangList::get(int index) 
	{ 
		return Objsc::LangAtomPtr(cloneAtom(values[index])); 
	}
	
	void LangList::setVar(int index, Objsc::LangAtomPtr value)
	{
		//printf("LangList::setVar A\n");
		for(LangAtomIter atom = values.begin(); atom != values.end(); ++atom) {
			//printf("LangList::setVar B\n");
			if((*atom)->getType() == LANG_VAR)	{
				//printf("LangList::setVar C\n");
				Objsc::LangVar* var = dynamic_cast<Objsc::LangVar*>((*atom));
				//printf("LangList::setVar D: %d\n", var->getID());
				if(var->getID() == index)	{
					//printf("LangList::setVar SET\n");
					var->setValue(value);
					//printf("LangList::setVar SET END\n");
				}
			}
		}
		//printf("LangList::setVar DONE\n");
	}
	
	const int LangList::find(Objsc::LangAtom* atom)
	{
		//printf("LangList::find\n");
		int pos = 0;
		for(LangAtomIter item = values.begin(); item != values.end(); ++item) {
			printAtom(atom);
			printAtom((*item));
			//printf("__\n");
			if(Objsc::equalsValue((*item), atom))	{
				//printf("match %d\n", pos);
				return pos;
			}
			++pos;
		}
		return -1;
	}
	
}
