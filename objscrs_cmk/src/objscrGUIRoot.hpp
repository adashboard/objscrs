#ifndef OBJSCR_GUIROOT_H
#define OBJSCR_GUIROOT_H


// objscrs
#include "objscrConstants.hpp"
#include "objscrGUI.hpp"
#include "singleton.h"
#include "objscrCommands.hpp"
#include "objscrDialogue.hpp"
#include "objscrStageView.hpp"
#include "objscrLangEditor.hpp"
#include "objscrFrameSourceEditor.hpp"
#include "objscrMediaPlayer.hpp"
#include "objscrZoneMapEditor.hpp"


// STL
#include <string>
using std::string;



namespace Objsc
{
	
	////////////////////////////////////////
	// PATH FIELD
	////////////////////////////////////////
	class PathField : public TextField
	{
		protected:
			string 			delim;
			vector <string>	path;
		
		public:
			PathField() : delim("/"), path() {}
			~PathField() {}
				
			void setDelimeter(const string& ndelim) {delim=ndelim;}
			void placeInPath(const string& pathpart, int index=-1);
	};
	
	////////////////////////////////////////
	// GUI ROOT
	////////////////////////////////////////
	class GUIRoot : public Singleton<GUIRoot>, public CommandReceiver
	{
		private:
			Objsc::GuiLayerPtrVector 	layers;
			Objsc::GuiLayerPtr 			rootlayer;
			Objsc::ContextMenuMap		rootmenus;
			Objsc::LangPatchEditorPtr 	patchlayer;
			Objsc::ZoneMapEditorPtr 	zonelayer;
			Objsc::FrameSourceEditorPtr 	framelayer;
			Objsc::MediaPlayer*			medialayer;
			Objsc::StageViewPtr		stagelayer;
			Objsc::PathField			pathfield;
			bool					debug;
			bool					cursoron;
			bool					dovideo;
		
			void initRootLayer();
			void initPatchLayer();
			void initZoneLayer();
			void initFrameLayer();
			void initMediaLayer();
			void initStageLayer();
			void initPathfield();
			void initStyles();
		
		public:
			GUIRoot();
			~GUIRoot() {}
			// scores
			void newScoreLoaded();
			void removingScore();
			// runtime
			void run();
			void update();
			void draw();
			// doc
			void grabScreen();
			void grabScreen(const string& prefix, int alpha);
			// commands
			void doCommand(Objsc::CommandPtr& cmd);
			void undoCommand(Objsc::CommandPtr& cmd);
			//events
			void keyPressed(int key);
			void keyReleased(int key);
			void mouseMoved(int x, int y );
			void mouseDragged(int x, int y, int button);
			void mousePressed(int x, int y, int button);
			void mouseReleased(int x, int y, int button);
			void windowResized(int w, int h);
				
			Objsc::GuiLayerPtr createLayer(float x, float y, float width, float height);
			Objsc::GuiLayerPtr createLayer(float x, float y, float width, float height, int level);
			// menus
			void openRootMenu(const string& menuname, int x=0, int y=0);
			void openRootMenu(Objsc::ContextMenuPtr menu, int x=0, int y=0);
			void closeRootMenu();
			// dialogues
			void openDialogue(Objsc::DialoguePtr dialogue);
			void closeDialogue();
			// layers
			void showPatches();
			Objsc::LangPatchEditorPtr  getPatchEditor() { return patchlayer; }
			void hidePatches();
			void showZones();
			void hideZones();
			void showFrames();
			void hideFrames();
			void showStage();
			void hideStage();
			void clearStage();
			// pathfield
			void setScorePath(const string& scorepath);
			void setMapPath(const string& mappath);
			void setZonePath(const string& zonepath);
			void clearZonePath();
			void showPath();
			void hidePath();
			void printToScreen(const string& text);
			// frame layer
			Objsc::FrameSourceEditorPtr getFrameSourceEditor() { return framelayer; };
			// settings
			void updateSettings(const string& settingsname);
	};
	
}

#endif  //OBJSCR_GUIROOT_H

