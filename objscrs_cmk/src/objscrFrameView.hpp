#ifndef  OBJSCR_FRAMEVIEW_H
#define OBJSCR_FRAMEVIEW_H

// objscrs
#include "objscrFrames.hpp"
#include "objscrGUI.hpp"

// openframeworks
#include "ofMain.h"

// openframeworks
#include "ofxOpenCv.h"

// shared_ptr
#include <tr1/memory>



namespace Objsc
{
	class FrameView : public GuiBase
	{
		private:
			Objsc::FrameSourcePtr		source;
			ofTexture					texture;
			unsigned int colorModel;
			
		public:
			FrameView() : 
				source(),
				texture(),
				colorModel(GL_RGB)
			{}
			~FrameView() {}
				
			void setSource(Objsc::FrameSourcePtr sourcePtr) 
			{
				source = sourcePtr;
				texture.allocate(source->getWidth(),source->getHeight(),GL_RGB);
				//texture.allocate(source->getWidth(),source->getHeight(),GL_LUMINANCE);
			}
			void update()
			{
				if(source->hasNewFrame())
				{
					if(colorModel == GL_RGB)
						texture.loadData(source->getFrame(),source->getWidth(),source->getHeight(),GL_RGB);
					else if(colorModel == GL_LUMINANCE)
					{
						ofxCvColorImage ci;
						ci.allocate(source->getWidth(),source->getHeight());
						ci.setFromPixels(source->getFrame(),source->getWidth(),source->getHeight());
						ofxCvGrayscaleImage gi;
						gi.allocate(source->getWidth(),source->getHeight());
						ci.toChannel(gi, ofxCvColorImage::ChannelValue);
						texture.loadData(gi.getPixels(), source->getWidth(),source->getHeight(),GL_LUMINANCE);
					}
					//texture.loadData(source->getFrame(),source->getWidth(),source->getHeight(),GL_LUMINANCE);
				}
			}
			void draw()
			{
				ofSetColor(0xffffff);
				texture.draw(bounds.getX(),bounds.getY(),bounds.getWidth(),bounds.getHeight());
			}
			
			void setColorModel(unsigned int cm)
			{
				if(cm == colorModel)
					return;

				colorModel = cm;
				texture.clear();
				texture.allocate(source->getWidth(),source->getHeight(), colorModel);

			}

			static std::tr1::shared_ptr<Objsc::FrameView> create()
			{
				return std::tr1::shared_ptr<Objsc::FrameView>(new FrameView());
			}
	};
	
	typedef std::tr1::shared_ptr<Objsc::FrameView> FrameViewPtr;
}

#endif // OBJSCR_FRAMEVIEW_H
