#include "objscrLangViews.hpp"


// objscrs
#include "objscrGUIStyles.hpp"
#include "objscrGUIRoot.hpp"
#include "objscrLangEditor.hpp"

// STL
#include <algorithm>
#include <functional>
#include <stdio.h>

#include <cmath>


namespace Objsc
{
	
	////////////////////////////////////////
	// LANG VIEW
	////////////////////////////////////////
	void LangView::mapToStage()
	{
		if(object)
		{
			// OLD
			object->setLocation((bounds.getX()-canvasrect.getX())/canvasrect.getWidth(), (bounds.getY()-canvasrect.getY())/canvasrect.getHeight());
			// TEST NEW
//			float HScale(stageRect.getWidth() / float(ofGetWidth()));
//			float VScale(stageRect.getHeight() / float(ofGetHeight()));
//			object->setLocation((bounds.getX()-canvasrect.getX())/canvasrect.getWidth(), (bounds.getY()-canvasrect.getY())/canvasrect.getHeight());

		}
	}
	
	void LangView::mapToScreen()
	{
		if(object)
		{
			float HScale(float(ofGetWidth()) / Objsc::STAGE_WIDTH);
			float VScale(float(ofGetHeight()) / Objsc::STAGE_HEIGHT);
//			_setLocation(canvasrect.getX()+(object->getX()*canvasrect.getWidth()), canvasrect.getY()+(object->getY()*canvasrect.getHeight()));
			_setLocation(object->getX() * HScale,  object->getY() * VScale);

		}
	}
	
	void LangView::_setLocation(float x, float y)
	{
		bounds.setLocation(x,y);
		inrect.setLocation(bounds.getX()+Objsc::LANGVIEW_MARGIN, bounds.getY()+Objsc::LANGVIEW_MARGIN);
	}
	
	void LangView::_setBounds(float x, float y, float width, float height)
	{
		bounds.set(x,y,width,height);
		inrect.set(bounds.getX()+Objsc::LANGVIEW_MARGIN, bounds.getY()+Objsc::LANGVIEW_MARGIN, bounds.getWidth()-Objsc::LANGVIEW_MARGIN*2, bounds.getHeight()-Objsc::LANGVIEW_MARGIN*2);
	}
	
	void LangView::link(std::string outlet, std::tr1::shared_ptr<Objsc::LangView> toView, std::string inlet)
	{
		if(object && toView->hasObject())	{
			object->link(outlet, toView->getObject(), inlet);
		}
	}
	
	void LangView::setLocation(float x, float y)
	{
		_setLocation(x,y);
		mapToStage();
	}
	
	void LangView::setBounds(float x, float y, float width, float height)
	{
		_setBounds(x,y,width,height);
		mapToStage();
	}
	
	void LangView::openMenu(int x, int y)
	{
		if(!editmenu)	{
			editmenu = Objsc::ContextMenu::create();
			Objsc::CommandPtr actionPatch = Objsc::CommandDispatcher::createCommand(Objsc::LangPatchEditor::cmdlabel  , "lobj_edit");
			actionPatch->getData()->addArg(Objsc::LangViewPtr(this));
			editmenu->addItem("edit", actionPatch);
			Objsc::CommandPtr actionDelete = Objsc::CommandDispatcher::createCommand(Objsc::LangPatchEditor::cmdlabel  , "lobj_delete");
			actionDelete->getData()->addArg(Objsc::LangViewPtr(this));
			editmenu->addItem("delete", actionDelete);
			Objsc::CommandPtr actionHelp = Objsc::CommandDispatcher::createCommand(Objsc::LangPatchEditor::cmdlabel  , "lobj_help");
			actionHelp->getData()->addArg(Objsc::LangViewPtr(this));
			editmenu->addItem("help", actionHelp);
		}
		if(hasFocus)
			looseFocus();
		Objsc::GUIRoot::Current()->openRootMenu(editmenu, x, y);
	}
	

}

