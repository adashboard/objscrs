#ifndef  OBJSCR_DATATYPES_H
#define OBJSCR_DATATYPES_H


// openframeworks
#include "ofMain.h"

// STL
#include <string>
#include <vector>
using std::string;
using std::vector;

namespace Objsc
{
	typedef vector< string > StringVector;
	typedef vector < float > FloatVec;
	typedef vector < int > IntVec;
	typedef vector < ofPoint > PointVec;
}

#endif // OBJSCR_DATATYPES_H
