#ifndef OBJSCR_DEVICES_H
#define OBJSCR_DEVICES_H

// objscrs
#include "singleton.h"
#include "objscrConstants.hpp"

// shared_ptr
#include <tr1/memory>

// STL
#include <iostream>
#include <string>
#include <exception>
#include <list>
using std::string;
using std::exception;
using std::list;

namespace Objsc
{
	class Device
	{
		private:
			int		devtype;
			string	path;
		
		public: 
			Device() : 
				devtype(DEVICE_NONE), 
				path() {}
			~Device() {}
			
			void setDeviceType(int nDevtype) { devtype = nDevtype; }
			const int getDeviceType() const { return devtype; }
			void setPath(const string& pathStr) { path = pathStr; }
			const string& getPath() const { return path; }
				
			static std::tr1::shared_ptr<Objsc::Device> create()
			{
				return std::tr1::shared_ptr<Objsc::Device>(new Device());
			}
			
			static std::tr1::shared_ptr<Objsc::Device> create(int nDevtype, const string &pathStr)
			{
				std::tr1::shared_ptr<Objsc::Device> ptr (new Device());
				ptr->setDeviceType(nDevtype);
				ptr->setPath(pathStr);
				std::cout << pathStr << std::endl;
				return ptr;
			}
	};
	
	typedef std::tr1::shared_ptr<Objsc::Device> DevicePtr;
	typedef list <Objsc::DevicePtr> DevicePtrList;
	
	class DeviceManager : public Singleton<DeviceManager>
	{
		
		public:
			DeviceManager() {}
			~DeviceManager() {}
				
			DevicePtrList scanVideoDevices();
	};
}

#endif 