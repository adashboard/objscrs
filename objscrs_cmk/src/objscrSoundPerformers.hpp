#ifndef NITT_SOUNDPERFORMERS
#define NITT_SOUNDPERFORMERS

// Objsc
#include "objscrSound.hpp"
#include "objscrDataTypes.hpp"

// STL
#include <vector>
#include <map>
using std::vector;
using std::map;


// shared_ptr
#include <tr1/memory>


namespace Objsc
{

	/////////////////////////////////////
	// FUNCTIONS
	/////////////////////////////////////
	void initSoundPerformers();
	
	////////////////////////////////////////
	// SOUND PERFORMER
	////////////////////////////////////////
	class SoundPerformer : public Sound, public SoundListener
	{
		protected:
			bool						active;
			Objsc::SoundListenerVec			listeners;
			float						speed;
			float						vol;
			float						panL;
			float						panR;
			int							loops;
			int							loopcount;
		
			void updateListeners(SoundEvent& event);
			
		public:
			SoundPerformer();
			virtual ~SoundPerformer();
		
			virtual float tickL() { return 0.0f; }
			virtual float tickR() { return 0.0f; }
		
			virtual void update(int timeframe) {}
				
			virtual void receiveSoundEvent(SoundEvent& event) {}
			
			virtual void start() { active=true; }
			virtual void stop() { active=false; }
			const bool isActive() const { return active; }
			virtual void setSpeed(float value) {speed=value;}
			virtual const float getSpeed() const { return speed; }
			virtual void setPan(float value)
			{
				panL = 1.0-value;
				panR = value;
			}
			virtual const float getPan() const { return panR; }
			virtual void setVolume(float value){vol=value;}
			virtual const float getVol() const { return vol; }
			virtual void setLoops(int value)
			{
				loops = value;
				loopcount = 0;
			}
			virtual const int getLoops() const { return loops; }
			virtual const int getCurrentLoop() const { return loopcount; }
			virtual const bool getIsPlaying() const { return false; }
			virtual const float getAmp() { return 0.0f; }
			
			virtual void loadSound(const string& filename) {}
			
			void addListener(Objsc::SoundListenerPtr listener);
	};
	
	typedef std::tr1::shared_ptr<Objsc::SoundPerformer> SoundPerformerPtr;
	typedef vector < Objsc::SoundPerformerPtr > SoundPerformerVec;
	typedef map < string, Objsc::SoundPerformerPtr > SoundPerformerMap;
	typedef SoundPerformerMap::value_type SoundPerformerEntry;
	
	
	////////////////////////////////////////
	// SINGLE NOTE
	////////////////////////////////////////
	class SingleNote : public SoundPerformer
	{
		protected:
			Objsc::SoundPlayerPtr	sound;
			
		public:
			SingleNote();
			~SingleNote();
		
			float tickL() { return sound?sound->tickL():0.0f; }
			float tickR() { return sound?sound->tickR():0.0f; }
			
			void receiveSoundEvent(SoundEvent& event);
		
			void start();
			void update(int timeframe);
			void loadSound(const string& filename);
			void setSpeed(float value);
			void setPan(float value);
			void setVolume(float value);
			void setLoops(int value);
			const int getCurrentLoop() const { return sound?sound->getCurrentLoop():0; }
			const bool getIsPlaying() const {sound?sound->getIsPlaying():false;}
			const float getAmp(){ sound?sound->getAmp():0.0f;}
			
			static const string soundname;
			const string& getSoundName() const { return Objsc::SingleNote::soundname; }
	};
	
	////////////////////////////////////////
	// RANDOM NOTE
	////////////////////////////////////////
	class RandomNote : public SoundPerformer
	{
		protected:
			Objsc::SoundPlayerVec 	sounds;
			Objsc::SoundPlayerPtr	sound;
			
		public:
			RandomNote();
			~RandomNote();
		
			float tickL() { return sound?sound->tickL():0.0f; }
			float tickR() { return sound?sound->tickR():0.0f; }
			
			void receiveSoundEvent(SoundEvent& event);
		
			void start();
			void update(int timeframe);
			void loadSound(const string& filename);
			void setSpeed(float value);
			void setPan(float value);
			void setVolume(float value);
			void setLoops(int value);
			const int getCurrentLoop() const { return sound?sound->getCurrentLoop():0; }
			const bool getIsPlaying() const {sound?sound->getIsPlaying():false;}
			const float getAmp(){ sound?sound->getAmp():0.0f;}
			
			static const string soundname;
			const string& getSoundName() const { return Objsc::RandomNote::soundname; }
	};
	
	////////////////////////////////////////
	// RANDOM SEQUENCE
	////////////////////////////////////////
	class RandomSequence : public SoundPerformer
	{
		protected:
			Objsc::SoundPlayerVec 	sounds;
			Objsc::SoundPlayerPtr	sound;
			Objsc::IntVec			seq;
			int					count;
		
			void shuffle();
			void refill();
			
		public:
			RandomSequence();
			~RandomSequence();
		
			static const string soundname;
			const string& getSoundName() const { return Objsc::RandomSequence::soundname; }
			
			float tickL() { return sound?sound->tickL():0.0f; }
			float tickR() { return sound?sound->tickR():0.0f; }
			
			void receiveSoundEvent(SoundEvent& event);
		
			void start();
			void update(int timeframe);
			void loadSound(const string& filename);
			void setSpeed(float value);
			void setPan(float value);
			void setVolume(float value);
			void setLoops(int value);
			const int getCurrentLoop() const { return sound?sound->getCurrentLoop():0; }
			const bool getIsPlaying() const {sound?sound->getIsPlaying():false;}
			const float getAmp(){ sound?sound->getAmp():0.0f;}
	};
	
	////////////////////////////////////////
	// CLOUD
	////////////////////////////////////////
	class Cloud : public SoundPerformer
	{
		protected:
			Objsc::SoundPlayerVec 	sounds;
			Objsc::SoundPlayerPtr	sound;
			float				sampleL;
			float				sampleR;
			int 					density;
			float 				variance;
			int 					nextsoundtime;
		
		public:
			Cloud();
			~Cloud();
		
			static const string soundname;
			const string& getSoundName() const { return Objsc::Cloud::soundname; }
			
			float tickL();
			float tickR();
			
			void receiveSoundEvent(SoundEvent event);
		
			void start();
			void update(int timeframe);
			void loadSound(const string& filename);
	};
	
	////////////////////////////////////////
	// SEQUENCER
	////////////////////////////////////////
	class Sequencer : public SoundPerformer
	{
		protected:
			Objsc::SoundPlayerVec 	sounds;
			Objsc::SoundPlayerPtr	sound;
			Objsc::IntVec			seq;
			
		public:
			Sequencer();
			~Sequencer();
		
			static const string soundname;
			const string& getSoundName() const { return Objsc::Sequencer::soundname; }
			
			float tickL() { return sound?sound->tickL():0.0f; }
			float tickR() { return sound?sound->tickR():0.0f; }
			
			void receiveSoundEvent(SoundEvent event);
		
			void start();
			void update(int timeframe);
			void loadSound(const string& filename);
	};
	
	////////////////////////////////////////
	// CREATORS
	////////////////////////////////////////
	class SoundPerformerCreator
	{
		public:
			virtual ~SoundPerformerCreator() {}
			virtual Objsc::SoundPerformerPtr create() const =0;
	};
	
	class SingleNoteCreator: public SoundPerformerCreator
	{
		public:
			Objsc::SoundPerformerPtr create() const {return Objsc::SoundPerformerPtr(new Objsc::SingleNote());}
	};
	
	class RandomNoteCreator: public SoundPerformerCreator
	{
		public:
			Objsc::SoundPerformerPtr create() const {return Objsc::SoundPerformerPtr(new Objsc::RandomNote());}
	};
	
	class RandomSequenceCreator: public SoundPerformerCreator
	{
		public:
			Objsc::SoundPerformerPtr create() const {return Objsc::SoundPerformerPtr(new Objsc::RandomSequence());}
	};
	
	class CloudCreator: public SoundPerformerCreator
	{
		public:
			Objsc::SoundPerformerPtr create() const {return Objsc::SoundPerformerPtr(new Objsc::Cloud());}
	};
	
	class SequencerCreator: public SoundPerformerCreator
	{
		public:
			Objsc::SoundPerformerPtr create() const {return Objsc::SoundPerformerPtr(new Objsc::Sequencer());}
	};
	
	////////////////////////////////////////
	// FACTORY
	////////////////////////////////////////
	typedef map < string, SoundPerformerCreator* > SoundPerformerCreatorMap;
	typedef SoundPerformerCreatorMap::value_type SoundPerformerCreatorEntry;

	class SoundPerformerFactory : public Singleton<SoundPerformerFactory>
	{
		private:
			Objsc::SoundPerformerCreatorMap creators;
		
		public:
			SoundPerformerFactory() : creators() {}
			~SoundPerformerFactory() { creators.clear(); }
			
			Objsc::SoundPerformerPtr create(const string& soundname)
			{
				Objsc::SoundPerformerCreatorMap::iterator iter = creators.find(soundname);
				if(iter == creators.end()) {
					throw std::exception();
				}
				return (*iter).second->create();
			}
			
			void addCreator(const string& soundname, SoundPerformerCreator* creatorPtr)
			{
				creators.insert(SoundPerformerCreatorEntry(soundname, creatorPtr));
			}
			
			bool hasCreator(const string& soundname) {return creators.count(soundname) > 0;}
			
	};
	
}

#endif