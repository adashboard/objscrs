#ifndef _TEXT_DISPLAY
#define _TEXT_DISPLAY

// openframeworks
#include "ofMain.h"

// project
#include "util.h"

// stdlib
//#include <string.h>
#include <string>
#include <vector>
using namespace std;


namespace Objsc
{
	////////////////////////////////////////
	// CLASSES
	////////////////////////////////////////
	class MultilineText
	{
		private:
			// text
			vector<string> textlines;
			int linesinstatement;
			int currentline;
			int currentchar;
			int currentlinelength;
			float lineX;
			float lineY;
			float lineoffsetY;
			float linepaddingX;
			float lineHeight;
			float lineWidth;
		
			// display
			ofTrueTypeFont displayFont;
			string *displaylines;
			int topline;
			int totaldisplaylines;
			int maxdisplaylines;
			int maxcharsperline;
		
			Colour bgcolour;
			Colour forecolour;
		
			float scrWidth;
			float scrHeight;
			
			float cursorWidth;
			float cursorHeight;
			
			int blinkperiod;
			int blinkcount;
			bool textprinting;
			
			int displaystate;
			
			
			// time
			int nowtime;
			int timeout;
			int textwait;
			int pausewait;
			int duration;
			bool doingtime;
			bool expired;
			
		public:
			MultilineText();
			~MultilineText();
			MultilineText(const MultilineText &copy);
			MultilineText& operator=(const MultilineText &copy);
		
			void setup(float width, float height, int maxlines);
			void setText(string newtext);
			
			void draw();
			void setBackgroundColour(Colour colour);
			void setForeColour(Colour colour);
				
			void nextLine();
			void nextChar();
		
			void setTimeout(int nduration);
			void setTextwait(int nduration);
			void setPausewait(int nduration);
			void cancelTimeout();
			void updateTimeout();
			void startTimeout();
			bool hasExpired() {return expired;}
	};
}
#endif

