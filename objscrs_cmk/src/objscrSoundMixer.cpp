#include "objscrSoundMixer.hpp"



namespace Objsc
{
	
	////////////////////////////////////////
	// SOUND MIXER
	////////////////////////////////////////
	SoundMixer::SoundMixer() : 
			sounds(),
			sampleL(0.0f),
			sampleR(0.0f)
	{
		initSoundPerformers();
	}

	SoundMixer::~SoundMixer()
	{
		sounds.clear();
	}
	
	Objsc::SoundPerformerPtr SoundMixer::createSound(const string& label, const string& soundname)
	{
		if(sounds.count(label) == 0)	{
			sounds.insert(SoundPerformerEntry(label, Objsc::SoundPerformerFactory::Current()->create(soundname)));
		}
		return sounds[label];
	}
	
	void SoundMixer::audioRequested(float* output, int bufferSize, int nChannels)
	{
		for (int i = 0; i < bufferSize; i++){
			sampleL = sampleR = 0.0f;
			for(Objsc::SoundPerformerMap::iterator snd = sounds.begin(); snd != sounds.end(); ++snd)	{
				sampleL += snd->second->tickL();
				sampleR += snd->second->tickR();
			}
			output[i*nChannels] = sampleL;
			output[i*nChannels + 1] = sampleR;
		}
	}
	
	
	void SoundMixer::update(int timeframe)
	{
		for(Objsc::SoundPerformerMap::iterator snd = sounds.begin(); snd != sounds.end(); ++snd)	{
			snd->second->update(timeframe);
		}
	}
}

