#include "objscrZoneMapEditor.hpp"

// objscrs
#include "objscrGUIRoot.hpp"
#include "objscrLangEditor.hpp"
#include "objscrApp.hpp"
#include "objscrZoneMapFinder.hpp"
#include "objscrZones.hpp"


// openframeworks
#include "ofMain.h"

namespace Objsc
{
	
	
	////////////////////////////////////////
	// DECLARATIONS
	////////////////////////////////////////
	const string Objsc::ZoneMapEditor::cmdlabel = "_zonemapeditor_";
	
	
	////////////////////////////////////////
	// ZONE MAP EDITOR
	////////////////////////////////////////
	ZoneMapEditor::ZoneMapEditor() : 
			//score(), 
			zonecanvas(), 
			zonemenu(), 
			zmapdialogue(),
			stylename("zonemapeditor"), 
			scrX(0), 
			scrY()
	{
		zonecanvas = Objsc::ZoneMapView::create();
		autosize();
		// commands
		Objsc::CommandDispatcher::Current()->registerCommandReceiver(Objsc::ZoneMapEditor::cmdlabel, this);
		initMenus();
		initDialogues();
		zonecanvas->show();
	}
	
	void ZoneMapEditor::initMenus()
	{
		// object menu
		zonemenu = Objsc::ContextMenu::create();
		zonemenu->addItem("square zone", Objsc::CommandDispatcher::createCommand(Objsc::ZoneMapEditor::cmdlabel, "new_zone"));
		zonemenu->addItem("point zone", Objsc::CommandDispatcher::createCommand(Objsc::ZoneMapEditor::cmdlabel, "new_pointzone"));
		zonemenu->addItem("grid zone", Objsc::CommandDispatcher::createCommand(Objsc::ZoneMapEditor::cmdlabel, "new_gridzone"));
	}
	
	void ZoneMapEditor::initDialogues()
	{
		zmapdialogue = Objsc::Dialogue::create("_zmapdia_");
		zmapdialogue->sizeEditItems(true);
		zmapdialogue->setActionLabel("edit");
		zmapdialogue->setFinder(Objsc::ZoneMapFinder::create());
	}
	
	void ZoneMapEditor::createZoneMap(const string& mapname)
	{
		if(mapname.empty())	{
			zonecanvas->setZoneMap(Objsc::App::Current()->getScore()->createZoneMapAsCurrent());
			Objsc::Stage::Current()->setZoneMap(Objsc::App::Current()->getScore()->getZoneMap());
			Objsc::GUIRoot::Current()->setMapPath(Objsc::App::Current()->getScore()->getZoneMap()->getName());
		} else {
			zonecanvas->setZoneMap(Objsc::App::Current()->getScore()->createZoneMapAsCurrent(mapname));
			Objsc::Stage::Current()->setZoneMap(Objsc::App::Current()->getScore()->getZoneMap());
			Objsc::GUIRoot::Current()->setMapPath(Objsc::App::Current()->getScore()->getZoneMap()->getName());
		}
	}
	
	void ZoneMapEditor::setZoneMap(const string& mapname)
	{
		if(mapname.empty())	{
			zonecanvas->setZoneMap(Objsc::App::Current()->getScore()->setZoneMap());
			Objsc::Stage::Current()->setZoneMap(Objsc::App::Current()->getScore()->getZoneMap());
			Objsc::GUIRoot::Current()->setMapPath(Objsc::App::Current()->getScore()->getZoneMap()->getName());
		} else {
			if(Objsc::App::Current()->getScore()->hasZoneMap(mapname))	{
				zonecanvas->setZoneMap(Objsc::App::Current()->getScore()->setZoneMap(mapname));
				Objsc::Stage::Current()->setZoneMap(Objsc::App::Current()->getScore()->getZoneMap(mapname));
				Objsc::GUIRoot::Current()->setMapPath(mapname);
			}
		}
	}
	
	
	void ZoneMapEditor::removeZoneMap(const string& mapname)
	{
		if(Objsc::App::Current()->getScore()->hasZoneMap(mapname))	{
			zonecanvas->setZoneMap(Objsc::App::Current()->getScore()->removeZoneMap(mapname));
			Objsc::Stage::Current()->setZoneMap(Objsc::App::Current()->getScore()->getZoneMap());
		}
	}
	
	
	void ZoneMapEditor::update()
	{
		zonecanvas->update();
	}
	
	void ZoneMapEditor::draw()
	{
		if(visible)	{
			zonecanvas->draw();
		}
	}
	
	
	// commands
	void ZoneMapEditor::doCommand(Objsc::CommandPtr& cmd)
	{
		//std::cout << "ZoneMapEditor::doCommand: " << cmd->getName() << std::endl;
		string cmdname = cmd->getName();
		if(cmdname ==  "new_zone")	{
			Objsc::GUIRoot::Current()->closeRootMenu();
			zonecanvas->createZone(scrX,scrY);
		} else if(cmdname ==  "new_gridzone")	{
			Objsc::GUIRoot::Current()->closeRootMenu();
			zonecanvas->createZone(scrX,scrY, Objsc::GridZone::zonetype);
		} else if(cmdname ==  "new_pointzone")	{
			Objsc::GUIRoot::Current()->closeRootMenu();
			zonecanvas->createZone(scrX,scrY, Objsc::CircleZone::zonetype);
		} else if(cmdname ==  "zone_patch")	{ // ZONE
			Objsc::GUIRoot::Current()->closeRootMenu();
			Objsc::ZoneViewPtr zoneview;
			Objsc::PacketPtr data = cmd->getData();
			data->getArg(0,&zoneview);
			if(zoneview)	{
				//printf("zone_patch <%s>\n", zoneview->getZone()->getName().c_str());
				Objsc::LangPatchEditorPtr patcheditor = Objsc::GUIRoot::Current()->getPatchEditor();
				patcheditor->setPatch(zoneview->getZone()->getPatch());
				patcheditor->enable();
			}
		} else if(cmdname ==  "zone_rename")	{
			Objsc::ZoneViewPtr zoneview;
			Objsc::PacketPtr data = cmd->getData();
			data->getArg(0,&zoneview);
			if(zoneview)	{
				//printf("zone_rename <%s>\n", zoneview->getZone()->getName().c_str());
				zoneview->startRename();
			}
		} else if(cmdname ==  "zone_delete")	{
			Objsc::GUIRoot::Current()->closeRootMenu();
			Objsc::ZoneViewPtr zoneview;
			Objsc::PacketPtr data = cmd->getData();
			data->getArg(0,&zoneview);
			if(zoneview)	{
				//printf("zone_delete <%s>\n", zoneview->getZone()->getName().c_str());
				zonecanvas->removeZone(zoneview);
			}
		} else if(cmdname ==  "list_maps")	{
			Objsc::GUIRoot::Current()->closeRootMenu();
			viewMapsList();
		} else if(cmdname ==  "set_map")	{
			Objsc::GUIRoot::Current()->closeRootMenu();
			string mapname = "";
			Objsc::PacketPtr data = cmd->getData();
			data->getArg(0,&mapname);
			setZoneMap(mapname);
		} else if(cmdname ==  "new_map")	{
			string mapname = "";
			Objsc::PacketPtr data = cmd->getData();
			data->getArg(0,&mapname);
			createZoneMap(mapname);
		} else if(cmdname ==  "remove_map")	{
			string mapname = "";
			Objsc::PacketPtr data = cmd->getData();
			data->getArg(0,&mapname);
			removeZoneMap(mapname);
		}
	}
	
	void ZoneMapEditor::undoCommand(Objsc::CommandPtr& cmd)
	{
		std::cout << "ZoneMapEditor::undoCommand: " << cmd->getName() << std::endl;
	}
	
	
	void ZoneMapEditor::viewMapsList()
	{
		zmapdialogue->setText("");
		Objsc::GUIRoot::Current()->openDialogue(zmapdialogue);
	}
	
	
	void ZoneMapEditor::keyPressed(int key)
	{
		zonecanvas->keyPressed(key);
	}
	
	void ZoneMapEditor::keyReleased(int key)
	{
		zonecanvas->keyReleased(key);
	}
	
	void ZoneMapEditor::mouseMoved(int x, int y )
	{
		zonecanvas->mouseMoved(x,y);
	}
	
	void ZoneMapEditor::mouseDragged(int x, int y, int button)
	{
		zonecanvas->mouseDragged(x,y,button);
	}
	
	void ZoneMapEditor::mousePressed(int x, int y, int button)
	{
		consumed = false;
		scrX = x;
		scrY = y;
		zonecanvas->mousePressed(x,y,button);
		if(zonecanvas->eventConsumed())	{
			consumed = true;
			return;
		}
		switch(button)	{
			case 0:
				consumed = true;
				Objsc::GUIRoot::Current()->openRootMenu(zonemenu, x, y);
				break;
			case 1:
				//openRootMenu("main", x, y);
				break;
			case 2:
				consumed = false;
				//~ consumed = true;
				//~ scrX = x;
				//~ scrY = y;
				//~ Objsc::GUIRoot::Current()->openRootMenu(patchmenu, x, y);
				break;
		}
	}
	
	void ZoneMapEditor::mouseReleased(int x, int y, int button)
	{
		zonecanvas->mouseReleased(x,y,button);
	}
	
	void ZoneMapEditor::windowResized(int w, int h)
	{
		zonecanvas->windowResized(w,h);
	}
	
	void ZoneMapEditor::autosize()
	{
		setBounds(0,0, ofGetWidth(),ofGetHeight());
		zonecanvas->setBounds(bounds.getX(), bounds.getY(), bounds.getWidth(), bounds.getHeight());
	}
}

