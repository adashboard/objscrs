#ifndef  OBJSCR_SCORE_H
#define OBJSCR_SCORE_H


// objscrs
#include "objscrZoneMap.hpp"
#include "objscrZones.hpp"
#include "objscrLang.hpp"


// shared_ptr
#include <tr1/memory>

// STL
#include <string>
using std::string;
#include <map>
using std::map;
#include <vector>
using std::vector;

namespace Objsc
{
	
	////////////////////////////////////////
	// SCORE PATH
	////////////////////////////////////////
	typedef vector< string > ScorePath;
	
	Objsc::ScorePath parseScorePath(const string& pathstr);
	
	////////////////////////////////////////
	// SCORE
	////////////////////////////////////////
	class Score
	{
		private:
			string 				name;
			Objsc::ZoneMapMap 	zonemaps;
			Objsc::ZoneMapPtr		zonemap;
			Objsc::LangPatchMap	patches;
			int					zonenums;
			bool				active;
		
		public:
			Score(const string& nname="");
			~Score() {}
				
			void setName(const string& nname);
			const string& getName() const {return name;}
			
			void update(int timeElapsed);
			void start() { active=true; }
			void stop() { active=false; }
			bool isActive() const { return active; }
			
			Objsc::LangPatchPtr createPatch(const string& patchname);
				
			Objsc::ZoneMapPtr createZoneMap();
			Objsc::ZoneMapPtr createZoneMapAsCurrent(const string& mapname="");
			Objsc::ZoneMapPtr createZoneMap(string mapname);
			const bool hasZoneMap(const string& mapname) const;
			Objsc::ZoneMapPtr getZoneMap() const { return zonemap; }
			Objsc::ZoneMapPtr getZoneMap(const string& mapname);
			Objsc::ZoneMapPtr setZoneMap(const string& mapname = "");
			Objsc::ZoneMapPtr removeZoneMap(const string& mapname);
			Objsc::ZoneMapPtr makeFirstMapCurrent();
			
			const Objsc::ZoneMapMap& getZoneMaps() const { return zonemaps; }
			
			Objsc::ZonePtr getZone(const string& zonepath);
			
			void sendToCurrentZoneMap(const string& label, Objsc::LangAtomPtr atom);
				
			static std::tr1::shared_ptr<Objsc::Score> create(const string& nname="")
			{
				return std::tr1::shared_ptr<Objsc::Score>(new Score(nname));
			}
	};
	
	typedef std::tr1::shared_ptr<Objsc::Score> ScorePtr;
	typedef map <string, ScorePtr> ScoreMap;
	typedef ScoreMap::value_type ScoreMapEntry;
}

#endif // OBJSCR_SCORE_H

