#ifndef  OBJSCR_ZONEMAP_H
#define OBJSCR_ZONEMAP_H

// objscrs
#include "objscrZones.hpp"
#include "objscrIDBroker.hpp"
//#include "objscrLang.hpp"
#include "objscrLangAtoms.hpp"

// shared_ptr
#include <tr1/memory>

// STL
#include <string>
#include <map>
#include <vector>
#include <set>
using std::string;
using std::map;
using std::vector;
using std::set;


namespace Objsc
{
	
	////////////////////////////////////////
	// ZONE MAP
	////////////////////////////////////////
	class ZoneMap
	{
		private:
			string 				name;
			Objsc::ZonePtrList 		zones;
			unsigned int			zonecount;
		
		public:
			ZoneMap();
			~ZoneMap() { zones.clear(); }
			
			void setName(string nname) { name = nname; }
			const string& getName() const { return name; }
			
			Objsc::ZonePtr createZone(const string& zonetype, const string& zonename="");
			void removeZone(Objsc::ZonePtr zone);
			const bool hasZones() const { return !zones.empty();}
			const Objsc::ZonePtrList& getZones() const { return zones; }
			Objsc::ZonePtr getZone(const string& zonename);
			
			void updateBodies(const Objsc::BodyPtrList&  bodies);
			
			void update(int timeElapsed);
			
			void onload();
			
			const int getZonecount() const { return zonecount; }
			void setZonecount(int value) { zonecount=value; }
			
			void sendToAllZones(const string& label, Objsc::LangAtomPtr atom);
			
			static std::tr1::shared_ptr<Objsc::ZoneMap> create(const string& nname="")
			{
				std::tr1::shared_ptr<Objsc::ZoneMap> ptr(new ZoneMap());
				ptr->setName(nname);
				return ptr;
			}
		
	};
	typedef std::tr1::shared_ptr<Objsc::ZoneMap> ZoneMapPtr;
	typedef vector < Objsc::ZoneMapPtr> ZoneMapVec;
	typedef set < Objsc::ZoneMapPtr> ZoneMapSet;
	typedef map <string, Objsc::ZoneMapPtr> ZoneMapMap;
	typedef ZoneMapMap::value_type ZoneMapEntry;
}

#endif // OBJSCR_ZONEMAP_H

