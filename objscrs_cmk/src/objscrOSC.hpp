#ifndef  OBJSCR_OSC_H
#define OBJSCR_OSC_H


// objscrs
#include "singleton.h"
#include "objscrLangAtoms.hpp"

// openframeworks
#include "ofxOsc.h"

// STL
#include <string>
#include <map>
using std::string;
using std::map;

// shared_ptr
#include <tr1/memory>


namespace Objsc
{
	
	////////////////////////////////////////
	// FUNCTIONS
	////////////////////////////////////////
	void initOSCComms();
	Objsc::LangList* OSCMessageToList(ofxOscMessage& msg);
	
	////////////////////////////////////////
	// OSC GROUP
	////////////////////////////////////////
	typedef map < string, ofxOscSender* >OSCAddressMap;
	typedef OSCAddressMap::value_type OSCAddressEntry;
	
	class OSCGroup
	{
		private:
			string 			name;
			OSCAddressMap 	addresses;
		
		public:
			OSCGroup() : name(), addresses() {}
			OSCGroup(string nname) : name(nname), addresses() {}
			~OSCGroup() {addresses.clear();}
			void join(string label, string host, int port);
			void leave(string label);
			void send(ofxOscMessage& msg);
			
			const string& getName() const { return name; }
			
			static std::tr1::shared_ptr<Objsc::OSCGroup> create(const string& nname="")
			{
				return std::tr1::shared_ptr<Objsc::OSCGroup>(new OSCGroup(nname));
			}
	};
	
	typedef std::tr1::shared_ptr<Objsc::OSCGroup> OSCGroupPtr;
		
	
	////////////////////////////////////////
	// OSC MESSAGE HANDLER
	////////////////////////////////////////
	class OSCHandler
	{
		public:
			virtual ~OSCHandler() {}
			virtual void handle(ofxOscMessage& msg) {}
	};
	
	typedef std::tr1::shared_ptr<Objsc::OSCHandler> OSCHandlerPtr;
	typedef map < string, Objsc::OSCHandlerPtr > OSCHandlerMap;
	typedef OSCHandlerMap::value_type OSCHandlerEntry;
	
	class OSCTestHandler : public OSCHandler
	{
		public:
			OSCTestHandler() {}
			~OSCTestHandler() {}
			void handle(ofxOscMessage& msg) 
			{
				printf("OSCTestHandler::handle\n");
			}
			
			static Objsc::OSCHandlerPtr create()
			{
				return Objsc::OSCHandlerPtr(new OSCTestHandler());
			} 
	};
	
	class OSCMessageHandler : public OSCHandler
	{
		public:
			OSCMessageHandler() {}
			~OSCMessageHandler() {}
			void handle(ofxOscMessage& msg);
			
			static Objsc::OSCHandlerPtr create()
			{
				return Objsc::OSCHandlerPtr(new OSCMessageHandler());
			} 
	};
	
	////////////////////////////////////////
	// JOIN HANDLER
	////////////////////////////////////////
	class OSCJoinHandler : public OSCHandler
	{
		public:
			OSCJoinHandler() {}
			~OSCJoinHandler() {}
			void handle(ofxOscMessage& msg);
			void sendConfirm(const string& groupname, const string& label);
			
			static Objsc::OSCHandlerPtr create()
			{
				return Objsc::OSCHandlerPtr(new OSCJoinHandler());
			} 
	};
	
	////////////////////////////////////////
	// LEAVE HANDLER
	////////////////////////////////////////
	class OSCLeaveHandler : public OSCHandler
	{
		public:
			OSCLeaveHandler() {}
			~OSCLeaveHandler() {}
			void handle(ofxOscMessage& msg);
			void sendConfirm(const string& groupname, const string& label);
			
			static Objsc::OSCHandlerPtr create()
			{
				return Objsc::OSCHandlerPtr(new OSCLeaveHandler());
			} 
	};
	
	////////////////////////////////////////
	// IMAGE HANDLER
	////////////////////////////////////////
	class OSCImageHandler : public OSCHandler
	{
		public:
			OSCImageHandler() {}
			~OSCImageHandler() {}
			void handle(ofxOscMessage& msg);
			
			static Objsc::OSCHandlerPtr create()
			{
				return Objsc::OSCHandlerPtr(new OSCImageHandler());
			} 
	};
	
	////////////////////////////////////////
	// OSC LISTENER
	////////////////////////////////////////
	class OSCListener
	{
		private:
			string 			name;
			ofxOscReceiver 	receiver;
			OSCHandlerMap	handlers;

		public:
			OSCListener(string nname, int port) : 
				name(nname), 
				receiver(),
				handlers()
			{
				receiver.setup(port);
			}
			~OSCListener() {}
				
			void update();
			void setHandler(string pktaddress, OSCHandlerPtr handler);
			void removeHandler(string pktaddress);
			void handle(ofxOscMessage& msg);
	};
	
	////////////////////////////////////////
	// OSC COMMS
	////////////////////////////////////////
	typedef map < string, Objsc::OSCGroupPtr > OSCGroupMap;
	typedef OSCGroupMap::value_type OSCGroupEntry;
	
	typedef map <string, OSCListener* > OSCListenerMap;
	typedef OSCListenerMap::value_type OSCListenerEntry;
	
	class OSCComms : public Singleton<OSCComms>
	{
		private:
			OSCGroupMap 		groups;
			OSCListenerMap 	listeners;
		
		public:
			OSCComms() : groups() {}
			~OSCComms() {groups.clear();}
			// outgoing
			Objsc::OSCGroupPtr createGroup(const string& groupname);
			void joinGroup(string groupname, string label, string host, int port);
			void leaveGroup(string groupname, string label);
			void send(string groupname, ofxOscMessage& msg);
			void send(ofxOscMessage& msg);
			// incoming
			void startListen(string listenername, int port);
			void stopListen(string listenername);
			void setListenHandler(string listenername, string pktaddress, OSCHandlerPtr handler);
			void update();
	};
	
}

#endif // OBJSCR_OSC_H
