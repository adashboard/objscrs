#ifndef OBJSCR_LANGVIEWS_H
#define OBJSCR_LANGVIEWS_H


// objscrs
#include "objscrConstants.hpp"
#include "objscrGUI.hpp"
#include "objscrGeom.hpp"
#include "objscrLang.hpp"
#include "objscrCommands.hpp"
#include "Lang/LangPatch.hpp"

// STL
#include <string>
#include <vector>
#include <map>
#include <list>
using std::string;
using std::vector;
using std::map;
using std::list;

// shared_ptr
#include <tr1/memory>


namespace Objsc
{
	class WidgetStyle;

	static const float LANGVIEW_DFTWIDTH = 40.0f;
	static const float LANGVIEW_DFTHEIGHT = 20.0f;
	static const float LANGVIEW_BEVEL = 5.0f;
	static const float LANGVIEW_SOCKETWIDTH = 10.0f;
	static const float LANGVIEW_SOCKETHEIGHT = 4.0f;
	static const float LANGVIEW_MARGIN = 3.0f;
	static const float LANGVIEW_RIGHTMARGIN = 10.0f;
	
	enum socket_type
	{
		LANG_INLET,
		LANG_OUTLET
	};
	
	////////////////////////////////////////
	// LANG VIEW BASE
	////////////////////////////////////////
	class LangViewBase : public Objsc::Widget
	{
		public:
			virtual ~LangViewBase() {}
			static const string stylename;
				
			virtual void mouseMoved(int x, int y)
			{
				if(withinBounds(x,y) && visible)	{
					if(!hasFocus && displayState!=Objsc::DISABLED) {
						gainFocus();
					}
				} else {
					if(hasFocus && displayState!=Objsc::DISABLED) {
						looseFocus();
					}
				}
			}
	};
	
	typedef std::tr1::shared_ptr<Objsc::LangViewBase> LangViewBasePtr;
	
	
	////////////////////////////////////////
	// LANG VIEW
	////////////////////////////////////////
	class LangView : public Objsc::LangViewBase
	{
		protected:
			Objsc::LangObjectPtr 	object;
			Objsc::Rect			inrect;
			Objsc::ContextMenuPtr 	editmenu;
			Objsc::Rect			canvasrect;
			
			void openMenu(int x, int y);
			virtual void _setLocation(float x, float y);
			virtual void _setBounds(float x, float y, float width, float height);
			
		public:
			LangView() : object(), inrect(), editmenu(), canvasrect() {}
			virtual ~LangView() {}
				
			void setCanvasRect(Objsc::Rect& crect) {canvasrect.set(crect.getX(),crect.getY(),crect.getWidth(),crect.getHeight());}
			virtual void setObject(Objsc::LangObjectPtr objectPtr) {object=objectPtr;}
			Objsc::LangObjectPtr getObject() const { return object; }
			bool hasObject() const { return object; }
			virtual void initSockets(std::tr1::weak_ptr<Objsc::LangView> weakPtr) {}
			virtual void placeSockets() {}
			virtual void initActions(std::tr1::weak_ptr<Objsc::LangView> weakPtr) {}
			
			virtual void link(std::string outlet, std::tr1::shared_ptr<Objsc::LangView> toView, std::string inlet);
			
			void setLocation(float x, float y);
			void setBounds(float x, float y, float width, float height);
			bool onMargin(float x, float y) { return bounds.containsPoint(x,y) && !inrect.containsPoint(x,y); }
			bool onInside(float x, float y) { return inrect.containsPoint(x,y); }
			void mapToStage();
			void mapToScreen();
			virtual void reset() {}
			
			virtual const float getOutletX(const std::string& index) const { return 0.0f; }
			virtual const float getOutletY(const std::string& index) const { return 0.0f; }
			virtual const float getInletX(const std::string& index) const { return 0.0f; }
			virtual const float getInletY(const std::string& index) const { return 0.0f; }
			
			virtual void startEdit() {}
			virtual void endEdit() {}
			
	};
	
	typedef std::tr1::shared_ptr<Objsc::LangView> LangViewPtr;
	typedef std::tr1::weak_ptr<Objsc::LangView> LangViewWeakPtr;
	typedef vector < Objsc::LangViewPtr > LangViewVector;
	

	
}

#endif  //OBJSCR_LANGVIEWS_H

