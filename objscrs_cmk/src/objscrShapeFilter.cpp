#include "objscrShapeFilter.hpp"

// objscrs
#include "objscrTimer.hpp"
#include "objscrConstants.hpp"
#include "objscrSettings.hpp"
#include "objscrFiles.hpp"

#include <highgui.h>

#include <sys/time.h>
#include <stdio.h>

#include <boost/foreach.hpp>

namespace Objsc
{
	
	const string Objsc::ShapeFilter::cmdlabel  = "_shapes_";
	unsigned int dbgImgCount = 1;
	ShapeFilter::ShapeFilter() :
		input(),
		active(false),
		colorImg(),
		grayImage(),
		grayBg(),
		grayDiff(),
		contourFinder(),
		maskimg(),
		domask(false),
		threshold(80),
		inWidth(0),
		inHeight(0),
		pixTotal(0),
		continuous(false),
		hasRef(false),
		minRadius(30),
		maxRadius(2000),
		blur(0)
	{
		// commands
		Objsc::CommandDispatcher::Current()->registerCommandReceiver(Objsc::ShapeFilter::cmdlabel , this);
		// images
		maskimg.setUseTexture(false);
		colorImg.setUseTexture(false);

		// default color channels
//		colorChannels.insert(ofxCvColorImage::ChannelHue);
		colorChannels.insert(ofxCvColorImage::ChannelValue);
//		colorChannels.insert(ofxCvColorImage::ChannelSaturation);

		// settings
		if(Objsc::Settings::Current()->isLoaded())	{
			//printf("ShapeFilter - loading preferences\n");
			minRadius = Objsc::Settings::Current()->getTrackingMinArea();
			maxRadius = Objsc::Settings::Current()->getTrackingMaxArea();
			threshold = Objsc::Settings::Current()->getTrackingThreshold();
			blur = Objsc::Settings::Current()->getTrackingBlur();
			continuous = Objsc::Settings::Current()->getTrackingContinuous();

			/*
			if(Settings::Current()->hasSetting("track_blur"))
				blur = atoi(Settings::Current()->getSetting("track_blur").c_str());
			if(Settings::Current()->hasSetting("track_continuous"))
				continuous = Settings::Current()->getSetting("track_continuous") == std::string("true") ? true : false;
			*/

		}

	}
	
	void ShapeFilter::setInput(Objsc::FrameSourcePtr inputPtr)
	{
		input=inputPtr;
		inWidth=Settings::Current()->getCameraWidth();
		inHeight=Settings::Current()->getCameraHeight();
		pixTotal = inWidth*inHeight;
		colorImg.allocate(inWidth,inHeight);
		grayImage.allocate(inWidth,inHeight);
		grayBg.allocate(inWidth,inHeight);
		grayDiff.allocate(inWidth,inHeight);

        BOOST_FOREACH(const ofxCvColorImage::ColorChannel& cc, colorChannels)
        {
            image[cc].allocate(inWidth,inHeight);
            diff[cc].allocate(inWidth,inHeight);
            image[cc].set(0);
            diff[cc].set(0);
        }

	}

	/**
	  Filter image is responsible to turn the RGB image coming
	  from the capture device into a grayscale image onto which
	  will be based the difference map.
	  */
	void ShapeFilter::filterImage()
	{
        // Get new frame from the grabber
        if(input->hasNewFrame())
            colorImg.setFromPixels(input->getFrame(), inWidth, inHeight);


		if(blur > 0)
            colorImg.blur(blur);

		// TODO make that more efficient
		BOOST_FOREACH(const ofxCvColorImage::ColorChannel& cc, colorChannels)
		{
			colorImg.toChannel(image[cc], cc);
		}

		if(domask)
			applyMask();
	}

	void ShapeFilter::process()
	{
        // Process the frame before we pass it to opencv
		filterImage();

		// Extract difference between the current frame and the background

		grayDiff.set(0);
		BOOST_FOREACH(const ofxCvColorImage::ColorChannel& cc, colorChannels)
        {
			diff[cc].absDiff(background[cc], image[cc]);
			diff[cc].threshold(threshold);
			grayDiff += diff[cc];
		}

//		grayDiff.absDiff(grayBg, grayImage);

//		grayDiff.threshold(threshold);
		/* SY: temporarily disabled
		if((Settings::Current()->hasSetting("track_view_marginalia1"))
			&& (Settings::Current()->getSetting("track_view_marginalia1") != std::string("none")))
		{
			int seconds(atoi(Settings::Current()->getSetting("track_view_marginalia1").c_str()));

			if(diffList.size() < (25 * seconds))
			{
				diffList.push_back(new ofxCvGrayscaleImage(grayDiff));
				return;
			}
			else
			{
				diffList.push_back(new ofxCvGrayscaleImage(grayDiff));
				grayDiff = ofxCvGrayscaleImage(*diffList.front());
				delete diffList.front();
				diffList.pop_front();
            }
        channelNames[] = std::string("");
		}
		*/
		
		// kind of graphical debug output

		if(Settings::Current()->hasSetting("Debug"))
		{
//			timeval pStart;
//			gettimeofday(&pStart, NULL);
			char buffer [250];
			sprintf (buffer, "Hue_%012d.jpg", dbgImgCount);
			cvSaveImage(buffer, diff[ofxCvColorImage::ChannelHue].getCvImage());
			sprintf (buffer, "Sat_%012d.jpg", dbgImgCount);
			cvSaveImage(buffer, diff[ofxCvColorImage::ChannelSaturation].getCvImage());
			sprintf (buffer, "Val_%012d.jpg", dbgImgCount);
			cvSaveImage(buffer, diff[ofxCvColorImage::ChannelValue].getCvImage());
			++dbgImgCount;
		}


		// find contours which are between the size of 20 pixels and 1/3 the w*h pixels.
		// also, find holes is set to true so we will get interior contours as well....
		// old max radius: (inWidth*inHeight)/3
		contourFinder.findContours(grayDiff, minRadius, maxRadius, 100, false);	// find holes

		// replace the source of the diff if asked for
		if(continuous)
//			grayBg = grayImage;
			background = image;

	}
	
	void ShapeFilter::start()
	{
		active=true;
        colorImg.setFromPixels(input->getFrame(), inWidth, inHeight);

		filterImage();
        if(continuous or !hasRef)
        {
			background = image;
		}
	}
    void ShapeFilter::stop()
    {
        active=false;
        hasRef = false;
    }
	
	const ofxCvBlob& ShapeFilter::getShape(int idx)
	{
		return contourFinder.blobs[idx];
	}
	
	
	void ShapeFilter::draw()
	{
        ofSetColor(0xff0000);
		float mWidth(float(ofGetWidth()) / 4);
		float mHeight(float(ofGetHeight() /4));
        grayDiff.draw(0, 0, ofGetWidth(), ofGetHeight());
        contourFinder.draw(0.0f, 0.0f, (float)ofGetWidth(), (float)ofGetHeight());
	}
	
	void ShapeFilter::takeRef()
	{
        filterImage();
		background = image;
		hasRef = true;
	}
	
	void ShapeFilter::startRef()
	{
		//printf("ShapeFilter::startRef\n");
		continuous=true;
		takeRef();
		//~ if(domask)
		//~ applyMask();
		//~ grayBg = grayImage;
	}
	
	void ShapeFilter::stopRef()
	{
		//printf("ShapeFilter::stopRef\n");
		continuous=false;
	}
	
	void ShapeFilter::setColorChannel(const ofxCvColorImage::ColorChannel &cc, bool add)
	{
		if(add)
		{
            colorChannels.insert(cc);
            image[cc].allocate(inWidth,inHeight);
            diff[cc].allocate(inWidth,inHeight);
            background[cc].allocate(inWidth,inHeight);
            image[cc].set(0);
            diff[cc].set(0);
            background[cc].set(0);

            colorImg.toChannel(image[cc], cc);
            background[cc] = image[cc];
		}
		else
		{
			colorChannels.erase(cc);
            image[cc].clear();
            diff[cc].clear();
            background[cc].clear();
            image.erase(cc);
            diff.erase(cc);
            background.erase(cc);
		}


        std::map<ofxCvColorImage::ColorChannel, std::string> channelNames;
        channelNames[ofxCvColorImage::ChannelRed] = std::string("Red");
        channelNames[ofxCvColorImage::ChannelBlue] = std::string("Blue");
        channelNames[ofxCvColorImage::ChannelGreen] = std::string("Green");
        channelNames[ofxCvColorImage::ChannelHue] = std::string("Hue");
        channelNames[ofxCvColorImage::ChannelSaturation] = std::string("Saturation");
        channelNames[ofxCvColorImage::ChannelValue] = std::string("Value");

        std::cerr<<"Channels: ";
		BOOST_FOREACH(const ofxCvColorImage::ColorChannel& cc, colorChannels)
		{
            std::cerr<<channelNames[cc]<<" ";
		}
		std::cerr<<std::endl;

	}
	
	void ShapeFilter::setMaskImage(const string& filename)
	{
		domask=false;
		maskimg.loadImage(Objsc::ProjectFiles::Current()->makeImagePath(filename).c_str());
		maskimg.setImageType(OF_IMAGE_GRAYSCALE);
		if (maskimg.width != inWidth || maskimg.height != inHeight)	{
			maskimg.resize(inWidth, inHeight);
		}
		unsigned char* pix = maskimg.getPixels();
		maskpix = new unsigned char[inWidth*inHeight];
		// NOTE: flip pixels on Y axis
		for (int i = 0; i < inWidth; i++){
			for (int j = 0; j < inHeight; j++){
				maskpix[(inHeight-j-1)*inWidth+i] = pix[j*inWidth+i];
			}
		}
	}
	
	void ShapeFilter::applyMask()
	{
		diffpix = grayImage.getPixels();
		for (int i = 0; i < pixTotal; i++){
			if (!maskpix[i])
				diffpix[i] = 0;
		}
		grayImage.setFromPixels(diffpix,inWidth, inHeight);
	}
	
	void ShapeFilter::useMask()
	{
		domask=true;
		takeRef();
	}
	
	void ShapeFilter::clearMask()
	{
		domask=false;
		takeRef();
	}

	// commands
	void ShapeFilter::doCommand(Objsc::CommandPtr& cmd)
	{
		//std::cout << "ShapeFilter::doCommand: " << cmd->getName() << std::endl;
		
		string cmdname = cmd->getName();
		if(cmdname ==  "proc_on")	{
			start();
		} else if(cmdname ==  "proc_off")	{
			stop();
		} else if(cmdname ==  "ref_on")	{
			startRef();
		} else if(cmdname ==  "ref_off")	{
			stopRef();
		} else if(cmdname ==  "ref_take")	{
			takeRef();
		}
		else if(cmdname == "min_area")
		{
			cmd->getData()->getArg(0, &minRadius);
		}
		else if(cmdname == "max_area")
		{
			cmd->getData()->getArg(0, &maxRadius);
		}
		else if(cmdname == "threshold")
		{
			cmd->getData()->getArg(0, &threshold);
		}
		else if(cmdname == "blur")
		{
			cmd->getData()->getArg(0, &blur);
		}
	}
	
	void ShapeFilter::undoCommand(Objsc::CommandPtr& cmd)
	{
		std::cout << "ShapeFilter::undoCommand: " << cmd->getName() << std::endl;
	}
}

