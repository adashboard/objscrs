


#ifndef  OBJSCR_LANGOBJECTS_H
#define OBJSCR_LANGOBJECTS_H

// Objsc
#include "objscrLangAtoms.hpp"
#include "objscrConstants.hpp"

// STL
#include <iostream>
#include <string>
#include <vector>
#include <list>
#include <map>
#include <algorithm>
using std::string;
using std::vector;
using std::list;
using std::map;
using std::multimap;
using std::find;

// shared_ptr
#include <tr1/memory>



namespace Objsc
{
	
	/////////////////////////////////////
	// FUNCTIONS
	/////////////////////////////////////
	void initLangObjects();
	string makeReceiverName(const string& langname);
	
	/////////////////////////////////////
	// LANG OBJECTS
	/////////////////////////////////////
	class LangObject;
	
	typedef std::tr1::shared_ptr<Objsc::LangObject> LangObjectPtr;
	typedef list < Objsc::LangObjectPtr > LangObjectList;
	typedef LangObjectList::iterator LangObjectListIter;
	
	typedef std::pair < Objsc::LangObjectPtr, std::string > LangAddress;
	typedef vector < Objsc::LangAddress > LangAddressVector;
	
	typedef map < Objsc::LangObjectPtr, int > LangObjectEncodingMap;
	typedef LangObjectEncodingMap::value_type LangObjectEncodingEntry;
	typedef map < int, Objsc::LangObjectPtr > LangObjectDecodingMap;
	typedef LangObjectDecodingMap::value_type LangObjectDecodingEntry;
	
	
	class LangObserver
	{
		public:
			LangObserver() {}
			virtual ~LangObserver() {}
			virtual void objActivated() {}
	};
	
	typedef std::tr1::shared_ptr<Objsc::LangObserver> LangObserverPtr;
	
	
	/////////////////////////////////////
	// LANG CORD
	/////////////////////////////////////
	class LangCord
	{
		private:
			Objsc::LangAddressVector objects;

		public:
			LangCord() : objects() {}
			~LangCord() { objects.clear();}
			
			void link(LangObjectPtr objectPtr, std::string inlet)
			{
				objects.push_back(LangAddress(objectPtr, inlet));
			}
			
			void unlink(LangObjectPtr objectPtr, std::string inlet)
			{
				objects.erase(find(objects.begin(), objects.end(), LangAddress(objectPtr, inlet)));
			}
			
			void unlinkAll(LangObjectPtr objectPtr);
			
			bool connectsTo(Objsc::LangObjectPtr objectPtr);
			
			const Objsc::LangAddressVector& getAddresses() const { return objects; }
			
			const bool hasAddresses() const { return !objects.empty();}
			const int numAddresses() const { return objects.empty()?0:objects.size();}
			
			void clear(){objects.clear();}
			void send(LangAtom* atom);
	};
	
	typedef std::tr1::shared_ptr<Objsc::LangCord> LangCordPtr;
	typedef map < std::string, Objsc::LangCord* > LangCordMap;
	typedef LangCordMap::value_type LangCordEntry;
	
	
	/////////////////////////////////////
	// LANG RECEIVER
	/////////////////////////////////////
	class LangReceiver
	{
		private:
			Objsc::LangObjectList	objects;
			string				label;

		public:
			LangReceiver(const string nlabel="") : objects(), label(nlabel) {}
			~LangReceiver() { clear();}
			
			void add(LangObjectPtr objectPtr);
			void remove(LangObjectPtr objectPtr);
			
			void clear(){objects.clear();}
			void receive(LangAtom* atom);
			
			void setLabel(const string& nlabel) { label = nlabel; }
			const string& getLabel() const { return label; }
			
			const bool empty() const { return objects.empty(); }
			const int size() const { return objects.size(); }
			
			const Objsc::LangObjectList& getObjects() const { return objects; }
			
			static std::tr1::shared_ptr<Objsc::LangReceiver> create(const string nlabel)
			{
				return std::tr1::shared_ptr<Objsc::LangReceiver>(new LangReceiver(nlabel));
			}
	};
	
	typedef std::tr1::shared_ptr<Objsc::LangReceiver> LangReceiverPtr;
	typedef std::map < string, Objsc::LangReceiverPtr > LangReceiverMap;
	typedef LangReceiverMap::value_type LangReceiverEntry;

	typedef std::list<std::string> InOutLetList ;

	/////////////////////////////////////
	// LANG OBJECT
	/////////////////////////////////////
	class LangObject
	{
		protected:
			int 					num_inlets;
			int 					num_outlets;
			InOutLetList	named_inlets;
			InOutLetList	named_outlets;
			LangCordMap 			outlets;
			float				x;
			float				y;
			bool				show_inlets;
			bool				show_outlets;
			string				displaystr;
			int 					min_args;
			int 					max_args;

			// used to reflect identifier in text-based file format
			std::string	ID;

			virtual void receiveBang(std::string inlet);
			virtual void receiveInt(std::string inlet, int value);
			virtual void receiveFloat(std::string inlet, float value);
			virtual void receiveString(std::string inlet, string value);
			virtual void receiveList(std::string inlet, LangList* value);
			virtual void parseValue(LangAtom* atom);
			virtual string parseCmd(LangAtom* atom) {}

		public:
			LangObject();
			virtual ~LangObject() {}
			
			virtual void cleanUp() {clearOutlets();}
			
			virtual bool parseArgs(Objsc::LangList* args) {return true;}
			virtual bool parseMessage(Objsc::LangList* msg) {return true;}
			
			static const string langname;
			virtual const string& getLangName() const { return Objsc::LangObject::langname; }

			virtual void setLabel(string nlabel) {}
			virtual const string& getLabel() const { return Objsc::NULL_STRING; }

			void setIdentifier(const std::string& id) {ID = id;}
			const string& getIdentifier() const { return ID; }
			
			virtual void setReceiverName(string rname) {}
			virtual const string& getReceiverName() const { return getLangName(); }
			virtual bool hasReceiverName() { return false; }
			

			virtual void setInlets(InOutLetList inlts);
			virtual void setOutlets(InOutLetList outlts);
			virtual void setInlets(int size);
			virtual void setOutlets(int size);
			virtual const bool checkInlet(const std::string& in) const;
			
			const Objsc::LangCordMap getOutletsMap() const { return outlets; }
			
			void showInlets() { show_inlets=true;}
			void hideInlets() { show_inlets=false;}
			void showOutlets() { show_outlets=true;}
			void hideOutlets() { show_outlets=false;}
			
			virtual void setDisplayString(const string& str) {displaystr=str;}
			virtual const string& getDisplayString();
			
			const int getNumInlets() const { return show_inlets?num_inlets:0; }
			const int getNumOutlets() const { return show_outlets?num_outlets:0; }
			const int definedOutlets() const;
			const InOutLetList& getInlets() const {return named_inlets;}
			const InOutLetList& getOutlets() const {return named_outlets;}
			
			virtual void link(std::string outlet, LangObjectPtr objectPtr, std::string inlet);
			virtual void unlink(std::string outlet, LangObjectPtr objectPtr, std::string inlet);
			virtual void unlinkAll(Objsc::LangObjectPtr objectPtr);
			bool connectsTo(Objsc::LangObjectPtr objectPtr);
			virtual void clearOutlet(std::string outlet);
			virtual void clearOutlets();

			virtual void receive(std::string inlet, Objsc::LangAtom* atom);
			virtual void send(std::string index, LangAtom* atom);
			virtual void send(int idx, LangAtom* atom);
			
			virtual void activate() {}
			virtual void update(int timeframe) {}

			virtual void setLocation(float nx, float ny);
			float getX() const { return x; }
			float getY() const { return y; }
			
			virtual void setObserver(Objsc::LangObserverPtr obsPtr) {}
			virtual void removeObserver() {}

			virtual const bool hasArgs() const { return false;}
			virtual Objsc::LangList* getArgs() { return NULL; }
			virtual string getArgString() { return Objsc::NULL_STRING; }
	};
	

	class LangObjectCreator
	{
		public:
			virtual ~LangObjectCreator() {}
			virtual Objsc::LangObjectPtr create() const = 0;
	};

	typedef map < string, LangObjectCreator* > LangObjectCreatorMap;
	typedef LangObjectCreatorMap::value_type LangObjectCreatorEntry;


	
	
}

#endif // OBJSCR_LANGOBJECTS_H
