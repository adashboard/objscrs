#include "objscrFiles.hpp"


// openframeworks
#include "ofMain.h"

// system
#include <stdlib.h>
#include <time.h>
#include <algorithm>
#include <cctype>

#include <time.h>



#ifdef TARGET_WIN32
	#include <winbase.h>
	#include <direct.h> // for mkdir - no permissions on windows, just path
	# define OBJSCR_USER "USERNAME"
	# define OBJSCR_HOME "HOMEPATH"
#else
	#include <dirent.h>
	#include <sys/types.h>
	#include <sys/stat.h>
	# define OBJSCR_USER "USER"
	# define OBJSCR_HOME "HOME"
#endif


namespace Objsc
{
	////////////////////////////////////////
	// FUNCTIONS
	////////////////////////////////////////
	bool fileExists(const char* path)
	{
	#ifdef TARGET_WIN32
		//printf("WIN32 fileExists A\n");
		DWORD fileattr;
		fileattr = GetFileAttributes(path);
		if(0xFFFFFFFF== fileattr) {
			//printf("WIN32 fileExists FALSE\n");
			return false;
		} else {
			//printf("WIN32 fileExists TRUE\n");
			return true;
		}
	#else
		//printf("fileExists A\n");
		struct stat fileinfo;
		int statresult = stat(path,&fileinfo);
		if(statresult == 0)	{
			//printf("fileExists TRUE\n");
			return true;
		} else {
			//printf("fileExists FALSE\n");
			return false;
		}
	#endif
	}

	
	bool createDirectory(const string& path)
	{
	#ifdef TARGET_WIN32
		if(!fileExists(path.c_str()))	{
			if(mkdir(path.c_str()) == 0)	{
				printf("%s CREATED\n", path.c_str());
				return true;
			} else {
				printf("%s ERROR - COULD NOT CREATE\n", path.c_str());
				return false;
			}
		}
	#else
		if(!fileExists(path.c_str()))	{
			if(mkdir(path.c_str(), S_IRWXU|S_IRGRP|S_IXGRP|S_IROTH|S_IXOTH) == 0)	{
				printf("%s CREATED\n", path.c_str());
				return true;
			} else {
				printf("%s ERROR - COULD NOT CREATE\n", path.c_str());
				return false;
			}
		}
	#endif
	}

	
	////////////////////////////////////////
	// PROJECT FILES
	////////////////////////////////////////
	ProjectFiles::ProjectFiles() : rootdir(), pathdelim()
	{
	}
	
	string ProjectFiles::formatName(const string& filepath)
	{
		if(filepath.empty())	{
			return "";
		}
		string fmt(filepath);
		replace(fmt.begin(), fmt.end(), ' ', '_');
		std::transform(fmt.begin(), fmt.end(), fmt.begin(), static_cast < int(*)(int) > (tolower));
		return fmt;
	}
	
	string ProjectFiles::formatScoreName(const string& filepath)
	{
		if(filepath.empty())	{
			return "";
		}
		string fmt(formatName(filepath));
		if(fmt.find_last_of('.') == -1 ||
				( fmt.substr(fmt.find_last_of('.')) != ".xml" && fmt.substr(fmt.find_last_of('.')) != ".txt") )
		{
			fmt.append(Objsc::FILE_EXT);
		}
		return fmt;
	}
	
	string ProjectFiles::screenGrabName(const string& prefix)
	{
		char timestr[32];
		char filestr[64];
		timespec now;
		clock_gettime(CLOCK_REALTIME, &now);
		strftime(timestr, 18, "%Y%m%d_%H%M%S", localtime(&now.tv_sec));
        sprintf(filestr, "%s_%s_%09ld.png", prefix.c_str(), timestr, now.tv_nsec);
		return string(filestr);
	}
	
	string ProjectFiles::makeTimestampedImagePath(const string& prefix)
	{
		return makeProjectPath(screenGrabName(prefix), "images");
	}
	
	string ProjectFiles::makeDocFilePath(const string& filename)
	{
		if(filename.empty())	{
			return "file://" + makeRootPath("index.html", "doc");
		} else {
			return "file://" + validateDocFile(makeRootPath(filename, "doc") + ".html");
		}
	}
	
	string ProjectFiles::makeObjectDocFilePath(const string& objname)
	{
		if(objname.empty())	{
			return "file://" + makeRootPath("index.html", "doc");
		} else {
			return "file://" + validateDocFile(makeRootPath(objname, "doc/objects") + ".html");
		}
	}
	
	string ProjectFiles::validateDocFile(const string& filepath)
	{
		if(fileExists(filepath.c_str()))	{
			return filepath;
		} else {
			return makeRootPath("404.html", "doc");
		}
	}
	
	void ProjectFiles::initPaths()
	{
		char pathstr[256];
		#ifdef TARGET_WIN32
			pathdelim = "\\";
			sprintf(pathstr, "%s\\%s\\%s", getenv("HOMEDRIVE"), getenv(OBJSCR_HOME), Objsc::ROOTDIRNAME.c_str());
		#else
			pathdelim = "/";
			sprintf(pathstr, "%s/%s", getenv(OBJSCR_HOME), Objsc::ROOTDIRNAME.c_str());
			//sprintf(pathstr, "/home/simon/%s", Objsc::ROOTDIRNAME.c_str());
			//sprintf(pathstr, "/home/constant/%s", Objsc::ROOTDIRNAME.c_str());
		#endif
		rootdir.assign(pathstr);
		createDirectory(rootdir);
	}
	
	void ProjectFiles::makeProjectDirectories(const string& projdir)
	{
		createDirectory(projdir);
		createDirectory(projdir + pathdelim + "scores");
		//createDirectory(projdir + pathdelim + "patches");
		createDirectory(projdir + pathdelim + "images");
		createDirectory(projdir + pathdelim + "sound");
		createDirectory(projdir + pathdelim + "video");
		createDirectory(projdir + pathdelim + "grabs");
		createDirectory(projdir + pathdelim + "admin");
	}
	
	void ProjectFiles::setCurrentProject(const string& projectname)
	{
		projectdir = makePath(formatName(projectname));
		makeProjectDirectories(projectdir);
	}
	
	
	////////////////////////////////////////
	// FILE RESOURCE
	////////////////////////////////////////
	FileResource::FileResource() :
		dirhandle(),
		path(),
		filenames(),
		filepaths(),
		num_files(0) {}
	
	FileResource::FileResource(const string& pstr) :
		dirhandle(),
		path(pstr),
		filenames(),
		filepaths(),
		num_files(0)
	{
		refresh();
	}
	
	
	FileResource::~FileResource()
	{
		filenames.clear();
		filepaths.clear();
	}
	
	void FileResource::open(const string& filepath)
	{
		//printf("FileResource::open <%s>\n", filepath.c_str());
		path.assign(filepath);
		refresh();
	}
	
	void FileResource::refresh()
	{
		ofDisableDataPath();
		filenames.clear();
		filepaths.clear();
		num_files = dirhandle.listDir(path);
		for(int i = 0; i < num_files; i++){
			filepaths.push_back(dirhandle.getPath(i));
			filenames.push_back(dirhandle.getName(i));
		}
		sort(filepaths.begin(),filepaths.end());
		sort(filenames.begin(),filenames.end());
	}
	
}

