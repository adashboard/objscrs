#ifndef  OBJSCR_GEOM_H
#define OBJSCR_GEOM_H

// c
#include <math.h>
#include <stdio.h>


#include "ofTypes.h"


// DEFINES
#ifndef MAX
	#define MAX(x,y) (((x) > (y)) ? (x) : (y))
#endif
#ifndef MIN
	#define MIN(x,y) (((x) < (y)) ? (x) : (y))
#endif


namespace Objsc
{
////////////////////////////////
// FUNCTIONS
////////////////////////////////
float distance(float x1, float y1, float x2, float y2);
float distance(const ofPoint& p1, const ofPoint& p2);
float screenToStage(float scrPos, float scrDim);
float stageToScreen(float stagePos, float scrDim);
inline int toInt(float f){return fmod(f,1) > 0.5f?ceil(f):floor(f);};


	////////////////////////////////
	// DECLARATION
	////////////////////////////////
	//class Circle;
	
	////////////////////////////////
	// RECT
	////////////////////////////////
	class Rect
	{
		protected:
			float x;
			float y;
			float minX;
			float minY;
			float maxX;
			float maxY;
			float width;
			float height;
			float ctrX;
			float ctrY;
			
		public:
			Rect() :
				x(0.0f),
				y(0.0f),
				minX(0.0f),
				minY(0.0f),
				width(0.0f),
				height(0.0f),
				maxX(0.0f),
				maxY(0.0f),
				ctrX(0.0f),
				ctrY(0.0f){}
			Rect(float nx, float ny, float nwidth, float nheight) :
				x(nx),
				y(ny),
				minX(nx),
				minY(ny),
				width(nwidth),
				height(nheight),
				maxX(minX+width),
				maxY(minY+height),
				ctrX(minX+(width/2)),
				ctrY(minY+(height/2)){}
			Rect(const Rect &copy)
			{
				x = copy.x;
				y = copy.y;
				minX = x;
				minY = y;
				width = copy.width;
				height = copy.height;
				maxX = minX+width;
				maxY = minY+height;
				ctrX = minX+(width/2);
				ctrY = minY+(height/2);
			}
			Rect(const ofRectangle &oRect)
			{
				x = oRect.x;
				y = oRect.y;
				minX = x;
				minY = y;
				width = oRect.width;
				height = oRect.height;
				maxX = minX+width;
				maxY = minY+height;
				ctrX = minX+(width/2);
				ctrY = minY+(height/2);
			}
			Rect& operator=(const Rect &copy)
			{
				this->x = copy.x;
				this->y = copy.y;
				this->minX = this->x;
				this->minY = this->y;
				this->width = copy.width;
				this->height = copy.height;
				this->maxX = this->minX+this->width;
				this->maxY = this->minY+this->height;
				this->ctrX = this->minX+(this->width/2);
				this->ctrY = this->minY+(this->height/2);
				return *this;
			}
			~Rect() {}
			
			ofRectangle toOFRect() const
			{
				return ofRectangle(x,y,width,height);
			}

			bool containsPoint(float x, float y) const
			{
				if(x < minX) return false;
				if(x > maxX) return false;
				if(y < minY) return false;
				if(y > maxY) return false;
				return true;
			}
			
			bool intersects(const Rect& other)
			{
				if(other.getLeft() > maxX) return false;
				if(other.getRight() < minX) return false;
				if(other.getTop() > maxY) return false;
				if(other.getBottom() < minY) return false;
				return true;
			}
			
			bool matches(const Rect& other)
			{
				if(other.getX() != minX) return false;
				if(other.getY() != minY) return false;
				if(other.getLeft() != maxX) return false;
				if(other.getBottom() != maxY) return false;
				return true;
			}
			
			bool matchesAsInt(const Rect& other)
			{
				if(toInt(other.getX()) != toInt(minX)) return false;
				if(toInt(other.getY()) != toInt(minY)) return false;
				if(toInt(other.getLeft()) != toInt(maxX)) return false;
				if(toInt(other.getBottom()) != toInt(maxY)) return false;
				return true;
			}
			
			//~ bool intersects(const Circle& other)
			//~ {
				//~ if(other.getX() < minX-other.getRadius()) return false;
				//~ if(other.getX() < maxX+other.getRadius()) return false;
				//~ if(other.getY() < minY-other.getRadius()) return false;
				//~ if(other.getY() < maxY+other.getRadius()) return false;
				//~ return true;
			//~ }
			
			const float getX() const { return minX; }
			const float getY() const { return minY; }
			const float getWidth() const { return width; }
			const float getHeight() const { return height; }
			const float getLeft() const { return minX; }
			const float getRight() const { return maxX; }
			const float getTop() const { return minY; }
			const float getBottom() const { return maxY; }
			const float getCentreX() const { return ctrX; }
			const float getCentreY() const { return ctrY; }
			const float getMinEdgeSqr() const { return (width > height) ? width/2 : height / 2; }
			const float getMaxEdgeSqr() const { return  pow(minX - ctrX,2) + pow(minY - ctrY,2); }
			const float getArea() const { return width*height; }
			
			void set(float nx, float ny, float nwidth, float nheight)
			{
				x = nx;
				y = ny;
				minX = nx;
				minY = ny;
				width = nwidth;
				height = nheight;
				maxX = minX+width;
				maxY = minY+height;
				ctrX = minX+(width/2);
				ctrY = minY+(height/2);
			}
			void setX(float nx)
			{ 
				x = nx;
				minX = nx; 
				maxX = minX+width;
				ctrX = minX+(width/2);
			}
			void setY(float ny)
			{ 
				y=ny;
				minY = ny;
				maxY = minY+height;
				ctrY = minY+(height/2);
			}
			void setLocation(float nx, float ny)
			{
				x = nx;
				y = ny;
				minX = nx;
				minY = ny;
				maxX = minX+width;
				maxY = minY+height;
				ctrX = minX+(width/2);
				ctrY = minY+(height/2);
			}
			void setWidth(float nwidth)
			{
				width = nwidth;
				maxX = minX+width;
				ctrX = minX+(width/2);
			}
			void growWidth(float nwidth)
			{
				setWidth(width + nwidth);
			}
			void shrinkWidth(float nwidth)
			{
				setWidth(MAX(0,width-nwidth));
			}
			void setHeight(float nheight)
			{
				height = nheight;
				maxY = minY+height;
				ctrY = minY+(height/2);
			}
			void growHeight(float nheight)
			{
				setHeight(height + nheight);
			}
			void shrinkHeight(float nheight)
			{
				setHeight(MAX(0, height-nheight));
			}
			void setDimensions(float nwidth, float nheight)
			{
				width = nwidth;
				maxX = minX+width;
				ctrX = minX+(width/2);
				height = nheight;
				maxY = minY+height;
				ctrY = minY+(height/2);
			}
			void setLeft(float nx)
			{ 
				x = nx;
				minX = nx; 
				maxX = minX+width;
				ctrX = minX+(width/2);
			}
			void setRight(float nx)
			{ 
				maxX = nx; 
				minX = maxX-width;
				x=minX;
				ctrX = minX+(width/2);
			}
			void setTop(float ny)
			{ 
				y=ny;
				minY = ny;
				maxY = minY+height;
				ctrY = minY+(height/2);
			}
			void setBottom(float ny)
			{ 
				maxY = ny;
				minY = maxY-height;
				y=minY;
				ctrY = minY+(height/2);
			}
			void setCentreX(float nx)  
			{ 	
				ctrX = nx;
				minX = ctrX-(width/2);
				x=minX;
				maxX = minX+width;
			}
			void setCentreY(float ny) 
			{ 	
				ctrY = ny;
				minY = ctrY-(height/2);
				y=minY;
				maxY = minY+height;
			}
			void setCentre(float nx, float ny)
			{
				ctrX = nx;
				ctrY = ny;
				minX = ctrX-(width/2);
				minY = ctrY-(height/2);
				maxX = minX+width;
				maxY = minY+height;
				x=minX;
				y=minY;
			}
			
			void move(float dx, float dy)
			{
				minX += dx;
				minY += dy;
				maxX = minX+width;
				maxY = minY+height;
				ctrX = minX+(width/2);
				ctrY = minY+(height/2);
				x=minX;
				y=minY;
			}
			void moveCentre(float dx, float dy)
			{
				ctrX += dx;
				ctrY += dy;
				minX = ctrX-(width/2);
				minY = ctrY-(height/2);
				maxX = minX+width;
				maxY = minY+height;
				x=minX;
				y=minY;
			}
			void stretchSides(float n, float e, float s, float w)
			{
				x+=w;
				y+=n;
				minX=x;
				minY=y;
				maxX+=e;
				maxY+=s;
				width = maxX-minX;
				height=maxY-minY;
				ctrX = minX+(width/2);
				ctrY = minY+(height/2);
			}
			
			void sizeAround(Rect& rect)
			{
				if(rect.getLeft() < minX)	{
					setX(rect.getLeft());
				}
				if(rect.getTop() < minY)	{
					setY(rect.getTop());
				}
				if(rect.getRight() < maxX)	{
					setRight(rect.getRight());
				}
				if(rect.getBottom() < maxY)	{
					setBottom(rect.getBottom());
				}
			}
			
			void sizeAround(const Rect& rect)
			{
				if(rect.getLeft() < minX)	{
					setX(rect.getLeft());
				}
				if(rect.getTop() < minY)	{
					setY(rect.getTop());
				}
				if(rect.getRight() < maxX)	{
					setRight(rect.getRight());
				}
				if(rect.getBottom() < maxY)	{
					setBottom(rect.getBottom());
				}
			}

			Rect scaled(float sx, float sy) const;
			
			float toUnitWidth(float coord) {return (coord-minX)/width;}
			float toUnitHeight(float coord) {return (coord-minY)/height;}
			float fromUnitWidth(float coord) { return (coord*width)+minX;}
			float fromUnitHeight(float coord) { return (coord*height)+minY;}
			void toUnitRect(Rect& other, Rect& unit){unit.set(toUnitWidth(other.getX()), toUnitHeight(other.getY()), toUnitWidth(other.getWidth()), toUnitHeight(other.getHeight()));}
			void fromUnitRect(Rect& other, Rect& scr){scr.set(fromUnitWidth(other.getX()), fromUnitHeight(other.getY()), fromUnitWidth(other.getWidth()), fromUnitHeight(other.getHeight()));}
			
			//~ void sizeAround(Circle& circle)
			//~ {
				//~ float n=0.0f;
				//~ float e=0.0f;
				//~ float s=0.0f;
				//~ float w=0.0f;
				//~ if(rect.getLeft() < minX)	{
					//~ w= rect.getLeft()-minX;
				//~ }
				//~ if(rect.getTop() < minY)	{
					//~ n = rect.getTop()-minY;
				//~ }
				//~ if(rect.getRight() > maxX)	{
					//~ e=rect.getRight()-maxX;
				//~ }
				//~ if(rect.getBottom() > maxY)	{
					//~ s=rect.getBottom()-maxY;
				//~ }
				//~ stretchSides(n,e,s,w);
			//~ }

			//~ void sizeAround(const Circle& circle)
			//~ {
				//~ float n=0.0f;
				//~ float e=0.0f;
				//~ float s=0.0f;
				//~ float w=0.0f;
				//~ if(rect.getLeft() < minX)	{
					//~ w= rect.getLeft()-minX;
				//~ }
				//~ if(rect.getTop() < minY)	{
					//~ n = rect.getTop()-minY;
				//~ }
				//~ if(rect.getRight() > maxX)	{
					//~ e=rect.getRight()-maxX;
				//~ }
				//~ if(rect.getBottom() > maxY)	{
					//~ s=rect.getBottom()-maxY;
				//~ }
				//~ stretchSides(n,e,s,w);
			//~ }
			
			void print(){printf("Rect %f,%f,%f,%f [%f,%f]\n", x,y,maxX,maxY,width,height);}
	};


	////////////////////////////////
	// CIRCLE
	////////////////////////////////
	class Circle
	{
		private:
			float ctrX;
			float ctrY;
			float radius;
			float radiusSqr;
		
		public:
			Circle()
			{
				ctrX = 0.0f;
				ctrY = 0.0f;
				radius = 0.0f;
				radiusSqr = 0.0f;
			}
			Circle(float nx, float ny, float nradius)
			{
				ctrX = nx;
				ctrY = ny;
				radius = nradius;
				radiusSqr = radius*radius;
			}
			~Circle() {}
			
			inline bool containsPoint(float x, float y) { return (pow(x - ctrX,2) + pow(y - ctrY,2)) < radiusSqr; }
			
			bool intersects(const Rect& other)
			{
				if(ctrX < other.getLeft()-radius) return false;
				if(ctrX < other.getRight()+radius) return false;
				if(ctrY < other.getTop()-radius) return false;
				if(ctrY < other.getBottom()+radius) return false;
				return true;
			}
			
			
			bool intersects(const Circle& other) { return (pow(other.getX() - ctrX,2) + pow(other.getY() - ctrY,2)) < radiusSqr; }
			
			const float getX() const { return ctrX; }
			const float getY() const { return ctrY; }
			const float getRadius() const { return radius; }
			const float getLeft() const { return ctrX-radius; }
			const float getRight() const { return ctrX+radius; }
			const float getTop() const { return ctrY-radius; }
			const float getBottom() const { return ctrY+radius; }
			
			void set(float nx, float ny, float nradius)
			{
				ctrX = nx;
				ctrY = ny;
				radius = nradius;
				radiusSqr = radius*radius;
			}
			void setX(float nx) { ctrX = nx; }
			void setY(float ny) { ctrY = ny; }
			void setCentre(float nx, float ny)
			{
				ctrX = nx;
				ctrY = ny;
			}
			void setRadius(float nradius)
			{ 
				radius = nradius;
				radiusSqr = radius*radius; 
			}
			
			void move(float dx, float dy)
			{
				ctrX += dx;
				ctrY += dy;
			}
			
			const float distanceFromCentre(float pX, float pY) const { return sqrt(pow(ctrX - pX, 2) + pow(ctrY - pY, 2));}
			const float percFromCentre(float pX, float pY) const { return sqrt(pow(ctrX - pX, 2) + pow(ctrY - pY, 2)) / radius;}
			
			void print(){printf("Circle %f,%f,%f\n", ctrX,ctrY,radius);}
			
	};
	
	////////////////////////////////
	// ASPECT RECT
	////////////////////////////////
	class AspectRect : public Rect
	{
		private:
			Rect parent;
			float aspect;
		
		public:
			AspectRect()  : parent() {}
			~AspectRect() {}
				
			void setParent(float nx, float ny, float nwidth, float nheight) 
			{
				parent.set(nx,ny,nwidth,nheight);
				fitToParent();
			}
			Rect& getParent() { return parent; }
			
			void set(float nx, float ny, float nwidth, float nheight)
			{
				Rect::set(nx,ny,nwidth,nheight);
				setAspect(width, height);
			}
			
			void setWidth(float nwidth)
			{
				Rect::setWidth(nwidth);
				height = width*aspect;
				maxY = minY+height;
				ctrY = minY+(height/2);
			}
			void setHeight(float nheight)
			{
				Rect::setHeight(nheight);
				width = height/aspect;
				maxX = minX+width;
				ctrX = minX+(width/2);
			}
			void setDimensions(float nwidth, float nheight)
			{
				Rect::setDimensions(nwidth,nheight);
				setAspect(width, height);
			}
			
			void setAspect(float ax, float ay) { aspect = ax/ay; }
			const float getAspect() const { return aspect; }
			
			void fitToParent()
			{
				if(width >= height)	{
					setWidth(parent.getWidth());
				} else {
					setHeight(parent.getHeight());
				}
				setCentre(parent.getCentreX(),parent.getCentreY());
			}
	};
	
	
}

#endif // OBJSCR_GEOM_H
