#ifndef OBJSC_SOUND
#define OBJSC_SOUND

// Objsc
#include "singleton.h"

// stk

// F... STK having PI as a float variable
#ifdef PI
#define GRRRR_PI PI
#undef PI
#endif

#ifdef TWO_PI
#define GRRRR_TWO_PI TWO_PI
#undef TWO_PI
#endif

//#include "FileWvIn.h" // NOTE: STK dependencies removed

// STL
#include <string>
#include <vector>
#include <map>
using std::string;
using std::vector;
using std::map;


// shared_ptr
#include <tr1/memory>

namespace Objsc
{
	////////////////////////////////////////
	// FUNCTIONS
	////////////////////////////////////////
	string toSoundPath(const string& filename);
	
	////////////////////////////////////////
	// DATA TYPES
	////////////////////////////////////////
	enum soundplaystates
	{
		SND_STOPPED,
		SND_PLAYING,
		SND_LOOPINGCOUNT,
		SND_LOOPINGINFINITE
	};

	enum objsc_soundevent
	{
		SNDSTART,
		SNDENDED,
		SNDNEWLOOP
	};


	struct SoundEvent {
		//int time;
		objsc_soundevent type;
		unsigned int source;
		float pos;
		int loop;
	};

	
	////////////////////////////////////////
	// SOUND LISTENER
	////////////////////////////////////////
	class SoundListener
	{
		public:
			virtual ~SoundListener() {}
			virtual void receiveSoundEvent(SoundEvent& event) {}
	};
	
	typedef std::tr1::shared_ptr<Objsc::SoundListener> SoundListenerPtr;
	typedef vector < Objsc::SoundListenerPtr > SoundListenerVec;
	
	
	////////////////////////////////////////
	// SOUND
	////////////////////////////////////////
	class Sound
	{
		public:
			virtual ~Sound() {}
			virtual void update() {}
			virtual float tickL() { 0.0f; }
			virtual float tickR() { 0.0f; }
			virtual void setSpeed(float value) {}
			virtual const float getSpeed() const { return 0.0f; }
			virtual void setPan(float value){}
			virtual const float getPan() const { return 0.0f; }
			virtual void setVolume(float value){}
			virtual const float getVol() const { return 0.0f; }
			virtual void setLoops(int value){}
			virtual const int getLoops() const { return 1; }
			virtual const int getCurrentLoop() const { return 0; }
			virtual const bool isPlaying() const { return false; }
			virtual const float getAmp() { return 0.0f; }
	};
	
	typedef std::tr1::shared_ptr<Objsc::Sound> SoundPtr;
	typedef vector < Objsc::SoundPtr > SoundVec;
	typedef map < string, Objsc::SoundPtr > SoundMap;
	typedef SoundMap::value_type SoundEntry;
	
	////////////////////////////////////////
	// SOUND PLAYER
	////////////////////////////////////////
	class SoundPlayer : public Sound
	{
		protected:
			//stk::FileWvIn  		snd;
			float			speed;
			float			vol;
			float			panL;
			float			panR;
			int				loops;
			int				loopcount;
			soundplaystates	state;
			SoundListenerPtr	listener;
			
		public:
			SoundPlayer();
			~SoundPlayer();
			
			void update();
			
			void play();
			void stop();
			void cut();
			void end();
			void nextLoop();
			float tickL() { 0.0f;} //return state != SND_STOPPED?snd.tick()*vol*panL:0.0f; }
			float tickR() { 0.0f;} // return state != SND_STOPPED?snd.lastOut()*vol*panR:0.0f; }
			
			//~ void setID(unsigned int nid) { id=nid; }
			//~ const unsigned int getID() const { return id; }
			void setSpeed(float value);
			const float getSpeed() const { return speed; }
			void setPan(float value);
			const float getPan() const { return panR; }
			void setVolume(float value);
			const float getVolume() const { return vol; }
			void setLoops(int value);
			const int getLoops() const { return loops; }
			const int getCurrentLoop() const { return loopcount; }
			const bool getIsPlaying() const { return state != SND_STOPPED; }
			const float getAmp() { 0.0f;} // return state != SND_STOPPED?snd.lastOut()*vol*panR:0.0f; }
			
			void loadSound(const string& filepath);
			void unloadSound();
			
			void setListener(Objsc::SoundListenerPtr listenerPtr) { listener=listenerPtr; }
			// sound events
			void postSoundEvent(objsc_soundevent eventType);
			
			static std::tr1::shared_ptr<Objsc::SoundPlayer> create()
			{
				return std::tr1::shared_ptr<Objsc::SoundPlayer>(new SoundPlayer());
			}
	};
	
	typedef std::tr1::shared_ptr<Objsc::SoundPlayer> SoundPlayerPtr;
	typedef vector < Objsc::SoundPlayerPtr > SoundPlayerVec;
	
}

#endif
