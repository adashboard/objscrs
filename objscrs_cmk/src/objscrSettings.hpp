#ifndef  OBJSCR_SETTINGS_H
#define OBJSCR_SETTINGS_H

// objscrs
#include "singleton.h"

// xml
#include "tinyxml.h"

#include <string>
#include <map>

namespace Objsc
{
	class Settings : public Singleton<Settings>
	{
		private:
			bool	loaded;
			// camera settings
			int		cameraID;
			int		cameraLib;
			bool	cameraOn;
			int		camWidth;
			int		camHeight;
			// display settings
			int		scrWidth;
			int		scrHeight;
			// tracking settings
			int 		trackingMinArea;
			int 		trackingMaxArea;
			int 		trackingThreshold;
			bool	trackingContinuous;
			int 		trackingBlur;
			// network settings
			int		oscport;

			std::map<std::string, std::string> aSetting;
			
			// methods
			void decodeCamera(TiXmlElement *parentNode);
			void decodeDisplay(TiXmlElement *parentNode);
			void decodeBodyPrefs(TiXmlElement *parentNode);
			void decodeTracking(TiXmlElement *parentNode);
			void decodeNetwork(TiXmlElement *parentNode);
			void decodeSettings(TiXmlElement *parentNode);
			
			void encodeCamera(TiXmlElement *parentNode);
			void encodeDisplay(TiXmlElement *parentNode);
			void encodeBodyPrefs(TiXmlElement *parentNode);
			void encodeTracking(TiXmlElement *parentNode);
			void encodeNetwork(TiXmlElement *parentNode);
			void encodeSettings(TiXmlElement *parentNode);
			
			void settingsNotFound(bool generateSettingsFile);
			
		public:
			Settings();
			~Settings() {}
				
			bool load(const std::string &filename = std::string());
			void print();
			void save();
			void reset();
				
			const bool isLoaded() const { return loaded; }
			const int getCameraID() const { return cameraID; }
			const int getCameraLib() const { return cameraLib; }
			const bool getCameraOn() const { return cameraOn; }
			const int getCameraWidth() const { return camWidth; }
			const int getCameraHeight() const { return camHeight; }
			const int getDisplayWidth() const { return scrWidth; }
			const int getDisplayHeight() const { return scrHeight; }
			const int getTrackingMinArea() const { return trackingMinArea; }
			const int getTrackingMaxArea() const { return trackingMaxArea; }
			const int getTrackingThreshold() const { return trackingThreshold; }
			const bool getTrackingContinuous() const { return trackingContinuous; }
			const int getTrackingBlur() const { return trackingBlur; }
			const int getOSCPort() const { return oscport; }
			std::string getSetting(const std::string& key) const;
			bool hasSetting(const std::string& key) const;
	};
}

#endif // OBJSCR_SETTINGS_H
