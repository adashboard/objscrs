#ifndef  OBJSCR_PLAYER_H
#define OBJSCR_PLAYER_H

// objscrs
#include "singleton.h"
#include "objscrGeom.hpp"

// shared_ptr
#include <tr1/memory>




namespace Objsc
{
	class Player : public Singleton<Player>
	{
		public:
			Player();
			~Player() {}
	};
}


#endif // OBJSCR_PLAYER_H
