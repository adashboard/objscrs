#include "objscrSound.hpp"

// Objsc
#include "objscrFiles.hpp"

// OF
#include "ofMain.h"

namespace Objsc
{
	////////////////////////////////////////
	// FUNCTIONS
	////////////////////////////////////////
	string toSoundPath(const string& filename)
	{
		return ofToDataPath(string("audio/") + filename);
	}
	
	////////////////////////////////////////
	// SOUND PLAYER
	////////////////////////////////////////
	SoundPlayer::SoundPlayer() :
		//snd(),
		speed(1.0f),
		vol(0.0f),
		panL(0.5f),
		panR(0.5f),
		loops(1),
		loopcount(0),
		state(SND_STOPPED),
		listener()
	{
		
	}
	
	SoundPlayer::~SoundPlayer()
	{
		//snd.reset();
		//snd.closeFile();
	}
	
	void SoundPlayer::update()
	{
		/*
		switch(state)	{
			case SND_PLAYING:
				
				if(//snd.isFinished())	{
					end();
				}
				break;
			case SND_LOOPINGCOUNT:
				//printf("SND_LOOPINGCOUNT\n");
				if(//snd.isFinished())	{
					nextLoop();
				}
				break;
			case SND_LOOPINGINFINITE:
				if(//snd.isFinished())	{
					nextLoop();
				}
				break;
		}
		*/
	}
	
	void SoundPlayer::play()
	{
		//printf("SoundPlayer::play %d\n", loops);
		//snd.reset();
		//loops=nloops;
		loopcount=0;
		if(loops==1)	{
			state=SND_PLAYING;
		} else if(loops==0)	{
			state=SND_LOOPINGINFINITE;
		} else {
			state=SND_LOOPINGCOUNT;
		}
		postSoundEvent(SNDSTART);
	}
	
	void SoundPlayer::stop()
	{
		state=SND_PLAYING;
		loops=1;
	}
	
	void SoundPlayer::cut()
	{
		//snd.reset();
		state=SND_STOPPED;
		postSoundEvent(SNDENDED);
	}
	
	void SoundPlayer::end()
	{
		state=SND_STOPPED;
		postSoundEvent(SNDENDED);
	}
	
	void SoundPlayer::nextLoop()
	{
		//printf("SoundPlayer::nextLoop A\n");
		loopcount++;
		if(loops == 0 || loopcount < loops)	{
			//printf("SoundPlayer::nextLoop B\n");
			//snd.reset();
			postSoundEvent(SNDNEWLOOP);
		} else {
			end();
		}
	}
	
	void SoundPlayer::setSpeed(float value)
	{
		speed = value;
		//snd.setRate(speed);
	}
	void SoundPlayer::setPan(float value)
	{
		panL = 1.0-value;
		panR = value;
		//printf("SoundPlayer::setPan L: %f R: %f\n",  panL, panR);
	}
	void SoundPlayer::setVolume(float value)
	{
		vol = value;
	}
	void SoundPlayer::setLoops(int value)
	{
		loops = value;
		loopcount = 0;
		//printf("SoundPlayer::setLoops %d %d\n",  loops, loopcount);
	}
	
	
	void SoundPlayer::loadSound(const string& filepath)
	{
		ofDisableDataPath();
		//snd.closeFile();
		//snd.openFile(Objsc::ProjectFiles::Current()->makeSoundPath(filepath));
		state=SND_STOPPED;
	}
	
	void SoundPlayer::unloadSound()
	{
		//snd.closeFile();
		state=SND_STOPPED;
	}
	
	
	// sound events
	void SoundPlayer::postSoundEvent(objsc_soundevent eventType)
	{
		//printf("SoundPlayer::postSoundEvent A\n");
		if(listener)	{
			//printf("SoundPlayer::postSoundEvent B\n");
			SoundEvent event;
			//event.source = id;
			event.type = eventType;
			event.loop = loopcount;
			listener->receiveSoundEvent(event);
		}
	}
}

