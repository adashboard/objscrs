#ifndef OBJSCR_FRAMESOURCEEDITOR_H
#define OBJSCR_FRAMESOURCEEDITOR_H


// objscrs
#include "objscrConstants.hpp"
#include "objscrGUI.hpp"
#include "objscrCommands.hpp"
#include "objscrDialogue.hpp"
#include "objscrFrames.hpp"
#include "objscrFrameView.hpp"

// STL
#include <string>
using std::string;


namespace Objsc
{
	
	////////////////////////////////////////
	// FRAME SOURCE EDITOR
	////////////////////////////////////////
	class FrameSourceEditor : public GuiBase, public CommandReceiver
	{
		private:
			Objsc::ContextMenuPtr 			objmenu;
			Objsc::ContextMenuPtr 			patchmenu;
			Objsc::DialoguePtr				patchdialogue;
			Objsc::FrameSourcePtr			frameSource;
			Objsc::FrameViewPtr			frameView;
			string						stylename;
			string						cmdlabel;
			int							scrX;
			int							scrY;
			
			void initMenus();
		
		public:
			FrameSourceEditor();
			~FrameSourceEditor() {}
			
			void update();
			void draw();
			void enable() 
			{ 
				active = true; 
				startLiveInput();
			}
			void disable() 
			{ 
				active = false; 
				stopLiveInput();
			}
			// frames
			void initLiveInput();
			void startLiveInput();
			void stopLiveInput();
			Objsc::FrameSourcePtr getFrameSource() { return frameSource;}
			// commands
			void doCommand(Objsc::CommandPtr& cmd);
			void undoCommand(Objsc::CommandPtr& cmd);
			// events
			void keyPressed(int key);
			void keyReleased(int key);
			void mouseMoved(int x, int y );
			void mouseDragged(int x, int y, int button);
			void mousePressed(int x, int y, int button);
			void mouseReleased(int x, int y, int button);
			void windowResized(int w, int h);
			
			void autosize();
			
			static std::tr1::shared_ptr<Objsc::FrameSourceEditor> create()
			{
				return std::tr1::shared_ptr<Objsc::FrameSourceEditor>(new FrameSourceEditor());
			}

	};
	
	
	typedef std::tr1::shared_ptr<Objsc::FrameSourceEditor> FrameSourceEditorPtr;
	
}

#endif  //OBJSCR_FRAMESOURCEEDITOR_H

