#ifndef OBJSCR_LANGEDITOR_H
#define OBJSCR_LANGEDITOR_H


// objscrs
#include "objscrConstants.hpp"
#include "objscrGUI.hpp"
#include "objscrLangViews.hpp"
#include "objscrScore.hpp"
#include "objscrCommands.hpp"
#include "objscrDialogue.hpp"

#include "View/LangPatchCanvas.h"

// STL
#include <string>
using std::string;


namespace Objsc
{
	
	////////////////////////////////////////
	// FUNCTIONS
	////////////////////////////////////////
	Objsc::LangViewPtr viewFromObject(Objsc::LangObjectPtr object);
	
	////////////////////////////////////////
	// PATCH EDITOR
	////////////////////////////////////////
	class LangPatchEditor : public GuiBase, public CommandReceiver
	{
		private:
			Objsc::ScorePtr				score;
			Objsc::LangPatchCanvasPtr		patchcanvas;
			Objsc::ContextMenuPtr 			objmenu;
			Objsc::ContextMenuPtr 			patchmenu;
			Objsc::DialoguePtr				patchdialogue;
			string						stylename;
			int							scrX;
			int							scrY;
			
			void initMenus();
		
		public:
			LangPatchEditor();
			~LangPatchEditor() {}
				
			void setScore(Objsc::ScorePtr scorePtr) {score=scorePtr;}
			void setPatch(Objsc::LangPatchPtr patchPtr);
			
			void update();
			void draw();
			void disable();
			void toggleBG();
			// commands
			void doCommand(Objsc::CommandPtr& cmd);
			void undoCommand(Objsc::CommandPtr& cmd);
			// events
			void keyPressed(int key);
			void keyReleased(int key);
			void mouseMoved(int x, int y );
			void mouseDragged(int x, int y, int button);
			void mousePressed(int x, int y, int button);
			void mouseReleased(int x, int y, int button);
			void windowResized(int w, int h);
			
			void autosize();
			
			static std::tr1::shared_ptr<Objsc::LangPatchEditor> create()
			{
				return std::tr1::shared_ptr<Objsc::LangPatchEditor>(new LangPatchEditor());
			}
			
			static const string	cmdlabel;

	};
	
	
	typedef std::tr1::shared_ptr<Objsc::LangPatchEditor> LangPatchEditorPtr;
	
}

#endif  //OBJSCR_LANGEDITOR_H

