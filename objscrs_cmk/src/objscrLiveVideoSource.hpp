#ifndef  OBJSCR_LIVEVIDEOSOURCE_H
#define OBJSCR_LIVEVIDEOSOURCE_H

#include "objscrFrames.hpp"
#include "objscrSettings.hpp"

// shared_ptr
#include <tr1/memory>

namespace Objsc
{
	////////////////////////////////////////
	// FRAME SOURCE
	////////////////////////////////////////
	class LiveVideoSource : public FrameSource
	{
		protected:
			bool				active;
			bool				hascam;
			ofVideoGrabber		grabber;
		
		public:
			LiveVideoSource():
				active(false),
				hascam(false),
				grabber()
			{
				grabber.listDevices();
				grabber.setDeviceID(Objsc::Settings::Current()->getCameraID());
			}
				
			virtual ~LiveVideoSource() {}
				
			void setFrameSize(int nwidth, int nheight)
			{
				width=nwidth;
				height=nheight;
				hascam=grabber.initGrabber(width,height);
			}
			unsigned char* getFrame() { return grabber.getPixels();}
			bool hasNewFrame() {return hascam?grabber.isFrameNew():false;}
			void start() { active=hascam?true:false; }
			void stop() { active=false; }
			void update() { if(active) {grabber.grabFrame();}}
			
			static Objsc::FrameSourcePtr create()
			{
				return Objsc::FrameSourcePtr(new LiveVideoSource());
			}
	};
}

#endif // OBJSCR_LIVEVIDEOSOURCE_H
