#include "objscrApp.hpp"

// objscrs
#include "objscrConstants.hpp"
#include "objscrGUIRoot.hpp"
#include "objscrIDBroker.hpp"
#include "objscrZones.hpp"
#include "objscrZoneViews.hpp"
#include "objscrFiles.hpp"
#include "objscrArchive.hpp"
#include "objscrArchiveLang.hpp"
#include "objscrDevices.hpp"

// tests
#include "objscrPackets.hpp"
#include "objscrBody.hpp"
#include "objscrLang.hpp"
#include "objscrLangExtensions.hpp"


// openframeworks
#include "ofMain.h"

// STL
#include <iostream>
#include <string>
#include <exception>
using std::string;
using std::exception;


void _quitApp()
{
	exit(0);
}

namespace Objsc
{
	void App::init()
	{
		// commands
		Objsc::CommandDispatcher::Current()->registerCommandReceiver("_app_", this);
		
		// time
		timer = Objsc::Timer::Current();
		
		// devices
		Objsc::DeviceManager::Current()->scanVideoDevices();
		
		// files
		//Objsc::ProjectFiles::Current()->initPaths();
		//Objsc::ProjectFiles::Current()->setCurrentProject("root");
		
		
		// score
		//std::cout << "App::init newScore" << std::endl;
		score = Objsc::Score::create();
		
		// lang
		//std::cout << "App::init setupLang" << std::endl;
		setupLang();
		
		// archive
		initLangArchivers();
		
		// OSC
		//std::cout << "App::init oscComms" << std::endl;
		oscComms = Objsc::OSCComms::Current();
		initOSCComms();
		
		// IDs
		//std::cout << "App::init IDBroker" << std::endl;
		Objsc::IDBroker::Current()->reset();
		// zones
		//std::cout << "App::init initZones" << std::endl;
		initZones();
		initZoneViews();
		
		// stage
		//std::cout << "App::init stage" << std::endl;
		stage = Objsc::StagePtr(Objsc::Stage::Current());
		stage->setScreenCoords(0,0,ofGetWidth(),ofGetHeight());
		
		input = new char[2];
		input[0] = ' ';
		input[1] = '\0';
	}
	
	void App::quit()
	{
		// clean up first ...
		_quitApp();
	}
	
	
	// score
	Objsc::ScorePtr App::newScore(const string& scorename)
	{
		//std::cout << "App::newScore" << std::endl;
		Objsc::GUIRoot::Current()->removingScore();
		score = Objsc::Score::create(scorename);
		return score;
	}
	
	void App::setScore(Objsc::ScorePtr newscore)
	{
		Objsc::GUIRoot::Current()->removingScore();
		score = newscore;
	}
	
	void App::loadScore(const string& scorename)
	{
		Objsc::GUIRoot::Current()->removingScore();
		score->stop();
		Objsc::TimedActionPtr ta = timer->createAction(200, 1);
		Objsc::CommandPtr cmd = Objsc::CommandDispatcher::createCommand("_app_", "load_score");
		cmd->getData()->addKwArg("score",string(scorename));
		ta->setCommand(cmd);
	}
				
	void App::update(int time, int timeframe)
	{
		timer->update(timeframe);
		stage->update(time, timeframe);
		score->update(timeframe);
		oscComms->update();
	}
	
	
	void App::setupStage(int x, int y, int width, int height)
	{
		stage->setScreenCoords(x,y,width,height);
		//stage->initLiveInput();
	}
	
	
	// keys
	void App::handleControlKey(int key)
	{
		if(score)	{
			input[0] = key;
			score->sendToCurrentZoneMap("_ctrlkey_", Objsc::LangAtomPtr(new LangString(string(input))));
		}
	}
	
	// patch lang
	void App::setupLang()
	{
//		Objsc::initLangObjects();
//		Objsc::initLangZoneObjects();
//		Objsc::initLangCommsObjects();
//		Objsc::initLangMathObjects();
//		Objsc::initLangMedia();
//		Objsc::initLangTimeObjects();
//		Objsc::initLangGraphicObjects();
	}
	
	// commands
	void App::doCommand(Objsc::CommandPtr& cmd)
	{
		//std::cout << "App::doCommand: " << cmd->getName() << std::endl;
		
		string cmdname = cmd->getName();
		if(cmdname ==  "quit")	{
			quit();
		} else if(cmdname ==  "load_score")	{
			string scorename = "";
			cmd->getData()->getKwArg("score",&scorename);
			if(!scorename.empty())	{
				Objsc::Archivist::Current()->open(scorename);
			}
		}
	}
	
	void App::undoCommand(Objsc::CommandPtr& cmd)
	{
		std::cout << "App::undoCommand: " << cmd->getName() << std::endl;
	}
}

