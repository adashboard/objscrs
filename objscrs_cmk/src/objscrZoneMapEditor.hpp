#ifndef OBJSCR_ZONEMAPEDITOR_H
#define OBJSCR_ZONEMAPEDITOR_H


// objscrs
#include "objscrConstants.hpp"
#include "objscrGUI.hpp"
#include "objscrScore.hpp"
#include "objscrCommands.hpp"
#include "objscrZoneViews.hpp"
#include "objscrDialogue.hpp"

// STL
#include <string>
using std::string;


namespace Objsc
{
	
	////////////////////////////////////////
	// ZONE MAP EDITOR
	////////////////////////////////////////
	class ZoneMapEditor : public Objsc::GuiBase, public Objsc::CommandReceiver
	{
		private:
			Objsc::ContextMenuPtr 			zonemenu;
			Objsc::ZoneMapViewPtr 			zonecanvas;
			Objsc::DialoguePtr				zmapdialogue;
			string						stylename;
			int							scrX;
			int							scrY;
			
			void initMenus();
			void initDialogues();
		
		public:
			ZoneMapEditor();
			~ZoneMapEditor() {}
				
			void setZoneMap(const string& mapname="");
			void createZoneMap(const string& mapname="");
			void removeZoneMap(const string& mapname);
			
			void update();
			void draw();
			// commands
			void doCommand(Objsc::CommandPtr& cmd);
			void undoCommand(Objsc::CommandPtr& cmd);
			// events
			void keyPressed(int key);
			void keyReleased(int key);
			void mouseMoved(int x, int y );
			void mouseDragged(int x, int y, int button);
			void mousePressed(int x, int y, int button);
			void mouseReleased(int x, int y, int button);
			void windowResized(int w, int h);
			
			void autosize();
			void viewMapsList();
			
			static std::tr1::shared_ptr<Objsc::ZoneMapEditor> create()
			{
				return std::tr1::shared_ptr<Objsc::ZoneMapEditor>(new ZoneMapEditor());
			}
			static const string	cmdlabel;
	};
	
	
	typedef std::tr1::shared_ptr<Objsc::ZoneMapEditor> ZoneMapEditorPtr;
	
}

#endif  //OBJSCR_ZONEMAPEDITOR_H

