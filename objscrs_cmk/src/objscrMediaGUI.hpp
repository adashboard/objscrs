#ifndef OBJSCR_MEDIAGUI_H
#define OBJSCR_MEDIAGUI_H


// objscrs
#include "objscrConstants.hpp"
#include "objscrGUI.hpp"
#include "objscrCommands.hpp"
#include "objscrGeom.hpp"
#include "objscrSound.hpp"

// openframeworks
#include "ofMain.h"

// STL
#include <string>
using std::string;

// shared_ptr
#include <tr1/memory>


namespace Objsc
{
	
	static const int MEDIAWIDTH = 120;
	static const int MEDIAHEIGHT = 90;
	static const string PLAYICON = "icons/playicon.png";
	static const string STOPICON = "icons/stopicon.png";
	
	////////////////////////////////////////
	// MEDIA PREVIEW
	////////////////////////////////////////
	class MediaPreview : public Objsc::Widget
	{
		protected:
			Objsc::TextField	text;
			Objsc::Rect		mrect;
			bool			loaded;
		
			virtual void resizeMedia() {}
		
		public:
			MediaPreview();
			virtual ~MediaPreview() {}
			// media
			virtual void setMedia(const string& filepath, const string& filename) {}
			virtual void playMedia() {}
			virtual void stopMedia() {}
			// geom
			void setLocation(float x, float y);
			void setDimensions(float width, float height);
			void setBounds(float x, float y, float width, float height);
			// runtime
			virtual void draw(){}
	};
	
	typedef std::tr1::shared_ptr<Objsc::MediaPreview> MediaPreviewPtr;
	
	////////////////////////////////////////
	// IMAGE PREVIEW
	////////////////////////////////////////
	class ImagePreview : public Objsc::MediaPreview
	{
		protected:
			ofImage			image;
		
			void resizeMedia();
		
		public:
			ImagePreview();
			~ImagePreview() {}
			// media
			void setMedia(const string& filepath, const string& filename);
			// runtime
			void draw();
				
			static Objsc::MediaPreviewPtr create()
			{
				return Objsc::MediaPreviewPtr(new ImagePreview());
			}
	};
	
	////////////////////////////////////////
	// SOUND PREVIEW
	////////////////////////////////////////
	class SoundPreview : public Objsc::MediaPreview
	{
		protected:
			//ofSoundPlayer		snd;
			Objsc::SoundPlayer	snd;
			ofImage			playicon;
			ofImage			stopicon;
			bool			playing;
		
			void resizeMedia();
		
		public:
			SoundPreview();
			~SoundPreview();
			// media
			void setMedia(const string& filepath, const string& filename);
			void playMedia();
			void stopMedia();
			// events
			void mousePressed(int x, int y, int button);
			// runtime
			void draw();
		
			static Objsc::MediaPreviewPtr create()
			{
				return Objsc::MediaPreviewPtr(new SoundPreview());
			}
	};
	
	
	////////////////////////////////////////
	// VIDEO PREVIEW
	////////////////////////////////////////
	class VideoPreview : public Objsc::MediaPreview
	{
		protected:
			ofVideoPlayer		video;
			bool			playing;
		
			void resizeMedia();
		
		public:
			VideoPreview();
			~VideoPreview();
			// media
			void setMedia(const string& filepath, const string& filename);
			void playMedia();
			void stopMedia();
			// events
			void mousePressed(int x, int y, int button);
			// runtime
			void draw();
		
			static Objsc::MediaPreviewPtr create()
			{
				return Objsc::MediaPreviewPtr(new VideoPreview());
			}
	};
	
}

#endif  //OBJSCR_MEDIAGUI_H

