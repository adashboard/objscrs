#ifndef  OBJSCR_FILES_H
#define OBJSCR_FILES_H


// objscrs
#include "singleton.h"
#include "objscrCommands.hpp"
#include "objscrDataTypes.hpp"
#include "objscrDirlist.hpp"

// openframeworks
//#include "ofxDirList.h"

// shared_ptr
#include <tr1/memory>

// STL
#include <string>
#include <map>
using std::string;
using std::map;

namespace Objsc
{
	
	// files
	static const string ROOTDIRNAME = "OBJSCRS";
	static const string FILE_EXT = ".xml";
	
	////////////////////////////////////////
	// PROJECT FILES
	////////////////////////////////////////
	class ProjectFiles : public Singleton<ProjectFiles>
	{
		private:
			string	rootdir;
			string	pathdelim;
			string	projectdir;
		
			string formatName(const string& filepath);
			string formatScoreName(const string& filepath);
			string screenGrabName(const string& prefix="objscrs");
			void makeProjectDirectories(const string& projectname);
			string validateDocFile(const string& filepath);
			string makeProjectPath(const string& filename, const string& dirname) { return projectdir + pathdelim + dirname + pathdelim + filename;}
			string makeRootPath(const string& filename, const string& dirname) { return rootdir + pathdelim + dirname + pathdelim + filename;}
			
		public:
			ProjectFiles();
			~ProjectFiles() {}
				
			void initPaths();
			const string& getProjectDir() const { return rootdir; }
			string makePath(const string& filepath) { return rootdir + pathdelim + filepath; }
			string makeAdminPath(const string& filename) { return makeProjectPath(formatScoreName(filename), "admin");}
			string makeScorePath(const string& filename="") { return makeProjectPath(formatScoreName(filename), "scores");}
			string makePatchPath(const string& filename="") { return makeProjectPath(formatName(filename), "patches");}
			//~ string makeImagePath(const string& filename="") { return makeProjectPath(formatName(filename), "images");}
			//~ string makeSoundPath(const string& filename="") { return makeProjectPath(formatName(filename), "sound");}
			//~ string makeVideoPath(const string& filename="") { return makeProjectPath(formatName(filename), "video");}
			string makeImagePath(const string& filename="") { return makeProjectPath(filename, "images");}
			string makeSoundPath(const string& filename="") { return makeProjectPath(filename, "sound");}
			string makeVideoPath(const string& filename="") { return makeProjectPath(filename, "video");}
			string makeScreenGrabPath() { return makeProjectPath(screenGrabName(), "grabs");}
			string makeTimestampedImagePath(const string& prefix="objscrs");
			string getSettingsPath() { return makeProjectPath("settings.xml", "admin");}
			string makeDocFilePath(const string& filename="");
			string makeObjectDocFilePath(const string& objname="");
			void setCurrentProject(const string& projectname); 
	};
	
	////////////////////////////////////////
	// FILE RESOURCE
	////////////////////////////////////////
	class FileResource
	{
		protected:
			//ofxDirList				dirhandle;
			Objsc::DirList			dirhandle;
			string 				path;
			Objsc::StringVector		filenames;
			Objsc::StringVector		filepaths;
			int					num_files;
		
		public:
			FileResource();
			FileResource(const string& pstr);
			~FileResource();
			
			void refresh();
			void open(const string& filepath);
			
			const bool empty() const { return num_files == 0; }
			const int size() const { return num_files; }
			
			void setPath(const string& pstr) { path=pstr; }
			const string& getPath() const { return path; }
			
			void includeFileType(const string& type) {dirhandle.allowExt(type); }
			void clearFileTypes() {dirhandle.reset();}
			
			const Objsc::StringVector& getFilenames() const { return filenames; }
			const Objsc::StringVector& getFilepaths() const { return filepaths; }
			
			static std::tr1::shared_ptr<Objsc::FileResource> create(const string& pstr)
			{
				return std::tr1::shared_ptr<Objsc::FileResource>(new FileResource(pstr));
			}
	};
	
	typedef std::tr1::shared_ptr<Objsc::FileResource> FileResourcePtr;
	typedef map <string, Objsc::FileResourcePtr> FileResourceMap;
	
	
}

#endif // OBJSCR_FILES_H
