/***************************************************************************
 *   Copyright (C)  2011 by Pierre Marchand                   *
 *   pierre@oep-h.com   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#include "objscrFontconfig.hpp"

#include <fontconfig/fontconfig.h>
#include <cstring>

#include <iostream>

//#define DEBUG_FONTS

namespace Objsc
{

	FontConfig::FontConfig()
	{
		FcInit();
	}

	FontConfig::~FontConfig()
	{
		FcFini();
	}

	std::string FontConfig::GetFont(const std::string &fn) const
	{
		const FcChar8 * grrr = (FcChar8 *)fn.c_str();
		std::string ret;
		// borrowed from mplayer source code
		FcPattern *fc_pattern;
		FcPattern *fc_pattern2;
		FcChar8 *s;
		int face_index;
		FcBool scalable;
		FcResult result;

		fc_pattern = FcNameParse(grrr);
		FcConfigSubstitute(0, fc_pattern, FcMatchPattern);
		FcDefaultSubstitute(fc_pattern);
		fc_pattern2 = fc_pattern;
		fc_pattern = FcFontMatch(0, fc_pattern, &result);
		if (fc_pattern)
		{
			FcPatternDestroy(fc_pattern2);
			FcPatternGetBool(fc_pattern, FC_SCALABLE, 0, &scalable);
//			if (scalable != FcTrue)
//			{
//				FcPatternDestroy(fc_pattern);
//				fc_pattern = FcNameParse("sans-serif");
//				FcConfigSubstitute(0, fc_pattern, FcMatchPattern);
//				FcDefaultSubstitute(fc_pattern);
//				fc_pattern2 = fc_pattern;
//				fc_pattern = FcFontMatch(0, fc_pattern, 0);
//				FcPatternDestroy(fc_pattern2);
//			}
//			// s doesn't need to be freed according to fontconfig docs
			FcPatternGetString(fc_pattern, FC_FILE, 0, &s);
			FcPatternGetInteger(fc_pattern, FC_INDEX, 0, &face_index);
			FcPatternDestroy(fc_pattern);
			ret = std::string((char*)s);
		}
		#ifdef DEBUG_FONTS
		std::cerr<<"FontConfig::GetFont: "<< fn << " => " << ret << std::endl;
		#endif
		return ret;
	}
}


