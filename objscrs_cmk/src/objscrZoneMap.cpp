#include "objscrZoneMap.hpp"


// STL
#include <algorithm>
#include <exception>
using std::exception;


namespace Objsc
{
	
	string createZoneName(unsigned int id)
	{
		char cstr[64];
		sprintf(cstr, "zone_%u", id);
		return string(cstr);
	}
	
	////////////////////////////////////////
	// ZONE MAP
	////////////////////////////////////////
	ZoneMap::ZoneMap() : name(), zones(), zonecount(0) {}
	
	Objsc::ZonePtr ZoneMap::createZone(const string& zonetype, const string& zonename)
	{
		if(Objsc::ZoneFactory::Current()->hasCreator(zonetype))	{
			zones.push_back(Objsc::ZoneFactory::Current()->create(zonetype));
			if(zonename.empty())	{
				if(zonecount < zones.size())	{
					zonecount= zones.size();
				}
				string zname = createZoneName(zonecount++);
				zones.back()->setName(zname);
			} else {
				zones.back()->setName(zonename);
			}
			return zones.back();
		} else {
			throw exception();
		}
	}
	
	void ZoneMap::removeZone(Objsc::ZonePtr zone)
	{
		zones.erase(find(zones.begin(), zones.end(), zone));
	}
	
	Objsc::ZonePtr ZoneMap::getZone(const string& zonename)
	{
		for(Objsc::ZonePtrList::iterator zone = zones.begin();  zone != zones.end(); ++zone)	{
			if((*zone)->getName() == zonename)	{
				return (*zone);
			}
		}
		return Objsc::ZonePtr();
	}
	
	
	void ZoneMap::updateBodies(const Objsc::BodyPtrList&  bodies)
	{
		for(Objsc::ZonePtrList::iterator zone = zones.begin();  zone != zones.end(); ++zone)	{
			(*zone)->updateBodies(bodies);
		}
	}
	
	void ZoneMap::update(int timeElapsed)
	{
		for(Objsc::ZonePtrList::iterator zone = zones.begin();  zone != zones.end(); ++zone)	{
			(*zone)->update(timeElapsed);
		}
	}
	
	void ZoneMap::onload()
	{
		for(Objsc::ZonePtrList::iterator zone = zones.begin();  zone != zones.end(); ++zone)	{
			(*zone)->onload();
		}
	}
	
	void ZoneMap::sendToAllZones(const string& label, Objsc::LangAtomPtr atom)
	{
		//printf("ZoneMap::sendToAllZones <%s>\n", label.c_str());
		for(Objsc::ZonePtrList::iterator zone = zones.begin();  zone != zones.end(); ++zone)	{
			(*zone)->receive(label,atom);
		}
	}
	
}

