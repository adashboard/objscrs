#include "objscrStage.hpp"

// objscrs
#include "objscrApp.hpp"


// STL
#include <iostream>
#include <algorithm>

namespace Objsc
{
	
	
	////////////////////////////////////////
	// DECLARATIONS
	////////////////////////////////////////
	const string Objsc::Stage::cmdlabel   = "_stage_";
	
	////////////////////////////////////////
	// STAGE
	////////////////////////////////////////
	Stage::Stage() : 
		screenRect(), 
		stageRect(0.0f,0.0f, STAGE_WIDTH, STAGE_HEIGHT),
		shapeFilter(),
		bodyFilter(),
		zonemaps(),
		stageNumbers(),
		active(false),
		preview(false)
	{
		shapeFilter = Objsc::ShapeFilter::create();
		bodyFilter = Objsc::BodyFilter::create();
		bodyFilter->setShapeFilter(shapeFilter);
		setZoneMap(Objsc::App::Current()->getScore()->getZoneMap());
	}
	
	void Stage::setFrameSource(Objsc::FrameSourcePtr sourcePtr)
	{
		shapeFilter->setInput(sourcePtr);
	}
	
	void Stage::start()
	{
		shapeFilter->start();
		active = true;
	}
	
	void Stage::stop()
	{
		shapeFilter->stop();
		bodyFilter->clear();
		active = false;
	}
	
	void Stage::update(int time, int timeframe)
	{
		if(active)	{
			shapeFilter->update();
			bodyFilter->update();
			updateBodies();
		}
	}
	
	void Stage::draw()
	{
		/*if(active && preview)*/	{
			shapeFilter->draw();
		}
	}
	
	void Stage::setScreenCoords(int x, int y, int width, int height)
	{
		screenRect.set(x,y,width,height);
		screenRect.print();
	}

	// zones
	void Stage::addZoneMap(Objsc::ZoneMapPtr zonemap)
	{
		zonemaps.insert(zonemap);
	}
	
	void Stage::setZoneMap(Objsc::ZoneMapPtr zonemap)
	{
		//printf("Stage::setZoneMap <%s>\n", zonemap->getName().c_str());
		zonemaps.clear();
		zonemaps.insert(zonemap);
		zonemap->onload();
	}
	
	void Stage::removeZoneMap(Objsc::ZoneMapPtr zonemap)
	{
		zonemaps.erase(zonemap);
	}
	
	void Stage::clearZoneMaps()
	{
		zonemaps.clear();
	}
	
	void Stage::doLoadActions()
	{
		//printf("Stage::doLoadActions\n");
		for(Objsc::ZoneMapSet::iterator zmap = zonemaps.begin(); zmap != zonemaps.end(); ++zmap)	{
			(*zmap)->onload();
		}
	}
	
	// bodies
	void Stage::updateBodies()
	{
		if(bodyFilter->hasBodies())	{
			for(Objsc::ZoneMapSet::iterator zmap = zonemaps.begin(); zmap != zonemaps.end(); ++zmap)	{
				(*zmap)->updateBodies(bodyFilter->getBodies());
			}
		}
	}
	
	// stage numbers
	unsigned int Stage::requestStageNumber()
	{
		unsigned int num;
		if(stageNumbers.empty())	{
			unsigned int num = 1;
			stageNumbers.push_back(num);
			return num;
		} else {
			sort(stageNumbers.begin(),stageNumbers.end());
			unsigned int num = 1;
			for(Objsc::StageNumVec::iterator snum = stageNumbers.begin(); snum != stageNumbers.end(); ++snum)	{
				if((*snum) > num)	{
					//printf("break\n");
					break;
				}
				++num;
			}
			//printf("requestStageNumber num: %u\n", num);
			stageNumbers.push_back(num);
			return num;
		}
	}
	
	void Stage::releaseStageNumber(unsigned int value)
	{
		stageNumbers.erase(find(stageNumbers.begin(), stageNumbers.end(), value));
	}
	
	void Stage::clearStageNumbers()
	{
		stageNumbers.clear();
	}
	
}
