#include "objscrBody.hpp"

// objscrs
#include "objscrConstants.hpp"
#include "objscrStageView.hpp"
#include "objscrStage.hpp"
#include "objscrSettings.hpp"

#include <sys/time.h>
#include <algorithm>


namespace Objsc
{
	
	////////////////////////////////////////
	// DECLARATIONS
	////////////////////////////////////////
	int Objsc::Body::unbornAge = Objsc::DEFAULT_BODY_UNBORN_AGE;
	int Objsc::Body::unbornHits = Objsc::DEFAULT_BODY_UNBORN_HITS;
	int Objsc::Body::unbornLastHit = Objsc::DEFAULT_BODY_UNBORN_LAST_HIT;
	int Objsc::Body::becomingAge = Objsc::DEFAULT_BODY_BECOMING_AGE;
	int Objsc::Body::becomingHits = Objsc::DEFAULT_BODY_BECOMING_HITS;
	int Objsc::Body::becomingLastHit = Objsc::DEFAULT_BODY_BECOMING_LAST_HIT;
	int Objsc::Body::presentLastHit = Objsc::DEFAULT_BODY_PRESENT_LAST_HIT;
	int Objsc::Body::absentLastHitMin = Objsc::DEFAULT_BODY_ABSENT_LAST_HIT_MIN;
	int Objsc::Body::absentLastHitMax = Objsc::DEFAULT_BODY_ABSENT_LAST_HIT_MAX;
	float Objsc::Body::maxDist = Objsc::DEFAULT_BODY_MAXDISTANCE;
	int Objsc::Body::velSteps = Objsc::DEFAULT_BODY_VELSTEPS;
	float Objsc::Body::velDist = Objsc::DEFAULT_BODY_VELDISTANCE;
	
	
	////////////////////////////////////////
	// BODY
	////////////////////////////////////////
	Body::Body() :
				uid(0),
				stageNum(0),
				age(0),
				lasthit(0),
				hits(0),
				bestShape(-1),
				bestdiffarea(0.0f),
				bestdist(0.0f),
				state(UNBORN),
				screenRect(),
				stageRect(),
				shapeRect(),
				screenLoc(),
				stageLoc(),
				velocity(0.0f),
				velIdx(0),
				velSum(0.0f),
				bearing(0.0f),
				area(0.0f),
				label(),
				hitTime(0)
	{
		actionPresent = Objsc::CommandDispatcher::createCommand(Objsc::StageView::cmdlabel , "body_present");
		actionDead = Objsc::CommandDispatcher::createCommand(Objsc::StageView::cmdlabel , "body_dead");
		enterState(UNBORN);
		resetVelocity();
		/*
		if(Settings::Current()->hasSetting(MAX_DIST))
			maxDist = atof(Settings::Current()->getSetting(MAX_DIST).c_str());
		*/
	}
	
	
	void Body::newFrame()
	{
		bestShape = -1;
		bestdiffarea = Objsc::MAXFLOAT;
		bestdist = Objsc::MAXFLOAT;
	}
	
    bool Body::ShapeDistComparator::operator()(const Body::ShapeDist& a, const Body::ShapeDist& b)
    {
        // Can be improved, weight both diffs and give a more general diff.
        // Furthermore, get color info from the image to be even more accurate
        if(a.posDiff < b.posDiff)
            return true;
        else if(a.posDiff > b.posDiff)
            return false;
        else // equality of floats...
        {
            if(a.areaDiff < b.areaDiff)
                return true;
            else if(a.areaDiff >= b.areaDiff)
                return false;
        }
    }

	void Body::compareShape(const ofxCvBlob& shape, int shapenum)
	{
		bool within = shapeRect.containsPoint(shape.centroid.x,shape.centroid.y);
		float diffarea = fabs(area - shape.area);
		float dist = Objsc::distance(shape.centroid, shapeLoc);
		if(within)
		{
			if(dist < (maxDist + (velDist*velocity)) && dist <= bestdist)	{
				bestdist = dist;
                bestShape = shapenum;
			}
		}
//		printf("[%u] dist: %f bestdist: %f bestShape: %i\n", uid, dist,bestdist, bestShape);
	}
	
	//~ bool Body::matchesArea(Objsc::BodyPtr other)
	//~ {
		//~ return stageRect.matches(other->getStageRect());
	//~ }
	
	const Body::DistanceMap& Body::getDistances(const Objsc::ShapeFilterPtr &shapes)
	{
		int shapenum = 0;
        distances.clear();
        std::vector<ShapeDist> tmp;
		for(ofxCvBlobIterator shapeItem(shapes->shapesBegin());
		    shapeItem != shapes->shapesEnd();
		    ++shapeItem)
		{
            if(shapeItem->area > 0.0)
			{
				float dist( Objsc::distance(shapeItem->centroid, shapeLoc) );
                float diffarea = fabs(area - shapeItem->area);
//				if(dist < (maxDist + (velDist*velocity)))
//				{
//					distances.insert(pair<float,int>(dist,shapenum));
//				}
                tmp.push_back(ShapeDist(shapenum, diffarea, dist));
				++shapenum;
			}
		}
        std::sort(tmp.begin(), tmp.end(), shapeDistComparator);
        for(std::vector<ShapeDist>::const_iterator ti(tmp.begin()); ti != tmp.end(); ti++)
            distances.push_back(Distance(ti->idx , ti->posDiff));
		return distances;
	}
	
	void Body::update()
	{
		switch(state)	{
			case UNBORN:
				if(age > Objsc::Body::unbornAge && hits > Objsc::Body::unbornHits) {
					enterState(BECOMING);
				//} else if(age-lasthit > 40)	{
				} else if(age-lasthit > Objsc::Body::unbornLastHit)	{
					enterState(DEAD);
				}
				break;
			case BECOMING:
				//if(age > 0 && hits > 12)	{
				if(age > Objsc::Body::becomingAge && hits > Objsc::Body::becomingHits)	{
					enterState(PRESENT);
				//} else if(age-lasthit > 40)	{
				} else if(age-lasthit > Objsc::Body::becomingLastHit)	{
					enterState(DEAD);
				}
				break;
			case PRESENT:
				//if(age-lasthit > 12)
				if(age-lasthit > Objsc::Body::presentLastHit)
					enterState(ABSENT);
				break;
			case ABSENT:
				//if(age-lasthit > 40)	{
				if(age-lasthit > Objsc::Body::absentLastHitMax)	{
					enterState(DEAD);
				//} else if(age-lasthit < 12)	{
				} else if(age-lasthit < Objsc::Body::absentLastHitMin)	{
					enterState(PRESENT);
				} 
				break;
			case DEAD:
				
				break;
		}
		++age;
	}
	
	void Body::enterState(bodystate newstate)
	{
		switch(newstate)	{
			case UNBORN:
				//printf("[%u] UNBORN\n", uid);
				break;
			case BECOMING:
				//printf("[%u] BECOMING\n", uid);
				break;
			case PRESENT:
				//printf("[%u] PRESENT\n", uid);
				stageNum = Objsc::Stage::Current()->requestStageNumber();
				//printf("stageNum: %u", stageNum);
				Objsc::CommandDispatcher::Current()->doCommand(actionPresent);
				// create view
				break;
			case ABSENT:
				//printf("[%u] ABSENT\n", uid);
				break;
			case DEAD:
				//printf("[%u] DEAD age: %d\n", uid, age);
				if(stageNum > 0)	{
					Objsc::Stage::Current()->releaseStageNumber(stageNum);
					stageNum = 0;
				}
				Objsc::CommandDispatcher::Current()->doCommand(actionDead);
				// remove view
				// remove body
				break;
		}
		state=newstate;
	}
	
	void Body::initActions(std::tr1::weak_ptr<Objsc::Body> weakPtr)
	{
		actionPresent->getData()->addArg(weakPtr);
		actionDead->getData()->addArg(weakPtr);
	}
	
	void Body::updateShape(const ofxCvBlob& shape)
	{
		shapeRect = Rect(shape.boundingRect);

		ofPoint storePos(shapeLoc);

		area = shape.area;
		shapeLoc.x = shape.centroid.x;
		shapeLoc.y = shape.centroid.y;
		hits++;
		lasthit=age;
		pts = shape.pts;
		
		//if(state == PRESENT || state == ABSENT)	{
		float HScale(Objsc::STAGE_WIDTH / float(Settings::Current()->getCameraWidth()));
		float VScale(Objsc::STAGE_HEIGHT / float(Settings::Current()->getCameraHeight()));
		stageRect = Rect(shape.boundingRect).scaled(HScale, VScale);
		stageLoc.x = shapeLoc.x * HScale;
		stageLoc.y = shapeLoc.y * VScale;
		//}


		// velocity
		timeval th;
		int lastHitTime(hitTime);
		gettimeofday(&th, NULL);
		hitTime = (th.tv_sec * 1000000) + th.tv_usec;
		//velocity = float(distance(storePos,shapeLoc)) / float(hitTime - lastHitTime) * 1000000;
		updateVelocity(float(distance(storePos,shapeLoc)) / float(hitTime - lastHitTime) * 1000000);
	}
    bool Body::matchesArea(tr1::shared_ptr<Objsc::Body> other)
    {
        return stageRect.matches(other->getStageRect());
    }

	void Body::resetVelocity()
	{
		//printf("Body::resetVelocity A\n");
		velocity = 0.0f;
		velIdx = 0;
		//printf("Body::resetVelocity B\n");
		velarray = new float[velSteps];
		//printf("Body::resetVelocity C\n");
		for(int i = 0; i < velSteps; i++)	{
			velarray[i] = 0.0f;
		}
		//printf("Body::resetVelocity DONE\n");
	}
	
	void Body::updateVelocity(float newVel)
	{
		//printf("Body::updateVelocity A\n");
		velIdx = ++velIdx % velSteps;
		//printf("Body::updateVelocity velIdx: %d\n", velIdx);
		velarray[velIdx] = newVel;
		//printf("Body::updateVelocity newVel: %f\n", newVel);
		velSum = 0.0f;
		//printf("Body::updateVelocity velSum A: %f\n", velSum);
		for(int i = 0; i < velSteps; i++)	{
			//printf("Body::updateVelocity velSum B: %f\n",  velarray[i]);
			velSum += velarray[i];
		}
		//printf("Body::updateVelocity velSum C: %f\n", velSum);
		velocity = velSum/(float)velSteps;
		//printf("Body::updateVelocity DONE: %f\n", velocity);
	}
	
	Objsc::PointVec Body::getStageBodyPoints()
	{
		int vSize(pts.size());
		float hScale(1.0f / float(Objsc::Settings::Current()->getCameraWidth()));
		float vScale(1.0f / float(Objsc::Settings::Current()->getCameraHeight()));
		Objsc::PointVec stagePoints;
		for(Objsc::PointVec::iterator pt = pts.begin(); pt != pts.end(); ++pt)	{
			stagePoints.push_back(ofPoint(float((*pt).x) * hScale, float((*pt).y) * vScale));
		}
		return stagePoints;
	}
	
	void Body::print()
	{
		printf("Body [%u] age: %d num: %u\n", uid, age, stageNum);
		shapeRect.print();
		stageRect.print();
	}
	
	void Body::resetThresholds()
	{
		Objsc::Body::unbornAge = Objsc::DEFAULT_BODY_UNBORN_AGE;
		Objsc::Body::unbornHits = Objsc::DEFAULT_BODY_UNBORN_HITS;
		Objsc::Body::unbornLastHit = Objsc::DEFAULT_BODY_UNBORN_LAST_HIT;
		Objsc::Body::becomingAge = Objsc::DEFAULT_BODY_BECOMING_AGE;
		Objsc::Body::becomingHits = Objsc::DEFAULT_BODY_BECOMING_HITS;
		Objsc::Body::becomingLastHit = Objsc::DEFAULT_BODY_BECOMING_LAST_HIT;
		Objsc::Body::presentLastHit = Objsc::DEFAULT_BODY_PRESENT_LAST_HIT;
		Objsc::Body::absentLastHitMin = Objsc::DEFAULT_BODY_ABSENT_LAST_HIT_MIN;
		Objsc::Body::absentLastHitMax = Objsc::DEFAULT_BODY_ABSENT_LAST_HIT_MAX;
		Objsc::Body::maxDist = Objsc::DEFAULT_BODY_MAXDISTANCE;
		Objsc::Body::velSteps = Objsc::DEFAULT_BODY_VELSTEPS;
	}
		
	void Body::printThresholds()
	{
		printf("-----------------------------------:\n");
		printf("Body thresholds reset:\n");
		printf("\tunbornAge: %d\n", Objsc::Body::unbornAge);
		printf("\tunbornHits: %d\n", Objsc::Body::unbornHits);
		printf("\tunbornLastHit: %d\n", Objsc::Body::unbornLastHit);
		printf("\tbecomingAge: %d\n", Objsc::Body::becomingAge);
		printf("\tbecomingHits: %d\n", Objsc::Body::becomingHits);
		printf("\tbecomingLastHit: %d\n", Objsc::Body::becomingLastHit);
		printf("\tpresentLastHit: %d\n", Objsc::Body::presentLastHit);
		printf("\tabsentLastHitMin: %d\n", Objsc::Body::absentLastHitMin);
		printf("\tabsentLastHitMax: %d\n", Objsc::Body::absentLastHitMax);
        printf("\tmax distance: %f\n", Objsc::Body::maxDist);
		printf("\tvelocity steps: %d\n", Objsc::Body::velSteps);
		printf("-----------------------------------:\n");
	}
}
