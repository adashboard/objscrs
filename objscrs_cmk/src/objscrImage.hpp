#ifndef OBJSCR_IMAGES_H
#define OBJSCR_IMAGES_H

// sstf
#include "singleton.h"

// openframeworks
#include "ofMain.h"

// STL
#include <string>
#include <map>
using std::string;
using std::map;

namespace Objsc
{
	
	////////////////////////////////////////
	// IMAGE PRODUCER
	////////////////////////////////////////
	typedef map< string, ofImage*  > ImageMap;
	typedef ImageMap::value_type ImageMapEntry;

	class ImageProducer : public Singleton<ImageProducer>
	{
		private:
			Objsc::ImageMap images;
			
		public:
			ImageProducer() {}
			~ImageProducer() {images.clear(); }
				
			void load(string imgname, const char* filepath);
			
			const int getWidth(string imgname);
			const int getHeight(string imgname);
			const int totalPixels(string imgname);
			
			void fillPixels(string imgname, unsigned char* pixels);
			void fillPixels(string imgname, unsigned char* pixels, int width, int height);
			void fillPixels(string imgname, unsigned char* pixels, float widthPerc, float heightPerc);
			
	};

}
#endif
