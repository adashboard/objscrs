#include "objscrGUIRoot.hpp"

// objscrs
#include "objscrGUIStyles.hpp"
#include "objscrCommands.hpp"
#include "objscrPackets.hpp"
#include "objscrArchive.hpp"
#include "objscrApp.hpp"
#include "objscrFiles.hpp"
#include "objscrSettings.hpp"
#include "objscrFontconfig.hpp"
#include "objscrIDBroker.hpp"

// shared_ptr
#include <tr1/memory>

// openframeworks
#include "ofMain.h"

// stl
#include <string>
using std::string;

# define OBJSCRS_VIDEO_ON // comment out to disable video

namespace Objsc
{
	
	////////////////////////////////////////
	// PATH FIELD
	////////////////////////////////////////
	void PathField::placeInPath(const string& pathpart, int index)
	{
		if(index < 0 || index >= path.size())	{
			path.push_back(pathpart);
		} else {
			path[index]=pathpart;
		}
		text.clear();
		for(vector<string>::iterator part = path.begin(); part != path.end(); ++part)	{
			if(!(*part).empty()) {
				text.append((*part));
				if(part != path.end()-1)	{
					text.append(delim);
				}
			}
		}
	}
	
	////////////////////////////////////////
	// GUI ROOT
	////////////////////////////////////////
	GUIRoot::GUIRoot() : 
				layers(), 
				rootlayer(), 
				rootmenus(),
				patchlayer(),
				zonelayer(),
				framelayer(),
				medialayer(),
				stagelayer(),
				pathfield(),
				debug(false),
				cursoron(true),
				dovideo(false)
	{
		dovideo = Objsc::Settings::Current()->getCameraOn();
		initStyles();
		initRootLayer();
		initPatchLayer();
		initZoneLayer();
		initFrameLayer();
		initMediaLayer();
		initStageLayer();
		initPathfield();
		// commands
		Objsc::CommandDispatcher::Current()->registerCommandReceiver("_gui_", this);
	}
	
	void GUIRoot::initStyles()
	{
		Objsc::StylePalette::Current()->createStyle("default");
		// lang styles
		Objsc::WidgetStyle* langobjectStyle = Objsc::StylePalette::Current()->createStyle("langobject");
		langobjectStyle->setBackColour(255,255,255,255);
		langobjectStyle->setTextColour(0,0,0,255);
		langobjectStyle->setHilightColour(255,255,0);
		langobjectStyle->setFont(FontConfig::Current()->GetFont(FONT_PLAIN), 10);
		Objsc::WidgetStyle* langeditorStyle = Objsc::StylePalette::Current()->createStyle("langeditor");
		langeditorStyle->setBackColour(255,255,255,255);
		langeditorStyle->setTextColour(0,0,0,255);
		langeditorStyle->setForeColour(0,0,0,255);
		Objsc::WidgetStyle* langpatchStyle = Objsc::StylePalette::Current()->createStyle("langpatch");
		langpatchStyle->setBackColour(255,255,255,120);
		langpatchStyle->setTextColour(Objsc::BGCOLOUR);
		langpatchStyle->setForeColour(0,0,0,255);
		langpatchStyle->setFont(FontConfig::Current()->GetFont(FONT_BOLD), 24);
		Objsc::WidgetStyle* langcordStyle = Objsc::StylePalette::Current()->createStyle("langcord");
		langcordStyle->setBackColour(0,0,0);
		langcordStyle->setTextColour(0,0,0);
		langcordStyle->setHilightColour(255,0,0);
		// zone styles
		Objsc::WidgetStyle* zoneviewsStyle = Objsc::StylePalette::Current()->createStyle("zoneviews");
		zoneviewsStyle->setBackColour(255,0,0,255);
		zoneviewsStyle->setTextColour(255,0,0,255);
		zoneviewsStyle->setHilightColour(255,255,0);
		zoneviewsStyle->setFont(FontConfig::Current()->GetFont(FONT_PLAIN), 10);
		Objsc::WidgetStyle* circlezoneviewsStyle = Objsc::StylePalette::Current()->createStyle("circlezoneviews");
		circlezoneviewsStyle->setBackColour(255,255,0,255);
		circlezoneviewsStyle->setTextColour(255,255,0,255);
		circlezoneviewsStyle->setHilightColour(255,255,0);
		circlezoneviewsStyle->setFont(FontConfig::Current()->GetFont(FONT_PLAIN), 10);
		Objsc::WidgetStyle* GridZoneStyle = Objsc::StylePalette::Current()->createStyle("gridzoneviews");
		GridZoneStyle->setBackColour(0,127,255,255);
		GridZoneStyle->setTextColour(0,127,255,255);
		GridZoneStyle->setHilightColour(0,127,255);
		GridZoneStyle->setFont(FontConfig::Current()->GetFont(FONT_PLAIN), 10);
		Objsc::WidgetStyle* zonecanvasStyle = Objsc::StylePalette::Current()->createStyle("zonecanvas");
		zonecanvasStyle->setBackColour(255,0,0,255);
		zonecanvasStyle->setTextColour(255,0,0,255);
		zonecanvasStyle->setHilightColour(255,255,0);
		zonecanvasStyle->setFont(FontConfig::Current()->GetFont(FONT_BOLD), 24);
		// text
		Objsc::WidgetStyle* textStyle = Objsc::StylePalette::Current()->createStyle("text");
		textStyle->setBackColour(255,255,255,255);
		textStyle->setTextColour(0,0,0,255);
		// body
		Objsc::WidgetStyle* bodyStyle = Objsc::StylePalette::Current()->createStyle("body");
		bodyStyle->setBackColour(255,255,255,60);
		bodyStyle->setTextColour(255,127,0,255);
		bodyStyle->setFont(FontConfig::Current()->GetFont(FONT_PLAIN), 28);
		langeditorStyle->setForeColour(255,255,255,255);
		// media
		Objsc::WidgetStyle* mediapreviewStyle = Objsc::StylePalette::Current()->createStyle("mediapreview");
		mediapreviewStyle->setTextColour(255,255,255,255);
		mediapreviewStyle->setFont(FontConfig::Current()->GetFont(FONT_PLAIN), 8);
		// pathfield
		Objsc::WidgetStyle* pathfieldStyle = Objsc::StylePalette::Current()->createStyle("pathfield");
		pathfieldStyle->setTextColour(255,0,0,255);
		pathfieldStyle->setFont(FontConfig::Current()->GetFont(FONT_BOLD), 24);
	}

	void GUIRoot::initRootLayer()
	{
		// layer
		rootlayer = Objsc::GuiLayer::create();
		rootlayer->setBounds(0,0, ofGetWidth(), ofGetHeight());
		rootlayer->disable();
		// menus
		rootmenus.insert(ContextMenuMapEntry("main", Objsc::ContextMenu::create()));
		Objsc::ContextMenuPtr mainmenu = rootmenus["main"];
		// scores
		Objsc::MenuItem* scoremenu = mainmenu->addItem("scores");
		scoremenu->addItem("new", Objsc::CommandDispatcher::createCommand("_gui_", "new_score"));
		scoremenu->addItem("open", Objsc::CommandDispatcher::createCommand("_gui_", "open_score"));
		scoremenu->addItem("save", Objsc::CommandDispatcher::createCommand("_gui_", "save_score"));
		scoremenu->addItem("save as", Objsc::CommandDispatcher::createCommand("_gui_", "saveas_score"));
		// zones
		Objsc::MenuItem* zonemenu = mainmenu->addItem("maps");
		zonemenu->addItem("show/hide", Objsc::CommandDispatcher::createCommand("_gui_", "display_zones"));
		zonemenu->addItem("list maps", Objsc::CommandDispatcher::createCommand("_gui_", "list_maps"));
		zonemenu->addItem("add map", Objsc::CommandDispatcher::createCommand("_gui_", "new_map"));
		//patches
		//mainmenu->addItem("patches", Objsc::CommandDispatcher::createCommand("_gui_", "patches"));
		//video
		Objsc::MenuItem* cameramenu = mainmenu->addItem("camera");
		cameramenu->addItem("show/hide", Objsc::CommandDispatcher::createCommand("_gui_", "display_frames"));
		//tracking
		Objsc::MenuItem* trackingmenu = mainmenu->addItem("tracking");
		trackingmenu->addItem("show/hide", Objsc::CommandDispatcher::createCommand("_gui_", "display_tracking"));
		trackingmenu->addItem("start", Objsc::CommandDispatcher::createCommand("_gui_", "tracking_start"));
		trackingmenu->addItem("stop", Objsc::CommandDispatcher::createCommand("_gui_", "tracking_stop"));
		//mainmenu->addItem("media", Objsc::CommandDispatcher::createCommand("_gui_", "media"));
		//path
		Objsc::MenuItem* pathmenu = mainmenu->addItem("path");
		pathmenu->addItem("show/hide", Objsc::CommandDispatcher::createCommand("_gui_", "display_path"));
		// help
		mainmenu->addItem("help", Objsc::CommandDispatcher::createCommand("_gui_", "help"));
		// quit
		mainmenu->addItem("quit", Objsc::CommandDispatcher::createCommand("_app_", "quit"));
	}
	
	void GUIRoot::initPatchLayer()
	{
		patchlayer = Objsc::LangPatchEditor::create();
		//patchlayer->setBounds(0,0, ofGetWidth(), ofGetHeight());
		//patchlayer->setRootWidget(std::tr1::static_pointer_cast<Objsc::GuiBase>(Objsc::LangPatchEditor::create()));
		patchlayer->disable();
	}
	
	void GUIRoot::initZoneLayer()
	{
		zonelayer = Objsc::ZoneMapEditor::create();
		zonelayer->enable();
	}
	
	void GUIRoot::initFrameLayer()
	{
		//printf("GUIRoot::initFrameLayer A\n");
		if (dovideo)	{
			framelayer = Objsc::FrameSourceEditor::create();
			//printf("GUIRoot::initFrameLayer B\n");
			#ifdef  OBJSCRS_VIDEO_ON
			framelayer->enable();
			#endif 
		}
		//printf("GUIRoot::initFrameLayer DONE\n");
	}
	
	void GUIRoot::initMediaLayer()
	{
		medialayer = Objsc::MediaPlayer::Current();
		//medialayer->enable();
	}
	
	void GUIRoot::initStageLayer()
	{
		stagelayer = Objsc::StageView::create();
		stagelayer->setStage(Objsc::App::Current()->getStage());
		stagelayer->enable();
	}
	
	void GUIRoot::initPathfield()
	{
		Objsc::StylePalette::Current()->applyStyle("pathfield", &pathfield);
		pathfield.clearText();
		pathfield.setLocation(Objsc::WINDOWMARGIN_WIDTH, 0.0f);
		pathfield.drawBackground(false);
		pathfield.placeInPath("score",0);
		pathfield.placeInPath("map",1);
		pathfield.show();
	}
	// scores
	void GUIRoot::newScoreLoaded()
	{
		//~ Objsc::Stage::Current()->stop();
		//~ clearStage();
		setScorePath(Objsc::App::Current()->getScore()->getName());
		zonelayer->setZoneMap();
		showZones();
		showStage();
		//Objsc::Stage::Current()->start();
	}
	
	void GUIRoot::removingScore()
	{
		Objsc::Stage::Current()->stop();
		clearStage();
		Objsc::IDBroker::Current()->reset();
		Objsc::App::Current()->getStage()->clearStageNumbers();
		medialayer->clearAll();
	}
	
	
	// runtime
	void GUIRoot::run()
	{
		setScorePath(Objsc::App::Current()->getScore()->getName());
		zonelayer->setZoneMap();
	}
	
	
	void GUIRoot::openRootMenu(const string& menuname, int x, int y)
	{
		//printf("GUIRoot::openRootMenu <%s>\n", menuname.c_str());
		if(rootmenus.count(menuname) > 0)	{
			//printf("GUIRoot::openRootMenu A\n");
			Objsc::ContextMenuPtr menu = rootmenus[menuname];
			menu->setLocation(x,y);
			menu->enable();
			menu->reset();
			//printf("GUIRoot::openRootMenu B [%d]\n", menu.use_count());
			rootlayer->setRootWidget(std::tr1::static_pointer_cast<Objsc::GuiBase>(menu));
			//printf("GUIRoot::openRootMenu C\n");
			rootlayer->enable();
			//printf("GUIRoot::openRootMenu D\n");
		} else {
			rootlayer->close();
		}
	}
	
	void GUIRoot::openRootMenu(Objsc::ContextMenuPtr menu, int x, int y)
	{
		menu->setLocation(x,y);
		menu->enable();
		menu->reset();
		rootlayer->setRootWidget(std::tr1::static_pointer_cast<Objsc::GuiBase>(menu));
		rootlayer->enable();
	}
	
	void GUIRoot::closeRootMenu()
	{
		//printf("GUIRoot::closeRootMenu\n");
		rootlayer->close();
	}
	
	// dialogues
	void GUIRoot::openDialogue(Objsc::DialoguePtr dialogue)
	{
		dialogue->placeCentre();
		dialogue->open();
		rootlayer->setRootWidget(std::tr1::static_pointer_cast<Objsc::GuiBase>(dialogue));
		rootlayer->enable();
	}
	
	void GUIRoot::closeDialogue()
	{
		rootlayer->close();
	}
	
	// doc
	void GUIRoot::grabScreen()
	{
		ofImage img;
		int w = ofGetWidth();
		int h = ofGetHeight();
		img.grabScreen(0,0,w,h);
		unsigned char *pixels = img.getPixels();
		int span;
		unsigned char r,b;
		for (int i = 0; i < h; i++) {
			for (int j = 0; j < w*3; j+=3) {
				// NOTE: swap red and blue channels to fix FreeImage 
				span = (i*w*3)+j;
				r = pixels[span];
				b = pixels[span+2];
				pixels[span] = b;
				pixels[span+2] = r;
			}
		}
		img.update();
		img.saveImage(Objsc::ProjectFiles::Current()->makeScreenGrabPath());
	}
	
	// runtime
	void GUIRoot::update()
	{
		rootlayer->update();
		if(dovideo)
			framelayer->update();
		stagelayer->update();
		patchlayer->update();
		zonelayer->update();
		medialayer->update();
	}
	
	void GUIRoot::draw()
	{
		ofEnableAlphaBlending(); 
		if(dovideo && !debug)
			framelayer->draw();
		if(debug)	{
			Objsc::Stage::Current()->draw();
		}
		medialayer->draw();
		stagelayer->draw();
		zonelayer->draw();
		patchlayer->draw();
		pathfield.draw();
		rootlayer->draw();
		ofDisableAlphaBlending();
	}
	
	// commands
	void GUIRoot::doCommand(Objsc::CommandPtr& cmd)
	{
		//std::cout << "GUIRoot::doCommand: " << cmd->getName() << std::endl;
		string cmdname = cmd->getName();
		if(cmdname ==  "patches")	{
			if(patchlayer->isActive())	{
				hidePatches();
			} else {
				showPatches();
			}
			closeRootMenu();
		} else if(cmdname ==  "close_patch")	{
			if(patchlayer->isActive())	{
				hidePatches();
			}
			clearZonePath();
			closeRootMenu();
		} else if(cmdname ==  "display_zones")	{ // ZONE MAPS
			if(zonelayer->isVisible())	{
				hideZones();
			} else {
				showZones();
			}
			closeRootMenu();
		} else if(cmdname ==  "new_map")	{
			closeRootMenu();
			zonelayer->createZoneMap();
		} else if(cmdname ==  "list_maps")	{
			closeRootMenu();
			zonelayer->viewMapsList();
		} else if(cmdname ==  "display_frames")	{
			if(dovideo)	{
				if(framelayer->isActive())	{
					hideFrames();
				} else {
					showFrames();
				}
			}
			closeRootMenu();
		} else if(cmdname ==  "display_tracking")	{
			if(stagelayer->isVisible())	{
				hideStage();
			} else {
				showStage();
			}
			closeRootMenu();
		} else if(cmdname ==  "tracking_start")	{
			Objsc::Stage::Current()->start();
			closeRootMenu();
		} else if(cmdname ==  "tracking_stop")	{
			if(Objsc::Stage::Current()->isActive())	{
				Objsc::Stage::Current()->stop();
				stagelayer->clear();
			} 
			closeRootMenu();
		} else if(cmdname ==  "new_score")	{ // SCORES
			closeRootMenu();
			Objsc::App::Current()->newScore()->makeFirstMapCurrent();
			newScoreLoaded();
		} else if(cmdname ==  "open_score")	{ // SCORES
			closeRootMenu();
			Objsc::Archivist::Current()->openFileDialogue();
		} else if(cmdname ==  "save_score")	{ 
			closeRootMenu();
			Objsc::Archivist::Current()->save();
		} else if(cmdname ==  "saveas_score")	{ 
			closeRootMenu();
			Objsc::Archivist::Current()->saveFileDialogue();
		} else if(cmdname ==  "display_path")	{ // PATH
			if(pathfield.isVisible())	{
				pathfield.hide();
			} else {
				pathfield.show();
			}
		} else if(cmdname ==  "close_dialogue")	{
			closeDialogue();
		} else if(cmdname ==  "help")	{
			closeRootMenu();
			ofLaunchBrowser(Objsc::ProjectFiles::Current()->makeDocFilePath());
		}
		else if(cmdname ==  "load_settings")
		{
			std::string ss;
			if(cmd->getData()->sizeArgs() > 0)
			{
				cmd->getData()->getArg(0, &ss);
			}
			updateSettings(ss);
		}
	}
	
	void GUIRoot::undoCommand(Objsc::CommandPtr& cmd)
	{
		std::cout << "App::undoCommand: " << cmd->getName() << std::endl;
	}
	
	// events
	void GUIRoot::keyPressed(int key)
	{
		//printf("key %d\n", key);
		if(glutGetModifiers() == GLUT_ACTIVE_CTRL)	{
			key += 96;
			Objsc::App::Current()->handleControlKey(key);
			return;
		}
			
		if(glutGetModifiers() == GLUT_ACTIVE_ALT)	{
			switch(key)	{
				case 'g':
					grabScreen();
					break;
				case 'd':
					debug = !debug;
					printf("DEBUG %d \n", debug);
					break;
				case 'c':
					patchlayer->toggleBG();
					break;
				case 'b':
					printf("LOADBANGS\n");
					stagelayer->doLoadActions();
					break;
				case 'm':
					//printf("MOUSE\n");
					if(cursoron)	{
						cursoron=false;
						ofHideCursor();
					} else {
						cursoron=true;
						ofShowCursor();
					}
					break;
				case 't':
					Objsc::Stage::Current()->start();
					break;
				case 'r':
					if(Objsc::Stage::Current()->isActive())	{
						Objsc::Stage::Current()->stop();
						stagelayer->clear();
					} 
					break;
			case 'k':
				Objsc::CommandDispatcher::Current()->doCommand("_frames_", std::string("color_on"));
				break;
			case 'w':
				Objsc::CommandDispatcher::Current()->doCommand("_frames_", std::string("color_off"));
				break;

			}

			return;
		}

		if((glutGetModifiers() & GLUT_ACTIVE_ALT) == GLUT_ACTIVE_ALT
				&& (glutGetModifiers() & GLUT_ACTIVE_SHIFT) == GLUT_ACTIVE_SHIFT)
        {
			switch(key + 32)
			{
			case 'h':
				Objsc::Stage::Current()->getShapeFilter()->setColorChannel(ofxCvColorImage::ChannelHue,
											   !Objsc::Stage::Current()->getShapeFilter()->hasColorChannel(ofxCvColorImage::ChannelHue));
				break;

			case 's':
				Objsc::Stage::Current()->getShapeFilter()->setColorChannel(ofxCvColorImage::ChannelSaturation,
											   !Objsc::Stage::Current()->getShapeFilter()->hasColorChannel(ofxCvColorImage::ChannelSaturation));
				break;

			case 'v':
				Objsc::Stage::Current()->getShapeFilter()->setColorChannel(ofxCvColorImage::ChannelValue,
											   !Objsc::Stage::Current()->getShapeFilter()->hasColorChannel(ofxCvColorImage::ChannelValue));
				break;

			case 'r':
				Objsc::Stage::Current()->getShapeFilter()->setColorChannel(ofxCvColorImage::ChannelRed,
											   !Objsc::Stage::Current()->getShapeFilter()->hasColorChannel(ofxCvColorImage::ChannelRed));
				break;

			case 'g':
				Objsc::Stage::Current()->getShapeFilter()->setColorChannel(ofxCvColorImage::ChannelGreen,
											   !Objsc::Stage::Current()->getShapeFilter()->hasColorChannel(ofxCvColorImage::ChannelGreen));
				break;

			case 'b':
				Objsc::Stage::Current()->getShapeFilter()->setColorChannel(ofxCvColorImage::ChannelBlue,
											   !Objsc::Stage::Current()->getShapeFilter()->hasColorChannel(ofxCvColorImage::ChannelBlue));
				break;
			}
			return;
		}

		
		if(rootlayer->isActive())	{
			rootlayer->keyPressed(key);
			return;
		}
		if(patchlayer->isActive())	{
			patchlayer->keyPressed(key);
			return;
		}
		if(zonelayer->isActive())	{
			zonelayer->keyPressed(key);
			return;
		}
	}
	void GUIRoot::keyReleased(int key)
	{
		if(rootlayer->isActive())	{
			rootlayer->keyReleased(key);
			return;
		}
		if(patchlayer->isActive())	{
			patchlayer->keyReleased(key);
			return;
		}
		if(zonelayer->isActive())	{
			zonelayer->keyReleased(key);
			return;
		}
	}
	void GUIRoot::mouseMoved(int x, int y )
	{
		if(rootlayer->isActive())	{
			rootlayer->mouseMoved(x,y);
			return;
		}
		if(patchlayer->isActive())	{
			patchlayer->mouseMoved(x,y);
			return;
		}
		if(zonelayer->isActive())	{
			zonelayer->mouseMoved(x,y);
			return;
		}
	}
	void GUIRoot::mouseDragged(int x, int y, int button)
	{
		if(rootlayer->isActive())	{
			rootlayer->mouseDragged(x,y,button);
			return;
		}
		if(patchlayer->isActive())	{
			patchlayer->mouseDragged(x,y,button);
			return;
		}
		if(zonelayer->isActive())	{
			zonelayer->mouseDragged(x,y,button);
			return;
		}
	}
	
	void GUIRoot::mousePressed(int x, int y, int button)
	{
		
		
		//printf("GUIRoot::mousePressed A\n");
		for (Objsc::GuiLayerPtrVector::iterator layer = layers.begin(); layer != layers.end(); ++layer)	{
			(*layer)->mousePressed(x,y,button);
			if((*layer)->eventConsumed())	{
				//printf("GUIRoot::mousePressed CONSUMED\n");
				return;
			}
		}
		//printf("GUIRoot::mousePressed NOT CONSUMED\n");
		if(rootlayer->isActive())	{
			rootlayer->mousePressed(x,y,button);
			//printf("GUIRoot::mousePressed rootlayer active\n");
			//~ if(!rootlayer->eventConsumed())	{
				//~ printf("GUIRoot::mousePressed rootlayer NOT CONSUMED\n");
				//~ //closeRootMenu();
			//~ }
			if(!rootlayer->withinBounds(x,y))	{
				//printf("GUIRoot::mousePressed rootlayer OUT OF BOUNDS\n");
				closeRootMenu();
			}
		} else {
			if(patchlayer->isActive())	{
				patchlayer->mousePressed(x,y,button);
				if(patchlayer->eventConsumed())	{
					return;
				}
			}
			if(zonelayer->isActive())	{
				zonelayer->mousePressed(x,y,button);
				if(zonelayer->eventConsumed())	{
					return;
				}
			}
			switch(button)	{
				case 0:
					openRootMenu("main", x, y);
					break;
				case 1:
					//openRootMenu("main", x, y);
					break;
				case 2:
					openRootMenu("main", x, y);
					break;
			}
		}
	}
	void GUIRoot::mouseReleased(int x, int y, int button)
	{
		if(rootlayer->isActive())
			rootlayer->mouseReleased(x,y,button);
		if(zonelayer->isActive())
			zonelayer->mouseReleased(x,y,button);
		if(patchlayer->isActive())
			patchlayer->mouseReleased(x,y,button);
	}
	void GUIRoot::windowResized(int w, int h)
	{
		for (Objsc::GuiLayerPtrVector::iterator layer = layers.begin(); layer != layers.end(); ++layer)	{
			(*layer)->windowResized(w,h);
		}
	}
		
	Objsc::GuiLayerPtr GUIRoot::createLayer(float x, float y, float width, float height)
	{
		layers.push_back(Objsc::GuiLayer::create());
		layers.back()->setBounds(x,y,width,height);
		return layers.back();
	}
	
	Objsc::GuiLayerPtr GUIRoot::createLayer(float x, float y, float width, float height, int level)
	{
		
		Objsc::GuiLayerPtr layer = Objsc::GuiLayer::create();
		layer->setBounds(x,y,width,height);
		if(level <= 0)	{
			layers.insert(layers.begin(), layer);
		} else if(level > layers.size())	{
			layers.push_back(layer);
		} else {
			GuiLayerPtrVector::iterator it = layers.begin();
			layers.insert(layers.begin()+level, layer);
		}
		return layer;
	}
	
	// layers
	void GUIRoot::showPatches()
	{
		patchlayer->enable();
	}
	
	void GUIRoot::hidePatches()
	{
		patchlayer->disable();
	}
	
	void GUIRoot::showZones()
	{
		zonelayer->show();
	}
	
	void GUIRoot::hideZones()
	{
		zonelayer->hide();
	}
	
	void GUIRoot::showFrames()
	{
		if(dovideo)
			framelayer->enable();
	}
	
	void GUIRoot::hideFrames()
	{
		if(dovideo)
			framelayer->disable();
	}
	
	void GUIRoot::showStage()
	{
		stagelayer->show();
	}
	
	void GUIRoot::hideStage()
	{
		stagelayer->hide();
	}
	
	void GUIRoot::clearStage()
	{
		stagelayer->clear();
	}
	
	// pathfield
	void GUIRoot::setScorePath(const string& scorepath)
	{
		if(scorepath.empty())	{
			pathfield.placeInPath("*",0);
		} else {
			pathfield.placeInPath(scorepath,0);
		}
	}
	
	void GUIRoot::setMapPath(const string& mappath)
	{
		pathfield.placeInPath(mappath,1);
	}
	
	void GUIRoot::setZonePath(const string& zonepath)
	{
		pathfield.placeInPath(zonepath,2);
	}
	
	void GUIRoot::clearZonePath()
	{
		pathfield.placeInPath("",2);
	}
	
	void GUIRoot::showPath()
	{
		pathfield.show();
	}
	
	void GUIRoot::hidePath()
	{
		pathfield.hide();
	}
	
	void GUIRoot::printToScreen(const string& text)
	{
		pathfield.setText(text);
	}

	void GUIRoot::updateSettings(const string &settingsname)
	{
		if(settingsname.empty())
			Objsc::Settings::Current()->load();
		else
		{
			Objsc::Settings::Current()->load(Objsc::ProjectFiles::Current()->makeAdminPath(settingsname));
		}
	}
}
