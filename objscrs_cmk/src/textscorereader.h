/***************************************************************************
 *   Copyright (C)  2012 by Pierre Marchand                   *
 *   pierre@oep-h.com   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef OBJSC_TEXTSCOREREADER_H
#define OBJSC_TEXTSCOREREADER_H

#include "objscrScore.hpp"
#include "objscrLang.hpp"

namespace Objsc {
	/**
	  [NOTE] this is a prototype intended to support discussion with performers, and i mean it - pm
	  This class is an attempt to interpret a score where notation happens
	  to be textual/code-oriented rather than structured, XML-based syntax.
	  The idea here is to take advantage of kind of introspection on objects
	  exposed by the program itself to interpret the score as to let the
	  notation evolves without touching this code (well, in a perfect world).

	  basics:
	  Creation of objects of any type is of the form
	  TYPE IDENTIFIER [PARAMS]
	  where TYPE matches a langname as found in objscrs
	  where IDENTIFIER is a (w?)string without any space character
	  where PARAMS is optional (not even sure it has to be here at all)

	  Affectation of object attributes is of the form
	  IDENTIFIER_A.ATTRIBUTE = IDENTIFIER_B
	  where '.' is the access operator
	  where '=' is the affectation operator

	  possible example (without keywords)
	  ===================================
	  # objects
	  basic_zone zone1
	  img myImage
	  message ml load animage.png 1 0
	  message ms show
	  cord cord_load
	  cord cord_show
	  rect rz 0 0 0.5 0.5
	  zone1.bounds = rz
	  myimage.zone = zone1

	  # connections
	  cord_load.from = ml.out0
	  cord_load.to = myImage.in0
	  cord_show.from = ms.out0
	  cord_show.to = myImage.in0
	  ===================================

	  If wanted, we might introduce few keywords and syntax shortcuts
	  in order to make the notation more compact.
	  example:
	  ===================================
	  basic_zone zone1
	  img myImage zone1
	  message ml load animage.png 1 0
	  message ms show
	  zone1.bounds = rect 0 0 0.5 0.5

	  connect ml.out0 myImage.in0
	  connect ms.out0 myImage.in0
	  ===================================


	  */
	class TextScoreReader
	{
		public:
			static const std::string startMap;
			static const std::string endMap;

			static const std::string startZone;
			static const std::string endZone;

			static const std::string keyLink;
			static const std::string keyReceive;

			// few utility typedefs
			typedef std::vector<std::string> stringList;
			typedef std::vector<stringList> lineList;

		private:
			void load(const std::string& text);
			void readText();
			void readMap(lineList::const_iterator& lineIterator);
			void readZone(lineList::const_iterator& lineIterator, Objsc::ZoneMapPtr& omap);
			void readObjects(lineList::const_iterator& lineIterator, Objsc::LangPatchPtr& opatch);
			void linkObjects(lineList::const_iterator& lineIterator);

			void layout();

		protected:
			Objsc::ScorePtr score;
			lineList lines;
			std::map<std::string, Objsc::LangObjectPtr> objects;

		public:
			TextScoreReader();
			void read(const std::string& filename);
	};

} // namespace Objsc

#endif // OBJSC_TEXTSCOREREADER_H
