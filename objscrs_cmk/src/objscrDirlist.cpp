#include "objscrDirlist.hpp"

#include <algorithm>
#include <string>


namespace Objsc
{
	// Handy string functions

	static std::string::size_type idx;
	static string getExt(string filename){
	    idx = filename.rfind('.');

	    if(idx != std::string::npos){
		return filename.substr(idx+1);
	    }
	    else{
		return "";
	    }
	}

	static string strToLower(string myStr){
	    transform( myStr.begin(), myStr.end(), myStr.begin(), ::tolower );
	    return myStr;
	}

	//----------------------------------------------------------
	DirList::DirList(){
	    reset();
	}

	//----------------------------------------------------------
	void DirList::reset(){
		allowedFileExt.clear();
		nameArray.clear();
		pathArray.clear();
	}



	//----------------------------------------------------------
	bool DirList::allowExt(string ext){
		allowedFileExt.push_back( strToLower(ext) );
		return true;
	}

	//----------------------------------------------------------
	string DirList::getName(int pos){
		if(pos >= (int)nameArray.size()) return 0;
		return nameArray[pos];
	}

	//----------------------------------------------------------
	string DirList::getPath(int pos){
		if(pos >= (int)pathArray.size()) return 0;
		return pathArray[pos];
	}

	//----------------------------------------------------------
	int DirList::listDir(string directory){
	    //directory = ofToDataPath(directory);

		nameArray.clear();
		pathArray.clear();

	    if(directory.length() <= 0)return 0;

	    //if the trailing slash was not added - then add it
		if( directory[directory.length()-1] != '/'){
		directory = directory + "/";
		}

		DIR *dir = NULL;
		struct dirent *entry;

	    //open the directory
	    //ofLog(OF_LOG_VERBOSE, "DirList - attempting to open %s", directory.c_str());
	    dir = opendir(directory.c_str());

		if(dir == NULL){
			//ofLog(OF_LOG_ERROR, "DirList - error opening directory");
			return 0;
		}

	    string entry_name = "";
	    string ext = "";
		bool skip = false;

		while ((entry = readdir(dir)) != NULL){

		//turn it into a C++ string
		entry_name = entry->d_name;

		//lets get the length of the string here as we query it again
		int fileLen = entry_name.length();

			if(fileLen <= 0)continue; //if the name is not existant
			if(entry_name[0] == '.')continue; //ignore invisible files, ./ and ../

			//by default we don't skip files unless we are checking extensions
			skip = false;

			if(allowedFileExt.size() > 0){
				//we will skip this files unless it has an allowed extension
				skip = true;
				for(int i = 0; i < (int)allowedFileExt.size(); i++){

					//if the wildecard * has been entered for an ext type then don't check any extensions
					if( allowedFileExt[i] == "*"){ skip = false; break; }


					int extLen = allowedFileExt[i].length();

					//the extension has to be shorter than the filename - simple check
					if(extLen >= fileLen) continue;

			//lets get the ext as lowercase
			ext = strToLower( getExt(entry_name) );

			//if no ext - then skip this ext check
			if( ext == "" )continue;

			//if we find a match then stop checking and approve this file
			if(ext == allowedFileExt[i]){
			    skip = false;
			    break;
			}
				}
			}

			if(skip) continue;

			//finally we store the result
		pathArray.push_back(directory + entry_name);
		nameArray.push_back(entry_name);

			//ofLog(OF_LOG_VERBOSE, "DirList - listing %s ", nameArray.back().c_str());
		}

		return nameArray.size();
	}
}