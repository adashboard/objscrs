#include "objscrDialogue.hpp"

// objscrs
#include "objscrGUIStyles.hpp"

// openframeworks
#include "ofMain.h"

namespace Objsc
{

const std::string Dialogue::OK_STRING = std::string("ok");
const std::string Dialogue::CANCEL_STRING = std::string("cancel");
const std::string Dialogue::ADD_STRING = std::string("add");
const std::string Dialogue::REMOVE_STRING = std::string("remove");

	Dialogue::Dialogue(const string& label) : listMenu(), textField(), actionBtn(), cancelBtn(), cmdlabel(label), edititems(false)
	{
		Objsc::CommandDispatcher::Current()->registerCommandReceiver(cmdlabel, this);
		Objsc::StylePalette::Current()->applyStyle("default", this);
		Objsc::StylePalette::Current()->applyStyle("default", &listMenu);
		Objsc::StylePalette::Current()->applyStyle("text", &textField);
		Objsc::StylePalette::Current()->applyStyle("default", &addBtn);
		Objsc::StylePalette::Current()->applyStyle("default", &removeBtn);
		Objsc::StylePalette::Current()->applyStyle("default", &actionBtn);
		Objsc::StylePalette::Current()->applyStyle("default", &cancelBtn);
		textField.clearText();
		actionBtn.setText(OK_STRING);
		actionBtn.setAction(Objsc::CommandDispatcher::createCommand(cmdlabel, "open_selection"));
		cancelBtn.setText(CANCEL_STRING);
		cancelBtn.setAction(Objsc::CommandDispatcher::createCommand(cmdlabel, "close_dialogue"));
		addBtn.setText(ADD_STRING);
		addBtn.setAction(Objsc::CommandDispatcher::createCommand(cmdlabel, "add_item"));
		removeBtn.setText(REMOVE_STRING);
		removeBtn.setAction(Objsc::CommandDispatcher::createCommand(cmdlabel, "remove_selected"));
		listMenu.setAction(Objsc::CommandDispatcher::createCommand(cmdlabel, "item_selected"));
		setDimensions(400,300);
	}
	
	void Dialogue::sizeEditItems(bool value) 
	{ 
		edititems=value; 
		setDimensions(bounds.getWidth(),bounds.getHeight());
	}
	
	void Dialogue::setActionLabel(const string& label)
	{
		actionBtn.setText(label);
	}
	
	void Dialogue::setActionType(const string& actiontype)
	{
		actionBtn.setAction(Objsc::CommandDispatcher::createCommand(cmdlabel, actiontype));
	}
	
	// text
	void Dialogue::setText(const string& text)
	{
		textField.setText(text);
	}
	
	// geom
	void Dialogue::setLocation(float x, float y)
	{
		bounds.setLocation(x,y);
		textField.setLocation(bounds.getX(),bounds.getY());
		if(edititems)	{
			addBtn.setLocation(textField.getRight(),bounds.getY());
			removeBtn.setLocation(addBtn.getRight(),bounds.getY());
			actionBtn.setLocation(removeBtn.getRight(),bounds.getY());
			cancelBtn.setLocation(actionBtn.getRight(),bounds.getY());
		} else {
			actionBtn.setLocation(textField.getRight(),bounds.getY());
			cancelBtn.setLocation(actionBtn.getRight(),bounds.getY());
		}
		listMenu.setLocation(bounds.getX(),bounds.getY()+textField.getHeight());
	}
	
	void Dialogue::setCentre(float x, float y)
	{
		bounds.setCentre(x,y);
		textField.setLocation(bounds.getX(),bounds.getY());
		if(edititems)	{
			addBtn.setLocation(textField.getRight(),bounds.getY());
			removeBtn.setLocation(addBtn.getRight(),bounds.getY());
			actionBtn.setLocation(removeBtn.getRight(),bounds.getY());
			cancelBtn.setLocation(actionBtn.getRight(),bounds.getY());
		} else {
			actionBtn.setLocation(textField.getRight(),bounds.getY());
			cancelBtn.setLocation(actionBtn.getRight(),bounds.getY());
		}
		listMenu.setLocation(bounds.getX(),bounds.getY()+textField.getHeight());
	}
	
	void Dialogue::placeCentre()
	{
		setCentre(ofGetWidth()/2,ofGetHeight()/2);
	}
	
	void Dialogue::setDimensions(float width, float height)
	{
		bounds.setDimensions(width,height);
		listMenu.setDimensions(width,height-textField.getHeight());
		if(edititems)	{
			textField.setWidth((width/6)*2);
			addBtn.setWidth(width/6);
			removeBtn.setWidth(width/6);
			actionBtn.setWidth(width/6);
			cancelBtn.setWidth(width/6);
			addBtn.setLocation(textField.getRight(),bounds.getY());
			removeBtn.setLocation(addBtn.getRight(),bounds.getY());
			actionBtn.setLocation(removeBtn.getRight(),bounds.getY());
			cancelBtn.setLocation(actionBtn.getRight(),bounds.getY());
		} else {
			textField.setWidth((width/6)*4);
			actionBtn.setWidth(width/6);
			cancelBtn.setWidth(width/6);
			actionBtn.setLocation(textField.getRight(),bounds.getY());
			cancelBtn.setLocation(actionBtn.getRight(),bounds.getY());
		}
	}
	
	bool Dialogue::withinBounds(float x, float y) 
	{ 
		return bounds.containsPoint(x,y); 
	}
	
	// runtime
	void Dialogue::draw()
	{
		textField.draw();
		if(edititems)	{
			addBtn.draw();
			removeBtn.draw();
		}
		actionBtn.draw();
		cancelBtn.draw();
		listMenu.draw();
	}
	
	// dialogues
	void Dialogue::open()
	{
		actionBtn.enable();
		cancelBtn.enable();
		textField.enable();
		if(edititems)	{
			addBtn.show();
			removeBtn.show();
		}
		actionBtn.show();
		cancelBtn.show();
		textField.show();
		refresh();
	}
	
	void Dialogue::refresh()
	{
		if(finder)	{
			listMenu.clear();
			const Objsc::ItemNames items  = finder->getItemNames();
			for(Objsc::ItemNames::const_iterator item = items.begin(); item != items.end(); ++item)	{
				listMenu.addItem((*item));
			}
			listMenu.refreshDisplay();
		}
	}
	
	void Dialogue::close()
	{
		addBtn.looseFocus();
		removeBtn.looseFocus();
		actionBtn.looseFocus();
		cancelBtn.looseFocus();
		textField.looseFocus();
		textField.clearText();
		Objsc::CommandDispatcher::Current()->doCommand(Objsc::CommandDispatcher::createCommand("_gui_", "close_dialogue"));
	}
	
	// commands
	void Dialogue::doCommand(Objsc::CommandPtr& cmd)
	{
		//printf("Dialogue::doCommand <%s>\n", cmd->getName().c_str());
		if(cmd->getName() ==  "item_selected")	{
			if(listMenu.hasSelection())	{
				textField.setText(listMenu.getSelectedName());
			}
		} else if(cmd->getName() ==  "open_selection")	{
			if(listMenu.hasSelection())	{
				if(finder)	{
					finder->selectItem(listMenu.getSelectedName());
				}
				setText("");
				close();
			}
		} else if(cmd->getName() ==  "handle_input")	{
			if(!textField.getText().empty())	{
				if(finder)	{
					finder->handleInput(textField.getText());
				}
				setText("");
				close();
			}
		} else if(cmd->getName() ==  "add_item")	{
			if(finder)	{
				finder->addItem(textField.getText());
				refresh();
				setText("");
			}
		} else if(cmd->getName() ==  "remove_selected")	{
			if(listMenu.hasSelection() && finder)	{
				finder->removeItem(listMenu.getSelectedName());
				refresh();
				setText("");
			}
		} else if(cmd->getName() ==  "close_dialogue")	{
			close();
		}
	}
	
	void Dialogue::undoCommand(Objsc::CommandPtr& cmd)
	{
		
	}
	
	// events
	void Dialogue::keyPressed(int key)
	{
		textField.keyPressed(key);
	}
	
	void Dialogue::keyReleased(int key)
	{
		textField.keyReleased(key);
	}
	
	void Dialogue::mouseMoved(int x, int y )
	{
		textField.mouseMoved(x,y);
		if(edititems)	{
			addBtn.mouseMoved(x,y);
			removeBtn.mouseMoved(x,y);
		}
		actionBtn.mouseMoved(x,y);
		cancelBtn.mouseMoved(x,y);
		listMenu.mouseMoved(x,y);
	}
	
	void Dialogue::mouseDragged(int x, int y, int button)
	{
		textField.mouseDragged(x,y,button);
		if(edititems)	{
			addBtn.mouseDragged(x,y,button);
			removeBtn.mouseDragged(x,y,button);
		}
		actionBtn.mouseDragged(x,y,button);
		cancelBtn.mouseDragged(x,y,button);
		listMenu.mouseDragged(x,y,button);
	}
	
	void Dialogue::mousePressed(int x, int y, int button)
	{
		textField.mousePressed(x,y,button);
		if(edititems)	{
			addBtn.mousePressed(x,y,button);
			removeBtn.mousePressed(x,y,button);
		}
		actionBtn.mousePressed(x,y,button);
		cancelBtn.mousePressed(x,y,button);
		listMenu.mousePressed(x,y,button);
	}
	
	void Dialogue::mouseReleased(int x, int y, int button)
	{
		
	}
	
	void Dialogue::windowResized(int w, int h)
	{
		
	}
	

}

