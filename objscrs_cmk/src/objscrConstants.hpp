#ifndef  OBJSCR_CONSTANTS_H
#define OBJSCR_CONSTANTS_H


#include <string>
#include <limits>

#define RINT(x) ( (x)-floor(x)>0.5? ceil(x) : floor(x) )

#define RAD2DEG 57.295779513082323

namespace Objsc
{
	// DATA TYPES
	// strings
	const std::string 		NULL_STRING = "";
	
	// numbers
	const double  		MAXDOUBLE = (std::numeric_limits<double>::max)();
	const double  		MINDOUBLE = (std::numeric_limits<double>::min)();
	const float  			MAXFLOAT = (std::numeric_limits<float>::max)();
	const float  			MINFLOAT = (std::numeric_limits<float>::min)();
	
	// GUI
	//const int COLOURBINS = 24;
	const int LARGEBTNWIDTH = 64;
	const int TOOLBUTTONWIDTH = 18;
	const int TOOLBUTTONHEIGHT = 18;
	const int TOOLBUTTONMARGIN = 10;

	const unsigned int ID_UNDEFINED = 0;
	const unsigned int INVALID_NODE_INDEX = 0;
	const int MOUSELEFT = 0;
	const int MOUSECENTRE = 1;
	const int MOUSERIGHT = 2;
	
	const int SCRWIDTH = 640;
	const int SCRHEIGHT = 480;
	const int FONTSIZE = 16;
	const int PADWIDTH = 2;
	const int PADHEIGHT = 4;
	const float HANDLESIZE = 8.0f;
	const float SCROLLHANDLEWIDTH = 20.0f;
	const float SCROLLHANDLEHEIGHT = 30.0f;
	const float TIMELINEHANDLEHEIGHT = 20.0f;
	const float TIMELINESCROLLHEIGHT = 21.0f;
	
	const float WINDOWMARGIN_WIDTH = 10.0f;
	const float WINDOWMARGIN_HEIGHT = 10.0f;
	
	// markers
	const float MARKERWIDTH = 32.0f; //40.0f;
	const float MARKERHEIGHT = 32.0f; //40.0f;
	const float MARKERICONWIDTH = 32.0f;
	const float MARKERICONHEIGHT = 32.0f;
	
	enum gui_drag
	{
		DRAG_NONE,
		DRAG_NORTH,
		DRAG_NORTHEAST,
		DRAG_EAST,
		DRAG_SOUTHEAST,
		DRAG_SOUTH,
		DRAG_SOUTHWEST,
		DRAG_WEST,
		DRAG_NORTHWEST,
		DRAG_STARTHANDLE,
		DRAG_ENDHANDLE,
		DRAG_WHOLE
	};

	enum gui_scrolls {
		SCROLL_NONE,
		SCROLL_UP,
		SCROLL_DOWN
	};
	
	enum text_states {
		TEXT_CLEAR,
		TEXT_TYPING,
		TEXT_PAUSED,
	};
	
	enum displaystates {ENABLED, DISABLED, HIGHLIGHT, SELECTED};
	enum selectionmodes {SELECTONE, SELECTMANY, SELECTNONE};
	
	// CAMERA
	enum camera_lib {CAMLIB_V4L = 0, CAMLIB_DVGRAB};
	const int 			VIDSOURCE_WIDTH = 320;
	const int 			VIDSOURCE_HEIGHT = 240;
	const int 			PAL_WIDTH = 720;
	const int 			PAL_HEIGHT = 576;
	const int 			DEFAULT_V4L_DEVICE = 0;
	const int 			DEFAULT_SCR_WIDTH = 640;
	const int 			DEFAULT_SCR_HEIGHT = 480;
	const bool			DEFAULT_CAMERA_ON = false;
	
	// ZONES
	enum zonetypes
	{
		ZONE_SQUARE,
		ZONE_POINT,
		ZONE_GRID
	};
	enum zonedatatypes
	{
		ZONE_DATA_NONE = -1,
		ZONE_DATA_COUNT,
		ZONE_DATA_LOC,
		ZONE_DATA_GEOM,
		ZONE_DATA_GRID,
		ZONE_DATA_CELLS
	};
	const float 			ZONE_DFTWIDTH = 0.1f; //1.0f;
	const float 			ZONE_DFTHEIGHT = 0.1f; //1.0f;
	const int			ZONE_TYPE_INDEX = 1;
	const int			ZONE_DATA_TYPE_INDEX = 2;
	const int			ZONE_DATA_INDEX = 5;

	// STAGE
	// because it might be used with screen dimensions
	// expressed as integers, we make it big enough
	// in order to avoid possible rounding problems.
	const float 			STAGE_WIDTH  = 1.0f;
	const float 			STAGE_HEIGHT = 1.0f;
	
	// FONTS
	const std::string 		FONT_PLAIN = "NotCourierSans";
	const std::string 		FONT_BOLD = "NotCourierSans:style=Bold";
	
	// OSC
	const std::string 		OSC_MAINLISTENER = "_main_";
	const int 			DEFAULT_OSC_MAINPORT = 9898;
	
	
	// TRACKING
	const int 			DEFAULT_TRACKING_MIN_AREA = 30;
	const int 			DEFAULT_TRACKING_MAX_AREA = 2000;
	const int 			DEFAULT_TRACKING_THRESHOLD = 80;
	const bool 			DEFAULT_TRACKING_CONTINUOUS = false;
	const int 			DEFAULT_TRACKING_BLUR =  3;
	
	// BODY 
	const int 			DEFAULT_BODY_UNBORN_AGE = 0;
	const int 			DEFAULT_BODY_UNBORN_HITS = 1;
	const int 			DEFAULT_BODY_UNBORN_LAST_HIT = 24;
	const int 			DEFAULT_BODY_BECOMING_AGE = 0;
	const int 			DEFAULT_BODY_BECOMING_HITS = 6;
	const int 			DEFAULT_BODY_BECOMING_LAST_HIT = 24;
	const int 			DEFAULT_BODY_PRESENT_LAST_HIT = 24;
	const int 			DEFAULT_BODY_ABSENT_LAST_HIT_MIN = 6;
	const int 			DEFAULT_BODY_ABSENT_LAST_HIT_MAX = 24;
	const float 			DEFAULT_BODY_MAXDISTANCE =  100.0f;
	const int			DEFAULT_BODY_VELSTEPS = 36;
	const float			DEFAULT_BODY_VELDISTANCE = 2000.0f;
	
	// DEVICES
	enum device_types {
		DEVICE_NONE,
		DEVICE_V4L,
		DEVICE_1394,
		DEVICE_KINECT
	};
}

#endif // OBJSCR_CONSTANTS_H



