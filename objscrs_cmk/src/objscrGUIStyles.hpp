#ifndef OBJSCR_GUISTYLES_H
#define OBJSCR_GUISTYLES_H


// objscrs
#include "objscrConstants.hpp"
#include "objscrColours.hpp"
#include "objscrGUI.hpp"
#include "singleton.h"
#include "objscrFontconfig.hpp"

// openframeworks
#include "ofMain.h"

// STL
#include <string>
#include <vector>
#include <map>
using namespace std;


namespace Objsc
{
	////////////////////////////////////////
	// WIDGET STYLE
	////////////////////////////////////////
	class WidgetStyle
	{
		private:
			string 			name;
			Objsc::Colour		bgColour; 
			Objsc::Colour		fgColour; 
			Objsc::Colour		hiColour; 
			Objsc::Colour		selColour;
			Objsc::Colour		disColour; 
			Objsc::Colour		textColour;
			ofTrueTypeFont 	font;
		
		public:
			WidgetStyle() {}
			WidgetStyle(string sname) {name=sname;}
			~WidgetStyle() {}
			// colour
			void setBackColour(int r, int g, int b, int a) {bgColour = makeColour(r, g, b, a);}
			void setBackColour(int r, int g, int b) {bgColour = makeColour(r, g, b);}
			void setBackColour(Objsc::Colour &col) {bgColour = col;}
			void setBackColour(const Objsc::Colour &col) {bgColour = col;}
			void setForeColour(int r, int g, int b, int a) {fgColour = makeColour(r, g, b, a);}
			void setForeColour(int r, int g, int b) {fgColour = makeColour(r, g, b);}
			void setForeColour(Objsc::Colour &col) {fgColour = col;}
			void setForeColour(const Objsc::Colour &col) {fgColour = col;}
			void setHilightColour(int r, int g, int b, int a) {hiColour = makeColour(r, g, b, a);}
			void setHilightColour(int r, int g, int b) {hiColour = makeColour(r, g, b);}
			void setHilightColour(Objsc::Colour &col) {hiColour = col;}
			void setHilightColour(const Objsc::Colour &col) {hiColour = col;}
			void setSelectedColour(int r, int g, int b, int a) {selColour = makeColour(r, g, b, a);}
			void setSelectedColour(int r, int g, int b) {selColour = makeColour(r, g, b);}
			void setSelectedColour(Objsc::Colour &col) {selColour = col;}
			void setSelectedColour(const Objsc::Colour &col) {selColour = col;}
			void setDisabledColour(int r, int g, int b, int a) {disColour = makeColour(r, g, b, a);}
			void setDisabledColour(int r, int g, int b) {disColour = makeColour(r, g, b);}
			void setDisabledColour(Objsc::Colour &col) {disColour = col;}
			void setDisabledColour(const Objsc::Colour &col) {disColour = col;}
			void setTextColour(int r, int g, int b, int a) {textColour = makeColour(r, g, b, a);}
			void setTextColour(int r, int g, int b) {textColour = makeColour(r, g, b);}
			void setTextColour(Objsc::Colour &col) {textColour = col;}
			void setTextColour(const Objsc::Colour &col) {textColour = col;}
			// font
			void setFont(const string &path, int size) 
			{ 
				ofEnableDataPath();
				font.loadFont(path, size);
				ofDisableDataPath();
			}
			// widget
			void apply(Objsc::Widget* widget)
			{
				widget->setBackColour(bgColour);
				widget->setForeColour(fgColour);
				widget->setHilightColour(hiColour);
				widget->setSelectedColour(selColour);
				widget->setDisabledColour(disColour);
				widget->setTextColour(textColour);
				widget->setFont(&font);
				widget->setStyleName(name);
			}
			void apply(Objsc::GuiContainer* widget)
			{
				widget->setBackColour(bgColour);
				widget->setForeColour(fgColour);
				widget->setHilightColour(hiColour);
				widget->setSelectedColour(selColour);
				widget->setDisabledColour(disColour);
				widget->setTextColour(textColour);
				widget->setFont(&font);
				widget->setStyleName(name);
			}
	};


	////////////////////////////////////////
	// STYLE PALETTE
	////////////////////////////////////////
	typedef map < string, Objsc::WidgetStyle* > WidgetStyleMap;
	typedef WidgetStyleMap::value_type WidgetStyleEntry;

	class StylePalette : public Singleton<StylePalette>
	{
		private:
			WidgetStyleMap styles;
		
		public:
			StylePalette() {}
			~StylePalette() {}
		
			WidgetStyle* createStyle(string name)
			{
				styles.insert(WidgetStyleEntry(name, new WidgetStyle(name)));
				WidgetStyle* style = styles[name];
				style->setBackColour(Objsc::BGCOLOUR);
				style->setForeColour(Objsc::FGCOLOUR);
				style->setHilightColour(Objsc::HICOLOUR);
				style->setSelectedColour(Objsc::SELCOLOUR);
				style->setDisabledColour(Objsc::DISCOLOUR);
				style->setTextColour(Objsc::TEXTCOLOUR);
				style->setFont(FontConfig::Current()->GetFont(FONT_PLAIN), 12);
				return style;
			}
			void applyStyle(string name, Objsc::Widget* widget)
			{
				WidgetStyleMap::iterator styleEntry = styles.find(name);
				if(styleEntry != styles.end()) {
					styleEntry->second->apply(widget);
				}
			}
			void applyStyle(string name, Objsc::GuiContainer* widget)
			{
				WidgetStyleMap::iterator styleEntry = styles.find(name);
				if(styleEntry != styles.end()) {
					styleEntry->second->apply(widget);
				}
			}
	};

}

#endif  //OBJSCR_GUISTYLES_H


