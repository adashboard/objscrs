#ifndef OBJSCR_TRACKING_DIALOGUE_H
#define OBJSCR_TRACKING_DIALOGUE_H


// objscrs
#include "objscrConstants.hpp"
#include "objscrGUI.hpp"
#include "objscrCommands.hpp"

// STL
#include <string>
using std::string;

// shared_ptr
#include <tr1/memory>


namespace Objsc
{
	class TrackingDialogue: public Objsc::Widget, public Objsc::CommandReceiver
	{
		
		protected:
			Objsc::NumberField		unbornAgeField;
			Objsc::NumberField		unbornHitsField;
			Objsc::NumberField		unbornLastHitField;
			Objsc::NumberField		becomingAgeField;
			Objsc::NumberField		becomingHitsField;
			Objsc::NumberField		becomingLastHitField;
			Objsc::NumberField		presentLastHitField;
			Objsc::NumberField		absentLastHitMaxField;
			Objsc::NumberField		absentLastHitMinField;
			Objsc::Button			actionBtn;
			Objsc::Button			cancelBtn;
			string				cmdlabel;
			bool				edititems;
		
		public:
			TrackingDialogue(const string& label);
			virtual ~TrackingDialogue() {}
			// edit items
			void sizeEditItems(bool value);
			const bool willEditItems() const { return edititems; }
			void setActionLabel(const string& label);
			void setActionType(const string& actiontype);
			// text
			void setText(const string& text);
			// finder
			void setFinder(Objsc::FinderPtr finderPtr) {finder=finderPtr;}
			void refresh();
			// geom
			void setLocation(float x, float y);
			void placeCentre();
			void setCentre(float x, float y);
			void setDimensions(float width, float height);
			bool withinBounds(float x, float y);
			bool isModal() const { return true;}
			// runtime
			void draw();
			// dialogues
			void open();
			void close();
			// commands
			void doCommand(Objsc::CommandPtr& cmd);
			void undoCommand(Objsc::CommandPtr& cmd);
			// events
			void keyPressed(int key);
			void keyReleased(int key);
			void mouseMoved(int x, int y );
			void mouseDragged(int x, int y, int button);
			void mousePressed(int x, int y, int button);
			void mouseReleased(int x, int y, int button);
			void windowResized(int w, int h);
			
			static std::tr1::shared_ptr<Objsc::TrackingDialogue> create(const string& label)
			{
				return std::tr1::shared_ptr<Objsc::TrackingDialogue>(new TrackingDialogue(label));
			}
	};
	
	
	typedef std::tr1::shared_ptr<Objsc::TrackingDialogue> TrackingDialoguePtr;
}

#endif  //OBJSCR_TRACKING_DIALOGUE_H


