#ifndef OBJSCR_FINDER_H
#define OBJSCR_FINDER_H

// STL
#include <string>
#include <vector>
using std::string;
using std::vector;


// shared_ptr
#include <tr1/memory>


namespace Objsc
{
	typedef vector< string > ItemNames;
	const ItemNames NO_ITEM_NAMES = ItemNames();
	
	class Finder
	{
		public:
			Finder() {}
			virtual ~Finder() {}
				
			virtual void addItem(const string& itemname){}
			virtual void selectItem(const string& itemname){}
			virtual void selectItem(int index){}
			virtual void handleInput(const string& input){}
			virtual void removeItem(const string& itemname){}
			virtual void removeItem(int index){}
			virtual void renameItem(const string& itemname, const string& newname){}
			virtual void renameItem(int index, const string& newname){}
			virtual const Objsc::ItemNames& getItemNames() { return NO_ITEM_NAMES;}
	};
	
	
	typedef std::tr1::shared_ptr<Objsc::Finder> FinderPtr;
	
}


#endif  //OBJSCR_FINDER_H

