#include "objscrDevices.hpp"


// boost
#include <boost/filesystem/operations.hpp>
#include <boost/filesystem/path.hpp>

namespace Objsc
{
	
	DevicePtrList DeviceManager::scanVideoDevices()
	{
		namespace fs = boost::filesystem;
		DevicePtrList devices;
		
		// V4L cameras
		fs::path full_path("/dev/");
		if (fs::exists(full_path)) 	{
			fs::directory_iterator end_iter;
			for (fs::directory_iterator dir_itr(full_path); dir_itr != end_iter; ++dir_itr )	{
				try
				{
				    std::string pathString(dir_itr->path().string());
				    if (pathString.find("video") != std::string::npos)  // devices matching video
					if (pathString.find("1394") == std::string::npos)   // that don't contain 1394
					    devices.push_back(Device::create(DEVICE_V4L, pathString));
				}
				catch (const std::exception & ex)
				{
				    std::cout << dir_itr->path() << " " << ex.what() << std::endl;
				}
			}
		}
		
		return devices;
	}
}