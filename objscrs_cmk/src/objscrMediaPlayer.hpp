#ifndef OBJSCR_MEDIAPLAYER_H
#define OBJSCR_MEDIAPLAYER_H


// objscrs
#include "singleton.h"
#include "objscrMediaViews.hpp"

// STL
#include <string>
using std::string;


namespace Objsc
{
	
	////////////////////////////////////////
	// MEDIA PLAYER
	////////////////////////////////////////
	class MediaPlayer : public Singleton<MediaPlayer>
	{
		private:
			Objsc::MediaViewLayerMap	medialayers;
			Objsc::MediaViewLabelMap	medialabels;
			
		public:
			MediaPlayer();
			~MediaPlayer();
		
			void update();
			void draw();
				
			// media
			Objsc::MediaViewPtr loadImage(const string& filename, const string& label);
			Objsc::MediaViewPtr loadVideo(const string& filename, const string& label);
			Objsc::MediaViewPtr createText(const string& label);
			Objsc::MediaViewPtr createDrawing(const string& label);
			
			const bool mediaLoaded(const string& label) const;
			Objsc::MediaViewPtr getMedia(const string& label);
			void remove(const string& label);
		
			void clearAll();
			
			// display
			void setLayer(const string& label, int layer);
			void show(const string& label, int layer=0);
			void hide(const string& label);
	};
	
	
	typedef std::tr1::shared_ptr<Objsc::MediaPlayer> MediaPlayerPtr;
	
	
}

#endif  //OBJSCR_MEDIAPLAYER_H

