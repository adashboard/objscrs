#ifndef OBJSCR_ZONEVIEWS_H
#define OBJSCR_ZONEVIEWS_H


// objscrs
#include "singleton.h"
#include "objscrConstants.hpp"
#include "objscrGUI.hpp"
#include "objscrGeom.hpp"
#include "objscrCommands.hpp"
#include "objscrZones.hpp"
#include "objscrZoneMap.hpp"

// STL
#include <string>
#include <vector>
#include <map>
#include <list>
using std::string;
using std::vector;
using std::map;
using std::list;

// shared_ptr
#include <tr1/memory>


namespace Objsc
{
	
	////////////////////////////////////////
	// FUNCTIONS
	////////////////////////////////////////
	void initZoneViews();
	
	////////////////////////////////////////
	// ZONE VIEW
	////////////////////////////////////////
	class ZoneView : public Objsc::Widget
	{
		protected:
			Objsc::ZonePtr 		zone;
			bool 				dodrag;
			int					scrX;
			int					scrY;
			gui_drag				dragdirection;
			Objsc::Rect			dragnorth;
			Objsc::Rect			dragnortheast;
			Objsc::Rect			drageast;
			Objsc::Rect			dragsoutheast;
			Objsc::Rect			dragsouth;
			Objsc::Rect			dragsouthwest;
			Objsc::Rect			dragwest;
			Objsc::Rect			dragnorthwest;
			Objsc::TextField		namefield;
			Objsc::ContextMenuPtr 	editmenu;
			bool				renaming;
	
			virtual void mapToScreen();
			virtual void mapToStage();
			void openMenu(int x, int y);
		
		public:
			ZoneView();
			virtual ~ZoneView() {}
			static const string stylename;
			void setZone(Objsc::ZonePtr zonePtr);
			Objsc::ZonePtr getZone() {return zone;}
			// geom
			virtual void setLocation(float x, float y);
			virtual void setDimensions(float width, float height);
			virtual void setBounds(float x, float y, float width, float height);
			// runtime
			void update();
			virtual void draw();
			// events
			void keyPressed(int key);
			void keyReleased(int key);
			void mouseMoved(int x, int y);
			void mouseDragged(int x, int y, int button);
			void mousePressed(int x, int y, int button);
			void mouseReleased(int x, int y, int button);
			void windowResized(int w, int h);
			void boundsResized();
			virtual void resetHandles();
			
			void startRename();
			void endRename();
			
			static std::tr1::shared_ptr<Objsc::ZoneView> create()
			{
				return std::tr1::shared_ptr<Objsc::ZoneView>(new ZoneView());
			}
	};
	
	typedef std::tr1::shared_ptr<Objsc::ZoneView> ZoneViewPtr;
	typedef vector < Objsc::ZoneViewPtr > ZoneViewVec;
	

	////////////////////////////////////////
	// CIRCLE ZONE VIEW
	////////////////////////////////////////
	class CircleZoneView : public Objsc::ZoneView
	{
		protected:
			Objsc::Circle		circle;
			bool			drawlines;
		
			void mapToScreen();
			void mapToStage();
			void drawProxLines();
		
		public:
			CircleZoneView();
			virtual ~CircleZoneView() {}
			static const string stylename;
				
			void draw();
			
			static Objsc::ZoneViewPtr create()
			{
				return Objsc::ZoneViewPtr(new CircleZoneView());
			}
	};
	
	////////////////////////////////////////
	// GRID ZONE VIEW
	////////////////////////////////////////
	class GridZoneView : public Objsc::ZoneView
	{
		protected:
			int				cols;
			int				rows;
			float			colWidth;
			float			rowHeight;
		
			void mapToScreen();
			void mapToStage();
		
			void resetGrid();
		
		public:
			GridZoneView();
			virtual ~GridZoneView() {}
			static const string stylename;
				
			void resetHandles();
			void draw();
			
			static Objsc::ZoneViewPtr create()
			{
				return Objsc::ZoneViewPtr(new GridZoneView());
			}
	};
	
	////////////////////////////////////////
	// ZONE VIEW CREATORS
	////////////////////////////////////////
	class ZoneViewCreator
	{
		public:
			virtual ~ZoneViewCreator() {}
			virtual Objsc::ZoneViewPtr create() const =0;
	};
	
	class BasicZoneViewCreator : public ZoneViewCreator
	{
		public:
			Objsc::ZoneViewPtr create() const { return Objsc::ZoneViewPtr(new Objsc::ZoneView());}
	};
	
	class CircleZoneViewCreator : public ZoneViewCreator
	{
		public:
			Objsc::ZoneViewPtr create() const { return Objsc::ZoneViewPtr(new Objsc::CircleZoneView());}
	};
	
	class GridZoneViewCreator : public ZoneViewCreator
	{
		public:
			Objsc::ZoneViewPtr create() const { return Objsc::ZoneViewPtr(new Objsc::GridZoneView());}
	};
	
	////////////////////////////////////////
	// ZONE VIEW FACTORY
	////////////////////////////////////////
	typedef map < string, ZoneViewCreator* > ZoneViewCreatorMap;
	typedef ZoneViewCreatorMap::value_type ZoneViewCreatorEntry;

	class ZoneViewFactory : public Singleton<ZoneViewFactory>
	{
		private:
			ZoneViewCreatorMap creators;
		
		public:
			ZoneViewFactory() : creators() {}
			~ZoneViewFactory() { creators.clear(); }
			
			Objsc::ZoneViewPtr create(string zonetype)
			{
				ZoneViewCreatorMap::iterator iter = creators.find(zonetype);
				if(iter == creators.end()) {
					throw std::exception();
				}
				return (*iter).second->create();
			}
			
			void addCreator(string zonetype, ZoneViewCreator* creatorPtr)
			{
				creators.insert(ZoneViewCreatorEntry(zonetype, creatorPtr));
			}
			
			bool hasCreator(string zonetype)	
			{
				return creators.count(zonetype) > 0;
			}
			
	};
	
	
	////////////////////////////////////////
	// ZONE MAP
	////////////////////////////////////////
	class ZoneMapView : public Objsc::Widget
	{
		private:
			Objsc::ZoneMapPtr		zonemap;
			Objsc::ZoneViewVec 	zoneviews;
		
		public:
			ZoneMapView();
			~ZoneMapView() {}
				
			void setZoneMap(Objsc::ZoneMapPtr zonemapPtr);
			
			void update();
			void draw();
				
			// geom
			virtual void setLocation(float x, float y);
			virtual void setDimensions(float width, float height);
			virtual void setBounds(float x, float y, float width, float height);
			
			// events
			void keyPressed(int key);
			void keyReleased(int key);
			void mouseMoved(int x, int y );
			void mouseDragged(int x, int y, int button);
			void mousePressed(int x, int y, int button);
			void mouseReleased(int x, int y, int button);
			void windowResized(int w, int h);
				
			static std::tr1::shared_ptr<Objsc::ZoneMapView> create()
			{
				return std::tr1::shared_ptr<Objsc::ZoneMapView>(new ZoneMapView());
			}
			
			void createZone(float scrX, float scrY, const string& zonetype=Objsc::BasicZone::zonetype);
			void removeZone(Objsc::ZoneViewPtr zoneview);
			void clearViews();
	};
	
	typedef std::tr1::shared_ptr<Objsc::ZoneMapView> ZoneMapViewPtr;
	
}

#endif  //OBJSCR_ZONEVIEWS_H
