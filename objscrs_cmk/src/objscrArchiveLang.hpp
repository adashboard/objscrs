#ifndef  OBJSCR_ARCHIVELANG_H
#define OBJSCR_ARCHIVELANG_H

// objscrs
#include "singleton.h"
#include "objscrLang.hpp"


// shared_ptr
#include <tr1/memory>

#include <string>
#include <map>
#include <exception>
using std::string;
using std::map;
using std::exception;


// xml
#include "tinyxml.h"



namespace Objsc
{
	
	/////////////////////////////////////
	// FUNCTIONS
	/////////////////////////////////////
	void initLangArchivers();
	
	
	////////////////////////////////////////
	// LANG OBJECT ARCHIVER
	////////////////////////////////////////
	class LangObjectArchiver
	{
		public:
			LangObjectArchiver() {}
			virtual ~LangObjectArchiver() {}
				
			virtual void encode(TiXmlElement *parentNode, Objsc::LangObjectPtr object, int encID=-1) {};
			virtual void decode(TiXmlElement *parentNode, Objsc::LangObjectPtr object) {};
	};
	
	typedef std::tr1::shared_ptr<Objsc::LangObjectArchiver> LangObjectArchiverPtr;
	
	
	
	////////////////////////////////////////
	// DEFAULT LANG OBJECT ARCHIVER
	////////////////////////////////////////
	class DefaultLangObjectArchiver : public LangObjectArchiver
	{
		public:
			DefaultLangObjectArchiver() {}
			virtual ~DefaultLangObjectArchiver() {}
				
			void encode(TiXmlElement *parentNode, Objsc::LangObjectPtr object, int encID=-1);
			void decode(TiXmlElement *parentNode, Objsc::LangObjectPtr object);
	};
	
	////////////////////////////////////////
	// LANG MESSAGE ARCHIVER
	////////////////////////////////////////
	class LangMessageArchiver : public LangObjectArchiver
	{
		public:
			LangMessageArchiver() {}
			virtual ~LangMessageArchiver() {}
				
			void encode(TiXmlElement *parentNode, Objsc::LangObjectPtr object, int encID=-1);
			void decode(TiXmlElement *parentNode, Objsc::LangObjectPtr object);
	};
	
	////////////////////////////////////////
	// LANG MEDIA OBJECT ARCHIVER
	////////////////////////////////////////
	class LangMediaObjectArchiver : public LangObjectArchiver
	{
		public:
			LangMediaObjectArchiver() {}
			virtual ~LangMediaObjectArchiver() {}
				
			void encode(TiXmlElement *parentNode, Objsc::LangObjectPtr object, int encID=-1);
			void decode(TiXmlElement *parentNode, Objsc::LangObjectPtr object);
	};
	
	
	////////////////////////////////////////
	// CREATORS
	////////////////////////////////////////
	class LangObjectArchiverCreator
	{
		public:
			virtual ~LangObjectArchiverCreator() {}
			virtual Objsc::LangObjectArchiverPtr create() const =0;
	};
	
	class DefaultLangObjectArchiverCreator: public LangObjectArchiverCreator
	{
		public:
			Objsc::LangObjectArchiverPtr create() const {return Objsc::LangObjectArchiverPtr(new Objsc::DefaultLangObjectArchiver());}
	};
	
	
	
	class LangMessageArchiverCreator: public LangObjectArchiverCreator
	{
		public:
			Objsc::LangObjectArchiverPtr create() const {return Objsc::LangObjectArchiverPtr(new Objsc::LangMessageArchiver());}
	};
	
	
	class LangMediaObjectArchiverCreator: public LangObjectArchiverCreator
	{
		public:
			Objsc::LangObjectArchiverPtr create() const {return Objsc::LangObjectArchiverPtr(new Objsc::LangMediaObjectArchiver());}
	};
	
	////////////////////////////////////////
	// FACTORY
	////////////////////////////////////////
	typedef map < string, LangObjectArchiverCreator* > LangObjectArchiverCreatorMap;
	typedef LangObjectArchiverCreatorMap::value_type LangObjectArchiverCreatorEntry;

	class LangObjectArchiverFactory : public Singleton<LangObjectArchiverFactory>
	{
		private:
			Objsc::LangObjectArchiverCreatorMap creators;
		
		public:
			LangObjectArchiverFactory() : creators() {}
			~LangObjectArchiverFactory() { creators.clear(); }
			
			Objsc::LangObjectArchiverPtr create(string langname)
			{
				Objsc::LangObjectArchiverCreatorMap::iterator iter = creators.find(langname);
				if(iter == creators.end()) {
					return creators["default"]->create();
				}
				return (*iter).second->create();
			}
			
			void addCreator(string langname, LangObjectArchiverCreator* creatorPtr)
			{
				creators.insert(LangObjectArchiverCreatorEntry(langname, creatorPtr));
			}
			
			bool hasCreator(string langname)	{return creators.count(langname) > 0;}
			
	};
}

#endif // OBJSCR_ARCHIVELANG_H
