#include "objscrZones.hpp"

// objscrs
#include "objscrConstants.hpp"
#include "objscrLangAtoms.hpp"
#include "objscrSettings.hpp"

// STL
#include <algorithm>
#include <functional>

namespace Objsc
{
	
	////////////////////////////////////////
	// DECLARATIONS
	////////////////////////////////////////
	/*
	will become new names
	const string Objsc::BasicZone::zonetype = "squarezone";
	const string Objsc::CircleZone::zonetype = "pointzone";
	*/
	const string Objsc::BasicZone::zonetype = "basiczone";
	const string Objsc::CircleZone::zonetype = "circlezone";
	const string Objsc::GridZone::zonetype = "gridzone";
	const int Objsc::BasicZone::zonetypeID = ZONE_SQUARE;
	const int Objsc::CircleZone::zonetypeID = ZONE_POINT;
	const int Objsc::GridZone::zonetypeID = ZONE_GRID;
	
	
	/////////////////////////////////////
	// FUNCTIONS
	/////////////////////////////////////
	void initZones()
	{
		Objsc::ZoneFactory* factory = Objsc::ZoneFactory::Current();
		factory->addCreator(Objsc::BasicZone::zonetype, new Objsc::BasicZoneCreator());
		factory->addCreator(Objsc::CircleZone::zonetype, new Objsc::CircleZoneCreator());
		factory->addCreator(Objsc::GridZone::zonetype, new Objsc::GridZoneCreator());
	}
	
	struct matchesBody : std::binary_function< Objsc::BodyHolder, Objsc::BodyPtr, bool >	{
		bool operator() (const Objsc::BodyHolder& holder, const Objsc::BodyPtr& body) const	{
			if(holder.getBody() == body)	{
				holder.update();
				return true;
			}
			return false;
		}
	};
	
	inline void resetBodyHolder(Objsc::BodyHolder& holder)
	{
		holder.reset();
	}
	
	inline bool bodyHasLeft(Objsc::BodyHolder& holder)
	{
		return !holder.isIn();
	}
	
	
	Objsc::LangPatchPtr createZonePatch(const string& name)
	{
		Objsc::LangPatchPtr patch = Objsc::LangPatch::create(name);
		patch->addObjectToReceiver("body_enter", patch->createObject("body_enter", 0.1f, 0.1f));
		patch->addObjectToReceiver("body_within", patch->createObject("body_within", 0.4f, 0.1f));
		patch->addObjectToReceiver("body_exit", patch->createObject("body_exit", 0.7f, 0.1f));
		return patch;
	}
	
	
	Objsc::LangAtomPtr zoneDataToAtom(const string& zonename, int zoneTypeID, const Objsc::BodyPtr& body)
	{
		Objsc::LangList* atom = new Objsc::LangList();
		atom->add(zonename); // 0 - string
		atom->add(zoneTypeID); // 1 - int
		atom->add((int)(body->getStageNum())); //2 - int
		atom->add(body->getAge()); //3 - int
		atom->add(body->getStageX()); //4 - float
		atom->add(body->getStageY()); //5 - float
		atom->add(body->getStageWidth()); //6 - float
		atom->add(body->getStageHeight()); //7 - float
		atom->add(body->getStageArea()); //8 - float
		atom->add(body->getBodyArea()); //9 - float
		return Objsc::LangAtomPtr(atom);
	}
	
	Objsc::LangAtomPtr proxDataToAtom(const string& zonename, int zoneTypeID, const Objsc::BodyPtr& body, float prox)
	{
		Objsc::LangList* atom = new Objsc::LangList();
		atom->add(zonename); // 0
		atom->add(zoneTypeID); // 1
		atom->add((int)(body->getStageNum())); // 2
		atom->add(body->getAge()); // 3
		atom->add(body->getStageX()); // 4
		atom->add(body->getStageY()); // 5
		atom->add(body->getStageWidth()); // 6
		atom->add(body->getStageHeight()); // 7
		atom->add(body->getStageArea()); // 8
		atom->add(body->getBodyArea()); // 9
		atom->add(prox); // 10
		return Objsc::LangAtomPtr(atom);
	}
	
	
	Objsc::LangAtomPtr zoneBoundsToAtom(const string &zoneName, const Objsc::Rect& bounds)
	{
		Objsc::LangList* atom = new Objsc::LangList();
		atom->add(zoneName);
		atom->add(bounds.getX());
		atom->add(bounds.getY());
		atom->add(bounds.getWidth());
		atom->add(bounds.getHeight());
		return Objsc::LangAtomPtr(atom);
	}
	
	////////////////////////////////////////
	// BASIC ZONE
	////////////////////////////////////////
	BasicZone::BasicZone():name(),bounds(),patch(),bodies(),bodyAtom(),refresh(false)
	{
		bounds.set(0,0, Objsc::ZONE_DFTWIDTH, Objsc::ZONE_DFTHEIGHT);
	}
	
	Objsc::LangPatchPtr BasicZone::getPatch()
	{
		if(!patch)	{
			patch = createZonePatch(name);
		}
		return patch;
	}
	
	void BasicZone::receive(const string& label, Objsc::LangAtomPtr atom)
	{
		//printf("BasicZone::receive %s <%s> A\n", name.c_str(), label.c_str());
		if(patch)	{
			//printf("BasicZone::receive %s <%s> B\n", name.c_str(), label.c_str());
			patch->receive(label, atom);
		}
	}
	
	void BasicZone::onload()
	{
		if(patch)	{
			patch->onload();
		}
	}
	
	void BasicZone::update(int timeElapsed)
	{
		if(patch)	{
			patch->update(timeElapsed);
		}
	}
	
	// bodies
	void BasicZone::updateBodies(const Objsc::BodyPtrList&  bodylist)
	{
		//printf("BasicZone::updateBodies <%s>\n", name.c_str());
		//for_each(bodies.begin(), bodies.end(), resetBodyHolder);
		for(Objsc::BodyHolderVec::iterator holder = bodies.begin(); holder != bodies.end(); ++holder)	{
			(*holder).reset();
		}
		for(Objsc::BodyPtrList::const_iterator body = bodylist.begin(); body != bodylist.end(); ++body)	{
			//printf("    body [%u]\n", (*body)->getID());
			//(*body)->print();
			if((*body)->isAlive())
			{
				if(withinBounds((*body)->getStageX(), (*body)->getStageY()))	{
					//printf("    body [%u] INSIDE\n", (*body)->getID());
					if(hasBody((*body)))	{
						//printf("within\n");
						bodyWithin((*body));
					} else {
						//printf("ENTER\n");
						bodyEnter((*body));
					}
				}
			}
		}
		for(Objsc::BodyHolderVec::iterator holder = bodies.begin(); holder != bodies.end(); ++holder)	{
			if(!(*holder).isIn())	{
				bodyExit((*holder).getBody());
			}
		}
		bodies.erase(remove_if(bodies.begin(), bodies.end(), bodyHasLeft), bodies.end());
	}
	
	bool BasicZone::hasBody(const Objsc::BodyPtr& body)
	{
		return find_if(bodies.begin(), bodies.end(), bind2nd(matchesBody(), body)) != bodies.end();
	}
	
	void BasicZone::bodyEnter(Objsc::BodyPtr body)
	{
		if(Settings::Current()->hasSetting("Debug"))
			std::cerr<<"Body Enter ["<< body->getUID() << "]"<<std::endl;
		bodies.push_back(Objsc::BodyHolder(body));
		if(patch)	{
			patch->receive("body_enter", Objsc::zoneDataToAtom(name, getZoneTypeID(), body));
		}
	}
	
	void BasicZone::bodyExit(Objsc::BodyPtr body)
	{
		if(Settings::Current()->hasSetting("Debug"))
			std::cerr<<"Body Exit ["<< body->getUID() << "]"<<std::endl;
		if(patch)	{
			patch->receive("body_exit", Objsc::zoneDataToAtom(name, getZoneTypeID(), body));
		}
	}
	
	void BasicZone::bodyWithin(Objsc::BodyPtr body)
	{
		if(patch)	{
			patch->receive("body_within", Objsc::zoneDataToAtom(name, getZoneTypeID(), body));
		}
	}
	
	// patch
	void BasicZone::setName(const string& nname) 
	{ 
		name.assign(nname);
		if(patch)	{
			patch->setName(nname);
		}
	}
	
	void BasicZone::labelBody(int index, const string &label)
	{
		if(index < bodies.size())	{
			bodies[index].getBody()->setLabel(label);
		}
	}
	
	
	////////////////////////////////////////
	// CIRCLE ZONE
	////////////////////////////////////////
	CircleZone::CircleZone() : circle()
	{
		
	}
	
	void CircleZone::setLocation(float x, float y) 
	{ 
		bounds.setCentre(x,y); 
		circle.setCentre(x,y);
	}
	void CircleZone::setDimensions(float width, float height) 
	{ 
		if(width < height) {
			height = width;
		} else {
			width = height;
		}
		bounds.setDimensions(width,height);
		circle.setRadius(width/2.0f);
		//bounds.print();
		//circle.print();
	}
	void CircleZone::setBounds(float x, float y, float width, float height) 
	{ 
		if(width > height) {
			height = width;
		} else {
			width = height;
		}
		bounds.set(x,y,width,height);
		circle.set(bounds.getCentreX(),bounds.getCentreY(),width/2.0f);
		//bounds.print();
		//circle.print();
	}
	
	void CircleZone::bodyEnter(Objsc::BodyPtr body)
	{
		bodies.push_back(Objsc::BodyHolder(body));
		if(patch)	{
			//patch->receive("body_enter", Objsc::proxDataToAtom(body, bodies.size(), proximity(body)));
			patch->receive("body_enter", Objsc::proxDataToAtom(name, getZoneTypeID(), body, proximity(body)));
		}
	}
	
	void CircleZone::bodyExit(Objsc::BodyPtr body)
	{
		if(patch)	{
			//patch->receive("body_exit", Objsc::proxDataToAtom(body, bodies.size(), proximity(body)));
			patch->receive("body_exit", Objsc::proxDataToAtom(name, getZoneTypeID(), body, proximity(body)));
		}
	}
	
	void CircleZone::bodyWithin(Objsc::BodyPtr body)
	{
		//printf("bodyWithin %f\n", proximity(body));
		if(patch)	{
			//patch->receive("body_within", Objsc::proxDataToAtom(body, bodies.size(), proximity(body)));
			patch->receive("body_within", Objsc::proxDataToAtom(name, getZoneTypeID(), body, proximity(body)));
		}
	}
	
	
	////////////////////////////////////////
	// GRID ZONE
	////////////////////////////////////////
	GridZone::GridZone() : cols(4), rows(4), colWidth(0.0), rowHeight(0.0), cells()
	{
		setBounds(0,0, Objsc::ZONE_DFTWIDTH, Objsc::ZONE_DFTHEIGHT);
	}
	
	void GridZone::setCols(int value)
	{
		cols = MAX(value, 0);
		sizeGrid();
	}
	
	void GridZone::setRows(int value)
	{
		rows = MAX(value, 0);
		sizeGrid();
	}
	
	void GridZone::setGrid(int newCols, int newRows)
	{
		cols = MAX(newCols, 0);
		rows = MAX(newRows, 0);
		sizeGrid();
	}
	
	void GridZone::sizeGrid()
	{
		if(cols > 0)
			colWidth = (float)(bounds.getWidth()/cols);
		if(rows > 0)
			rowHeight = (float)(bounds.getHeight()/rows);
		cells.empty();
		for (int i=1;i<cols*rows;i++) cells.push_back(0);
	}
	
	void GridZone::update(int timeElapsed)
	{
		updateGrid();
		if(patch)	{
			patch->update(timeElapsed);
		}
	}
	
	void GridZone::updateGrid()
	{
		int x,y=0;
		for (int i=1;i<cols*rows;i++) cells[i]=0;
		for(Objsc::BodyHolderVec::iterator holder = bodies.begin(); holder != bodies.end(); ++holder)	{
			if(colWidth > 0.0f)	{
				x = (int)((*holder).getStageX()/colWidth);
			} else {
				x=0;
			}
			if(rowHeight > 0.0f)	{
				y = (int)((*holder).getStageY()/rowHeight);
			} else {
				y=0;
			}
			cells[(y*cols)+x]++;
		}
	}
	
	void GridZone::setLocation(float x, float y) 
	{ 
		bounds.setCentre(x,y); 
		sizeGrid();
	}
	void GridZone::setDimensions(float width, float height) 
	{ 
		bounds.setDimensions(width,height);
		sizeGrid();
	}
	void GridZone::setBounds(float x, float y, float width, float height) 
	{ 
		bounds.set(x,y,width,height);
		sizeGrid();
	}
	
	void GridZone::bodyEnter(Objsc::BodyPtr body)
	{
		bodies.push_back(Objsc::BodyHolder(body));
		if(patch)	{
			patch->receive("body_enter", Objsc::zoneDataToAtom(name, getZoneTypeID(), body));
		}
	}
	
	void GridZone::bodyExit(Objsc::BodyPtr body)
	{
		if(patch)	{
			patch->receive("body_exit", Objsc::zoneDataToAtom(name, getZoneTypeID(), body));
		}
	}
	
	void GridZone::bodyWithin(Objsc::BodyPtr body)
	{
		if(patch)	{
			patch->receive("body_within", Objsc::zoneDataToAtom(name, getZoneTypeID(), body));
		}
	}

}

