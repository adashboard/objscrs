#ifndef OBJSCR_GUI_H
#define OBJSCR_GUI_H


// objscrs
#include "objscrConstants.hpp"
#include "objscrColours.hpp"
#include "objscrGeom.hpp"
#include "singleton.h"
#include "objscrCommands.hpp"

// openframeworks
#include "ofMain.h"

// STL
#include <string>
#include <stdlib.h>
#include <vector>
#include <map>
#include <list>
using std::string;
using std::vector;
using std::map;
using std::list;

// shared_ptr
#include <tr1/memory>


namespace Objsc
{
	////////////////////////////////////////
	// FUNCTIONS
	////////////////////////////////////////
	void outlineBox(Objsc::Rect& rect);
	void outlineBox(float x1, float y1, float x2, float y2);
	void crossXLines(Objsc::Rect& rect);


	////////////////////////////////////////
	// CLASSES
	////////////////////////////////////////
	class GuiBase
	{
		protected:
			Objsc::Rect 	bounds;
			bool 		active;
			bool		visible; 
			bool		consumed;
			bool		hasFocus;
		
		public:
			GuiBase() : bounds(), active(true), visible(true), consumed(false), hasFocus(false) {}
			virtual ~GuiBase() {}
				
			virtual bool isActive() const { return active; }
			virtual void enable() { active = true; }
			virtual void disable() { active = false; }
			void show() {visible = true;}
			void hide() {visible = false;}
			const bool isVisible() const { return visible; }
			virtual const string& getText() { return Objsc::NULL_STRING; }
			virtual const string& getText() const { return Objsc::NULL_STRING; }
			// geom
			virtual bool withinBounds(float x, float y) { return bounds.containsPoint(x,y); }
			virtual void setLocation(float x, float y) { bounds.setLocation(x,y); }
			virtual void setDimensions(float width, float height) { bounds.setDimensions(width,height); }
			virtual void setBounds(float x, float y, float width, float height) { bounds.set(x,y,width,height); }
			virtual void setX(float x) { bounds.setX(x); }
			virtual void setY(float y) { bounds.setY(y); }
			virtual void setWidth(float width) { bounds.setWidth(width); }
			virtual void setHeight(float height) { bounds.setHeight(height); }
			virtual float getX() const {return bounds.getX();}
			virtual float getY() const {return bounds.getY();}
			virtual float getRight() const {return bounds.getRight();}
			virtual float getBottom() const {return bounds.getBottom();}
			virtual float getWidth() const {return bounds.getWidth();}
			virtual float getHeight() const {return bounds.getHeight();}
			virtual void resize() {}
			virtual bool isModal() const { return false;}
			// runtime
			virtual void update() {}
			virtual void draw() {}
			// focus
			virtual void gainFocus() {hasFocus=true;}
			virtual void looseFocus() {hasFocus=false;}
			// events
			const bool eventConsumed() const { return consumed; }
			virtual void keyPressed(int key) {}
			virtual void keyReleased(int key) {}
			virtual void mouseMoved(int x, int y ) {}
			virtual void mouseDragged(int x, int y, int button) {}
			virtual void mousePressed(int x, int y, int button) {}
			virtual void mouseReleased(int x, int y, int button) {}
			virtual void windowResized(int w, int h) {} 
		
	};


	typedef vector < Objsc::GuiBase* > GuiBaseVector;
	typedef std::tr1::shared_ptr<Objsc::GuiBase> GuiBasePtr;
	typedef std::tr1::weak_ptr<Objsc::GuiBase> GuiBaseWeakPtr;
	typedef map < string, Objsc::GuiBasePtr > GuiBasePtrMap;
	typedef GuiBasePtrMap::value_type GuiBasePtrMapEntry;

	class GuiContainer : public GuiBase
	{
		
		protected:
			Objsc::GuiBase* 		parent;
			Objsc::GuiBaseVector 	children;
			string			stylename;
			
		public:
			GuiContainer() : stylename("default") {}
			virtual ~GuiContainer() {}
				
			virtual void clear() { children.clear(); }
			// runtime
			void update()
			{
				for (Objsc::GuiBaseVector::iterator child = children.begin(); child != children.end(); ++child)	{
					(*child)->update();
				}
			}
			virtual void draw()
			{
				if(visible) {
					for (Objsc::GuiBaseVector::iterator child = children.begin(); child != children.end(); ++child)	{
						(*child)->draw();
					}
				}
			}
			// style
			virtual void setStyleName(string sname) { stylename=sname; }
			const string getStyleName() const { return stylename; }
			virtual void setFont(ofTrueTypeFont* fontPtr) {}
			virtual void setBackColour(Objsc::Colour &col) {}
			virtual void setForeColour(Objsc::Colour &col) {}
			virtual void setHilightColour(Objsc::Colour &col) {}
			virtual void setSelectedColour(Objsc::Colour &col) {}
			virtual void setDisabledColour(Objsc::Colour &col) {}
			virtual void setTextColour(Objsc::Colour &col) {}
			// events
			virtual void keyPressed(int key);
			virtual void keyReleased(int key);
			virtual void mouseMoved(int x, int y );
			virtual void mouseDragged(int x, int y, int button);
			virtual void mousePressed(int x, int y, int button);
			virtual void mouseReleased(int x, int y, int button);
			virtual void windowResized(int w, int h);
	};

	typedef std::tr1::shared_ptr<Objsc::GuiContainer> GuiContainerPtr;


	////////////////////////////////////////
	// WIDGETS BASE
	////////////////////////////////////////
	class Widget : public GuiBase
	{
		
		protected:
			Objsc::GuiBase* 	parent;
			Objsc::Colour	bgColour; 
			Objsc::Colour	fgColour; 
			Objsc::Colour	hiColour; 
			Objsc::Colour	selColour;
			Objsc::Colour	disColour; 
			Objsc::Colour	textColour; 
			int			displayState; 
			bool		selected;
			string		stylename;
			
		public:
			Widget() : displayState(Objsc::ENABLED),selected(false),stylename("default") {}
			virtual ~Widget() {}
			// parent
			void setParent(Objsc::GuiBase* parentPtr) { parent=parentPtr; }
			Objsc::GuiBase* getParent() { return parent; }
			// events
			const bool eventConsumed() const { return consumed; }
			// colour
			void setBackColour(int r, int g, int b, int a) {bgColour = makeColour(r, g, b, a);}
			void setBackColour(int r, int g, int b) {bgColour = makeColour(r, g, b);}
			void setBackColour(Objsc::Colour &col) {bgColour = col;}
			void setForeColour(int r, int g, int b, int a) {fgColour = makeColour(r, g, b, a);}
			void setForeColour(int r, int g, int b) {fgColour = makeColour(r, g, b);}
			void setForeColour(Objsc::Colour &col) {fgColour = col;}
			void setHilightColour(int r, int g, int b, int a) {hiColour = makeColour(r, g, b, a);}
			void setHilightColour(int r, int g, int b) {hiColour = makeColour(r, g, b);}
			void setHilightColour(Objsc::Colour &col) {hiColour = col;}
			void setSelectedColour(int r, int g, int b, int a) {selColour = makeColour(r, g, b, a);}
			void setSelectedColour(int r, int g, int b) {selColour = makeColour(r, g, b);}
			void setSelectedColour(Objsc::Colour &col) {selColour = col;}
			void setDisabledColour(int r, int g, int b, int a) {disColour = makeColour(r, g, b, a);}
			void setDisabledColour(int r, int g, int b) {disColour = makeColour(r, g, b);}
			void setDisabledColour(Objsc::Colour &col) {disColour = col;}
			void setTextColour(int r, int g, int b, int a) {textColour = makeColour(r, g, b, a);}
			void setTextColour(int r, int g, int b) {textColour = makeColour(r, g, b);}
			void setTextColour(Objsc::Colour &col) {textColour = col;}
			virtual void drawBackground(bool value) {}
			// state
			void enable() {displayState = Objsc::ENABLED;}
			void disable() {displayState = Objsc::DISABLED;}
			// display
			virtual void draw() {}
			// text
			virtual void setText(char* newtext) {}
			virtual void setText(const string& newtext) {}
			virtual void clearText() {}
			virtual void setFont(ofTrueTypeFont* fontPtr) {}
			virtual void setFont(const string& fontfile, int size) {}
			// style
			void setStyleName(string sname) { stylename=sname; }
			const string getStyleName() const { return stylename; }
			
	};

	////////////////////////////////////////
	// IMAGE
	////////////////////////////////////////
	class Image : public GuiBase
	{
		private:
			ofTexture			texture;
			unsigned char* 	pixels;
			unsigned char*	colpix;
			Objsc::Colour		colour;
			int				pWidth;
			int				pHeight;
			string			name;
			bool			loaded;
			
			void paint();
		
		public:
			Image() : pWidth(0), pHeight(0), loaded(false) {colour = makeColour(0, 0, 0, 255);}
			~Image() {delete[] pixels;}
			// image
			void load(string imgname, int width, int height);
			void resize(int width, int height);
			void setColour(int r, int g, int b, int a) 
			{
				colour = makeColour(r, g, b, a);
				paint();
			}
			void setColour(int r, int g, int b) 
			{
				colour = makeColour(r, g, b);
				paint();
			}
			void setColour(const Objsc::Colour& col) 
			{
				colour = col;
				paint();
			}
			// geom
			void setCentre(float x, float y) { bounds.setCentre(x,y); }
			void setDimensions(float width, float height) { resize(width,height); }
			void setBounds(float x, float y, float width, float height) 
			{ 
				bounds.setLocation(x,y);
				resize(width,height);
			}
			// runtime
			void draw();
	};

	////////////////////////////////////////
	// HORIZONTAL SLIDER
	////////////////////////////////////////
	class HorizontalSlider : public Widget
	{
		protected:
			float 		value;
			bool		dodrag;
			bool		showcentre;
			Objsc::Rect 		sliderect;
			Objsc::Rect 		scrollrect;
		
			void scrollUpdated();
			
		public:
			HorizontalSlider() : value(0.0f), dodrag(false), showcentre(false){ stylename="slider";}
			~HorizontalSlider() {}
			// geom
			void setLocation(float x, float y) 
			{ 
				sliderect.setLocation(x+(sliderect.getX()-bounds.getX()),y);
				bounds.setLocation(x,y); 
				scrollrect.setLocation(bounds.getX()+Objsc::TIMELINESCROLLHEIGHT, bounds.getY());
			}
			void setDimensions(float width, float height) 
			{ 
				bounds.setDimensions(width,height);
				scrollrect.setDimensions(bounds.getWidth()-(Objsc::TIMELINESCROLLHEIGHT*2),bounds.getHeight());
				sliderect.setDimensions(Objsc::TIMELINESCROLLHEIGHT*2,bounds.getHeight());
			}
			void setBounds(float x, float y, float width, float height) 
			{ 
				setLocation(x,y);
				setDimensions(width,height);
			}
			// value
			void setValue(float fvalue) 
			{ 
				value=fvalue<1.0f?(fvalue>0.0f?fvalue:0.0f):1.0f;
				sliderect.setCentreX(scrollrect.getX()+(scrollrect.getWidth()*value));
			}
			const float getValue() const { return value; }
			// display
			void draw();
			void showCentre() {showcentre=true;}
			void hideCentre() {showcentre=false;}
			// events
			void keyPressed(int key);
			void keyReleased(int key);
			void mouseMoved(int x, int y);
			void mouseDragged(int x, int y, int button);
			void mousePressed(int x, int y, int button);
			void mouseReleased(int x, int y, int button);
			
	};

	////////////////////////////////////////
	// RANGE SLIDER
	////////////////////////////////////////
	class RangeSlider : public Widget
	{
		protected:
			float 		minValue;
			float 		maxValue;
			bool		dodrag;
			bool		showcentre;
			Objsc::Rect 		sliderect;
		
			void scrollUpdated();
			inline float scaleToWidth(float value) {return ofMap(value, 0.0f, 1.0f, 0.0f, bounds.getWidth());}
			inline float scaleToPos(float value) {return ofMap(value, 0.0f, 1.0f, bounds.getLeft(),bounds.getRight());}
			inline float widthToScale(float value) {return ofMap(value, 0.0f, bounds.getWidth(), 0.0f, 1.0f);}
			inline float posToScale(float value) {return ofMap(value, bounds.getLeft(),bounds.getRight(), 0.0f, 1.0f);}
			
		public:
			RangeSlider() : minValue(0.0f), maxValue(1.0f), dodrag(false), showcentre(false){ stylename="slider";}
			~RangeSlider() {}
			// geom
			void setLocation(float x, float y) 
			{ 
				bounds.setLocation(x,y);
				sliderect.setLocation(scaleToPos(minValue), bounds.getY());
			}
			void setDimensions(float width, float height) 
			{ 
				bounds.setDimensions(width,height);
				sliderect.setDimensions(scaleToWidth(maxValue-minValue),bounds.getHeight());
			}
			void setBounds(float x, float y, float width, float height) 
			{ 
				setLocation(x,y);
				setDimensions(width,height);
			}
			// value
			void setMinValue(float fvalue) 
			{ 
				minValue=fvalue<1.0f?(fvalue>0.0f?fvalue:0.0f):1.0f;
				sliderect.setX(scaleToPos(minValue));
			}
			void setMaxValue(float fvalue) 
			{ 
				maxValue=fvalue<1.0f?(fvalue>0.0f?fvalue:0.0f):1.0f;
				sliderect.setRight(scaleToPos(maxValue));
			}
			const float getMinValue() const { return minValue; }
			const float getMaxValue() const { return maxValue; }
			// display
			void draw();
			void showCentre() {showcentre=true;}
			void hideCentre() {showcentre=false;}
			// events
			void keyPressed(int key) {}
			void keyReleased(int key) {}
			void mouseMoved(int x, int y);
			void mouseDragged(int x, int y, int button);
			void mousePressed(int x, int y, int button);
			void mouseReleased(int x, int y, int button)
			{
				if(dodrag)
					dodrag=false;
			}
			
	};

	////////////////////////////////////////
	// COLOUR CHIP
	////////////////////////////////////////
	class ColourChip : public Widget
	{
		protected:
			float 			hue;
			float 			saturation;
			float 			intensity;
			
		public:
			ColourChip() : hue(0.0f), saturation(0.0f), intensity(0.0f) {}
			~ColourChip() {}
			// display
			void draw()
			{
				if (visible)	{
					ofSetColor(fgColour.r, fgColour.g, fgColour.b, fgColour.a);
					ofRect(bounds.getX(), bounds.getY(), bounds.getWidth(), bounds.getHeight());
					ofSetColor(bgColour.r, bgColour.g, bgColour.b, bgColour.a);
					ofNoFill();
					ofRect(bounds.getX(), bounds.getY(), bounds.getWidth(), bounds.getHeight());
					ofFill();
				}
			}
			// colour
			void setRGB(int r, int g, int b, int a)
			{
				fgColour.r=r;
				fgColour.g=g;
				fgColour.b=b;
				fgColour.a=a;
				rgb2hsi(r, g, b, &hue, &saturation, &intensity);
			}
			void setRGB(int r, int g, int b) {setRGB(r,g,b,255);}
			void setAlpha(int a) {fgColour.a=a;}
			const int getAlpha() const {return fgColour.a;}
			Objsc::Colour& getRGB() {return fgColour;}
			const int getRed() const { return fgColour.r; }
			const int getGreen() const { return fgColour.g; }
			const int getBlue() const { return fgColour.b; }
			void setHSI(float h, float s, float i)
			{
				hue=h;
				saturation=s;
				intensity=i;
				fgColour = hsi2rgb(hue,saturation,intensity);
			}
			const float getHue() const { return hue;}
			const float getSaturation() const { return saturation;}
			const float getIntensity() const { return intensity;}
			
	};


	////////////////////////////////////////
	// LABEL
	////////////////////////////////////////
	class Label : public Widget
	{
		protected:
			string 			text;
			ofTrueTypeFont* 	font;
			float 			baseline;
			
		public:
			Label() {}
			virtual ~Label() {}
			// display
			virtual void draw();
			// text
			virtual void setFont(ofTrueTypeFont* fontPtr) 
			{
				font = fontPtr;
				if (font->bLoadedOk)	{
					baseline = font->stringHeight("Aljpy");
					if (bounds.getHeight() < baseline) 
						bounds.setHeight(baseline+Objsc::PADHEIGHT*2);
				} else {
					printf("ERROR: font not loaded\n");
				}
			}
			virtual void setFont(const string& fontfile, int size)
			{
				font = new ofTrueTypeFont();
				font->loadFont(fontfile, size);
				if (font->bLoadedOk)	{
					baseline = font->stringHeight("Aljpy");
					if (bounds.getHeight() < baseline) 
						bounds.setHeight(baseline+Objsc::PADHEIGHT*2);
				} else {
					printf("ERROR: font not loaded\n");
				}
			}
			virtual void setText(char* newtext);
			virtual void setText(const string& newtext);
			virtual void clearText();
			bool hasText() { return !text.empty(); }
			const string& getText() { return text; }
			const string& getText() const { return text; }
			ofTrueTypeFont* getFont() const {return font;}
	};


	////////////////////////////////////////
	// TIME DISPLAY
	////////////////////////////////////////
	class TimeDisplay : public Label
	{
		protected:
			int time;
			int milli;
			int secs;
			int mins;
			char cstr[64];
		
		public:
			TimeDisplay() {}
			virtual ~TimeDisplay() {}
			// geom
			void setLocation(float x, float y) 
			{ 
				bounds.setCentreX(x); 
				bounds.setY(y);
			}
			// display
			void draw();
			// time
			void setTime(int ntime)
			{
				time = ntime;
				milli = time%1000;
				secs = (time/1000)%60;
				mins = (time/60000)%60;
				sprintf(cstr, "%02d:%02d:%03d", mins,secs,milli);
				text.assign(cstr);
			}
		
	};

	////////////////////////////////////////
	// TEXT FIELD
	////////////////////////////////////////
	class TextField : public Label
	{
		protected:
			Objsc::CommandPtr action;
			bool 			editable;
			int 				cursor;
			int 				maxchars;
			char* 			input;
			Objsc::Rect 		cursorRect;
			bool			sizewithtext;
			bool			drawbg;
			bool			hasMinSize;
		
			void resetCursor();
			void _setFont();
		
		public:
			TextField() : editable(true), cursor(0), maxchars(0), sizewithtext(false), drawbg(true), hasMinSize(true)
			{
				input = new char[1];
				cursorRect.setWidth(2);
			}
			virtual ~TextField() {}
			void sizeWithText(bool value) { sizewithtext=value; }
			const bool willSizeWithText() const { return sizewithtext; }
			void drawBackground(bool value) { drawbg=value; }
			const bool willDrawBackground() const { return drawbg; }
			void setFont(ofTrueTypeFont* fontPtr) 
			{
				font = fontPtr;
				_setFont();
			}
			void setFont(const string& fontfile, int size)
			{
				font = new ofTrueTypeFont();
				font->loadFont(fontfile, size);
				_setFont();
			}
			// geom
			void setLocation(float x, float y) 
			{
				bounds.setLocation(x,y); 
				resetCursor();
			}
			void setDimensions(float width, float height) 
			{ 
				bounds.setDimensions(width,height); 
				resetCursor();
			}
			void setBounds(float x, float y, float width, float height) 
			{ 
				bounds.set(x,y,width,height); 
				resetCursor();
			}
			void setHasMinSize(bool value)
			{
				hasMinSize=value;
			}
			// display
			void draw();
			// text
			void setMaxCharacters(int numchars) {maxchars = numchars;}
			void setText(char* newtext);
			void setText(const string& newtext);
			void clearText();
			const string& getText() const { return text; }
			void editOn() { editable=true; }
			void editOff() { editable=false; }
			const bool isEditable() const { return editable; }
			// action
			void setAction(Objsc::CommandPtr actionPtr);
			void activate();
			// events
			virtual void keyPressed(int key);
			void mouseMoved(int x, int y);
			void mousePressed(int x, int y, int button);
	};
	
	
	////////////////////////////////////////
	// NUMBER FIELD
	////////////////////////////////////////
	class NumberField : public TextField
	{
		public:
			NumberField(){}
			virtual ~NumberField() {}
			void keyPressed(int key);
				
			const int getNumber() const { return atoi(text.c_str()); }
	};
	
	////////////////////////////////////////
	// MULTILINE TEXT
	////////////////////////////////////////
	class MultilineText
	{
		private:
			// text
			string rawtext;
			vector<string> textlines;
			int linesinstatement;
			int currentline;
			int currentchar;
			int currentlinelength;
			float lineX;
			float lineY;
			float lineoffsetY;
			float linepaddingX;
			float lineHeight;
			float lineWidth;
		
			// display
			ofTrueTypeFont displayFont;
			string *displaylines;
			int topline;
			int totaldisplaylines;
			int maxdisplaylines;
			int maxcharsperline;
		
			Colour bgcolour;
			Colour forecolour;
		
			float scrWidth;
			float scrHeight;
			
			float cursorWidth;
			float cursorHeight;
			
			int blinkperiod;
			int blinkcount;
			bool textprinting;
			
			int displaystate;
			
			// time
			int nowtime;
			int timeout;
			int textwait;
			int pausewait;
			int duration;
			bool doingtime;
			bool expired;
			
		public:
			MultilineText() : rawtext() {}
			~MultilineText() {}
			MultilineText(const MultilineText &copy);
			MultilineText& operator=(const MultilineText &copy);
				
			void setFont(ofTrueTypeFont* fontPtr) {} // SY: to do
			void setFont(const string& fontfile, int size) {} // SY: to do
		
			void setup(float width, float height, int maxlines);
			void setText(char* newtext) { setText(string(newtext)); }
			void setText(const string& newtext);
			void clearText();
			const string& getText() { return rawtext; }
			const string& getText() const { return rawtext; }
			
			void draw();
			void setBackgroundColour(Colour colour);
			void setForeColour(Colour colour);
				
			void nextLine();
			void nextChar();
		
			void setTimeout(int nduration);
			void setTextwait(int nduration);
			void setPausewait(int nduration);
			void cancelTimeout();
			void updateTimeout();
			void startTimeout();
			bool hasExpired() {return expired;}
	};
	
	class TextArea : public Widget
	{
		protected:
			MultilineText	mltext;
			
		public:
			TextArea() : mltext() {}
			~TextArea() {}
			// display
			void draw() {mltext.draw();}
			// text
			void setFont(ofTrueTypeFont* fontPtr){mltext.setFont(fontPtr);}
			void setFont(const string& fontfile, int size){mltext.setFont(fontfile, size);}
			void setText(char* newtext){mltext.setText(newtext);}
			void setText(const string& newtext){mltext.setText(newtext);}
			void clearText(){mltext.clearText();}
			const string& getText() { return mltext.getText(); }
			const string& getText() const { return mltext.getText(); }
	};

	////////////////////////////////////////
	// BUTTON
	////////////////////////////////////////
	class Button : public Label
	{
		protected:
			Objsc::CommandPtr action;
			float			textX;
		
		public:
			Button() : textX(0) {}
			virtual ~Button() {}
			// display
			virtual void draw();
			// text
			void setText(char* newtext);
			void setText(const string& newtext);
			void clearText();
			// action
			void setAction(Objsc::CommandPtr actionPtr);
			void activate();
			// events
			void keyPressed(int key);
			void keyReleased(int key);
			void mouseMoved(int x, int y);
			void mouseDragged(int x, int y, int button);
			void mousePressed(int x, int y, int button);
			void mouseReleased(int x, int y, int button);
			void windowResized(int w, int h);
	};

	////////////////////////////////////////
	// HOT SPOT
	////////////////////////////////////////
	class HotSpot : public Button
	{
		protected:
			Objsc::Rect 			hotarea;
		
		public:
			HotSpot() {}
			virtual ~HotSpot() {}
			// display
			virtual void draw();
			// events
			void keyPressed(int key);
			void keyReleased(int key);
			void mouseMoved(int x, int y);
			void mouseDragged(int x, int y, int button);
			void mousePressed(int x, int y, int button);
			void mouseReleased(int x, int y, int button);
			void windowResized(int w, int h);
	};

	////////////////////////////////////////
	// MENU ITEM
	////////////////////////////////////////
	class MenuItem : public Button
	{
		protected:
			Objsc::GuiBaseVector 	children;
			Objsc::Rect 			childbounds;
		
		public:
			MenuItem() {}
			virtual ~MenuItem() {}
			// geom
			void setLocation(float x, float y);
			bool withinBounds(float x, float y);
			// display
			virtual void draw();
			// children
			Objsc::MenuItem* addItem(string name);
			Objsc::MenuItem* addItem(string name, Objsc::CommandPtr actionPtr)
			{
				Objsc::MenuItem* menuItem=addItem(name);
				menuItem->setAction(actionPtr);
				return menuItem;
			}
			// events
			void keyPressed(int key);
			void keyReleased(int key);
			void mouseMoved(int x, int y);
			void mouseDragged(int x, int y, int button);
			void mousePressed(int x, int y, int button);
			void mouseReleased(int x, int y, int button);
			void windowResized(int w, int h);
	};

	////////////////////////////////////////
	// GRAPHIC MENU ITEM
	////////////////////////////////////////
	class GraphicMenuItem : public MenuItem
	{
		protected:
			Objsc::Image 	icon;
		
		public:
			GraphicMenuItem() {}
			virtual ~GraphicMenuItem() {}
			// geom
			void setLocation(float x, float y);
			void setDimensions(float width, float height) 
			{ 
				bounds.setDimensions(width,height);
				icon.setDimensions(width,height);
			}
			// icon
			void setIcon(string imgname);
			// display
			virtual void draw();
			// children
			Objsc::GraphicMenuItem* addItem(string name, string imgname);
			Objsc::GraphicMenuItem* addItem(string name, string imgname, Objsc::CommandPtr actionPtr)
			{
				Objsc::GraphicMenuItem* menuItem=addItem(name, imgname);
				menuItem->setAction(action);
				return menuItem;
			}
	};
			

	////////////////////////////////////////
	// CONTEXT MENU
	////////////////////////////////////////
	class ContextMenu;

	class ContextMenuSource
	{
		public:
			virtual ~ContextMenuSource() {}
				
			virtual void buildContextMenu(Objsc::ContextMenu* menu) =0;
			virtual void buildContextMenu(Objsc::ContextMenu* menu, string info) =0;
	};

	class ContextMenu : public GuiContainer
	{
		public:
			ContextMenu() {}
			virtual ~ContextMenu() {}
			// geom
			void setLocation(float x, float y);
			bool withinBounds(float x, float y);
			// items
			Objsc::MenuItem* addItem(const string& name);
			Objsc::MenuItem* addItem(const string& name, Objsc::CommandPtr actionPtr)
			{
				Objsc::MenuItem* menuItem=addItem(name);
				menuItem->setAction(actionPtr);
				return menuItem;
			}
				
			static Objsc::GuiBasePtr createWidget()
			{
				return Objsc::GuiBasePtr(new ContextMenu());
			}
			
			static std::tr1::shared_ptr<Objsc::ContextMenu> create()
			{
				return std::tr1::shared_ptr<Objsc::ContextMenu>(new ContextMenu());
			}
			
			void reset();
	};
	
	typedef std::tr1::shared_ptr<Objsc::ContextMenu> ContextMenuPtr;
	typedef map < string, Objsc::ContextMenuPtr > ContextMenuMap;
	typedef ContextMenuMap::value_type ContextMenuMapEntry;


	////////////////////////////////////////
	// GRAPHIC MENU
	////////////////////////////////////////
	class GraphicMenu : public ContextMenu
	{
		public:
			GraphicMenu() {}
			virtual ~GraphicMenu() {}
			// geom
			void setLocation(float x, float y);
			// items
			Objsc::GraphicMenuItem* addItem(string name, string imgname);
			Objsc::GraphicMenuItem* addItem(string name, string imgname, Objsc::CommandPtr actionPtr)
			{
				Objsc::GraphicMenuItem* menuItem=addItem(name, imgname);
				menuItem->setAction(actionPtr);
				return menuItem;
			}
	};


	////////////////////////////////////////
	// LIST MENU
	////////////////////////////////////////
	class ListMenu;

	class ListMenuSource
	{
		public:
			virtual ~ListMenuSource() {}
				
			virtual void buildListMenu(Objsc::ListMenu* menu) =0;
			virtual void buildListMenu(Objsc::ListMenu* menu, string info) =0;
	};

	class ListMenu : public GuiContainer
	{
		protected:
			int 				displayitems;
			int 				scollitems;
			unsigned int 		topitem;
			Objsc::Rect		childbounds;
			Objsc::Rect		scrollrect;
			Objsc::Rect		sliderect;
			Objsc::Colour		bgColour; 
			Objsc::Colour		fgColour; 
			Objsc::Colour		hiColour;
			bool			doscroll;
			Objsc::CommandPtr action;
			int				selected;
		
			void scrollUpdated();
			
		public:
			ListMenu() : displayitems(0), topitem(0), doscroll(false), action(), selected(-1)
			{
				scrollrect.setWidth(Objsc::SCROLLHANDLEWIDTH);
				sliderect.setDimensions(Objsc::SCROLLHANDLEWIDTH,Objsc::SCROLLHANDLEHEIGHT);
			}
			virtual ~ListMenu() {}
			void draw();
			// geom
			void setLocation(float x, float y) 
			{ 
				bounds.setLocation(x,y);
				childbounds.setLocation(x,y);
				scrollrect.setLocation(childbounds.getRight(),childbounds.getY());
				sliderect.setLocation(scrollrect.getX(),scrollrect.getY());
			}
			void setDimensions(float width, float height) 
			{ 
				bounds.setDimensions(width,height); 
				childbounds.setDimensions(bounds.getWidth()-Objsc::SCROLLHANDLEWIDTH,bounds.getHeight());
				scrollrect.setHeight(MAX(childbounds.getHeight(),bounds.getHeight()));
				scrollrect.setLocation(childbounds.getRight(),childbounds.getY());
				sliderect.setLocation(scrollrect.getX(),scrollrect.getY());
			}
			// action
			void setAction(Objsc::CommandPtr actionPtr);
			void activate();
			// items
			bool hasSelection() const { return selected > -1; }
			const int getSelectedIndex() const { return selected; }
			const string& getSelectedName() const;
			Objsc::MenuItem* addItem(string name);
			Objsc::MenuItem* addItem(string name, Objsc::CommandPtr actionPtr)
			{
				Objsc::MenuItem* menuItem=addItem(name);
				menuItem->setAction(actionPtr);
				return menuItem;
			}
			void setToTop();
			void scrollUp();
			void scrollDown();
			void refreshDisplay();
			void shrinkWrap(float minHeight);
			void shrinkWrap() {shrinkWrap(0.0f);}
			// styles
			void setStyleName(string sname);
			void setBackColour(Objsc::Colour &col) {bgColour = col;}
			void setForeColour(Objsc::Colour &col) {fgColour = col;}
			void setHilightColour(Objsc::Colour &col) {hiColour = col;}
			// events
			void keyPressed(int key);
			void keyReleased(int key);
			void mouseMoved(int x, int y);
			void mouseDragged(int x, int y, int button);
			void mousePressed(int x, int y, int button);
			void mouseReleased(int x, int y, int button);
			void windowResized(int w, int h);
	};

	////////////////////////////////////////
	// DIALOGUE
	////////////////////////////////////////
	class DialogueSource
	{
		public:
			virtual ~DialogueSource() {}
				
	};


	////////////////////////////////////////
	// LAYER
	////////////////////////////////////////
	class GuiLayer : public GuiBase
	{
		protected:
			Objsc::GuiBasePtr root;
		
		public:
			GuiLayer() : root() {}
			virtual ~GuiLayer() {}
				
			void setRootWidget(Objsc::GuiBasePtr containerPtr);
			Objsc::ContextMenu* createContextMenu(float x, float y, Objsc::ContextMenuSource* source);
			Objsc::ContextMenu* createContextMenu(float x, float y, Objsc::ContextMenuSource* source, string info);
			void close();
			bool withinBounds(float x, float y);
			// runtime
			void enable() 
			{ 
				active = true; 
				if(root)
					root->enable();
			}
			void disable() 
			{ 
				active = false; 
				if(root)
					root->disable();
			}
			void update();
			void draw();
			// events
			void keyPressed(int key);
			void keyReleased(int key);
			void mouseMoved(int x, int y);
			void mouseDragged(int x, int y, int button);
			void mousePressed(int x, int y, int button);
			void mouseReleased(int x, int y, int button);
			void windowResized(int w, int h);
			
			static std::tr1::shared_ptr<Objsc::GuiLayer> create()
			{
				return std::tr1::shared_ptr<Objsc::GuiLayer>(new GuiLayer());
			}
	};
	
	
	typedef std::tr1::shared_ptr<Objsc::GuiLayer> GuiLayerPtr;
	typedef vector <GuiLayerPtr> GuiLayerPtrVector;

}


#endif

