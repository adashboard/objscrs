#ifndef  OBJSCR_FRAMES_H
#define OBJSCR_FRAMES_H


// shared_ptr
#include <tr1/memory>

namespace Objsc
{
	////////////////////////////////////////
	// FRAME SOURCE
	////////////////////////////////////////
	class FrameSource
	{
		protected:
			int width;
			int height;
			int channels;
			int rowstride;
			int bpp;
		
		public:
			FrameSource():
				width(),
				height(),
				channels(3),
				rowstride(),
				bpp(8){}
			virtual ~FrameSource() {}
				
			virtual void setFrameSize(int nwidth, int nheight) {}
			virtual int getWidth() const { return width; }
			virtual int getHeight() const { return height; }
			virtual int getChannels() const { return channels; }
			virtual int getRowstride() const { return rowstride; }
			virtual int getBPP() const { return bpp; }
			virtual unsigned char* getFrame() =0;
			virtual bool hasNewFrame() {return true;}
			virtual void update() {}
			virtual void start() {}
			virtual void stop() {}
	};
	
	
	typedef std::tr1::shared_ptr<Objsc::FrameSource> FrameSourcePtr;
}

#endif // OBJSCR_FRAMES_H
