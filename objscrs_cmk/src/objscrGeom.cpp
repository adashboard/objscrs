#include "objscrGeom.hpp"
#include "objscrSettings.hpp"


////////////////////////////////
// FUNCTIONS
////////////////////////////////

namespace Objsc
{

	float distance(float x1, float y1, float x2, float y2) {return sqrt(pow(x2-x1,2)+pow(y2-y1,2));}
	float distance(const ofPoint& p1, const ofPoint& p2)
	{
		return sqrt(pow(p2.x - p1.x , 2) + pow( p2.y - p1.y , 2));
	}

	// deprecated ?
	float screenToStage(float scrPos, float scrDim) { return scrPos/scrDim; }
	float stageToScreen(float stagePos, float scrDim) { return scrDim*stagePos; }

	Rect Rect::scaled(float sx, float sy) const
	{
		Rect ret(x * sx, y * sy, width * sx, height * sy);
		return ret;

	}
}

