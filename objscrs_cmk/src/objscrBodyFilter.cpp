#include "objscrBodyFilter.hpp"

//objscrs
#include "objscrIDBroker.hpp"

// STL
#include <iostream>
#include <algorithm>
#include <map>


void printShape(ofxCvBlob& shape)
{
	printf("shape: %f, %f, %f, %f\n", shape.boundingRect.x, shape.boundingRect.y, shape.boundingRect.width, shape.boundingRect.height);
}

namespace Objsc
{
	
	
	////////////////////////////////////////
	// FUNCTIONS
	////////////////////////////////////////
	struct bodyIsDead : std::unary_function< Objsc::BodyPtr, bool >	{
		bool operator() (const Objsc::BodyPtr& ptr) const	{
			//printf("bodyIsDead [%u] %d - %d\n", ptr->getID(), ptr->isDead(), ptr->getState());
			return ptr->isDead();
		}
	};
	
	////////////////////////////////////////
	// BODY FILTER
	////////////////////////////////////////
	void BodyFilter::processOLD()
	{
		//printf("==\n");
		ofxCvBlobIterator shapeItem;
		ofxCvBlobIterator shapeEnd = shapes->shapesEnd();
		int shapenum = 0;
		int matches = 0;
		matchedShapes.clear();
		for(Objsc::BodyPtrList::iterator body = bodies.begin(); body != bodies.end(); ++body)	{
			(*body)->newFrame();
			shapenum = 0;
			for(shapeItem = shapes->shapesBegin();shapeItem!=shapeEnd;++shapeItem)	{
				if((*shapeItem).area > 0.0f)	{
					//printf("shapeItem area: %f\n", (*shapeItem).area);
					(*body)->compareShape((*shapeItem),shapenum++);
				}
			}
			//printf("best: %d\n", (*body)->getBestShape());
			if((*body)->getBestShape() != -1)	{
				++matches;
				(*body)->updateShape(shapes->getShape((*body)->getBestShape()));
				matchedShapes.push_back((*body)->getBestShape());
			}
		}
		//printf("matches: %d shapes: %d\n", matches, shapes->totalShapes()); 
		if(matches < shapes->totalShapes())	{
			for (int i = 0; i < shapes->totalShapes(); i++){
				if(find(matchedShapes.begin(), matchedShapes.end(), i) == matchedShapes.end())	{
					//printf("no match: %d\n", i); 
					createBody(shapes->getShape(i));
				}
			}
		}
	}
	
	void BodyFilter::process()
	{
		if(shapes->totalShapes() == 0)
			return;
		//printf("==\n");
		ofxCvBlobIterator shapeItem;
		ofxCvBlobIterator shapeEnd = shapes->shapesEnd();
		int shapenum = 0;
		int matches = 0;
		matchedShapes.clear();
		std::map<BodyPtr, Body::DistanceMap> distances;
		std::map<BodyPtr, Body::DistanceMap>::iterator bi;
		std::map<BodyPtr, Body::DistanceMap>::iterator bo;

		for(Objsc::BodyPtrList::iterator body = bodies.begin(); body != bodies.end(); ++body)
		{
			distances.insert(std::pair<BodyPtr, Body::DistanceMap>(*body, (*body)->getDistances(shapes)));
		}

		for(bi = distances.begin(); bi != distances.end(); bi++)
		{
			Body::DistanceMap refDist(bi->second);
            float di0(0);
			int num0(0);
			// now we check all other bodies to see if one is closer than the current body
			// and go like that through all acceptable shapes for this body until if we found one.
            // Note: Everything here relies on the fact that std::map keys are sorted in ascendant order.
			for(Body::DistanceMap::iterator mi(refDist.begin()); mi != refDist.end();  mi++)
			{
                num0 = mi->first;
				if(std::find(matchedShapes.begin(), matchedShapes.end(), num0) != matchedShapes.end())
					continue;

                di0 = mi->second;

                bool closer(true);
				for(bo = distances.begin(); bo != distances.end(); bo++)
				{
                    if((bo->first->getState() == Body::PRESENT || bo->first->getState() == Body::ABSENT)
							&& (bo->first->getUID() != bi->first->getUID()))
					{
						Body::DistanceMap compDist(bo->second);
                        float di1(compDist.begin()->second);
                        int num1(compDist.begin()->first);
						if((num1 == num0) // same shape
							&& (di1 < di0)) // this other body is closer than the current body
                        {
                            closer = false;
							break;
                        }
					}
				}
				if(closer)
				{
					++matches;
					bi->first->updateShape(shapes->getShape(num0));
                    matchedShapes.push_back(num0);
					break;
                }
            }
        }


		if(matches < shapes->totalShapes())
		{
			for (int i = 0; i < shapes->totalShapes(); i++)
			{
				if(find(matchedShapes.begin(), matchedShapes.end(), i) == matchedShapes.end())
				{
					//printf("no match: %d\n", i); 
					createBody(shapes->getShape(i));
				}
			}
		}
	}
	
	void BodyFilter::update()
	{
		//printf("===============================\n");
		process();
		for(Objsc::BodyPtrList::iterator body = bodies.begin(); body != bodies.end(); ++body)	{
			(*body)->update();
		}
        prune();
		cull();
	}
	
	void BodyFilter::prune()
	{
        for(Objsc::BodyPtrList::iterator body = bodies.begin(); body != bodies.end(); ++body)
        {
            for(Objsc::BodyPtrList::iterator other = bodies.begin(); other != bodies.end(); ++other)
            {
                if((*other)->getState() == (*body)->getState())
                {
                    if((*body)->matchesArea((*other)))
                    {
                        if((*body)->getAge() >= (*other)->getAge()
                                && (*body)->getUID() < (*other)->getUID())
                        {
                            (*other)->kill();
                        }
                    }
                }
			}
		}
	}
	
	void BodyFilter::cull()
	{
		if(!bodies.empty())	{
			bodies.erase(remove_if(bodies.begin(), bodies.end(), bodyIsDead()), bodies.end());
			if(bodies.empty())	{
				Objsc::IDBroker::Current()->reset();
			}
		}
	}
	
	void BodyFilter::clear()
	{
		bodies.clear();
		Objsc::IDBroker::Current()->reset();
	}
	
	void BodyFilter::velocitySmoothingReset()
	{
		for(Objsc::BodyPtrList::iterator body = bodies.begin(); body != bodies.end(); ++body)	{
			(*body)->resetVelocity();
		}
	}
	
	
	Objsc::BodyPtr BodyFilter::getBody(unsigned int uid)
	{
		for(Objsc::BodyPtrList::iterator body = bodies.begin(); body != bodies.end(); ++body)	{
			if((*body)->getStageNum() == uid)
				return (*body);
		}
		return Objsc::BodyPtr();
	}
	
	void BodyFilter::createBody(const ofxCvBlob& shape)
	{
		//std::cout << "BodyFilter::createBody A" << std::endl;
		if (shape.area > 0.0f)	{
			//printf("shapeItem area: %f\n", shape.area);
			Objsc::BodyPtr body = Objsc::Body::create();
			body->updateShape(shape);
			body->setID(Objsc::IDBroker::Current()->newID());
			bodies.push_back(body);
#ifdef OBJSCRS_DEBUG
//			std::cout << "BodyFilter::createBody" << std::endl;
//			body->print();
#endif
		}
	}
}

