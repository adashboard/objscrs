#ifndef  OBJSCR_ZONES_H
#define OBJSCR_ZONES_H

// objscrs
#include "objscrGeom.hpp"
#include "singleton.h"
#include "objscrLang.hpp"
#include "objscrBody.hpp"
#include "Lang/LangPatch.hpp"

// STL
#include <string>
#include <map>
#include <list>
#include <vector>
using std::string;
using std::map;
using std::list;
using std::vector;

// shared_ptr
#include <tr1/memory>


namespace Objsc
{
	
	////////////////////////////////////////
	// TYPES
	////////////////////////////////////////
	class BasicZone;
	typedef std::tr1::shared_ptr<Objsc::BasicZone> ZonePtr;
	typedef list < Objsc::ZonePtr > ZonePtrList;
	typedef ZonePtrList::iterator ZonePtrListIter;
	typedef map < string, Objsc::ZonePtr > ZonePtrMap;
	typedef ZonePtrMap::value_type ZonePtrEntry;
	typedef vector< int > GridCells;
	
	
	/////////////////////////////////////
	// FUNCTIONS
	/////////////////////////////////////
	void initZones();
	Objsc::LangPatchPtr createZonePatch(const string& name);
	
	Objsc::LangAtomPtr zoneBoundsToAtom(const string &zoneName, const Objsc::Rect& bounds);
	
	
	////////////////////////////////////////
	// BODY HOLDER
	////////////////////////////////////////
	class BodyHolder
	{
		private:
			Objsc::BodyPtr		body;
			mutable bool		in;
			mutable int		count;
		
		public:
			BodyHolder() : body(), in(true), count(0) {}
			BodyHolder(Objsc::BodyPtr ptr) : body(ptr), in(true), count(0) {}
			~BodyHolder() {}
				
			const Objsc::BodyPtr& getBody() const { return body; }
			const bool isIn() const { return in;}
			void update() const
			{ 
				++count;
				in = true;
			}
			void reset() { in = false; }
			void putIn() { in = true; }
			const float getX() const { body->getX(); }
			const float getY() const { body->getY(); }
			const float getStageX() const { body->getStageX(); }
			const float getStageY() const { body->getStageY(); }
			const float getScreenX() const { body->getScreenX(); }
			const float getScreenY() const { body->getScreenY(); }
	};
	
	typedef vector < Objsc::BodyHolder > BodyHolderVec;
	
	////////////////////////////////////////
	// BASIC ZONE
	////////////////////////////////////////
	class BasicZone
	{
		protected:
			string				name;
			Objsc::Rect			bounds;
			Objsc::LangPatchPtr	patch;
			Objsc::BodyHolderVec	bodies;
			Objsc::LangAtomPtr 	bodyAtom;
			bool				refresh;
		
		public:
			BasicZone();
			virtual ~BasicZone() {}
			
			static const string zonetype;
			virtual const string& getZoneType() const { return Objsc::BasicZone::zonetype; }
			
			static const int zonetypeID;
			virtual const int getZoneTypeID() const { return Objsc::BasicZone::zonetypeID; }
			
			virtual void setLocation(float x, float y) { bounds.setCentre(x,y); }
			virtual void setDimensions(float width, float height) { bounds.setDimensions(width,height); }
			virtual void setBounds(float x, float y, float width, float height) { bounds.set(x,y,width,height); }
			const Objsc::Rect& getBounds() const { return bounds;}
			virtual const bool withinBounds(float x, float y) { return bounds.containsPoint(x,y); }
			
			// patch
			Objsc::LangPatchPtr getPatch();
			void setPatch(Objsc::LangPatchPtr patchPtr) { patch=patchPtr; }
			const bool hasPatch() const { return patch; }
			void receive(const string& label, Objsc::LangAtomPtr atom);
			
			virtual void update(int timeElapsed);
			const bool needsRefresh() const { return refresh; }
			void setNeedsRefresh() { refresh = true; }
			void doneRefresh() { refresh = false; }
			
			void onload();
			
			void setName(const string& nname);
			const string& getName() const { return name; }
			
			// bodies
			void updateBodies(const Objsc::BodyPtrList&  bodylist);
			bool hasBody(const Objsc::BodyPtr& body);
			virtual void bodyEnter(Objsc::BodyPtr body);
			virtual void bodyExit(Objsc::BodyPtr body);
			virtual void bodyWithin(Objsc::BodyPtr body);
			const int getBodyCount() const { return bodies.size(); }
			const Objsc::BodyHolderVec& getBodies() const { return bodies; }
			Objsc::BodyHolderVec::iterator getBodyIterator() { return bodies.begin(); }
			Objsc::BodyHolderVec::iterator getBodyIteratorEnd() { return bodies.end(); }
			void labelBody(int index, const string &label);
	};
	
	////////////////////////////////////////
	// CIRCLE ZONE
	////////////////////////////////////////
	class CircleZone : public BasicZone
	{
		protected:
			Objsc::Circle		circle;
		
			const float proximity(Objsc::BodyPtr body) const { return circle.percFromCentre(body->getStageX() , body->getStageY());}
			//const float proximity(Objsc::BodyPtr body) const { return circle.distanceFromCentre(body->getStageX() , body->getStageY());} // TMP DEBUG
		
		public:
			CircleZone();
			~CircleZone() {}
				
			static const string zonetype;
			const string& getZoneType() const { return Objsc::CircleZone::zonetype; }
			
			static const int zonetypeID;
			virtual const int getZoneTypeID() const { return Objsc::CircleZone::zonetypeID; }
			
			void setLocation(float x, float y);
			void setDimensions(float width, float height);
			void setBounds(float x, float y, float width, float height);
			const bool withinBounds(float x, float y) { return circle.containsPoint(x,y); }
			
			void bodyEnter(Objsc::BodyPtr body);
			void bodyExit(Objsc::BodyPtr body);
			void bodyWithin(Objsc::BodyPtr body);
	};
	
	////////////////////////////////////////
	// GRID ZONE
	////////////////////////////////////////
	class GridZone : public BasicZone
	{
		protected:
			int				cols;
			int				rows;
			float			colWidth;
			float			rowHeight;
			GridCells			cells;
		
			void sizeGrid();
			void updateGrid();
			
		public:
			GridZone();
			~GridZone() {}
				
			static const string zonetype;
			const string& getZoneType() const { return Objsc::GridZone::zonetype; }
			
			static const int zonetypeID;
			virtual const int getZoneTypeID() const { return Objsc::GridZone::zonetypeID; }
			
			void setLocation(float x, float y);
			void setDimensions(float width, float height);
			void setBounds(float x, float y, float width, float height);
			
			void setCols(int value);
			void setRows(int value);
			void setGrid(int newCols, int newRows);
			const int getCols() const { return cols; }
			const int getRows() const { return rows; }
			const GridCells& getCells() const { return cells; }
			
			void update(int timeElapsed);
			
			void bodyEnter(Objsc::BodyPtr body);
			void bodyExit(Objsc::BodyPtr body);
			void bodyWithin(Objsc::BodyPtr body);
	};
	
	
	////////////////////////////////////////
	// ZONE CREATORS
	////////////////////////////////////////
	class ZoneCreator
	{
		public:
			virtual ~ZoneCreator() {}
			virtual Objsc::ZonePtr create() const =0;
	};
	
	class BasicZoneCreator : public ZoneCreator
	{
		public:
			Objsc::ZonePtr create() const { return Objsc::ZonePtr(new Objsc::BasicZone());}
	};
	
	class CircleZoneCreator : public ZoneCreator
	{
		public:
			Objsc::ZonePtr create() const { return Objsc::ZonePtr(new Objsc::CircleZone());}
	};
	
	class GridZoneCreator : public ZoneCreator
	{
		public:
			Objsc::ZonePtr create() const { return Objsc::ZonePtr(new Objsc::GridZone());}
	};
	
	////////////////////////////////////////
	// ZONE FACTORY
	////////////////////////////////////////
	typedef map < string, ZoneCreator* > ZoneCreatorMap;
	typedef ZoneCreatorMap::value_type ZoneCreatorEntry;

	class ZoneFactory : public Singleton<ZoneFactory>
	{
		private:
			ZoneCreatorMap creators;
		
		public:
			ZoneFactory() : creators() {}
			~ZoneFactory() { creators.clear(); }
			
			Objsc::ZonePtr create(string zonetype)
			{
				ZoneCreatorMap::iterator iter = creators.find(zonetype);
				if(iter == creators.end()) {
					throw std::exception();
				}
				return (*iter).second->create();
			}
			
			void addCreator(string zonetype, ZoneCreator* creatorPtr)
			{
				creators.insert(ZoneCreatorEntry(zonetype, creatorPtr));
			}
			
			bool hasCreator(string zonetype)	
			{
				return creators.count(zonetype) > 0;
			}
			
	};
	
}

#endif // OBJSCR_ZONES_H
