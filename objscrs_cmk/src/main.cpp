#include "ofMain.h"
#include "testApp.h"
#include "ofAppGlutWindow.h"

// objscrs
#include "objscrFiles.hpp"
#include "objscrSettings.hpp"


int main(int argc, char ** argv)
{
	// version info
	printf("\nobjscrs version: 0.3.6\n");
	printf("[belo horizonte]\n");
	printf("18-07-2011\n\n");
	
	// read settings file before launching
	Objsc::ProjectFiles::Current()->initPaths();
	Objsc::ProjectFiles::Current()->setCurrentProject("root");
	Objsc::Settings::Current()->load();
	Objsc::Settings::Current()->print();

	if(Objsc::Settings::Current()->hasSetting("ofdebug"))
		ofSetLogLevel(atoi(Objsc::Settings::Current()->getSetting("ofdebug").c_str()));
	
	// run app
    ofAppGlutWindow window;
    // FIXME this fullscreen mode is a quick hack. It has to go in GUIRoot first for interactive fs selection.
    // About command line arguments, we want a decent options handler able to alter main configuration.
    // -pm
    bool fullscreen(false);
    if(argc > 1)
    {
        for(unsigned int i(1); i < argc; ++i)
        {
            const std::string arg(argv[i]);
            if(arg == std::string("fullscreen"))
                fullscreen = true;
        }
    }
    if(fullscreen)
        ofSetupOpenGL(&window, Objsc::Settings::Current()->getDisplayWidth(),Objsc::Settings::Current()->getDisplayHeight(), OF_FULLSCREEN);
    else
        ofSetupOpenGL(&window, Objsc::Settings::Current()->getDisplayWidth(),Objsc::Settings::Current()->getDisplayHeight(), OF_WINDOW);
	ofRunApp( new testApp());
}
