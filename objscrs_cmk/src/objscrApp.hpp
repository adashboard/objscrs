#ifndef  OBJSCR_APP_H
#define OBJSCR_APP_H

// objscrs
#include "singleton.h"
#include "objscrScore.hpp"
#include "objscrStage.hpp"
#include "objscrTimer.hpp"
#include "objscrOSC.hpp"
#include "objscrCommands.hpp"


namespace Objsc
{
	class App : public Singleton<App>, public CommandReceiver
	{
		private:
			Objsc::ScorePtr			score;
			Objsc::StagePtr			stage;
			Objsc::Timer*				timer;
			Objsc::OSCComms*			oscComms;
			int						modules;
			char* 					input;
			
		public:
			App() : 
				score(),
				stage(), 
				timer(NULL),
				oscComms(NULL),
				modules(0),
				input()
			{}
			~App() {}
				
			void init();
			void update(int time, int timeframe);
			void draw();
			void quit();
				
			// score
			Objsc::ScorePtr newScore(const string& scorename="");
			void setScore(Objsc::ScorePtr newscore);
			Objsc::ScorePtr getScore() const { return score; }
			void loadScore(const string& scorename);
			// stage
			Objsc::StagePtr getStage() const { return stage; }
			
			// commands
			void doCommand(Objsc::CommandPtr& cmd);
			void undoCommand(Objsc::CommandPtr& cmd);
			// patch lang
			void setupLang();
			// stage
			void setupStage(int x, int y, int width, int height);
			
			// keys
			void handleControlKey(int key);
	};
}

#endif // OBJSCR_APP_H
