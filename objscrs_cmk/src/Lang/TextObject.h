/***************************************************************************
 *   Copyright (C) 2012 by Pierre Marchand   *
 *   pierre@oep-h.com   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#ifndef OBJSC_TEXTOBJECT_H
#define OBJSC_TEXTOBJECT_H

#include "ImageObject.h"

namespace Objsc {
	
	class TextObject : public ImageObject
	{
		protected:
			void receiveList(std::string inlet, LangList* value);
			void setText(LangList* value);
			void setText(const string& value);
			void clearText();
			void locMedia(float x, float y);
			void locXMedia(float x);
			void locYMedia(float y);
			void setLayerMedia(int layer);

			void setTextColour(int r, int g, int b);
			void setBackColour(int r, int g, int b);

		public:
			TextObject()
			{
				setInlets(4);
				setOutlets(2);
			}
			~TextObject();

			void update(int timeElapsed);

			static const string langname;
			virtual const string& getLangName() const { return Objsc::TextObject::langname; }

			const string& getDisplayString()
			{
				if(displaystr.empty())	{
					return Objsc::TextObject::langname;
				}
				return displaystr;
			}

			void setLabel(string nlabel);

			void receive(std::string inlet, Objsc::LangAtom* atom);
			void activate();
	};

	class TextObjectCreator : public LangObjectCreator
	{
		public:
			Objsc::LangObjectPtr create() const { return Objsc::LangObjectPtr(new Objsc::TextObject());}
	};


	
} // namespace Objsc

#endif // OBJSC_TEXTOBJECT_H
