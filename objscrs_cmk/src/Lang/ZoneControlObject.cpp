
#include "ZoneControlObject.hpp"
#include "../objscrApp.hpp"

namespace Objsc
{
	const string Objsc::ZoneControlObject::langname = "zone";

	/////////////////////////////////////
	// ZONE CONTROL OBJECT
	/////////////////////////////////////
	bool ZoneControlObject::parseArgs(Objsc::LangList* args)
	{
		if(args->size() < min_args)	{
			return false;
		} else {
			string argStr = getItemAsString(args, 0);
			if(argStr.empty())	{
				setLabel("");
				zone = Objsc::ZonePtr();
			} else {
				zone = Objsc::App::Current()->getScore()->getZone(argStr);
				if (zone){
					setLabel(argStr);
					return true;
				} else {
					setLabel("");
					return false;
				}
			}
		}
	}

	void ZoneControlObject::setLabel(string nlabel)
	{
		label.assign(nlabel);
		displaystr = Objsc::ZoneControlObject::langname + " " + label;
	}

	const string& ZoneControlObject::getDisplayString()
	{
		if(displaystr.empty())	{
			return Objsc::ZoneControlObject::langname;
		}
		return displaystr;
	}

	void ZoneControlObject::receive(std::string inlet, Objsc::LangAtom* atom)
	{
		if(checkInlet(inlet))
		{
			switch(atom->getType())	{
				case LANG_BANG:
				if(inlet == "0")	{
						activate();
					}
					break;
				case LANG_LIST:
					receiveList(inlet, dynamic_cast<LangList*>(atom));
					break;
			}
		}
	}

	void ZoneControlObject::receiveList(std::string inlet, LangList* value)
	{
		if(!zone)	{
			return;
		}
		string cmd = parseString(value->idx(0), "");
		if(cmd == "type")	{
			send(0, new Objsc::LangInt(zone->getZoneTypeID()));
		} else if(cmd == "loc")	{
			if(value->size() > 2)	{
				setLoc(parseFloat(value->idx(1), 0), parseFloat(value->idx(2), 0));
			}
			sendLoc();
		} else if(cmd == "geom")	{
			if(value->size() > 4)	{
				setGeom(parseFloat(value->idx(1), 0), parseFloat(value->idx(2), 0),parseFloat(value->idx(3), -1), parseFloat(value->idx(4), -1));
			}
			sendGeom();
		} else if(cmd == "grid")	{
			if(zone->getZoneTypeID() == ZONE_GRID)	{
				if(value->size() > 2)	{
					setGrid(parseInt(value->idx(1), 0), parseInt(value->idx(2), 0));
				}
				sendGrid();
			}
		} else if(cmd == "cells")	{
			if(zone->getZoneTypeID() == ZONE_GRID)	{
				sendCells();
			}
		} else if(cmd == "label")	{
			if(value->size() > 2)	{
				setBodyLabel(parseInt(value->idx(1), 0), parseString(value->idx(2), ""));
			}
			sendGeom();
		}
	}

	void  ZoneControlObject::setLoc(float x, float y)
	{
		if(zone)	{
			zone->setLocation(x,y);
			zone->setNeedsRefresh();
		}
	}

	void ZoneControlObject::sendLoc()
	{
		if(zone)	{
			Objsc::LangList* atom = new Objsc::LangList();
			atom->add(zone->getName());
			atom->add(zone->getZoneTypeID());
			atom->add(ZONE_DATA_LOC);
			atom->add(zone->getBounds().getX());
			atom->add(zone->getBounds().getY());
			send(0, atom);
		}
	}

	void  ZoneControlObject::setGeom(float x, float y, float w, float h)
	{
		if(zone)	{
			zone->setBounds(x,y,w,h);
			zone->setNeedsRefresh();
		}
	}

	void ZoneControlObject::sendGeom()
	{
		if(zone)	{
			Objsc::LangList* atom = new Objsc::LangList();
			atom->add(zone->getName());
			atom->add(zone->getZoneTypeID());
			atom->add(ZONE_DATA_GEOM);
			atom->add(zone->getBounds().getX());
			atom->add(zone->getBounds().getY());
			if(zone->getZoneTypeID() == Objsc::ZONE_POINT)	{
				atom->add(zone->getBounds().getWidth()/2.0f);
			} else {
				atom->add(zone->getBounds().getWidth());
				atom->add(zone->getBounds().getHeight());
			}
			send(0, atom);
		}
	}

	void ZoneControlObject::setGrid(int x, int y)
	{
		if(zone)	{
			dynamic_cast<GridZone*>(zone.get())->setGrid(x,y);
			zone->setNeedsRefresh();
		}
	}

	void ZoneControlObject::sendGrid()
	{
		if(zone)	{
			Objsc::LangList* atom = new Objsc::LangList();
			atom->add(zone->getName());
			atom->add(zone->getZoneTypeID());
			atom->add(ZONE_DATA_GRID);
			atom->add(dynamic_cast<GridZone*>(zone.get())->getCols());
			atom->add(dynamic_cast<GridZone*>(zone.get())->getRows());
			send(0, atom);
		}
	}

	void ZoneControlObject::sendCells()
	{
		GridCells cells = dynamic_cast<GridZone*>(zone.get())->getCells();
		if(cells.empty())
			return;
		Objsc::LangList* atom = new Objsc::LangList();
		atom->add(zone->getName());
		atom->add(zone->getZoneTypeID());
		atom->add(ZONE_DATA_CELLS);
		atom->add(dynamic_cast<GridZone*>(zone.get())->getCols());
		atom->add(dynamic_cast<GridZone*>(zone.get())->getRows());
		for (vector<int>::iterator it=cells.begin() ; it < cells.end(); it++ ) {
			atom->add(*it);
		}
		send(0, atom);
	}

	void  ZoneControlObject::setBodyLabel(int index, const string &label)
	{
		if(zone)	{
			zone->labelBody(index,label);
		}
	}

	void ZoneControlObject::activate()
	{
		if(zone)	{
			Objsc::LangList* atom = new Objsc::LangList();
			atom->add(zone->getName());
			atom->add(zone->getZoneTypeID());
			atom->add(ZONE_DATA_COUNT);
			atom->add(zone->getBodyCount());
			send(0, atom);
		}
	}


}
