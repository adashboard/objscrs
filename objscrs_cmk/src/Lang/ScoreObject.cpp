#include "ScoreObject.hpp"
#include "../objscrApp.hpp"

namespace Objsc
{
	const string Objsc::ScoreObject::langname = "scores";

	/////////////////////////////////////
	// SCORE OBJECT
	/////////////////////////////////////
	void ScoreObject::receive(std::string inlet, Objsc::LangAtom* atom)
	{
		if(checkInlet(inlet))
		{
			switch(atom->getType())	{
				case LANG_LIST:
					receiveList("0", dynamic_cast<Objsc::LangList*>(atom));
					break;
			}
		}
	}

	void ScoreObject::receiveList(std::string inlet, LangList* value)
	{
		string cmd = parseString(value->idx(0), "");
		if(cmd == "load")	{
			loadScore(parseString(value->idx(1), ""));
		}
	}

	void ScoreObject::loadScore(const string& scorename)
	{
		if(!scorename.empty())	{
			//Objsc::Archivist::Current()->open(scorename);
			Objsc::App::Current()->loadScore(scorename);
		}
	}

}
