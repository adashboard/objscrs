#include "BodyAreaObject.hpp"
namespace Objsc
{

	const string Objsc::BodyAreaObject::langname = "body_area";
	/////////////////////////////////////
	// BODY AREA OBJECT
	/////////////////////////////////////
	void BodyAreaObject::receive(std::string inlet, Objsc::LangAtom* atom)
	{
		if(checkInlet(inlet))
		{
			//std::cout << "handling ..." << std::endl;
			switch(atom->getType())	{
				case LANG_BANG:
				if(inlet == "0")	{
						activate();
					}
					break;
				case LANG_LIST:
					receiveBody(inlet, dynamic_cast<LangList*>(atom));
					break;
			}
		}
	}

	void BodyAreaObject::receiveBody(const std::string& inlet, Objsc::LangList* nbody)
	{
		id = parseInt(nbody->idx(2));
		shapeArea = parseFloat(nbody->idx(8));
		bodyArea = parseFloat(nbody->idx(9));
		//printf("receiveBody %d\n", nbody->size());
		if(inlet == "0")	{
			activate();
		}
	}

	void BodyAreaObject::activate()
	{
		send(0, new Objsc::LangInt(id));
		send(1, new Objsc::LangFloat(shapeArea));
		send(2, new Objsc::LangFloat(bodyArea));
	}

}
