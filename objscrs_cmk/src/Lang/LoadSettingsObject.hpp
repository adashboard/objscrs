#ifndef OBJSCRS_LOADSETTINGSOBJECT_HPP
#define OBJSCRS_LOADSETTINGSOBJECT_HPP
#include "../objscrLangObjects.hpp"
namespace Objsc
{

	/////////////////////////////////////
	// SETTINGS OBJECT
	/////////////////////////////////////
	class LoadSettingsObject : public LangObject
	{
		public:
			LoadSettingsObject()
			{
				setDisplayString(Objsc::LoadSettingsObject::langname);
				InOutLetList l;
				l.push_back("input_file");
				setInlets(l);
				setOutlets(0);
			}
			~LoadSettingsObject() {}

			static const string langname;

			const string& getLangName() const { return Objsc::LoadSettingsObject::langname; }
			const string& getDisplayString(){return displaystr;}

			void receive(std::string inlet, Objsc::LangAtom* atom);
	};


	class LoadSettingsObjectCreator : public LangObjectCreator
	{
		public:
			LoadSettingsObjectCreator(){}
			Objsc::LangObjectPtr create() const
			{
				Objsc::LangObjectPtr ptr = Objsc::LangObjectPtr(new Objsc::LoadSettingsObject());
				return ptr;
			}
	};

}
#endif
