/***************************************************************************
 *   Copyright (C) 2012 by Pierre Marchand   *
 *   pierre@oep-h.com   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#include "LangObjectFactory.h"

/**
  I got it around my head for a while and didn't find any convenient way to feed the creators.
  At some point, a file will have a lot dependencies on LangObjects, for the time being, to leave
  this role to the factory doesnt look that absurd, even if questionable. -pm

  Please keep the list sorted
  */

#include "AddObject.h"
#include "AnimObject.h"
#include "AverageObject.hpp"
#include "BangObject.hpp"
#include "BodyAgeObject.hpp"
#include "BodyAreaObject.hpp"
#include "BodyContourObject.hpp"
#include "BodyMoveObject.hpp"
#include "BodyPosObject.hpp"
#include "BodyPrefsObject.hpp"
#include "BodyProxObject.hpp"
#include "BodyRouteObject.hpp"
#include "CeilingObject.h"
#include "ChangeObject.h"
#include "ControlKeyObject.hpp"
#include "DelayObject.hpp"
#include "DivideObject.h"
#include "DrawObject.hpp"
#include "DurationObject.hpp"
#include "EqualsObject.h"
#include "FloorObject.h"
#include "GateObject.hpp"
#include "GrabObject.hpp"
#include "GreaterThanObject.h"
#include "GridCellObject.hpp"
#include "ImageObject.h"
#include "IntObject.h"
#include "InvertObject.h"
#include "LangMediaObject.hpp"
#include "LangPatch.hpp"
#include "LessThanObject.h"
#include "LineObject.hpp"
#include "LoadBangObject.h"
#include "LoadSettingsObject.hpp"
#include "MathObject.hpp"
#include "MaxObject.h"
#include "MessageObject.hpp"
#include "MetroObject.hpp"
#include "MinObject.h"
#include "MinusObject.h"
#include "ModuloObject.h"
#include "MultiplyObject.h"
#include "NumberObject.hpp"
#include "OSCInObject.hpp"
#include "OSCOutObject.hpp"
#include "PackObject.hpp"
#include "PatchOffObject.h"
#include "PatchOnObject.h"
#include "PrintObject.hpp"
#include "PrintScreenObject.h"
#include "RandomObject.hpp"
#include "ReceiveObject.hpp"
#include "RouteObject.hpp"
#include "ScoreObject.hpp"
#include "SelectObject.hpp"
#include "SendObject.hpp"
#include "SoundObject.hpp"
#include "TextObject.h"
#include "TimeObject.h"
#include "ToggleObject.hpp"
#include "TrackingObject.hpp"
#include "TriggerObject.hpp"
#include "UnpackObject.hpp"
#include "VideoObject.h"
#include "ZoneControlObject.hpp"
#include "ZoneMapObject.hpp"
#include "ZoneObject.hpp"



namespace Objsc {
	
	LangObjectFactory::LangObjectFactory() :
		creators()
	{
		InitObjectCreators();
	}

	void LangObjectFactory::InitObjectCreators()
	{
		// Objscrs objects
		addCreator(Objsc::BangObject::langname, new Objsc::BangObjectCreator());
		addCreator(Objsc::LoadBangObject::langname, new Objsc::LoadBangObjectCreator());
		addCreator(Objsc::PatchOnObject::langname, new Objsc::PatchOnObjectCreator());
		addCreator(Objsc::PatchOffObject::langname, new Objsc::PatchOffObjectCreator());
		addCreator(Objsc::ToggleObject::langname, new Objsc::ToggleObjectCreator());
		addCreator(Objsc::MessageObject::langname, new Objsc::MessageObjectCreator());
		addCreator(Objsc::PrintObject::langname, new Objsc::PrintObjectCreator());
		addCreator(Objsc::PrintScreenObject::langname, new Objsc::PrintScreenObjectCreator());
		addCreator(Objsc::NumberObject::langname, new Objsc::NumberObjectCreator());
		addCreator("float", new Objsc::NumberObjectCreator("float"));
		addCreator(Objsc::SelectObject::langname, new Objsc::SelectObjectCreator());
		addCreator("sel", new Objsc::SelectObjectCreator("sel"));
		addCreator(Objsc::TriggerObject::langname, new Objsc::TriggerObjectCreator());
		addCreator("t", new Objsc::TriggerObjectCreator("t"));
		addCreator(Objsc::UnpackObject::langname, new Objsc::UnpackObjectCreator());
		addCreator(Objsc::PackObject::langname, new Objsc::PackObjectCreator());
		addCreator(Objsc::RouteObject::langname, new Objsc::RouteObjectCreator());
		addCreator(Objsc::GateObject::langname, new Objsc::GateObjectCreator());
		addCreator(Objsc::LoadSettingsObject::langname, new Objsc::LoadSettingsObjectCreator());

		// Zone objects
		addCreator(Objsc::BodyPosObject::langname, new Objsc::BodyPosObjectCreator());
		addCreator(Objsc::BodyAgeObject::langname, new Objsc::BodyAgeObjectCreator());
		addCreator(Objsc::BodyProxObject::langname, new Objsc::BodyProxObjectCreator());
		addCreator(Objsc::BodyAreaObject::langname, new Objsc::BodyAreaObjectCreator());
		addCreator(Objsc::BodyMoveObject::langname, new Objsc::BodyMoveObjectCreator());
		addCreator(Objsc::BodyRouteObject::langname, new Objsc::BodyRouteObjectCreator());
		addCreator(Objsc::BodyContourObject::langname, new Objsc::BodyContourObjectCreator());
		addCreator(Objsc::ZoneObject::langname, new Objsc::ZoneObjectCreator());
		addCreator("body_enter", new Objsc::NoInletsZoneObjectCreator("body_enter"));
		addCreator("body_exit", new Objsc::NoInletsZoneObjectCreator("body_exit"));
		addCreator("body_within", new Objsc::NoInletsZoneObjectCreator("body_within"));
		addCreator(Objsc::ZoneControlObject::langname, new Objsc::ZoneControlObjectCreator());
		addCreator(Objsc::ZoneMapObject::langname, new Objsc::ZoneMapObjectCreator());
		addCreator(Objsc::GridCellObject::langname, new Objsc::GridCellObjectCreator());
		addCreator(Objsc::TrackingObject::langname, new Objsc::TrackingObjectCreator());
		addCreator(Objsc::ScoreObject::langname, new Objsc::ScoreObjectCreator());
		addCreator(Objsc::BodyPrefsObject::langname, new Objsc::BodyPrefsObjectCreator());

		// Media objects
		addCreator(Objsc::SoundObject::langname, new Objsc::SoundObjectCreator());
		addCreator(Objsc::VideoObject::langname, new Objsc::VideoObjectCreator());
		addCreator(Objsc::ImageObject::langname, new Objsc::ImageObjectCreator());
		addCreator(Objsc::AnimObject::langname, new Objsc::AnimObjectCreator());
		addCreator(Objsc::TextObject::langname, new Objsc::TextObjectCreator());
		addCreator(Objsc::GrabObject::langname, new Objsc::GrabObjectCreator());

		// Comms objects
		addCreator(Objsc::OSCOutObject::langname, new Objsc::OSCOutObjectCreator());
		addCreator(Objsc::OSCInObject::langname, new Objsc::OSCInObjectCreator());
		addCreator(Objsc::SendObject::langname, new Objsc::SendObjectCreator());
		addCreator(Objsc::ReceiveObject::langname, new Objsc::ReceiveObjectCreator());
		addCreator(Objsc::ControlKeyObject::langname, new Objsc::ControlKeyObjectCreator());

		// Graphic objects
		addCreator(Objsc::DrawObject::langname, new Objsc::DrawObjectCreator());

		// Math objects
		addCreator(Objsc::EqualsObject::langname, new Objsc::EqualsObjectCreator());
		addCreator(Objsc::GreaterThanObject::langname, new Objsc::GreaterThanObjectCreator());
		addCreator(Objsc::LessThanObject::langname, new Objsc::LessThanObjectCreator());
		addCreator(Objsc::AddObject::langname, new Objsc::AddObjectCreator());
		addCreator(Objsc::MinusObject::langname, new Objsc::MinusObjectCreator());
		addCreator(Objsc::MultiplyObject::langname, new Objsc::MultiplyObjectCreator());
		addCreator(Objsc::DivideObject::langname, new Objsc::DivideObjectCreator());
		addCreator(Objsc::ModuloObject::langname, new Objsc::ModuloObjectCreator());
		addCreator(Objsc::InvertObject::langname, new Objsc::InvertObjectCreator());
		addCreator(Objsc::CeilingObject::langname, new Objsc::CeilingObjectCreator());
		addCreator(Objsc::FloorObject::langname, new Objsc::FloorObjectCreator());
		addCreator(Objsc::AverageObject::langname, new Objsc::AverageObjectCreator());
		addCreator(Objsc::RandomObject::langname, new Objsc::RandomObjectCreator());
		addCreator(Objsc::MaxObject::langname, new Objsc::MaxObjectCreator());
		addCreator(Objsc::MinObject::langname, new Objsc::MinObjectCreator());
		addCreator(Objsc::ChangeObject::langname, new Objsc::ChangeObjectCreator());
		addCreator(Objsc::IntObject::langname, new Objsc::IntObjectCreator());

		// Time objects
		addCreator(Objsc::MetroObject::langname, new Objsc::MetroObjectCreator());
		addCreator(Objsc::TimeObject::langname, new Objsc::TimeObjectCreator());
		addCreator(Objsc::DurationObject::langname, new Objsc::DurationObjectCreator());
		addCreator(Objsc::LineObject::langname, new Objsc::LineObjectCreator());
		addCreator(Objsc::DelayObject::langname, new Objsc::DelayObjectCreator());

	}

	std::vector<std::string> LangObjectFactory::getRegisteredLangNames() const
	{
		std::vector<std::string> ret;
		for(LangObjectCreatorMap::const_iterator it(creators.begin()); it != creators.end(); it++)
		{
			ret.push_back(it->first);
		}
		return ret;
	}

	Objsc::LangObjectPtr LangObjectFactory::create(const string& langname)
	{
		LangObjectCreatorMap::iterator iter = creators.find(langname);
		if(iter == creators.end()) {
			throw std::exception();
		}
		return (*iter).second->create();
	}

	void LangObjectFactory::addCreator(const string& langname, LangObjectCreator* creatorPtr)
	{
		creators.insert(LangObjectCreatorEntry(langname, creatorPtr));
	}
	
} // namespace Objsc
