#include "LoadSettingsObject.hpp"
#include "../objscrCommands.hpp"

namespace Objsc
{
	const string Objsc::LoadSettingsObject::langname = "settings";

	/////////////////////////////////////
	// SETTINGS OBJECT
	/////////////////////////////////////
	void LoadSettingsObject::receive(std::string inlet, Objsc::LangAtom *atom)
	{
		if(atom->getType() != LANG_NULL)
		{
			std::string settings;
			if(atom->getType() == LANG_LIST)
				settings = getFirstAsString(dynamic_cast<Objsc::LangList*>(atom));
			if(!settings.empty())
			{
				Objsc::CommandPtr cp(Objsc::CommandDispatcher::Current()->createCommand("_gui_", "load_settings"));
				cp->getData()->addArg(settings);
				Objsc::CommandDispatcher::Current()->doCommand(cp);
			}
			else
				Objsc::CommandDispatcher::Current()->doCommand("_gui_", "load_settings");
		}
	}



}
