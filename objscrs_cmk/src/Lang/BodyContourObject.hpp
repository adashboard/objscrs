#ifndef OBJSCRS_BODYCONTOUROBJECT_HPP
#define OBJSCRS_BODYCONTOUROBJECT_HPP
#include "../objscrLangObjects.hpp"
#include "../objscrBody.hpp"

namespace Objsc
{
	/////////////////////////////////////
	// BODY CONTOUR OBJECT
	/////////////////////////////////////
	class BodyContourObject : public LangObject
	{
		protected:
			int 		id;

			void receiveBody(const std::string& inlet, LangList* nbody);

		public:
			BodyContourObject() : id(0)
			{
				setInlets(2);
				setOutlets(1);
			}
			~BodyContourObject() {}

			static const string langname;
			const string& getLangName() const { return Objsc::BodyContourObject::langname; }

			const string& getDisplayString()
			{
				if(displaystr.empty())	{
					return Objsc::BodyContourObject::langname;
				}
				return displaystr;
			}

			void receive(std::string inlet, Objsc::LangAtom* atom);
			void activate();
	};

	class BodyContourObjectCreator : public LangObjectCreator
	{
		public:
			Objsc::LangObjectPtr create() const { return Objsc::LangObjectPtr(new Objsc::BodyContourObject());}
	};
}
#endif
