/***************************************************************************
 *   Copyright (C) 2012 by Pierre Marchand   *
 *   pierre@oep-h.com   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef PRINTSCREENOBJECT_H
#define PRINTSCREENOBJECT_H

#include "PrintObject.hpp"

namespace Objsc
{
	class PrintScreenObject : public PrintObject
	{

		public:
			static const string langname;
			const string& getLangName() const { return Objsc::PrintScreenObject::langname; }

			void setLabel(string nlabel);
			const string& getDisplayString();
			void activate();
	};

	class PrintScreenObjectCreator : public LangObjectCreator
	{
		public:
			Objsc::LangObjectPtr create() const { return Objsc::LangObjectPtr(new Objsc::PrintScreenObject());}
	};

}

#endif // PRINTSCREENOBJECT_H
