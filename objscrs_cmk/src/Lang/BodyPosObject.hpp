#ifndef OBJSCRS_BODYPOSOBJECT_HPP
#define OBJSCRS_BODYPOSOBJECT_HPP
#include "../objscrLangObjects.hpp"
namespace Objsc
{
	/////////////////////////////////////
	// BODY POS OBJECT
	/////////////////////////////////////
	class BodyPosObject : public LangObject
	{
		protected:
			int 		id;
			float 	x;
			float 	y;

			void receiveBody(const std::string& inlet, LangList* nbody);

		public:
			BodyPosObject() : id(0), x(0.0f), y(0.0f)
			{
				setInlets(2);
				setOutlets(3);
			}
			~BodyPosObject() {}

			static const string langname;
			const string& getLangName() const { return Objsc::BodyPosObject::langname; }

			const string& getDisplayString()
			{
				if(displaystr.empty())	{
					return Objsc::BodyPosObject::langname;
				}
				return displaystr;
			}

			void receive(std::string inlet, Objsc::LangAtom* atom);
			void activate();
	};

	class BodyPosObjectCreator : public LangObjectCreator
	{
		public:
			Objsc::LangObjectPtr create() const { return Objsc::LangObjectPtr(new Objsc::BodyPosObject());}
	};

}
#endif
