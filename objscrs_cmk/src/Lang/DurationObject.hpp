#ifndef OBJSCRS_DURATIONOBJECT_HPP
#define OBJSCRS_DURATIONOBJECT_HPP
#include "../objscrLangObjects.hpp"
namespace Objsc
{
	////////////////////////////////////////
	// DURATION OBJECT
	////////////////////////////////////////
	class DurationObject : public LangObject
	{
		protected:
			int 			count;
			bool		active;

			void receiveList(std::string inlet, LangList* value);

		public:
			DurationObject();
			~DurationObject();

			static const string langname;
			const string& getLangName() const { return Objsc::DurationObject::langname; }

			void receive(std::string inlet, Objsc::LangAtom* atom);
			void activate();
			void update(int timeframe);

			void start();
			void stop() { active=false; }
	};

	class DurationObjectCreator : public LangObjectCreator
	{
		public:
			Objsc::LangObjectPtr create() const { return Objsc::LangObjectPtr(new Objsc::DurationObject());}
	};

}
#endif
