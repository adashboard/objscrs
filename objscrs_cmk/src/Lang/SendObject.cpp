#include "SendObject.hpp"

#include "../objscrApp.hpp"

namespace Objsc
{
	const string Objsc::SendObject::langname = "send";

	/////////////////////////////////////
	// SEND OBJECT
	/////////////////////////////////////
	SendObject::SendObject() : label(), recname()
	{
		setInlets(1);
		setOutlets(0);
		min_args=1;
	}

	SendObject::~SendObject()
	{

	}

	bool SendObject::parseArgs(Objsc::LangList* args)
	{
		if(args->size() < min_args)	{
			return false;
		} else {
			setLabel(getItemAsString(args, 0));
			return true;
		}
	}

	void SendObject::setLabel(string nlabel)
	{
		label.assign(nlabel);
		recname.assign(makeReceiverName(label));
		displaystr = getLangName() + " " + label;
	}

	void SendObject::receive(std::string inlet, Objsc::LangAtom* atom)
	{
		Objsc::App::Current()->getScore()->sendToCurrentZoneMap(recname, Objsc::LangAtomPtr(cloneAtom(atom)));
	}

}
