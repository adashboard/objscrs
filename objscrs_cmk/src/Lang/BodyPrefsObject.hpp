#ifndef OBJSCRS_BODYPREFSOBJECT_HPP
#define OBJSCRS_BODYPREFSOBJECT_HPP
#include "../objscrLangObjects.hpp"
namespace Objsc
{

	/////////////////////////////////////
	// BODY PREFS OBJECT
	/////////////////////////////////////
	class BodyPrefsObject : public LangObject
	{
		protected:

			void receiveList(std::string inlet, LangList* value);

		public:
			BodyPrefsObject()
			{
				setInlets(11);
				setOutlets(0);
			}
			~BodyPrefsObject() {}

			static const string langname;
			const string& getLangName() const { return Objsc::BodyPrefsObject::langname; }


			const string& getDisplayString()
			{
				return Objsc::BodyPrefsObject::langname;
			}

			void receive(std::string inlet, Objsc::LangAtom* atom);
	};


	class BodyPrefsObjectCreator : public LangObjectCreator
	{
		public:
			Objsc::LangObjectPtr create() const { return Objsc::LangObjectPtr(new Objsc::BodyPrefsObject());}
	};
}
#endif
