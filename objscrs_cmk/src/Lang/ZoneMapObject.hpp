#ifndef OBJSCRS_ZONEMAPOBJECT_HPP
#define OBJSCRS_ZONEMAPOBJECT_HPP
#include "../objscrLangObjects.hpp"
namespace Objsc
{

	/////////////////////////////////////
	// ZONE MAP OBJECT
	/////////////////////////////////////
	class ZoneMapObject : public LangObject
	{
		protected:
			void showZones();
			void hideZones();
			void gotoMap(const string& mapname);
			void receiveList(std::string inlet, LangList* value);

		public:
			ZoneMapObject()
			{
				setInlets(1);
				setOutlets(2);
			}
			~ZoneMapObject() {}

			static const string langname;
			const string& getLangName() const { return Objsc::ZoneMapObject::langname; }


			const string& getDisplayString()
			{
				return Objsc::ZoneMapObject::langname;
			}

			void receive(std::string inlet, Objsc::LangAtom* atom);
	};

	class ZoneMapObjectCreator : public LangObjectCreator
	{
		public:
			Objsc::LangObjectPtr create() const { return Objsc::LangObjectPtr(new Objsc::ZoneMapObject());}
	};

}
#endif
