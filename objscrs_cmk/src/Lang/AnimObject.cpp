/***************************************************************************
 *   Copyright (C) 2012 by Pierre Marchand   *
 *   pierre@oep-h.com   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#include "AnimObject.h"

#include "../objscrMediaPlayer.hpp"

namespace Objsc {
	const string Objsc::AnimObject::langname = "anim";
	
	AnimObject::~AnimObject()
	{

	}

	void AnimObject::setLabel(string nlabel)
	{
		label.assign(nlabel);
		displaystr = Objsc::AnimObject::langname + " " + label;
	}

	int AnimObject::loadAnimFiles(const string& sBasename)
	{
		//int found;
		basename = sBasename;
		frames.clear();
		currentFrame = 0;
		finder.open(Objsc::ProjectFiles::Current()->makeImagePath());
		if(finder.empty())	{
			return 0;
		}
		const Objsc::StringVector items  = finder.getFilenames();
		for(Objsc::StringVector::const_iterator item = items.begin(); item != items.end(); ++item)	{
			if(int((*item).find(basename)) == 0)	{
				frames.push_back((*item));
			}
		}
		if (frames.empty())	{
			return 0;
		} else {
			sort(frames.begin(),frames.end());
			return int(frames.size());
		}
	}

	int AnimObject::reloadAnimFiles()
	{
		if(basename.empty())	{
			return 0;
		}
		frames.clear();
		finder.open(Objsc::ProjectFiles::Current()->makeImagePath());
		if(finder.empty())	{
			return 0;
		}
		const Objsc::StringVector items  = finder.getFilenames();
		for(Objsc::StringVector::const_iterator item = items.begin(); item != items.end(); ++item)	{
			if(int((*item).find(basename)) == 0)	{
				frames.push_back((*item));
			}
		}
		if (frames.empty())	{
			return 0;
		} else {
			sort(frames.begin(),frames.end());
			return int(frames.size());
		}
	}

	void AnimObject::nextFrame()
	{
		if(totalFrames == 0)
			return;
		if(++currentFrame >= totalFrames)	{
			currentFrame = 0;
		}
		//std::cout << "nextFrame " << totalFrames << " " << currentFrame << std::endl;
		Objsc::MediaPlayer::Current()->loadImage(frames[currentFrame], label);
		Objsc::MediaPlayer::Current()->setLayer(label, lyr);
		send(0, new Objsc::LangInt(currentFrame));
	}

	void AnimObject::prevFrame()
	{
		if(totalFrames == 0)
			return;
		if(--currentFrame < 0)	{
			currentFrame = totalFrames-1;
		}
		//std::cout << "nextFrame " << totalFrames << " " << currentFrame << std::endl;
		Objsc::MediaPlayer::Current()->loadImage(frames[currentFrame], label);
		Objsc::MediaPlayer::Current()->setLayer(label, lyr);
		send(0, new Objsc::LangInt(currentFrame));
	}

	void AnimObject::firstFrame()
	{
		if(totalFrames == 0)
			return;
		currentFrame = 0;
		Objsc::MediaPlayer::Current()->loadImage(frames[currentFrame], label);
		Objsc::MediaPlayer::Current()->setLayer(label, lyr);
		send(0, new Objsc::LangInt(currentFrame));
	}

	void AnimObject::lastFrame()
	{
		if(totalFrames ==0)
			return;
		currentFrame = totalFrames-1;
		Objsc::MediaPlayer::Current()->loadImage(frames[currentFrame], label);
		Objsc::MediaPlayer::Current()->setLayer(label, lyr);
		send(0, new Objsc::LangInt(currentFrame));
	}

	void AnimObject::gotoFrame(int frameNum)
	{
		if(totalFrames ==0)
			return;
		if(frameNum > -1 or frameNum < totalFrames)	{
			currentFrame = frameNum;
			Objsc::MediaPlayer::Current()->loadImage(frames[currentFrame], label);
			Objsc::MediaPlayer::Current()->setLayer(label, lyr);
			send(0, new Objsc::LangInt(currentFrame));
		}
	}

	void AnimObject::loadMedia(const string& filename, int layer, bool show)
	{
		if(!filename.empty() && !label.empty())	{
			totalFrames = loadAnimFiles(filename);
			if(totalFrames > 0)	{
				Objsc::MediaPlayer::Current()->loadImage(frames[currentFrame], label);
				lyr = layer;
				if(layer != 0)	{
					if(show)	{
						Objsc::MediaPlayer::Current()->show(label, layer);
					} else {
						Objsc::MediaPlayer::Current()->setLayer(label, layer);
					}
				}
			}
		}
	}

	void AnimObject::receiveList(std::string inlet, LangList* value)
	{

		string cmd = parseString(value->idx(0), "");
		if(cmd.empty()) {
			if(parseInt(value->idx(0), -1) == 1)	{
				showMedia();
			} else if(parseInt(value->idx(0), -1) == 0)	{
				hideMedia();
			}
		} else {
			if(cmd == "load")	{
				if(value->size() == 2)	{
					loadMedia(parseString(value->idx(1), ""),0,false);
				} else if(value->size() == 3)	{
					loadMedia(parseString(value->idx(1), ""),parseInt(value->idx(2), 0),false);
				} if(value->size() > 3)	{
					loadMedia(parseString(value->idx(1), ""),parseInt(value->idx(2), 0),parseBool(value->idx(3), false));
				}
				send(2, new Objsc::LangInt(totalFrames));
			} else if(cmd == "show")	{
				if(value->size() == 2)	{
					showMedia(parseInt(value->idx(1), 0));
				} else {
					showMedia();
				}
			} else if(cmd == "hide")	{
				hideMedia();
			} else if(cmd == "fullscreen")	{
				fullscreenMedia();
				fs = true;
			} else if(cmd == "centre")	{
				centreMedia();
			} else if(cmd == "size")	{
				if(value->size() > 2)	{
					sizeMedia(parseFloat(value->idx(1), -1), parseFloat(value->idx(2), -1));
				}
			} else if(cmd == "loc")	{
				if(value->size() > 2)	{
					locMedia(parseFloat(value->idx(1), 0), parseFloat(value->idx(2), 0));
				}
			} else if(cmd == "geom")	{
				if(value->size() > 4)	{
					geomMedia(parseFloat(value->idx(1), 0), parseFloat(value->idx(2), 0),parseFloat(value->idx(3), -1), parseFloat(value->idx(4), -1));
				}
			} else if(cmd == "layer")	{
				if(value->size() > 1)	{
					setLayerMedia(parseInt(value->idx(1), 0));
					lyr = parseInt(value->idx(1), 0);
				}
			} else if(cmd == "remove")	{
				removeMedia();
			} else if(cmd == "reset")	{
				removeMedia();
			} else if(cmd == "reload")	{
				totalFrames = reloadAnimFiles();
				send(2, new Objsc::LangInt(totalFrames));
			} else if(cmd == "next")	{
				nextFrame();
				showMedia(lyr);
				if (fs)	{
					fullscreenMedia();
				}
			} else if(cmd == "prev")	{
				prevFrame();
				showMedia(lyr);
				if (fs)	{
					fullscreenMedia();
				}
			} else if(cmd == "go")	{
				if(value->size() == 2)	{
					gotoFrame(parseInt(value->idx(1), -1));
				}
				showMedia(lyr);
				if (fs)	{
					fullscreenMedia();
				}
			} else if(cmd == "first")	{
				firstFrame();
				showMedia(lyr);
				if (fs)	{
					fullscreenMedia();
				}
			} else if(cmd == "last")	{
				lastFrame();
				showMedia(lyr);
				if (fs)	{
					fullscreenMedia();
				}
			}
		}
	}

	void AnimObject::activate()
	{
		nextFrame();
		showMedia(lyr);
		if (fs)	{
			fullscreenMedia();
		}
	}

} // namespace Objsc
