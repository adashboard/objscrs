#ifndef OBJSCRS_BODYROUTEOBJECT_HPP
#define OBJSCRS_BODYROUTEOBJECT_HPP
#include "../objscrLangObjects.hpp"
namespace Objsc
{

	/////////////////////////////////////
	// BODY ROUTE OBJECT
	/////////////////////////////////////
	class BodyRouteObject : public LangObject
	{
		protected:
			Objsc::LangList	keys;
			string			label;

			void receiveList(const std::string& inlet, Objsc::LangList* atom);

		public:
			BodyRouteObject() : keys(), label()
			{
				setInlets(1);
				setOutlets(0);
			}
			~BodyRouteObject() {}

			static const string langname;
			const string& getLangName() const { return Objsc::BodyRouteObject::langname; }
			const string& getDisplayString(){return displaystr;}

			void setLabel(string nlabel) {label.assign(nlabel);}
			const string& getLabel() const { return label; }

			bool parseArgs(Objsc::LangList* args);
			void receive(std::string inlet, Objsc::LangAtom* atom);

			const bool hasArgs() const { return true;}
			Objsc::LangList* getArgs() { return &keys; }
			string getArgString() { return listToString(&keys); }
	};

	class BodyRouteObjectCreator : public LangObjectCreator
	{
		public:
			Objsc::LangObjectPtr create() const { return Objsc::LangObjectPtr(new Objsc::BodyRouteObject());}
	};
}
#endif
