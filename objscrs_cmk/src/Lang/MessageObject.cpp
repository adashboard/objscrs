#include "MessageObject.hpp"
namespace Objsc
{

	const string Objsc::MessageObject::langname = "message";
	/////////////////////////////////////
	// MESSAGE OBJECT
	/////////////////////////////////////
	void MessageObject::setDisplayString(const string& str)
	{
		//printf("MessageObject::setDisplayString\n");
		Objsc::tokenizeString(str, message);
		displaystr = listToString(&message);
	}

	void MessageObject::receive(std::string inlet, Objsc::LangAtom* atom)
	{
		switch(atom->getType())	{
			case LANG_NULL:
				break;
			case LANG_BANG:
			if(inlet == "0")
					activate();
				break;
			case LANG_INT:
				message.setVar(1, Objsc::LangAtomPtr(atom));
				if(inlet == "0")
					activate();
				break;
			case LANG_FLOAT:
				message.setVar(1, Objsc::LangAtomPtr(atom));
				if(inlet == "0")
					activate();
				break;
			case LANG_STRING:
				message.setVar(1, Objsc::LangAtomPtr(atom));
				if(inlet == "0")
					activate();
				break;
			case LANG_VAR:
				message.setVar(1, Objsc::LangAtomPtr(atom));
				if(inlet == "0")
					activate();
				break;
			case LANG_LIST:
				//printf("MessageObject::receive LANG_LIST A\n");
				int limit = dynamic_cast<Objsc::LangList*>(atom)->size();
				for(int i = 0; i < limit; ++i)	{
					message.setVar(i+1, dynamic_cast<Objsc::LangList*>(atom)->get(i));
				}
				//printf("MessageObject::receive LANG_LIST DONE\n");
				if(inlet == "0")
					activate();
				break;
		}
	}

	void MessageObject::activate()
	{
		//printf("activate [[%s]]\n", listToString(&message).c_str());
		if(message.size() > 1)	{
			send(0, &message);
		} else if(message.size() == 1)	{
			switch(message.getItemType(0))	{
				case LANG_NULL:
					break;
				case LANG_BANG:
					send(0, new Objsc::LangBang());
					break;
				case LANG_INT:
					send(0, new Objsc::LangInt(getItemAsInt(&message, 0)));
					break;
				case LANG_FLOAT:
					send(0, new Objsc::LangFloat(getItemAsFloat(&message, 0)));
					break;
				case LANG_STRING:
					//send(0, new Objsc::LangString(getFirstAsString(&message))); // NOTE: breaks a lot of existing code
					send(0, &message);
					break;
				case LANG_VAR:
					send(0, &message); // NOTE: change to send out value
				case LANG_LIST:
					send(0, &message);
					break;
			}
		}
	}


}
