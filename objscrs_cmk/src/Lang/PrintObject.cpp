#include "PrintObject.hpp"
namespace Objsc
{
	const string Objsc::PrintObject::langname = "print";

	/////////////////////////////////////
	// PRINT OBJECT
	/////////////////////////////////////
	bool PrintObject::parseArgs(Objsc::LangList* args)
	{
		if(args->size() < min_args)	{
			return false;
		} else {
			//setLabel(getItemAsString(args, 1));
			setLabel(getItemAsString(args, 0));
			return true;
		}
	}

	void PrintObject::setLabel(string nlabel)
	{
		label.assign(nlabel);
		displaystr = Objsc::PrintObject::langname + " " + label;
	}

	const string& PrintObject::getDisplayString()
	{
		if(displaystr.empty())	{
			return Objsc::PrintObject::langname;
		}
		return displaystr;
	}

	void PrintObject::receive(std::string inlet, Objsc::LangAtom* atom)
	{
		switch(atom->getType())	{
			case LANG_NULL:
				buffer.assign(Objsc::LANG_NULLSTRING);
				break;
			case LANG_BANG:
				buffer.assign(Objsc::LANG_BANGSTRING);
				activate();
				break;
			case LANG_INT:
				buffer.assign(intToString(parseInt(atom)));
				activate();
				break;
			case LANG_FLOAT:
				buffer.assign(floatToString(parseFloat(atom)));
				activate();
				break;
			case LANG_STRING:
				buffer.assign(parseString(atom));
				activate();
				break;
			case LANG_VAR:
				buffer.assign(varToString(dynamic_cast<Objsc::LangVar*>(atom)));
				activate();
				break;
			case LANG_LIST:
				receiveList(dynamic_cast<Objsc::LangList*>(atom));
				activate();
				break;
		}
	}

	void PrintObject::activate()
	{
		if(label.empty())	{
			std::cout << buffer << std::endl;
		} else {
			std::cout << label << " " << buffer << std::endl;
		}
		buffer.clear();
	}

}
