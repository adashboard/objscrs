#ifndef OBJSCRS_BANGOBJECT_HPP
#define OBJSCRS_BANGOBJECT_HPP
#include "../objscrLangObjects.hpp"
namespace Objsc
{
	/////////////////////////////////////
	// BANG OBJECT
	/////////////////////////////////////
	class BangObject : public LangObject
	{
		protected:
			Objsc::LangObserverPtr 		observer;

		public:
			BangObject() : observer()
			{
				setInlets(1);
				setOutlets(1);
			}
			~BangObject() {}

			static const string langname;
			const string& getLangName() const { return Objsc::BangObject::langname; }
			const string& getDisplayString(){return Objsc::BangObject::langname;}

			void receive(std::string inlet, Objsc::LangAtom* atom) {activate();}
			void activate()
			{
				if(observer)
					observer->objActivated();
				send(0, new Objsc::LangBang());
			}

			void setObserver(Objsc::LangObserverPtr obsPtr) {observer=obsPtr;}
			void removeObserver() {observer.reset();}
	};

	class BangObjectCreator : public LangObjectCreator
	{
		public:
			Objsc::LangObjectPtr create() const { return Objsc::LangObjectPtr(new Objsc::BangObject());}
	};
}
#endif
