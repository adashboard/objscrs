#ifndef OBJSCRS_BODYAREAOBJECT_HPP
#define OBJSCRS_BODYAREAOBJECT_HPP
#include "../objscrLangObjects.hpp"
namespace Objsc
{
	/////////////////////////////////////
	// BODY AREA OBJECT
	/////////////////////////////////////
	class BodyAreaObject : public LangObject
	{
		protected:
			int 		id;
			float 	shapeArea;
			float 	bodyArea;

			void receiveBody(const std::string& inlet, LangList* nbody);

		public:
			BodyAreaObject() : id(0), shapeArea(0.0f), bodyArea(0.0f)
			{
				setInlets(2);
				setOutlets(3);
			}
			~BodyAreaObject() {}

			static const string langname;
			const string& getLangName() const { return Objsc::BodyAreaObject::langname; }

			const string& getDisplayString()
			{
				if(displaystr.empty())	{
					return Objsc::BodyAreaObject::langname;
				}
				return displaystr;
			}

			void receive(std::string inlet, Objsc::LangAtom* atom);
			void activate();
	};

	class BodyAreaObjectCreator : public LangObjectCreator
	{
		public:
			Objsc::LangObjectPtr create() const { return Objsc::LangObjectPtr(new Objsc::BodyAreaObject());}
	};
}
#endif
