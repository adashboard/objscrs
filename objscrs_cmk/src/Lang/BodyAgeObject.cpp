#include "BodyAgeObject.hpp"
namespace Objsc
{
	const string Objsc::BodyAgeObject::langname = "body_age";
	/////////////////////////////////////
	// BODY AGE OBJECT
	/////////////////////////////////////
	void BodyAgeObject::receive(std::string inlet, Objsc::LangAtom* atom)
	{
		if(checkInlet(inlet))
		{
			//std::cout << "handling ..." << std::endl;
			switch(atom->getType())	{
				case LANG_BANG:
				if(inlet == "0")	{
						activate();
					}
					break;
				case LANG_LIST:
					receiveBody(inlet, dynamic_cast<LangList*>(atom));
					break;
			}
		}
	}

	void BodyAgeObject::receiveBody(const std::string& inlet, Objsc::LangList* nbody)
	{
		id = parseInt(nbody->idx(2));
		age = parseInt(nbody->idx(3));
		if(inlet == "0")	{
			activate();
		}
	}

	void BodyAgeObject::activate()
	{
		send(0, new Objsc::LangInt(id));
		send(1, new Objsc::LangInt(age));
	}

}
