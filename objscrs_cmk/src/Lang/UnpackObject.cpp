#include "UnpackObject.hpp"
namespace Objsc
{
	const string Objsc::UnpackObject::langname = "unpack";

	/////////////////////////////////////
	// UNPACK OBJECT
	/////////////////////////////////////
	bool UnpackObject::parseArgs(Objsc::LangList* args)
	{
		//printf("UnpackObject::parseArgs START %d\n", args->size());
		if(args->size() < min_args)	{
			return false;
		} else {
			//keys.copy(args, 1);
			keys.copy(args);
			if(label.empty())	{
				//printf("UnpackObject::parseArgs <%s> A\n", displaystr.c_str());
				displaystr.assign(getLangName().c_str());
				displaystr.append(" ");
				displaystr.append(listToString(&keys));
			} else {
				displaystr.assign(label.c_str());
				displaystr.append(" ");
				displaystr.append(listToString(&keys));
			}
			//printf("UnpackObject::parseArgs <%s>\n", displaystr.c_str());
			setOutlets(keys.size());
			//printf("UnpackObject::parseArgs DONE %d\n", keys.size());
			return true;
		}
	}

	void UnpackObject::receive(std::string inlet, Objsc::LangAtom* atom)
	{
		switch(atom->getType())	{
			case LANG_NULL:
				break;
			case LANG_BANG:
				break;
			case LANG_INT:
				break;
			case LANG_FLOAT:
				break;
			case LANG_STRING:
				break;
			case LANG_VAR:
				break;
			case LANG_LIST:
				receiveList(inlet, dynamic_cast<Objsc::LangList*>(atom));
				break;
		}
	}

	void UnpackObject::receiveList(std::string inlet, Objsc::LangList* atom)
	{
		if(atom->size() >= num_outlets)	{
			for(int i = num_outlets-1; i > -1; --i)	{
				//printf("types: %d %d\n", atom->idx(i)->getType(), keys.idx(i)->getType());
				if(atom->idx(i)->getType() == keys.idx(i)->getType())	{
					send(i, atom->idx(i));
				} else {
					send(i, keys.idx(i));
				}
			}
		}
	}


}
