
#include "BodyPosObject.hpp"
#include "BodyMoveObject.hpp"

namespace Objsc
{
	const string Objsc::BodyPosObject::langname = "body_xy";

	/////////////////////////////////////
	// BODY POS OBJECT
	/////////////////////////////////////
	void BodyPosObject::receive(std::string inlet, Objsc::LangAtom* atom)
	{
		if(checkInlet(inlet))
		{
			//std::cout << "handling ..." << std::endl;
			switch(atom->getType())	{
				case LANG_BANG:
				if(inlet ==" 0")
					{
						activate();
					}
					break;
				case LANG_LIST:
					receiveBody(inlet, dynamic_cast<LangList*>(atom));
					break;
			}
		}
	}

	void BodyPosObject::receiveBody(const std::string& inlet, Objsc::LangList* nbody)
	{
		id = parseInt(nbody->idx(2));
		x = parseFloat(nbody->idx(4));
		y = parseFloat(nbody->idx(5));
		if(inlet == "0")	{
			activate();
		}
	}

	void BodyPosObject::activate()
	{
		send(0, new Objsc::LangInt(id));
		send(1, new Objsc::LangFloat(x));
		send(2, new Objsc::LangFloat(y));
	}

}
