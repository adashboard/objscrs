
#include "BodyMoveObject.hpp"
#include "ofMain.h"

#include <cmath>

namespace Objsc
{

	const string Objsc::BodyMoveObject::langname = "body_move";

	/////////////////////////////////////
	// BODY MOVE OBJECT
	/////////////////////////////////////
	void BodyMoveObject::receive(std::string inlet, Objsc::LangAtom* atom)
	{
		if(checkInlet(inlet))
		{
			//std::cout << "handling ..." << std::endl;
			switch(atom->getType())	{
				case LANG_BANG:
				if(inlet == "0")	{
						activate();
					}
					break;
				case LANG_LIST:
					receiveBody(inlet, dynamic_cast<LangList*>(atom));
					break;
			}
		}
	}

	void BodyMoveObject::receiveBody(const std::string& inlet, Objsc::LangList* nbody)
	{
		id = parseInt(nbody->idx(2));
		x = parseFloat(nbody->idx(4));
		y = parseFloat(nbody->idx(5));
		t = ofGetElapsedTimef();
		if (t-pT > 0.0f)	{
			velX = (x-pX)/(t-pT);
			velY = (y-pY)/(t-pT);
			dist = (float)sqrt(pow(x-pX, 2) + pow(y-pY, 2));
			vel = dist/(t-pT);
		} else {
			velX = 0.0f;
			velY = 0.0f;
			dist = 0.0f;
			vel = 0.0f;
		}
		pX = x;
		pY = y;
		pT = t;
		if(inlet == "0")	{
			activate();
		}
	}

	void BodyMoveObject::activate()
	{
		send(0, new Objsc::LangInt(id));
		send(1, new Objsc::LangFloat(vel));
		send(2, new Objsc::LangFloat(velX));
		send(3, new Objsc::LangFloat(velY));
		send(4, new Objsc::LangFloat(dist));
	}

}
