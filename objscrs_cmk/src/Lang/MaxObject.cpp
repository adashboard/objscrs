/***************************************************************************
 *   Copyright (C) 2012 by Pierre Marchand   *
 *   pierre@oep-h.com   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#include "MaxObject.h"


namespace Objsc {
	const string Objsc::MaxObject::langname = "max";
	
	void MaxObject::eval(float input)
	{
		limit = MAX(input,value);
		send(1, new Objsc::LangFloat(limit));
	}

	void MaxObject::receive(std::string inlet, Objsc::LangAtom* atom)
	{
		switch(atom->getType())	{
			case LANG_NULL:
				//send(1, new Objsc::LangBang());
				break;
			case LANG_BANG:
				activate();
				break;
			case LANG_INT:
			if(inlet == "0")	{
						eval(parseInt(atom));
						activate();
			}
			else if(inlet == "1")
			{
						eval(parseInt(atom));

				}
				break;
			case LANG_FLOAT:
			if(inlet == "0")	{
						eval(parseFloat(atom));
						activate();
			}
			else if(inlet =="1")
			{
						eval(parseFloat(atom));

				}
				break;
			case LANG_STRING:
				//send(1, new Objsc::LangBang());
				break;
			case LANG_VAR:
				//send(1, new Objsc::LangBang());
				break;
			case LANG_LIST:
				receiveList(inlet, dynamic_cast<Objsc::LangList*>(atom));
				break;
		}
	}

	void MaxObject::receiveList(std::string inlet, LangList* value)
	{
		string cmd = parseString(value->idx(0), "");
		if(cmd == "reset" && inlet=="0")	{
			reset();
		} else if(cmd == "")	{
			if(inlet == "0")	{
					eval(parseFloat(value->idx(0), 0.0f));
					activate();
			}
			else if(inlet == "1")
			{
					eval(parseFloat(value->idx(0), 0.0f));

			}
		}
	}

	void MaxObject::activate()
	{
		send(0, new Objsc::LangFloat(limit));
	}

	void MaxObject::reset()
	{
		limit=value;
	}
} // namespace Objsc
