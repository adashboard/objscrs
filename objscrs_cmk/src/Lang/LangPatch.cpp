
#include "LangPatch.hpp"
#include "LangObjectFactory.h"

#include <exception>


namespace Objsc
{

	////////////////////////////////////////
	// DECLARATIONS
	////////////////////////////////////////
	const string Objsc::LangPatch::langname = "patch";


	/////////////////////////////////////
	// LANG PATCH
	/////////////////////////////////////
	Objsc::LangObjectPtr LangPatch::createObject(string langname, bool addreceiver)
	{
		if(Objsc::LangObjectFactory::Current()->hasCreator(langname))	{
			objects.push_back(Objsc::LangObjectFactory::Current()->create(langname));
			//printf("LangPatch::createObject AA <%s>\n", langname.c_str());
			if(addreceiver)	{
				//printf("receiver <%s>\n", objects.back()->getReceiverName().c_str());
				if(objects.back()->hasReceiverName())	{
					addObjectToReceiver(makeReceiverName(objects.back()->getReceiverName()), objects.back());
				}
			}
			return objects.back();
		} else {
			throw std::exception();
		}
	}

	Objsc::LangObjectPtr LangPatch::createObject(string langname, float x, float y, bool addreceiver)
	{
		if(Objsc::LangObjectFactory::Current()->hasCreator(langname))	{
			objects.push_back(Objsc::LangObjectFactory::Current()->create(langname));
			objects.back()->setLocation(x,y);
			//printf("LangPatch::createObject BB <%s>\n", langname.c_str());
			if(addreceiver)	{
				//printf("receiver <%s>\n", objects.back()->getReceiverName().c_str());
				if(objects.back()->hasReceiverName())	{
					addObjectToReceiver(makeReceiverName(objects.back()->getReceiverName()), objects.back());
				}
			}
			return objects.back();
		} else {
			throw std::exception();
		}
	}

	void LangPatch::removeObject(Objsc::LangObjectPtr objPtr)
	{
		for(Objsc::LangObjectList::iterator obj = objects.begin(); obj != objects.end(); ++obj)	{
			(*obj)->unlinkAll(objPtr);
		}
		objPtr->cleanUp();
		removeObjectFromReceiver(makeReceiverName(objPtr->getLangName()), objPtr);
		objects.erase(find(objects.begin(), objects.end(), objPtr));
	}

	Objsc::LangObjectPtr LangPatch::parseStringToObject(string objstr)
	{
		Objsc::LangList objList;
		Objsc::tokenizeString(objstr, objList);
		string langname = getFirstAsString(&objList);
		//printf("parseStringToObject [%s]\n", langname.c_str());
		if(Objsc::LangObjectFactory::Current()->hasCreator(langname))	{
			Objsc::LangObjectPtr objPtr = Objsc::LangObjectFactory::Current()->create(langname);
			if(objList.size() > 1)	{
				Objsc::LangList argList;
				argList.copy(&objList,1);
				if(!objPtr->parseArgs(&argList))	{
					return Objsc::LangObjectPtr();
				}
			}
			return objPtr;
		} else {
			return Objsc::LangObjectPtr();
		}
	}

	void LangPatch::addObject(Objsc::LangObjectPtr objPtr, bool addreceiver)
	{
		//printf("LangPatch::addObject <%s> %d\n", objPtr->getLangName().c_str(), addreceiver);
		objects.push_back(objPtr);
		if(addreceiver)	{
			//printf("receiver <%s> %d\n", objPtr->getReceiverName().c_str(), objPtr->hasReceiverName());
			if(objPtr->hasReceiverName())	{
				addObjectToReceiver(makeReceiverName(objPtr->getReceiverName()), objects.back());
			}
		}
	}


	void LangPatch::addObjectToReceiver(const string& label, Objsc::LangObjectPtr objPtr)
	{
		//printf("LangPatch::addObjectToReceiver <%s>\n", label.c_str());
		if(receivers.count(label) == 0)	{
			receivers.insert(Objsc::LangReceiverEntry(label, Objsc::LangReceiver::create(label)));
		}
		receivers[label]->add(objPtr);
	}

	void LangPatch::removeObjectFromReceiver(const string& label, Objsc::LangObjectPtr objPtr)
	{
		if(receivers.count(label) > 0)	{
			receivers[label]->remove(objPtr);
			if(receivers.count(label) > 0)	{
				receivers.erase(label);
			}
		}
	}


	void LangPatch::receive(const string& label, Objsc::LangAtomPtr atom)
	{
		//printf("LangPatch::receive <%s> A %d\n", label.c_str(), atom.use_count());
		if(active)	{
			if(receivers.count(label) > 0)	{
				//printf("LangPatch::receive <%s> B\n", label.c_str());
				receivers[label]->receive(atom.get());
			}
		}
		//printf("LangPatch::receive <%s> DONE\n", label.c_str());
	}


	void LangPatch::start()
	{
		active=true;
		if(receivers.count("_patchon_") > 0)	{
			receivers["_patchon_"]->receive(new Objsc::LangBang());
		}
	}

	void LangPatch::stop()
	{
		active=false;
		if(receivers.count("_patchoff_") > 0)	{
			receivers["_patchoff_"]->receive(new Objsc::LangBang());
		}
	}

	void LangPatch::onload()
	{
		//printf("LangPatch::onload A\n");
		if(receivers.count("_loadbang_") > 0)	{
			//printf("LangPatch::onload B %d\n", receivers.count("_loadbang_"));
			receivers["_loadbang_"]->receive(new Objsc::LangBang());
		}
		//tmp for kaleidoscope
		if(receivers.count("_onload_") > 0)	{
			//printf("LangPatch::onload B %d\n", receivers.count("_onload_"));
			receivers["_onload_"]->receive(new Objsc::LangBang());
		}
	}

	void LangPatch::update(int timeframe)
	{
		if(active)	{
			for(Objsc::LangObjectList::iterator obj = objects.begin(); obj != objects.end(); ++obj)	{
				(*obj)->update(timeframe);
			}
		}
	}


}
