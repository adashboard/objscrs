#include "LineObject.hpp"
#include "../objscrGeom.hpp"
#include "../objscrMath.hpp"

namespace Objsc
{

	const string Objsc::LineObject::langname = "line";
	////////////////////////////////////////
	// LINE OBJECT
	////////////////////////////////////////
	LineObject::LineObject() :
		current(0.0f),
		begin(0.0f),
		end(0.0f),
		step(0.0f),
		interval(200),
		tframe(0.0f),
		count(0),
		active(false)
	{
		setInlets(4);
		setOutlets(2);
	}

	LineObject::~LineObject()
	{

	}

	bool LineObject::parseArgs(Objsc::LangList* args)
	{
		//~ if(args->size() > 1)	{
			//~ setInterval(getItemAsInt(args, 1));
		if(!args->empty())	{
			setInterval(getItemAsInt(args, 0));
			char cstr[64];
			sprintf(cstr, "%s %d", getLangName().c_str(), interval);
			displaystr.assign(cstr);
		}
		return true;
	}

	void LineObject::receive(std::string inlet, Objsc::LangAtom* atom)
	{
		switch(atom->getType())	{
			case LANG_BANG:
				break;
			case LANG_INT:
			if(inlet == "0")	{
						if(parseBool(atom))	{
							start();
						} else {
							stop();
						}
			}
			else if(inlet =="1")
						setInterval(parseInt(atom));

				break;
			case LANG_FLOAT:
			if(inlet == "0")	{
						active=true;
						setEndValue(parseFloat(atom));
			}
			else if(inlet == "1")
						setInterval(parseFloat(atom));
			else if(inlet == "2")
						setBeginValue(parseFloat(atom));
			else if(inlet == "3")
						setEndValue(parseFloat(atom));

				break;
			case LANG_LIST:
				receiveList(inlet, dynamic_cast<Objsc::LangList*>(atom));
				break;
		}
	}

	void LineObject::receiveList(std::string inlet, LangList* value)
	{
		if(inlet == "0") {
			string cmd = parseString(value->idx(0), "");
			if(cmd.empty())	{
				parseInt(value->idx(0), 0)>0?start():stop();
			} else {
				if(cmd == "start")	{
					start();
				} else if(cmd == "stop")	{
					stop();
				}
			}
		} else if(inlet == "1") {
			setInterval(parseInt(value->idx(0), 200));
		}
	}

	void LineObject::start()
	{
		active=true;
		reset();
	}

	void LineObject::setInterval(int value)
	{
		interval=MAX(0, value);
	}

	void LineObject::setBeginValue(int value)
	{
		begin = value;
		if(tframe > 0)	{
			step = (end-begin)/(interval/tframe);
		} else {
			step = 0.0f;
		}
	}

	void LineObject::setEndValue(int value)
	{
		end = value;
		if(tframe > 0)	{
			if(active)	{
				step = (end-current)/(interval/tframe);
			} else {
				step = (end-begin)/(interval/tframe);
			}
		} else {
			step = 0.0f;
		}
	}

	void LineObject::reset()
	{
		current = begin;
		if(tframe > 0)	{
			step = (end-begin)/(interval/tframe);
		} else {
			step = 0.0f;
		}
		count=0;
	}

	void LineObject::update(int timeframe)
	{
		tframe = timeframe;
		if(active)	{
			send(0, new Objsc::LangFloat(current));
			current += step;
			if(valueMatches(end,current,step))	{
				activate();
				stop();
				send(0, new Objsc::LangFloat(end));
			}
		}
	}

	void LineObject::activate()
	{
		send(1, new Objsc::LangBang());
	}


}
