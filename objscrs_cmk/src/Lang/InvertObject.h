/***************************************************************************
 *   Copyright (C) 2012 by Pierre Marchand   *
 *   pierre@oep-h.com   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#ifndef OBJSC_INVERTOBJECT_H
#define OBJSC_INVERTOBJECT_H

#include "MathObject.hpp"
namespace Objsc {
	
	class InvertObject : public MathObject
	{
		protected:
			void eval(float input);

		public:
			InvertObject()
			{
				setInlets(1);
				setOutlets(1);
			}
			~InvertObject() {}

			static const string langname;
			const string& getLangName() const { return Objsc::InvertObject::langname; }

			void setValue(float nvalue) {}
	};
	
	class InvertObjectCreator : public LangObjectCreator
	{
		public:
			Objsc::LangObjectPtr create() const { return Objsc::LangObjectPtr(new Objsc::InvertObject());}
	};

} // namespace Objsc

#endif // OBJSC_INVERTOBJECT_H
