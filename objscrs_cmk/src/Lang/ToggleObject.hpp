#ifndef OBJSCRS_TOGGLEOBJECT_HPP
#define OBJSCRS_TOGGLEOBJECT_HPP
#include "../objscrLangObjects.hpp"
namespace Objsc
{

	/////////////////////////////////////
	// TOGGLE OBJECT
	/////////////////////////////////////
	class ToggleObject : public LangObject
	{
		protected:
			Objsc::LangObserverPtr 		observer;
			bool					on;

			void receiveList(std::string inlet, LangList* value);

		public:
			ToggleObject() : observer(), on(false)
			{
				setInlets(1);
				setOutlets(1);
			}
			~ToggleObject() {}

			static const string langname;
			const string& getLangName() const { return Objsc::ToggleObject::langname; }
			const string& getDisplayString(){return Objsc::ToggleObject::langname;}

			void receive(std::string inlet, Objsc::LangAtom* atom);
			void activate()
			{
				on = !on;
				if(observer)
					observer->objActivated();
				send(0, new Objsc::LangInt((int)on));
			}

			void turnOn();
			void turnOff();

			const bool isOn() const { return on; }

			void setObserver(Objsc::LangObserverPtr obsPtr);
			void removeObserver();
	};


	class ToggleObjectCreator : public LangObjectCreator
	{
		public:
			Objsc::LangObjectPtr create() const { return Objsc::LangObjectPtr(new Objsc::ToggleObject());}
	};

}
#endif
