#ifndef OBJSCRS_LANGMEDIAOBJECT_HPP
#define OBJSCRS_LANGMEDIAOBJECT_HPP
#include "../objscrLangObjects.hpp"
namespace Objsc
{
	/////////////////////////////////////
	// MEDIA OBJECT
	/////////////////////////////////////
	class LangMediaObject : public LangObject
	{
		protected:
			string			label;

		public:
			LangMediaObject() : label() {}
			virtual ~LangMediaObject() {}
			bool parseArgs(Objsc::LangList* args);
			const string& getLabel() const { return label; }
	};

	typedef std::tr1::shared_ptr<Objsc::LangMediaObject> LangMediaObjectPtr;

}
#endif
