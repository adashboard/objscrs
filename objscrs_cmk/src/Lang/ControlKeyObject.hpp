#ifndef OBJSCRS_CONTROLKEYOBJECT_HPP
#define OBJSCRS_CONTROLKEYOBJECT_HPP

#include "../objscrLangObjects.hpp"
#include "ChangeObject.h"

namespace Objsc
{
	/////////////////////////////////////
	// CONTROLKEY OBJECT
	/////////////////////////////////////
	class ControlKeyObject : public LangObject
	{
		protected:
			string 			label;

		public:
			ControlKeyObject();
			~ControlKeyObject();

			static const string langname;
			const string& getLangName() const { return Objsc::ControlKeyObject::langname; }
			void setLabel(string nlabel);
			const string& getLabel() const { return label; }

			bool parseArgs(Objsc::LangList* args);

			void receive(std::string inlet, Objsc::LangAtom* atom);

			bool hasReceiverName() { return true; }
			const string& getReceiverName() const { return getLangName(); }
	};


	class ControlKeyObjectCreator : public LangObjectCreator
	{
		public:
			Objsc::LangObjectPtr create() const { return Objsc::LangObjectPtr(new Objsc::ControlKeyObject());}
	};


}
#endif
