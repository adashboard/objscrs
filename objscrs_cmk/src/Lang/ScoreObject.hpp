#ifndef OBJSCRS_SCOREOBJECT_HPP
#define OBJSCRS_SCOREOBJECT_HPP
#include "../objscrLangObjects.hpp"
namespace Objsc
{

	/////////////////////////////////////
	// SCORE OBJECT
	/////////////////////////////////////
	class ScoreObject : public LangObject
	{
		protected:
			void loadScore(const string& scorename);
			void receiveList(std::string inlet, LangList* value);

		public:
			ScoreObject()
			{
				setInlets(1);
				setOutlets(0);
			}
			~ScoreObject() {}

			static const string langname;
			const string& getLangName() const { return Objsc::ScoreObject::langname; }


			const string& getDisplayString()
			{
				return Objsc::ScoreObject::langname;
			}

			void receive(std::string inlet, Objsc::LangAtom* atom);
	};

	class ScoreObjectCreator : public LangObjectCreator
	{
		public:
			Objsc::LangObjectPtr create() const { return Objsc::LangObjectPtr(new Objsc::ScoreObject());}
	};


}
#endif
