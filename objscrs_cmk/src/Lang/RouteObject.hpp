#ifndef OBJSCRS_ROUTEOBJECT_HPP
#define OBJSCRS_ROUTEOBJECT_HPP
#include "../objscrLangObjects.hpp"
namespace Objsc
{
	/////////////////////////////////////
	// ROUTE OBJECT
	/////////////////////////////////////
	class RouteObject : public LangObject
	{
		protected:
			Objsc::LangList	keys;
			string			label;

			void receiveList(std::string inlet, Objsc::LangList* atom);

		public:
			RouteObject() : keys(), label()
			{
				setInlets(1);
				setOutlets(0);
			}
			~RouteObject() {}

			static const string langname;
			const string& getLangName() const { return Objsc::RouteObject::langname; }
			const string& getDisplayString(){return displaystr;}

			void setLabel(string nlabel) {label.assign(nlabel);}
			const string& getLabel() const { return label; }

			bool parseArgs(Objsc::LangList* args);
			void receive(std::string inlet, Objsc::LangAtom* atom);

			const bool hasArgs() const { return true;}
			Objsc::LangList* getArgs() { return &keys; }
			string getArgString() { return listToString(&keys); }
	};


	class RouteObjectCreator : public LangObjectCreator
	{
		public:
			Objsc::LangObjectPtr create() const { return Objsc::LangObjectPtr(new Objsc::RouteObject());}
	};

}
#endif
