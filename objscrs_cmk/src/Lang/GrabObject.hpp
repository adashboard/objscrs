#ifndef OBJSCRS_GRABOBJECT_HPP
#define OBJSCRS_GRABOBJECT_HPP

#include "../objscrLangObjects.hpp"
#include "../objscrFrames.hpp"

#include "ofMain.h"

namespace Objsc
{
	/////////////////////////////////////
	// GRAB OBJECT
	/////////////////////////////////////
	class GrabObject : public LangObject
	{
		protected:
			string				label;
			ofImage 				img;
			unsigned char		alpha;
			bool				screen;
			Objsc::FrameSourcePtr	frameSource;

			void grabScreen();
			void grabCamera();
			void grab();
			void receiveList(std::string inlet, LangList* value);
			void setAlpha(int value);

		public:
			GrabObject();
			~GrabObject() {}
			bool parseArgs(Objsc::LangList* args);
			const string& getLabel() const { return label; }

			static const string langname;
			const string& getLangName() const { return Objsc::GrabObject::langname; }

			const string& getDisplayString()
			{
				if(displaystr.empty())	{
					return Objsc::GrabObject::langname;
				}
				return displaystr;
			}

			void setLabel(string nlabel);

			void receive(std::string inlet, Objsc::LangAtom* atom);
			void activate();
	};

	class GrabObjectCreator : public LangObjectCreator
	{
		public:
			Objsc::LangObjectPtr create() const { return Objsc::LangObjectPtr(new Objsc::GrabObject());}
	};
}
#endif
