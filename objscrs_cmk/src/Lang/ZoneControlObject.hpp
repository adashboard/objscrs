#ifndef OBJSCRS_ZONECONTROLOBJECT_HPP
#define OBJSCRS_ZONECONTROLOBJECT_HPP

#include "../objscrLangObjects.hpp"
#include "../objscrZones.hpp"

namespace Objsc
{

	/////////////////////////////////////
	// ZONE CONTROL OBJECT
	/////////////////////////////////////
	class ZoneControlObject : public LangObject
	{
		protected:
			Objsc::ZonePtr	zone;
			string			label;

			void receiveList(std::string inlet, LangList* value);

			void setGeom(float x, float y, float w, float h);
			void sendGeom();
			void setLoc(float x, float y);
			void sendLoc();
			void setGrid(int x, int y);
			void sendGrid();
			void sendCells();
			void setBodyLabel(int index, const string &label);

		public:
			ZoneControlObject() : zone(), label()
			{
				setInlets(1);
				setOutlets(1);
				min_args = 1;
				max_args = 1;
			}
			~ZoneControlObject() {}

			static const string langname;
			const string& getLangName() const { return Objsc::ZoneControlObject::langname; }

			const string& getDisplayString();
			void setLabel(string nlabel);
			const string& getLabel() const { return label; }

			bool parseArgs(Objsc::LangList* args);
			const bool hasArgs() const { return true;}
			//Objsc::LangList* getArgs() { return label; }
			string getArgString() { return label; }

			void receive(std::string inlet, Objsc::LangAtom* atom);
			void activate();
	};


	class ZoneControlObjectCreator : public LangObjectCreator
	{
		public:
			Objsc::LangObjectPtr create() const { return Objsc::LangObjectPtr(new Objsc::ZoneControlObject());}
	};


}
#endif
