/***************************************************************************
 *   Copyright (C) 2012 by Pierre Marchand   *
 *   pierre@oep-h.com   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#include "TextObject.h"

#include "../objscrMediaPlayer.hpp"
#include "../objscrMediaViews.hpp"

namespace Objsc {
	const string Objsc::TextObject::langname = "txt";
	
	TextObject::~TextObject()
	{

	}

	void TextObject::setLabel(string nlabel)
	{
		label.assign(nlabel);
		displaystr = Objsc::TextObject::langname + " " + label;
		Objsc::MediaPlayer::Current()->createText(label);
	}

	void TextObject::setTextColour(int r, int g, int b)
	{
		Objsc::MediaViewPtr media = Objsc::MediaPlayer::Current()->getMedia(label);
		if(media)	{
			media->setTextColour(r,g,b);
		}
	}

	void TextObject::setBackColour(int r, int g, int b)
	{
		Objsc::MediaViewPtr media = Objsc::MediaPlayer::Current()->getMedia(label);
		if(media)	{
			media->setBackColour(r,g,b);
		}
	}

	void TextObject::receive(std::string inlet, Objsc::LangAtom* atom)
	{
		switch(atom->getType())	{
			case LANG_NULL:
				break;
			case LANG_BANG:
			if(inlet == "0")	{
					showMedia();
				}
				break;
			case LANG_INT:
			if(inlet == "0") {
						if(parseBool(atom))	{
							showMedia();
						} else {
							hideMedia();
						}
			}
			else if(inlet == "1")
						locXMedia(parseFloat(atom, 0.0f));
			else if(inlet == "2")
						locYMedia(parseFloat(atom, 0.0f));
			else if(inlet == "3")
						setText(intToString(parseInt(atom, 0.0f)));

				break;
			case LANG_FLOAT:
			if(inlet == "0") {
						if(parseBool(atom))	{
							showMedia();
						} else {
							hideMedia();
						}
			}
			else if(inlet == "1")
						locXMedia(parseFloat(atom, 0.0f));
			else if(inlet == "2")
						locYMedia(parseFloat(atom, 0.0f));
			else if(inlet == "3")
						setText(floatToString(parseInt(atom, 0.0f)));

				break;
			case LANG_STRING:
			if(inlet == "3") {
						setText(parseString(atom, ""));

				}
				break;
			case LANG_VAR:
				break;
			case LANG_LIST:
			if(inlet == "0")
						receiveList("0", dynamic_cast<Objsc::LangList*>(atom));
			else if(inlet == "1")
						locXMedia(parseFloat(dynamic_cast<Objsc::LangList*>(atom)->idx(0), 0.0f));
			else if(inlet == "2")
						locYMedia(parseFloat(dynamic_cast<Objsc::LangList*>(atom)->idx(0), 0.0f));
			else if(inlet == "3")
						setText(dynamic_cast<Objsc::LangList*>(atom));

				break;
		}
	}

	void TextObject::setText(LangList* value)
	{
		Objsc::MediaViewPtr media = Objsc::MediaPlayer::Current()->getMedia(label);
		if(media)	{
			media->setText(listToString(value));
			activate();
		}
	}

	void TextObject::setText(const string& value)
	{
		Objsc::MediaViewPtr media = Objsc::MediaPlayer::Current()->getMedia(label);
		if(media)	{
			media->setText(value);
			activate();
		}
	}

	void TextObject::clearText()
	{
		Objsc::MediaViewPtr media = Objsc::MediaPlayer::Current()->getMedia(label);
		if(media)	{
			media->clearText();
			activate();
		}
	}

	void TextObject::receiveList(std::string inlet, LangList* value)
	{

		string cmd = parseString(value->idx(0), "");
		if(cmd.empty()) {
			if(parseInt(value->idx(0), -1) == 1)	{
				showMedia();
			} else if(parseInt(value->idx(0), -1) == 0)	{
				hideMedia();
			}
		} else {
			if(cmd == "clear")	{
				clearText();
			} else if(cmd == "show")	{
				showMedia();
			} else if(cmd == "hide")	{
				hideMedia();
				//printf("============= HIDE %d\n", playing);
			} else if(cmd == "centre")	{
				centreMedia();
			//~ } else if(cmd == "size")	{
				//~ if(value->size() > 2)	{
					//~ sizeMedia(parseFloat(value->idx(1), -1), parseFloat(value->idx(2), -1));
				//~ }
			} else if(cmd == "loc")	{
				if(value->size() > 2)	{
					locMedia(parseFloat(value->idx(1), 0), parseFloat(value->idx(2), 0));
				}
			//~ } else if(cmd == "geom")	{
				//~ if(value->size() > 4)	{
					//~ geomMedia(parseFloat(value->idx(1), 0), parseFloat(value->idx(2), 0),parseFloat(value->idx(3), -1), parseFloat(value->idx(4), -1));
				//~ }
			} else if(cmd == "layer")	{
				if(value->size() > 1)	{
					setLayerMedia(parseInt(value->idx(1), 0));
				}
			} else if(cmd == "remove")	{
				removeMedia();
			} else if(cmd == "rgb")	{
				if(value->size() > 3)	{
					setTextColour(parseInt(value->idx(1), 255), parseInt(value->idx(2), 255), parseInt(value->idx(3), 255));
				}
			} else if(cmd == "bg")	{
				if(value->size() > 3)	{
					setBackColour(parseInt(value->idx(1), 0), parseInt(value->idx(2), 0), parseInt(value->idx(3), 0));
				}
			} else if(cmd == "bg_on")	{
				Objsc::MediaViewPtr media = Objsc::MediaPlayer::Current()->getMedia(label);
				if(media)	{
					media->showBG();
				}
			} else if(cmd == "bg_off")	{
				Objsc::MediaViewPtr media = Objsc::MediaPlayer::Current()->getMedia(label);
				if(media)	{
					media->hideBG();
				}
			} else if(cmd == "size")	{
				if(value->size() > 1)	{
					Objsc::MediaViewPtr media = Objsc::MediaPlayer::Current()->getMedia(label);
					if(media)	{
						media->setTextSize(parseInt(value->idx(1), 24));
					}
				}
			}
		}
	}


	void TextObject::activate()
	{
		send(0, new Objsc::LangBang());
	}


	void TextObject::update(int timeElapsed)
	{

	}

	void TextObject::locMedia(float x, float y)
	{
		Objsc::MediaViewPtr media = Objsc::MediaPlayer::Current()->getMedia(label);
		if(media)	{
			media->setX(x);
			media->setY(y);
		}
	}

	void TextObject::locXMedia(float x)
	{
		Objsc::MediaViewPtr media = Objsc::MediaPlayer::Current()->getMedia(label);
		if(media)	{
			media->setX(x);
		}
	}

	void TextObject::locYMedia(float y)
	{
		Objsc::MediaViewPtr media = Objsc::MediaPlayer::Current()->getMedia(label);
		if(media)	{
			media->setY(y);
		}
	}

	void TextObject::setLayerMedia(int layer)
	{
		Objsc::MediaPlayer::Current()->setLayer(label, layer);
	}

	
} // namespace Objsc
