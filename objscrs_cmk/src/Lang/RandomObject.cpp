#include "RandomObject.hpp"
#include "ofMain.h"

namespace Objsc
{
	const string Objsc::RandomObject::langname = "rnd";

	/////////////////////////////////////
	// RANDOM OBJECT
	/////////////////////////////////////
	RandomObject::RandomObject() :
			lower(0.0f),
			upper(1.0f)
	{
		setInlets(2);
		setOutlets(1);
	}

	void RandomObject::receive(std::string inlet, Objsc::LangAtom* atom)
	{
		switch(atom->getType())	{
			case LANG_NULL:
				break;
			case LANG_BANG:
			if(inlet == "0")	{
					activate();
				}
				break;
			case LANG_INT:
			if(inlet == "0")	{
					lower = 0.0f;
					upper = (float)parseInt(atom);
					activate();
			} else if(inlet == "1")	{
					lower = 0.0f;
					upper = (float)parseInt(atom);
				}
				break;
			case LANG_FLOAT:
			if(inlet == "0")	{
					lower = 0.0f;
					upper = parseFloat(atom);
					activate();
			}else if(inlet == "1")	{
					lower = 0.0f;
					upper = parseFloat(atom);
				}
				break;
			case LANG_STRING:
				break;
			case LANG_VAR:
				break;
			case LANG_LIST:
				receiveList(inlet, dynamic_cast<Objsc::LangList*>(atom));
				break;
		}
	}

	void RandomObject::receiveList(std::string inlet, LangList* value)
	{
		if(value->size() == 1)	{
			lower = 0.0f;
			upper = parseFloat(value->idx(0), 1.0f);
		} else if(value->size() > 1)	{
			lower = parseFloat(value->idx(0), 0.0f);
			upper = parseFloat(value->idx(1), 1.0f);
		}
		if(inlet == "0")	{
			activate();
		}
	}

	void RandomObject::activate()
	{
		send(0, new Objsc::LangFloat(ofRandom(lower, upper)));
	}

}
