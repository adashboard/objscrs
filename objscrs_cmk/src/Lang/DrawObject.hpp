#ifndef OBJSCRS_DRAWOBJECT_HPP
#define OBJSCRS_DRAWOBJECT_HPP

#include "../objscrLangObjects.hpp"
#include "../objscrMediaViews.hpp"

namespace Objsc
{
	/////////////////////////////////////
	// LINE OBJECT
	/////////////////////////////////////
	class DrawObject : public LangObject
	{
		protected:
			string				label;
			Objsc::DrawViewPtr		graphic;

			virtual void receiveList(std::string inlet, LangList* value);
			void setLayerMedia(int layer);

		public:
			DrawObject() : label(), graphic()
			{
				setInlets(2);
				setOutlets(0);
			}
			virtual ~DrawObject() {}

			bool parseArgs(Objsc::LangList* args);
			void setLabel(string nlabel);
			const string& getLabel() const { return label; }

			virtual void receive(std::string inlet, Objsc::LangAtom* atom);
			virtual void activate();

			static const string langname;
			const string& getLangName() const { return Objsc::DrawObject::langname; }
	};

	class DrawObjectCreator : public LangObjectCreator
	{
		public:
			Objsc::LangObjectPtr create() const { return Objsc::LangObjectPtr(new Objsc::DrawObject());}
	};
}

#endif
