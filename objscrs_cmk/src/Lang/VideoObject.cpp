/***************************************************************************
 *   Copyright (C) 2012 by Pierre Marchand   *
 *   pierre@oep-h.com   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#include "VideoObject.h"

#include "../objscrMediaPlayer.hpp"
#include "../objscrMediaViews.hpp"

namespace Objsc {

	const string Objsc::VideoObject::langname = "vid";

	VideoObject::~VideoObject()
	{

	}

	void VideoObject::update(int timeElapsed)
	{
		if(media)	{
			//printf("VideoObject::update B %d\n", playing);
			if(playing) {
				//media->update();
				timeplayed += timeElapsed*0.001;
				//~ printf("timeplayed %f, duration: %f\n", timeplayed,duration);
				//~ if(timeplayed >= duration)
					//~ mediaEnded();
				if(media->isFinished())
					mediaEnded();
				send(0, new Objsc::LangInt(media->getCurrentFrame()));
			}
		}
	}

	void VideoObject::mediaEnded()
	{
		stopMedia();
		send(1, new Objsc::LangBang());
	}

	void VideoObject::setLabel(string nlabel)
	{
		label.assign(nlabel);
		displaystr = Objsc::VideoObject::langname + " " + label;
		//printf("VideoObject::setLabel <%s>\n", label.c_str());
	}

	void VideoObject::receive(std::string inlet, Objsc::LangAtom* atom)
	{
		switch(atom->getType())	{
			case LANG_NULL:
				break;
			case LANG_BANG:
			if(inlet == "0")	{
					showMedia();
					playMedia();
				}
				break;
			case LANG_INT:
			if(inlet == "0") {
						if(parseBool(atom))	{
							showMedia();
							playMedia();
						} else {
							stopMedia();
							hideMedia();
						}
			}
			else if(inlet =="1")
						locXMedia(parseFloat(atom, 0.0f));
			else if(inlet =="2")
						locYMedia(parseFloat(atom, 0.0f));
			else if(inlet =="3")
						widthMedia(parseFloat(atom, -1.0f));
			else if(inlet =="4")
						heightMedia(parseFloat(atom, -1.0f));

				break;
			case LANG_FLOAT:
			if(inlet == "0") {
						if(parseBool(atom))	{
							showMedia();
							playMedia();
						} else {
							stopMedia();
							hideMedia();
						}
			}
			else if(inlet == "1")
						locXMedia(parseFloat(atom, 0.0f));
			else if(inlet == "2")
						locYMedia(parseFloat(atom, 0.0f));
			else if(inlet == "3")
						widthMedia(parseFloat(atom, -1.0f));
			else if(inlet == "4")
						heightMedia(parseFloat(atom, -1.0f));
			else if(inlet == "5")
						setSpeed(parseFloat(atom, 1.0f));

				break;
			case LANG_STRING:
				break;
			case LANG_VAR:
				break;
			case LANG_LIST:
			if(inlet == "0")
				receiveList("0", dynamic_cast<Objsc::LangList*>(atom));
			else if(inlet == "1")
						locXMedia(parseFloat(dynamic_cast<Objsc::LangList*>(atom)->idx(0), 0.0f));
			else if(inlet == "2")
						locYMedia(parseFloat(dynamic_cast<Objsc::LangList*>(atom)->idx(0), 0.0f));
			else if(inlet == "3")
						widthMedia(parseFloat(dynamic_cast<Objsc::LangList*>(atom)->idx(0), -1.0f));
			else if(inlet == "4")
						heightMedia(parseFloat(dynamic_cast<Objsc::LangList*>(atom)->idx(0), -1.0f));

				break;
		}
	}

	void VideoObject::receiveList(std::string inlet, LangList* value)
	{

		string cmd = parseString(value->idx(0), "");
		if(cmd.empty()) {
			if(parseInt(value->idx(0), -1) == 1)	{
				showMedia();
			} else if(parseInt(value->idx(0), -1) == 0)	{
				hideMedia();
			}
		} else {
			if(cmd == "load")	{
				if(value->size() == 2)	{
					loadMedia(parseString(value->idx(1), ""),0,false);
				} else if(value->size() == 3)	{
					loadMedia(parseString(value->idx(1), ""),parseInt(value->idx(2), 0),false);
				} else if(value->size() == 4)	{
					loadMedia(parseString(value->idx(1), ""),parseInt(value->idx(2), 0),parseBool(value->idx(3), false));
					if(parseBool(value->idx(3), false))	{
						playMedia();
					}
				} else if(value->size() == 5)	{
					loadMedia(parseString(value->idx(1), ""),parseInt(value->idx(2), 0),parseBool(value->idx(3), false));
					if(parseBool(value->idx(3), false))	{
						playMedia(parseFloat(value->idx(4), 0.0f));
					}
				} else if(value->size() > 5)	{
					loadMedia(parseString(value->idx(1), ""),parseInt(value->idx(2), 0),parseBool(value->idx(3), false));
					if(parseBool(value->idx(3), false))	{
						playMedia(parseFloat(value->idx(4), 0.0f),parseFloat(value->idx(5), 0.0f));
					}
				}
			} else if(cmd == "show")	{
				if(value->size() == 2)	{
					showMedia(parseInt(value->idx(1), 0));
					playMedia();
				} else {
					playMedia();
					showMedia();
				}
			} else if(cmd == "hide")	{
				stopMedia();
				playing = false;
				timeplayed = 0.0f;
				hideMedia();
				//printf("============= HIDE %d\n", playing);
			} else if(cmd == "play")	{
				if(value->size() > 2)	{
					playMedia(parseFloat(value->idx(1), 0.0f),parseFloat(value->idx(2), 0.0f));
				} else if(value->size() == 2)	{
					playMedia(parseFloat(value->idx(1), 0.0f));
				} else {
					playMedia();
				}
			} else if(cmd == "stop")	{
				stopMedia();
			} else if(cmd == "fullscreen")	{
				fullscreenMedia();
			} else if(cmd == "centre")	{
				centreMedia();
			} else if(cmd == "size")	{
				if(value->size() > 2)	{
					sizeMedia(parseFloat(value->idx(1), -1), parseFloat(value->idx(2), -1));
				}
			} else if(cmd == "loc")	{
				if(value->size() > 2)	{
					locMedia(parseFloat(value->idx(1), 0), parseFloat(value->idx(2), 0));
				}
			} else if(cmd == "geom")	{
				if(value->size() > 4)	{
					geomMedia(parseFloat(value->idx(1), 0), parseFloat(value->idx(2), 0),parseFloat(value->idx(3), -1), parseFloat(value->idx(4), -1));
				}
			} else if(cmd == "layer")	{
				if(value->size() > 1)	{
					setLayerMedia(parseInt(value->idx(1), 0));
				}
			} else if(cmd == "speed")	{
				if(value->size() == 2)	{
					setSpeed(parseFloat(value->idx(1), 1.0f));
				}
			} else if(cmd == "vol")	{
				if(value->size() == 2)	{
					setVolume(parseFloat(value->idx(1), 0.0f));
				}
			} else if(cmd == "remove")	{
				removeMedia();
			}
		}
	}

	void VideoObject::loadMedia(const string& filename, int layer, bool show)
	{
		if(!filename.empty() && !label.empty())	{
			//media = Objsc::MediaPlayer::Current()->loadVideo(filename, label);
			media = std::tr1::static_pointer_cast<Objsc::VideoView>(Objsc::MediaPlayer::Current()->loadVideo(filename, label));
			if(layer != 0)	{
				if(show)	{
					Objsc::MediaPlayer::Current()->show(label, layer);
				} else {
					Objsc::MediaPlayer::Current()->setLayer(label, layer);
				}
			}
			duration = media->getDuration();
		}
	}

	void VideoObject::playMedia(float vol, float speed)
	{
		//media = Objsc::MediaPlayer::Current()->getMedia(label);
		//printf("VideoObject::playMedia vol: %f speed: %f\n", vol, speed);
		if(media)	{
			//printf("VideoObject::playMedia B\n");
			media->start();
			//printf("VideoObject::playMedia C\n");
			if(vol !=0.0f)
				media->setVolume(vol);
			//~ if(speed !=0.0f)
				//~ media->setSpeed(speed);
			playing = true;
			timeplayed = 0.0f;
			send(2, new Objsc::LangInt(1));
		}
		//printf("VideoObject::playMedia DONE\n");
	}

	void VideoObject::stopMedia()
	{
		//Objsc::MediaViewPtr media = Objsc::MediaPlayer::Current()->getMedia(label);
		if(media)	{
			media->stop();
			playing = false;
			timeplayed = 0.0f;
		}
		send(2, new Objsc::LangInt(0));
	}

	void VideoObject::showMedia(int layer)
	{
		if(layer != 0)	{
			Objsc::MediaPlayer::Current()->show(label, layer);
		} else {
			Objsc::MediaPlayer::Current()->show(label);
		}
		//send(0, new Objsc::LangBang());
		send(2, new Objsc::LangInt(1));
	}

	void VideoObject::hideMedia()
	{
		if(media)	{
			media->hide();
			media->stop();
			playing = false;
			timeplayed = 0.0f;
		}
	}

	void VideoObject::setVolume(float vol)
	{
		//Objsc::MediaViewPtr media = Objsc::MediaPlayer::Current()->getMedia(label);
		if(media)	{
			media->setVolume(vol);
		}
	}

	void VideoObject::setSpeed(float speed)
	{
		//Objsc::MediaViewPtr media = Objsc::MediaPlayer::Current()->getMedia(label);
		if(media)	{
			media->setSpeed(speed);
		}
	}

	void VideoObject::activate()
	{
		playMedia();
	}


	
} // namespace Objsc
