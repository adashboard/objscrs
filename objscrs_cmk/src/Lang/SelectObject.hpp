#ifndef OBJSCRS_SELECTOBJECT_HPP
#define OBJSCRS_SELECTOBJECT_HPP
#include "../objscrLangObjects.hpp"
namespace Objsc
{

	/////////////////////////////////////
	// SELECT OBJECT
	/////////////////////////////////////
	class SelectObject : public LangObject
	{
		protected:
			Objsc::LangList		keys;
			string			label;

			void select(Objsc::LangAtom* atom);

		public:
			SelectObject() : keys(), label()
			{
				setInlets(1);
				setOutlets(1);
			}
			~SelectObject() {}

			static const string langname;
			const string& getLangName() const { return Objsc::SelectObject::langname; }
			const string& getDisplayString(){return displaystr;}

			void setLabel(string nlabel) {label.assign(nlabel);}
			const string& getLabel() const { return label; }

			bool parseArgs(Objsc::LangList* args);
			void receive(std::string inlet, Objsc::LangAtom* atom);

			const bool hasArgs() const { return true;}
			Objsc::LangList* getArgs() { return &keys; }
			string getArgString() { return listToString(&keys); }
	};


	class SelectObjectCreator : public LangObjectCreator
	{
		protected:
			string displaystr;

		public:
			SelectObjectCreator(const string& str="") : displaystr(str) {}
			Objsc::LangObjectPtr create() const
			{
				Objsc::LangObjectPtr ptr = Objsc::LangObjectPtr(new Objsc::SelectObject());
				if(!displaystr.empty())	{
					ptr->setLabel(displaystr);
				}
				return ptr;
			}
	};
}
#endif
