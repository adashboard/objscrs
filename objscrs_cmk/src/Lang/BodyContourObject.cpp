#include "BodyContourObject.hpp"
#include "../objscrStage.hpp"

namespace Objsc
{
	const string Objsc::BodyContourObject::langname = "body_contour";


	/////////////////////////////////////
	// BODY CONTOUR OBJECT
	/////////////////////////////////////
	void BodyContourObject::receive(std::string inlet, Objsc::LangAtom* atom)
	{
		if(checkInlet(inlet))
		{
			//std::cout << "handling ..." << std::endl;
			switch(atom->getType())	{
				case LANG_BANG:
				if(inlet == "0")	{
						activate();
					}
					break;
				case LANG_LIST:
					receiveBody(inlet, dynamic_cast<LangList*>(atom));
					break;
			}
		}
	}

	void BodyContourObject::receiveBody(const std::string& inlet, Objsc::LangList* nbody)
	{
		id = parseInt(nbody->idx(2));
		if(inlet == "0")	{
			activate();
		}
	}

	void BodyContourObject::activate()
	{
		if(id > -1)	{
			Objsc::BodyPtr body = Objsc::Stage::Current()->getBody(id);
			if(body)	{
				Objsc::LangList* atom = new Objsc::LangList();
				Objsc::PointVec points = body->getStageBodyPoints();
				for(Objsc::PointVec::iterator point = points.begin(); point != points.end(); ++point)	{
					atom->add((*point).x);
					atom->add((*point).y);
				}
				send(0, atom);
			}
		}
	}

}
