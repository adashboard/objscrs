#ifndef OBJSCRS_SOUNDOBJECT_HPP
#define OBJSCRS_SOUNDOBJECT_HPP

#include "../objscrLangObjects.hpp"
#include "../objscrSound.hpp"

// openframeworks
#include "ofMain.h"

namespace Objsc
{

	/////////////////////////////////////
	// SOUND OBJECT
	/////////////////////////////////////
	class SoundObject : public LangObject, public SoundListener
	{
		protected:
			string					label;
			ofSoundPlayerGST			sound;

			void receiveList(std::string inlet, LangList* value);
			//void getAmp();

		public:
			SoundObject();
			~SoundObject();

			static const string langname;
			const string& getLangName() const { return Objsc::SoundObject::langname; }

			bool parseArgs(Objsc::LangList* args);
			void setLabel(string nlabel);
			const string& getLabel() const { return label; }

			void receive(std::string inlet, Objsc::LangAtom* atom);

			void update(int timeElapsed);

			void activate();

			void start();
			void stop();

			void receiveSoundEvent(SoundEvent& event);
	};

	class SoundObjectCreator : public LangObjectCreator
	{
		public:
			Objsc::LangObjectPtr create() const { return Objsc::LangObjectPtr(new Objsc::SoundObject());}
	};

}
#endif
