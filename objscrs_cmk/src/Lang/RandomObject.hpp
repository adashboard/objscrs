#ifndef OBJSCRS_RANDOMOBJECT_HPP
#define OBJSCRS_RANDOMOBJECT_HPP
#include "../objscrLangObjects.hpp"
namespace Objsc
{
	/////////////////////////////////////
	// RANDOM OBJECT
	/////////////////////////////////////
	class RandomObject : public LangObject
	{
		protected:
			float lower;
			float upper;

			void receiveList(std::string inlet, LangList* value);

		public:
			RandomObject();
			~RandomObject() {}

			static const string langname;
			const string& getLangName() const { return Objsc::RandomObject::langname; }

			void receive(std::string inlet, Objsc::LangAtom* atom);
			void activate();
	};

	class RandomObjectCreator : public LangObjectCreator
	{
		public:
			Objsc::LangObjectPtr create() const { return Objsc::LangObjectPtr(new Objsc::RandomObject());}
	};

}
#endif
