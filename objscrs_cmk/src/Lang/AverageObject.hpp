#ifndef OBJSCRS_AVERAGEOBJECT_HPP
#define OBJSCRS_AVERAGEOBJECT_HPP
#include "../objscrLangObjects.hpp"
namespace Objsc
{
	/////////////////////////////////////
	// AVERAGE OBJECT
	/////////////////////////////////////
	class AverageObject : public LangObject
	{
		protected:
			float sum;
			float avg;
			float count;

			void receiveList(std::string inlet, LangList* value);

		public:
			AverageObject();
			~AverageObject() {}

			static const string langname;
			const string& getLangName() const { return Objsc::AverageObject::langname; }

			void receive(std::string inlet, Objsc::LangAtom* atom);
			void activate();
			void reset();
			void add(float value);
	};

	class AverageObjectCreator : public LangObjectCreator
	{
		public:
			Objsc::LangObjectPtr create() const { return Objsc::LangObjectPtr(new Objsc::AverageObject());}
	};

}
#endif
