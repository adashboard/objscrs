#include "RouteObject.hpp"
namespace Objsc
{
	const string Objsc::RouteObject::langname = "route";

	/////////////////////////////////////
	// ROUTE OBJECT
	/////////////////////////////////////
	bool RouteObject::parseArgs(Objsc::LangList* args)
	{
		//printf("RouteObject::parseArgs START %d\n", args->size());
		if(args->size() < min_args)	{
			return false;
		} else {
			//keys.copy(args, 1);
			keys.copy(args);
			if(label.empty())	{
				//printf("RouteObject::parseArgs <%s> A\n", displaystr.c_str());
				displaystr.assign(getLangName().c_str());
				displaystr.append(" ");
				displaystr.append(listToString(&keys));
			} else {
				displaystr.assign(label.c_str());
				displaystr.append(" ");
				displaystr.append(listToString(&keys));
			}
			//printf("RouteObject::parseArgs <%s>\n", displaystr.c_str());
			setOutlets(keys.size()+1);
			//printf("RouteObject::parseArgs DONE %d\n", keys.size());
			return true;
		}
	}

	void RouteObject::receive(std::string inlet, Objsc::LangAtom* atom)
	{
		switch(atom->getType())	{
			case LANG_NULL:
				break;
			case LANG_BANG:
				break;
			case LANG_INT:
				break;
			case LANG_FLOAT:
				break;
			case LANG_STRING:
				break;
			case LANG_VAR:
				break;
			case LANG_LIST:
				receiveList(inlet, dynamic_cast<Objsc::LangList*>(atom));
				break;
		}
	}

	void RouteObject::receiveList(std::string inlet, Objsc::LangList* atom)
	{
		bool nomatch;
		if(atom->size() > 1)	{
			nomatch = true;
			for(int i = 0; i < num_outlets-1; ++i)	{
				//printf("types: %d %d\n", atom->idx(0)->getType(), keys.idx(i)->getType());
				if(equalsAtom(atom->idx(0), keys.idx(i)))	{
					nomatch = false;
					send(i, cloneList(atom,1));
					break;
				}
			}
			if(nomatch)	{
				send(num_outlets-1, atom);
			}
		}
	}

}
