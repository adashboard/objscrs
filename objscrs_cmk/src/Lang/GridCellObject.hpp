#ifndef OBJSCRS_GRIDCELLOBJECT_HPP
#define OBJSCRS_GRIDCELLOBJECT_HPP
#include "../objscrLangObjects.hpp"
namespace Objsc
{

	/////////////////////////////////////
	// GRID CELL OBJECT
	/////////////////////////////////////
	class GridCellObject : public LangObject
	{
		protected:
			int				count;
			int				col;
			int				row;
			int				idx;
			string			label;

			void receiveList(std::string inlet, LangList* value);

		public:
			GridCellObject() : count(0), col(0), row(0), idx(0), label()
			{
				setInlets(2);
				setOutlets(2);
				min_args = 2;
				max_args = 2;
			}
			~GridCellObject() {}

			static const string langname;
			const string& getLangName() const { return Objsc::GridCellObject::langname; }

			const string& getDisplayString();
			void setLabel(string nlabel);
			const string& getLabel() const { return label; }

			bool parseArgs(Objsc::LangList* args);
			const bool hasArgs() const { return true;}
			//Objsc::LangList* getArgs() { return label; }
			string getArgString() { return label; }

			void receive(std::string inlet, Objsc::LangAtom* atom);
			void activate();
	};


	class GridCellObjectCreator : public LangObjectCreator
	{
		public:
			Objsc::LangObjectPtr create() const { return Objsc::LangObjectPtr(new Objsc::GridCellObject());}
	};

}
#endif
