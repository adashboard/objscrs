/***************************************************************************
 *   Copyright (C) 2012 by Pierre Marchand   *
 *   pierre@oep-h.com   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#ifndef OBJSC_ANIMOBJECT_H
#define OBJSC_ANIMOBJECT_H

#include "ImageObject.h"

#include "../objscrFiles.hpp"

namespace Objsc {
	
	class AnimObject : public ImageObject
	{
		private:
			Objsc::FileResource		finder;
			Objsc::StringVector		frames;
			string				basename;
			int 					totalFrames;
			int 					currentFrame;
			bool				fs;
			float				x;
			float				y;
			int					lyr;

		protected:
			void nextFrame();
			void prevFrame();
			void firstFrame();
			void lastFrame();
			void gotoFrame(int frameNum);
			int loadAnimFiles(const string& sBasename);
			int reloadAnimFiles();
			void loadMedia(const string& filename, int layer, bool show);
			void receiveList(std::string inlet, LangList* value);

		public:
			AnimObject() :
				finder(),
				frames(),
				basename(),
				totalFrames(0),
				currentFrame(0),
				fs(false),
				x(0.0f),
				y(0.0f),
				lyr(0)
			{
				setInlets(5);
				setOutlets(3);
				finder.includeFileType("png");
				finder.includeFileType("jpg");
				finder.includeFileType("jpeg");
			}
			~AnimObject();

			static const string langname;
			const string& getLangName() const { return Objsc::AnimObject::langname; }

			const string& getDisplayString()
			{
				if(displaystr.empty())	{
					return Objsc::AnimObject::langname;
				}
				return displaystr;
			}

			void setLabel(string nlabel);

			void activate();
	};

	class AnimObjectCreator : public LangObjectCreator
	{
		public:
			Objsc::LangObjectPtr create() const { return Objsc::LangObjectPtr(new Objsc::AnimObject());}
	};

	
} // namespace Objsc

#endif // OBJSC_ANIMOBJECT_H
