#ifndef OBJSCRS_LANGPATCH_HPP
#define OBJSCRS_LANGPATCH_HPP

#include "../objscrLangObjects.hpp"
#include "BangObject.hpp"

namespace Objsc
{
	/////////////////////////////////////
	// LANG PATCH
	/////////////////////////////////////
	class LangPatch : public LangObject
	{
		private:
			Objsc::LangObjectList 		objects;
			Objsc::LangReceiverMap		receivers;
			string 					name;
			bool					active;

		public:
			LangPatch(const string& nname="") :
				objects(),
				receivers(),
				name(nname),
				active(true) {}
			~LangPatch() {}

			void setName(const string& nname) { name=nname; }
			const string& getName() { return name; }

			void onload();

			void update(int timeframe);

			static const string langname;
			const string& getLangName() const { return Objsc::BangObject::langname; }

			Objsc::LangObjectPtr createObject(string langname, bool addreceiver=true);
			Objsc::LangObjectPtr createObject(string langname, float x, float y, bool addreceiver=true);
			void removeObject(Objsc::LangObjectPtr objPtr);
			Objsc::LangObjectPtr parseStringToObject(string objstr);
			void addObject(Objsc::LangObjectPtr objPtr, bool addreceiver=true);
			void addObjectToReceiver(const string& label, Objsc::LangObjectPtr objPtr);
			void removeObjectFromReceiver(const string& label, Objsc::LangObjectPtr objPtr);
			const Objsc::LangReceiverMap& getReceivers() const { return receivers; }
			void receive(const string& label, Objsc::LangAtomPtr atom);
			const Objsc::LangObjectList& getObjects() const { return objects; }
			Objsc::LangObjectListIter objectsBegin() { return objects.begin(); }
			Objsc::LangObjectListIter objectsEnd() { return objects.end(); }

			void start();
			void stop();

			static std::tr1::shared_ptr<Objsc::LangPatch> create(const string& nname="")
			{
				return std::tr1::shared_ptr<Objsc::LangPatch>(new LangPatch(nname));
			}
	};

	typedef std::tr1::shared_ptr<Objsc::LangPatch> LangPatchPtr;
	typedef map <string, Objsc::LangPatchPtr> LangPatchMap;
	typedef LangPatchMap::value_type LangPatchMapEntry;
}
#endif
