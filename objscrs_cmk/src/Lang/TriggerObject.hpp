#ifndef OBJSCRS_TRIGGEROBJECT_HPP
#define OBJSCRS_TRIGGEROBJECT_HPP
#include "../objscrLangObjects.hpp"
namespace Objsc
{

	/////////////////////////////////////
	// TRIGGER OBJECT
	/////////////////////////////////////
	class TriggerObject : public LangObject
	{
		protected:
			Objsc::LangList		keys;
			string			label;

			void trigger(Objsc::LangAtom* atom);

		public:
			TriggerObject() : keys(), label()
			{
				setInlets(1);
				setOutlets(0);
			}
			~TriggerObject() {}

			static const string langname;
			const string& getLangName() const { return Objsc::TriggerObject::langname; }
			const string& getDisplayString(){return displaystr;}

			void setLabel(string nlabel) {label.assign(nlabel);}
			const string& getLabel() const { return label; }

			bool parseArgs(Objsc::LangList* args);
			void receive(std::string inlet, Objsc::LangAtom* atom);

			const bool hasArgs() const { return true;}
			Objsc::LangList* getArgs() { return &keys; }
			string getArgString() { return listToString(&keys); }
	};


	class TriggerObjectCreator : public LangObjectCreator
	{
		protected:
			string displaystr;

		public:
			TriggerObjectCreator(const string& str="") : displaystr(str) {}
			Objsc::LangObjectPtr create() const
			{
				Objsc::LangObjectPtr ptr = Objsc::LangObjectPtr(new Objsc::TriggerObject());
				if(!displaystr.empty())	{
					ptr->setLabel(displaystr);
				}
				return ptr;
			}
	};

}
#endif
