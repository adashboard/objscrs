#include "OSCInObject.hpp"
namespace Objsc
{
	const string Objsc::OSCInObject::langname = "osc_in";
	/////////////////////////////////////
	// OSC IN OBJECT
	/////////////////////////////////////
	//~ bool OSCInObject::parseArgs(Objsc::LangList* args)
	//~ {
		//~ if(args->size() < min_args)	{
			//~ return false;
		//~ } else {
			//~ oscgroup = Objsc::OSCComms::Current()->startListen(getItemAsString(args, 0), getItemAsInt(args, 1));
			//~ displaystr = Objsc::OSCOutObject::langname + " " + getItemAsString(args, 0) + " " + getItemAsInt(args, 1);
			//~ return true;
		//~ }
	//~ }

	bool OSCInObject::parseArgs(Objsc::LangList* args)
	{
		if(args->size() < min_args)	{
			return false;
		} else {
			setLabel(getItemAsString(args, 0));
			return true;
		}
	}


	void OSCInObject::setLabel(string nlabel)
	{
		label.assign(nlabel);
		displaystr = getLangName() + " " + label;
	}


	void OSCInObject::receive(std::string inlet, Objsc::LangAtom* atom)
	{
		if(label.empty())	{
			send(0, atom);
		} else {
			receiveList("0", dynamic_cast<Objsc::LangList*>(atom));
		}
	}

	void OSCInObject::receiveList(std::string inlet, Objsc::LangList* value)
	{
		string grp = parseString(value->idx(0), "");
		if(grp == label)	{
			send(0, cloneList(value,1));
		}
	}

}
