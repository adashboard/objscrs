#ifndef OBJSCRS_NUMBEROBJECT_HPP
#define OBJSCRS_NUMBEROBJECT_HPP
#include "../objscrLangObjects.hpp"
namespace Objsc
{
	/////////////////////////////////////
	// NUMBER OBJECT
	/////////////////////////////////////
	class NumberObject : public LangObject
	{
		protected:
			float					value;
			string					label;
			Objsc::LangObserverPtr 		observer;

			void receiveList(std::string inlet, LangList* value);

		public:
			NumberObject() : value(), label(), observer()
			{
				setInlets(1);
				setOutlets(1);
			}
			~NumberObject() {}

			static const string langname;
			const string& getLangName() const { return Objsc::NumberObject::langname; }
			void setDisplayString(const string& str);
			const string& getDisplayString(){return displaystr;}

			void setLabel(string nlabel) {label.assign(nlabel);}
			const string& getLabel() const { return label; }

			void setValue(float nvalue);
			const float getValue() const { return value; }

			void receive(std::string inlet, Objsc::LangAtom* atom);
			void activate();

			void setObserver(Objsc::LangObserverPtr obsPtr) {observer=obsPtr;}
			void removeObserver() {observer.reset();}
	};


	class NumberObjectCreator : public LangObjectCreator
	{
		protected:
			string displaystr;

		public:
			NumberObjectCreator(const string& str="") : displaystr(str) {}
			Objsc::LangObjectPtr create() const
			{
				Objsc::LangObjectPtr ptr = Objsc::LangObjectPtr(new Objsc::NumberObject());
				if(!displaystr.empty())	{
					ptr->setLabel(displaystr);
				}
				return ptr;
			}
	};

}
#endif
