#ifndef OBJSCRS_MATHOBJECT_HPP
#define OBJSCRS_MATHOBJECT_HPP

#include "../objscrLangObjects.hpp"
#include "../objscrMath.hpp"
#include "../objscrGeom.hpp"

namespace Objsc
{
	/////////////////////////////////////
	// MATH OBJECT
	/////////////////////////////////////
	class MathObject : public LangObject
	{
		protected:
			float	value;
			string	label;

			virtual void eval(float input) =0;

			virtual void receiveList(std::string inlet, LangList* value);

		public:
			MathObject();
			virtual ~MathObject() {}

			virtual void setValue(float nvalue);

			bool parseArgs(Objsc::LangList* args);
			void setLabel(string nlabel);
			const string& getLabel() const { return label; }

			virtual void receive(std::string inlet, Objsc::LangAtom* atom);
			virtual void activate() {}
	};

}

#endif
