#include "LangMediaObject.hpp"
namespace Objsc
{
	/////////////////////////////////////
	// MEDIA OBJECT
	/////////////////////////////////////
	bool LangMediaObject::parseArgs(Objsc::LangList* args)
	{
		if(args->size() < min_args)	{
			return false;
		} else {
			setLabel(getItemAsString(args, 0));
			return true;
		}
	}

}
