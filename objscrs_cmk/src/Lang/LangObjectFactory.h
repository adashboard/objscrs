/***************************************************************************
 *   Copyright (C) 2012 by Pierre Marchand   *
 *   pierre@oep-h.com   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#ifndef OBJSC_LANGOBJECTFACTORY_H
#define OBJSC_LANGOBJECTFACTORY_H

#include "../singleton.h"
#include "../objscrLangObjects.hpp"

namespace Objsc {
	
	class LangObjectFactory : public Singleton<LangObjectFactory>
	{
		private:
			LangObjectCreatorMap creators;

			void InitObjectCreators();

		public:
			LangObjectFactory();
			~LangObjectFactory() { creators.clear(); }

			Objsc::LangObjectPtr create(const string& langname);

			void addCreator(const string& langname, LangObjectCreator* creatorPtr);

			bool hasCreator(const string& langname)	{return creators.count(langname) > 0;}

			std::vector<std::string> getRegisteredLangNames() const;

	};
	
} // namespace Objsc

#endif // OBJSC_LANGOBJECTFACTORY_H
