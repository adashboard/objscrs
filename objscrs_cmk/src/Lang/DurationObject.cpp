#include "DurationObject.hpp"
namespace Objsc
{

	const string Objsc::DurationObject::langname = "duration";
	////////////////////////////////////////
	// DURATION OBJECT
	////////////////////////////////////////
	DurationObject::DurationObject() :
		count(0),
		active(false)
	{
		setInlets(2);
		setOutlets(1);
	}

	DurationObject::~DurationObject()
	{

	}

	void DurationObject::receive(std::string inlet, Objsc::LangAtom* atom)
	{
		switch(atom->getType())	{
			case LANG_BANG:
			if(inlet == "0")	{
						activate();
						count=0;
			}
			else if(inlet == "1")
						activate();

				break;
			case LANG_INT:
			if(inlet == "0")	{
						if(parseBool(atom))	{
							start();
						} else {
							stop();
						}
				}
				break;
			case LANG_FLOAT:
			if(inlet == "0")	{
						if(parseBool(atom))	{
							start();
						} else {
							stop();
						}
				}
				break;
			case LANG_LIST:
				receiveList(inlet, dynamic_cast<Objsc::LangList*>(atom));
				break;
		}
	}

	void DurationObject::receiveList(std::string inlet, LangList* value)
	{
		if(inlet == "0") {
			string cmd = parseString(value->idx(0), "");
			if(cmd.empty())	{
				parseInt(value->idx(0), 0)>0?start():stop();
			} else {
				if(cmd == "start")	{
					start();
				} else if(cmd == "stop")	{
					stop();
				} else if(cmd == "reset")	{
					count = 0;
				} else if(cmd == "bang")	{
					activate();
					count = 0;
				}
			}
		}
	}

	void DurationObject::start()
	{
		active=true;
		count=0;
	}

	void DurationObject::update(int timeframe)
	{
		if(active)	{
			count += timeframe;
		}
	}

	void DurationObject::activate()
	{
		send(0, new Objsc::LangInt(count));
	}

}
