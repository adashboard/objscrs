#ifndef OBJSCRS_METROOBJECT_HPP
#define OBJSCRS_METROOBJECT_HPP
#include "../objscrLangObjects.hpp"
namespace Objsc
{

	////////////////////////////////////////
	// METRO OBJECT
	////////////////////////////////////////
	class MetroObject : public LangObject
	{
		protected:
			int			interval;
			int 			count;
			bool		active;

			void receiveList(std::string inlet, LangList* value);

		public:
			MetroObject();
			~MetroObject();

			static const string langname;
			const string& getLangName() const { return Objsc::MetroObject::langname; }

			bool parseArgs(Objsc::LangList* args);

			void receive(std::string inlet, Objsc::LangAtom* atom);
			virtual void activate();
			void update(int timeframe);

			void setInterval(int value);
			void start();
			void stop() { active=false; }

			const bool hasArgs() const { return true;}
			string getArgString() { return intToString(interval); }
	};

	class MetroObjectCreator : public LangObjectCreator
	{
		public:
			Objsc::LangObjectPtr create() const { return Objsc::LangObjectPtr(new Objsc::MetroObject());}
	};

}
#endif
