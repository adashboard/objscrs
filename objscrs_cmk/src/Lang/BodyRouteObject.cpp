#include "BodyRouteObject.hpp"
namespace Objsc
{
	const string Objsc::BodyRouteObject::langname = "body_route";

	/////////////////////////////////////
	// BODY ROUTE OBJECT
	/////////////////////////////////////
	bool BodyRouteObject::parseArgs(Objsc::LangList* args)
	{
		//printf("BodyRouteObject::parseArgs START %d\n", args->size());
		if(args->size() < min_args)	{
			return false;
		} else {
			//keys.copy(args, 1);
			keys.copy(args);
			if(label.empty())	{
				//printf("BodyRouteObject::parseArgs <%s> A\n", displaystr.c_str());
				displaystr.assign(getLangName().c_str());
				displaystr.append(" ");
				displaystr.append(listToString(&keys));
			} else {
				displaystr.assign(label.c_str());
				displaystr.append(" ");
				displaystr.append(listToString(&keys));
			}
			//printf("BodyRouteObject::parseArgs <%s>\n", displaystr.c_str());
			setOutlets(keys.size()+1);
			//printf("BodyRouteObject::parseArgs DONE %d\n", keys.size());
			return true;
		}
	}

	void BodyRouteObject::receive(std::string inlet, Objsc::LangAtom* atom)
	{
		switch(atom->getType())	{
			case LANG_NULL:
				break;
			case LANG_BANG:
				break;
			case LANG_INT:
				break;
			case LANG_FLOAT:
				break;
			case LANG_STRING:
				break;
			case LANG_VAR:
				break;
			case LANG_LIST:
				receiveList(inlet, dynamic_cast<Objsc::LangList*>(atom));
				break;
		}
	}

	void BodyRouteObject::receiveList(const std::string& inlet, Objsc::LangList* atom)
	{
		bool nomatch;
		if(atom->size() > 1)	{
			nomatch = true;
			for(int i = 0; i < num_outlets-1; ++i)	{
				//printf("types: %d %d\n", atom->idx(0)->getType(), keys.idx(i)->getType());
				if(equalsAtom(atom->idx(2), keys.idx(i)))	{
					nomatch = false;
					send(i, atom);
					break;
				}
			}
			if(nomatch)	{
				send(num_outlets-1, atom);
			}
		}
	}


}
