#ifndef OBJSCRS_TRACKINGOBJECT_HPP
#define OBJSCRS_TRACKINGOBJECT_HPP
#include "../objscrLangObjects.hpp"
namespace Objsc
{

	/////////////////////////////////////
	// TRACKING OBJECT
	/////////////////////////////////////
	class TrackingObject : public LangObject
	{
		protected:
			void showTracking();
			void hideTracking();
			void startTracking();
			void stopTracking();
			void takeRef();
			void startRef();
			void stopRef();
			void receiveList(std::string inlet, LangList* value);

			void setShapesMinRadius(int value);
			void setShapesMaxRadius(int value);

		public:
			TrackingObject()
			{
				setInlets(5);
				setOutlets(2);
			}
			~TrackingObject() {}

			static const string langname;
			const string& getLangName() const { return Objsc::TrackingObject::langname; }


			const string& getDisplayString()
			{
				return Objsc::TrackingObject::langname;
			}

			void receive(std::string inlet, Objsc::LangAtom* atom);
	};

	class TrackingObjectCreator : public LangObjectCreator
	{
		public:
			Objsc::LangObjectPtr create() const { return Objsc::LangObjectPtr(new Objsc::TrackingObject());}
	};

}

#endif
