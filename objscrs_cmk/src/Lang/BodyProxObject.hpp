#ifndef OBJSCRS_BODYPROXOBJECT_HPP
#define OBJSCRS_BODYPROXOBJECT_HPP
#include "../objscrLangObjects.hpp"
namespace Objsc
{

	/////////////////////////////////////
	// BODY PROX OBJECT
	/////////////////////////////////////
	class BodyProxObject : public LangObject
	{
		protected:
			int 		id;
			float 	x;
			float 	y;
			float 	p;

			void receiveBody(const std::string& inlet, LangList* nbody);

		public:
			BodyProxObject() : id(0), x(0.0f), y(0.0f), p(0.0f)
			{
				setInlets(2);
				setOutlets(4);
			}
			~BodyProxObject() {}

			static const string langname;
			const string& getLangName() const { return Objsc::BodyProxObject::langname; }

			const string& getDisplayString()
			{
				if(displaystr.empty())	{
					return Objsc::BodyProxObject::langname;
				}
				return displaystr;
			}

			void receive(std::string inlet, Objsc::LangAtom* atom);
			void activate();
	};

	class BodyProxObjectCreator : public LangObjectCreator
	{
		public:
			Objsc::LangObjectPtr create() const { return Objsc::LangObjectPtr(new Objsc::BodyProxObject());}
	};
}
#endif
