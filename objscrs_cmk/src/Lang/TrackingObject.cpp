
#include "TrackingObject.hpp"

#include "../objscrStage.hpp"
#include "../objscrStageView.hpp"
#include "../objscrGUIRoot.hpp"

namespace Objsc
{
	const string Objsc::TrackingObject::langname = "tracking";

	/////////////////////////////////////
	// TRACKING OBJECT
	/////////////////////////////////////
	void TrackingObject::receive(std::string inlet, Objsc::LangAtom* atom)
	{
		if(checkInlet(inlet))
		{
			switch(atom->getType())	{
				case LANG_INT:
				if(inlet == "0") {
							if(parseBool(atom))	{
								startTracking();
							} else {
								stopTracking();
							}
				}
				else if(inlet == "1")
							Objsc::Stage::Current()->setShapesMinRadius(parseInt(atom, Objsc::DEFAULT_TRACKING_MIN_AREA));
				else if(inlet == "2")
							Objsc::Stage::Current()->setShapesMaxRadius(parseInt(atom, Objsc::DEFAULT_TRACKING_MAX_AREA));
				else if(inlet == "3")
							Objsc::Stage::Current()->setShapesThreshold(parseInt(atom, Objsc::DEFAULT_TRACKING_THRESHOLD));
				else if(inlet == "4")
							Objsc::Stage::Current()->setShapesBlur(parseInt(atom, Objsc::DEFAULT_TRACKING_BLUR));

					break;
				case LANG_FLOAT:
				if(inlet == "0") {
							if(parseBool(atom))	{
								startTracking();
							} else {
								stopTracking();
							}
				}
				else if(inlet == "1")
							Objsc::Stage::Current()->setShapesMinRadius(parseInt(atom, Objsc::DEFAULT_TRACKING_MIN_AREA));
				else if(inlet == "2")
							Objsc::Stage::Current()->setShapesMaxRadius(parseInt(atom, Objsc::DEFAULT_TRACKING_MAX_AREA));
				else if(inlet == "3")
							Objsc::Stage::Current()->setShapesThreshold(parseInt(atom, Objsc::DEFAULT_TRACKING_THRESHOLD));
				else if(inlet == "4")
							Objsc::Stage::Current()->setShapesBlur(parseInt(atom, Objsc::DEFAULT_TRACKING_BLUR));

					break;
				case LANG_LIST:
				receiveList("0", dynamic_cast<Objsc::LangList*>(atom));
					break;
			}
		}
	}

	void TrackingObject::receiveList(std::string inlet, LangList* value)
	{

		string cmd = parseString(value->idx(0), "");
		if(cmd == "show")	{
			showTracking();
		} else if(cmd == "hide")	{
			hideTracking();
		} else if(cmd == "start")	{
			startTracking();
		} else if(cmd == "stop")	{
			stopTracking();
		} else if(cmd == "ref")	{
			takeRef();
		} else if(cmd == "continuous")	{
			startRef();
		} else if(cmd == "static")	{
			stopRef();
		} else if(cmd == "preview_on")	{
			Objsc::Stage::Current()->previewOn();
		} else if(cmd == "preview_off")	{
			Objsc::Stage::Current()->previewOff();
		} else if(cmd == "vidmask")	{
			if(value->size() > 1)	{
				string opt = parseString(value->idx(1), "");
				if (opt.empty())	{
					return;
				} else {
					if(opt == "on")	{
						Objsc::Stage::Current()->getShapeFilter()->useMask();
					} else if(opt == "off")	{
						Objsc::Stage::Current()->getShapeFilter()->clearMask();
					} else {
						Objsc::Stage::Current()->getShapeFilter()->setMaskImage(opt);
					}
				}
			}
		}
		else if(cmd == "colour")
		{
			string cVal(parseString(value->idx(1), "gray"));
			ofxCvColorImage::ColorChannel cc;
			if(cVal == "red")
			{
				cc = ofxCvColorImage::ChannelRed;
			}
			else if(cVal == "green")
			{
				cc = ofxCvColorImage::ChannelGreen;
			}
			else if(cVal == "blue")
			{
				cc = ofxCvColorImage::ChannelBlue;
			}
			else if(cVal == "hue")
			{
				cc = ofxCvColorImage::ChannelHue;
			}
			else if(cVal == "saturation")
			{
				cc = ofxCvColorImage::ChannelSaturation;
			}
			else if(cVal == "value")
			{
				cc = ofxCvColorImage::ChannelValue;
			}
			else
			{
				cc = ofxCvColorImage::ChannelGray;
			}
			Objsc::Stage::Current()->getShapeFilter()->setColorChannel(cc);
		}
		else if((cmd == "view") && (value->size() > 2))
		{
			std::string viewType(parseString(value->idx(1) ,std::string()));
			bool cVal(parseBool(value->idx(2), true));
			// TODO - refactor this to build the command based on viewType and cVal
			if(viewType == "contour")
			{
				if(cVal)
					Objsc::CommandDispatcher::Current()->doCommand(Objsc::StageView::cmdlabel, std::string("body_view_contour_on"));
				else
					Objsc::CommandDispatcher::Current()->doCommand(Objsc::StageView::cmdlabel, std::string("body_view_contour_off"));
			}
			else if(viewType == "box")
			{
				if(cVal)
					Objsc::CommandDispatcher::Current()->doCommand(Objsc::StageView::cmdlabel, std::string("body_view_box_on"));
				else
					Objsc::CommandDispatcher::Current()->doCommand(Objsc::StageView::cmdlabel, std::string("body_view_box_off"));
			}
			else if(viewType == "loc")
			{
				if(cVal)
					Objsc::CommandDispatcher::Current()->doCommand(Objsc::StageView::cmdlabel, std::string("body_view_loc_on"));
				else
					Objsc::CommandDispatcher::Current()->doCommand(Objsc::StageView::cmdlabel, std::string("body_view_loc_off"));
			}
		}
		else if((cmd == "min_area"
			 || cmd == "max_area"
			 || cmd == "threshold"
			 || cmd == "blur")
				&& (value->size() > 1))
		{
			int cVal(parseInt(value->idx(1)));
			Objsc::CommandPtr cp = Objsc::CommandDispatcher::Current()->createCommand(Objsc::ShapeFilter::cmdlabel, cmd);
			cp->getData()->addArg(cVal);
			Objsc::CommandDispatcher::Current()->doCommand(cp);
		}
	}

	void TrackingObject::showTracking()
	{
		Objsc::GUIRoot::Current()->showStage();
		send(3, new Objsc::LangInt(1));
	}

	void TrackingObject::hideTracking()
	{
		Objsc::GUIRoot::Current()->hideStage();
		send(3, new Objsc::LangInt(0));
	}

	void TrackingObject::startTracking()
	{
		Objsc::Stage::Current()->start();
		send(2, new Objsc::LangInt(1));
	}

	void TrackingObject::stopTracking()
	{
		Objsc::Stage::Current()->stop();
		Objsc::GUIRoot::Current()->clearStage();
		send(2, new Objsc::LangInt(0));
	}

	void TrackingObject::takeRef()
	{
		Objsc::CommandPtr cmd = Objsc::CommandDispatcher::createCommand(Objsc::ShapeFilter::cmdlabel, "ref_take");
		Objsc::CommandDispatcher::Current()->doCommand(cmd);
		send(0, new Objsc::LangBang());
	}

	void TrackingObject::startRef()
	{
		Objsc::CommandPtr cmd = Objsc::CommandDispatcher::createCommand(Objsc::ShapeFilter::cmdlabel, "ref_on");
		Objsc::CommandDispatcher::Current()->doCommand(cmd);
		send(1, new Objsc::LangInt(1));
	}

	void TrackingObject::stopRef()
	{
		Objsc::CommandPtr cmd = Objsc::CommandDispatcher::createCommand(Objsc::ShapeFilter::cmdlabel, "ref_off");
		Objsc::CommandDispatcher::Current()->doCommand(cmd);
		send(1, new Objsc::LangInt(0));
	}

}
