#ifndef OBJSCRS_MESSAGEOBJECT_HPP
#define OBJSCRS_MESSAGEOBJECT_HPP
#include "../objscrLangObjects.hpp"
namespace Objsc
{
	/////////////////////////////////////
	// MESSAGE OBJECT
	/////////////////////////////////////
	class MessageObject : public LangObject
	{
		protected:
			Objsc::LangList message;

		public:
			MessageObject() : message()
			{
				setInlets(1);
				setOutlets(1);
			}
			~MessageObject() {}

			static const string langname;
			const string& getLangName() const { return Objsc::MessageObject::langname; }
			void setDisplayString(const string& str);
			const string& getDisplayString(){return displaystr;}

			void receive(std::string inlet, Objsc::LangAtom* atom);
			void activate();
	};

	class MessageObjectCreator : public LangObjectCreator
	{
		public:
			Objsc::LangObjectPtr create() const { return Objsc::LangObjectPtr(new Objsc::MessageObject());}
	};

}
#endif
