#include "ControlKeyObject.hpp"
namespace Objsc
{

	const string Objsc::ControlKeyObject::langname = "ctrlkey";

	/////////////////////////////////////
	// RECEIVE OBJECT
	/////////////////////////////////////
	ControlKeyObject::ControlKeyObject() : label()
	{
		setInlets(0);
		setOutlets(3);
		min_args=0;
	}

	ControlKeyObject::~ControlKeyObject()
	{

	}

	bool ControlKeyObject::parseArgs(Objsc::LangList* args)
	{
		if(args->size() < min_args)	{
			return false;
		} else {
			setLabel(getItemAsString(args, 0));
			return true;
		}
	}

	void ControlKeyObject::setLabel(string nlabel)
	{
		label.assign(nlabel);
		displaystr = getLangName() + " " + label;
	}

	void ControlKeyObject::receive(std::string inlet, Objsc::LangAtom* atom)
	{
		send(1, atom);
		if(label == parseString(atom))	{
			send(0, new Objsc::LangBang());
		} else {
			send(2, new Objsc::LangBang());
		}
	}
}

