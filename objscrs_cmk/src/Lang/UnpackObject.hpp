#ifndef OBJSCRS_UNPACKOBJECT_HPP
#define OBJSCRS_UNPACKOBJECT_HPP
#include "../objscrLangObjects.hpp"
namespace Objsc
{

	/////////////////////////////////////
	// UNPACK OBJECT
	/////////////////////////////////////
	class UnpackObject : public LangObject
	{
		protected:
			Objsc::LangList		keys;
			string			label;

			void receiveList(std::string inlet, Objsc::LangList* atom);

		public:
			UnpackObject() : keys(), label()
			{
				setInlets(1);
				setOutlets(0);
			}
			~UnpackObject() {}

			static const string langname;
			const string& getLangName() const { return Objsc::UnpackObject::langname; }
			const string& getDisplayString(){return displaystr;}

			void setLabel(string nlabel) {label.assign(nlabel);}
			const string& getLabel() const { return label; }

			bool parseArgs(Objsc::LangList* args);
			void receive(std::string inlet, Objsc::LangAtom* atom);

			const bool hasArgs() const { return true;}
			Objsc::LangList* getArgs() { return &keys; }
			string getArgString() { return listToString(&keys); }
	};


	class UnpackObjectCreator : public LangObjectCreator
	{
		public:
			Objsc::LangObjectPtr create() const { return Objsc::LangObjectPtr(new Objsc::UnpackObject());}
	};

}
#endif
