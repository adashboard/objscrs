#ifndef OBJSCRS_ZONEOBJECT_HPP
#define OBJSCRS_ZONEOBJECT_HPP
#include "../objscrLangObjects.hpp"
namespace Objsc
{

	/////////////////////////////////////
	// ZONE OBJECT
	/////////////////////////////////////
	class ZoneObject : public LangObject
	{
		protected:
			Objsc::LangList 	body;
			int				numbodies;
			string 			label;

			void receiveBody(const std::string& inlet, LangList* nbody);

		public:
			ZoneObject() : label()
			{
				setInlets(2);
				setOutlets(3); // this will change to 2
			}
			~ZoneObject() {}

			static const string langname;
			const string& getLangName() const { return Objsc::ZoneObject::langname; }

			void setLabel(string nlabel) {label.assign(nlabel);}
			const string& getLabel() const { return label; }

			const string& getDisplayString()
			{
				if(!displaystr.empty())	{
					return displaystr;
				} else if(!label.empty())	{
					return label;
				}
				return Objsc::ZoneObject::langname;
			}

			void receive(std::string inlet, Objsc::LangAtom* atom);
			void activate();
	};


	class ZoneObjectCreator : public LangObjectCreator
	{
		public:
			Objsc::LangObjectPtr create() const { return Objsc::LangObjectPtr(new Objsc::ZoneObject());}
	};

	class NoInletsZoneObjectCreator : public LangObjectCreator
	{
		protected:
			string displaystr;

		public:
			NoInletsZoneObjectCreator(const string& str="") : displaystr(str) {}
			Objsc::LangObjectPtr create() const
			{
				Objsc::LangObjectPtr ptr = Objsc::LangObjectPtr(new Objsc::ZoneObject());
				ptr->hideInlets();
				if(!displaystr.empty())	{
					ptr->setLabel(displaystr);
				}
				return ptr;
			}
	};


}
#endif
