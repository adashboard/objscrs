#include "ReceiveObject.hpp"
namespace Objsc
{
	const string Objsc::ReceiveObject::langname = "receive";

	/////////////////////////////////////
	// RECEIVE OBJECT
	/////////////////////////////////////
	ReceiveObject::ReceiveObject() : label()
	{
		setInlets(0);
		setOutlets(1);
		min_args=1;
	}

	ReceiveObject::~ReceiveObject()
	{

	}

	bool ReceiveObject::parseArgs(Objsc::LangList* args)
	{
		if(args->size() < min_args)	{
			return false;
		} else {
			setLabel(getItemAsString(args, 0));
			return true;
		}
	}

	void ReceiveObject::setLabel(string nlabel)
	{
		label.assign(nlabel);
		displaystr = getLangName() + " " + label;
	}

	void ReceiveObject::receive(std::string inlet, Objsc::LangAtom* atom)
	{
		send(0, atom);
	}

}
