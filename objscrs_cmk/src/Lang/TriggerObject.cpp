#include "TriggerObject.hpp"
namespace Objsc
{
	const string Objsc::TriggerObject::langname = "trigger";

	/////////////////////////////////////
	// TRIGGER OBJECT
	/////////////////////////////////////
	bool TriggerObject::parseArgs(Objsc::LangList* args)
	{
		//printf("TriggerObject::parseArgs START %d\n", args->size());
		if(args->size() < min_args)	{
			return false;
		} else {
			//keys.copy(args, 1);
			keys.copy(args);
			if(label.empty())	{
				//printf("TriggerObject::parseArgs <%s> A\n", displaystr.c_str());
				displaystr.assign(getLangName().c_str());
				displaystr.append(" ");
				displaystr.append(listToString(&keys));
			} else {
				displaystr.assign(label.c_str());
				displaystr.append(" ");
				displaystr.append(listToString(&keys));
			}
			//printf("TriggerObject::parseArgs <%s>\n", displaystr.c_str());
			setOutlets(keys.size());
			//printf("TriggerObject::parseArgs DONE %d\n", keys.size());
			return true;
		}
	}

	void TriggerObject::receive(std::string inlet, Objsc::LangAtom* atom)
	{
		switch(atom->getType())	{
			case LANG_NULL:
				break;
			case LANG_BANG:
				trigger(atom);
				break;
			case LANG_INT:
				trigger(atom);
				break;
			case LANG_FLOAT:
				trigger(atom);
				break;
			case LANG_STRING:
				trigger(atom);
				break;
			case LANG_VAR:
				break;
			case LANG_LIST:
				trigger(dynamic_cast<Objsc::LangList*>(atom)->idx(0));
				break;
		}
	}

	void TriggerObject::trigger(Objsc::LangAtom* atom)
	{
		for(int i = num_outlets-1; i > -1; --i)	{
			if(atom->getType() == keys.idx(i)->getType())	{
				send(i, atom);
			} else {
				send(i, keys.idx(i));
			}
		}
	}

}
