#include "BodyPrefsObject.hpp"
#include "../objscrBody.hpp"
#include "../objscrStage.hpp"


namespace Objsc
{
	const string Objsc::BodyPrefsObject::langname = "body_prefs";

	/////////////////////////////////////
	// BODY PREFS OBJECT
	/////////////////////////////////////
	void BodyPrefsObject::receive(std::string inlet, Objsc::LangAtom* atom)
	{
		if(checkInlet(inlet))
		{
			switch(atom->getType())	{
				case LANG_BANG:
				if(inlet == "0")
					Objsc::Body::printThresholds();
				break;
				case LANG_INT:
				if(inlet == "0")
							Objsc::Body::unbornAge = parseInt(atom, Objsc::DEFAULT_BODY_UNBORN_AGE);
				else if(inlet == "1")
							Objsc::Body::unbornHits = parseInt(atom, Objsc::DEFAULT_BODY_UNBORN_HITS);
				else if(inlet == "2")
							Objsc::Body::unbornLastHit = parseInt(atom, Objsc::DEFAULT_BODY_UNBORN_LAST_HIT);
				else if(inlet == "3")
							Objsc::Body::becomingAge = parseInt(atom, Objsc::DEFAULT_BODY_BECOMING_AGE);
				else if(inlet == "4")
							Objsc::Body::becomingHits = parseInt(atom, Objsc::DEFAULT_BODY_BECOMING_HITS);
				else if(inlet == "5")
							Objsc::Body::becomingLastHit = parseInt(atom, Objsc::DEFAULT_BODY_BECOMING_LAST_HIT);
				else if(inlet == "6")
							Objsc::Body::presentLastHit = parseInt(atom, Objsc::DEFAULT_BODY_PRESENT_LAST_HIT);
				else if(inlet == "7")
							Objsc::Body::absentLastHitMin = parseInt(atom, Objsc::DEFAULT_BODY_ABSENT_LAST_HIT_MIN);
				else if(inlet == "8")
							Objsc::Body::absentLastHitMax = parseInt(atom, Objsc::DEFAULT_BODY_ABSENT_LAST_HIT_MAX);
				else if(inlet == "9")
							Objsc::Body::maxDist = parseFloat(atom, Objsc::DEFAULT_BODY_MAXDISTANCE);
				else if(inlet == "10")
				{
							Objsc::Body::velSteps = parseInt(atom, Objsc::DEFAULT_BODY_VELSTEPS);
							Objsc::Stage::Current()->getBodyFilter()->velocitySmoothingReset();

					}
					break;
				case LANG_FLOAT:
				if(inlet == "0")
							Objsc::Body::unbornAge = parseInt(atom, Objsc::DEFAULT_BODY_UNBORN_AGE);
				else if(inlet == "1")
							Objsc::Body::unbornHits = parseInt(atom, Objsc::DEFAULT_BODY_UNBORN_HITS);
				else if(inlet == "2")
							Objsc::Body::unbornLastHit = parseInt(atom, Objsc::DEFAULT_BODY_UNBORN_LAST_HIT);
				else if(inlet == "3")
							Objsc::Body::becomingAge = parseInt(atom, Objsc::DEFAULT_BODY_BECOMING_AGE);
				else if(inlet == "4")
							Objsc::Body::becomingHits = parseInt(atom, Objsc::DEFAULT_BODY_BECOMING_HITS);
				else if(inlet == "5")
							Objsc::Body::becomingLastHit = parseInt(atom, Objsc::DEFAULT_BODY_BECOMING_LAST_HIT);
				else if(inlet == "6")
							Objsc::Body::presentLastHit = parseInt(atom, Objsc::DEFAULT_BODY_PRESENT_LAST_HIT);
				else if(inlet == "7")
							Objsc::Body::absentLastHitMin = parseInt(atom, Objsc::DEFAULT_BODY_ABSENT_LAST_HIT_MIN);
				else if(inlet == "8")
							Objsc::Body::absentLastHitMax = parseInt(atom, Objsc::DEFAULT_BODY_ABSENT_LAST_HIT_MAX);
				else if(inlet == "9")
							Objsc::Body::maxDist = parseFloat(atom, Objsc::DEFAULT_BODY_MAXDISTANCE);
				else if(inlet == "10")
				{
							Objsc::Body::velSteps = parseInt(atom, Objsc::DEFAULT_BODY_VELSTEPS);
							Objsc::Stage::Current()->getBodyFilter()->velocitySmoothingReset();

					}
					break;
				case LANG_LIST:
				receiveList("0", dynamic_cast<Objsc::LangList*>(atom));
					break;
			}
		}
	}



	void BodyPrefsObject::receiveList(std::string inlet, LangList* value)
	{
		string cmd = parseString(value->idx(0), "");
		if(cmd == "reset")	{
			Objsc::Body::resetThresholds();
			Objsc::Body::printThresholds();
		} else if(cmd == "print")	{
			Objsc::Body::printThresholds();
		} else if(cmd == "save")	{
			Objsc::Body::printThresholds();
		}
	}

}
