#include "GrabObject.hpp"
#include "../objscrFiles.hpp"
#include "../objscrSettings.hpp"
#include "../objscrGUIRoot.hpp"

namespace Objsc
{
	const string Objsc::GrabObject::langname = "grab";

	/////////////////////////////////////
	// GRAB OBJECT
	/////////////////////////////////////
	GrabObject::GrabObject() :
		label(),
		img(),
		alpha(255),
		screen(false)
	{
		setInlets(2);
		setOutlets(1);
	}

	bool GrabObject::parseArgs(Objsc::LangList* args)
	{
		if(args->size() < min_args)	{
			return false;
		} else {
			setLabel(getItemAsString(args, 0));
			return true;
		}
	}

	void GrabObject::setLabel(string nlabel)
	{
		label.assign(nlabel);
		displaystr = Objsc::GrabObject::langname + " " + label;
		//printf("ImageObject::setLabel <%s>\n", label.c_str());
	}

	void GrabObject::activate()
	{
		grab();
		send(0, new Objsc::LangBang());
	}

	void GrabObject::setAlpha(int value)
	{
		if(value > 99)	{
			alpha = 255;
		} else if(value < 1)	{
			alpha = 0;
		} else {
			alpha = (unsigned char)(255*(float(value)/100.0));
		}
	}

	void GrabObject::grab()
	{
		if(screen)	{
			grabScreen();
		} else {
			grabCamera();
		}
	}

	void GrabObject::grabScreen()
	{
		int w = ofGetWidth();
		int h = ofGetHeight();
		img.grabScreen(0,0,w,h);
		img.setImageType(OF_IMAGE_COLOR_ALPHA);
		unsigned char *pixels = img.getPixels();
		unsigned char r,b;
		int span4;
		for (int i = 0; i < h; i++) {
			for (int j = 0; j < w*4; j+=4) {
				// NOTE: swap red and blue channels to fix FreeImage
				span4 = (i*w*4)+j;
				r = pixels[span4];
				b = pixels[span4+2];
				pixels[span4] = b;
				pixels[span4+2] = r;
				pixels[span4+3]=alpha;
				//pixels[(i*w*4) + (j+3)]=alpha;
			}
		}
		img.update();
		if(label.empty())	{
			img.saveImage(Objsc::ProjectFiles::Current()->makeTimestampedImagePath("objscrs"));
		} else {
			img.saveImage(Objsc::ProjectFiles::Current()->makeTimestampedImagePath(label));
		}
	}

	void GrabObject::grabCamera()
	{
		if(Objsc::Settings::Current()->getCameraOn())	{
			if(!frameSource)	{
				frameSource = Objsc::GUIRoot::Current()->getFrameSourceEditor()->getFrameSource();
			}
			int w=frameSource->getWidth();
			int h=frameSource->getHeight();
			img.allocate(w,h, OF_IMAGE_COLOR_ALPHA);
			unsigned char *srcPixels = frameSource->getFrame();
			unsigned char *dstPixels = img.getPixels();

			int span3, span4;
			for (int i = 0; i < h; i++) {
				for (int j = 0; j < w; j++) {
					span3 = (i*w*3)+(j*3);
					span4 = (i*w*4)+(j*4);
					/*
					dstPixels[span4] = srcPixels[span3];
					dstPixels[span4+1] = srcPixels[span3+1];
					dstPixels[span4+2] = srcPixels[span3+2];
					*/
					// NOTE: swap red and blue channels to fix FreeImage
					dstPixels[span4] = srcPixels[span3+2];
					dstPixels[span4+1] = srcPixels[span3+1];
					dstPixels[span4+2] = srcPixels[span3];
					dstPixels[span4+3]=alpha;
				}
			}

			img.update();
			if(label.empty())	{
				img.saveImage(Objsc::ProjectFiles::Current()->makeTimestampedImagePath("objscrs"));
			} else {
				img.saveImage(Objsc::ProjectFiles::Current()->makeTimestampedImagePath(label));
			}
		}
	}

	void GrabObject::receive(std::string inlet, Objsc::LangAtom* atom)
	{
		switch(atom->getType())	{
		case LANG_NULL:
			break;
		case LANG_BANG:
			if(inlet == "0")	{
				activate();
			}
			break;
		case LANG_INT:
			if(inlet == "0")
			{
				if(parseBool(atom))	{
					activate();
				}
			}
			if(inlet == "1")
			{
				setAlpha(parseInt(atom, 100));
				break;
			}

			break;
		case LANG_FLOAT:
			if(inlet == "0")
			{
				if(parseBool(atom))	{
					activate();
				}
			}
			if(inlet == "1")
			{
				setAlpha(parseInt(atom, 100));
				break;
			}
			break;
		case LANG_STRING:
			break;
		case LANG_VAR:
			break;
		case LANG_LIST:
			if(inlet == "0") {
				receiveList("0", dynamic_cast<Objsc::LangList*>(atom));
			}
			break;
		}
	}

	void GrabObject::receiveList(std::string inlet, LangList* value)
	{

		string cmd = parseString(value->idx(0), "");
		if(cmd.empty()) {
			int num = parseInt(value->idx(0), -1);
			if(num > -1)	{
				setAlpha(num);
			}
		} else {
			if(cmd == "grab")	{
				activate();
			} else if(cmd == "prefix")	{
				setLabel(parseString(value->idx(1), ""));
			} else if(cmd == "alpha")	{
				if(value->size() > 1)	{
					setAlpha(parseInt(value->idx(1), 100));
				}
			} else if(cmd == "source")	{
				if(value->size() > 1)	{
					if(parseString(value->idx(1), "") == "screen") {
						screen = true;
					} else {
						screen = false;
					}
				}
			}
		}
	}

}
