/***************************************************************************
 *   Copyright (C) 2012 by Pierre Marchand   *
 *   pierre@oep-h.com   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#include "ImageObject.h"

#include "../objscrMediaViews.hpp"
#include "../objscrMediaPlayer.hpp"

namespace Objsc {
	const string Objsc::ImageObject::langname = "img";
	
	ImageObject::~ImageObject()
	{

	}

	void ImageObject::setLabel(string nlabel)
	{
		label.assign(nlabel);
		displaystr = Objsc::ImageObject::langname + " " + label;
		//printf("ImageObject::setLabel <%s>\n", label.c_str());
	}

	void ImageObject::receive(std::string inlet, Objsc::LangAtom* atom)
	{
		switch(atom->getType())	{
			case LANG_NULL:
				break;
			case LANG_BANG:
			if(inlet == "0")	{
					activate();
				}
				break;
			case LANG_INT:
			if(inlet == "0") {
				if(parseBool(atom))	{
					showMedia();
				} else {
					hideMedia();
				}
			}
			else if(inlet == "1")
				locXMedia(parseFloat(atom, 0.0f));
			else if(inlet == "2")
				locYMedia(parseFloat(atom, 0.0f));
			else if(inlet == "3")
				widthMedia(parseFloat(atom, -1.0f));
			else if(inlet == "4")
				heightMedia(parseFloat(atom, -1.0f));

			break;
			case LANG_FLOAT:
			if(inlet == "0") {
				if(parseBool(atom))	{
					showMedia();
				} else {
					hideMedia();
				}
			}
			else if(inlet == "1")
				locXMedia(parseFloat(atom, 0.0f));
			else if(inlet == "2")
				locYMedia(parseFloat(atom, 0.0f));
			else if(inlet == "3")
				widthMedia(parseFloat(atom, -1.0f));
			else if(inlet == "4")
				heightMedia(parseFloat(atom, -1.0f));

			break;
			case LANG_STRING:
				break;
			case LANG_VAR:
				break;
			case LANG_LIST:
			if(inlet == "0")
				receiveList("0", dynamic_cast<Objsc::LangList*>(atom));
			else if(inlet == "1")
				locXMedia(parseFloat(dynamic_cast<Objsc::LangList*>(atom)->idx(0), 0.0f));
			else if(inlet == "2")
				locYMedia(parseFloat(dynamic_cast<Objsc::LangList*>(atom)->idx(0), 0.0f));
			else if(inlet == "3")
				widthMedia(parseFloat(dynamic_cast<Objsc::LangList*>(atom)->idx(0), -1.0f));
			else if(inlet == "4")
				heightMedia(parseFloat(dynamic_cast<Objsc::LangList*>(atom)->idx(0), -1.0f));

			break;
		}
	}

	void ImageObject::receiveList(std::string inlet, LangList* value)
	{

		string cmd = parseString(value->idx(0), "");
		if(cmd.empty()) {
			if(parseInt(value->idx(0), -1) == 1)	{
				showMedia();
			} else if(parseInt(value->idx(0), -1) == 0)	{
				hideMedia();
			}
		} else {
			if(cmd == "load")	{
				if(value->size() == 2)	{
					loadMedia(parseString(value->idx(1), ""),0,false);
				} else if(value->size() == 3)	{
					loadMedia(parseString(value->idx(1), ""),parseInt(value->idx(2), 0),false);
				} if(value->size() > 3)	{
					loadMedia(parseString(value->idx(1), ""),parseInt(value->idx(2), 0),parseBool(value->idx(3), false));
				}
			} else if(cmd == "show")	{
				if(value->size() == 2)	{
					showMedia(parseInt(value->idx(1), 0));
				} else {
					showMedia();
				}
			} else if(cmd == "hide")	{
				hideMedia();
			} else if(cmd == "fullscreen")	{
				fullscreenMedia();
			} else if(cmd == "centre")	{
				centreMedia();
			} else if(cmd == "size")	{
				if(value->size() > 2)	{
					sizeMedia(parseFloat(value->idx(1), -1), parseFloat(value->idx(2), -1));
				}
			} else if(cmd == "loc")	{
				if(value->size() > 2)	{
					locMedia(parseFloat(value->idx(1), 0), parseFloat(value->idx(2), 0));
				}
			} else if(cmd == "geom")	{
				if(value->size() > 4)	{
					geomMedia(parseFloat(value->idx(1), 0), parseFloat(value->idx(2), 0),parseFloat(value->idx(3), -1), parseFloat(value->idx(4), -1));
				}
			} else if(cmd == "layer")	{
				if(value->size() > 1)	{
					setLayerMedia(parseInt(value->idx(1), 0));
				}
			} else if(cmd == "remove")	{
				removeMedia();
			}
			else if(cmd == "alpha")
			{
				setAlpha(parseFloat(value->idx(1), 1.0));
			}
		}
	}

	void ImageObject::setAlpha(float alpha)
	{
		if(!label.empty())
		{
			MediaViewPtr mptr(Objsc::MediaPlayer::Current()->getMedia(label));

			reinterpret_cast<ImageView*>(&(*mptr))->setAlpha(alpha);
		}
	}

	void ImageObject::loadMedia(const string& filename, int layer, bool show)
	{
		if(!filename.empty() && !label.empty())	{
			Objsc::MediaPlayer::Current()->loadImage(filename, label);
			if(layer != 0)	{
				if(show)	{
					Objsc::MediaPlayer::Current()->show(label, layer);
				} else {
					Objsc::MediaPlayer::Current()->setLayer(label, layer);
				}
			}
		}
	}

	void ImageObject::removeMedia()
	{
		Objsc::MediaPlayer::Current()->remove(label);
	}

	void ImageObject::showMedia(int layer)
	{
		if(layer != 0)	{
			Objsc::MediaPlayer::Current()->show(label, layer);
		} else {
			Objsc::MediaPlayer::Current()->show(label);
		}
		send(0, new Objsc::LangBang());
		send(1, new Objsc::LangInt(1));
	}

	void ImageObject::hideMedia()
	{
		Objsc::MediaPlayer::Current()->hide(label);
		send(1, new Objsc::LangInt(0));
	}

	void ImageObject::fullscreenMedia()
	{
		Objsc::MediaViewPtr media = Objsc::MediaPlayer::Current()->getMedia(label);
		if(media)	{
			media->fullscreen();
		}
	}

	void ImageObject::centreMedia()
	{
		Objsc::MediaViewPtr media = Objsc::MediaPlayer::Current()->getMedia(label);
		if(media)	{
			media->fullscreen();
		}
	}

	void ImageObject::sizeMedia(float width, float height)
	{
		if(width < 0 || height < 0)
			return;
		Objsc::MediaViewPtr media = Objsc::MediaPlayer::Current()->getMedia(label);
		if(media)	{
			media->setDimensions(width,height);
		}
	}

	void ImageObject::widthMedia(float width)
	{
		Objsc::MediaViewPtr media = Objsc::MediaPlayer::Current()->getMedia(label);
		if(media)	{
			media->setWidth(width);
		}
	}

	void ImageObject::heightMedia(float height)
	{
		Objsc::MediaViewPtr media = Objsc::MediaPlayer::Current()->getMedia(label);
		if(media)	{
			media->setHeight(height);
		}
	}

	void ImageObject::locXMedia(float x)
	{
		Objsc::MediaViewPtr media = Objsc::MediaPlayer::Current()->getMedia(label);
		if(media)	{
			media->setCentreX(x);
		}
	}

	void ImageObject::locYMedia(float y)
	{
		Objsc::MediaViewPtr media = Objsc::MediaPlayer::Current()->getMedia(label);
		if(media)	{
			media->setCentreY(y);
		}
	}

	void ImageObject::locMedia(float x, float y)
	{
		Objsc::MediaViewPtr media = Objsc::MediaPlayer::Current()->getMedia(label);
		if(media)	{
			media->setLocation(x,y);
		}
	}

	void ImageObject::geomMedia(float x, float y,float width, float height)
	{
		if(width < 0 || height < 0)
			return;
		Objsc::MediaViewPtr media = Objsc::MediaPlayer::Current()->getMedia(label);
		if(media)	{
			media->setBounds(x,y,width,height);
		}
	}

	void ImageObject::setLayerMedia(int layer)
	{
		Objsc::MediaPlayer::Current()->setLayer(label, layer);
	}

	void ImageObject::activate()
	{
		showMedia();
	}

	
} // namespace Objsc
