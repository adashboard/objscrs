#include "BodyProxObject.hpp"
namespace Objsc
{
	const string Objsc::BodyProxObject::langname = "body_prox";

	/////////////////////////////////////
	// BODY PROX OBJECT
	/////////////////////////////////////
	void BodyProxObject::receive(std::string inlet, Objsc::LangAtom* atom)
	{
		if(checkInlet(inlet))
		{
			//std::cout << "handling ..." << std::endl;
			switch(atom->getType())	{
				case LANG_BANG:
				if(inlet == "0")	{
						activate();
					}
					break;
				case LANG_LIST:
					receiveBody(inlet, dynamic_cast<LangList*>(atom));
					break;
			}
		}
	}

	void BodyProxObject::receiveBody(const std::string& inlet, Objsc::LangList* nbody)
	{
		id = parseInt(nbody->idx(2));
		x = parseFloat(nbody->idx(4));
		y = parseFloat(nbody->idx(5));
		//printf("receiveBody %d\n", nbody->size());
		//if(nbody->size() >3)	{
		if(parseInt(nbody->idx(1)) == ZONE_POINT)	{
			p = parseFloat(nbody->idx(10));
			//printf("prox %f\n", p);
		} else {
			p = 0.0f;
		}
		if(inlet == "0")	{
			activate();
		}
	}

	void BodyProxObject::activate()
	{
		send(0, new Objsc::LangInt(id));
		send(1, new Objsc::LangFloat(x));
		send(2, new Objsc::LangFloat(y));
		send(3, new Objsc::LangFloat(p));
	}

}
