/***************************************************************************
 *   Copyright (C) 2012 by Pierre Marchand   *
 *   pierre@oep-h.com   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#ifndef OBJSC_IMAGEOBJECT_H
#define OBJSC_IMAGEOBJECT_H

#include "LangMediaObject.hpp"

namespace Objsc {
	
	class ImageObject : public LangMediaObject
	{
		protected:
			virtual void loadMedia(const string& filename, int layer, bool show);
			void removeMedia();
			virtual void showMedia(int layer=0);
			virtual void hideMedia();
			void fullscreenMedia();
			void centreMedia();
			void sizeMedia(float width, float height);
			void widthMedia(float width);
			void heightMedia(float height);
			virtual void locMedia(float x, float y);
			virtual void locXMedia(float x);
			virtual void locYMedia(float y);
			void geomMedia(float x, float y,float width, float height);
			virtual void setLayerMedia(int layer);
			void setAlpha(float alpha);

			virtual void receiveList(std::string inlet, LangList* value);

		public:
			ImageObject()
			{
				setInlets(5);
				setOutlets(2);
			}
			~ImageObject();

			static const string langname;
			virtual const string& getLangName() const { return Objsc::ImageObject::langname; }

			virtual const string& getDisplayString()
			{
				if(displaystr.empty())	{
					return Objsc::ImageObject::langname;
				}
				return displaystr;
			}

			virtual void setLabel(string nlabel);

			virtual void receive(std::string inlet, Objsc::LangAtom* atom);
			virtual void activate();
	};


	class ImageObjectCreator : public LangObjectCreator
	{
		public:
			Objsc::LangObjectPtr create() const { return Objsc::LangObjectPtr(new Objsc::ImageObject());}
	};
	
} // namespace Objsc

#endif // OBJSC_IMAGEOBJECT_H
