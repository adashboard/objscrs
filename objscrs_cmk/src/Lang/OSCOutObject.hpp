#ifndef OBJSCRS_OSCOUTOBJECT_HPP
#define OBJSCRS_OSCOUTOBJECT_HPP
#include "../objscrLangObjects.hpp"
#include "../objscrOSC.hpp"
namespace Objsc
{
	/////////////////////////////////////
	// OSC OUT OBJECT
	/////////////////////////////////////
	class OSCOutObject : public LangObject
	{
		protected:
			Objsc::OSCGroupPtr 	oscgroup;
			string 				label;

			void listToMessage(LangList* value, ofxOscMessage* msgPtr);
			void itemToMessage(LangAtom* atom, ofxOscMessage* msgPtr);
			void varToMessage(LangVar* atom, ofxOscMessage* msgPtr);

		public:
			OSCOutObject() : oscgroup()
			{
				setInlets(1);
				setOutlets(0);
				min_args = 1;
			}
			~OSCOutObject() {}

			static const string langname;
			const string& getLangName() const { return Objsc::OSCOutObject::langname; }
			void setLabel(string nlabel);
			const string& getLabel() const { return label; }

			const string& getDisplayString()
			{
				if(displaystr.empty())	{
					return Objsc::OSCOutObject::langname;
				}
				return displaystr;
			}

			bool parseArgs(Objsc::LangList* args);
			void setGroup(const string& groupname);

			void receive(std::string inlet, Objsc::LangAtom* atom);
			void activate();
	};

	class OSCOutObjectCreator : public LangObjectCreator
	{
		public:
			Objsc::LangObjectPtr create() const { return Objsc::LangObjectPtr(new Objsc::OSCOutObject());}
	};

}
#endif
