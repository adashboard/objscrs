/***************************************************************************
 *   Copyright (C) 2012 by Pierre Marchand   *
 *   pierre@oep-h.com   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#include "IntObject.h"


#ifndef RINT
	#define RINT(x) ( (x)-floor(x)>0.5? ceil(x) : floor(x) )
#endif

namespace Objsc {
	const string Objsc::IntObject::langname = "int";

	void IntObject::eval(float input)
	{
		intvalue = RINT(input);
		send(0, new Objsc::LangInt(intvalue));
	}

	void IntObject::activate()
	{
		send(0, new Objsc::LangInt(intvalue));
	}

	void IntObject::setValue(float nvalue)
	{
		value = intvalue = RINT(nvalue);
		char cstr[64];
		sprintf(cstr, "%s %.3f", getLangName().c_str(), value);
		displaystr.assign(cstr);
		sprintf(cstr, "%.3f", value);
		label.assign(cstr);
	}
	
} // namespace Objsc
