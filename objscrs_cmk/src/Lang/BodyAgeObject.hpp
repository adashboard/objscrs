#ifndef OBJSCRS_BODYAGEOBJECT_HPP
#define OBJSCRS_BODYAGEOBJECT_HPP
#include "../objscrLangObjects.hpp"
namespace Objsc
{
	/////////////////////////////////////
	// BODY AGE OBJECT
	/////////////////////////////////////
	class BodyAgeObject : public LangObject
	{
		protected:
			int 		id;
			int 		age;

			void receiveBody(const std::string& inlet, LangList* nbody);

		public:
			BodyAgeObject() : id(0), age(0)
			{
				setInlets(2);
				setOutlets(2);
			}
			~BodyAgeObject() {}

			static const string langname;
			const string& getLangName() const { return Objsc::BodyAgeObject::langname; }

			const string& getDisplayString()
			{
				if(displaystr.empty())	{
					return Objsc::BodyAgeObject::langname;
				}
				return displaystr;
			}

			void receive(std::string inlet, Objsc::LangAtom* atom);
			void activate();
	};

	class BodyAgeObjectCreator : public LangObjectCreator
	{
		public:
			Objsc::LangObjectPtr create() const { return Objsc::LangObjectPtr(new Objsc::BodyAgeObject());}
	};

}
#endif
