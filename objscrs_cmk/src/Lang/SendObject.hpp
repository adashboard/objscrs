#ifndef OBJSCRS_SENDOBJECT_HPP
#define OBJSCRS_SENDOBJECT_HPP
#include "../objscrLangObjects.hpp"
namespace Objsc
{
	/////////////////////////////////////
	// SEND OBJECT
	/////////////////////////////////////
	class SendObject : public LangObject
	{
		protected:
			string 			label;
			string			recname;

		public:
			SendObject();
			~SendObject();

			static const string langname;
			const string& getLangName() const { return Objsc::SendObject::langname; }
			void setLabel(string nlabel);
			const string& getLabel() const { return label; }

			bool parseArgs(Objsc::LangList* args);

			void receive(std::string inlet, Objsc::LangAtom* atom);
	};

	class SendObjectCreator : public LangObjectCreator
	{
		public:
			Objsc::LangObjectPtr create() const { return Objsc::LangObjectPtr(new Objsc::SendObject());}
	};

}
#endif
