#include "GateObject.hpp"
namespace Objsc
{
	const string Objsc::GateObject::langname = "gate";
	/////////////////////////////////////
	// GATE OBJECT
	/////////////////////////////////////
	void GateObject::receive(std::string inlet, Objsc::LangAtom* atom)
	{
		if(inlet == "0")	{
			if(active)	{
				send(0, atom);
			}
		} else if(inlet == "1")	{
			switch(atom->getType())	{
				case LANG_BANG:
					break;
				case LANG_INT:
					if(parseBool(atom))	{
						open();
					} else {
						close();
					}
					break;
				case LANG_FLOAT:
					if(parseBool(atom))	{
						open();
					} else {
						close();
					}
					break;
				case LANG_LIST:
					receiveList(inlet, dynamic_cast<Objsc::LangList*>(atom));
					break;
			}
		}
	}

	void GateObject::receiveList(std::string inlet, LangList* value)
	{
		if(inlet == "1") {
			string cmd = parseString(value->idx(0), "");
			if(cmd.empty())	{
				parseInt(value->idx(0), 0)>0?open():close();
			} else {
				if(cmd == "open")	{
					open();
				} else if(cmd == "close")	{
					close();
				}
			}
		}
	}


}
