/***************************************************************************
 *   Copyright (C) 2012 by Pierre Marchand   *
 *   pierre@oep-h.com   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#ifndef OBJSC_VIDEOOBJECT_H
#define OBJSC_VIDEOOBJECT_H

#include "ImageObject.h"

#include "../objscrMediaViews.hpp"

namespace Objsc {
	
	class VideoObject : public ImageObject
	{
		protected:
			Objsc::VideoViewPtr		media;
			bool				playing;
			float				duration;
			float 				timeplayed;


			void loadMedia(const string& filename, int layer, bool show);

			void playMedia(float vol=0.0f, float speed=0.0f);
			void stopMedia();
			void showMedia(int layer=0);
			void hideMedia();
			void mediaEnded();
			void setVolume(float vol);
			void setSpeed(float speed);
			void setPan(float pan);

			void receiveList(std::string inlet, LangList* value);

		public:
			VideoObject() :
				media(),
				playing(false),
				duration(0.0f),
				timeplayed(0.0f)
			{
				setInlets(6);
				setOutlets(3);
			}
			~VideoObject();

			void update(int timeElapsed);

			static const string langname;
			virtual const string& getLangName() const { return Objsc::VideoObject::langname; }

			const string& getDisplayString()
			{
				if(displaystr.empty())	{
					return Objsc::VideoObject::langname;
				}
				return displaystr;
			}

			void setLabel(string nlabel);

			void receive(std::string inlet, Objsc::LangAtom* atom);
			void activate();
	};


	class VideoObjectCreator : public LangObjectCreator
	{
		public:
			Objsc::LangObjectPtr create() const { return Objsc::LangObjectPtr(new Objsc::VideoObject());}
	};

	
} // namespace Objsc

#endif // OBJSC_VIDEOOBJECT_H
