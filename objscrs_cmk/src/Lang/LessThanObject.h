/***************************************************************************
 *   Copyright (C) 2012 by Pierre Marchand   *
 *   pierre@oep-h.com   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#ifndef OBJSC_LESSTHANOBJECT_H
#define OBJSC_LESSTHANOBJECT_H

#include "MathObject.hpp"
namespace Objsc {
	
	class LessThanObject : public MathObject
	{
		protected:
			void eval(float input);

		public:
			LessThanObject()
			{
				setInlets(2);
				setOutlets(2);
			}
			~LessThanObject() {}

			static const string langname;
			const string& getLangName() const { return Objsc::LessThanObject::langname; }

			//void setValue(float nvalue);
	};

	class LessThanObjectCreator : public LangObjectCreator
	{
		public:
			Objsc::LangObjectPtr create() const { return Objsc::LangObjectPtr(new Objsc::LessThanObject());}
	};
	
} // namespace Objsc

#endif // OBJSC_LESSTHANOBJECT_H
