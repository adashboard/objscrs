#include "NumberObject.hpp"

#include <cstdio>

namespace Objsc
{
	const string Objsc::NumberObject::langname = "number";

	/////////////////////////////////////
	// NUMBER OBJECT
	/////////////////////////////////////
	void NumberObject::setDisplayString(const string& str)
	{
		setValue(atof(str.c_str()));
	}

	void NumberObject::receive(std::string inlet, Objsc::LangAtom* atom)
	{
		switch(atom->getType())	{
			case LANG_NULL:
				break;
			case LANG_BANG:
				activate();
				break;
			case LANG_INT:
				setValue(parseFloat(atom, 0.0f));
				if(inlet == "0")
					activate();
				break;
			case LANG_FLOAT:
				setValue(parseFloat(atom));
				if(inlet == "0")
					activate();
				break;
			case LANG_STRING:
				break;
			case LANG_VAR:
				break;
			case LANG_LIST:
				receiveList(inlet, dynamic_cast<Objsc::LangList*>(atom));
				break;
		}
	}

	void NumberObject::receiveList(std::string inlet, LangList* value)
	{
		setValue(parseFloat(value->idx(0), 0));
		if(inlet == "0")
			activate();
	}

	void NumberObject::activate()
	{
		if(observer)
			observer->objActivated();
		send(0, new Objsc::LangFloat(value));
	}

	void NumberObject::setValue(float nvalue)
	{
		value=nvalue;
		char cstr[32];
		sprintf(cstr,"%.3f", value);
		if(label.empty())	{
			displaystr.assign(cstr);
		} else {
			displaystr = label + " " + string(cstr);
		}
		if(observer)
			observer->objActivated();
	}

}
