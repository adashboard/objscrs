#include "ToggleObject.hpp"
namespace Objsc
{
	const string Objsc::ToggleObject::langname = "toggle";

	/////////////////////////////////////
	// TOGGLE OBJECT
	/////////////////////////////////////
	void ToggleObject::receive(std::string inlet, Objsc::LangAtom* atom)
	{
		switch(atom->getType())	{
			case LANG_NULL:
				break;
			case LANG_BANG:
				activate();
				break;
			case LANG_INT:
				if(parseInt(atom, 0)==0)	{
					turnOff();
				} else {
					turnOn();
				}
				break;
			case LANG_FLOAT:
				if(parseInt(atom, 0)==0)	{
					turnOff();
				} else {
					turnOn();
				}
				break;
			case LANG_STRING:
				break;
			case LANG_VAR:
				break;
			case LANG_LIST:
				receiveList("0", dynamic_cast<Objsc::LangList*>(atom));
				break;
		}
	}

	void ToggleObject::receiveList(std::string inlet, LangList* value)
	{
		if(parseInt(value->idx(0), 0) == 0)	{
			turnOff();
		} else {
			turnOn();
		}
	}

	void ToggleObject::turnOn()
	{
		on = true;
		if(observer)	{
			observer->objActivated();
		}
		send(0, new Objsc::LangInt(1));
	}

	void ToggleObject::turnOff()
	{
		on = false;
		if(observer)
			observer->objActivated();
		send(0, new Objsc::LangInt(0));
	}

	void ToggleObject::setObserver(Objsc::LangObserverPtr obsPtr)
	{
		observer=obsPtr;
	}
	void ToggleObject::removeObserver()
	{
		observer.reset();
	}

}
