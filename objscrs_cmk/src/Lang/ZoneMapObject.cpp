#include "ZoneMapObject.hpp"
#include "../objscrGUIRoot.hpp"
namespace Objsc
{
	const string Objsc::ZoneMapObject::langname = "zonemaps";

	/////////////////////////////////////
	// ZONE MAP OBJECT
	/////////////////////////////////////
	void ZoneMapObject::receive(std::string inlet, Objsc::LangAtom* atom)
	{
		if(checkInlet(inlet))
		{
			switch(atom->getType())	{
				case LANG_LIST:
				receiveList("0", dynamic_cast<Objsc::LangList*>(atom));
					break;
			}
		}
	}

	void ZoneMapObject::receiveList(std::string inlet, LangList* value)
	{
		string cmd = parseString(value->idx(0), "");
		if(cmd == "show")	{
			showZones();
		} else if(cmd == "hide")	{
			hideZones();
		} else if(cmd == "goto")	{
			gotoMap(parseString(value->idx(1), ""));
		}
	}

	void ZoneMapObject::showZones()
	{
		Objsc::GUIRoot::Current()->showZones();
		send(1, new Objsc::LangInt(1));
	}

	void ZoneMapObject::hideZones()
	{
		Objsc::GUIRoot::Current()->hideZones();
		send(1, new Objsc::LangInt(0));
	}

	void ZoneMapObject::gotoMap(const string& mapname)
	{
		if(!mapname.empty())	{
			Objsc::CommandPtr cmd = Objsc::CommandDispatcher::createCommand("_zonemapeditor_", "set_map");
			Objsc::PacketPtr data = cmd->getData();
			data->addArg(mapname);
			Objsc::CommandDispatcher::Current()->doCommand(cmd);
			send(0, new Objsc::LangBang());
		}
	}

}
