#ifndef OBJSCRS_LINEOBJECT_HPP
#define OBJSCRS_LINEOBJECT_HPP
#include "../objscrLangObjects.hpp"
namespace Objsc
{
	////////////////////////////////////////
	// LINE OBJECT
	////////////////////////////////////////
	class LineObject : public LangObject
	{
		protected:
			float		current;
			float 		begin;
			float 		end;
			float 		step;
			int			interval;
			int			tframe;
			int 			count;
			bool		active;

			void receiveList(std::string inlet, LangList* value);

		public:
			LineObject();
			~LineObject();

			static const string langname;
			const string& getLangName() const { return Objsc::LineObject::langname; }

			bool parseArgs(Objsc::LangList* args);

			void receive(std::string inlet, Objsc::LangAtom* atom);
			virtual void activate();
			void update(int timeframe);

			void setInterval(int value);
			void setBeginValue(int value);
			void setEndValue(int value);
			void reset();
			void start();
			void stop() { active=false; }

			const bool hasArgs() const { return true;}
			string getArgString() { return intToString(interval); }
	};

	class LineObjectCreator : public LangObjectCreator
	{
		public:
			Objsc::LangObjectPtr create() const { return Objsc::LangObjectPtr(new Objsc::LineObject());}
	};

}
#endif
