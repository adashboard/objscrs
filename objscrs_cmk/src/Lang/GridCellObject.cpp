#include "GridCellObject.hpp"
#include "../objscrGeom.hpp"

namespace Objsc
{
	const string Objsc::GridCellObject::langname = "grid_cell";

	/////////////////////////////////////
	// GRID CELL OBJECT
	/////////////////////////////////////
	bool GridCellObject::parseArgs(Objsc::LangList* args)
	{
		if(args->size() < min_args)	{
			return false;
		} else {
			col = MAX(0, getItemAsInt(args, 0)-1);
			row = MAX(0, getItemAsInt(args, 1)-1);
			count = 0;
			std::stringstream tmp;
			tmp << col+1 << " " << row+1;
			setLabel( tmp.str());
			return true;
		}
	}

	void GridCellObject::setLabel(string nlabel)
	{
		label.assign(nlabel);
		displaystr = Objsc::GridCellObject::langname + " " + label;
	}

	const string& GridCellObject::getDisplayString()
	{
		if(displaystr.empty())	{
			return Objsc::GridCellObject::langname;
		}
		return displaystr;
	}

	void GridCellObject::receive(std::string inlet, Objsc::LangAtom* atom)
	{
		if(checkInlet(inlet))
		{
			switch(atom->getType())	{
				case LANG_BANG:
				if(inlet == "0")	{
						activate();
					}
					break;
				case LANG_LIST:
					receiveList(inlet, dynamic_cast<LangList*>(atom));
					break;
			}
		}
	}

	void GridCellObject::receiveList(std::string inlet, LangList* value)
	{
		if (parseInt(value->idx(ZONE_DATA_TYPE_INDEX), ZONE_DATA_NONE) == ZONE_DATA_CELLS)	{
			count = 0;
			idx = ZONE_DATA_INDEX + (row*parseInt(value->idx(3), 0)) + col;
			if (idx < parseInt(value->idx(3), 0)*parseInt(value->idx(4), 0))	{
				count = parseInt(value->idx(idx), 0);
				if(inlet == "0")	{
					activate();
				}
			}
		}
	}

	void GridCellObject::activate()
	{
		send(0, new Objsc::LangInt(count));
		send(1, new Objsc::LangInt(CLAMP(count,0,1)));
	}

}
