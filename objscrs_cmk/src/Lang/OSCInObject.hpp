#ifndef OBJSCRS_OSCINOBJECT_HPP
#define OBJSCRS_OSCINOBJECT_HPP
#include "../objscrLangObjects.hpp"
namespace Objsc
{
	/////////////////////////////////////
	// OSC IN OBJECT
	/////////////////////////////////////
	class OSCInObject : public LangObject
	{
		protected:
			string 			label;

			void receiveList(std::string inlet, Objsc::LangList* value);

		public:
			OSCInObject()
			{
				setInlets(0);
				setOutlets(1);
			}
			~OSCInObject() {}

			static const string langname;
			const string& getLangName() const { return Objsc::OSCInObject::langname; }
			void setLabel(string nlabel);
			const string& getLabel() const { return label; }

			const string& getDisplayString()
			{
				if(displaystr.empty())	{
					return Objsc::OSCInObject::langname;
				}
				return displaystr;
			}

			bool parseArgs(Objsc::LangList* args);

			void receive(std::string inlet, Objsc::LangAtom* atom);
			//void activate();

			bool hasReceiverName() { return true; }
			const string& getReceiverName() const { return Objsc::OSCInObject::langname; }
	};

	class OSCInObjectCreator : public LangObjectCreator
	{
		public:
			Objsc::LangObjectPtr create() const { return Objsc::LangObjectPtr(new Objsc::OSCInObject());}
	};

}
#endif
