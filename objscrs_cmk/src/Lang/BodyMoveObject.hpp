#ifndef OBJSCRS_BODYMOVEOBJECT_HPP
#define OBJSCRS_BODYMOVEOBJECT_HPP
#include "../objscrLangObjects.hpp"
namespace Objsc
{

	/////////////////////////////////////
	// BODY MOVE OBJECT
	/////////////////////////////////////
	class BodyMoveObject : public LangObject
	{
		protected:
			int 		id;
			float 	x;
			float 	y;
			float 	pX;
			float 	pY;
			float 	velX;
			float 	velY;
			float	vel;
			float	dist;
			float	pT;
			float	t;

			void receiveBody(const std::string& inlet, LangList* nbody);

		public:
			BodyMoveObject() : id(0), x(0.0f), y(0.0f), pX(0.0f), pY(0.0f), velX(0.0f), velY(0.0f), vel(0.0), dist(0.0f), pT(0.0f), t(0.0f)
			{
				setInlets(2);
				setOutlets(5);
			}
			~BodyMoveObject() {}

			static const string langname;
			const string& getLangName() const { return Objsc::BodyMoveObject::langname; }

			const string& getDisplayString()
			{
				if(displaystr.empty())	{
					return Objsc::BodyMoveObject::langname;
				}
				return displaystr;
			}

			void receive(std::string inlet, Objsc::LangAtom* atom);
			void activate();
	};


	class BodyMoveObjectCreator : public LangObjectCreator
	{
		public:
			Objsc::LangObjectPtr create() const { return Objsc::LangObjectPtr(new Objsc::BodyMoveObject());}
	};
}
#endif
