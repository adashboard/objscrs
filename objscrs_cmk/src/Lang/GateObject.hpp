#ifndef OBJSCRS_GATEOBJECT_HPP
#define OBJSCRS_GATEOBJECT_HPP
#include "../objscrLangObjects.hpp"
namespace Objsc
{

	/////////////////////////////////////
	// GATE OBJECT
	/////////////////////////////////////
	class GateObject : public LangObject
	{
		protected:
			bool			active;

			void receiveList(std::string inlet, LangList* value);
			void open() { active=true; }
			void close() { active=false; }

		public:
			GateObject() : active(false)
			{
				setInlets(2);
				setOutlets(1);
			}
			~GateObject() {}

			static const string langname;
			const string& getLangName() const { return Objsc::GateObject::langname; }

			void receive(std::string inlet, Objsc::LangAtom* atom);
	};

	class GateObjectCreator : public LangObjectCreator
	{
		public:
			Objsc::LangObjectPtr create() const { return Objsc::LangObjectPtr(new Objsc::GateObject());}
	};

}
#endif
