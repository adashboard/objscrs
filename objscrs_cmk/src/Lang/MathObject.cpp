#include "MathObject.hpp"
namespace Objsc
{

	/////////////////////////////////////
	// MATH OBJECT
	/////////////////////////////////////
	MathObject::MathObject() : value(0.0f), label() {}

	bool MathObject::parseArgs(Objsc::LangList* args)
	{
		if(args->size() < min_args)	{
			return false;
		} else {
			//setValue(getItemAsFloat(args, 1));
			setValue(getItemAsFloat(args, 0));
			return true;
		}
	}

	void MathObject::setLabel(string nlabel)
	{
		setValue((float)atof(nlabel.c_str()));
	}

	void MathObject::setValue(float nvalue)
	{
		value = nvalue;
		char cstr[64];
		sprintf(cstr, "%s %.3f", getLangName().c_str(), value);
		displaystr.assign(cstr);
		sprintf(cstr, "%.3f", value);
		label.assign(cstr);
	}

	void MathObject::receive(std::string inlet, Objsc::LangAtom* atom)
	{
		switch(atom->getType())	{
			case LANG_NULL:
				//send(1, new Objsc::LangBang());
				break;
			case LANG_BANG:
				activate();
				break;
			case LANG_INT:
			if(inlet == "0")
			{
						eval(parseInt(atom));
			}
			else if(inlet == "1")
			{
						setValue(parseInt(atom));
			}

				break;
			case LANG_FLOAT:
			if(inlet == "0")
			{
						eval(parseFloat(atom));
			}
			else if(inlet == "1")
			{
						setValue(parseFloat(atom));
			}
				break;
			case LANG_STRING:
				//send(1, new Objsc::LangBang());
				break;
			case LANG_VAR:
				//send(1, new Objsc::LangBang());
				break;
			case LANG_LIST:
				receiveList(inlet, dynamic_cast<Objsc::LangList*>(atom));
				break;
		}
	}

	void MathObject::receiveList(std::string inlet, LangList* value)
	{
		if(inlet == "0")
			eval(parseFloat(value->idx(0), 0.0f));
		else if(inlet == "1")
			setValue(parseFloat(value->idx(0), 0.0f));

	}

}
