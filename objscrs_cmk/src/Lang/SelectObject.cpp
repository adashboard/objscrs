#include "SelectObject.hpp"
namespace Objsc
{
	const string Objsc::SelectObject::langname = "select";

	/////////////////////////////////////
	// SELECT OBJECT
	/////////////////////////////////////
	bool SelectObject::parseArgs(Objsc::LangList* args)
	{
		if(args->size() < min_args)	{
			return false;
		} else {
			//keys.copy(args, 1);
			keys.copy(args);
			if(label.empty())	{
				displaystr = Objsc::SelectObject::langname + " " + listToString(&keys);
			} else {
				displaystr.assign(label.c_str());
				displaystr.append(" ");
				displaystr.append(listToString(&keys));
			}
			setOutlets(keys.size()+1);
			return true;
		}
	}


	void SelectObject::receive(std::string inlet, Objsc::LangAtom* atom)
	{
		switch(atom->getType())	{
			case LANG_NULL:
				break;
			case LANG_BANG:
				break;
			case LANG_INT:
				select(atom);
				break;
			case LANG_FLOAT:
				select(atom);
				break;
			case LANG_STRING:
				select(atom);
				break;
			case LANG_VAR:
				break;
			case LANG_LIST:
				//printf("SelectObject::receive LIST\n");
				select(dynamic_cast<Objsc::LangList*>(atom)->idx(0));
				break;
		}
	}

	void SelectObject::select(Objsc::LangAtom* atom)
	{
		int index = keys.find(atom);
		if(index == -1)	{
			send(num_outlets-1, atom);
		} else {
			send(index, new Objsc::LangBang());
		}
	}

}
