#ifndef OBJSCRS_PRINTOBJECT_HPP
#define OBJSCRS_PRINTOBJECT_HPP
#include "../objscrLangObjects.hpp"
namespace Objsc
{
	/////////////////////////////////////
	// PRINT OBJECT
	/////////////////////////////////////
	class PrintObject : public LangObject
	{
		protected:
			string buffer;
			string label;

			void receiveList(LangList* value)
			{
				for(LangAtomIter atom = value->begin(); atom != value->end(); ++atom) {
					parseValue((*atom));
					buffer.append(" ");
				}
			}
			virtual void parseValue(LangAtom* atom)
			{
				switch(atom->getType())	{
					case LANG_NULL:
						break;
					case LANG_BANG:
						buffer.append("bang!");
						break;
					case LANG_INT:
						buffer.append(intToString(parseInt(atom)));
						break;
					case LANG_FLOAT:
						buffer.append(floatToString(parseFloat(atom)));
						break;
					case LANG_STRING:
						buffer.append(parseString(atom));
						break;
					case LANG_LIST:
						receiveList(dynamic_cast<Objsc::LangList*>(atom));
						break;
					case LANG_VAR:
						buffer.append(varToString(dynamic_cast<Objsc::LangVar*>(atom)));
						break;
				}
			}

		public:
			PrintObject() : buffer(), label()
			{
				setInlets(1);
				setOutlets(0);
				max_args = 1;
			}
			~PrintObject() {}

			static const string langname;
			const string& getLangName() const { return Objsc::PrintObject::langname; }

			bool parseArgs(Objsc::LangList* args);

			virtual void setLabel(string nlabel);
			virtual const string& getLabel() const { return label; }
			const string& getDisplayString();

			void receive(std::string inlet, Objsc::LangAtom* atom);
			void send(int index, LangAtom* atom) {}
			virtual void activate();
	};

	class PrintObjectCreator : public LangObjectCreator
	{
		public:
			Objsc::LangObjectPtr create() const { return Objsc::LangObjectPtr(new Objsc::PrintObject());}
	};

}
#endif
