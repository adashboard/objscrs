#include "SoundObject.hpp"
namespace Objsc
{
	const string Objsc::SoundObject::langname = "snd";

	/////////////////////////////////////
	// SOUND OBJECT
	/////////////////////////////////////
	SoundObject::SoundObject() :
				label(),
				sound()
	{
		setInlets(4);
		setOutlets(3);
		min_args = 1;
	}

	SoundObject::~SoundObject()
	{

	}


	void SoundObject::receive(std::string inlet, Objsc::LangAtom* atom)
	{
		switch(atom->getType())	{
			case LANG_BANG:
			if(inlet == "0")	{
					activate();
				}
				break;
			case LANG_INT:
			if(inlet == "0")
			{
						if(parseBool(atom))	{
							start();
						} else {
							stop();
						}
				}
				break;
			case LANG_FLOAT:
			if(inlet == "0")	{
						if(parseBool(atom))	{
							start();
						} else {
							stop();
						}
			}
			else if(inlet == "1")
						sound.setVolume((int)(100 * parseFloat(atom)));
			else if(inlet == "2")
						sound.setSpeed(parseFloat(atom));

				break;
			case LANG_LIST:
				receiveList(inlet, dynamic_cast<Objsc::LangList*>(atom));
				break;
		}
	}

	void SoundObject::receiveList(std::string inlet, LangList* value)
	{
		if(inlet == "0") {
			string cmd = parseString(value->idx(0), "");
			if(cmd.empty())	{
				parseInt(value->idx(0), 0)>0?start():stop();
			} else {
				if(cmd == "start")	{
					start();
				} else if(cmd == "stop")	{
					stop();
				} else if(cmd == "play")	{
					start();
				//~ } else if(cmd == "amp")	{
					//~ getAmp();
				} else if(cmd == "load")	{
					if(value->size() > 1)	{
						sound.loadSound(parseString(value->idx(1), ""));
					}
				//~ } else if(cmd == "loops")	{
					//~ if(value->size() > 1)	{
						//~ sound.setLoops(parseInt(value->idx(1), 1));
					//~ }
				} else if(cmd == "vol")	{
					if(value->size() > 1)	{
						sound.setVolume((int)(100 * parseFloat(value->idx(1), 0.0f)));
					}
				} else if(cmd == "speed")	{
					if(value->size() > 1)	{
						sound.setSpeed(parseFloat(value->idx(1), 1.0f));
					}
				//~ } else if(cmd == "pan")	{
					//~ if(value->size() > 1)	{
						//~ sound.setPan(parseFloat(value->idx(1), 0.5f));
					//~ }
				}
			}
		} else if(inlet == "1")	{
			sound.setVolume((int)(100 * parseFloat(value->idx(0), 0.0f)));
		} else if(inlet == "2")	{
			sound.setSpeed(parseFloat(value->idx(0), 1.0f));
		//~ } else if(inlet == 3)	{
			//~ sound.setPan(parseFloat(value->idx(0), 0.5f));
		}
	}


	void SoundObject::start()
	{
		sound.play();
	}

	void SoundObject::stop()
	{
		sound.stop();
		//send(2, new Objsc::LangInt(0));
	}

	void SoundObject::activate()
	{
		start();
	}

	void SoundObject::update(int timeElapsed)
	{
		sound.update();
	}

	//~ void SoundObject::getAmp()
	//~ {
		//~ send(0, new Objsc::LangFloat(sound.getAmp()));
	//~ }

	bool SoundObject::parseArgs(Objsc::LangList* args)
	{
		if(args->size() < min_args)	{
			return false;
		} else {
			//setLabel(getItemAsString(args, 1));
			setLabel(getItemAsString(args, 0));
			return true;
		}
	}

	void SoundObject::setLabel(string nlabel)
	{
		label.assign(nlabel);
		displaystr.assign(getLangName() + " " + label);
		//~ sound = Objsc::SoundMixer::Current()->createSound(label, getLangName());
		//~ sound.addListener(SoundListenerPtr(this));
	}


	void SoundObject::receiveSoundEvent(SoundEvent& event)
	{
		//printf("SoundObject::receiveSoundEvent %d\n", event.type);
		switch(event.type)	{
			case SNDSTART:
				send(1, new Objsc::LangBang());
				send(2, new Objsc::LangInt(1));
				break;
			case SNDENDED:
				send(2, new Objsc::LangInt(0));
				break;
			case SNDNEWLOOP:
				send(1, new Objsc::LangBang());
				break;
		}
	}

}
