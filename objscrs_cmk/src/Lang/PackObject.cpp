#include "PackObject.hpp"

#include "boost/lexical_cast.hpp"

namespace Objsc
{
	const string Objsc::PackObject::langname = "pack";

	/////////////////////////////////////
	// PACK OBJECT
	/////////////////////////////////////
	bool PackObject::parseArgs(Objsc::LangList* args)
	{
		//printf("PackObject::parseArgs START %d\n", args->size());
		if(args->size() < min_args)	{
			return false;
		} else {
			//keys.copy(args, 1);
			keys.copy(args);
			input.copy(args);
			if(label.empty())	{
				//printf("PackObject::parseArgs <%s> A\n", displaystr.c_str());
				displaystr.assign(getLangName().c_str());
				displaystr.append(" ");
				displaystr.append(listToString(&keys));
			} else {
				displaystr.assign(label.c_str());
				displaystr.append(" ");
				displaystr.append(listToString(&keys));
			}
			//printf("PackObject::parseArgs <%s>\n", displaystr.c_str());
			setInlets(keys.size());
			//printf("PackObject::parseArgs DONE %d\n", keys.size());
			return true;
		}
	}

	void PackObject::receive(std::string inlet, Objsc::LangAtom* atom)
	{
		switch(atom->getType())	{
			case LANG_NULL:
				break;
			case LANG_BANG:
				input.set(boost::lexical_cast<int>(inlet), atom);
				if(inlet == "0")	{
					activate();
				}
				break;
			case LANG_INT:
				input.set(boost::lexical_cast<int>(inlet), atom);
				if(inlet == "0")	{
					activate();
				}
				break;
			case LANG_FLOAT:
				input.set(boost::lexical_cast<int>(inlet), atom);
				if(inlet == "0")	{
					activate();
				}
				break;
			case LANG_STRING:
				input.set(boost::lexical_cast<int>(inlet), atom);
				if(inlet == "0")	{
					activate();
				}
				break;
			case LANG_VAR:
				break;
			case LANG_LIST:
				receiveList(inlet, dynamic_cast<Objsc::LangList*>(atom));
				if(inlet == "0")	{
					activate();
				}
				break;
		}
	}


	void PackObject::activate()
	{
		for(int i = 0; i < num_inlets; ++i)	{
			//printf("types: %d %d\n", atom->idx(i)->getType(), keys.idx(i)->getType());
			if(input.idx(i)->getType() != keys.idx(i)->getType())	{
				input.set(i, keys.idx(i));
			}
		}
		send(0, &input);
		input.copy(&keys);
	}

	void PackObject::receiveList(std::string inlet, Objsc::LangList* atom)
	{
		int idx(boost::lexical_cast<int>(inlet));
		if(idx > -1 && idx < num_inlets)	{
			input.set(idx, atom->idx(0));
		}
	}

}
