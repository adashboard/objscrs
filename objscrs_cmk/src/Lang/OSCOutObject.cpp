#include "OSCOutObject.hpp"
namespace Objsc
{
	const string Objsc::OSCOutObject::langname = "osc_out";
	/////////////////////////////////////
	// OSC OUT OBJECT
	/////////////////////////////////////
	bool OSCOutObject::parseArgs(Objsc::LangList* args)
	{
		if(args->size() < min_args)	{
			return false;
		} else {
			setLabel(getItemAsString(args, 0));
			return true;
		}
	}

	void OSCOutObject::setLabel(string nlabel)
	{
		label.assign(nlabel);
		oscgroup = Objsc::OSCComms::Current()->createGroup(nlabel);
		displaystr = Objsc::OSCOutObject::langname + " " + label;
	}

	void OSCOutObject::receive(std::string inlet, Objsc::LangAtom* atom)
	{
		ofxOscMessage msg;
		switch(atom->getType())	{
			case LANG_NULL:
				break;
			case LANG_BANG:
				msg.setAddress( "/" +  oscgroup->getName());
				msg.addIntArg(1);
				oscgroup->send(msg);
				break;
			case LANG_INT:
				msg.setAddress( "/" +  oscgroup->getName());
				msg.addIntArg(parseInt(atom));
				oscgroup->send(msg);
				break;
			case LANG_FLOAT:
				msg.setAddress( "/" +  oscgroup->getName());
				msg.addFloatArg(parseFloat(atom));
				oscgroup->send(msg);
				break;
			case LANG_STRING:
				msg.setAddress( "/" +  oscgroup->getName());
				msg.addStringArg(parseString(atom));
				oscgroup->send(msg);
				break;
			case LANG_VAR:
				msg.setAddress( "/" +  oscgroup->getName());
				varToMessage(dynamic_cast<Objsc::LangVar*>(atom),&msg);
				oscgroup->send(msg);
				break;
			case LANG_LIST:
				msg.setAddress( "/" +  oscgroup->getName());
				listToMessage(dynamic_cast<Objsc::LangList*>(atom),&msg);
				oscgroup->send(msg);
				break;
		}
	}

	void OSCOutObject::listToMessage(LangList* value, ofxOscMessage* msgPtr)
	{
		for(LangAtomIter atom = value->begin(); atom != value->end(); ++atom) {
			itemToMessage((*atom), msgPtr);
		}
	}

	void OSCOutObject::itemToMessage(LangAtom* atom, ofxOscMessage* msgPtr)
	{
		switch(atom->getType())	{
			case LANG_NULL:
				break;
			case LANG_BANG:
				msgPtr->addIntArg(1);
				break;
			case LANG_INT:
				msgPtr->addIntArg(parseInt(atom));
				break;
			case LANG_FLOAT:
				msgPtr->addFloatArg(parseFloat(atom));
				break;
			case LANG_STRING:
				msgPtr->addStringArg(parseString(atom));
				break;
			case LANG_LIST:
				listToMessage(dynamic_cast<Objsc::LangList*>(atom),msgPtr);
				break;
			case LANG_VAR:
				varToMessage(dynamic_cast<Objsc::LangVar*>(atom), msgPtr);
				break;
		}
	}

	void OSCOutObject::varToMessage(LangVar* atom, ofxOscMessage* msgPtr)
	{
		switch(atom->getValueType())	{
			case LANG_NULL:
				break;
			case LANG_BANG:
				msgPtr->addIntArg(1);
				break;
			case LANG_INT:
				msgPtr->addIntArg(getVarInt(atom));
				break;
			case LANG_FLOAT:
				msgPtr->addFloatArg(getVarFloat(atom));
				break;
			case LANG_STRING:
				msgPtr->addStringArg(getVarString(atom));
				break;
			case LANG_LIST:
				listToMessage(dynamic_cast<Objsc::LangList*>(atom),msgPtr);
				break;
			case LANG_VAR:
				// ignore
				break;
		}
	}

	void OSCOutObject::activate()
	{

	}


}
