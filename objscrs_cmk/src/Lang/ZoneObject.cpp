#include "ZoneObject.hpp"
namespace Objsc
{
	const string Objsc::ZoneObject::langname = "zone_sender";

	/////////////////////////////////////
	// ZONE OBJECT
	/////////////////////////////////////
	void ZoneObject::receive(std::string inlet, Objsc::LangAtom* atom)
	{
		if(checkInlet(inlet))
		{
			//std::cout << "handling ..." << std::endl;
			switch(atom->getType())	{
				case LANG_BANG:
					activate();
					break;
				case LANG_LIST:
					receiveBody(inlet, dynamic_cast<LangList*>(atom));
					break;
			}
		}
	}

	void ZoneObject::receiveBody(const std::string& inlet, Objsc::LangList* nbody)
	{
		//printf("ZoneObject::receiveBody A\n");
		body.clear();
		int zonetype = parseInt(nbody->idx(1));
		//printf("ZoneObject::receiveBody 0\n");
		body.add(parseString(nbody->idx(0)));
		//printf("ZoneObject::receiveBody 1\n");
		body.add(zonetype);
		//printf("ZoneObject::receiveBody 2\n");
		body.add(parseInt(nbody->idx(2)));
		//printf("ZoneObject::receiveBody 3\n");
		body.add(parseInt(nbody->idx(3)));
		//printf("ZoneObject::receiveBody 4\n");
		body.add(parseFloat(nbody->idx(4)));
		//printf("ZoneObject::receiveBody 5\n");
		body.add(parseFloat(nbody->idx(5)));
		//printf("ZoneObject::receiveBody 6\n");
		body.add(parseFloat(nbody->idx(6)));
		//printf("ZoneObject::receiveBody 7\n");
		body.add(parseFloat(nbody->idx(7)));
		//printf("ZoneObject::receiveBody 8\n");
		body.add(parseFloat(nbody->idx(8)));
		//printf("ZoneObject::receiveBody 9\n");
		body.add(parseFloat(nbody->idx(9)));
		//printf("ZoneObject::receiveBody all\n");
		//numbodies = parseInt(nbody->idx(7));
		//numbodies = -1; // TMP!
		//printf("receiveBody %d\n", nbody->size());

		if(zonetype == ZONE_POINT)	{
			body.add(parseFloat(nbody->idx(10)));
		}

		if(inlet == "0")	{
			//printf("ZoneObject::receiveBody activate\n");
			activate();
			//printf("ZoneObject::receiveBody activate done\n");
		}
	}

	void ZoneObject::activate()
	{
		send(0, new Objsc::LangBang());
		//send(1, new Objsc::LangInt(numbodies));
		//send(2, new Objsc::LangList(body));
		send(2, new Objsc::LangList(body));
	}

}
