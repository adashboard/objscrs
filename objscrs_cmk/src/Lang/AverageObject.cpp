#include "AverageObject.hpp"
namespace Objsc
{
	const string Objsc::AverageObject::langname = "avg";

	/////////////////////////////////////
	// AVERAGE OBJECT
	/////////////////////////////////////
	AverageObject::AverageObject() :
		sum(0.0f),
		avg(0.0f),
		count(0.0f)
	{
		setInlets(2);
		setOutlets(3);
	}

	void AverageObject::receive(std::string inlet, Objsc::LangAtom* atom)
	{
		switch(atom->getType())	{
		case LANG_NULL:
			break;
		case LANG_BANG:
			if(inlet == "0")	{
				activate();
			}
			break;
		case LANG_INT:
			if(inlet == "1")	{
				add(parseInt(atom));
			}
			break;
		case LANG_FLOAT:
			if(inlet == "1")	{
				add(parseFloat(atom));
			}
			break;
		case LANG_STRING:
			break;
		case LANG_VAR:
			break;
		case LANG_LIST:
			if(inlet == "0")	{
				receiveList("0", dynamic_cast<Objsc::LangList*>(atom));
			}
			break;
		}
	}

	void AverageObject::receiveList(std::string inlet, LangList* value)
	{
		string cmd = parseString(value->idx(0), "");
		if(cmd == "reset")	{
			reset();
		} else if(cmd == "count")	{
			send(1, new Objsc::LangFloat(count));
		} else if(cmd == "sum")	{
			send(1, new Objsc::LangFloat(sum));
		}
	}

	void AverageObject::activate()
	{
		send(0, new Objsc::LangFloat(avg));
	}

	void AverageObject::reset()
	{
		sum = 0.0f;
		count = 0.0f;
		avg = 0.0f;
		send(2, new Objsc::LangFloat(avg));
	}

	void AverageObject::add(float value)
	{
		sum += value;
		++count;
		avg = sum/count;
		send(2, new Objsc::LangFloat(avg));
	}
}
