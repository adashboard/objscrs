#include "DelayObject.hpp"
#include "../objscrGeom.hpp"

namespace Objsc
{

	const string Objsc::DelayObject::langname = "delay";

	////////////////////////////////////////
	// DELAY OBJECT
	////////////////////////////////////////
	DelayObject::DelayObject() :
		interval(200),
		count(0),
		active(false)
	{
		setInlets(2);
		setOutlets(1);
	}

	DelayObject::~DelayObject()
	{

	}

	bool DelayObject::parseArgs(Objsc::LangList* args)
	{
		//~ if(args->size() > 1)	{
			//~ setInterval(getItemAsInt(args, 1));
		if(!args->empty())	{
			setInterval(getItemAsInt(args, 0));
			char cstr[64];
			sprintf(cstr, "%s %d", getLangName().c_str(), interval);
			displaystr.assign(cstr);
		}
		return true;
	}

	void DelayObject::receive(std::string inlet, Objsc::LangAtom* atom)
	{
		switch(atom->getType())	{
			case LANG_BANG:
				break;
			case LANG_INT:
			if(inlet == "0")	{
						if(parseBool(atom))	{
							start();
						} else {
							stop();
						}
			}
			else if(inlet == "1")
						setInterval(parseInt(atom));

				break;
			case LANG_FLOAT:
			if(inlet == "0")	{
						if(parseBool(atom))	{
							start();
						} else {
							stop();
						}
			}
			else if(inlet == "1")
						setInterval(parseFloat(atom));

				break;
			case LANG_LIST:
				receiveList(inlet, dynamic_cast<Objsc::LangList*>(atom));
				break;
		}
	}

	void DelayObject::receiveList(std::string inlet, LangList* value)
	{
		if(inlet == "0") {
			string cmd = parseString(value->idx(0), "");
			if(cmd.empty())	{
				parseInt(value->idx(0), 0)>0?start():stop();
			} else {
				if(cmd == "start")	{
					start();
				} else if(cmd == "stop")	{
					stop();
				}
			}
		} else if(inlet == "1") {
			setInterval(parseInt(value->idx(0), 200));
		}
	}

	void DelayObject::start()
	{
		active=true;
		count=0;
	}

	void DelayObject::setInterval(int value)
	{
		interval=MAX(0, value);
	}

	void DelayObject::update(int timeframe)
	{
		if(active)	{
			count += timeframe;
			if(interval > 0 && count >= interval)
				activate();
		}
	}

	void DelayObject::activate()
	{
		count=0;
		send(0, new Objsc::LangBang());
	}
}
