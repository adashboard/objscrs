#ifndef OBJSCRS_RECEIVEOBJECT_HPP
#define OBJSCRS_RECEIVEOBJECT_HPP
#include "../objscrLangObjects.hpp"
namespace Objsc
{

	/////////////////////////////////////
	// RECEIVE OBJECT
	/////////////////////////////////////
	class ReceiveObject : public LangObject
	{
		protected:
			string 			label;


		public:
			ReceiveObject();
			~ReceiveObject();

			static const string langname;
			const string& getLangName() const { return Objsc::ReceiveObject::langname; }
			void setLabel(string nlabel);
			const string& getLabel() const { return label; }

			bool parseArgs(Objsc::LangList* args);

			void receive(std::string inlet, Objsc::LangAtom* atom);

			bool hasReceiverName() { return true; }
			const string& getReceiverName() const { return label; }
	};


	class ReceiveObjectCreator : public LangObjectCreator
	{
		public:
			Objsc::LangObjectPtr create() const { return Objsc::LangObjectPtr(new Objsc::ReceiveObject());}
	};


}
#endif
