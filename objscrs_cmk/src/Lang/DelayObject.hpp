#ifndef OBJSCRS_DELAYOBJECT_HPP
#define OBJSCRS_DELAYOBJECT_HPP
#include "../objscrLangObjects.hpp"
namespace Objsc
{

	////////////////////////////////////////
	// DELAY OBJECT
	////////////////////////////////////////
	class DelayObject : public LangObject
	{
		protected:
			int			interval;
			int 			count;
			bool		active;

			void receiveList(std::string inlet, LangList* value);

		public:
			DelayObject();
			~DelayObject();

			static const string langname;
			const string& getLangName() const { return Objsc::DelayObject::langname; }

			bool parseArgs(Objsc::LangList* args);

			void receive(std::string inlet, Objsc::LangAtom* atom);
			virtual void activate();
			void update(int timeframe);

			void setInterval(int value);
			void start();
			void stop() { active=false; }

			const bool hasArgs() const { return true;}
			string getArgString() { return intToString(interval); }
	};

	class DelayObjectCreator : public LangObjectCreator
	{
		public:
			Objsc::LangObjectPtr create() const { return Objsc::LangObjectPtr(new Objsc::DelayObject());}
	};
}
#endif
