
#include "DrawObject.hpp"
#include "../objscrMediaPlayer.hpp"

namespace Objsc
{
	const string Objsc::DrawObject::langname = "draw";

	/////////////////////////////////////
	// DRAW LINE OBJECT
	/////////////////////////////////////
	bool DrawObject::parseArgs(Objsc::LangList* args)
	{
		if(args->size() < min_args)	{
			return false;
		} else {
			setLabel(getItemAsString(args, 0));
			return true;
		}
	}

	void DrawObject::setLabel(string nlabel)
	{
		label.assign(nlabel);
		displaystr = Objsc::DrawObject::langname + " " + label;
		graphic = std::tr1::static_pointer_cast<Objsc::DrawView>(Objsc::MediaPlayer::Current()->createDrawing(label));
	}

	void DrawObject::receive(std::string inlet, Objsc::LangAtom* atom)
	{
		switch(atom->getType())	{
			case LANG_NULL:
				//send(1, new Objsc::LangBang());
				break;
			case LANG_BANG:
				activate();
				break;
			case LANG_INT:

				break;
			case LANG_FLOAT:

				break;
			case LANG_STRING:
				//send(1, new Objsc::LangBang());
				break;
			case LANG_VAR:
				//send(1, new Objsc::LangBang());
				break;
			case LANG_LIST:
				receiveList(inlet, dynamic_cast<Objsc::LangList*>(atom));
				break;
		}
	}

	void DrawObject::receiveList(std::string inlet, LangList* value)
	{
		string cmd = parseString(value->idx(0), "");
		if(cmd.empty()) {
			int total = value->size();
			total -= total % 2;
			for(int x = 0; x < total; x += 2)	{
				graphic->addPoint(parseFloat(value->idx(x), 0.0f),parseFloat(value->idx(x+1), 0.0f));
			}
		} else {
			if(cmd == "show")	{
				graphic->show();
			} else if(cmd == "hide")	{
				graphic->hide();
			} else if(cmd == "clear")	{
				graphic->clearPoints();
			} else if(cmd == "layer")	{
				if(value->size() > 1)	{
					setLayerMedia(parseInt(value->idx(1), 0));
				}
			} else if(cmd == "limit")	{
				if(value->size() > 1)	{
					graphic->setMaxPoints(parseInt(value->idx(1), 0));
				}
			} else if(cmd == "rgb")	{
				if(value->size() > 4)	{
					graphic->setForeColour(parseInt(value->idx(1), 255), parseInt(value->idx(2), 255), parseInt(value->idx(3), 255), parseInt(value->idx(4), 255));
				} else if(value->size() > 3)	{
					graphic->setForeColour(parseInt(value->idx(1), 255), parseInt(value->idx(2), 255), parseInt(value->idx(3), 255), 255);
				}
			} else if(cmd == "refresh")	{
				if(value->size() > 1)	{
					string opt = parseString(value->idx(1), "");
					if (opt.empty())	{
						return;
					} else {
						if(opt == "on")	{
							graphic->setRefresh(true);
						} else if(opt == "off")	{
							graphic->setRefresh(false);
						}
					}
				}
			} else if(cmd == "fill")	{
				if(value->size() > 1)	{
					string opt = parseString(value->idx(1), "");
					if (opt.empty())	{
						return;
					} else {
						if(opt == "on")	{
							graphic->setFill(true);
						} else if(opt == "off")	{
							graphic->setFill(false);
						}
					}
				}
			}
		}
	}

	void DrawObject::setLayerMedia(int layer)
	{
		Objsc::MediaPlayer::Current()->setLayer(label, layer);
	}


	void DrawObject::activate()
	{
		graphic->show();
	}
}
