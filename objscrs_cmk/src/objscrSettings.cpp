#include "objscrSettings.hpp"

// objscrs
#include "objscrFiles.hpp"
#include "objscrBody.hpp"
#include "objscrConstants.hpp"

#include <sstream>

namespace Objsc
{
	// NOTE: these are temporary and will move into a separate file later
	string _intToString(const int& value) 
	{
		std::stringstream out;
		out << value;
		return out.str();
	}
	
	string _floatToString(const float& value)
	{
		std::stringstream out;
		out << value;
		return out.str();
	}
	
	////////////////////////////////////////
	// SETTINGS
	////////////////////////////////////////
	Settings::Settings() : 
		loaded(false),
		cameraID(Objsc::DEFAULT_V4L_DEVICE),
		cameraLib(Objsc::CAMLIB_V4L),
		cameraOn(Objsc::DEFAULT_CAMERA_ON),
		camWidth(Objsc::VIDSOURCE_WIDTH),
		camHeight(Objsc::VIDSOURCE_HEIGHT),
		scrWidth(Objsc::DEFAULT_SCR_WIDTH),
		scrHeight(Objsc::DEFAULT_SCR_HEIGHT),
		trackingMinArea(Objsc::DEFAULT_TRACKING_MIN_AREA),
		trackingMaxArea(Objsc::DEFAULT_TRACKING_MAX_AREA),
		trackingThreshold(Objsc::DEFAULT_TRACKING_THRESHOLD),
		trackingContinuous(Objsc::DEFAULT_TRACKING_CONTINUOUS),
		trackingBlur(Objsc::DEFAULT_TRACKING_BLUR),
		oscport(Objsc::DEFAULT_OSC_MAINPORT)
	{
		
	}
		
		
	void Settings::print()
	{
		printf("-------------------------------------------\n");
		printf("SETTINGS\n");
		printf("-------------------------------------------\n");
		if(loaded)	{
			printf("loaded from file\n");
		} else {
			printf("file not loaded, using default values\n");
		}
		printf("-------------------------------------------\n");
		printf("display preferences:\n");
		printf("width: %d\n", scrWidth);
		printf("height: %d\n", scrHeight);
		printf("-------------------------------------------\n");
		printf("network preferences:\n");
		printf("OSC port: %d\n", oscport);
		printf("-------------------------------------------\n");
		printf("camera preferences:\n");
		if(cameraOn)	{
			printf("camera on\n");
			if(cameraLib == CAMLIB_DVGRAB)	{
				printf("camera lib: dvgrab\n");
				printf("camera width: %d\n", PAL_WIDTH);
				printf("camera height: %d\n", PAL_HEIGHT);
			} else {
				printf("camera lib: v4l/gst\n");
				printf("camera: /dev/video%d\n", cameraID);
				printf("camera width: %d\n", camWidth);
				printf("camera height: %d\n", camHeight);
			}
		} else {
			printf("camera off\n");
		}
		printf("-------------------------------------------\n");
		printf("tracking preferences:\n");
		printf("minimum area: %d\n", trackingMinArea);
		printf("maximum area: %d\n", trackingMaxArea);
		printf("threshold: %d\n", trackingThreshold);
		printf("blur: %d\n", trackingBlur);
		if(trackingContinuous)	{
			printf("tracking continuous\n");
		} else {
			printf("tracking static\n");
		}
		printf("-------------------------------------------\n");
		printf("body preferences:\n");
		printf("unborn age: %d\n", Objsc::Body::unbornAge);
		printf("unborn hits: %d\n", Objsc::Body::unbornHits);
		printf("unborn last hit: %d\n", Objsc::Body::unbornLastHit);
		printf("becoming age: %d\n", Objsc::Body::becomingAge);
		printf("becoming hits: %d\n", Objsc::Body::becomingHits);
		printf("becoming last hit: %d\n", Objsc::Body::becomingLastHit);
		printf("present last hit: %d\n", Objsc::Body::presentLastHit);
		printf("absent last hit min: %d\n", Objsc::Body::absentLastHitMin);
		printf("absent last hit max: %d\n", Objsc::Body::absentLastHitMax);
		printf("max distance: %f\n", Objsc::Body::maxDist);
		printf("velocity steps: %d\n", Objsc::Body::velSteps);
		printf("-------------------------------------------\n");
		for(std::map<std::string, std::string>::iterator it(aSetting.begin());
		    it != aSetting.end();
		    it++)
		{
			printf("%s: %s\n", (*it).first.c_str(), (*it).second.c_str());
		}
		printf("-------------------------------------------\n\n");
	}
		
	bool Settings::load(const std::string &filename)
	{
		loaded = false;
		TiXmlDocument doc = TiXmlDocument();
		std::string settingsFilename(filename.empty() ? Objsc::ProjectFiles::Current()->getSettingsPath() : filename);
		if (doc.LoadFile(settingsFilename)) {
			//printf("decoding ...\n");
			TiXmlHandle hDoc(&doc);
			TiXmlElement* pElem;
			TiXmlHandle root(0);
			
			pElem=hDoc.FirstChildElement().Element();
			if (!pElem) {
				loaded = false;
				printf("ERROR: settings file invalid.\n");
				return loaded;
			}
			root=TiXmlHandle(pElem);
			pElem=root.FirstChild("settings").Element();
			if(pElem)	{
				decodeSettings(pElem);
				decodeCamera(pElem);
				decodeDisplay(pElem);
				decodeBodyPrefs(pElem);
				decodeTracking(pElem);
				decodeNetwork(pElem);
				loaded = true;
			} else {
				loaded = false;
				settingsNotFound( false );
				printf("ERROR: Cannot extract settings from settings file (please check its syntax), will use default settings.\n");
			}
		} else {
			settingsNotFound( true );
		}
		return loaded;
	}
	
	void Settings::save()
	{
		TiXmlDocument doc = TiXmlDocument();
		TiXmlDeclaration *decl = new TiXmlDeclaration("1.0", "", "");  
		doc.LinkEndChild(decl);
		
		TiXmlElement *root = new TiXmlElement("objscrs");  
		doc.LinkEndChild(root);
		
		TiXmlElement *settingsEle = new TiXmlElement("settings");
		
		encodeCamera(settingsEle);
		encodeDisplay(settingsEle);
		encodeBodyPrefs(settingsEle);
		encodeTracking(settingsEle);
		encodeNetwork(settingsEle);
		
		root->LinkEndChild(settingsEle);
		
		doc.SaveFile(Objsc::ProjectFiles::Current()->getSettingsPath());
	}
	
	void Settings::reset()
	{
		// camera
		cameraID = Objsc::DEFAULT_V4L_DEVICE;
		cameraLib = Objsc::CAMLIB_V4L;
		cameraOn = Objsc::DEFAULT_CAMERA_ON;
		camWidth = Objsc::VIDSOURCE_WIDTH;
		camHeight = Objsc::VIDSOURCE_HEIGHT;
		
		// display
		scrWidth = Objsc::DEFAULT_SCR_WIDTH;
		scrHeight = Objsc::DEFAULT_SCR_HEIGHT;
		
		// body prefs
		Objsc::Body::unbornAge = Objsc::DEFAULT_BODY_UNBORN_AGE;
		Objsc::Body::unbornHits = Objsc::DEFAULT_BODY_UNBORN_HITS;
		Objsc::Body::unbornLastHit = Objsc::DEFAULT_BODY_UNBORN_LAST_HIT;
		Objsc::Body::becomingAge = Objsc::DEFAULT_BODY_BECOMING_AGE;
		Objsc::Body::becomingHits = Objsc::DEFAULT_BODY_BECOMING_HITS;
		Objsc::Body::becomingLastHit = Objsc::DEFAULT_BODY_BECOMING_LAST_HIT;
		Objsc::Body::presentLastHit = Objsc::DEFAULT_BODY_PRESENT_LAST_HIT;
		Objsc::Body::absentLastHitMin = Objsc::DEFAULT_BODY_ABSENT_LAST_HIT_MIN;
		Objsc::Body::absentLastHitMax = Objsc::DEFAULT_BODY_ABSENT_LAST_HIT_MAX;
		Objsc::Body::maxDist = Objsc::DEFAULT_BODY_MAXDISTANCE;
		
		// tracking
		trackingMinArea = Objsc::DEFAULT_TRACKING_MIN_AREA;
		trackingMaxArea = Objsc::DEFAULT_TRACKING_MAX_AREA;
		trackingThreshold = Objsc::DEFAULT_TRACKING_THRESHOLD;
		
		// network
		oscport = Objsc::DEFAULT_OSC_MAINPORT;
	}
	
	void Settings::settingsNotFound(bool generateSettingsFile)
	{
		printf("ERROR: unable to read settings file.\n");
		printf("Generating default version.\n");
		reset();
		if(generateSettingsFile)
			save();
	}
	
	void Settings::decodeCamera(TiXmlElement *parentNode)
	{
		TiXmlElement *videoinputEle = parentNode->FirstChildElement("videoinput");
		if(videoinputEle) {
			// camera
			TiXmlElement *cameraEle = videoinputEle->FirstChildElement("camera");
			if(cameraEle) {
				if (string(cameraEle->GetText()) == "on")	{
					cameraOn  = true;
				} else {
					cameraOn = false;
				}
			} else {
				cameraOn = false;
			}
			
			// camera_id
			TiXmlElement *camIDEle = videoinputEle->FirstChildElement("camera_id");
			if(camIDEle) {
				cameraID  = atoi(camIDEle->GetText());
			} else {
				cameraID = Objsc::DEFAULT_V4L_DEVICE;
			}
			
			// camera_lib
			TiXmlElement *camLibEle = videoinputEle->FirstChildElement("camera_lib");
			if(camLibEle) {
				if (string(camLibEle->GetText()) == "dvgrab")	{
					cameraLib  = Objsc::CAMLIB_DVGRAB;
				} else {
					cameraLib = Objsc::CAMLIB_V4L;
				}
			} else {
				cameraLib = Objsc::CAMLIB_V4L;
			}
			
			// camera_width
			TiXmlElement *camWidthEle = videoinputEle->FirstChildElement("camera_width");
			if(camWidthEle) {
				camWidth  = atoi(camWidthEle->GetText());
			} else {
				camWidth = Objsc::VIDSOURCE_WIDTH;
			}
			
			// camera_height
			TiXmlElement *camHeightEle = videoinputEle->FirstChildElement("camera_height");
			if(camHeightEle) {
				camHeight  = atoi(camHeightEle->GetText());
			} else {
				camHeight = Objsc::VIDSOURCE_HEIGHT;
			}
		
		}
	}
	
	void Settings::encodeCamera(TiXmlElement *parentNode)
	{
		TiXmlElement *videoinputEle = new TiXmlElement("videoinput");
		
		// camera
		TiXmlElement *cameraEle = new TiXmlElement("camera");
		if (cameraOn)	{
			cameraEle->LinkEndChild(new TiXmlText("on"));
		} else {
			cameraEle->LinkEndChild(new TiXmlText("off"));
		}
		videoinputEle->LinkEndChild(cameraEle);
		
		//camera_id
		TiXmlElement *camIDEle = new TiXmlElement("camera_id");
		camIDEle->LinkEndChild(new TiXmlText(_intToString(cameraID)));
		videoinputEle->LinkEndChild(camIDEle);
		
		// camera_lib
		TiXmlElement *camLibEle = new TiXmlElement("camera_lib");
		if (cameraLib == Objsc::CAMLIB_DVGRAB)	{
			camLibEle->LinkEndChild(new TiXmlText("dvgrab"));
		} else {
			camLibEle->LinkEndChild(new TiXmlText("v4l"));
		}
		videoinputEle->LinkEndChild(camLibEle);
		
		// camera_width
		TiXmlElement *camWidthEle = new TiXmlElement("camera_width");
		camWidthEle->LinkEndChild(new TiXmlText(_intToString(camWidth)));
		videoinputEle->LinkEndChild(camWidthEle);
		
		// camera_height
		TiXmlElement *camHeightEle = new TiXmlElement("camera_height");
		camHeightEle->LinkEndChild(new TiXmlText(_intToString(camHeight)));
		videoinputEle->LinkEndChild(camHeightEle);
		
		parentNode->LinkEndChild(videoinputEle);
	}
	
	void Settings::decodeDisplay(TiXmlElement *parentNode)
	{
		TiXmlElement *displayEle = parentNode->FirstChildElement("display");
		if(displayEle) {
			// width
			TiXmlElement *widthEle = displayEle->FirstChildElement("width");
			if(widthEle) {
				scrWidth  = atoi(widthEle->GetText());
			} else {
				scrWidth = Objsc::DEFAULT_SCR_WIDTH;
			}
			
			// height
			TiXmlElement *heightEle = displayEle->FirstChildElement("height");
			if(heightEle) {
				scrHeight  = atoi(heightEle->GetText());
			} else {
				scrHeight = Objsc::DEFAULT_SCR_HEIGHT;
			}
		}

		if(CAMLIB_DVGRAB == cameraLib)
		{
			camWidth = PAL_WIDTH;
			camHeight = PAL_HEIGHT;
		}
	}
	
	void Settings::encodeDisplay(TiXmlElement *parentNode)
	{
		TiXmlElement *displayEle = new TiXmlElement("display");
		
		// width
		TiXmlElement *widthEle = new TiXmlElement("width");
		widthEle->LinkEndChild(new TiXmlText(_intToString(scrWidth)));
		displayEle->LinkEndChild(widthEle);
		
		// height
		TiXmlElement *heightEle = new TiXmlElement("height");
		heightEle->LinkEndChild(new TiXmlText(_intToString(scrHeight)));
		displayEle->LinkEndChild(heightEle);
		
		parentNode->LinkEndChild(displayEle);
	}
	
	void Settings::decodeBodyPrefs(TiXmlElement *parentNode)
	{
		TiXmlElement *bodyprefEle = parentNode->FirstChildElement("bodyprefs");
		if(bodyprefEle) {
			// unbornage
			TiXmlElement *unbornAgeEle = bodyprefEle->FirstChildElement("unbornage");
			if(unbornAgeEle) {
				Objsc::Body::unbornAge = atoi(unbornAgeEle->GetText());
			} else {
				Objsc::Body::unbornAge = Objsc::DEFAULT_BODY_UNBORN_AGE;
			}
			
			// unbornhits
			TiXmlElement *unbornHitsEle = bodyprefEle->FirstChildElement("unbornhits");
			if(unbornHitsEle) {
				Objsc::Body::unbornHits = atoi(unbornHitsEle->GetText());
			} else {
				Objsc::Body::unbornHits = Objsc::DEFAULT_BODY_UNBORN_HITS;
			}
			
			// unbornlasthit
			TiXmlElement *unbornLastHitEle = bodyprefEle->FirstChildElement("unbornlasthit");
			if(unbornLastHitEle) {
				Objsc::Body::unbornLastHit = atoi(unbornLastHitEle->GetText());
			} else {
				Objsc::Body::unbornLastHit = Objsc::DEFAULT_BODY_UNBORN_LAST_HIT;
			}
			
			// becomingage
			TiXmlElement *becomingAgeEle = bodyprefEle->FirstChildElement("becomingage");
			if(becomingAgeEle) {
				Objsc::Body::becomingAge = atoi(becomingAgeEle->GetText());
			} else {
				Objsc::Body::becomingAge = Objsc::DEFAULT_BODY_BECOMING_AGE;
			}
			
			// becominghits
			TiXmlElement *becomingHitsEle = bodyprefEle->FirstChildElement("becominghits");
			if(becomingHitsEle) {
				Objsc::Body::becomingHits = atoi(becomingHitsEle->GetText());
			} else {
				Objsc::Body::becomingHits = Objsc::DEFAULT_BODY_BECOMING_HITS;
			}
			
			// becominglasthit
			TiXmlElement *becomingLastHitEle = bodyprefEle->FirstChildElement("becominglasthit");
			if(becomingLastHitEle) {
				Objsc::Body::becomingLastHit = atoi(becomingLastHitEle->GetText());
			} else {
				Objsc::Body::becomingLastHit = Objsc::DEFAULT_BODY_BECOMING_LAST_HIT;
			}
			
			// presentlasthit
			TiXmlElement *presentLastHitEle = bodyprefEle->FirstChildElement("presentlasthit");
			if(presentLastHitEle) {
				Objsc::Body::presentLastHit = atoi(presentLastHitEle->GetText());
			} else {
				Objsc::Body::presentLastHit = Objsc::DEFAULT_BODY_PRESENT_LAST_HIT;
			}
			
			// absentlasthitmin
			TiXmlElement *absentLastHitMinEle = bodyprefEle->FirstChildElement("absentlasthitmin");
			if(absentLastHitMinEle) {
				Objsc::Body::absentLastHitMin = atoi(absentLastHitMinEle->GetText());
			} else {
				Objsc::Body::absentLastHitMin = Objsc::DEFAULT_BODY_ABSENT_LAST_HIT_MIN;
			}
			
			// absentlasthitmax
			TiXmlElement *absentLastHitMaxEle = bodyprefEle->FirstChildElement("absentlasthitmax");
			if(absentLastHitMaxEle) {
				Objsc::Body::absentLastHitMax = atoi(absentLastHitMaxEle->GetText());
			} else {
				Objsc::Body::absentLastHitMax = Objsc::DEFAULT_BODY_ABSENT_LAST_HIT_MAX;
			}
			
			// max distance
			TiXmlElement *maxdistEle = bodyprefEle->FirstChildElement("maxdistance");
			if(maxdistEle) {
				Objsc::Body::maxDist = (float)atof(maxdistEle->GetText());
			} else {
				Objsc::Body::maxDist = Objsc::DEFAULT_BODY_MAXDISTANCE;
			}
			
			// velocity steps for averaging
			TiXmlElement *velstepsEle = bodyprefEle->FirstChildElement("velsteps");
			if(velstepsEle) {
				Objsc::Body::velSteps = (float)atof(velstepsEle->GetText());
			} else {
				Objsc::Body::velSteps = Objsc::DEFAULT_BODY_VELSTEPS;
			}
		}
	}
	
	void Settings::encodeBodyPrefs(TiXmlElement *parentNode)
	{
		TiXmlElement *bodyprefEle = new TiXmlElement("bodyprefs");
		
		// unbornage
		TiXmlElement *unbornAgeEle = new TiXmlElement("unbornage");
		unbornAgeEle->LinkEndChild(new TiXmlText(_intToString(Objsc::Body::unbornAge)));
		bodyprefEle->LinkEndChild(unbornAgeEle);
		
		// unbornhits
		TiXmlElement *unbornHitsEle = new TiXmlElement("unbornhits");
		unbornHitsEle->LinkEndChild(new TiXmlText(_intToString(Objsc::Body::unbornHits)));
		bodyprefEle->LinkEndChild(unbornHitsEle);
		
		// unbornlasthit
		TiXmlElement *unbornLastHitEle = new TiXmlElement("unbornlasthit");
		unbornLastHitEle->LinkEndChild(new TiXmlText(_intToString(Objsc::Body::unbornLastHit)));
		bodyprefEle->LinkEndChild(unbornLastHitEle);
		
		// becomingage
		TiXmlElement *becomingAgeEle = new TiXmlElement("becomingage");
		becomingAgeEle->LinkEndChild(new TiXmlText(_intToString(Objsc::Body::becomingAge)));
		bodyprefEle->LinkEndChild(becomingAgeEle);
		
		// becominghits
		TiXmlElement *becomingHitsEle = new TiXmlElement("becominghits");
		becomingHitsEle->LinkEndChild(new TiXmlText(_intToString(Objsc::Body::becomingHits)));
		bodyprefEle->LinkEndChild(becomingHitsEle);
		
		// becominglasthit
		TiXmlElement *becomingLastHitEle = new TiXmlElement("becominglasthit");
		becomingLastHitEle->LinkEndChild(new TiXmlText(_intToString(Objsc::Body::becomingLastHit)));
		bodyprefEle->LinkEndChild(becomingLastHitEle);
		
		// presentlasthit
		TiXmlElement *presentLastHitEle = new TiXmlElement("presentlasthit");
		presentLastHitEle->LinkEndChild(new TiXmlText(_intToString(Objsc::Body::presentLastHit)));
		bodyprefEle->LinkEndChild(presentLastHitEle);
		
		// absentlasthitmin
		TiXmlElement *absentLastHitMinEle = new TiXmlElement("absentlasthitmin");
		absentLastHitMinEle->LinkEndChild(new TiXmlText(_intToString(Objsc::Body::absentLastHitMin)));
		bodyprefEle->LinkEndChild(absentLastHitMinEle);
		
		// absentlasthitmax
		TiXmlElement *absentLastHitMaxEle = new TiXmlElement("absentlasthitmax");
		absentLastHitMaxEle->LinkEndChild(new TiXmlText(_intToString(Objsc::Body::absentLastHitMax)));
		bodyprefEle->LinkEndChild(absentLastHitMaxEle);
		
		// maxdistance
		TiXmlElement *maxdistEle = new TiXmlElement("maxdistance");
		maxdistEle->LinkEndChild(new TiXmlText(_floatToString(Objsc::Body::maxDist)));
		bodyprefEle->LinkEndChild(maxdistEle);
		
		// velocity steps
		TiXmlElement *velstepsEle = new TiXmlElement("velsteps");
		velstepsEle->LinkEndChild(new TiXmlText(_floatToString(Objsc::Body::velSteps)));
		bodyprefEle->LinkEndChild(velstepsEle);
		
		parentNode->LinkEndChild(bodyprefEle);
	}
	
	void Settings::decodeTracking(TiXmlElement *parentNode)
	{
		TiXmlElement *trackingEle = parentNode->FirstChildElement("tracking");
		if(trackingEle) {
			TiXmlElement *minareaEle = trackingEle->FirstChildElement("minarea");
			if(minareaEle) {
				trackingMinArea = atoi(minareaEle->GetText());
			} else {
				trackingMinArea = Objsc::DEFAULT_TRACKING_MIN_AREA;
			}
			
			TiXmlElement *maxareaEle = trackingEle->FirstChildElement("maxarea");
			if(maxareaEle) {
				trackingMaxArea = atoi(maxareaEle->GetText());
			} else {
				trackingMaxArea = Objsc::DEFAULT_TRACKING_MAX_AREA;
			}
			
			TiXmlElement *thresholdEle = trackingEle->FirstChildElement("threshold");
			if(thresholdEle) {
				trackingThreshold = atoi(thresholdEle->GetText());
			} else {
				trackingThreshold = Objsc::DEFAULT_TRACKING_THRESHOLD;
			}
			
			// continuous
			TiXmlElement *continuousEle = trackingEle->FirstChildElement("continuous");
			if(continuousEle) {
				if (string(continuousEle->GetText()) == "on")	{
					trackingContinuous  = true;
				} else {
					trackingContinuous = false;
				}
			} else {
				trackingContinuous = false;
			}
			
			TiXmlElement *blurEle = trackingEle->FirstChildElement("blur");
			if(blurEle) {
				trackingBlur = atoi(blurEle->GetText());
			} else {
				trackingBlur = Objsc::DEFAULT_TRACKING_BLUR;
			}
			
		}
	}
	
	void Settings::encodeTracking(TiXmlElement *parentNode)
	{
		TiXmlElement *trackingEle = new TiXmlElement("tracking");
		
		// minarea
		TiXmlElement *minareaEle = new TiXmlElement("minarea");
		minareaEle->LinkEndChild(new TiXmlText(_intToString(trackingMinArea)));
		trackingEle->LinkEndChild(minareaEle);
		
		// maxarea
		TiXmlElement *maxareaEle = new TiXmlElement("maxarea");
		maxareaEle->LinkEndChild(new TiXmlText(_intToString(trackingMaxArea)));
		trackingEle->LinkEndChild(maxareaEle);
		
		// threshold
		TiXmlElement *thresholdEle = new TiXmlElement("threshold");
		thresholdEle->LinkEndChild(new TiXmlText(_intToString(trackingThreshold)));
		trackingEle->LinkEndChild(thresholdEle);
		
		// continuous
		TiXmlElement *continuousEle = new TiXmlElement("continuous");
		if (trackingContinuous)	{
			continuousEle->LinkEndChild(new TiXmlText("on"));
		} else {
			continuousEle->LinkEndChild(new TiXmlText("off"));
		}
		trackingEle->LinkEndChild(continuousEle);
		
		// blur
		TiXmlElement *blurEle = new TiXmlElement("blur");
		blurEle->LinkEndChild(new TiXmlText(_intToString(trackingBlur)));
		trackingEle->LinkEndChild(blurEle);
		
		parentNode->LinkEndChild(trackingEle);
	}
	
	void Settings::decodeNetwork(TiXmlElement *parentNode)
	{
		TiXmlElement *networkEle = parentNode->FirstChildElement("network");
		if(networkEle) {
			TiXmlElement *oscportEle = networkEle->FirstChildElement("oscport");
			if(oscportEle) {
				oscport  = atoi(oscportEle->GetText());
			} else {
				oscport = Objsc::DEFAULT_OSC_MAINPORT;
			}
		}
	}
	
	void Settings::encodeNetwork(TiXmlElement *parentNode)
	{
		TiXmlElement *networkEle = new TiXmlElement("network");
		
		// width
		TiXmlElement *oscportEle = new TiXmlElement("oscport");
		oscportEle->LinkEndChild(new TiXmlText(_intToString(oscport)));
		networkEle->LinkEndChild(oscportEle);
		
		parentNode->LinkEndChild(networkEle);
	}

	// Decode arbitrary settings of the form <set><name>foo</name><value>bar</value></set>
	// would dream of QVariant here :-) -pm
	void Settings::decodeSettings(TiXmlElement *parentNode)
	{
		for(TiXmlElement* setting=parentNode->FirstChildElement("set" );setting != NULL; setting = setting->NextSiblingElement())
		{
			std::string aName(setting->FirstChildElement("name")->GetText());
			std::string aValue(setting->FirstChildElement("value")->GetText());
			if(aName.empty() || aValue.empty())
				continue;

			aSetting.insert(std::pair<std::string, std::string>(aName, aValue));
		}
	}

	std::string Settings::getSetting(const std::string &key) const
	{
		if(aSetting.find(key) != aSetting.end())
			return aSetting.find(key)->second;
		return std::string();
	}

	bool Settings::hasSetting(const std::string &key) const
	{
		if(aSetting.find(key) != aSetting.end())
			return true;
		return false;
	}
}
