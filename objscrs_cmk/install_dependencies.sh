#/bin/bash

############################
# This is a bash script to install the
# required libraries for objscs on Debian
# and Ubuntu systems. 
#
# Minimum Debian: 6.0.2 - Squeeze
# Minimum Ubuntu: 11.10 - Oneiric
#
# Note: a number of packages may need to 
# be downloaded from additional
# repositories, these include:
#
# libmp3lame0_3.98.4-0.0_i386.deb
# liboscpack1_1.0.2-1_i386.deb
# liboscpack-dev_1.0.2-1_i386.deb
# libv4l-0_0.8.3-2_i386.deb
# libv4l-dev_0.8.3-2_i386.deb
# libfreenect-dev-0.0-i686.deb
#
# Run in a terminal by entering either:
#
#     ./install_dependencies.sh
#
# or:
#
#    bash install_dependencies.sh
#
# Last modified: 14/08/2011
############################

sudo apt-get install build-essential cmake freeglut3-dev libasound2-dev libatk1.0-dev libavc1394-dev libavcodec-dev libavformat-dev libavutil-dev libblas-dev libbz2-dev libc6-dev libcairo2-dev libcv-dev libcvaux-dev libdbus-1-dev libdc1394-22-dev libdirac-encoder0 libdrm2 libdv4-dev libexpat1-dev libfaad2 libfontconfig1-dev libfreeimage-dev libfreetype6-dev libgcc1 libgfortran3 libgl1-mesa-glx libglee0d1 libglib2.0-dev libglu1-mesa-dev libgsm1 libgstreamer-plugins-base0.10-dev libgstreamer0.10-dev libgtk2.0-dev libhal-dev libhighgui-dev libice-dev libiec61883-dev libilmbase6 libjack-dev libjasper-dev libjpeg62-dev liblapack3gf liblcms1 libmng1 libogg-dev libopenexr6 libopenjpeg2 liborc-0.4-0 libpango1.0-dev libpcre3 libpixman-1-dev libpng12-dev libpoco-dev libraw1394-dev libschroedinger-1.0-0 libselinux1 libsm-dev libspeex1 libstk0-dev libswscale-dev libtheora-dev libtiff4-dev libunicap2-dev libusb-1.0-0-dev libuuid1 libvorbis-dev libx11-dev libxau-dev libxcb-render-util0-dev libxcb-render0-dev libxcb1-dev libxcomposite-dev libxcursor-dev libxdamage-dev libxdmcp-dev libxext-dev libxfixes-dev libxi-dev libxinerama-dev libxml2-dev libxmu-dev libxrandr-dev libxrender-dev libxt-dev libxv-dev libxxf86vm-dev zlib1g-dev libv4l-dev libtinyxml-dev libboost-filesystem-dev glee-dev liboscpack-dev hal
