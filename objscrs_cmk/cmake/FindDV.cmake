# - Try to find DV
# Once done this will define
#
#  DV_FOUND - system has DV
#  DV_INCLUDE_DIR - the DV include directory
#  DV_LIBRARIES - the libraries needed to use DV

if(DV_INCLUDE_DIR AND DV_LIBRARIES)
	# in cache already
	set(DV_FIND_QUIETLY TRUE)
endif(DV_INCLUDE_DIR AND DV_LIBRARIES)

find_package(PkgConfig)
PKG_CHECK_MODULES(PKG_DV libdv)
set(DV_VERSION ${PKG_DV_VERSION})
set(DV_DEFINITIONS ${PKG_DV_CFLAGS})


find_path(DV_INCLUDE_DIR dv.h
PATHS
${PKG_DV_INCLUDE_DIRS}
PATH_SUFFIXES libdv
)

find_library(DV_LIBRARIES NAMES dv
PATHS
${PKG_DV_LIBRARY_DIRS}
)

include(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(DV DEFAULT_MSG DV_LIBRARIES DV_INCLUDE_DIR)

mark_as_advanced(DV_INCLUDE_DIR DV_LIBRARIES)
