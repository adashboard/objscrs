# - Try to find GLee
# Once done this will define
#
#  GLEE_FOUND - system has GLee
#  GLEE_INCLUDE_DIR - the GLee include directory
#  GLEE_LIBRARIES - the libraries needed to use GLee

if(GLEE_INCLUDE_DIR AND GLEE_LIBRARIES)
	# in cache already
	set(GLee_FIND_QUIETLY TRUE)
endif(GLEE_INCLUDE_DIR AND GLEE_LIBRARIES)

find_package(PkgConfig)
PKG_CHECK_MODULES(PKG_GLEE glee)
set(GLEE_VERSION ${PKG_GLEE_VERSION})
set(GLEE_DEFINITIONS ${PKG_GLEE_CFLAGS})


find_path(GLEE_INCLUDE_DIR GLee.h
PATHS
${PKG_GLEE_INCLUDE_DIRS}
)

find_library(GLEE_LIBRARIES NAMES glee
PATHS
${PKG_GLEE_LIBRARY_DIRS}
)

include(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(GLee DEFAULT_MSG GLEE_LIBRARIES GLEE_INCLUDE_DIR)

mark_as_advanced(GLEE_INCLUDE_DIR GLEE_LIBRARIES)
