# - Try to find Unicap
# Once done this will define
#
#  UNICAP_FOUND - system has Unicap
#  UNICAP_INCLUDE_DIR - the Unicap include directory
#  UNICAP_LIBRARIES - the libraries needed to use Unicap

if(UNICAP_INCLUDE_DIR AND UNICAP_LIBRARIES)
	# in cache already
	set(Unicap_FIND_QUIETLY TRUE)
endif(UNICAP_INCLUDE_DIR AND UNICAP_LIBRARIES)

find_package(PkgConfig)
PKG_CHECK_MODULES(PKG_UNICAP libunicap)
set(UNICAP_VERSION ${PKG_UNICAP_VERSION})
set(UNICAP_DEFINITIONS ${PKG_UNICAP_CFLAGS})


find_path(UNICAP_INCLUDE_DIR unicap.h
PATHS
${PKG_UNICAP_INCLUDE_DIRS}
PATH_SUFFIXES unicap
)

find_library(UNICAP_LIBRARIES NAMES unicap
PATHS
${PKG_UNICAP_LIBRARY_DIRS}
)

include(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(Unicap DEFAULT_MSG UNICAP_LIBRARIES UNICAP_INCLUDE_DIR)

mark_as_advanced(UNICAP_INCLUDE_DIR UNICAP_LIBRARIES)
