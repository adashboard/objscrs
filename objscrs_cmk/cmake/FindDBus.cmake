# - Try to find DBus
# Once done this will define
#
#  DBUS_FOUND - system has DBus
#  DBUS_INCLUDE_DIR - the DBus include directory
#  DBUS_LIBRARIES - the libraries needed to use DBus
#  DBUS_DEFINITIONS - Compiler switches required for using DBus


if(DBUS_INCLUDE_DIR AND DBUS_LIBRARIES)
   set(DBus_FIND_QUIETLY TRUE)
endif(DBUS_INCLUDE_DIR AND DBUS_LIBRARIES)

find_package(PkgConfig)
PKG_CHECK_MODULES(PKG_DBUS dbus-1)
set(DBUS_VERSION ${PKG_DBUS_VERSION})
set(DBUS_DEFINITIONS ${PKG_DBUS_CFLAGS})

find_path(DBUS_INCLUDE_DIR dbus/dbus.h
   PATHS
   ${PKG_DBUS_INCLUDE_DIRS}
   PATH_SUFFIXES dbus-1.0
)

find_library(DBUS_LIBRARIES NAMES dbus-1
   PATHS
   ${PKG_DBUS_LIBRARY_DIRS}
)

if(DBUS_INCLUDE_DIR)
	SET(DBUS_INCLUDE_DIR ${DBUS_INCLUDE_DIR}  ${DBUS_INCLUDE_DIR}/include)
# 	MESSAGE(STATUS "DBus include dirs: ${DBUS_INCLUDE_DIRS}")
endif(DBUS_INCLUDE_DIR)

include(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(DBus DEFAULT_MSG DBUS_LIBRARIES DBUS_INCLUDE_DIR)

mark_as_advanced(DBUS_INCLUDE_DIR DBUS_LIBRARIES)
