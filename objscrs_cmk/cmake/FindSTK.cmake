# Finds STK Library
#
#  STK_FOUND
#  STK_INCLUDE_DIRS
#  STK_LIBRARIES

FIND_PATH(
STK_INCLUDE_DIR 
NAMES Stk.h
DOC "Include directory for the STK library"
PATH_SUFFIXES stk
)

# Look for the library.
find_library(
  STK_LIBRARY
  NAMES stk
  DOC "Library to link against core STK"
)

# Copy the results to the output variables.
if(STK_INCLUDE_DIR AND STK_LIBRARY)
  set(STK_FOUND 1)
  set(STK_LIBRARIES ${STK_LIBRARY})
  set(STK_INCLUDE_DIRS ${STK_INCLUDE_DIR})
  set(STK_DEFINITIONS " -D__OS_LINUX__ ")
  else(STK_INCLUDE_DIR AND STK_LIBRARY)
	 MESSAGE(FATAL_ERROR "CANNOT FIND STK" )
endif(STK_INCLUDE_DIR AND STK_LIBRARY)
