# - Try to find Hal
# Once done this will define
#
#  HAL_FOUND - system has Hal
#  HAL_INCLUDE_DIR - the Hal include directory
#  HAL_LIBRARIES - the libraries needed to use Hal
#  HAL_DEFINITIONS - Compiler switches required for using Hal


if(HAL_INCLUDE_DIR AND HAL_LIBRARIES)
   set(Hal_FIND_QUIETLY TRUE)
endif(HAL_INCLUDE_DIR AND HAL_LIBRARIES)

find_package(PkgConfig)
PKG_CHECK_MODULES(PKG_HAL hal)
set(HAL_VERSION ${PKG_HAL_VERSION})
set(HAL_DEFINITIONS ${PKG_HAL_CFLAGS})

find_path(HAL_INCLUDE_DIR libhal.h
   PATHS
   ${PKG_HAL_INCLUDE_DIRS}
   PATH_SUFFIXES hal
)

find_library(HAL_LIBRARIES NAMES hal
   PATHS
   ${PKG_HAL_LIBRARY_DIRS}
)


include(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(Hal DEFAULT_MSG HAL_LIBRARIES HAL_INCLUDE_DIR)

mark_as_advanced(HAL_INCLUDE_DIR HAL_LIBRARIES)
