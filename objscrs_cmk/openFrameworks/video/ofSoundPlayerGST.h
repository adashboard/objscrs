#ifndef  _OF_SOUND_PLAYER_GST
#define  _OF_SOUND_PLAYER_GST

/**

PLEASE NOTE:

Thsi is not the orignal openframeworks sound player, but rather a
version of ofMoviePlayer that has been adated to use gstreamer for 
audio playback.

*/

#include "ofConstants.h"

#include "ofGstUtils.h"



class ofSoundPlayerGST {

	public:
		ofSoundPlayerGST ():
			bLoaded(false),
			speed(1),
			bPlaying(false),
			bPaused(false) {}
		virtual ~ofSoundPlayerGST() { close(); }

		bool loadSound(string name);
		void close();

		void update();
		void play();
		void stop();

		float speed;
		bool bLoaded;

		float getPosition() { return gstUtils.getPosition(); }
		float getSpeed() { return speed; }
		float getDuration() { return gstUtils.getDuration(); }
		bool getPlayIsDone() { return gstUtils.getIsMovieDone(); }

		void setPosition(float pct) { gstUtils.setPosition(pct); }
		void setVolume(int volume) { gstUtils.setVolume(volume); }
		void setLoopState(int state) { gstUtils.setLoopState(state); }
		void setSpeed(float speed);
		void setFrame(int frame) { gstUtils.setFrame(frame); }

		void setPaused(bool bPause);

		int getCurrentFrame() {return gstUtils.getCurrentFrame();}
		int getTotalNumFrames() { return gstUtils.getTotalNumFrames(); }

		void firstFrame() { gstUtils.firstFrame(); }
		void nextFrame() { gstUtils.nextFrame(); }
		void previousFrame() { gstUtils.previousFrame(); }

	protected:
		bool bPlaying;
		bool bPaused;
		ofGstUtils gstUtils;
};
#endif






