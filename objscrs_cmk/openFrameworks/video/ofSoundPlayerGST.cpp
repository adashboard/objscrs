#include "ofSoundPlayerGST.h"
#include "ofUtils.h"

#include <gst/gst.h>
#include <glib.h>


void ofSoundPlayerGST::update()
{
	if (bLoaded){
		gstUtils.update();
	}
}


void ofSoundPlayerGST::close()
{
	gstUtils.close();
	bLoaded = false;
}


bool ofSoundPlayerGST::loadSound(string name)
{
	if(gstUtils.loadSound(name)){
		bLoaded = true;
		ofLog(OF_LOG_VERBOSE,"ofSoundPlayerGST: sound loaded");
		return true;
	}else{
		ofLog(OF_LOG_ERROR,"ofSoundPlayerGST couldn't load sound");
		return false;
	}
	
	//~ GstElement *pipeline, *source, *demuxer, *decoder, *conv, *sink;
	//~ /* Create gstreamer elements */
	//~ pipeline = gst_pipeline_new ("audio-player");
	//~ source   = gst_element_factory_make ("filesrc",       "file-source");
	//~ demuxer  = gst_element_factory_make ("oggdemux",      "ogg-demuxer");
	//~ decoder  = gst_element_factory_make ("vorbisdec",     "vorbis-decoder");
	//~ conv     = gst_element_factory_make ("audioconvert",  "converter");
	//~ sink     = gst_element_factory_make ("autoaudiosink", "audio-output");

	//~ if (!pipeline || !source || !demuxer || !decoder || !conv || !sink) {
		//~ g_printerr ("One element could not be created. Exiting.\n");
		//~ return false;
	//~ }


}

void ofSoundPlayerGST::play() 
{
	bPaused = false;
	bPlaying = true;
	gstUtils.play(); 
}

void ofSoundPlayerGST::stop() 
{
	bPaused = true;
	bPlaying = false;
	gstUtils.setPaused(true);
}

void ofSoundPlayerGST::setSpeed(float _speed)
{
	speed = _speed;
	gstUtils.setSpeed(_speed);
}


void ofSoundPlayerGST::setPaused(bool _bPause)
{
	bPaused = _bPause;
	bPlaying = !bPaused;
	gstUtils.setPaused(_bPause);
}



