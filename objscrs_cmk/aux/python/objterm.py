#!/usr/bin/python

"""
TO INSTALL:

objterm requires pyOSC which is available from:

	http://trac.v2.nl/wiki/pyOSC

Everything else is just standard Python and should be compatible 
with Python 2.4 upwards.
"""

README =\
"""
__objterm__

version 0.2
23rd July 2011

objterm is a simple terminal client for objscrs. It allows you to
send and receive messages to and from objscrs using osc_in
and osc_out objects.

The objterm client first joins a osc group and then can send
messages to that group, which are received by the 
corresponding osc_in object. It can also receive messages
that are sent from the corresponding osc_out object.

objterm can be used as a basic testing and debugging tool
for OSC communications with objscrs. It is intended as a 
demo for creating Python-based tools to interact with objscrs.

objterm uses a set of prompt characters to indicate different
input and output states on the terminal, these are:

#	waiting for user input
~	log message from objterm (user feedback and debugging)
!	error message from objterm

objterm works from a set of basic built-in commands. All
commands start with a colon ':'. A full list of commands with 
descriptions can be obtained by entering the command:

	:help
	
Info for an individual command can be obtained by entering:

	:help command-name
	
ie:

	:help connect
	
All text entered that does not beging with ':' is treated as a
message which will be sent over the currently active OSC
connection. If the text begins with '/' this will be uased as the 
address for the OSC packet. Text that does not begin with '/'
will be tarted as a plain message and sent with the default 
address of '/msg'. Messages will be parsed before they are sent
so that integers, floats and strings will be sent as the correct
data types.
	


A typical objterm session would be something like this:

1.) Set a name for the objterm client to be identified by objscrs:

	:name harry
	
2.) Connect to objscrs, with the IP address and port used by the
objscrs computer:

	:connect 192.168.0.3 9898
	
3.) Join a group:

	:join pop
	
4.) Once you have joined a group, any text that does not begin 
with a colon will be sent as a message to that group:

	hello there!
	
5.) Any messages received from objscs will be displayed with the
name of the group in square brackets:

	[pop] hello harry
	
6.) To end the session and disconnect type:

	:quit
	
	
objterm also supports intercation with Toonloop, for info on
Toonloop interaction enter the command:

	:toonloop
	
"""

TOONLOOP =\
"""
To connect to a Toonloop client, enter the address and the
receiving port for the that client:

	:connect <toonloop_ip> <toonloop_receiving_port>
	
To receive messages, open a listening port that matches the
sending port of Toonloop:

	:listen <toonloop_sending_port>
	
	
Messages Toonloop can receive:

/ping: Answer with /pong
/pong: Do nothing.
/toon/quit: Quit
/toon/frame/add: Grab a frame
/toon/frame/remove: Remove a frame
/toon/clip/select i:clip_number : Selects a clip
/toon/clip/save_current : Saves the currently selected clip
/toon/clip/import_image s:file_name : Imports an image from the filesystem

Messages sent by Toonloop:

/pong
/toon/clip/cleared i:clip_number
/toon/clip/fps i:clip_number i:fps
/toon/clip/playhead i:clip_number i:image_number s:file_name
/toon/clip/saved i:clip_number s:file_name
/toon/clip/select i:clip_number
/toon/frame/add i:clip_number i:frame_number
/toon/frame/remove i:clip_number i:frame_number
/toon/playhead/direction i:clip_number s:direction
/toon/playhead/none
/toon/clip/import_image s:file_name : Imports an image from the filesystem

Toonloop website: http://toonloop.com/
"""

import os
import sys
import readline
import OSC
import time, threading

###################
## UTILITY FUNCTIONS
###################
def isInt(txt):
	try:
		int(txt)
		return True
	except:
		return False
		
		
def isFloat(txt):
	try:
		float(txt)
		return True
	except:
		return False

		
def dftServerHandler(addr, tags, data, source):
	print "---"
	print "received new osc msg from %s" % OSC.getUrlStr(source)
	print "with addr : %s" % addr
	print "typetags %s" % tags
	print "data %s" % data
	print "---"
	

###################
## OBJTERM CLASS
###################
class ObjTerm(object):
	dft_prompt = '# '
	dft_port = 9697
	dft_ip = '127.0.0.1'
	dft_hostport = 9898
	dft_oscaddr = '/msg'
	frameStep = 0.25
	cmd_char = ':'
	osc_char = '/'
	helpinfo = {
		'quit':("disconnect from remote host and quit, shortcut = :q", None),
		'help':("display help for given command, or all help if no command listed, shortcut = :h",'<[command]>'),
		'debug':("turn debug mode on and off", None),
		'echo':("turn echo mode on and off", '<on|off>'),
		'prompt':("set user display prompt, ':prompt off' removes prompt, ':prompt' restores default", '<[new-prompt]>'),
		'verbose':("show feedback information", None),
		'quiet':("hide feedback information", None),
		'connect':("connect to objscrs on remote host", '<host-ip-address> <host-port-number>'),
		'listen':("listen for incoming messages", '<local-port-number>'),
		'name':("set name for objterm client", '<client-name>'),
		'join':("join objscrs group", '<group-name>'),
		'leave':("leave objscrs group", '<group-name>'),
		'empty':("if 'on' empty strings are sent to objscrs, if 'off' they are ignored", '<on|off>'),
		'read':("read commands and text from file", '<path-to-file>'),
		'wait':("wait for specified duration, includes fractions of a second, ie. 0.5 for half a second", '<duration>'),
		'readme':("prints the README information for objterm", None),
		'toonloop':("prints the OSC messages for Toonloop", None),
	}
	
	def __init__(self):
		self.prompt = self.dft_prompt
		self.ip = self.dft_ip
		self.port = self.dft_port
		self.debug = False
		self.active = False
		self.echo = True
		self.verbose = 1
		self.hostip = None
		self.hostport = None
		self.client = None
		self.server = None
		self.thread = None
		self.name = None
		self.group = None
		self.oscaddr = self.dft_oscaddr
		self.emptyStr = False
		self.blocking = False
		self.waiting = 0
		self.commands = {
			'quit': self.quit,
			'q': self.quit,
			'help':self.printHelp,
			'h':self.printHelp,
			'debug':self.setDebug,
			'echo':self.setEcho,
			'prompt':self.setPrompt,
			'verbose':self.verboseOn,
			'quiet':self.verboseOff,
			'connect':self.connectToHost,
			'listen':self.listen,
			'name':self.setName,
			'join':self.joinGroup,
			'leave':self.leaveGroup,
			'empty':self.setEmtpyStr,
			'read':self.readFile,
			'wait':self.startWait,
			'readme':self.printReadme,
			'toonloop':self.printToonloop,
		}
		
	def parseCommand(self, cmdstr):
		tokens = cmdstr.split()
		cmd = tokens[0][1:]
		args = tokens[1:]
		if self.debug:
			print "~ command: <%s>" % cmd
			print "~ args:", args
		func = self.commands.get(cmd, None)
		if func:
			func(args)
			
	def connectToHost(self, args=None):
		try: self.hostip = str(args[0])
		except: 
			self.hostip = None
			print "! no host IP set"
			return
		try: self.hostport = int(args[1])
		except: self.hostport = self.dft_hostport
		if self.verbose:
			if self.hostip:
				print "~ host: %s, %d" % (self.hostip, self.hostport)
		self.client = OSC.OSCClient()
		try:
			self.client.connect((self.hostip, self.hostport))
		except:
			self.client = None
			print "! unable to connect to host: %s %d" % (self.hostip, self.hostport)
		finally:
			if self.verbose:print "~ connected"
			
	def listen(self, args=None):
		if args:
			try:
				self.port = int(args[0])
			except:
				print "! unable to define local port: %s" % args
				return
		else:
			self.port = self.dft_port
		self.server = OSC.OSCServer((self.ip,self.port))
		self.server.addDefaultHandlers(prefix="/unknownaddress")
		self.server.addMsgHandler("/unknownaddress", dftServerHandler)
		self.server.addMsgHandler("/msg", dftServerHandler)
		# objscrs handlers
		self.server.addMsgHandler("/joined", self.joinedGroupHandler)
		self.server.addMsgHandler("/left", self.leftGroupHandler)
		# toonloop handlers
		self.server.addMsgHandler("/pong", self.tl_pongHandler)
		self.server.addMsgHandler("/toon/clip/cleared", self.tl_ClipClearedHandler)
		self.server.addMsgHandler("/toon/clip/fps", self.tl_ClipFPSHandler)
		#self.server.addMsgHandler("/toon/clip/playhead", self.tl_ClipPlayheadHandler) 
		self.server.addMsgHandler("/toon/clip/playhead", self.ignoredHandler) #NOTE: sends too many messages
		self.server.addMsgHandler("/toon/clip/saved", self.tl_ClipSavedHandler)
		self.server.addMsgHandler("/toon/clip/select", self.tl_ClipSelectHandler)
		self.server.addMsgHandler("/toon/frame/add", self.tl_FrameAddHandler)
		self.server.addMsgHandler("/toon/frame/remove", self.tl_FrameRemoveHandler)
		self.server.addMsgHandler("/toon/clip/direction", self.tl_ClipDirectionHandler)
		self.server.addMsgHandler("/toon/playhead/none", self.ignoredHandler) #NOTE: sends too many messages
		self.server.addMsgHandler("/toon/clip/import_image", self.tl_ClipImportImageHandler)
		# threads
		self.thread = threading.Thread(target=self.server.serve_forever)
		self.thread.start()
		if self.verbose:
			print "~ server running, listening on: %d" % self.port
			
	def setName(self, args=None):
		try:
			self.name = str(args[0])
			if self.verbose:print "~ name set to: '%s'" % self.name
		except:
			print "! new name not defined"
			
	def joinGroup(self, args=None):
		group = None
		try:
			group = str(args[0])
		except:
			print "! no group defined, use :join <group-name>"
		if group:
			if not self.name:
				print "! no name defined, use :name <client-name>"
				return
			if not self.server:
				self.listen()
			pkt = OSC.OSCMessage()
			pkt.setAddress('/join')
			pkt.append(group)
			pkt.append(self.name)
			pkt.append(self.ip)
			pkt.append(self.port)
			self.sendPacket(pkt)
			self.group = None
			if self.verbose:print "~ joining group: '%s'" % group
		
	def leaveGroup(self, args=None):
		if not self.group:
			print "! not currently group member"
			return
		else:
			pkt = OSC.OSCMessage()
			pkt.setAddress('/leave')
			pkt.append(group)
			pkt.append(self.name)
			self.sendPacket(pkt)
			if self.verbose:print "~ leaving group: '%s'" % self.group
				
	# OBJSCRS HANDLERS START
	def joinedGroupHandler(self, addr, tags, data, source):
		self.group = data[0]
		self.server.addMsgHandler("/%s" % self.group, self.groupMessageHandler)
		print "~ %s has joined group %s" % (data[1], data[0])
		
	def leftGroupHandler(self, addr, tags, data, source):
		self.group = data[0]
		print "~ %s has left group %s" % (data[1], data[0])
		
	def groupMessageHandler(self, addr, tags, data, source):
		input = ' '.join(map(str, data))
		print "[%s] %s" % (addr[1:], input)
		if input.startswith('/'):
			self.parseInput(input)
	# OBJSCRS HANDLERS END
				
	# TOONLOOP HANDLERS START
	def tl_pongHandler(self, addr, tags, data, source):
		print "[TOONLOOP] pong received from %s" % OSC.getUrlStr(source)
		
	def tl_ClipClearedHandler(self, addr, tags, data, source):
		try:
			print "[TOONLOOP] clip %d cleared" % data[0]
		except:
			print "! unable to parse toonloop packet: %s %s" % (addr, data)
		
	def tl_ClipFPSHandler(self, addr, tags, data, source):
		try:
			print "[TOONLOOP] clip %d FPS: %d" % (data[0], data[1])
		except:
			print "! unable to parse toonloop packet: %s %s" % (addr, data)
		
	def tl_ClipPlayheadHandler(self, addr, tags, data, source):
		try:
			print "[TOONLOOP] clip %d playhead: %d %s" % (data[0], data[1], data[2])
		except:
			print "! unable to parse toonloop packet: %s %s" % (addr, data)
		
	def tl_ClipSavedHandler(self, addr, tags, data, source):
		try:
			print "[TOONLOOP] clip %d saved to: %s" % (data[0], data[1])
		except:
			print "! unable to parse toonloop packet: %s %s" % (addr, data)
		
	def tl_ClipSelectHandler(self, addr, tags, data, source):
		try:
			print "[TOONLOOP] clip %d selected" % data[0]
		except:
			print "! unable to parse toonloop packet: %s %s" % (addr, data)
		
	def tl_FrameAddHandler(self, addr, tags, data, source):
		try:
			print "[TOONLOOP] clip %d frame added: %d" % (data[0], data[1])
		except:
			print "! unable to parse toonloop packet: %s %s" % (addr, data)
		
	def tl_FrameRemoveHandler(self, addr, tags, data, source):
		try:
			print "[TOONLOOP] clip %d frame removed: %d" % (data[0], data[1])
		except:
			print "! unable to parse toonloop packet: %s %s" % (addr, data)
		
	def tl_ClipDirectionHandler(self, addr, tags, data, source):
		try:
			print "[TOONLOOP] clip %d playhead changed to: %s" % (data[0], data[1])
		except:
			print "! unable to parse toonloop packet: %s %s" % (addr, data)
		
	def tl_ClipImportImageHandler(self, addr, tags, data, source):
		try:
			print "[TOONLOOP] image imported: %s" % data[0]
		except:
			print "! unable to parse toonloop packet: %s %s" % (addr, data)
	# TOONLOOP HANDLERS END
	
	# GENERIC HANDLERS START
	def ignoredHandler(self, addr, tags, data, source):
		if self.debug:print '~ ignored OSC packet: %s' % addr
	# GENERIC HANDLERS END
	
	def quit(self, args=None):
		self.active = False
		if self.client:
			if self.verbose:print '~ disconnecting ...'
			self.client.close()
		if self.server :
			if self.verbose:print '~ stopping server ...'
			try:
				self.server.close()
				self.thread.join()
			except:
				sys.exit(2)
		if self.verbose:
			print '~ bye!'
		sys.exit(0)
		
	def setDebug(self, args=None):
		self.debug = not self.debug
		if self.verbose:
			if self.debug:
				print "~ debug turned ON"
			else:
				print "~ debug turned OFF"
				
	def setEmtpyStr(self, args=None):
		try:
			if args[0] == 'on':
				self.emptyStr = True
				if self.verbose:print "~ empty strings will be sent as OSC messages"
			else:
				self.emptyStr = False
				if self.verbose:print "~ empty strings will be ignored"
		except:
			print "! enter ':empty on' or ':empty off'"
				
		
	def setEcho(self, args=None):
		try:
			if args[0] == 'on':
				self.echo = True
				if self.verbose:print "~ echo turned ON"
			else:
				self.echo = False
				if self.verbose:print "~ echo turned OFF"
		except:
			print "! enter ':echo on' or ':echo off'"
				
	def verboseOn(self, args=None):
		self.verbose = 1
		if self.verbose:
				print "~ verbose turned ON"
	
	def verboseOff(self, args=None):
		self.verbose = 0
		if self.verbose:
				print "~ verbose turned OFF"
		
	def setPrompt(self, args=None):
		if not args:
			self.prompt = self.dft_prompt
		else:
			if args[0] == 'off':
				self.prompt= ''
			elif args[0] == 'on':
				self.prompt = self.dft_prompt
			else:
				self.prompt = "%s " % args[0]
		
	def printHelp(self, args=None):
		if not args:
			cmds = self.helpinfo.keys()
			cmds.sort()
			lines = len(cmds)
			line = 0
			for cmd in cmds:
				info = self.helpinfo[cmd]
				self.printHelpLine(cmd,info[0], info[1])
				line += 1
				if line < lines:print
		else:
			cmd = args[0]
			info = self.helpinfo.get(cmd, None)
			if not info:
				print "! command not found: '%s'" % cmd
			else:
				self.printHelpLine(cmd,info[0], info[1]) 
			
	def printHelpLine(self, cmd, description, options=None):
		if options:
			print "%s%s %s\n%s" % (self.cmd_char,cmd,options,description) 
		else:
			print "%s%s\n%s" % (self.cmd_char,cmd,description) 
		
	def printReadme(self, args=None):
		print README
		
	def printToonloop(self, args=None):
		print TOONLOOP
				
	def readFile(self, args=None):
		try:
			filepath = args[0]
		except:
			print "! enter name of file to read"
			return
		if filepath.startswith('~'):
			filepath = os.path.expanduser(filepath)
		elif not filepath.startswith('/'):
			filepath = os.path.abspath(filepath)
		self.execFile(filepath)
			
	def execFile(self, filepath):
		try:
			f =  open(filepath)
		except:
			print "! unable to open file: %s" % filepath
			return
		haslines = True
		while haslines:
			try:
				line = f.next()
			except StopIteration:
				haslines = False
				break
			if line.startswith(self.cmd_char) and self.echo:
				print "%s%s" % (self.prompt, line.rstrip())
			self.parseInput(line)
		if self.verbose:print "~ file ended: %s" % filepath
			
			
	def startWait(self, args=None):
		try:
			self.waiting = float(args[0])
		except:
			self.blocking = False
			self.waiting=0
			print "! invalid waiting time, use :wait <duration>"
			return
		finally:
			self.blocking = True
			if self.verbose:print "~ wait started: %f" % self.waiting
			time.sleep(self.waiting)
			self.waiting =0
			self.blocking = False
			if self.verbose:print "~ wait ended"
				
			
	def run(self, filepath=None):
		self.active = True
		if filepath:
			self.execFile(filepath)
		while self.active:
			if not self.blocking:
				self.parseInput(raw_input(self.prompt))
			time.sleep(self.frameStep) # for server
			
	def parseInput(self, s):
			if s.startswith(self.cmd_char):
				self.parseCommand(s)
			else:
				if self.echo:print s
				if self.emptyStr:
					self.sendMessage(s)
				else:
					if len(s) > 0:
						self.sendMessage(s)
					
	def sendMessage(self, msg):
		if not self.client:
			print "! unable to send message, not connected to host"
		else:
			if not self.sendToOSCAddress(msg):
				self.sendToDefaultAddress(msg)
			
	def packMessage(self, pkt, parts):
		for p in parts:
			if isInt(p):pkt.append(int(p))
			elif isFloat(p):pkt.append(float(p))
			else:pkt.append(p)
				
			
	def sendPacket(self, pkt):
			try:
				self.client.send(pkt)
			except:
				print "! error sending message"
				
	def sendToDefaultAddress(self, msg):
		pkt = OSC.OSCMessage()
		pkt.setAddress(self.oscaddr)
		if self.group:
			pkt.append(self.group)
		self.packMessage(pkt,msg.split())
		self.sendPacket(pkt)
				
	def sendToOSCAddress(self, msg):
		if not msg.startswith(self.osc_char):
			return False
		else:
			parts = msg.split()
			pkt = OSC.OSCMessage()
			pkt.setAddress(parts[0])
			if len(parts) > 1:
				self.packMessage(pkt,parts[1:])
			self.sendPacket(pkt)
			return True
			
	
	
def main(filepath=None):
	objterm = ObjTerm()
	objterm.run(filepath=filepath)
	sys.exit(0)


###################
## MAIN
###################
if __name__=="__main__":
	try:
		filepath = sys.argv[1]
	except:
		filepath = None
	main(filepath)
	
	