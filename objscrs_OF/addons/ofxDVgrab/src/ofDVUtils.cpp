#include "ofDVUtils.h"

// C includes
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <fcntl.h>
#include <pthread.h>
#include <sched.h>
#include <string.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <sys/mman.h>


bool g_done = false;

void signal_handler( int sig )
{
	printf("g_done = true\n");
	g_done = true;
}

int rt_raisepri (int pri)
{
#ifdef _SC_PRIORITY_SCHEDULING
	struct sched_param scp;

	/*
	 * Verify that scheduling is available
	 */
	if ( sysconf( _SC_PRIORITY_SCHEDULING ) == -1)
	{
		//printf("Warning: RR-scheduler not available, disabling.\n");
		return -1;
	}
	else
	{
		memset( &scp, '\0', sizeof( scp ) );
		scp.sched_priority = sched_get_priority_max( SCHED_RR ) - pri;
		if ( sched_setscheduler( 0, SCHED_RR, &scp ) < 0 )
		{
			//printf("Warning: Cannot set RR-scheduler\n");
			return -1;
		}
	}
	return 0;

#else
	return -1;
#endif
}



//--------------------------------------------------------------------

ofDVUtils::ofDVUtils()
: dvgrab(NULL), bUseTexture(false), bGrabberInited(false), bIsFrameNew(false), pixels(NULL)
{

}


//--------------------------------------------------------------------

ofDVUtils::~ofDVUtils()
{
	delete dvgrab;
	dvgrab = NULL;
}

void ofDVUtils::initGrabber(bool _useTexture, DeinterlaceType _deinterlace, VideoType _videoType)
{

    bUseTexture = _useTexture;

    if(_videoType == OF_DV_PAL)
    {
        width = 720;
        height = 576;
    }
    else
    {
        width = 720;
        height = 480;
    }
    pixels	= new unsigned char[width * height * 3];

    if (bUseTexture){
        // create the texture, set the pixels to black and
        // upload them to the texture (so at least we see nothing black the callback)
        tex.allocate(width,height,GL_RGB);
        memset(pixels, 0, width*height*3);
        tex.loadData(pixels, width, height, GL_RGB);
    }

	fcntl( fileno( stderr ), F_SETFL, O_NONBLOCK );
	try
	{

		dvgrab = new DVgrab() ;

		if ( rt_raisepri( 1 ) != 0 ) {
			setpriority( PRIO_PROCESS, 0, -20 );
		}

#if _POSIX_MEMLOCK > 0
		if ( mlockall( MCL_CURRENT | MCL_FUTURE ) == -1 )
			//printf( "Warning: Cannot disable swapping\n" );
#endif

        deinterlace = _deinterlace;
		printf("start capture\n");
		dvgrab->startCapture();
		bGrabberInited = true;

	}
	catch ( std::string s )
	{
		printf( "*** Error: %s\n", s.c_str() );
	}
	catch ( ... )
	{
		printf( "Error: unknown\n" );
	}

}

void ofDVUtils::deinterlaceAverage( void * image, int bpp )
{
	int pixel_row = width * bpp;
	int pixel_column = height;
	uint8_t row[pixel_row];
	for ( int i = 0; i < pixel_column; i += 2 )
	{
		for(int j = 0; j < pixel_row; j++)
		{
			row[j] = (*(( uint8_t * ) image + pixel_row * ( i ) + j) + *(( uint8_t * ) image + pixel_row * ( i + 1 ) + j))/2;
		}
		//memcpy( ( uint8_t * ) image + pixel_row * ( i + 1 ), ( uint8_t * ) image + pixel_row * i, pixel_row );
		//memcpy( ( uint8_t * ) &row[0], ( uint8_t * ) image + pixel_row * i, pixel_row );
		memcpy( ( uint8_t * ) image + pixel_row * i, ( uint8_t * ) &row[0], pixel_row );
		memcpy( ( uint8_t * ) image + pixel_row * (i + 1), ( uint8_t * ) &row[0], pixel_row );
	}
}


void ofDVUtils::deinterlaceLinearBlend( void *input_image, void *output_image, int bpp )
{
	register int x, y;
	register uint8_t *l0, *l1, *l2, *l3;

    int pixel_row = width * bpp;
	int pixel_column = height;

	l0 = (uint8_t *) output_image;		/* target line */
	l1 = (uint8_t *) input_image;		/* 1st source line */
	l2 = l1 + pixel_row;	/* 2nd source line = line that follows l1 */
	l3 = l2 + pixel_row;	/* 3rd source line = line that follows l2 */

	/* Copy the first line */
	memcpy( l0, l1, pixel_row );
	l0 += pixel_row;

	for (y = 1; y < pixel_column-1; ++y)
	{
		/* computes avg of: l1 + 2*l2 + l3 */
		for ( x = 0; x < pixel_row; ++x )
			l0[x] = ( l1[ x ] + ( l2[ x ] << 1 ) + l3[ x ] ) >> 2;

		/* updates the line pointers */
		l1 = l2;
		l2 = l3;
		l3 += pixel_row;
		l0 += pixel_row;
	}

	/* Copy the last line */
	memcpy( l0, l1, pixel_row );
}
void ofDVUtils::grabFrame()
{
    if(bGrabberInited)
    {
        bIsFrameNew = dvgrab->newFrame;
        //dvgrab->newFrame = false;
        if (bIsFrameNew)
        {
            dvgrab->newFrame = false;
            if(bUseTexture) {
                switch ( deinterlace )
                {
                    case 0:
                        break;
                    case 1 :
                        deinterlaceAverage( dvgrab->pixels, 3 );
                        break;
                    case 2:
                        deinterlaceLinearBlend( dvgrab->pixels, dvgrab->pixels, 3 );
                        break;
                    case 3:
                        //dvframe->DeinterlaceBobWeave( ( uint8_t* ) image_buffer_in, ( uint8_t* ) image_buffer_prev, ( uint8_t* ) image_buffer, 3 );
                        break;
                    default :
                        break;
                }

                //if(deinterlace == 3) {
                   // memcpy ((unsigned char *) image_buffer_prev, (unsigned char *) image_buffer, 720*576*3);
               // }
                tex.loadData(dvgrab->pixels, width, height, GL_RGB);
            }
        }
    }

}

unsigned char * ofDVUtils::getPixels()
{

    if(bGrabberInited) {
        bool active = dvgrab->captureIsActive();
        if(active)
        {
            return dvgrab->pixels;
        }
    }

    return NULL;

}

bool ofDVUtils::isFrameNew()
{

    {
        return bIsFrameNew;
    }
//    else
//    {
//        if(bGrabberInited)
//        {
//            bool result = dvgrab->newFrame;
//            dvgrab->newFrame = false;
//            return result;
//        }
//        return false;
//    }
}

//------------------------------------
void ofDVUtils::draw(float _x, float _y, float _w, float _h){
	if (bUseTexture){
		tex.draw(_x, _y, _w, _h);
	}
}

//------------------------------------
void ofDVUtils::draw(float _x, float _y){
	draw(_x, _y, width, height);
}
