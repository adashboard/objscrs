#include "objscrMediaViews.hpp"


// objscrs
#include "objscrFiles.hpp"
#include "objscrStage.hpp"
#include "objscrGUIStyles.hpp"


namespace Objsc
{
	////////////////////////////////////////
	// MEDIA VIEW
	////////////////////////////////////////
	void MediaView::fullscreen()
	{
		bounds.set(0,0,ofGetWidth(),ofGetHeight());
	}
	
	void MediaView::centre()
	{
		bounds.setCentre(ofGetWidth()/2.0f,ofGetHeight()/2.0f);
	}
	
	void MediaView::setLocation(float x, float y) 
	{ 
		int scrX = 0.0f;
		int scrY = 0.0f;
		Objsc::Stage::Current()->stageToScreen(x, y, &scrX,&scrY);
		bounds.setLocation(scrX,scrY);
	}
	void MediaView::setDimensions(float width, float height) 
	{ 
		int scrW = 0.0f;
		int scrH = 0.0f;
		Objsc::Stage::Current()->stageToScreen(width, height, &scrW,&scrH);
		bounds.setDimensions(scrW,scrH); 
		//sizeToBounds();
	}
	void MediaView::setBounds(float x, float y, float width, float height) 
	{ 
		int scrX = 0.0f;
		int scrY = 0.0f;
		int scrW = 0.0f;
		int scrH = 0.0f;
		Objsc::Stage::Current()->stageToScreen(x, y, &scrX,&scrY);
		Objsc::Stage::Current()->stageToScreen(width, height, &scrW,&scrH);
		bounds.set(scrX,scrY,scrW,scrH); 
		//sizeToBounds();
	}
	
	void MediaView::setCentreX(float x)
	{
		int scrX = 0.0f;
		int scrY = 0.0f;
		Objsc::Stage::Current()->stageToScreen(x, 0.0f, &scrX,&scrY);
		bounds.setCentreX(scrX);
		//bounds.print();
	}
	
	void MediaView::setCentreY(float y)
	{
		int scrX = 0.0f;
		int scrY = 0.0f;
		Objsc::Stage::Current()->stageToScreen(0.0f, y, &scrX,&scrY);
		bounds.setCentreY(scrY);
	}
	
	void MediaView::setX(float x)
	{
		int scrX = 0.0f;
		int scrY = 0.0f;
		Objsc::Stage::Current()->stageToScreen(x, 0.0f, &scrX,&scrY);
		bounds.setX(scrX);
		//bounds.print();
	}
	
	void MediaView::setY(float y)
	{
		int scrX = 0.0f;
		int scrY = 0.0f;
		Objsc::Stage::Current()->stageToScreen(0.0f, y, &scrX,&scrY);
		bounds.setY(scrY);
	}
	
	
	void MediaView::setWidth(float width) 
	{ 
		int scrW = 0.0f;
		int scrH = 0.0f;
		Objsc::Stage::Current()->stageToScreen(width, 0.0f, &scrW,&scrH);
		bounds.setWidth(scrW); 
		//sizeToBounds();
	}
	void MediaView::setHeight(float height) 
	{ 
		int scrW = 0.0f;
		int scrH = 0.0f;
		Objsc::Stage::Current()->stageToScreen(0.0f, height, &scrW,&scrH);
		bounds.setHeight(scrH); 
		//sizeToBounds();
	}
	
	////////////////////////////////////////
	// IMAGE VIEW
	////////////////////////////////////////
	ImageView::ImageView() :media(), layer(0)
	{
		//fullscreen();
	}
	ImageView::~ImageView() {}
	
	void ImageView::draw()
	{
		if(loaded && visible)	{
			ofSetColor(0xFFFFFF);
			media.draw(bounds.getX(), bounds.getY(), bounds.getWidth(), bounds.getHeight());
		}
	}
		
	void ImageView::load(const string& filename, bool fitmedia)
	{
		if(!filename.empty())	{
			ofDisableDataPath();
			media.loadImage(Objsc::ProjectFiles::Current()->makeImagePath(filename));
			media.setImageType(OF_IMAGE_COLOR_ALPHA);
			loaded = true;
			if(fitmedia)	{
				sizeToMedia();
				centre();
			} else {
				sizeToBounds();
			}
		}
	}
	
	void ImageView::sizeToMedia()
	{
		if(loaded)	{
			bounds.setDimensions(media.width,media.height);
		}
	}
	
	//~ void ImageView::sizeToBounds()
	//~ {
		//~ if(loaded)	{
			//~ if(media.width != bounds.getWidth() || media.height != bounds.getHeight())	{
				//~ if(media.width > media.height)	{
					//~ media.resize(bounds.getWidth(),bounds.getWidth()*((float)media.height/(float)media.width));
				//~ } else {
					//~ media.resize(bounds.getHeight()*((float)media.width/(float)media.height),bounds.getHeight());
				//~ }
			//~ }
		//~ }
	//~ }
	
	////////////////////////////////////////
	// TEXT VIEW
	////////////////////////////////////////
	TextView::TextView() :widget(), layer(0)
	{
		widget.setHasMinSize(false);
		widget.setBackColour(0,0,0);
		widget.setForeColour(255,255,255);
		widget.setHilightColour(0,0,0);
		widget.setSelectedColour(0,0,0);
		widget.setDisabledColour(0,0,0);
		widget.setTextColour(255,255,255);
		ofEnableDataPath();
		font.loadFont("fonts/NotCourier-sans-Bold.ttf", 24);
		if (!font.bLoadedOk)	{
			printf("ERROR: unable to load font.\n");
		}
		ofDisableDataPath();
		widget.setFont(&font);
		widget.setLocation(0.0f, 0.0f);
		widget.sizeWithText(true);
		widget.clearText();
		visible = false;
	}
	TextView::~TextView() {}
	
	void TextView::draw()
	{
		if(visible)	{
			widget.draw();
		}
	}
	
	
	void TextView::setText(const string& newtext)
	{ 
		widget.setText(newtext);
	}
	
	void TextView::loadText(const string& filename)
	{
		
	}
		
	void TextView::setTextSize(int size)
	{
		ofEnableDataPath();
		font.loadFont("fonts/NotCourier-sans-Bold.ttf", size);
		widget.setFont(&font);
		ofDisableDataPath();
	}
	
	void TextView::sizeToMedia()
	{
		if(loaded)	{
			//bounds.setDimensions(widget.width,widget.height);
		}
	}
	
	void TextView::setX(float x)
	{
		int scrX = 0.0f;
		int scrY = 0.0f;
		Objsc::Stage::Current()->stageToScreen(x, 0.0f, &scrX,&scrY);
		widget.setX(scrX);
		//bounds.print();
	}
	
	void TextView::setY(float y)
	{
		int scrX = 0.0f;
		int scrY = 0.0f;
		Objsc::Stage::Current()->stageToScreen(0.0f, y, &scrX,&scrY);
		widget.setY(scrY);
	}
	
	void TextView::showBG() 
	{
		widget.drawBackground(true);
	}
	
	void TextView::hideBG() 
	{
		widget.drawBackground(false);
	}
	
	////////////////////////////////////////
	// VIDEO VIEW
	////////////////////////////////////////
	VideoView::VideoView() : 
		media(), 
		playing(false), 
		loops(0), 
		layer(0)
	{
		fullscreen();
		visible = true;
	}
	VideoView::~VideoView() {}
		
	void VideoView::sizeToMedia()
	{
		if(loaded)	{
			bounds.setDimensions(media.getWidth(),media.getHeight());
			//bounds.setDimensions(media.width,media.height);
		}
	}
	
	void VideoView::update()
	{
		if(media.bLoaded && visible)	{
			//printf("VideoView::update A\n");
			media.idleMovie();
			//printf("VideoView::update B\n");
		}
	}
	
	void VideoView::draw()
	{
		//~ if(loaded && visible)	{
			//~ ofSetColor(0xFFFFFF);
			//~ media.draw(bounds.getX(), bounds.getY(), bounds.getWidth(), bounds.getHeight());
		//~ }
		if(media.bLoaded && visible)	{
			//printf("VideoView::draw\n");
			//bounds.print();
			ofSetColor(0xFFFFFF);
			//media.draw(bounds.getX(), bounds.getY(), bounds.getWidth(), bounds.getHeight());
			media.draw(20,20);
		}
	}
		
	void VideoView::load(const string& filename, bool fitmedia)
	{
		if(!filename.empty())	{
			ofDisableDataPath();
			//printf("VideoView::load <%s>\n", Objsc::ProjectFiles::Current()->makeVideoPath(filename).c_str());
			media.loadMovie(Objsc::ProjectFiles::Current()->makeVideoPath(filename));
			printf("VideoView::load loaded?\n");
			if(media.bLoaded)	{
				printf("VideoView::load loaded\n");
				loaded = true;
				//media.setLoopState(OF_LOOP_NONE);
				if(fitmedia)	{
					sizeToMedia();
					centre();
				} else {
					sizeToBounds();
				}
			} else {
				loaded = false;
				printf("VideoView::load ERROR <%s>\n", filename.c_str());
			}
		}
	}
	
	void VideoView::start()
	{
		printf("VideoView::start A\n");
		if(media.bLoaded) {
			printf("VideoView::start B - loaded\n");
			//media.setPosition(0.0f);
			//media.firstFrame();
			//~ media.play();
			//~ draw();
			//~ media.idleMovie();
			//media.setPaused(false);
			printf("VideoView::start C\n");
			media.setSpeed(1.0f);
			media.play();
			//media.setPaused(false);
			//media.setSpeed(1.0f);
			printf("VideoView::start D\n");
			show();
		}
		printf("VideoView::start DONE\n");
	}
	void VideoView::stop()
	{
		if(loaded) {
			//media.setPosition(0.0f);
			//media.idleMovie();
			//media.stop();
			media.setPaused(true);
		}
	}
	
	void VideoView::setVolume(float value) 
	{
		//printf("VideoView::setVolume %f\n", value);
		if(loaded) {
			//printf("VideoView::setVolume %f loaded\n", value);
			media.setVolume(value);
		}
	}
	
	void VideoView::setSpeed(float value) 
	{
		if(loaded) {
			media.setSpeed(value);
		}
	}
	const float VideoView::getSpeed() 
	{ 
		if(loaded)
			return media.getSpeed();
		else
			return 0.0f;
	}
	
	bool VideoView::isFinished() 
	{
		if(loaded){
			//printf("media.getPosition: %f\n", media.getPosition());
			return media.getPosition()==1.0f;
		}
	}
	
}

