#ifndef OBJSCR_ZONEMAPFINDER_H
#define OBJSCR_ZONEMAPFINDER_H


// objscrs
#include "objscrFinder.hpp"
#include "objscrZoneMapEditor.hpp"


// STL
#include <string>
using std::string;


// shared_ptr
#include <tr1/memory>


namespace Objsc
{
	
	class ZoneMapFinder : public Objsc::Finder
	{
		private:
			Objsc::ItemNames items;
		
		public:
			ZoneMapFinder() : items() {}
			~ZoneMapFinder() {}
				
			void addItem(const string& itemname);
			void selectItem(const string& itemname);
			void selectItem(int index){}
			void removeItem(const string& itemname);
			void removeItem(int index){}
			void renameItem(const string& itemname, const string& newname){}
			void renameItem(int index, const string& newname){}
			const Objsc::ItemNames& getItemNames();
			
			static Objsc::FinderPtr create()
			{
				return Objsc::FinderPtr(new ZoneMapFinder());
			}
	};
	
}


#endif  //OBJSCR_ZONEMAPFINDER_H

