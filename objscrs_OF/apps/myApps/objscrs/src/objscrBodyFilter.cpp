#include "objscrBodyFilter.hpp"

//objscrs
#include "objscrIDBroker.hpp"

// STL
#include <iostream>
#include <algorithm>


void printShape(ofxCvBlob& shape)
{
	printf("shape: %f, %f, %f, %f\n", shape.boundingRect.x, shape.boundingRect.y, shape.boundingRect.width, shape.boundingRect.height);
}

namespace Objsc
{
	
	
	////////////////////////////////////////
	// FUNCTIONS
	////////////////////////////////////////
	struct bodyIsDead : std::unary_function< Objsc::BodyPtr, bool >	{
		bool operator() (const Objsc::BodyPtr& ptr) const	{
			//printf("bodyIsDead [%u] %d - %d\n", ptr->getID(), ptr->isDead(), ptr->getState());
			return ptr->isDead();
		}
	};
	
	////////////////////////////////////////
	// BODY FILTER
	////////////////////////////////////////
	void BodyFilter::process()
	{
		//printf("==\n");
		ofxCvBlobIterator shapeItem;
		ofxCvBlobIterator shapeEnd = shapes->shapesEnd();
		int shapenum = 0;
		int matches = 0;
		matchedShapes.clear();
		for(Objsc::BodyPtrList::iterator body = bodies.begin(); body != bodies.end(); ++body)	{
			(*body)->newFrame();
			shapenum = 0;
			for(shapeItem = shapes->shapesBegin();shapeItem!=shapeEnd;++shapeItem)	{
				if((*shapeItem).area > 0.0f)	{
					//printf("shapeItem area: %f\n", (*shapeItem).area);
					(*body)->compareShape((*shapeItem),shapenum++);
				}
			}
			//printf("best: %d\n", (*body)->getBestShape());
			if((*body)->getBestShape() != -1)	{
				++matches;
				(*body)->updateShape(shapes->getShape((*body)->getBestShape()));
				matchedShapes.push_back((*body)->getBestShape());
			}
		}
		//printf("matches: %d shapes: %d\n", matches, shapes->totalShapes()); 
		if(matches < shapes->totalShapes())	{
			for (int i = 0; i < shapes->totalShapes(); i++){
				if(find(matchedShapes.begin(), matchedShapes.end(), i) == matchedShapes.end())	{
					//printf("no match: %d\n", i); 
					createBody(shapes->getShape(i));
				}
			}
		}
	}
	
	void BodyFilter::update()
	{
		//printf("===============================\n");
		process();
		for(Objsc::BodyPtrList::iterator body = bodies.begin(); body != bodies.end(); ++body)	{
			(*body)->update();
		}
		prune();
		cull();
	}
	
	void BodyFilter::prune()
	{
		for(Objsc::BodyPtrList::iterator body = bodies.begin(); body != bodies.end(); ++body)	{
			for(Objsc::BodyPtrList::iterator other = bodies.begin(); other != bodies.end(); ++other)	{
				//if((*other) != (*body))	{
				if((*body)->matchesArea((*other)))	{
					//printf("BODY:\n");
					//(*body)->print();
					//(*other)->print();
					if((*body)->getAge() >= (*other)->getAge() && (*body)->getUID() < (*other)->getUID())	{
						(*other)->kill();
					}
				}
				//}
			}
		}
	}
	
	void BodyFilter::cull()
	{
		if(!bodies.empty())	{
			bodies.erase(remove_if(bodies.begin(), bodies.end(), bodyIsDead()), bodies.end());
			if(bodies.empty())	{
				Objsc::IDBroker::Current()->reset();
			}
		}
	}
	
	void BodyFilter::clear()
	{
		bodies.clear();
		Objsc::IDBroker::Current()->reset();
	}
	
	void BodyFilter::createBody(const ofxCvBlob& shape)
	{
		//std::cout << "BodyFilter::createBody A" << std::endl;
		if (shape.area > 0.0f)	{
			//printf("shapeItem area: %f\n", shape.area);
			Objsc::BodyPtr body = Objsc::Body::create();
			body->updateShape(shape);
			body->setID(Objsc::IDBroker::Current()->newID());
			bodies.push_back(body);
//                        std::cout << "BodyFilter::createBody B" << std::endl;
		}
	}
}

