#ifndef  OBJSCR_DATATYPES_H
#define OBJSCR_DATATYPES_H


#include <string>
#include <vector>
using std::string;
using std::vector;

namespace Objsc
{
	typedef vector< string > StringVector;
	typedef vector < float > FloatVec;
	typedef vector < int > IntVec;
}

#endif // OBJSCR_DATATYPES_H
