#include "objscrBodyViews.hpp"

// openframeworks
#include "ofMain.h"

// objscrs
#include "objscrGUIStyles.hpp"
#include "objscrStage.hpp"
#include "objscrGUIRoot.hpp"
#include "objscrSettings.hpp"

// STL
#include <algorithm>
#include <iostream>


namespace Objsc
{
	
	////////////////////////////////////////
	// DECLARATIONS
	////////////////////////////////////////
	const string Objsc::BodyView::stylename = "body";
	
	
	////////////////////////////////////////
	// ZONE VIEW
	////////////////////////////////////////
	BodyView::BodyView() : 
		body(), 
		centroid(),
		scrX(0), 
		scrY(0), 
		namefield(),
		//stagenumfield(),
		editmenu()
	{
		Objsc::StylePalette::Current()->applyStyle(Objsc::BodyView::stylename, &namefield);
		namefield.clearText();
		//Objsc::StylePalette::Current()->applyStyle(Objsc::BodyView::stylename, &stagenumfield);
		//stagenumfield.clearText();
		Objsc::StylePalette::Current()->applyStyle(Objsc::BodyView::stylename, this);
	}
	
	void BodyView::setBody(Objsc::BodyPtr bodyPtr) 
	{
		body=bodyPtr;
		//printf("BodyView::setBody %u\n", body->getID());
		//char cstr[256];
		//sprintf(cstr, "body %u", body->getID());
		//sprintf(cstr, "body %u", body->getStageNum());
		//namefield.setText(string(cstr));
		namefield.setText(ofToString((int)(body->getStageNum())));
		//namefield.setText(body->getName());
		//mapToScreen();
	}
	
	//~ void BodyView::mapToScreen()
	//~ {
		//~ if(body)	{
			//~ int x=0;
			//~ int y=0;
			//~ int w=0;
			//~ int h=0;
			//~ const Objsc::Rect zrect = body->getBounds();
			//~ Objsc::Stage::Current()->stageToScreen(zrect.getX(), zrect.getY(), &x,&y);
			//~ Objsc::Stage::Current()->stageToScreen(zrect.getWidth(), zrect.getHeight(),&w,&h);
			//~ bounds.set(x,y,w,h);
			//~ namefield.setLocation(bounds.getX(), bounds.getY());
		//~ }
	//~ }
	
	
	//~ void BodyView::mapToStage()
	//~ {
		//~ if(body)	{
			//~ float x=0.0f;
			//~ float y=0.0f;
			//~ float w=0.0f;
			//~ float h=0.0f;
			//~ Objsc::Stage::Current()->screenToStage(bounds.getX(), bounds.getY(), &x,&y);
			//~ Objsc::Stage::Current()->screenToStage(bounds.getWidth(), bounds.getHeight(),&w,&h);
			//~ body->setBounds(x,y,w,h);
		//~ }
	//~ }
	
	
	void BodyView::setLocation(float x, float y)
	{
		bounds.setLocation(x,y);
		boundsResized();
	}
	
	void BodyView::setDimensions(float width, float height)
	{
		
	}
	
	void BodyView::setBounds(float x, float y, float width, float height)
	{
		
	}
	
	void BodyView::boundsResized()
	{
		//mapToStage();
		namefield.setLocation(bounds.getX(), bounds.getY());	
	}
	
	void BodyView::update()
	{
		const Objsc::Rect& sr  = body->getStageRect();
		bounds.set(stageToScreen(sr.getX(), ofGetWidth()),
					stageToScreen(sr.getY(), ofGetHeight()),
					stageToScreen(sr.getWidth(), ofGetWidth()),
					stageToScreen(sr.getHeight(), ofGetHeight()));
		centroid.x = stageToScreen(body->getStageX(), ofGetWidth());
		centroid.y = stageToScreen(body->getStageY(), ofGetHeight());
		namefield.setLocation(bounds.getX(), bounds.getY());
		switch(body->getState())	{
			case UNBORN:
				namefield.setTextColour(0,255,0);
				break;
			case BECOMING:
				namefield.setTextColour(0,255,0);
				break;
			case PRESENT:
				namefield.setTextColour(255,120,0);
				break;
			case ABSENT:
				namefield.setTextColour(0,120,255); 
				break;
			case DEAD:
				namefield.setTextColour(120,120,120); 
				break;
		}
	}
	
	void BodyView::draw()
	{
		if (visible)	{
			if(hasFocus)	{
				ofSetColor(bgColour.r, bgColour.g, bgColour.b, 30);
				ofRect(bounds.getX(), bounds.getY(), bounds.getWidth(), bounds.getHeight());
				ofSetColor(bgColour.r, bgColour.g, bgColour.b);
				ofNoFill();
				
				ofFill();
			}
			ofFill();
			ofSetColor(bgColour.r, bgColour.g, bgColour.b, 120);
			ofRect(bounds.getX(), bounds.getY(), bounds.getWidth(), bounds.getHeight());
			ofSetColor(bgColour.r, bgColour.g, bgColour.b, 255);
			ofLine(centroid.x, bounds.getY(), centroid.x, bounds.getBottom());
			ofLine(bounds.getX(), centroid.y, bounds.getRight(), centroid.y);
			ofNoFill();
			ofEnableSmoothing();
			ofCircle(centroid.x, centroid.y, 10.0f);
			ofDisableSmoothing();
			ofFill();
			namefield.draw();

                        int vSize(body->getBodyPoints().size());
                        float vScale(float(Objsc::Settings::Current()->getDisplayWidth()) / float(Objsc::VIDSOURCE_WIDTH));
//                        std::cout << "Scale " << vScale << std::endl;
                        for(int p(0); p < vSize ; ++p)
                        {
                            ofPoint cP(body->getBodyPoints().at(p));
//                            std::cout << "Point[" << p <<"] "<< cP.x << " ; " << cP.y << std::endl;
                            ofCircle(float(cP.x) * vScale, float(cP.y) * vScale, 1.0 * vScale);
                        }
                        ofPoint curP;
                        bool firstP(true);
                        for(int p(0); p < vSize ; ++p)
                        {
                            ofPoint cP(body->getBodyPoints().at(p));
                            if(firstP)
                                firstP = false;
                            else
                                ofLine(float(curP.x) * vScale, float(curP.y) * vScale,
                                     float(cP.x) * vScale, float(cP.y) * vScale);
                            curP = cP;
                        }
		}
	}
	
	
	void BodyView::keyPressed(int key)
	{
		
	}
	
	void BodyView::keyReleased(int key)
	{
		
	}
	
	void BodyView::mouseMoved(int x, int y )
	{
		if(withinBounds(x,y))	{
			if(!hasFocus) {
				gainFocus();
			}
		} else {
			if(hasFocus) {
				looseFocus();
			}
		}
	}
	
	void BodyView::mouseDragged(int x, int y, int button)
	{
		consumed = false;
		float dX = x-scrX;
		float dY = y-scrY;
		
		scrX = x;
		scrY = y;
	}
	
	void BodyView::mousePressed(int x, int y, int button)
	{
		consumed = false;
		if(withinBounds(x,y))	{
			scrX = x;
			scrY = y;
			switch(button)	{
				case 0:
					consumed = true;
					break;
				case 1:
					break;
				case 2:
					consumed = true;
					openMenu(x, y);
					break;
			}
		}
	}
	
	void BodyView::mouseReleased(int x, int y, int button)
	{
		
	}
	
	void BodyView::windowResized(int w, int h)
	{
		
	}
	
	void BodyView::openMenu(int x, int y)
	{
		//~ if(!editmenu)	{
			//~ editmenu = Objsc::ContextMenu::create();
			//~ Objsc::CommandPtr actionPatch = Objsc::CommandDispatcher::createCommand(Objsc::BodyMapEditor::cmdlabel , "body_patch");
			//~ actionPatch->getData()->addArg(Objsc::BodyViewPtr(this));
			//~ editmenu->addItem("patch", actionPatch);
			//~ Objsc::CommandPtr actionRename = Objsc::CommandDispatcher::createCommand(Objsc::BodyMapEditor::cmdlabel , "body_rename");
			//~ actionRename->getData()->addArg(Objsc::BodyViewPtr(this));
			//~ editmenu->addItem("rename", actionRename);
			//~ Objsc::CommandPtr actionDelete = Objsc::CommandDispatcher::createCommand(Objsc::BodyMapEditor::cmdlabel , "body_delete");
			//~ actionDelete->getData()->addArg(Objsc::BodyViewPtr(this));
			//~ editmenu->addItem("delete", actionDelete);
		//~ }
		//~ Objsc::GUIRoot::Current()->openRootMenu(editmenu, x, y);
	}
	
}

