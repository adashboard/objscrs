#ifndef  OBJSCR_SHAPEFILTER_H
#define OBJSCR_SHAPEFILTER_H


// objscrs
#include "objscrFrames.hpp"
#include "objscrCommands.hpp"


// openframeworks
#include "ofxOpenCv.h"

// shared_ptr
#include <tr1/memory>

typedef vector<ofxCvBlob>::iterator ofxCvBlobIterator;

namespace Objsc
{
	////////////////////////////////////////
	// SHAPE FILTER
	////////////////////////////////////////
	class ShapeFilter : public CommandReceiver
	{
		protected:
			Objsc::FrameSourcePtr 	input;
			bool				active;
			ofxCvColorImage		inputImg;
			ofxCvColorImage		colorImg;
			ofxCvGrayscaleImage 	grayImage;
			ofxCvGrayscaleImage 	grayBg;
			ofxCvGrayscaleImage 	grayDiff;
			ofxCvContourFinder 		contourFinder;
			int 					threshold;
			int					inWidth;
			int					inHeight;
			bool				continuous;
			bool				resize;
			int					minRadius;
			int					maxRadius;
		
			void process();
		
		public:
			ShapeFilter();
			virtual ~ShapeFilter() {}
				
			void setInput(Objsc::FrameSourcePtr inputPtr);
			void setThreshold(int value) { threshold = value>255?255:(value<0?0:value);}
			
			void start();
			void stop() 
			{ 
				active=false;
			}
			void update(){if(active) {process();}}
			void draw(); // pos tmp
			
			ofxCvBlobIterator shapesBegin() { return contourFinder.blobs.begin();}
			ofxCvBlobIterator shapesEnd() { return contourFinder.blobs.end();}
			const bool hasShapes() const {return !contourFinder.blobs.empty();}
			const ofxCvBlob& getShape(int idx);
			const int totalShapes() const { return contourFinder.nBlobs; }
			
			void takeRef();
			void startRef();
			void stopRef();
			
			void setMinRadius(int value) { minRadius = value<0?0:value; }
			void setMaxRadius(int value) { maxRadius = value<0?0:value; }
			
			// commands
			void doCommand(Objsc::CommandPtr& cmd);
			void undoCommand(Objsc::CommandPtr& cmd);
			
			static std::tr1::shared_ptr<Objsc::ShapeFilter> create()
			{
				return std::tr1::shared_ptr<Objsc::ShapeFilter>(new ShapeFilter());
			}
			static const string	cmdlabel;
	};
	
	typedef std::tr1::shared_ptr<Objsc::ShapeFilter> ShapeFilterPtr;
}

#endif // OBJSCR_SHAPEFILTER_H
