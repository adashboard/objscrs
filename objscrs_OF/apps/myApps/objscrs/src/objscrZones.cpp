#include "objscrZones.hpp"

// objscrs
#include "objscrConstants.hpp"
#include "objscrLangAtoms.hpp"

// STL
#include <algorithm>
#include <functional>

namespace Objsc
{
	
	////////////////////////////////////////
	// DECLARATIONS
	////////////////////////////////////////
	const string Objsc::BasicZone::zonetype = "basiczone";
	const string Objsc::CircleZone::zonetype = "circlezone";
	
	
	/////////////////////////////////////
	// FUNCTIONS
	/////////////////////////////////////
	void initZones()
	{
		Objsc::ZoneFactory* factory = Objsc::ZoneFactory::Current();
		factory->addCreator(Objsc::BasicZone::zonetype, new Objsc::BasicZoneCreator());
		factory->addCreator(Objsc::CircleZone::zonetype, new Objsc::CircleZoneCreator());
	}
	
	struct matchesBody : std::binary_function< Objsc::BodyHolder, Objsc::BodyPtr, bool >	{
		bool operator() (const Objsc::BodyHolder& holder, const Objsc::BodyPtr& body) const	{
			if(holder.getBody() == body)	{
				holder.update();
				return true;
			}
			return false;
		}
	};
	
	inline void resetBodyHolder(Objsc::BodyHolder& holder)
	{
		holder.reset();
	}
	
	inline bool bodyHasLeft(Objsc::BodyHolder& holder)
	{
		return !holder.isIn();
	}
	
	
	Objsc::LangPatchPtr createZonePatch(const string& name)
	{
		Objsc::LangPatchPtr patch = Objsc::LangPatch::create(name);
		patch->addObjectToReceiver("body_enter", patch->createObject("body_enter", 0.1f, 0.1f));
		patch->addObjectToReceiver("body_within", patch->createObject("body_within", 0.4f, 0.1f));
		patch->addObjectToReceiver("body_exit", patch->createObject("body_exit", 0.7f, 0.1f));
		return patch;
	}
	
	
	
	Objsc::LangAtomPtr proxDataToAtom(const Objsc::BodyPtr& body, int numbodies, float prox)
	{
		Objsc::LangList* atom = new Objsc::LangList();
		atom->add((int)(body->getID()));
		atom->add(body->getStageX());
		atom->add(body->getStageY());
		atom->add(body->getStageArea());
		atom->add(body->getBodyArea());
		atom->add(numbodies);
		atom->add(prox);
		return Objsc::LangAtomPtr(atom);
	}
	
	
	Objsc::LangAtomPtr boundsToAtom(const Objsc::Rect& bounds)
	{
		Objsc::LangList* atom = new Objsc::LangList();
		atom->add(bounds.getX());
		atom->add(bounds.getY());
		atom->add(bounds.getWidth());
		atom->add(bounds.getHeight());
		return Objsc::LangAtomPtr(atom);
	}
	
	////////////////////////////////////////
	// BASIC ZONE
	////////////////////////////////////////
	BasicZone::BasicZone():name(),bounds(),patch(),bodies(),bodyAtom()
	{
		bounds.set(0,0, Objsc::ZONE_DFTWIDTH, Objsc::ZONE_DFTHEIGHT);
	}
	
	Objsc::LangPatchPtr BasicZone::getPatch()
	{
		if(!patch)	{
			patch = createZonePatch(name);
		}
		return patch;
	}
	
	void BasicZone::receive(const string& label, Objsc::LangAtomPtr atom)
	{
		//printf("BasicZone::receive <%s> A\n", label.c_str());
		if(patch)	{
			//printf("BasicZone::receive <%s> B\n", label.c_str());
			patch->receive(label, atom);
		}
	}
	
	void BasicZone::onload()
	{
		if(patch)	{
			patch->onload();
		}
	}
	
	void BasicZone::update(int timeElapsed)
	{
		if(patch)	{
			patch->update(timeElapsed);
		}
	}
	
	// bodies
	void BasicZone::updateBodies(const Objsc::BodyPtrList&  bodylist)
	{
		//printf("BasicZone::updateBodies <%s>\n", name.c_str());
		//for_each(bodies.begin(), bodies.end(), resetBodyHolder);
		for(Objsc::BodyHolderVec::iterator holder = bodies.begin(); holder != bodies.end(); ++holder)	{
			(*holder).reset();
		}
		for(Objsc::BodyPtrList::const_iterator body = bodylist.begin(); body != bodylist.end(); ++body)	{
			//printf("    body [%u]\n", (*body)->getID());
			if((*body)->isAlive())	{
				if(withinBounds((*body)->getStageX(), (*body)->getStageY()))	{
					//printf("    body [%u] INSIDE\n", (*body)->getID());
					if(hasBody((*body)))	{
						bodyWithin((*body));
					} else {
						bodyEnter((*body));
					}
				}
			}
		}
		for(Objsc::BodyHolderVec::iterator holder = bodies.begin(); holder != bodies.end(); ++holder)	{
			if(!(*holder).isIn())	{
				bodyExit((*holder).getBody());
			}
		}
		bodies.erase(remove_if(bodies.begin(), bodies.end(), bodyHasLeft), bodies.end());
	}
	
	bool BasicZone::hasBody(const Objsc::BodyPtr& body)
	{
		return find_if(bodies.begin(), bodies.end(), bind2nd(matchesBody(), body)) != bodies.end();
	}
	
	void BasicZone::bodyEnter(Objsc::BodyPtr body)
	{
		bodies.push_back(Objsc::BodyHolder(body));
		if(patch)	{
			patch->receive("zonespace", Objsc::boundsToAtom(bounds));
			patch->receive("body_enter", Objsc::bodyToLangAtom(body, bodies.size()));
		}
	}
	
	void BasicZone::bodyExit(Objsc::BodyPtr body)
	{
		if(patch)	{
			patch->receive("body_exit", Objsc::bodyToLangAtom(body, bodies.size()));
		}
	}
	
	void BasicZone::bodyWithin(Objsc::BodyPtr body)
	{
		if(patch)	{
			patch->receive("body_within", Objsc::bodyToLangAtom(body, bodies.size()));
		}
	}
	
	// patch
	void BasicZone::setName(const string& nname) 
	{ 
		name.assign(nname);
		if(patch)	{
			patch->setName(nname);
		}
	}
	
	
	////////////////////////////////////////
	// CIRCLE ZONE
	////////////////////////////////////////
	CircleZone::CircleZone() : circle()
	{
		
	}
	
	void CircleZone::setLocation(float x, float y) 
	{ 
		bounds.setCentre(x,y); 
		circle.setCentre(x,y);
	}
	void CircleZone::setDimensions(float width, float height) 
	{ 
		if(width < height) {
			height = width;
		} else {
			width = height;
		}
		bounds.setDimensions(width,height);
		circle.setRadius(width/2.0f);
	}
	void CircleZone::setBounds(float x, float y, float width, float height) 
	{ 
		if(width > height) {
			height = width;
		} else {
			width = height;
		}
		bounds.set(x,y,width,height);
		circle.set(x,y,width/2.0f);
	}
	
	void CircleZone::bodyEnter(Objsc::BodyPtr body)
	{
		bodies.push_back(Objsc::BodyHolder(body));
		if(patch)	{
			patch->receive("body_enter", Objsc::proxDataToAtom(body, bodies.size(), proximity(body)));
		}
	}
	
	void CircleZone::bodyExit(Objsc::BodyPtr body)
	{
		if(patch)	{
			patch->receive("body_exit", Objsc::proxDataToAtom(body, bodies.size(), proximity(body)));
		}
	}
	
	void CircleZone::bodyWithin(Objsc::BodyPtr body)
	{
		//printf("bodyWithin %f\n", proximity(body));
		if(patch)	{
			patch->receive("body_within", Objsc::proxDataToAtom(body, bodies.size(), proximity(body)));
		}
	}
	
}

