#ifndef  OBJSCR_LANGMATHOBJECTS_H
#define OBJSCR_LANGMATHOBJECTS_H

// objscrs
#include "objscrLangAtoms.hpp"
#include "objscrLangObjects.hpp"
#include "objscrConstants.hpp"

// STL
#include <string>
using std::string;


// shared_ptr
#include <tr1/memory>


	
namespace Objsc
{
	
	/////////////////////////////////////
	// FUNCTIONS
	/////////////////////////////////////
	void initLangMathObjects();
	
	
	/////////////////////////////////////
	// MATH OBJECT
	/////////////////////////////////////
	class MathObject : public LangObject
	{
		protected:
			float	value;
			string	label;
		
			virtual void eval(float input) =0;
		
			virtual void receiveList(int inlet, LangList* value);
		
		public:
			MathObject();
			virtual ~MathObject() {}
			
			virtual void setValue(float nvalue);
			
			bool parseArgs(Objsc::LangList* args);
			void setLabel(string nlabel);
			const string& getLabel() const { return label; }
			
			virtual void receive(int inlet, Objsc::LangAtom* atom);
			virtual void activate() {}
	};
	
	/////////////////////////////////////
	// EQUALS OBJECT
	/////////////////////////////////////
	class EqualsObject : public MathObject
	{
		protected:
			void eval(float input);
		
		public:
			EqualsObject()
			{
				setInlets(2);
				setOutlets(2);
			}
			~EqualsObject() {}
				
			static const string langname;
			const string& getLangName() const { return Objsc::EqualsObject::langname; }

			
			//void setValue(float nvalue);
	};
	
	/////////////////////////////////////
	// CHANGE OBJECT
	/////////////////////////////////////
	class ChangeObject : public MathObject
	{
		protected:
			void eval(float input);
		
		public:
			ChangeObject()
			{
				setInlets(2);
				setOutlets(2);
			}
			~ChangeObject() {}
				
			static const string langname;
			const string& getLangName() const { return Objsc::ChangeObject::langname; }

	};
	
	/////////////////////////////////////
	// GREATER THAN OBJECT
	/////////////////////////////////////
	class GreaterThanObject : public MathObject
	{
		protected:
			void eval(float input);
		
		public:
			GreaterThanObject()
			{
				setInlets(2);
				setOutlets(2);
			}
			~GreaterThanObject() {}
				
			static const string langname;
			const string& getLangName() const { return Objsc::GreaterThanObject::langname; }
	};
	
	/////////////////////////////////////
	// LESS THAN OBJECT
	/////////////////////////////////////
	class LessThanObject : public MathObject
	{
		protected:
			void eval(float input);
		
		public:
			LessThanObject()
			{
				setInlets(2);
				setOutlets(2);
			}
			~LessThanObject() {}
				
			static const string langname;
			const string& getLangName() const { return Objsc::LessThanObject::langname; }
			
			//void setValue(float nvalue);
	};
	
	
	/////////////////////////////////////
	// ADD OBJECT
	/////////////////////////////////////
	class AddObject : public MathObject
	{
		protected:
			void eval(float input);
		
		public:
			AddObject()
			{
				setInlets(2);
				setOutlets(1);
			}
			~AddObject() {}
				
			static const string langname;
			const string& getLangName() const { return Objsc::AddObject::langname; }
			
			//void setValue(float nvalue);
	};
	
	/////////////////////////////////////
	// MINUS OBJECT
	/////////////////////////////////////
	class MinusObject : public MathObject
	{
		protected:
			void eval(float input);
		
		public:
			MinusObject()
			{
				setInlets(2);
				setOutlets(1);
			}
			~MinusObject() {}
				
			static const string langname;
			const string& getLangName() const { return Objsc::MinusObject::langname; }
			
			//void setValue(float nvalue);
	};
	
	/////////////////////////////////////
	// MULTIPLY OBJECT
	/////////////////////////////////////
	class MultiplyObject : public MathObject
	{
		protected:
			void eval(float input);
		
		public:
			MultiplyObject()
			{
				setInlets(2);
				setOutlets(1);
			}
			~MultiplyObject() {}
				
			static const string langname;
			const string& getLangName() const { return Objsc::MultiplyObject::langname; }
			
			//void setValue(float nvalue);
	};
	
	/////////////////////////////////////
	// DIVIDE OBJECT
	/////////////////////////////////////
	class DivideObject : public MathObject
	{
		protected:
			void eval(float input);
		
		public:
			DivideObject()
			{
				setInlets(2);
				setOutlets(1);
			}
			~DivideObject() {}
				
			static const string langname;
			const string& getLangName() const { return Objsc::DivideObject::langname; }
			
			//void setValue(float nvalue);
	};
	
	/////////////////////////////////////
	// MODULO OBJECT
	/////////////////////////////////////
	class ModuloObject : public MathObject
	{
		protected:
			void eval(float input);
		
		public:
			ModuloObject()
			{
				setInlets(2);
				setOutlets(1);
			}
			~ModuloObject() {}
				
			static const string langname;
			const string& getLangName() const { return Objsc::ModuloObject::langname; }
			
			//void setValue(float nvalue);
	};
	
	/////////////////////////////////////
	// MAX OBJECT
	/////////////////////////////////////
	class MaxObject : public MathObject
	{
		protected:
			float limit;
			void eval(float input);
		
			void receiveList(int inlet, LangList* value);
		
		public:
			MaxObject()
			{
				setInlets(2);
				setOutlets(2);
			}
			~MaxObject() {}
				
			static const string langname;
			const string& getLangName() const { return Objsc::MaxObject::langname; }
			
			void receive(int inlet, Objsc::LangAtom* atom);
			void activate();
			void reset();
	};
	
	/////////////////////////////////////
	// MIN OBJECT
	/////////////////////////////////////
	class MinObject : public MathObject
	{
		protected:
			float limit;
			void eval(float input);
		
			void receiveList(int inlet, LangList* value);
		
		public:
			MinObject()
			{
				setInlets(2);
				setOutlets(2);
			}
			~MinObject() {}
				
			static const string langname;
			const string& getLangName() const { return Objsc::MinObject::langname; }
			
			void receive(int inlet, Objsc::LangAtom* atom);
			void activate();
			void reset();
	};
	
	/////////////////////////////////////
	// INVERT OBJECT
	/////////////////////////////////////
	class InvertObject : public MathObject
	{
		protected:
			void eval(float input);
		
		public:
			InvertObject()
			{
				setInlets(1);
				setOutlets(1);
			}
			~InvertObject() {}
				
			static const string langname;
			const string& getLangName() const { return Objsc::InvertObject::langname; }
			
			void setValue(float nvalue) {}
	};
	
	/////////////////////////////////////
	// CEILING OBJECT
	/////////////////////////////////////
	class CeilingObject : public MathObject
	{
		protected:
			void eval(float input);
		
		public:
			CeilingObject()
			{
				setInlets(2);
				setOutlets(1);
			}
			~CeilingObject() {}
				
			static const string langname;
			const string& getLangName() const { return Objsc::CeilingObject::langname; }
			
			//void setValue(float nvalue);
	};
	
	/////////////////////////////////////
	// FLOOR OBJECT
	/////////////////////////////////////
	class FloorObject : public MathObject
	{
		protected:
			void eval(float input);
		
		public:
			FloorObject()
			{
				setInlets(2);
				setOutlets(1);
			}
			~FloorObject() {}
				
			static const string langname;
			const string& getLangName() const { return Objsc::FloorObject::langname; }
			
			//void setValue(float nvalue);
	};
	
	/////////////////////////////////////
	// AVERAGE OBJECT
	/////////////////////////////////////
	class AverageObject : public LangObject
	{
		protected:
			float sum;
			float avg;
			float count;
		
			void receiveList(int inlet, LangList* value);
		
		public:
			AverageObject();
			~AverageObject() {}
			
			static const string langname;
			const string& getLangName() const { return Objsc::AverageObject::langname; }
			
			void receive(int inlet, Objsc::LangAtom* atom);
			void activate();
			void reset();
			void add(float value);
	};
	
	/////////////////////////////////////
	// RANDOM OBJECT
	/////////////////////////////////////
	class RandomObject : public LangObject
	{
		protected:
			float lower;
			float upper;
		
			void receiveList(int inlet, LangList* value);
		
		public:
			RandomObject();
			~RandomObject() {}
			
			static const string langname;
			const string& getLangName() const { return Objsc::RandomObject::langname; }
			
			void receive(int inlet, Objsc::LangAtom* atom);
			void activate();
	};
	
	
	/////////////////////////////////////
	// INT OBJECT
	/////////////////////////////////////
	class IntObject : public MathObject
	{
		protected:
			int		intvalue;
			void eval(float input);
		
		public:
			IntObject() : intvalue(0)
			{
				setInlets(2);
				setOutlets(1);
			}
			~IntObject() {}
				
			static const string langname;
			const string& getLangName() const { return Objsc::IntObject::langname; }
			
			void activate();
			void setValue(float nvalue);
	};
	
	////////////////////////////////////////
	// CREATORS
	////////////////////////////////////////
	class EqualsObjectCreator : public LangObjectCreator
	{
		public:
			Objsc::LangObjectPtr create() const { return Objsc::LangObjectPtr(new Objsc::EqualsObject());}
	};
	
	class GreaterThanObjectCreator : public LangObjectCreator
	{
		public:
			Objsc::LangObjectPtr create() const { return Objsc::LangObjectPtr(new Objsc::GreaterThanObject());}
	};
	
	class LessThanObjectCreator : public LangObjectCreator
	{
		public:
			Objsc::LangObjectPtr create() const { return Objsc::LangObjectPtr(new Objsc::LessThanObject());}
	};
	
	class AddObjectCreator : public LangObjectCreator
	{
		public:
			Objsc::LangObjectPtr create() const { return Objsc::LangObjectPtr(new Objsc::AddObject());}
	};
	
	class MinusObjectCreator : public LangObjectCreator
	{
		public:
			Objsc::LangObjectPtr create() const { return Objsc::LangObjectPtr(new Objsc::MinusObject());}
	};
	
	class MultiplyObjectCreator : public LangObjectCreator
	{
		public:
			Objsc::LangObjectPtr create() const { return Objsc::LangObjectPtr(new Objsc::MultiplyObject());}
	};
	
	class DivideObjectCreator : public LangObjectCreator
	{
		public:
			Objsc::LangObjectPtr create() const { return Objsc::LangObjectPtr(new Objsc::DivideObject());}
	};
	
	class ModuloObjectCreator : public LangObjectCreator
	{
		public:
			Objsc::LangObjectPtr create() const { return Objsc::LangObjectPtr(new Objsc::ModuloObject());}
	};
	
	class InvertObjectCreator : public LangObjectCreator
	{
		public:
			Objsc::LangObjectPtr create() const { return Objsc::LangObjectPtr(new Objsc::InvertObject());}
	};
	
	class AverageObjectCreator : public LangObjectCreator
	{
		public:
			Objsc::LangObjectPtr create() const { return Objsc::LangObjectPtr(new Objsc::AverageObject());}
	};
	
	class CeilingObjectCreator : public LangObjectCreator
	{
		public:
			Objsc::LangObjectPtr create() const { return Objsc::LangObjectPtr(new Objsc::CeilingObject());}
	};
	
	class FloorObjectCreator : public LangObjectCreator
	{
		public:
			Objsc::LangObjectPtr create() const { return Objsc::LangObjectPtr(new Objsc::FloorObject());}
	};
	
	class RandomObjectCreator : public LangObjectCreator
	{
		public:
			Objsc::LangObjectPtr create() const { return Objsc::LangObjectPtr(new Objsc::RandomObject());}
	};
	
	class MaxObjectCreator : public LangObjectCreator
	{
		public:
			Objsc::LangObjectPtr create() const { return Objsc::LangObjectPtr(new Objsc::MaxObject());}
	};
	
	class MinObjectCreator : public LangObjectCreator
	{
		public:
			Objsc::LangObjectPtr create() const { return Objsc::LangObjectPtr(new Objsc::MinObject());}
	};
	
	
	class ChangeObjectCreator : public LangObjectCreator
	{
		public:
			Objsc::LangObjectPtr create() const { return Objsc::LangObjectPtr(new Objsc::ChangeObject());}
	};
	
	class IntObjectCreator : public LangObjectCreator
	{
		public:
			Objsc::LangObjectPtr create() const { return Objsc::LangObjectPtr(new Objsc::IntObject());}
	};
	

}

#endif // OBJSCR_LANGMATHOBJECTS_H


