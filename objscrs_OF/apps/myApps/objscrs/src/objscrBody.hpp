#ifndef  OBJSCR_BODY_H
#define OBJSCR_BODY_H


// objscrs
#include "objscrGeom.hpp"
#include "objscrCommands.hpp"
#include "objscrLangAtoms.hpp"

// openframeworks
#include "ofxOpenCv.h"

// shared_ptr
#include <tr1/memory>

// STL
#include <string>
#include <list>
#include <set>
using std::string;
using std::list;
using std::set;

namespace Objsc
{
	
	enum bodystate
	{
		UNBORN,
		BECOMING,
		PRESENT,
		ABSENT,
		DEAD
	};
	
	
	////////////////////////////////////////
	// BODY
	////////////////////////////////////////
	class Body
	{
		protected:
			unsigned int			uid;
			unsigned int			stageNum;
			int					age;
			int					lasthit;
			int					hits;
			int					bestShape;
			float 				bestdiffarea;
			float				bestdist;
			float				maxdist;
			bodystate			state;
			Objsc::Rect			screenRect;
			Objsc::Rect			stageRect;
			Objsc::Rect			shapeRect;
			ofPoint             		screenLoc;
			ofPoint             		stageLoc;
			ofPoint             		shapeLoc;
			float				velocity;
			float				bearing;
			float				area;
                        vector <ofPoint>                pts;
			string				label;
			Objsc::CommandPtr		actionPresent;
			Objsc::CommandPtr 	actionDead;
			
			
			void enterState(bodystate newstate);
			void initActions(std::tr1::weak_ptr<Objsc::Body> weakPtr);
		
		public:
			Body();
			~Body() {}
			const Objsc::Rect& getScreenRect() const { return screenRect; }
			const Objsc::Rect& getStageRect() const { return stageRect; }
			const ofPoint& getScreenLoc() const { return screenLoc; }
			const ofPoint& getStageLoc() const { return stageLoc; }
			const float getStageX() const { return stageLoc.x;}
			const float getStageY() const { return stageLoc.y;}
			const float getVelocity() const { return velocity; }
			const float getBearing() const { return bearing; }
			const bodystate getState() const { return state; }
			const bool isAlive() const { return (state == Objsc::PRESENT || state == Objsc::ABSENT); }
			const bool isDead() const { return state == Objsc::DEAD; }
			const int getAge() const { return age; }
			const int getUID() const { return uid; }
			void kill() { enterState(Objsc::DEAD); }
			
			void setID(unsigned int nid) { uid=nid; }
			const unsigned int getID() const { return uid;}
			const unsigned int getStageNum() const { return stageNum;}
			
			void setLabel(string nlabel) { label=nlabel;}
			const string& getLabel() const { return label;}
			
			void update();
			void newFrame();
			void compareShape(const ofxCvBlob& shape, int shapenum);
			const int getBestShape() const { return bestShape; }
			void updateShape(const ofxCvBlob& shape);
			
			bool matchesArea(std::tr1::shared_ptr<Objsc::Body> other) { return stageRect.matchesAsInt(other->getStageRect()); }
			
			void setScreenRect(float x, float y, float width, float height){screenRect.set(x,y,width,height);}
			
			void setScreenLoc(float x, float y)
			{
				screenLoc.x=x;
				screenLoc.y=y;
			}
			
			const float getX() const { return shapeLoc.x;}
			const float getY() const { return shapeLoc.y;}
			const Objsc::Rect& getShapeRect() const { return shapeRect; }
			const float getStageArea() const { return stageRect.getArea(); }
			const float getShapeArea() const { return shapeRect.getArea(); }
			const float getScreenArea() const { return screenRect.getArea(); }
			const float getBodyArea() const { return area; }
                        const vector<ofPoint>& getBodyPoints() const {return pts;}
			
			void print();
			
			static void resetThresholds();
			static void printThresholds();
			
			static std::tr1::shared_ptr<Objsc::Body> create()
			{
				std::tr1::shared_ptr<Objsc::Body> ptr(new Body());
				ptr->initActions(std::tr1::weak_ptr<Objsc::Body>(ptr));
				return ptr;
			}
			
			// body update variables
			static int			unbornAge;
			static int			unbornHits;
			static int			unbornLastHit;
			static int			becomingAge;
			static int			becomingHits;
			static int			becomingLastHit;
			static int			presentLastHit;
			static int			absentLastHitMin;
			static int			absentLastHitMax;
			
	};
	
	typedef std::tr1::shared_ptr<Objsc::Body> BodyPtr;
	typedef list <Objsc::BodyPtr> BodyPtrList;
	typedef Objsc::BodyPtrList::iterator BodyPtrIter;
	typedef set <Objsc::BodyPtr> BodyPtrSet;
	
	
	////////////////////////////////////////
	// FUNCTIONS
	////////////////////////////////////////
	Objsc::LangAtomPtr bodyToLangAtom(const Objsc::BodyPtr& body, int numbodies=1);
}

#endif // OBJSCR_BODY_H
