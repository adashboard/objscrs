#ifndef OBJSCR_LANGVIEWS_H
#define OBJSCR_LANGVIEWS_H


// objscrs
#include "objscrConstants.hpp"
#include "objscrGUI.hpp"
#include "objscrGeom.hpp"
#include "objscrLang.hpp"
#include "objscrCommands.hpp"

// STL
#include <string>
#include <vector>
#include <map>
#include <list>
using std::string;
using std::vector;
using std::map;
using std::list;

// shared_ptr
#include <tr1/memory>


namespace Objsc
{
	static const float LANGVIEW_DFTWIDTH = 40.0f;
	static const float LANGVIEW_DFTHEIGHT = 20.0f;
	static const float LANGVIEW_BEVEL = 5.0f;
	static const float LANGVIEW_SOCKETWIDTH = 10.0f;
	static const float LANGVIEW_SOCKETHEIGHT = 4.0f;
	static const float LANGVIEW_MARGIN = 3.0f;
	static const float LANGVIEW_RIGHTMARGIN = 10.0f;
	
	enum socket_type
	{
		LANG_INLET,
		LANG_OUTLET
	};
	
	////////////////////////////////////////
	// LANG VIEW BASE
	////////////////////////////////////////
	class LangViewBase : public Objsc::Widget
	{
		public:
			virtual ~LangViewBase() {}
			static const string stylename;
				
			virtual void mouseMoved(int x, int y)
			{
				if(withinBounds(x,y) && visible)	{
					if(!hasFocus && displayState!=Objsc::DISABLED) {
						gainFocus();
					}
				} else {
					if(hasFocus && displayState!=Objsc::DISABLED) {
						looseFocus();
					}
				}
			}
	};
	
	typedef std::tr1::shared_ptr<Objsc::LangViewBase> LangViewBasePtr;
	
	
	////////////////////////////////////////
	// LANG VIEW
	////////////////////////////////////////
	class LangView : public Objsc::LangViewBase
	{
		protected:
			Objsc::LangObjectPtr 	object;
			Objsc::Rect			inrect;
			Objsc::ContextMenuPtr 	editmenu;
			Objsc::Rect			canvasrect;
			
			void openMenu(int x, int y);
			virtual void _setLocation(float x, float y);
			virtual void _setBounds(float x, float y, float width, float height);
			
		public:
			LangView() : object(), inrect(), editmenu(), canvasrect() {}
			virtual ~LangView() {}
				
			void setCanvasRect(Objsc::Rect& crect) {canvasrect.set(crect.getX(),crect.getY(),crect.getWidth(),crect.getHeight());}
			virtual void setObject(Objsc::LangObjectPtr objectPtr) {object=objectPtr;}
			Objsc::LangObjectPtr getObject() const { return object; }
			bool hasObject() const { return object; }
			virtual void initSockets(std::tr1::weak_ptr<Objsc::LangView> weakPtr) {}
			virtual void placeSockets() {}
			virtual void initActions(std::tr1::weak_ptr<Objsc::LangView> weakPtr) {}
			
			virtual void link(int outlet, std::tr1::shared_ptr<Objsc::LangView> toView, int inlet);
			
			void setLocation(float x, float y);
			void setBounds(float x, float y, float width, float height);
			bool onMargin(float x, float y) { return bounds.containsPoint(x,y) && !inrect.containsPoint(x,y); }
			bool onInside(float x, float y) { return inrect.containsPoint(x,y); }
			void mapToStage();
			void mapToScreen();
			virtual void reset() {}
			
			virtual const float getOutletX(int index) const { return 0.0f; }
			virtual const float getOutletY(int index) const { return 0.0f; }
			virtual const float getInletX(int index) const { return 0.0f; }
			virtual const float getInletY(int index) const { return 0.0f; }
			
			virtual void startEdit() {}
			virtual void endEdit() {}
			
	};
	
	typedef std::tr1::shared_ptr<Objsc::LangView> LangViewPtr;
	typedef std::tr1::weak_ptr<Objsc::LangView> LangViewWeakPtr;
	typedef vector < Objsc::LangViewPtr > LangViewVector;
	
	////////////////////////////////////////
	// SOCKET VIEW
	////////////////////////////////////////
	class SocketView : public Objsc::LangViewBase
	{
		private:
			socket_type 			type;
			Objsc::CommandPtr 	action;
			void setType(socket_type ntype) { type=ntype;}
			
		public:
			SocketView(socket_type ntype);
			~SocketView() {}
				
			void setAction(Objsc::LangViewWeakPtr view, int socketnum);
			void draw();
			void mousePressed(int x, int y, int button);
				
			static std::tr1::shared_ptr<Objsc::SocketView> createInlet()
			{
				return std::tr1::shared_ptr<Objsc::SocketView>(new SocketView(Objsc::LANG_INLET));
			}
			
			static std::tr1::shared_ptr<Objsc::SocketView> createOutlet()
			{
				return std::tr1::shared_ptr<Objsc::SocketView>(new SocketView(Objsc::LANG_OUTLET));
			}
			
			void moveCentre(float dX, float dY) { bounds.moveCentre(dX,dY);}
			
			const float getOutletX() const { return bounds.getCentreX(); }
			const float getOutletY() const { return bounds.getBottom(); }
			const float getInletX() const { return bounds.getCentreX(); }
			const float getInletY() const { return bounds.getY(); }
	};
	
	typedef std::tr1::shared_ptr<Objsc::SocketView> SocketViewPtr;
	typedef vector < Objsc::SocketViewPtr > SocketViewVector;
	
	////////////////////////////////////////
	// OBJECT VIEW
	////////////////////////////////////////
	class LangObjectView : public Objsc::LangView
	{
		protected:
			Objsc::TextField		text;
			bool				dodrag;
			int					scrX;
			int					scrY;
			Objsc::CommandPtr 	actionParse;
			Objsc::SocketViewVector	inlets;
			Objsc::SocketViewVector	outlets;
			
			void fitToText();
			void updateText();
			void placeSockets();
			void _setLocation(float x, float y);
			void _setBounds(float x, float y, float width, float height);
			
		public:
			LangObjectView();
			~LangObjectView();
				
			void setObject(Objsc::LangObjectPtr objectPtr);
			void initSockets(std::tr1::weak_ptr<Objsc::LangView> weakPtr);
			void initActions(std::tr1::weak_ptr<Objsc::LangView> weakPtr);
			void setText(const string& str);
			void reset();
				
			void update();
			void draw();
				
			void keyPressed(int key);
			void keyReleased(int key);
			void mouseMoved(int x, int y );
			void mouseDragged(int x, int y, int button);
			void mousePressed(int x, int y, int button);
			void mouseReleased(int x, int y, int button);
			void windowResized(int w, int h);
		
			const float getOutletX(int index) const { return outlets[index]->getOutletX(); }
			const float getOutletY(int index) const { return outlets[index]->getOutletY(); }
			const float getInletX(int index) const { return inlets[index]->getInletX(); }
			const float getInletY(int index) const { return inlets[index]->getInletY(); }
				
			static std::tr1::shared_ptr<Objsc::LangObjectView> create()
			{
				std::tr1::shared_ptr<Objsc::LangObjectView> ptr(new LangObjectView());
				ptr->initSockets(std::tr1::weak_ptr<Objsc::LangView>(ptr));
				ptr->initActions(std::tr1::weak_ptr<Objsc::LangView>(ptr));
				return ptr;
			}
	};
	
	typedef std::tr1::shared_ptr<Objsc::LangObjectView> LangObjectViewPtr;
	typedef vector < Objsc::LangObjectViewPtr > LangObjectViewVector;
	
	////////////////////////////////////////
	// BANG VIEW
	////////////////////////////////////////
	class LangBangView : public Objsc::LangView, public Objsc::LangObserver
	{
		protected:
			Objsc::SocketView		inlet;
			Objsc::SocketView		outlet;
			bool				dodrag;
			int					scrX;
			int					scrY;
			bool				showbang;
		
			void _setLocation(float x, float y);
			void _setBounds(float x, float y, float width, float height);
				
		public:
			LangBangView();
			~LangBangView();
				
			void update();
			void draw();
				
			void initSockets(std::tr1::weak_ptr<Objsc::LangView> weakPtr);
				
			void setObject(Objsc::LangObjectPtr objectPtr);
			
			const float getOutletX(int index) const { return outlet.getOutletX(); }
			const float getOutletY(int index) const { return outlet.getOutletY(); }
			const float getInletX(int index) const { return inlet.getInletX(); }
			const float getInletY(int index) const { return inlet.getInletY(); }
			
			void objActivated() {showbang=true;}
				
			void keyPressed(int key);
			void keyReleased(int key);
			void mouseMoved(int x, int y );
			void mouseDragged(int x, int y, int button);
			void mousePressed(int x, int y, int button);
			void mouseReleased(int x, int y, int button);
			void windowResized(int w, int h);
				
			static std::tr1::shared_ptr<Objsc::LangBangView> create()
			{
				std::tr1::shared_ptr<Objsc::LangBangView> ptr(new LangBangView());
				ptr->initSockets(std::tr1::weak_ptr<Objsc::LangView>(ptr));
				return ptr;
			}
	};
	
	typedef std::tr1::shared_ptr<Objsc::LangBangView> LangBangViewPtr;
	typedef vector < Objsc::LangBangViewPtr > LangBangViewVector;
	
	
	////////////////////////////////////////
	// TOGGLE VIEW
	////////////////////////////////////////
	class LangToggleView : public Objsc::LangView, public Objsc::LangObserver
	{
		protected:
			Objsc::SocketView		inlet;
			Objsc::SocketView		outlet;
			bool				dodrag;
			int					scrX;
			int					scrY;
			bool				drawon;
		
			void _setLocation(float x, float y);
			void _setBounds(float x, float y, float width, float height);
				
		public:
			LangToggleView();
			~LangToggleView();
				
			void update();
			void draw();
				
			void initSockets(std::tr1::weak_ptr<Objsc::LangView> weakPtr);
				
			void setObject(Objsc::LangObjectPtr objectPtr);
			
			const float getOutletX(int index) const { return outlet.getOutletX(); }
			const float getOutletY(int index) const { return outlet.getOutletY(); }
			const float getInletX(int index) const { return inlet.getInletX(); }
			const float getInletY(int index) const { return inlet.getInletY(); }
			
			void objActivated();
				
			void keyPressed(int key);
			void keyReleased(int key);
			void mouseMoved(int x, int y );
			void mouseDragged(int x, int y, int button);
			void mousePressed(int x, int y, int button);
			void mouseReleased(int x, int y, int button);
			void windowResized(int w, int h);
				
			static std::tr1::shared_ptr<Objsc::LangToggleView> create()
			{
				std::tr1::shared_ptr<Objsc::LangToggleView> ptr(new LangToggleView());
				ptr->initSockets(std::tr1::weak_ptr<Objsc::LangView>(ptr));
				return ptr;
			}
	};
	
	typedef std::tr1::shared_ptr<Objsc::LangToggleView> LangToggleViewPtr;
	typedef vector < Objsc::LangToggleViewPtr > LangToggleViewVector;
	
	////////////////////////////////////////
	// MESSAGE VIEW
	////////////////////////////////////////
	class LangMessageView : public Objsc::LangView
	{
		protected:
			Objsc::TextField		text;
			Objsc::SocketView		inlet;
			Objsc::SocketView		outlet;
			bool				dodrag;
			int					scrX;
			int					scrY;
			Objsc::CommandPtr 	actionParse;
		
			void fitToText();
			void updateText();
			void _setLocation(float x, float y);
			void _setBounds(float x, float y, float width, float height);
		
		public:
			LangMessageView();
			~LangMessageView() {}
				
			void update();
			void draw();
				
			const float getOutletX(int index) const { return outlet.getOutletX(); }
			const float getOutletY(int index) const { return outlet.getOutletY(); }
			const float getInletX(int index) const { return inlet.getInletX(); }
			const float getInletY(int index) const { return inlet.getInletY(); }
			
			void initSockets(std::tr1::weak_ptr<Objsc::LangView> weakPtr);
			void initActions(std::tr1::weak_ptr<Objsc::LangView> weakPtr);
				
			void keyPressed(int key);
			void keyReleased(int key);
			void mouseMoved(int x, int y );
			void mouseDragged(int x, int y, int button);
			void mousePressed(int x, int y, int button);
			void mouseReleased(int x, int y, int button);
				
			void setObject(Objsc::LangObjectPtr objectPtr);
			
			void startEdit();
			void endEdit();
				
			static std::tr1::shared_ptr<Objsc::LangMessageView> create()
			{
				std::tr1::shared_ptr<Objsc::LangMessageView> ptr(new LangMessageView());
				ptr->initSockets(std::tr1::weak_ptr<Objsc::LangView>(ptr));
				ptr->initActions(std::tr1::weak_ptr<Objsc::LangView>(ptr));
				return ptr;
			}
	};
	
	typedef std::tr1::shared_ptr<Objsc::LangMessageView> LangMessageViewPtr;
	
	
	////////////////////////////////////////
	// NUMBER VIEW
	////////////////////////////////////////
	class LangNumberView : public Objsc::LangView, public Objsc::LangObserver
	{
		protected:
			Objsc::TextField		text;
			Objsc::SocketView		inlet;
			Objsc::SocketView		outlet;
			bool				dodrag;
			int					scrX;
			int					scrY;
			Objsc::CommandPtr 	actionParse;
		
			void fitToText();
			void updateText();
			void _setLocation(float x, float y);
			void _setBounds(float x, float y, float width, float height);
		
		public:
			LangNumberView();
			~LangNumberView();
				
			void update();
			void draw();
				
			const float getOutletX(int index) const { return outlet.getOutletX(); }
			const float getOutletY(int index) const { return outlet.getOutletY(); }
			const float getInletX(int index) const { return inlet.getInletX(); }
			const float getInletY(int index) const { return inlet.getInletY(); }
			
			void objActivated();
			
			void initSockets(std::tr1::weak_ptr<Objsc::LangView> weakPtr);
			void initActions(std::tr1::weak_ptr<Objsc::LangView> weakPtr);
				
			void keyPressed(int key);
			void keyReleased(int key);
			void mouseMoved(int x, int y );
			void mouseDragged(int x, int y, int button);
			void mousePressed(int x, int y, int button);
			void mouseReleased(int x, int y, int button);
				
			void setObject(Objsc::LangObjectPtr objectPtr);
				
			static std::tr1::shared_ptr<Objsc::LangNumberView> create()
			{
				std::tr1::shared_ptr<Objsc::LangNumberView> ptr(new LangNumberView());
				ptr->initSockets(std::tr1::weak_ptr<Objsc::LangView>(ptr));
				ptr->initActions(std::tr1::weak_ptr<Objsc::LangView>(ptr));
				return ptr;
			}
	};
	
	typedef std::tr1::shared_ptr<Objsc::LangNumberView> LangNumberViewPtr;
	
	////////////////////////////////////////
	// CORD VIEW
	////////////////////////////////////////
	class LangCordView : public Objsc::LangViewBase
	{
		protected:
			Objsc::LangViewPtr		outletView;
			Objsc::LangViewPtr		inletView;
			int					outlet;
			int					inlet;
			float				endX;
			float				endY;
			bool				editing;
			Objsc::CommandPtr 	actionDelete;
			
		
		public:
			LangCordView();
			~LangCordView() {}
				
			void update();
			void draw();
				
			void start(Objsc::LangViewPtr viewPtr, int socket);
			void connect(Objsc::LangViewPtr viewPtr, int socket, bool linkobject=true);
			bool connectsWith(Objsc::LangViewPtr viewPtr) const { return viewPtr == outletView || viewPtr == inletView; }
			void print() { printf("LangCordView\n");}
			
			void unlink();
			
			void keyPressed(int key);
			void keyReleased(int key);
			void mouseMoved(int x, int y );
			void mouseDragged(int x, int y, int button);
			void mousePressed(int x, int y, int button);
			void mouseReleased(int x, int y, int button);
			void windowResized(int w, int h);
			
			bool withinBounds(float x, float y);
			
			void initActions(std::tr1::weak_ptr<Objsc::LangCordView> weakPtr);
				
			static std::tr1::shared_ptr<Objsc::LangCordView> create()
			{
				std::tr1::shared_ptr<Objsc::LangCordView> ptr(new LangCordView());
				ptr->initActions(std::tr1::weak_ptr<Objsc::LangCordView>(ptr));
				return ptr;
			}
			
			static const string stylename;
	};
	
	typedef std::tr1::shared_ptr<Objsc::LangCordView> LangCordViewPtr;
	typedef vector < Objsc::LangCordViewPtr > LangCordViewVector;
	typedef std::tr1::weak_ptr<Objsc::LangCordView> LangCordViewWeakPtr;
	
	
	////////////////////////////////////////
	// PATCH CANVAS
	////////////////////////////////////////
	class LangPatchCanvas : public Objsc::LangViewBase
	{
		protected:
			Objsc::LangPatchPtr 		patch;
			Objsc::LangViewVector 		objviews;
			Objsc::LangCordViewPtr		cord;
			Objsc::LangCordViewVector 	cordviews;
			//Objsc::TextField			namefield;
			
		public:
			LangPatchCanvas();
			~LangPatchCanvas() {}
			
			void update();
			void draw();
			
			// geom
			virtual void setLocation(float x, float y);
			virtual void setDimensions(float width, float height);
			virtual void setBounds(float x, float y, float width, float height);
				
			// events
			void keyPressed(int key);
			void keyReleased(int key);
			void mouseMoved(int x, int y );
			void mouseDragged(int x, int y, int button);
			void mousePressed(int x, int y, int button);
			void mouseReleased(int x, int y, int button);
			void windowResized(int w, int h);
				
			void setPatch(Objsc::LangPatchPtr patchPtr);
				
			static std::tr1::shared_ptr<Objsc::LangPatchCanvas> create()
			{
				return std::tr1::shared_ptr<Objsc::LangPatchCanvas>(new LangPatchCanvas());
			}
			
			static const string stylename; 
			
			// lang
			void addObjectView(Objsc::LangViewPtr view);
			void linkAllViews();
			void linkFromView(Objsc::LangViewPtr viewFrom, bool linkobject);
			void linkViews(Objsc::LangViewPtr viewFrom, int outlet, Objsc::LangViewPtr viewTo, int inlet, bool linkobject);
			Objsc::LangViewPtr& viewForObject(Objsc::LangObjectPtr objPtr);
			void createObject(float x, float y);
			void createMessage(float x, float y);
			void createNumber(float x, float y);
			void createBang(float x, float y);
			void createToggle(float x, float y);
			void startCord(Objsc::LangViewPtr view, int socket);
			void endCord(Objsc::LangViewPtr view, int socket);
			void dropCord();
			
			void clear();
			void removeObject(Objsc::LangViewPtr objview);
			void removeCord(Objsc::LangCordViewPtr cordview);
			
			// parsing
			void parseStringToObject(Objsc::LangViewPtr objview, const string& str);
			
	};
	
	typedef std::tr1::shared_ptr<Objsc::LangPatchCanvas> LangPatchCanvasPtr;
	typedef vector < Objsc::LangPatchCanvasPtr > LangPatchCanvasVector;
	
}

#endif  //OBJSCR_LANGVIEWS_H

