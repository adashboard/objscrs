#ifndef OBJSCR_MATH
#define OBJSCR_MATH


#include <math.h>


namespace Objsc
{

	////////////////////////////////////////
	// FUNCTIONS
	////////////////////////////////////////
	inline bool hasChanged(float old, float value, float diff)	{ return fabs(value-old) > diff;};
	inline bool valueMatches(float a, float b, float diff)	{ return fabs(a-b) <= fabs(diff);};
}

#endif