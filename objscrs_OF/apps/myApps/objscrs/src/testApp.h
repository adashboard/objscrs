#ifndef _TEST_APP
#define _TEST_APP


#include "ofMain.h"

#include "objscrApp.hpp"
#include "objscrGUIRoot.hpp"
//#include "objscrSoundMixer.hpp"


class testApp : public ofBaseApp{
	
	private:
		//time
		int timenow;
		int timepassed;
		int timeframe;
		// app
		Objsc::App*			app;
		Objsc::GUIRoot*		guiroot;
		// audio
		//Objsc::SoundMixer* 	audioout;
		bool				sndon;
	
		void startTime();
	
	public:
		
		void setup();
		void update();
		void draw();
		
		void keyPressed(int key);
		void keyReleased(int key);
		void mouseMoved(int x, int y );
		void mouseDragged(int x, int y, int button);
		void mousePressed(int x, int y, int button);
		void mouseReleased(int x, int y, int button);
		void windowResized(int w, int h);
	
		// audio out
		//void audioRequested(float * input, int bufferSize, int nChannels);
};

#endif	
