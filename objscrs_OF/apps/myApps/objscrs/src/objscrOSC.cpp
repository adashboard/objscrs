#include "objscrOSC.hpp"


// objscrs
#include "objscrConstants.hpp"
#include "objscrMediaViews.hpp"
#include "objscrMediaPlayer.hpp"

// test
#include "objscrApp.hpp"


namespace Objsc
{
	
	////////////////////////////////////////
	// FUNCTIONS
	////////////////////////////////////////
	void initOSCComms()
	{
		Objsc::OSCComms::Current()->startListen(OSC_MAINLISTENER, OSC_MAINPORT);
		Objsc::OSCComms::Current()->setListenHandler(OSC_MAINLISTENER, "/test", Objsc::OSCTestHandler::create());
		Objsc::OSCComms::Current()->setListenHandler(OSC_MAINLISTENER, "/join", Objsc::OSCJoinHandler::create());
		Objsc::OSCComms::Current()->setListenHandler(OSC_MAINLISTENER, "/leave", Objsc::OSCLeaveHandler::create());
		Objsc::OSCComms::Current()->setListenHandler(OSC_MAINLISTENER, "/img", Objsc::OSCImageHandler::create());
		Objsc::OSCComms::Current()->setListenHandler(OSC_MAINLISTENER, "/msg", Objsc::OSCMessageHandler::create());
	}
	
	
	Objsc::LangList* OSCMessageToList(ofxOscMessage& msg)
	{
		Objsc::LangList* list = new Objsc::LangList();
		//list->add(msg.getAddress());
		for ( int i=0; i<msg.getNumArgs(); i++ )
		{
			if (msg.getArgType(i) == OFXOSC_TYPE_INT32)
				list->add(msg.getArgAsInt32(i));
			else if(msg.getArgType(i) == OFXOSC_TYPE_FLOAT)
				list->add(msg.getArgAsFloat(i));
			else if(msg.getArgType(i) == OFXOSC_TYPE_STRING )
				list->add(msg.getArgAsString(i));
		}
		return list;
	}
	
	
	////////////////////////////////////////
	// OSC GROUP
	////////////////////////////////////////
	void OSCGroup::join(string label, string host, int port)
	{
		addresses.insert(OSCAddressEntry(label, new ofxOscSender()));
		addresses[label]->setup(host, port);
		//printf("OSCGroup::join\n");
	}
	
	void OSCGroup::leave(string label)
	{
		if(addresses.count(label) > 0)	{
			addresses.erase(label);
		}
	}
	
	void OSCGroup::send(ofxOscMessage& msg)
	{
		//printf("OSCGroup::send A\n");
		for(OSCAddressMap::iterator addr = addresses.begin(); addr != addresses.end(); ++addr)	{
			//printf("OSCGroup::send B\n");
			(*addr).second->sendMessage(msg);
		}
		//printf("OSCGroup::send DONE\n");
	}
	
	////////////////////////////////////////
	// OSC LISTENER
	////////////////////////////////////////
	void OSCListener::update()	
	{
		while(receiver.hasWaitingMessages())	{
			//printf("OSCListener::update RECEIVED\n");
			ofxOscMessage msg;
			receiver.getNextMessage(&msg);
			handle(msg);
		}
	}
	
	void OSCListener::setHandler(string pktaddress, OSCHandlerPtr handler)	
	{
		handlers.insert(OSCHandlerEntry(pktaddress, handler));
	}
	
	void OSCListener::removeHandler(string pktaddress)	
	{
		handlers.erase(pktaddress);
	}
	
	void OSCListener::handle(ofxOscMessage& msg)
	{
		if(handlers.count(msg.getAddress()) > 0)	{
			handlers[msg.getAddress()]->handle(msg);
		}
	}
	
	////////////////////////////////////////
	// OSC COMMS
	////////////////////////////////////////
	// outgoing
	Objsc::OSCGroupPtr OSCComms::createGroup(const string& groupname)
	{
		if(groups.count(groupname) == 0)	{
			groups.insert(OSCGroupEntry(groupname, Objsc::OSCGroup::create(groupname)));
		}
		return groups[groupname];
	}
	
	void OSCComms::joinGroup(string groupname, string label, string host, int port)
	{
		if(groups.count(groupname) == 0)	{
			createGroup(groupname);
		}
		groups[groupname]->join(label, host, port);
	}
	
	void OSCComms::leaveGroup(string groupname, string label)
	{
		if(groups.count(groupname) > 0)	{
			groups[groupname]->leave(label);
		}
	}
	
	void OSCComms::send(string groupname, ofxOscMessage& msg)
	{
		if(groups.count(groupname) > 0)	{
			groups[groupname]->send(msg);
		}
	}
	
	void OSCComms::send(ofxOscMessage& msg)
	{
		for(OSCGroupMap::iterator group = groups.begin(); group != groups.end(); ++group)	{
			(*group).second->send(msg);
		}
	}
	
	// incoming
	void OSCComms::startListen(string listenername, int port)
	{
		if(listeners.count(listenername) > 0)	{
			listeners.erase(listenername);
		}
		listeners.insert(OSCListenerEntry(listenername, new OSCListener(listenername, port)));
	}
	
	void OSCComms::stopListen(string listenername)
	{
		if(listeners.count(listenername) > 0)	{
			listeners.erase(listenername);
		}
	}
	
	void OSCComms::setListenHandler(string listenername, string pktaddress, OSCHandlerPtr handler)
	{
		if(listeners.count(listenername) > 0)	{
			listeners[listenername]->setHandler(pktaddress, handler);
		}
	}
		
	void OSCComms::update()	
	{
		for(OSCListenerMap::iterator listener = listeners.begin(); listener != listeners.end(); ++listener)	{
			(*listener).second->update();
		}
	}
	
	
	////////////////////////////////////////
	// JOIN HANDLER
	////////////////////////////////////////
	void OSCJoinHandler::handle(ofxOscMessage& msg) 
	{
		//printf("OSCJoinHandler::handle\n");
		string group = msg.getArgAsString(0);
		string label = msg.getArgAsString(1);
		string host = msg.getArgAsString(2);
		int port = msg.getArgAsInt32(3);
		//printf("group: <%s> label: <%s> host: <%s> port [%d]\n", group.c_str(), label.c_str(), host.c_str(), port);
		Objsc::OSCComms::Current()->joinGroup(group, label, host, port);
		sendConfirm(group, label);
	}
	
	void OSCJoinHandler::sendConfirm(const string& groupname, const string& label)
	{
		ofxOscMessage m;
		m.setAddress( "/joined" );
		m.addStringArg(groupname);
		m.addStringArg(label);
		Objsc::OSCComms::Current()->send(groupname, m);
	}
	
	////////////////////////////////////////
	// IMAGE HANDLER
	////////////////////////////////////////
	void OSCImageHandler::handle(ofxOscMessage& msg) 
	{
		//printf("OSCImageHandler::handle\n");
		string label = msg.getArgAsString(0);
		string filename = msg.getArgAsString(1);
		int layer = msg.getArgAsInt32(2);
		//~ Objsc::MediaViewPtr media = Objsc::MediaPlayer::Current()->loadImage(filename, true, label, layer);
		//~ media->centre();
		//~ media->show();
	}
	
	////////////////////////////////////////
	// MESSAGE HANDLER
	////////////////////////////////////////
	void OSCMessageHandler::handle(ofxOscMessage& msg) 
	{
		//printf("OSCMessageHandler::handle\n");
		Objsc::App::Current()->getScore()->sendToCurrentZoneMap("_osc_in_", Objsc::LangAtomPtr(OSCMessageToList(msg)));
	}

	
	////////////////////////////////////////
	// LEAVE HANDLER
	////////////////////////////////////////
	void OSCLeaveHandler::handle(ofxOscMessage& msg) 
	{
		//printf("OSCJoinHandler::handle\n");
		string group = msg.getArgAsString(0);
		string label = msg.getArgAsString(1);
		//printf("group: <%s> label: <%s> host: <%s> port [%d]\n", group.c_str(), label.c_str(), host.c_str(), port);
		sendConfirm(group, label);
		Objsc::OSCComms::Current()->leaveGroup(group, label);
	}
	
	void OSCLeaveHandler::sendConfirm(const string& groupname, const string& label)
	{
		ofxOscMessage m;
		m.setAddress( "/leaving" );
		m.addStringArg(groupname);
		m.addStringArg(label);
		Objsc::OSCComms::Current()->send(groupname, m);
	}
	
}

