#include "objscrFrameSourceEditor.hpp"

// objscrs
#include "objscrGUIRoot.hpp"
#include "objscrLiveVideoSource.hpp"
//#include "objscrDVVideoSource.hpp"

// openframeworks
#include "ofMain.h"

namespace Objsc
{
	
	FrameSourceEditor::FrameSourceEditor() : 
			objmenu(), 
			patchmenu(),
			patchdialogue(),
			stylename("default"), 
			cmdlabel("_frames_"), 
			scrX(0), 
			scrY()
	{
		frameView = Objsc::FrameView::create();
		autosize();
		// commands
		Objsc::CommandDispatcher::Current()->registerCommandReceiver(cmdlabel, this);
		//initMenus();
		patchdialogue = Objsc::Dialogue::create("_frmsrcdia_");
	}
	
	void FrameSourceEditor::initMenus()
	{
		// object menu
		//~ objmenu = Objsc::ContextMenu::create();
		//~ objmenu->addItem("object", Objsc::CommandDispatcher::createCommand(cmdlabel, "new_object"));
		//~ objmenu->addItem("message", Objsc::CommandDispatcher::createCommand(cmdlabel, "new_message"));
		//~ objmenu->addItem("bang", Objsc::CommandDispatcher::createCommand(cmdlabel, "new_bang"));
		// patch menu
		//~ patchmenu = Objsc::ContextMenu::create();
		//~ patchmenu->addItem("new patch", Objsc::CommandDispatcher::createCommand(cmdlabel, "new_patch"));
		//~ patchmenu->addItem("open patch", Objsc::CommandDispatcher::createCommand(cmdlabel, "open_patch"));
		//~ patchmenu->addItem("save patch", Objsc::CommandDispatcher::createCommand(cmdlabel, "save_patch"));
		//~ patchmenu->addItem("save patch as", Objsc::CommandDispatcher::createCommand(cmdlabel, "rename_patch"));
		//~ patchmenu->addItem("close", Objsc::CommandDispatcher::createCommand("_gui_", "close_patch"));
	}
	
	void FrameSourceEditor::update()
	{
		if(active && frameSource)	{
			frameSource->update();
			frameView->update();
		}
	}
	
	void FrameSourceEditor::draw()
	{
		if(active && frameSource)	{
			frameView->draw();
		}
	}
	
	
	// frames
	void FrameSourceEditor::initLiveInput()
	{
		frameSource = Objsc::LiveVideoSource::create();
		//frameSource->setFrameSize(PAL_WIDTH,PAL_HEIGHT); //320,240);
		frameSource->setFrameSize(VIDSOURCE_WIDTH,VIDSOURCE_HEIGHT);
		frameView->setSource(frameSource);
		Objsc::Stage::Current()->setFrameSource(frameSource);
	}
	
	void FrameSourceEditor::startLiveInput()
	{
		if(!frameSource) {
			initLiveInput();
		}
		frameSource->start();
	}
	
	void FrameSourceEditor::stopLiveInput()
	{
		if(frameSource) {
			frameSource->stop();
		}
	}
	
	// commands
	void FrameSourceEditor::doCommand(Objsc::CommandPtr& cmd)
	{
		//std::cout << "FrameSourceEditor::doCommand: " << cmd->getName() << std::endl;
		string cmdname = cmd->getName();
		//~ if(cmdname ==  "new_object")	{
			//~ frameView->createObject(scrX,scrY);
			//~ Objsc::GUIRoot::Current()->closeRootMenu();
		//~ } else if(cmdname ==  "new_message")	{
			//~ frameView->createMessage(scrX,scrY);
			//~ Objsc::GUIRoot::Current()->closeRootMenu();
		//~ } else if(cmdname ==  "new_bang")	{
			//~ frameView->createBang(scrX,scrY);
			//~ Objsc::GUIRoot::Current()->closeRootMenu();
		//~ } else if(cmdname ==  "start_cord")	{
			//~ int socket = -1;
			//~ Objsc::LangViewPtr view;
			//~ float cX = -1.0f;
			//~ float cY = -1.0f;
			//~ Objsc::PacketPtr data = cmd->getData();
			//~ data->getArg(0,&socket);
			//~ data->getArg(1,&view);
			//~ data->getArg(2,&cX);
			//~ data->getArg(3,&cY);
			//~ frameView->startCord(cX,cY,view,socket);
		//~ } else if(cmdname ==  "end_cord")	{
			//~ int socket = -1;
			//~ Objsc::LangViewPtr view;
			//~ float cX = -1.0f;
			//~ float cY = -1.0f;
			//~ Objsc::PacketPtr data = cmd->getData();
			//~ data->getArg(0,&socket);
			//~ data->getArg(1,&view);
			//~ data->getArg(2,&cX);
			//~ data->getArg(3,&cY);
			//~ frameView->endCord(cX,cY,view,socket);
		//~ } else if(cmdname ==  "new_patch")	{
			//~ patchdialogue->setText("new_patch");
			//~ Objsc::GUIRoot::Current()->openDialogue(patchdialogue);
		//~ } else if(cmdname ==  "open_patch")	{
			//~ patchdialogue->setText("open_patch");
			//~ Objsc::GUIRoot::Current()->openDialogue(patchdialogue);
		//~ } else if(cmdname ==  "save_patch")	{
			//~ patchdialogue->setText("save_patch");
			//~ Objsc::GUIRoot::Current()->openDialogue(patchdialogue);
		//~ } else if(cmdname ==  "rename_patch")	{
			//~ patchdialogue->setText("rename_patch");
			//~ Objsc::GUIRoot::Current()->openDialogue(patchdialogue);
		//~ }
	}
	
	void FrameSourceEditor::undoCommand(Objsc::CommandPtr& cmd)
	{
		std::cout << "FrameSourceEditor::undoCommand: " << cmd->getName() << std::endl;
	}
	
	
	void FrameSourceEditor::keyPressed(int key)
	{
		frameView->keyPressed(key);
	}
	
	void FrameSourceEditor::keyReleased(int key)
	{
		frameView->keyReleased(key);
	}
	
	void FrameSourceEditor::mouseMoved(int x, int y )
	{
		frameView->mouseMoved(x,y);
	}
	
	void FrameSourceEditor::mouseDragged(int x, int y, int button)
	{
		frameView->mouseDragged(x,y,button);
	}
	
	void FrameSourceEditor::mousePressed(int x, int y, int button)
	{
		consumed = false;
		frameView->mousePressed(x,y,button);
		if(frameView->eventConsumed())	{
			consumed = true;
			return;
		}
		//~ switch(button)	{
			//~ case 0:
				//~ consumed = true;
				//~ scrX = x;
				//~ scrY = y;
				//~ Objsc::GUIRoot::Current()->openRootMenu(objmenu, x, y);
				//~ break;
			//~ case 1:
				//~ //openRootMenu("main", x, y);
				//~ break;
			//~ case 2:
				//~ consumed = true;
				//~ scrX = x;
				//~ scrY = y;
				//~ Objsc::GUIRoot::Current()->openRootMenu(patchmenu, x, y);
				//~ break;
		//~ }
	}
	
	void FrameSourceEditor::mouseReleased(int x, int y, int button)
	{
		frameView->mouseReleased(x,y,button);
	}
	
	void FrameSourceEditor::windowResized(int w, int h)
	{
		frameView->windowResized(w,h);
	}
	
	void FrameSourceEditor::autosize()
	{
		setBounds(0,0, ofGetWidth(),ofGetHeight());
		frameView->setBounds(bounds.getX(), bounds.getY(), bounds.getWidth(), bounds.getHeight());
	}
}

