#ifndef  OBJSCR_FRAMEVIEW_H
#define OBJSCR_FRAMEVIEW_H

// objscrs
#include "objscrFrames.hpp"
#include "objscrGUI.hpp"

// openframeworks
#include "ofMain.h"

// shared_ptr
#include <tr1/memory>



namespace Objsc
{
	class FrameView : public GuiBase
	{
		private:
			Objsc::FrameSourcePtr		source;
			ofTexture					texture;
			
		public:
			FrameView() : 
				source(),
				texture()
			{}
			~FrameView() {}
				
			void setSource(Objsc::FrameSourcePtr sourcePtr) 
			{
				source = sourcePtr;
				texture.allocate(source->getWidth(),source->getHeight(),GL_RGB);
				//texture.allocate(source->getWidth(),source->getHeight(),GL_LUMINANCE);
			}
			void update()
			{
				if(source->hasNewFrame())	{
					texture.loadData(source->getFrame(),source->getWidth(),source->getHeight(),GL_RGB);
					//texture.loadData(source->getFrame(),source->getWidth(),source->getHeight(),GL_LUMINANCE);
				}
			}
			void draw()
			{
				ofSetColor(0xffffff);
				texture.draw(bounds.getX(),bounds.getY(),bounds.getWidth(),bounds.getHeight());
			}
			
			static std::tr1::shared_ptr<Objsc::FrameView> create()
			{
				return std::tr1::shared_ptr<Objsc::FrameView>(new FrameView());
			}
	};
	
	typedef std::tr1::shared_ptr<Objsc::FrameView> FrameViewPtr;
}

#endif // OBJSCR_FRAMEVIEW_H