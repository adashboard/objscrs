#include "objscrSettings.hpp"

// objscrs
#include "objscrFiles.hpp"
#include "objscrBody.hpp"



namespace Objsc
{
	
	////////////////////////////////////////
	// SETTINGS
	////////////////////////////////////////
	Settings::Settings() : 
		loaded(false),
		cameraID(1),
		cameraOn(false),
		scrWidth(640),
		scrHeight(480),
		trackingMinArea(30),
		trackingMaxArea(2000),
		trackingThreshold(80)
	{
		
	}
		
		
	void Settings::print()
	{
		printf("-------------------------------------------\n");
		printf("SETTINGS\n");
		printf("-------------------------------------------\n");
		if(loaded)	{
			printf("loaded from file\n");
		} else {
			printf("file not loaded, using default values\n");
		}
		if(cameraOn)	{
			printf("camera on\n");
			printf("camera: %d\n", cameraID);
		} else {
			printf("camera off\n");
		}
		printf("display width: %d\n", scrWidth);
		printf("display height: %d\n", scrHeight);
		printf("-------------------------------------------\n");
		printf("tracking preferences:\n");
		printf("minimum area: %d\n", trackingMinArea);
		printf("maximum area: %d\n", trackingMaxArea);
		printf("threshold: %d\n", trackingThreshold);
		printf("-------------------------------------------\n");
		printf("body preferences:\n");
		printf("unborn age: %d\n", Objsc::Body::unbornAge);
		printf("unborn hits: %d\n", Objsc::Body::unbornHits);
		printf("unborn last hit: %d\n", Objsc::Body::unbornLastHit);
		printf("becoming age: %d\n", Objsc::Body::becomingAge);
		printf("becoming hits: %d\n", Objsc::Body::becomingHits);
		printf("becoming last hit: %d\n", Objsc::Body::becomingLastHit);
		printf("present last hit: %d\n", Objsc::Body::presentLastHit);
		printf("absent last hit min: %d\n", Objsc::Body::absentLastHitMin);
		printf("absent last hit max: %d\n", Objsc::Body::absentLastHitMax);
		printf("-------------------------------------------\n\n");
	}
		
	bool Settings::load()
	{
		loaded = false;
		TiXmlDocument doc = TiXmlDocument();
		if (doc.LoadFile(Objsc::ProjectFiles::Current()->getSettingsPath())) {
			//printf("decoding ...\n");
			TiXmlHandle hDoc(&doc);
			TiXmlElement* pElem;
			TiXmlHandle root(0);
			
			pElem=hDoc.FirstChildElement().Element();
			if (!pElem) {
				loaded = false;
				printf("ERROR: settings file invalid.\n");
				return loaded;
			}
			root=TiXmlHandle(pElem);
			pElem=root.FirstChild("settings").Element();
			if(pElem)	{
				decodeCamera(pElem);
				decodeDimensions(pElem);
				decodeBodyPrefs(pElem);
				decodeTracking(pElem);
				loaded = true;
			} else {
				loaded = false;
				printf("ERROR: settings file invalid.\n");
				return loaded;
			}
		} else {
			printf("ERROR: unable to read settings file.\n");
		}
		return loaded;
	}
	
	
	void Settings::decodeCamera(TiXmlElement *parentNode)
	{
		TiXmlElement *cameraEle = parentNode->FirstChildElement("camera");
		if(cameraEle) {
			if (string(cameraEle->GetText()) == "on")	{
				cameraOn  = true;
			} else {
				cameraOn = false;
			}
		} else {
			cameraOn = false;
		}
		
		TiXmlElement *camIDEle = parentNode->FirstChildElement("camera_id");
		if(camIDEle) {
			cameraID  = atoi(camIDEle->GetText());
		} else {
			cameraID = 1;
		}
	}
	
	void Settings::decodeDimensions(TiXmlElement *parentNode)
	{
		TiXmlElement *widthEle = parentNode->FirstChildElement("width");
		if(widthEle) {
			scrWidth  = atoi(widthEle->GetText());
		} else {
			scrWidth = 0;
		}
		
		TiXmlElement *heightEle = parentNode->FirstChildElement("height");
		if(heightEle) {
			scrHeight  = atoi(heightEle->GetText());
		} else {
			scrHeight = 0;
		}
	}
	
	void Settings::decodeBodyPrefs(TiXmlElement *parentNode)
	{
		TiXmlElement *bodyprefEle = parentNode->FirstChildElement("bodyprefs");
		if(bodyprefEle) {
			TiXmlElement *unbornAgeEle = bodyprefEle->FirstChildElement("unbornage");
			if(unbornAgeEle) {
				Objsc::Body::unbornAge = atoi(unbornAgeEle->GetText());
			}
			TiXmlElement *unbornHitsEle = bodyprefEle->FirstChildElement("unbornhits");
			if(unbornHitsEle) {
				Objsc::Body::unbornHits = atoi(unbornHitsEle->GetText());
			}
			TiXmlElement *unbornLastHitEle = bodyprefEle->FirstChildElement("unbornlasthit");
			if(unbornLastHitEle) {
				Objsc::Body::unbornLastHit = atoi(unbornLastHitEle->GetText());
			}
			TiXmlElement *becomingAgeEle = bodyprefEle->FirstChildElement("becomingage");
			if(becomingAgeEle) {
				Objsc::Body::becomingAge = atoi(becomingAgeEle->GetText());
			}
			TiXmlElement *becomingHitsEle = bodyprefEle->FirstChildElement("becominghits");
			if(becomingHitsEle) {
				Objsc::Body::becomingHits = atoi(becomingHitsEle->GetText());
			}
			TiXmlElement *becomingLastHitEle = bodyprefEle->FirstChildElement("becominglasthit");
			if(becomingLastHitEle) {
				Objsc::Body::becomingLastHit = atoi(becomingLastHitEle->GetText());
			}
			TiXmlElement *presentLastHitEle = bodyprefEle->FirstChildElement("presentlasthit");
			if(presentLastHitEle) {
				Objsc::Body::presentLastHit = atoi(presentLastHitEle->GetText());
			}
			TiXmlElement *absentLastHitMinEle = bodyprefEle->FirstChildElement("absentlasthitmin");
			if(absentLastHitMinEle) {
				Objsc::Body::unbornAge = atoi(unbornAgeEle->GetText());
			}
			TiXmlElement *absentLastHitMaxEle = bodyprefEle->FirstChildElement("absentlasthitmax");
			if(absentLastHitMaxEle) {
				Objsc::Body::absentLastHitMax = atoi(absentLastHitMaxEle->GetText());
			}
		}
	}
	
	void Settings::decodeTracking(TiXmlElement *parentNode)
	{
		TiXmlElement *trackingEle = parentNode->FirstChildElement("tracking");
		if(trackingEle) {
			TiXmlElement *minareaEle = trackingEle->FirstChildElement("minarea");
			if(minareaEle) {
				trackingMinArea = atoi(minareaEle->GetText());
			}
			TiXmlElement *maxareaEle = trackingEle->FirstChildElement("maxarea");
			if(maxareaEle) {
				trackingMaxArea = atoi(maxareaEle->GetText());
			}
			TiXmlElement *thresholdEle = trackingEle->FirstChildElement("threshold");
			if(thresholdEle) {
				trackingThreshold = atoi(thresholdEle->GetText());
			}
		}
	}
}