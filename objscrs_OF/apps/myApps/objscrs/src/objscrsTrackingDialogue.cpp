#include "objscrTrackingDialogue.hpp"

// objscrs
#include "objscrGUIStyles.hpp"

// openframeworks
#include "ofMain.h"

namespace Objsc
{
	
	TrackingDialogue::TrackingDialogue(const string& label) : listMenu(), textField(), actionBtn(), cancelBtn(), cmdlabel(label), edititems(false)
	{
		Objsc::CommandDispatcher::Current()->registerCommandReceiver(cmdlabel, this);
		Objsc::StylePalette::Current()->applyStyle("default", this);
		Objsc::StylePalette::Current()->applyStyle("text", &unbornAgeField);
		Objsc::StylePalette::Current()->applyStyle("text", &unbornHitsField);
		Objsc::StylePalette::Current()->applyStyle("text", &unbornLastHitField);
		Objsc::StylePalette::Current()->applyStyle("text", &becomingAgeField);
		Objsc::StylePalette::Current()->applyStyle("text", &becomingHitsField);
		Objsc::StylePalette::Current()->applyStyle("text", &becomingLastHitField);
		Objsc::StylePalette::Current()->applyStyle("text", &presentLastHitField);
		Objsc::StylePalette::Current()->applyStyle("text", &absentLastHitMaxField);
		Objsc::StylePalette::Current()->applyStyle("text", &absentLastHitMinField);
		Objsc::StylePalette::Current()->applyStyle("default", &actionBtn);
		Objsc::StylePalette::Current()->applyStyle("default", &cancelBtn);
		unbornAgeField.clearText();
		unbornHitsField.clearText();
		unbornLastHitField.clearText();
		becomingAgeField.clearText();
		becomingHitsField.clearText();
		becomingLastHitField.clearText();
		presentLastHitField.clearText();
		absentLastHitMaxField.clearText();
		absentLastHitMinField.clearText();
		actionBtn.setText("ok");
		actionBtn.setAction(Objsc::CommandDispatcher::createCommand(cmdlabel, "set_trackingvalues"));
		cancelBtn.setText("cancel");
		cancelBtn.setAction(Objsc::CommandDispatcher::createCommand(cmdlabel, "close_dialogue"));
		setDimensions(400,300);
	}
	
	void TrackingDialogue::sizeEditItems(bool value) 
	{ 
		edititems=value; 
		setDimensions(bounds.getWidth(),bounds.getHeight());
	}
	
	void TrackingDialogue::setActionLabel(const string& label)
	{
		actionBtn.setText(label);
	}
	
	void TrackingDialogue::setActionType(const string& actiontype)
	{
		actionBtn.setAction(Objsc::CommandDispatcher::createCommand(cmdlabel, actiontype));
	}
	
	// text
	void TrackingDialogue::setText(const string& text)
	{
		//textField.setText(text);
	}
	
	// geom
	void TrackingDialogue::setLocation(float x, float y)
	{
		bounds.setLocation(x,y);
		unbornAgeField.setLocation(bounds.getX(),bounds.getY());
		
		actionBtn.setLocation(bounds.getX(),bounds.getY());
		cancelBtn.setLocation(actionBtn.getRight(),bounds.getY());
	}
	
	void TrackingDialogue::setCentre(float x, float y)
	{
		bounds.setCentre(x,y);
		textField.setLocation(bounds.getX(),bounds.getY());
		
		actionBtn.setLocation(textField.getRight(),bounds.getY());
		cancelBtn.setLocation(actionBtn.getRight(),bounds.getY());
	}
	
	void TrackingDialogue::placeCentre()
	{
		setCentre(ofGetWidth()/2,ofGetHeight()/2);
	}
	
	void TrackingDialogue::setDimensions(float width, float height)
	{
		bounds.setDimensions(width,height);
		unbornAgeField.setWidth((width/6));
		actionBtn.setWidth(width/6);
		cancelBtn.setWidth(width/6);
		actionBtn.setLocation(textField.getRight(),bounds.getY());
		cancelBtn.setLocation(actionBtn.getRight(),bounds.getY());
	}
	
	bool TrackingDialogue::withinBounds(float x, float y) 
	{ 
		return bounds.containsPoint(x,y); 
	}
	
	// runtime
	void TrackingDialogue::draw()
	{
		textField.draw();
		if(edititems)	{
			addBtn.draw();
			removeBtn.draw();
		}
		actionBtn.draw();
		cancelBtn.draw();
		listMenu.draw();
	}
	
	// dialogues
	void TrackingDialogue::open()
	{
		actionBtn.enable();
		cancelBtn.enable();
		textField.enable();
		if(edititems)	{
			addBtn.show();
			removeBtn.show();
		}
		actionBtn.show();
		cancelBtn.show();
		textField.show();
		refresh();
	}
	
	void TrackingDialogue::refresh()
	{
		if(finder)	{
			listMenu.clear();
			const Objsc::ItemNames items  = finder->getItemNames();
			for(Objsc::ItemNames::const_iterator item = items.begin(); item != items.end(); ++item)	{
				listMenu.addItem((*item));
			}
			listMenu.refreshDisplay();
		}
	}
	
	void TrackingDialogue::close()
	{
		addBtn.looseFocus();
		removeBtn.looseFocus();
		actionBtn.looseFocus();
		cancelBtn.looseFocus();
		textField.looseFocus();
		textField.clearText();
		Objsc::CommandDispatcher::Current()->doCommand(Objsc::CommandDispatcher::createCommand("_gui_", "close_dialogue"));
	}
	
	// commands
	void TrackingDialogue::doCommand(Objsc::CommandPtr& cmd)
	{
		//printf("TrackingDialogue::doCommand <%s>\n", cmd->getName().c_str());
		if(cmd->getName() ==  "item_selected")	{
			if(listMenu.hasSelection())	{
				textField.setText(listMenu.getSelectedName());
			}
		} else if(cmd->getName() ==  "open_selection")	{
			if(listMenu.hasSelection())	{
				if(finder)	{
					finder->selectItem(listMenu.getSelectedName());
				}
				setText("");
				close();
			}
		} else if(cmd->getName() ==  "handle_input")	{
			if(!textField.getText().empty())	{
				if(finder)	{
					finder->handleInput(textField.getText());
				}
				setText("");
				close();
			}
		} else if(cmd->getName() ==  "add_item")	{
			if(finder)	{
				finder->addItem(textField.getText());
				refresh();
				setText("");
			}
		} else if(cmd->getName() ==  "remove_selected")	{
			if(listMenu.hasSelection() && finder)	{
				finder->removeItem(listMenu.getSelectedName());
				refresh();
				setText("");
			}
		} else if(cmd->getName() ==  "close_dialogue")	{
			close();
		}
	}
	
	void TrackingDialogue::undoCommand(Objsc::CommandPtr& cmd)
	{
		
	}
	
	// events
	void TrackingDialogue::keyPressed(int key)
	{
		textField.keyPressed(key);
	}
	
	void TrackingDialogue::keyReleased(int key)
	{
		textField.keyReleased(key);
	}
	
	void TrackingDialogue::mouseMoved(int x, int y )
	{
		textField.mouseMoved(x,y);
		if(edititems)	{
			addBtn.mouseMoved(x,y);
			removeBtn.mouseMoved(x,y);
		}
		actionBtn.mouseMoved(x,y);
		cancelBtn.mouseMoved(x,y);
		listMenu.mouseMoved(x,y);
	}
	
	void TrackingDialogue::mouseDragged(int x, int y, int button)
	{
		textField.mouseDragged(x,y,button);
		if(edititems)	{
			addBtn.mouseDragged(x,y,button);
			removeBtn.mouseDragged(x,y,button);
		}
		actionBtn.mouseDragged(x,y,button);
		cancelBtn.mouseDragged(x,y,button);
		listMenu.mouseDragged(x,y,button);
	}
	
	void TrackingDialogue::mousePressed(int x, int y, int button)
	{
		textField.mousePressed(x,y,button);
		if(edititems)	{
			addBtn.mousePressed(x,y,button);
			removeBtn.mousePressed(x,y,button);
		}
		actionBtn.mousePressed(x,y,button);
		cancelBtn.mousePressed(x,y,button);
		listMenu.mousePressed(x,y,button);
	}
	
	void TrackingDialogue::mouseReleased(int x, int y, int button)
	{
		
	}
	
	void TrackingDialogue::windowResized(int w, int h)
	{
		
	}
	

}

