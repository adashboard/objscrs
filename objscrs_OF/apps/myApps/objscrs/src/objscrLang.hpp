#ifndef  OBJSCR_LANG_H
#define OBJSCR_LANG_H

// objscrs
#include "objscrLangAtoms.hpp"
#include "objscrLangObjects.hpp"
#include "objscrLangZoneObjects.hpp"
#include "objscrLangMedia.hpp"
#include "objscrLangCommsObjects.hpp"
#include "objscrLangMathObjects.hpp"
#include "objscrLangPatch.hpp"
#include "objscrLangTimeObjects.hpp"

#endif // OBJSCR_LANG_H
