#ifndef  OBJSCR_LANGTOKENS_H
#define OBJSCR_LANGTOKENS_H

// STL
#include <iostream>
#include <string>
#include <vector>
using std::string;
using std::vector;

// shared_ptr
#include <tr1/memory>


namespace Objsc
{
	struct langtoken {
		string str;
		int argid = -1;
	};
	
	typedef vector < langtoken > langtokenVector;
	
	
	class LangTokens
	{
		private:
			langtokenVector tokens;
			static string delimiters = " ";
		
		public:
			LangTokens() : tokens() {}
			~LangTokens() { tokens.clear(); }
			
			void parse(const string& str)	{
				string::size_type lastPos = str.find_first_not_of(delimiters, 0);
				// Find first "non-delimiter".
				string::size_type pos = str.find_first_of(delimiters, lastPos);

				while (string::npos != pos || string::npos != lastPos) {
					// Found a token, add it to the vector.
					tokens.push_back(str.substr(lastPos, pos - lastPos));
					// Skip delimiters.  Note the "not_of"
					lastPos = str.find_first_not_of(delimiters, pos);
					// Find next "non-delimiter"
					pos = str.find_first_of(delimiters, lastPos);
				}
			}
			
	};
	
}


#endif // OBJSCR_LANGTOKENS_H
