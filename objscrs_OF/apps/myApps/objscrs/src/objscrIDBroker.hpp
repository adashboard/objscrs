#ifndef OBJSCR_IDBROKER_H
#define OBJSCR_IDBROKER_H


#include "singleton.h"

namespace Objsc
{

	class IDBroker : public Singleton<IDBroker>
	{
		private:
			unsigned int ids;
		
		public:
			unsigned int newID() { return ++ids; }
			void reset() { ids = 0; }
			void loadID(unsigned int newID)
			{
				if(newID > ids)	{
					ids=newID;
				}
			}
	};
	
}



#endif //OBJSCR_IDBROKER_H

