#include "objscrBody.hpp"

// objscrs
#include "objscrConstants.hpp"
#include "objscrStageView.hpp"
#include "objscrStage.hpp"


namespace Objsc
{
	
	////////////////////////////////////////
	// DECLARATIONS
	////////////////////////////////////////
	int Objsc::Body::unbornAge = 0;
	int Objsc::Body::unbornHits = 1;
	int Objsc::Body::unbornLastHit = 24;
	int Objsc::Body::becomingAge = 0;
	int Objsc::Body::becomingHits = 6;
	int Objsc::Body::becomingLastHit = 24;
	int Objsc::Body::presentLastHit = 24;
	int Objsc::Body::absentLastHitMin = 6;
	int Objsc::Body::absentLastHitMax = 24;
	
	
	////////////////////////////////////////
	// FUNCTIONS
	////////////////////////////////////////
	Objsc::LangAtomPtr bodyToLangAtom(const Objsc::BodyPtr& body, int numbodies)
	{
		Objsc::LangList* atom = new Objsc::LangList();
		atom->add((int)(body->getID()));
		atom->add(body->getStageX());
		atom->add(body->getStageY());
		atom->add(body->getStageArea());
		atom->add(body->getBodyArea());
		atom->add(numbodies);
		return Objsc::LangAtomPtr(atom);
	}
	
	////////////////////////////////////////
	// BODY
	////////////////////////////////////////
	Body::Body() :
				uid(0),
				stageNum(0),
				age(0),
				lasthit(0),
				hits(0),
				bestShape(-1),
				bestdiffarea(0.0f),
				bestdist(0.0f),
				maxdist(100.0f),
				state(UNBORN),
				screenRect(),
				stageRect(),
				shapeRect(),
				screenLoc(),
				stageLoc(),
				velocity(0.0f),
				bearing(0.0f),
				area(0.0f),
				label()
	{
		actionPresent = Objsc::CommandDispatcher::createCommand(Objsc::StageView::cmdlabel , "body_present");
		actionDead = Objsc::CommandDispatcher::createCommand(Objsc::StageView::cmdlabel , "body_dead");
		enterState(UNBORN);
	}
	
	
	void Body::newFrame()
	{
		bestShape = -1;
		bestdiffarea = Objsc::MAXFLOAT;
		bestdist = Objsc::MAXFLOAT;
	}
	
	void Body::compareShape(const ofxCvBlob& shape, int shapenum)
	{
		bool within = shapeRect.containsPoint(shape.centroid.x,shape.centroid.y);
		float diffarea = fabs(area - shape.area);
		float dist = distance(shape.centroid.x, shape.centroid.y, shapeLoc.x, shapeLoc.y);
		if(within)	{
			if(dist < maxdist && dist <= bestdist)	{
				bestdist = dist;
				bestShape = shapenum;
			}
		}
		//printf("[%u] dist: %f bestdist: %f\n", uid, dist,bestdist);
	}
	
	//~ bool Body::matchesArea(Objsc::BodyPtr other)
	//~ {
		//~ return stageRect.matches(other->getStageRect());
	//~ }
	
	
	void Body::update()
	{
		switch(state)	{
			case UNBORN:
				if(age > Objsc::Body::unbornAge && hits > Objsc::Body::unbornHits) {
					enterState(BECOMING);
				//} else if(age-lasthit > 40)	{
				} else if(age-lasthit > Objsc::Body::unbornLastHit)	{
					enterState(DEAD);
				}
				break;
			case BECOMING:
				//if(age > 0 && hits > 12)	{
				if(age > Objsc::Body::becomingAge && hits > Objsc::Body::becomingHits)	{
					enterState(PRESENT);
				//} else if(age-lasthit > 40)	{
				} else if(age-lasthit > Objsc::Body::becomingLastHit)	{
					enterState(DEAD);
				}
				break;
			case PRESENT:
				//if(age-lasthit > 12)
				if(age-lasthit > Objsc::Body::presentLastHit)
					enterState(ABSENT);
				break;
			case ABSENT:
				//if(age-lasthit > 40)	{
				if(age-lasthit > Objsc::Body::absentLastHitMax)	{
					enterState(DEAD);
				//} else if(age-lasthit < 12)	{
				} else if(age-lasthit < Objsc::Body::absentLastHitMin)	{
					enterState(PRESENT);
				} 
				break;
			case DEAD:
				
				break;
		}
		++age;
	}
	
	void Body::enterState(bodystate newstate)
	{
		switch(newstate)	{
			case UNBORN:
				//printf("[%u] UNBORN\n", uid);
				break;
			case BECOMING:
				//printf("[%u] BECOMING\n", uid);
				break;
			case PRESENT:
				//printf("[%u] PRESENT\n", uid);
				stageNum = Objsc::Stage::Current()->requestStageNumber();
				//printf("stageNum: %u", stageNum);
				Objsc::CommandDispatcher::Current()->doCommand(actionPresent);
				// create view
				break;
			case ABSENT:
				//printf("[%u] ABSENT\n", uid);
				break;
			case DEAD:
				//printf("[%u] DEAD age: %d\n", uid, age);
				if(stageNum > 0)	{
					Objsc::Stage::Current()->releaseStageNumber(stageNum);
					stageNum = 0;
				}
				Objsc::CommandDispatcher::Current()->doCommand(actionDead);
				// remove view
				// remove body
				break;
		}
		state=newstate;
	}
	
	void Body::initActions(std::tr1::weak_ptr<Objsc::Body> weakPtr)
	{
		actionPresent->getData()->addArg(weakPtr);
		actionDead->getData()->addArg(weakPtr);
	}
	
	void Body::updateShape(const ofxCvBlob& shape)
	{
		shapeRect.set(shape.boundingRect.x,shape.boundingRect.y,shape.boundingRect.width,shape.boundingRect.height);
                pts = shape.pts;
		area = shape.area;
		shapeLoc.x = shape.centroid.x;
		shapeLoc.y = shape.centroid.y;
		hits++;
		lasthit=age;
		
		//if(state == PRESENT || state == ABSENT)	{
		stageRect.set(	screenToStage(shape.boundingRect.x, Objsc::VIDSOURCE_WIDTH),
					screenToStage(shape.boundingRect.y, Objsc::VIDSOURCE_HEIGHT),
					screenToStage(shape.boundingRect.width, Objsc::VIDSOURCE_WIDTH),
					screenToStage(shape.boundingRect.height, Objsc::VIDSOURCE_HEIGHT));
		stageLoc.x = screenToStage(shapeLoc.x, Objsc::VIDSOURCE_WIDTH);
		stageLoc.y = screenToStage(shapeLoc.y, Objsc::VIDSOURCE_HEIGHT);
		//}
	}
	
	void Body::print()
	{
		printf("Body [%u] age: %d num: %u\n", uid, age, stageNum);
		shapeRect.print();
		stageRect.print();
	}
	
	void Body::resetThresholds()
	{
		Objsc::Body::unbornAge = 0;
		Objsc::Body::unbornHits = 1;
		Objsc::Body::unbornLastHit = 24;
		Objsc::Body::becomingAge = 0;
		Objsc::Body::becomingHits = 6;
		Objsc::Body::becomingLastHit = 24;
		Objsc::Body::presentLastHit = 24;
		Objsc::Body::absentLastHitMax = 24;
		Objsc::Body::absentLastHitMin = 6;
	}
		
	void Body::printThresholds()
	{
		printf("-----------------------------------:\n");
		printf("Body thresholds reset:\n");
		printf("\tunbornAge: %d\n", Objsc::Body::unbornAge);
		printf("\tunbornHits: %d\n", Objsc::Body::unbornHits);
		printf("\tunbornLastHit: %d\n", Objsc::Body::unbornLastHit);
		printf("\tbecomingAge: %d\n", Objsc::Body::becomingAge);
		printf("\tbecomingHits: %d\n", Objsc::Body::becomingHits);
		printf("\tbecomingLastHit: %d\n", Objsc::Body::becomingLastHit);
		printf("\tpresentLastHit: %d\n", Objsc::Body::presentLastHit);
		printf("\tabsentLastHitMin: %d\n", Objsc::Body::absentLastHitMin);
		printf("\tabsentLastHitMax: %d\n", Objsc::Body::absentLastHitMax);
		printf("-----------------------------------:\n");
	}
}
