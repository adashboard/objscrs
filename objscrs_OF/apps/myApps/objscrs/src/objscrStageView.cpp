#include "objscrStageView.hpp"

namespace Objsc
{
	
	////////////////////////////////////////
	// DECLARATIONS
	////////////////////////////////////////
	const string Objsc::StageView::cmdlabel   = "_stageview_";
	
	
	
	////////////////////////////////////////
	// FUNCTIONS
	////////////////////////////////////////
	struct matchesBody : std::binary_function< Objsc::BodyViewPtr, Objsc::BodyPtr, bool >	{
		bool operator() (const Objsc::BodyViewPtr& ptr, const Objsc::BodyPtr& body) const	{
			return ptr->matchesBody(body);
		}
	};
	
	
	////////////////////////////////////////
	// STAGE VIEW
	////////////////////////////////////////
	StageView::StageView() : 
		stage(), 
		bodyviews() 
	{
		autoSize();
		// commands
		Objsc::CommandDispatcher::Current()->registerCommandReceiver(Objsc::StageView::cmdlabel , this);
	}
	
	void StageView::update()
	{
		for(Objsc::BodyViewVec::iterator view = bodyviews.begin(); view != bodyviews.end(); ++view)	{
			(*view)->update();
		}
	}
	
	void StageView::draw()
	{
		if(visible)	{
			stage->draw();
			for(Objsc::BodyViewVec::iterator view = bodyviews.begin(); view != bodyviews.end(); ++view)	{
				(*view)->draw();
			}
		}
	}
	
	// commands
	void StageView::doCommand(Objsc::CommandPtr& cmd)
	{
		//std::cout << "StageView::doCommand: " << cmd->getName() << std::endl;
		string cmdname = cmd->getName();
		if(cmdname ==  "body_present")	{
			Objsc::BodyPtr body;
			cmd->getData()->getArg(0,&body);
			createBodyView(body);
		} else if(cmdname ==  "body_dead")	{
			Objsc::BodyPtr body;
			cmd->getData()->getArg(0,&body);
			removeBodyView(body);
		} 	
	}
	
	void StageView::undoCommand(Objsc::CommandPtr& cmd)
	{
		std::cout << "StageView::undoCommand: " << cmd->getName() << std::endl;
	}
	
	
	void StageView::doLoadActions()
	{
		if(stage)	{
			stage->doLoadActions();
		}
	}
	
	void StageView::createBodyView(Objsc::BodyPtr body)
	{
		//printf("StageView::createBodyView\n");
		bodyviews.push_back(Objsc::BodyView::create());
		bodyviews.back()->setBody(body);
	}
	
	void StageView::removeBodyView(const Objsc::BodyPtr body)
	{
		//printf("StageView::removeBodyView\n");
		bodyviews.erase(remove_if(bodyviews.begin(), bodyviews.end(), bind2nd(matchesBody(),  body)), bodyviews.end());
	}
	
	void StageView::clear()
	{
		bodyviews.clear();
	}
	
	
	void StageView::autoSize()
	{
		setBounds(0,0, ofGetWidth(),ofGetHeight());
	}
}

