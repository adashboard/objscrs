#ifndef  OBJSCR_LANGZONEOBJECTS_H
#define OBJSCR_LANGZONEOBJECTS_H

// objscrs
#include "singleton.h"
#include "objscrLangAtoms.hpp"
#include "objscrLangObjects.hpp"
#include "objscrConstants.hpp"
#include "objscrGeom.hpp"

// STL
#include <string>
using std::string;


// shared_ptr
#include <tr1/memory>


	
namespace Objsc
{
	
	/////////////////////////////////////
	// FUNCTIONS
	/////////////////////////////////////
	void initLangZoneObjects();
	
	
	/////////////////////////////////////
	// BODY POS OBJECT
	/////////////////////////////////////
	class BodyPosObject : public LangObject
	{
		protected:
			int 		id;
			float 	x;
			float 	y;
		
			void receiveBody(int inlet, LangList* nbody);
			
		public:
			BodyPosObject() : id(0), x(0.0f), y(0.0f)
			{
				setInlets(2);
				setOutlets(3);
			}
			~BodyPosObject() {}
				
			static const string langname;
			const string& getLangName() const { return Objsc::BodyPosObject::langname; }
			
			const string& getDisplayString()
			{
				if(displaystr.empty())	{
					return Objsc::BodyPosObject::langname;
				}
				return displaystr;
			}
			
			void receive(int inlet, Objsc::LangAtom* atom);
			void activate();
	};
	
	/////////////////////////////////////
	// BODY PROX OBJECT
	/////////////////////////////////////
	class BodyProxObject : public LangObject
	{
		protected:
			int 		id;
			float 	x;
			float 	y;
			float 	p;
		
			void receiveBody(int inlet, LangList* nbody);
			
		public:
			BodyProxObject() : id(0), x(0.0f), y(0.0f), p(0.0f)
			{
				setInlets(2);
				setOutlets(4);
			}
			~BodyProxObject() {}
				
			static const string langname;
			const string& getLangName() const { return Objsc::BodyProxObject::langname; }
			
			const string& getDisplayString()
			{
				if(displaystr.empty())	{
					return Objsc::BodyProxObject::langname;
				}
				return displaystr;
			}
			
			void receive(int inlet, Objsc::LangAtom* atom);
			void activate();
	};
	
	/////////////////////////////////////
	// BODY AREA OBJECT
	/////////////////////////////////////
	class BodyAreaObject : public LangObject
	{
		protected:
			int 		id;
			float 	shapeArea;
			float 	bodyArea;
		
			void receiveBody(int inlet, LangList* nbody);
			
		public:
			BodyAreaObject() : id(0), shapeArea(0.0f), bodyArea(0.0f)
			{
				setInlets(2);
				setOutlets(3);
			}
			~BodyAreaObject() {}
				
			static const string langname;
			const string& getLangName() const { return Objsc::BodyAreaObject::langname; }
			
			const string& getDisplayString()
			{
				if(displaystr.empty())	{
					return Objsc::BodyAreaObject::langname;
				}
				return displaystr;
			}
			
			void receive(int inlet, Objsc::LangAtom* atom);
			void activate();
	};
	
	/////////////////////////////////////
	// BODY MOVE OBJECT
	/////////////////////////////////////
	class BodyMoveObject : public LangObject
	{
		protected:
			int 		id;
			float 	x;
			float 	y;
			float 	pX;
			float 	pY;
			float 	velX;
			float 	velY;
			float	vel;
			float	dist;
			float	pT;
			float	t;
		
			void receiveBody(int inlet, LangList* nbody);
			
		public:
			BodyMoveObject() : id(0), x(0.0f), y(0.0f), pX(0.0f), pY(0.0f), velX(0.0f), velY(0.0f), vel(0.0), dist(0.0f), pT(0.0f), t(0.0f)
			{
				setInlets(2);
				setOutlets(5);
			}
			~BodyMoveObject() {}
				
			static const string langname;
			const string& getLangName() const { return Objsc::BodyMoveObject::langname; }
			
			const string& getDisplayString()
			{
				if(displaystr.empty())	{
					return Objsc::BodyMoveObject::langname;
				}
				return displaystr;
			}
			
			void receive(int inlet, Objsc::LangAtom* atom);
			void activate();
	};
	
	/////////////////////////////////////
	// ZONE OBJECT
	/////////////////////////////////////
	class ZoneObject : public LangObject
	{
		protected:
			Objsc::LangList 	body;
			int				numbodies;
			string 			label;
			
			void receiveBody(int inlet, LangList* nbody);
			
		public:
			ZoneObject() : label()
			{
				setInlets(1);
				setOutlets(3);
			}
			~ZoneObject() {}
				
			static const string langname;
			const string& getLangName() const { return Objsc::ZoneObject::langname; }
			
			void setLabel(string nlabel) {label.assign(nlabel);}
			const string& getLabel() const { return label; }
			
			const string& getDisplayString()
			{
				if(!displaystr.empty())	{
					return displaystr;
				} else if(!label.empty())	{
					return label;
				}
				return Objsc::ZoneObject::langname;
			}
			
			void receive(int inlet, Objsc::LangAtom* atom);
			void activate();
	};
	
	
	/////////////////////////////////////
	// ZONE MAP OBJECT
	/////////////////////////////////////
	class ZoneMapObject : public LangObject
	{
		protected:
			void showZones();
			void hideZones();
			void gotoMap(const string& mapname);
			void receiveList(int inlet, LangList* value);
		
		public:
			ZoneMapObject()
			{
				setInlets(1);
				setOutlets(2);
			}
			~ZoneMapObject() {}
				
			static const string langname;
			const string& getLangName() const { return Objsc::ZoneMapObject::langname; }
			
			
			const string& getDisplayString()
			{
				return Objsc::ZoneMapObject::langname;
			}
			
			void receive(int inlet, Objsc::LangAtom* atom);
	};
	
	
	/////////////////////////////////////
	// ZONE SCALE OBJECT
	/////////////////////////////////////
	class ZoneSpaceObject : public LangObject
	{
		protected:
			bool			active;
			Objsc::Rect		zonebounds;
		
			void receiveList(int inlet, LangList* value);
		
		public:
			ZoneSpaceObject() : active(true), zonebounds()
			{
				setInlets(3);
				setOutlets(3);
			}
			~ZoneSpaceObject() {}
				
			static const string langname;
			const string& getLangName() const { return Objsc::ZoneSpaceObject::langname; }
			
			const string& getDisplayString() { return Objsc::ZoneSpaceObject::langname;}
			
			void receive(int inlet, Objsc::LangAtom* atom);
	};
	
	/////////////////////////////////////
	// TRACKING OBJECT
	/////////////////////////////////////
	/*
	outlets:
	0: bang ref
	1: ref on/off
	2: start/stop track
	3: show/hide track
	*/
	class TrackingObject : public LangObject
	{
		protected:
			void showTracking();
			void hideTracking();
			void startTracking();
			void stopTracking();
			void takeRef();
			void startRef();
			void stopRef();
			void receiveList(int inlet, LangList* value);
		
			void setShapesMinRadius(int value);
			void setShapesMaxRadius(int value);
		
		public:
			TrackingObject()
			{
				setInlets(4);
				setOutlets(2);
			}
			~TrackingObject() {}
				
			static const string langname;
			const string& getLangName() const { return Objsc::TrackingObject::langname; }
			
			
			const string& getDisplayString()
			{
				return Objsc::TrackingObject::langname;
			}
			
			void receive(int inlet, Objsc::LangAtom* atom);
	};
	
	/////////////////////////////////////
	// SCORE OBJECT
	/////////////////////////////////////
	class ScoreObject : public LangObject
	{
		protected:
			void loadScore(const string& scorename);
			void receiveList(int inlet, LangList* value);
		
		public:
			ScoreObject()
			{
				setInlets(1);
				setOutlets(0);
			}
			~ScoreObject() {}
				
			static const string langname;
			const string& getLangName() const { return Objsc::ScoreObject::langname; }
			
			
			const string& getDisplayString()
			{
				return Objsc::ScoreObject::langname;
			}
			
			void receive(int inlet, Objsc::LangAtom* atom);
	};
	
	/////////////////////////////////////
	// BODY PREFS OBJECT
	/////////////////////////////////////
	class BodyPrefsObject : public LangObject
	{
		protected:
			
			void receiveList(int inlet, LangList* value);
		
		public:
			BodyPrefsObject()
			{
				setInlets(9);
				setOutlets(0);
			}
			~BodyPrefsObject() {}
				
			static const string langname;
			const string& getLangName() const { return Objsc::BodyPrefsObject::langname; }
			
			
			const string& getDisplayString()
			{
				return Objsc::BodyPrefsObject::langname;
			}
			
			void receive(int inlet, Objsc::LangAtom* atom);
	};
	
	
	
	////////////////////////////////////////
	// ZONE OBJECT CREATORS
	////////////////////////////////////////
	class BodyPosObjectCreator : public LangObjectCreator
	{
		public:
			Objsc::LangObjectPtr create() const { return Objsc::LangObjectPtr(new Objsc::BodyPosObject());}
	};
	
	class BodyProxObjectCreator : public LangObjectCreator
	{
		public:
			Objsc::LangObjectPtr create() const { return Objsc::LangObjectPtr(new Objsc::BodyProxObject());}
	};
	
	class BodyAreaObjectCreator : public LangObjectCreator
	{
		public:
			Objsc::LangObjectPtr create() const { return Objsc::LangObjectPtr(new Objsc::BodyAreaObject());}
	};
	
	class BodyMoveObjectCreator : public LangObjectCreator
	{
		public:
			Objsc::LangObjectPtr create() const { return Objsc::LangObjectPtr(new Objsc::BodyMoveObject());}
	};
	
	class ZoneObjectCreator : public LangObjectCreator
	{
		public:
			Objsc::LangObjectPtr create() const { return Objsc::LangObjectPtr(new Objsc::ZoneObject());}
	};
	
	class NoInletsZoneObjectCreator : public LangObjectCreator
	{
		protected:
			string displaystr;
		
		public:
			NoInletsZoneObjectCreator(const string& str="") : displaystr(str) {}
			Objsc::LangObjectPtr create() const 
			{ 
				Objsc::LangObjectPtr ptr = Objsc::LangObjectPtr(new Objsc::ZoneObject());
				ptr->hideInlets();
				if(!displaystr.empty())	{
					ptr->setLabel(displaystr);
				}
				return ptr;
			}
	};
	
	class ZoneMapObjectCreator : public LangObjectCreator
	{
		public:
			Objsc::LangObjectPtr create() const { return Objsc::LangObjectPtr(new Objsc::ZoneMapObject());}
	};
	
	
	class ZoneSpaceObjectCreator : public LangObjectCreator
	{
		public:
			Objsc::LangObjectPtr create() const { return Objsc::LangObjectPtr(new Objsc::ZoneSpaceObject());}
	};
	
	
	class TrackingObjectCreator : public LangObjectCreator
	{
		public:
			Objsc::LangObjectPtr create() const { return Objsc::LangObjectPtr(new Objsc::TrackingObject());}
	};
	
	class ScoreObjectCreator : public LangObjectCreator
	{
		public:
			Objsc::LangObjectPtr create() const { return Objsc::LangObjectPtr(new Objsc::ScoreObject());}
	};

	class BodyPrefsObjectCreator : public LangObjectCreator
	{
		public:
			Objsc::LangObjectPtr create() const { return Objsc::LangObjectPtr(new Objsc::BodyPrefsObject());}
	};
	
}

#endif // OBJSCR_LANGZONEOBJECTS_H


