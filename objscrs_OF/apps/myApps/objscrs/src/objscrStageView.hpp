#ifndef  OBJSCR_STAGEVIEW_H
#define OBJSCR_STAGEVIEW_H

// objscrs
#include "objscrStage.hpp"
#include "objscrGUI.hpp"
#include "objscrBodyViews.hpp"
#include "objscrBody.hpp"
#include "objscrCommands.hpp"

// shared_ptr
#include <tr1/memory>



namespace Objsc
{
	class StageView : public GuiBase, public CommandReceiver
	{
		private:
			Objsc::StagePtr 			stage;
			Objsc::BodyViewVec			bodyviews;
		
			void autoSize();
			
		public:
			StageView();
			~StageView() {}
				
			void setStage(Objsc::StagePtr stagePtr) {stage = stagePtr;}
			void update();
			void draw();
			
			void createBodyView(Objsc::BodyPtr body);
			void removeBodyView(const Objsc::BodyPtr body);
			
			void clear();
			
			// commands
			void doCommand(Objsc::CommandPtr& cmd);
			void undoCommand(Objsc::CommandPtr& cmd);
			
			void doLoadActions();
			
			static std::tr1::shared_ptr<Objsc::StageView> create()
			{
				return std::tr1::shared_ptr<Objsc::StageView>(new StageView());
			}
			
			static const string cmdlabel;
	};
	
	typedef std::tr1::shared_ptr<Objsc::StageView> StageViewPtr;
}

#endif // OBJSCR_STAGEVIEW_H