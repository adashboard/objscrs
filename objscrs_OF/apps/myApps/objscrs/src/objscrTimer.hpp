#ifndef  OBJSCR_TIMER_H
#define OBJSCR_TIMER_H

// objscrs
#include "singleton.h"
#include "objscrCommands.hpp"

// STL
#include <string>
#include <list>
#include <exception>
using std::string;
using std::list;
using std::exception;

// shared_ptr
#include <tr1/memory>


namespace Objsc
{
	////////////////////////////////////////
	// TIMED ACTION
	////////////////////////////////////////
	class TimedAction
	{
		private:
			int timecount;
			int timeout;
			int repeats;
			int repeatcount;
			bool active;
			Objsc::CommandPtr cmd;
		
		public:
			TimedAction() : timecount(0), timeout(0), repeats(0), repeatcount(0), active(true), cmd() {}
			TimedAction(int ntimeout, int nrepeats) //, string target, string command)
			{
				timeout = ntimeout;
				repeats = nrepeats;
				reset();
			}
			~TimedAction() {}
				
			void update(int timeframe);
			void expire();
			bool isExpired(){return active == false;}
			void fire();
				
			void reset()
			{
				timecount=0;
				repeatcount=0;
				active=true;
			}
			void setTimeout(int ntime)
			{
				timeout = ntime;
				reset();
			}
			void setRepeats(int nrepeats) 
			{ 
				repeats=nrepeats;
				repeatcount=0;
			}
			void setCommand(Objsc::CommandPtr newcmd) { cmd = newcmd; }
			Objsc::CommandPtr& getCommand() { return cmd; }
	};
	
	typedef std::tr1::shared_ptr<Objsc::TimedAction> TimedActionPtr;
	typedef list < Objsc::TimedActionPtr > TimedActionList;
	
	////////////////////////////////////////
	// TIMER
	////////////////////////////////////////
	bool actionHasExpired(Objsc::TimedActionPtr& action);
	
	class Timer  : public Singleton<Timer>
	{
		private:
			TimedActionList actions;
			
		public:
			Timer() : actions() {}
			~Timer() {actions.clear();}
				
			void update(int timeframe);
			
			TimedActionPtr& createAction(int timeout, int repeats) //, string target, string command)
			{
				actions.push_back(TimedActionPtr(new TimedAction(timeout, repeats)));  //, target, command));
				return actions.back();
			}
			
			TimedActionPtr& createAction(int timeout, int repeats, Objsc::CommandPtr newcmd)
			{
				actions.push_back(TimedActionPtr(new TimedAction(timeout, repeats)));
				actions.back()->setCommand(newcmd);
				return actions.back();
			}
			
	};
	
}

#endif // OBJSCR_TIMER_H

