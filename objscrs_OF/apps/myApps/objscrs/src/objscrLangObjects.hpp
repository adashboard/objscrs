#ifndef  OBJSCR_LANGOBJECTS_H
#define OBJSCR_LANGOBJECTS_H

// Objsc
#include "singleton.h"
#include "objscrLangAtoms.hpp"
#include "objscrConstants.hpp"

// STL
#include <iostream>
#include <string>
#include <vector>
#include <list>
#include <map>
#include <algorithm>
using std::string;
using std::vector;
using std::list;
using std::map;
using std::multimap;
using std::find;

// shared_ptr
#include <tr1/memory>


	
namespace Objsc
{
	
	/////////////////////////////////////
	// FUNCTIONS
	/////////////////////////////////////
	void initLangObjects();
	string makeReceiverName(const string& langname);
	
	
	/////////////////////////////////////
	// LANG OBJECTS
	/////////////////////////////////////
	class LangObject;
	
	typedef std::tr1::shared_ptr<Objsc::LangObject> LangObjectPtr;
	typedef list < Objsc::LangObjectPtr > LangObjectList;
	typedef LangObjectList::iterator LangObjectListIter;
	
	typedef std::pair < Objsc::LangObjectPtr, int > LangAddress;
	typedef vector < Objsc::LangAddress > LangAddressVector;
	
	typedef map < Objsc::LangObjectPtr, int > LangObjectEncodingMap;
	typedef LangObjectEncodingMap::value_type LangObjectEncodingEntry;
	typedef map < int, Objsc::LangObjectPtr > LangObjectDecodingMap;
	typedef LangObjectDecodingMap::value_type LangObjectDecodingEntry;
	
	
	class LangObserver
	{
		public:
			LangObserver() {}
			virtual ~LangObserver() {}
			virtual void objActivated() {};
	};
	
	typedef std::tr1::shared_ptr<Objsc::LangObserver> LangObserverPtr;
	
	
	/////////////////////////////////////
	// LANG CORD
	/////////////////////////////////////
	class LangCord
	{
		private:
			Objsc::LangAddressVector objects;
		
		public:
			LangCord() : objects() {}
			~LangCord() { objects.clear();}
			
			void link(LangObjectPtr objectPtr, int inlet)
			{
				objects.push_back(LangAddress(objectPtr, inlet));
			}
			
			void unlink(LangObjectPtr objectPtr, int inlet)
			{
				objects.erase(find(objects.begin(), objects.end(), LangAddress(objectPtr, inlet)));
			}
			
			void unlinkAll(LangObjectPtr objectPtr);
			
			bool connectsTo(Objsc::LangObjectPtr objectPtr);
			
			const Objsc::LangAddressVector& getAddresses() const { return objects; }
			
			const bool hasAddresses() const { return !objects.empty();}
			const int numAddresses() const { return objects.empty()?0:objects.size();}
			
			void clear(){objects.clear();}
			void send(LangAtom* atom);
	};
	
	typedef std::tr1::shared_ptr<Objsc::LangCord> LangCordPtr;
	typedef map < int, Objsc::LangCord* > LangCordMap;
	typedef LangCordMap::value_type LangCordEntry;
	
	
	/////////////////////////////////////
	// LANG RECEIVER
	/////////////////////////////////////
	class LangReceiver
	{
		private:
			Objsc::LangObjectList	objects;
			string				label;
		
		public:
			LangReceiver(const string nlabel="") : objects(), label(nlabel) {}
			~LangReceiver() { clear();}
			
			void add(LangObjectPtr objectPtr);
			void remove(LangObjectPtr objectPtr);
			
			void clear(){objects.clear();}
			void receive(LangAtom* atom);
			
			void setLabel(const string& nlabel) { label = nlabel; }
			const string& getLabel() const { return label; }
			
			const bool empty() const { return objects.empty(); }
			const int size() const { return objects.size(); }
			
			const Objsc::LangObjectList& getObjects() const { return objects; }
			
			static std::tr1::shared_ptr<Objsc::LangReceiver> create(const string nlabel)
			{
				return std::tr1::shared_ptr<Objsc::LangReceiver>(new LangReceiver(nlabel));
			} 
	};
	
	typedef std::tr1::shared_ptr<Objsc::LangReceiver> LangReceiverPtr;
	typedef map < string, Objsc::LangReceiverPtr > LangReceiverMap;
	typedef LangReceiverMap::value_type LangReceiverEntry;
	
	/////////////////////////////////////
	// LANG OBJECT
	/////////////////////////////////////
	class LangObject
	{
		protected:
			int 					num_inlets;
			int 					num_outlets;
			LangCordMap 			outlets;
			float				x;
			float				y;
			bool				show_inlets;
			bool				show_outlets;
			string				displaystr;
			int 					min_args;
			int 					max_args;
		
			virtual void receiveBang(int inlet);
			virtual void receiveInt(int inlet, int value);
			virtual void receiveFloat(int inlet, float value);
			virtual void receiveString(int inlet, string value);
			virtual void receiveList(int inlet, LangList* value);
			virtual void parseValue(LangAtom* atom);
			virtual string parseCmd(LangAtom* atom) {}
		
		public:
			LangObject();
			virtual ~LangObject() {}
			
			virtual void cleanUp() {clearOutlets();}
			
			virtual bool parseArgs(Objsc::LangList* args) {return true;}
			virtual bool parseMessage(Objsc::LangList* msg) {return true;}
			
			static const string langname;
			virtual const string& getLangName() const { return Objsc::LangObject::langname; }
			
			virtual void setLabel(string nlabel) {}
			virtual const string& getLabel() const { return Objsc::NULL_STRING; }
			
			virtual void setReceiverName(string rname) {}
			virtual const string& getReceiverName() const { return getLangName(); }
			virtual bool hasReceiverName() { return false; }
			
			virtual void setInlets(int size) { num_inlets = size; }
			virtual void setOutlets(int size);
			
			const Objsc::LangCordMap getOutlets() const { return outlets; }
			
			void showInlets() { show_inlets=true;}
			void hideInlets() { show_inlets=false;}
			void showOutlets() { show_outlets=true;}
			void hideOutlets() { show_outlets=false;}
			
			virtual void setDisplayString(const string& str) {displaystr=str;}
			virtual const string& getDisplayString();
			
			const int getNumInlets() const { return show_inlets?num_inlets:0; }
			const int getNumOutlets() const { return show_outlets?num_outlets:0; }
			const int definedOutlets() const;
			
			virtual void link(int outlet, LangObjectPtr objectPtr, int inlet);
			virtual void unlink(int outlet, LangObjectPtr objectPtr, int inlet);
			virtual void unlinkAll(Objsc::LangObjectPtr objectPtr);
			bool connectsTo(Objsc::LangObjectPtr objectPtr);
			virtual void clearOutlet(int outlet);
			virtual void clearOutlets();
				
			virtual void receive(int inlet, Objsc::LangAtom* atom);
			virtual void send(int index, LangAtom* atom);
			
			virtual void activate() {}
			virtual void update(int timeframe) {}
				
			virtual void setLocation(float nx, float ny);
			float getX() const { return x; }
			float getY() const { return y; }
			
			virtual void setObserver(Objsc::LangObserverPtr obsPtr) {}
			virtual void removeObserver() {}
				
			virtual const bool hasArgs() const { return false;}
			virtual Objsc::LangList* getArgs() { return NULL; }
			virtual string getArgString() { return Objsc::NULL_STRING; } 
	};
	
	/////////////////////////////////////
	// BANG OBJECT
	/////////////////////////////////////
	class BangObject : public LangObject
	{
		protected:
			Objsc::LangObserverPtr 		observer;
		
		public:
			BangObject() : observer()
			{
				setInlets(1);
				setOutlets(1);
			}
			~BangObject() {}
				
			static const string langname;
			const string& getLangName() const { return Objsc::BangObject::langname; }
			const string& getDisplayString(){return Objsc::BangObject::langname;}
			
			void receive(int inlet, Objsc::LangAtom* atom) {activate();}
			void activate()
			{
				if(observer)
					observer->objActivated();
				send(0, new Objsc::LangBang());
			}
			
			void setObserver(Objsc::LangObserverPtr obsPtr) {observer=obsPtr;}
			void removeObserver() {observer.reset();}
	};
	
	/////////////////////////////////////
	// LOADBANG OBJECT
	/////////////////////////////////////
	class LoadBangObject : public BangObject
	{
		public:
			LoadBangObject()
			{
				setInlets(0);
				setOutlets(1);
			}
			~LoadBangObject() {}
				
			static const string langname;
			const string& getLangName() const { return Objsc::LoadBangObject::langname; }
			const string& getDisplayString(){return Objsc::LoadBangObject::langname;}
			static const string recname;
			//const string& getReceiverName() const { return makeReceiverName(getLangName());}
			bool hasReceiverName() { return true; }
	};
	
	/////////////////////////////////////
	// PATCH ON OBJECT
	/////////////////////////////////////
	class PatchOnObject : public BangObject
	{
		public:
			PatchOnObject()
			{
				setInlets(0);
				setOutlets(1);
			}
			~PatchOnObject() {}
				
			static const string langname;
			const string& getLangName() const { return Objsc::PatchOnObject::langname; }
			const string& getDisplayString(){return Objsc::PatchOnObject::langname;}
			//const string& getReceiverName() const { return makeReceiverName(getLangName());}
			bool hasReceiverName() { return true; }
	};
	
	/////////////////////////////////////
	// PATCH OFF OBJECT
	/////////////////////////////////////
	class PatchOffObject : public BangObject
	{
		public:
			PatchOffObject()
			{
				setInlets(0);
				setOutlets(1);
			}
			~PatchOffObject() {}
				
			static const string langname;
			const string& getLangName() const { return Objsc::PatchOffObject::langname; }
			const string& getDisplayString(){return Objsc::PatchOffObject::langname;}
			//const string& getReceiverName() const { return makeReceiverName(getLangName());}
			bool hasReceiverName() { return true; }
	};
	
	/////////////////////////////////////
	// TOGGLE OBJECT
	/////////////////////////////////////
	class ToggleObject : public LangObject
	{
		protected:
			Objsc::LangObserverPtr 		observer;
			bool					on;
		
			void receiveList(int inlet, LangList* value);
		
		public:
			ToggleObject() : observer(), on(false)
			{
				setInlets(1);
				setOutlets(1);
			}
			~ToggleObject() {}
				
			static const string langname;
			const string& getLangName() const { return Objsc::ToggleObject::langname; }
			const string& getDisplayString(){return Objsc::ToggleObject::langname;}
			
			void receive(int inlet, Objsc::LangAtom* atom);
			void activate()
			{
				on = !on;
				if(observer)
					observer->objActivated();
				send(0, new Objsc::LangInt((int)on));
			}
			
			void turnOn();
			void turnOff();
			
			const bool isOn() const { return on; }
			
			void setObserver(Objsc::LangObserverPtr obsPtr);
			void removeObserver();
	};
	
	/////////////////////////////////////
	// MESSAGE OBJECT
	/////////////////////////////////////
	class MessageObject : public LangObject
	{
		protected:
			Objsc::LangList message;
			
		public:
			MessageObject() : message()
			{
				setInlets(1);
				setOutlets(1);
			}
			~MessageObject() {}
				
			static const string langname;
			const string& getLangName() const { return Objsc::MessageObject::langname; }
			void setDisplayString(const string& str);
			const string& getDisplayString(){return displaystr;}
			
			void receive(int inlet, Objsc::LangAtom* atom);
			void activate();
	};
	
	/////////////////////////////////////
	// NUMBER OBJECT
	/////////////////////////////////////
	class NumberObject : public LangObject
	{
		protected:
			float					value;
			string					label;
			Objsc::LangObserverPtr 		observer;
		
			void receiveList(int inlet, LangList* value);
			
		public:
			NumberObject() : value(), label(), observer()
			{
				setInlets(1);
				setOutlets(1);
			}
			~NumberObject() {}
				
			static const string langname;
			const string& getLangName() const { return Objsc::NumberObject::langname; }
			void setDisplayString(const string& str);
			const string& getDisplayString(){return displaystr;}
			
			void setLabel(string nlabel) {label.assign(nlabel);}
			const string& getLabel() const { return label; }
			
			void setValue(float nvalue);
			const float getValue() const { return value; }
			
			void receive(int inlet, Objsc::LangAtom* atom);
			void activate();
			
			void setObserver(Objsc::LangObserverPtr obsPtr) {observer=obsPtr;}
			void removeObserver() {observer.reset();}
	};
	
	/////////////////////////////////////
	// SELECT OBJECT
	/////////////////////////////////////
	class SelectObject : public LangObject
	{
		protected:
			Objsc::LangList		keys;
			string			label;
		
			void select(Objsc::LangAtom* atom);
			
		public:
			SelectObject() : keys(), label()
			{
				setInlets(1);
				setOutlets(1);
			}
			~SelectObject() {}
				
			static const string langname;
			const string& getLangName() const { return Objsc::SelectObject::langname; }
			const string& getDisplayString(){return displaystr;}
			
			void setLabel(string nlabel) {label.assign(nlabel);}
			const string& getLabel() const { return label; }
			
			bool parseArgs(Objsc::LangList* args);
			void receive(int inlet, Objsc::LangAtom* atom);
			
			const bool hasArgs() const { return true;}
			Objsc::LangList* getArgs() { return &keys; }
			string getArgString() { return listToString(&keys); }
	};
	
	/////////////////////////////////////
	// TRIGGER OBJECT
	/////////////////////////////////////
	class TriggerObject : public LangObject
	{
		protected:
			Objsc::LangList		keys;
			string			label;
		
			void trigger(Objsc::LangAtom* atom);
			
		public:
			TriggerObject() : keys(), label()
			{
				setInlets(1);
				setOutlets(0);
			}
			~TriggerObject() {}
				
			static const string langname;
			const string& getLangName() const { return Objsc::TriggerObject::langname; }
			const string& getDisplayString(){return displaystr;}
			
			void setLabel(string nlabel) {label.assign(nlabel);}
			const string& getLabel() const { return label; }
			
			bool parseArgs(Objsc::LangList* args);
			void receive(int inlet, Objsc::LangAtom* atom);
			
			const bool hasArgs() const { return true;}
			Objsc::LangList* getArgs() { return &keys; }
			string getArgString() { return listToString(&keys); }
	};
	
	/////////////////////////////////////
	// UNPACK OBJECT
	/////////////////////////////////////
	class UnpackObject : public LangObject
	{
		protected:
			Objsc::LangList		keys;
			string			label;
		
			void receiveList(int inlet, Objsc::LangList* atom);
			
		public:
			UnpackObject() : keys(), label()
			{
				setInlets(1);
				setOutlets(0);
			}
			~UnpackObject() {}
				
			static const string langname;
			const string& getLangName() const { return Objsc::UnpackObject::langname; }
			const string& getDisplayString(){return displaystr;}
			
			void setLabel(string nlabel) {label.assign(nlabel);}
			const string& getLabel() const { return label; }
			
			bool parseArgs(Objsc::LangList* args);
			void receive(int inlet, Objsc::LangAtom* atom);
			
			const bool hasArgs() const { return true;}
			Objsc::LangList* getArgs() { return &keys; }
			string getArgString() { return listToString(&keys); }
	};
	
	/////////////////////////////////////
	// PACK OBJECT
	/////////////////////////////////////
	class PackObject : public LangObject
	{
		protected:
			Objsc::LangList		input;
			Objsc::LangList		keys;
			string			label;
		
			void receiveList(int inlet, Objsc::LangList* atom);
			
		public:
			PackObject() : input(), keys(), label()
			{
				setInlets(0);
				setOutlets(1);
			}
			~PackObject() {}
				
			static const string langname;
			const string& getLangName() const { return Objsc::PackObject::langname; }
			const string& getDisplayString(){return displaystr;}
			
			void setLabel(string nlabel) {label.assign(nlabel);}
			const string& getLabel() const { return label; }
			
			bool parseArgs(Objsc::LangList* args);
			void receive(int inlet, Objsc::LangAtom* atom);
			void activate();
			
			const bool hasArgs() const { return true;}
			Objsc::LangList* getArgs() { return &keys; }
			string getArgString() { return listToString(&keys); }
	};
	
	/////////////////////////////////////
	// PRINT OBJECT
	/////////////////////////////////////
	class PrintObject : public LangObject
	{
		protected:
			string buffer;
			string label;
		
			void receiveList(LangList* value) 
			{	
				for(LangAtomIter atom = value->begin(); atom != value->end(); ++atom) {
					parseValue((*atom));
					buffer.append(" ");
				}
			}
			virtual void parseValue(LangAtom* atom)
			{
				switch(atom->getType())	{
					case LANG_NULL:
						break;
					case LANG_BANG:
						buffer.append("bang!");
						break;
					case LANG_INT:
						buffer.append(intToString(parseInt(atom)));
						break;
					case LANG_FLOAT:
						buffer.append(floatToString(parseFloat(atom)));
						break;
					case LANG_STRING:
						buffer.append(parseString(atom));
						break;
					case LANG_LIST:
						receiveList(dynamic_cast<Objsc::LangList*>(atom));
						break;
					case LANG_VAR:
						buffer.append(varToString(dynamic_cast<Objsc::LangVar*>(atom)));
						break;
				}
			}
			
		public:
			PrintObject() : buffer(), label()
			{
				setInlets(1);
				setOutlets(0);
				max_args = 1;
			}
			~PrintObject() {}
				
			static const string langname;
			const string& getLangName() const { return Objsc::PrintObject::langname; }
			
			bool parseArgs(Objsc::LangList* args);
			
			virtual void setLabel(string nlabel);
			virtual const string& getLabel() const { return label; }
			const string& getDisplayString();
			
			void receive(int inlet, Objsc::LangAtom* atom);
			void send(int index, LangAtom* atom) {}
			virtual void activate();
	};
	
	/////////////////////////////////////
	// PRINT SCREEN OBJECT
	/////////////////////////////////////
	class PrintScreenObject : public PrintObject
	{
		
		public:
			static const string langname;
			const string& getLangName() const { return Objsc::PrintScreenObject::langname; }
			
			void setLabel(string nlabel);
			const string& getDisplayString();
			void activate();
	};
	
	/////////////////////////////////////
	// GATE OBJECT
	/////////////////////////////////////
	class GateObject : public LangObject
	{
		protected:
			bool			active;
		
			void receiveList(int inlet, LangList* value);
			void open() { active=true; }
			void close() { active=false; }
		
		public:
			GateObject() : active(false)
			{
				setInlets(2);
				setOutlets(1);
			}
			~GateObject() {}
				
			static const string langname;
			const string& getLangName() const { return Objsc::GateObject::langname; }
			
			void receive(int inlet, Objsc::LangAtom* atom);
	};
	
	
	////////////////////////////////////////
	// LANG OBJECT CREATORS
	////////////////////////////////////////
	class LangObjectCreator
	{
		public:
			virtual ~LangObjectCreator() {}
			virtual Objsc::LangObjectPtr create() const =0;
	};
	
	class BangObjectCreator : public LangObjectCreator
	{
		public:
			Objsc::LangObjectPtr create() const { return Objsc::LangObjectPtr(new Objsc::BangObject());}
	};
	
	
	class LoadBangObjectCreator : public LangObjectCreator
	{
		public:
			Objsc::LangObjectPtr create() const { return Objsc::LangObjectPtr(new Objsc::LoadBangObject());}
	};
	
	class PatchOnObjectCreator : public LangObjectCreator
	{
		public:
			Objsc::LangObjectPtr create() const { return Objsc::LangObjectPtr(new Objsc::PatchOnObject());}
	};
	
	class PatchOffObjectCreator : public LangObjectCreator
	{
		public:
			Objsc::LangObjectPtr create() const { return Objsc::LangObjectPtr(new Objsc::PatchOffObject());}
	};
	
	
	class ToggleObjectCreator : public LangObjectCreator
	{
		public:
			Objsc::LangObjectPtr create() const { return Objsc::LangObjectPtr(new Objsc::ToggleObject());}
	};
	
	class MessageObjectCreator : public LangObjectCreator
	{
		public:
			Objsc::LangObjectPtr create() const { return Objsc::LangObjectPtr(new Objsc::MessageObject());}
	};
	
	class PrintObjectCreator : public LangObjectCreator
	{
		public:
			Objsc::LangObjectPtr create() const { return Objsc::LangObjectPtr(new Objsc::PrintObject());}
	};
	
	class PrintScreenObjectCreator : public LangObjectCreator
	{
		public:
			Objsc::LangObjectPtr create() const { return Objsc::LangObjectPtr(new Objsc::PrintScreenObject());}
	};
	
	class NumberObjectCreator : public LangObjectCreator
	{
		protected:
			string displaystr;
		
		public:
			NumberObjectCreator(const string& str="") : displaystr(str) {}
			Objsc::LangObjectPtr create() const 
			{ 
				Objsc::LangObjectPtr ptr = Objsc::LangObjectPtr(new Objsc::NumberObject());
				if(!displaystr.empty())	{
					ptr->setLabel(displaystr);
				}
				return ptr;
			}
	};
	
	class SelectObjectCreator : public LangObjectCreator
	{
		protected:
			string displaystr;
		
		public:
			SelectObjectCreator(const string& str="") : displaystr(str) {}
			Objsc::LangObjectPtr create() const 
			{ 
				Objsc::LangObjectPtr ptr = Objsc::LangObjectPtr(new Objsc::SelectObject());
				if(!displaystr.empty())	{
					ptr->setLabel(displaystr);
				}
				return ptr;
			}
	};
	
	class TriggerObjectCreator : public LangObjectCreator
	{
		protected:
			string displaystr;
		
		public:
			TriggerObjectCreator(const string& str="") : displaystr(str) {}
			Objsc::LangObjectPtr create() const 
			{ 
				Objsc::LangObjectPtr ptr = Objsc::LangObjectPtr(new Objsc::TriggerObject());
				if(!displaystr.empty())	{
					ptr->setLabel(displaystr);
				}
				return ptr;
			}
	};
	
	class UnpackObjectCreator : public LangObjectCreator
	{
		public:
			Objsc::LangObjectPtr create() const { return Objsc::LangObjectPtr(new Objsc::UnpackObject());}
	};
	
	class PackObjectCreator : public LangObjectCreator
	{
		public:
			Objsc::LangObjectPtr create() const { return Objsc::LangObjectPtr(new Objsc::PackObject());}
	};
	
	class GateObjectCreator : public LangObjectCreator
	{
		public:
			Objsc::LangObjectPtr create() const { return Objsc::LangObjectPtr(new Objsc::GateObject());}
	};
	
	////////////////////////////////////////
	// LANG OBJECT FACTORY
	////////////////////////////////////////
	typedef map < string, LangObjectCreator* > LangObjectCreatorMap;
	typedef LangObjectCreatorMap::value_type LangObjectCreatorEntry;

	class LangObjectFactory : public Singleton<LangObjectFactory>
	{
		private:
			LangObjectCreatorMap creators;
		
		public:
			LangObjectFactory() : creators() {}
			~LangObjectFactory() { creators.clear(); }
			
			Objsc::LangObjectPtr create(const string& langname)
			{
				LangObjectCreatorMap::iterator iter = creators.find(langname);
				if(iter == creators.end()) {
					throw std::exception();
				}
				return (*iter).second->create();
			}
			
			void addCreator(const string& langname, LangObjectCreator* creatorPtr)
			{
				creators.insert(LangObjectCreatorEntry(langname, creatorPtr));
			}
			
			bool hasCreator(const string& langname)	{return creators.count(langname) > 0;}
			
	};
	
	
}

#endif // OBJSCR_LANGOBJECTS_H
