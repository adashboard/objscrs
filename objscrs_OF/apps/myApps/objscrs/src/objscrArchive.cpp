#include "objscrArchive.hpp"


// objscrs
#include "objscrApp.hpp"
#include "objscrGUIRoot.hpp"
#include "objscrLang.hpp"
#include "objscrColours.hpp"
#include "objscrGeom.hpp"
#include "objscrArchiveLang.hpp"



namespace Objsc
{
	
	////////////////////////////////////////
	// WRITER
	////////////////////////////////////////
	Writer::Writer()
	{
		
	}
		
	void Writer::write(Objsc::ScorePtr score)
	{
		TiXmlDocument doc = TiXmlDocument();
		TiXmlDeclaration *decl = new TiXmlDeclaration("1.0", "", "");  
		doc.LinkEndChild(decl);
		
		TiXmlElement *root = new TiXmlElement("objscrs");  
		doc.LinkEndChild(root);
		
		encodeScore(root, score);
		
		doc.SaveFile(Objsc::ProjectFiles::Current()->makeScorePath(score->getName()));
	}
	
	void Writer::encodeScore(TiXmlElement *parentNode, Objsc::ScorePtr score)
	{
		TiXmlElement *scoreNode = new TiXmlElement("score");
		TiXmlElement *nameEle = new TiXmlElement("name");
		nameEle->LinkEndChild(new TiXmlText(score->getName()));
		scoreNode->LinkEndChild(nameEle);
		// notations
		const Objsc::ZoneMapMap& zonemaps = score->getZoneMaps();
		if(!zonemaps.empty())	{
			TiXmlElement *zonemapsNode = new TiXmlElement("zonemaps");
			for(Objsc::ZoneMapMap::const_iterator item=zonemaps.begin(); item != zonemaps.end(); ++item)	{
				encodeZoneMap(zonemapsNode, (*item).second);
			}
			scoreNode->LinkEndChild(zonemapsNode);
		}
		parentNode->LinkEndChild(scoreNode);
	}
	
	
	void Writer::encodeZoneMap(TiXmlElement *parentNode, Objsc::ZoneMapPtr zonemap)
	{
		TiXmlElement *zmapNode = new TiXmlElement("zonemap");
		TiXmlElement *nameEle = new TiXmlElement("name");
		nameEle->LinkEndChild(new TiXmlText(zonemap->getName()));
		zmapNode->LinkEndChild(nameEle);
		// zones
		const Objsc::ZonePtrList& zones = zonemap->getZones();
		if(!zones.empty())	{
			TiXmlElement *zonesNode = new TiXmlElement("zones");
			zonesNode->SetAttribute("count", zonemap->getZonecount());
			for(Objsc::ZonePtrList::const_iterator item=zones.begin(); item != zones.end(); ++item)	{
				encodeZone(zonesNode, (*item));
			}
			zmapNode->LinkEndChild(zonesNode);
		}
		parentNode->LinkEndChild(zmapNode);
	}
	
	
	void Writer::encodeZone(TiXmlElement *parentNode, Objsc::ZonePtr zone)
	{
		TiXmlElement *zoneNode = new TiXmlElement("zone");
		TiXmlElement *typeEle = new TiXmlElement("type");
		typeEle->LinkEndChild(new TiXmlText(zone->getZoneType()));
		zoneNode->LinkEndChild(typeEle);
		TiXmlElement *nameEle = new TiXmlElement("name");
		nameEle->LinkEndChild(new TiXmlText(zone->getName()));
		zoneNode->LinkEndChild(nameEle);
		TiXmlElement *boundsEle = new TiXmlElement("bounds");
		encodeRect(boundsEle, zone->getBounds());
		zoneNode->LinkEndChild(boundsEle);
		// patch
		if(zone->hasPatch() )	{
			encodeLangPatch(zoneNode, zone->getPatch());
		}
		parentNode->LinkEndChild(zoneNode);
	}
	
	void Writer::encodeRect(TiXmlElement *parentNode, Objsc::Rect& rect)
	{
		TiXmlElement *rectNode = new TiXmlElement("rect");
		rectNode->SetDoubleAttribute("x", rect.getX());
		rectNode->SetDoubleAttribute("y", rect.getY());
		rectNode->SetDoubleAttribute("width", rect.getWidth());
		rectNode->SetDoubleAttribute("height", rect.getHeight());
		parentNode->LinkEndChild(rectNode);
	}
	
	void Writer::encodeRect(TiXmlElement *parentNode, const Objsc::Rect& rect)
	{
		TiXmlElement *rectNode = new TiXmlElement("rect");
		rectNode->SetDoubleAttribute("x", rect.getX());
		rectNode->SetDoubleAttribute("y", rect.getY());
		rectNode->SetDoubleAttribute("width", rect.getWidth());
		rectNode->SetDoubleAttribute("height", rect.getHeight());
		parentNode->LinkEndChild(rectNode);
	}
	
	
	void Writer::encodeColour(TiXmlElement *parentNode, Objsc::Colour& colour)
	{
		TiXmlElement *colourNode = new TiXmlElement("colour");
		colourNode->SetAttribute("r", colour.r);
		colourNode->SetAttribute("g", colour.g);
		colourNode->SetAttribute("b", colour.b);
		colourNode->SetAttribute("a", colour.a);
		parentNode->LinkEndChild(colourNode);
	}
	
	void Writer::encodeLangPatch(TiXmlElement *parentNode, Objsc::LangPatchPtr patch)
	{
		TiXmlElement *patchNode = new TiXmlElement("patch");
		TiXmlElement *nameEle = new TiXmlElement("name");
		nameEle->LinkEndChild(new TiXmlText(patch->getName()));
		patchNode->LinkEndChild(nameEle);
		// objects
		Objsc::LangObjectEncodingMap objectIDs;
		TiXmlElement *objectsEle = new TiXmlElement("objects");
		const Objsc::LangObjectList& objects = patch->getObjects();
		int encID = 0;
		for(Objsc::LangObjectList::const_iterator obj = objects.begin(); obj != objects.end(); ++obj)	{
			objectIDs.insert(Objsc::LangObjectEncodingEntry((*obj), encID));
			encodeLangObject(objectsEle, (*obj), encID);
			++encID;
		}
		patchNode->LinkEndChild(objectsEle);
		// cords
		TiXmlElement *cordsEle = new TiXmlElement("cords");
		for(Objsc::LangObjectList::const_iterator obj = objects.begin(); obj != objects.end(); ++obj)	{
			encodeLangCord(cordsEle, (*obj), objectIDs);
		}
		patchNode->LinkEndChild(cordsEle);
		// receivers
		const Objsc::LangReceiverMap& receivers = patch->getReceivers();
		TiXmlElement *receiversEle = new TiXmlElement("receivers");
		for(Objsc::LangReceiverMap::const_iterator rec = receivers.begin(); rec != receivers.end(); ++rec)	{
			TiXmlElement *recEle = new TiXmlElement("receiver");
			TiXmlElement *recnameEle = new TiXmlElement("name");
			recnameEle->LinkEndChild(new TiXmlText(rec->first));
			recEle->LinkEndChild(recnameEle);
			TiXmlElement *recObjectsEle = new TiXmlElement("objects");
			const Objsc::LangObjectList& recObjs = rec->second->getObjects();
			for(Objsc::LangObjectList::const_iterator obj = recObjs.begin(); obj != recObjs.end(); ++obj)	{
				TiXmlElement *recObjectEle = new TiXmlElement("object");
				recObjectEle->SetAttribute("encid", objectIDs[(*obj)]);
				recObjectsEle->LinkEndChild(recObjectEle);
			}
			recEle->LinkEndChild(recObjectsEle);
			receiversEle->LinkEndChild(recEle);
		}
		patchNode->LinkEndChild(receiversEle);
		parentNode->LinkEndChild(patchNode);
	}
	
	void Writer::encodeLangObject(TiXmlElement *parentNode, Objsc::LangObjectPtr object, int encID)
	{
		Objsc::LangObjectArchiverPtr archiver = Objsc::LangObjectArchiverFactory::Current()->create(object->getLangName());
		if(archiver)	{
			archiver->encode(parentNode, object, encID);
		}
	}
	
	void Writer::encodeLangCord(TiXmlElement *parentNode, Objsc::LangObjectPtr object, Objsc::LangObjectEncodingMap& objectIDs)
	{
		if(object->getNumOutlets() == 0 || object->definedOutlets() == 0) {
			return;
		}
		
		TiXmlElement *cordEle = new TiXmlElement("cordset");
		cordEle->SetAttribute("encid", objectIDs[object]);
		const Objsc::LangCordMap outlets = object->getOutlets();
		TiXmlElement *outletsEle = new TiXmlElement("outlets");
		for(Objsc::LangCordMap::const_iterator outlet = outlets.begin(); outlet != outlets.end(); ++outlet)	{
			if(outlet->second->hasAddresses())	{
				TiXmlElement *outletEle = new TiXmlElement("outlet");
				outletEle->SetAttribute("index", outlet->first);
				TiXmlElement *addressesEle = new TiXmlElement("addresses");
				const Objsc::LangAddressVector& addresses = outlet->second->getAddresses();
				for(Objsc::LangAddressVector::const_iterator addrs = addresses.begin(); addrs != addresses.end(); ++addrs)	{
					TiXmlElement *addrsEle = new TiXmlElement("address");
					addrsEle->SetAttribute("encid", objectIDs[addrs->first]);
					addrsEle->SetAttribute("inlet", addrs->second);
					addressesEle->LinkEndChild(addrsEle);
				}
				outletEle->LinkEndChild(addressesEle);
				outletsEle->LinkEndChild(outletEle);
			}
		}
		cordEle->LinkEndChild(outletsEle);
		parentNode->LinkEndChild(cordEle);
	}
	
	void Writer::encodeLangAtom(TiXmlElement *parentNode, Objsc::LangAtom* atom)
	{
		TiXmlElement *atomNode = new TiXmlElement("atom");
		atomNode->SetAttribute("type", atom->getType());
		switch(atom->getType())	{
			case LANG_NULL:
				break;
			case LANG_BANG:
				break;
			case LANG_INT:
				encodeLangAtomInt(atomNode, atom);
				break;
			case LANG_FLOAT:
				encodeLangAtomFloat(atomNode, atom);
				break;
			case LANG_STRING:
				encodeLangAtomString(atomNode, atom);
				break;
			case LANG_VAR:
				encodeLangAtomVar(atomNode, atom);
				break;
			case LANG_LIST:
				encodeLangAtomList(atomNode, atom);
				break;
		}
		parentNode->LinkEndChild(atomNode);
	}
	
	void Writer::encodeLangAtomInt(TiXmlElement *parentNode, Objsc::LangAtom* atom)
	{
		TiXmlElement *valueEle = new TiXmlElement("value");
		valueEle->LinkEndChild(new TiXmlText(intToString(parseInt(atom))));
		parentNode->LinkEndChild(valueEle);
	}
	
	void Writer::encodeLangAtomFloat(TiXmlElement *parentNode, Objsc::LangAtom* atom)
	{
		TiXmlElement *valueEle = new TiXmlElement("value");
		valueEle->LinkEndChild(new TiXmlText(floatToString(parseFloat(atom))));
		parentNode->LinkEndChild(valueEle);
	}
	
	void Writer::encodeLangAtomString(TiXmlElement *parentNode, Objsc::LangAtom* atom)
	{
		TiXmlElement *valueEle = new TiXmlElement("value");
		valueEle->LinkEndChild(new TiXmlText(parseString(atom)));
		parentNode->LinkEndChild(valueEle);
	}
	
	void Writer::encodeLangAtomVar(TiXmlElement *parentNode, Objsc::LangAtom* atom)
	{
		TiXmlElement *valueEle = new TiXmlElement("value");
		valueEle->LinkEndChild(new TiXmlText(varToString(dynamic_cast<Objsc::LangVar*>(atom))));
		parentNode->LinkEndChild(valueEle);
	}
	
	void Writer::encodeLangAtomList(TiXmlElement *parentNode, Objsc::LangAtom* atom)
	{
		Objsc::LangList* listAtom = dynamic_cast<Objsc::LangList*>(atom);
		TiXmlElement *valueEle = new TiXmlElement("value");
		for(LangAtomIter item = listAtom->begin(); item != listAtom->end(); ++item) {
			encodeLangAtom(valueEle, (*item));
		}
		parentNode->LinkEndChild(valueEle);
	}
	
	
	
	////////////////////////////////////////
	// READER
	////////////////////////////////////////
	void Reader::read(const string& filename)
	{
		TiXmlDocument doc = TiXmlDocument();
		if (doc.LoadFile(Objsc::ProjectFiles::Current()->makeScorePath(filename))) {
			//printf("decoding ...\n");
			TiXmlHandle hDoc(&doc);
			TiXmlElement* pElem;
			TiXmlHandle root(0);
			
			pElem=hDoc.FirstChildElement().Element();
			if (!pElem) {
				return;
			}
			root=TiXmlHandle(pElem);
			pElem=root.FirstChild("score").Element();
			if(pElem)	{
				score = Objsc::App::Current()->newScore();
				decodeScore(pElem, score);
				score->makeFirstMapCurrent();
				Objsc::GUIRoot::Current()->newScoreLoaded();
			}
		} else {
			printf("ERROR: unable to read file.\n");
		}
	}
	
	void Reader::decodeScore(TiXmlElement *parentNode, Objsc::ScorePtr score)
	{
		TiXmlElement *nameEle = parentNode->FirstChildElement("name");
		if(nameEle) {
			score->setName(string(nameEle->GetText()));
		}
		TiXmlElement *pElem = parentNode->FirstChild( "zonemaps" )->FirstChildElement("zonemap");
		if(pElem) {
			for( pElem; pElem; pElem=pElem->NextSiblingElement()) {
				TiXmlElement *nameEle = pElem->FirstChildElement("name");
				if(nameEle) {
					decodeZoneMap(pElem, score->createZoneMap(string(nameEle->GetText())));
				}
			}
		}
	}
	
	void Reader::decodeZoneMap(TiXmlElement *parentNode, Objsc::ZoneMapPtr zonemap)
	{
		TiXmlElement *zonesNode = parentNode->FirstChildElement( "zones" );
		if(zonesNode)	{
			int count = 0;
			zonesNode->QueryIntAttribute("count", &count);
			zonemap->setZonecount(count);
			TiXmlElement *pElem = parentNode->FirstChild( "zones" )->FirstChildElement("zone");
			if(pElem) {
				for( pElem; pElem; pElem=pElem->NextSiblingElement()) {
					TiXmlElement *typeEle = pElem->FirstChildElement("type");
					TiXmlElement *nameEle = pElem->FirstChildElement("name");
					if(typeEle && nameEle) {
						decodeZone(pElem, zonemap->createZone(string(typeEle->GetText()), string(nameEle->GetText())));
					}
				}
			}
		}
	}
	
	void Reader::decodeZone(TiXmlElement *parentNode, Objsc::ZonePtr zone)
	{
		TiXmlElement *rectElem = parentNode->FirstChild( "bounds" )->FirstChildElement("rect");
		if(rectElem)	{
			Objsc::Rect rect;
			decodeRect(rectElem, rect);
			zone->setBounds(rect.getX(), rect.getY(), rect.getWidth(), rect.getHeight());
		}
		TiXmlElement *patchElem = parentNode->FirstChildElement("patch");
		if(patchElem)	{
			Objsc::LangPatchPtr patch = Objsc::LangPatch::create();
			decodeLangPatch(patchElem, patch);
			zone->setPatch(patch);
		}
	}
	
	void Reader::decodeRect(TiXmlElement *parentNode, Objsc::Rect& rect)
	{
		float dX=0.0;
		float dY=0.0;
		float dW=0.0;
		float dH=0.0;
		parentNode->QueryFloatAttribute("x", &dX);
		parentNode->QueryFloatAttribute("y", &dY);
		parentNode->QueryFloatAttribute("width", &dW);
		parentNode->QueryFloatAttribute("height", &dH);
		rect.set(dX,dY,dW,dH);
	}
	
	void Reader::decodeColour(TiXmlElement *parentNode, Objsc::Colour& colour)
	{
		int r=0;
		int g=0;
		int b=0;
		int a=255;
		parentNode->QueryIntAttribute("r", &r);
		parentNode->QueryIntAttribute("g", &g);
		parentNode->QueryIntAttribute("b", &b);
		parentNode->QueryIntAttribute("a", &a);
		colour.r = r;
		colour.g = g;
		colour.b = b;
		colour.a = a;
	}
	
	void Reader::decodeLangPatch(TiXmlElement *parentNode, Objsc::LangPatchPtr patch)
	{
		// name
		TiXmlElement *nameEle = parentNode->FirstChildElement("name");
		if(nameEle) {
			patch->setName(string(nameEle->GetText()));
		}
		// objects
		Objsc::LangObjectDecodingMap objectIDs;
		TiXmlElement *objElem = parentNode->FirstChild( "objects" )->FirstChildElement("object");
		if(objElem) {
			for( objElem; objElem; objElem=objElem->NextSiblingElement()) {
				float dX=0.0;
				float dY=0.0;
				int encID = 0;
				objElem->QueryFloatAttribute("x", &dX);
				objElem->QueryFloatAttribute("y", &dY);
				objElem->QueryIntAttribute("encid", &encID);
				string langname(objElem->Attribute("langname"));
				//printf("decodeLangPatch %s\n", langname.c_str());
				Objsc::LangObjectPtr object = patch->createObject(langname, dX, dY, false);
				//printf("decodeLangPatch patch->createObject\n");
				objectIDs.insert(Objsc::LangObjectDecodingEntry(encID, object));
				//printf("decodeLangPatch objectIDs.insert\n");
				decodeLangObject(objElem, object);
				//printf("decodeLangPatch decodeLangObject DONE\n");
			}
		}
		// cords
		TiXmlElement *cordElem = parentNode->FirstChild( "cords" )->FirstChildElement("cordset");
		if(cordElem) {
			for( cordElem; cordElem; cordElem=cordElem->NextSiblingElement()) {
				int encID = -1;
				cordElem->QueryIntAttribute("encid", &encID);
				if(objectIDs.count(encID) > 0)	{
					decodeLangCord(cordElem, objectIDs[encID], objectIDs);
				}
			}
		}
		// receivers
		TiXmlElement *recElem = parentNode->FirstChild( "receivers" )->FirstChildElement("receiver");
		if(recElem) {
			for( recElem; recElem; recElem=recElem->NextSiblingElement()) {
				decodeLangReceiver(recElem, patch, objectIDs);
			}
		}
	}
	
	void Reader::decodeLangObject(TiXmlElement *parentNode, Objsc::LangObjectPtr object)
	{
		//printf("decodeLangObject A\n");
		Objsc::LangObjectArchiverPtr archiver = Objsc::LangObjectArchiverFactory::Current()->create(object->getLangName());
		if(archiver)	{
			//printf("decodeLangObject has archiver\n");
			archiver->decode(parentNode, object);
		}
		//printf("decodeLangObject DONE\n");
	}
	
	void Reader::decodeLangCord(TiXmlElement *parentNode, Objsc::LangObjectPtr object, Objsc::LangObjectDecodingMap& objectIDs)
	{
		// outlets
		TiXmlElement *outletElem = parentNode->FirstChild( "outlets" )->FirstChildElement("outlet");
		if(outletElem) {
			for( outletElem; outletElem; outletElem=outletElem->NextSiblingElement()) {
				int outlet = -1;
				outletElem->QueryIntAttribute("index", &outlet);
				if(outlet > -1)	{
					TiXmlElement *addrElem = outletElem->FirstChild( "addresses" )->FirstChildElement("address");
					if(addrElem) {
						for( addrElem; addrElem; addrElem=addrElem->NextSiblingElement()) {
							int encID = -1;
							int inlet = -1;
							addrElem->QueryIntAttribute("encid", &encID);
							addrElem->QueryIntAttribute("inlet", &inlet);
							if(encID > -1 && inlet > -1)	{
								if(objectIDs.count(encID) > 0)	{
									object->link(outlet, objectIDs[encID], inlet);
								}
							}
						}
					}
				}
			}
		}
	}
	
	void Reader::decodeLangReceiver(TiXmlElement *parentNode, Objsc::LangPatchPtr patch, Objsc::LangObjectDecodingMap& objectIDs)
	{
		// name
		string recname = "";
		TiXmlElement *nameEle = parentNode->FirstChildElement("name");
		if(nameEle) {
			recname.assign(nameEle->GetText());
		}
		if(recname.empty())	{
			return;
		}
		// objects
		TiXmlElement *objElem = parentNode->FirstChild( "objects" )->FirstChildElement("object");
		if(objElem) {
			for( objElem; objElem; objElem=objElem->NextSiblingElement()) {
				int encID = -1;
				objElem->QueryIntAttribute("encid", &encID);
				if(objectIDs.count(encID) > 0)	{
					//printf("Reader::decodeLangReceiver\n");
					patch->addObjectToReceiver(recname, objectIDs[encID]);
				}
			}
		}
	}
	
	void Reader::decodeLangAtom(TiXmlElement *parentNode, Objsc::LangAtom* atom)
	{
		
	}
	
	void Reader::decodeLangAtomInt(TiXmlElement *parentNode, Objsc::LangAtom* atom)
	{
		
	}
	
	void Reader::decodeLangAtomFloat(TiXmlElement *parentNode, Objsc::LangAtom* atom)
	{
		
	}
	
	void Reader::decodeLangAtomString(TiXmlElement *parentNode, Objsc::LangAtom* atom)
	{
		
	}
	
	void Reader::decodeLangAtomVar(TiXmlElement *parentNode, Objsc::LangAtom* atom)
	{
		
	}
	
	void Reader::decodeLangAtomList(TiXmlElement *parentNode, Objsc::LangAtom* atom)
	{
		
	}
	
	
	////////////////////////////////////////
	// ARCHIVIST
	////////////////////////////////////////
	Archivist::Archivist() : 
		scorefiles(),
		writer(),
		reader(),
		dialogue(),
		mode(OPEN_MODE)
	{
		scorefiles.includeFileType("xml");
	}
	
	void Archivist::initDialogue()
	{
		dialogue = Objsc::Dialogue::create("_archivedia_");
		dialogue->sizeEditItems(false);
		dialogue->setActionLabel("open");
		dialogue->setFinder(Objsc::FinderPtr(Objsc::Archivist::Current()));
	}
	
	void Archivist::openFileDialogue()
	{
		if(!dialogue)	{
			initDialogue();
		}
		mode=OPEN_MODE;
		dialogue->setActionLabel("open");
		dialogue->setActionType("open_selection");
		dialogue->setText("");
		Objsc::GUIRoot::Current()->openDialogue(dialogue);
	}
	
	void Archivist::saveFileDialogue(const string& filename)
	{
		if(!dialogue)	{
			initDialogue();
		}
		mode=SAVE_MODE;
		dialogue->setActionLabel("save");
		dialogue->setActionType("handle_input");
		dialogue->setText(filename);
		Objsc::GUIRoot::Current()->openDialogue(dialogue);
	}
	
	void Archivist::save()
	{
		Objsc::ScorePtr score = Objsc::App::Current()->getScore();
		if(score->getName().empty())	{
			saveFileDialogue();
			return;
		}
		writer.write(score);
	}
	
	void Archivist::saveAs(const string& filename)
	{
		if(filename.empty())
			return;
		Objsc::ScorePtr score = Objsc::App::Current()->getScore();
		score->setName(filename);
		save();
	}
	
	void Archivist::open(const string& filename)
	{
		//printf("Archivist::open <%s>\n", filename.c_str());
		if(filename.empty())
			return;
		reader.read(filename);
	}
	
	// commands
	void Archivist::doCommand(Objsc::CommandPtr& cmd)
	{
		//std::cout << "Archivist::doCommand: " << cmd->getName() << std::endl;
		string cmdname = cmd->getName();
		if(cmdname ==  "new_zone")	{
			
		}
	}
	
	void Archivist::undoCommand(Objsc::CommandPtr& cmd)
	{
		std::cout << "Archivist::undoCommand: " << cmd->getName() << std::endl;
	}
	
	// fimder
	void Archivist::selectItem(const string& itemname)
	{
		//printf("Archivist::selectItem <%s>\n", itemname.c_str());
		switch(mode)	{
			case SAVE_MODE:
				saveAs(itemname);
				break;
			case OPEN_MODE:
				open(itemname);
				break;
		}
	}
	
	void Archivist::handleInput(const string& input)
	{
		//printf("Archivist::handleInput <%s>\n", input.c_str());
		switch(mode)	{
			case SAVE_MODE:
				printf("saving ...\n");
				saveAs(input);
				break;
			case OPEN_MODE:
				open(input);
				break;
		}
	}

	
	const Objsc::ItemNames& Archivist::getItemNames()
	{
		scorefiles.open(Objsc::ProjectFiles::Current()->makeScorePath());
		return scorefiles.getFilenames();
	}
	
}