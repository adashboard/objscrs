#include "objscrLangCommsObjects.hpp"

// project
#include "objscrApp.hpp"



namespace Objsc
{
	
	////////////////////////////////////////
	// DECLARATIONS
	////////////////////////////////////////
	const string Objsc::OSCInObject::langname = "osc_in";
	const string Objsc::OSCOutObject::langname = "osc_out";
	const string Objsc::SendObject::langname = "send";
	const string Objsc::ReceiveObject::langname = "receive";
	const string Objsc::ControlKeyObject::langname = "ctrlkey";
	
	/////////////////////////////////////
	// FUNCTIONS
	/////////////////////////////////////
	void initLangCommsObjects()
	{
		Objsc::LangObjectFactory* factory = Objsc::LangObjectFactory::Current();
		factory->addCreator(Objsc::OSCOutObject::langname, new Objsc::OSCOutObjectCreator());
		factory->addCreator(Objsc::OSCInObject::langname, new Objsc::OSCInObjectCreator());
		factory->addCreator(Objsc::SendObject::langname, new Objsc::SendObjectCreator());
		factory->addCreator(Objsc::ReceiveObject::langname, new Objsc::ReceiveObjectCreator());
		factory->addCreator(Objsc::ControlKeyObject::langname, new Objsc::ControlKeyObjectCreator());
	}
	
	
	/////////////////////////////////////
	// OSC OUT OBJECT
	/////////////////////////////////////
	bool OSCOutObject::parseArgs(Objsc::LangList* args)
	{
		if(args->size() < min_args)	{
			return false;
		} else {
			setLabel(getItemAsString(args, 0));
			return true;
		}
	}
	
	void OSCOutObject::setLabel(string nlabel) 
	{ 
		label.assign(nlabel);
		oscgroup = Objsc::OSCComms::Current()->createGroup(nlabel);
		displaystr = Objsc::OSCOutObject::langname + " " + label;
	}
	
	void OSCOutObject::receive(int inlet, Objsc::LangAtom* atom)
	{
		ofxOscMessage msg;
		switch(atom->getType())	{
			case LANG_NULL:
				break;
			case LANG_BANG:
				msg.setAddress( "/" +  oscgroup->getName());
				msg.addIntArg(1);
				oscgroup->send(msg);
				break;
			case LANG_INT:
				msg.setAddress( "/" +  oscgroup->getName());
				msg.addIntArg(parseInt(atom));
				oscgroup->send(msg);
				break;
			case LANG_FLOAT:
				msg.setAddress( "/" +  oscgroup->getName());
				msg.addFloatArg(parseFloat(atom));
				oscgroup->send(msg);
				break;
			case LANG_STRING:
				msg.setAddress( "/" +  oscgroup->getName());
				msg.addStringArg(parseString(atom));
				oscgroup->send(msg);
				break;
			case LANG_VAR:
				msg.setAddress( "/" +  oscgroup->getName());
				varToMessage(dynamic_cast<Objsc::LangVar*>(atom),&msg);
				oscgroup->send(msg);
				break;
			case LANG_LIST:
				msg.setAddress( "/" +  oscgroup->getName());
				listToMessage(dynamic_cast<Objsc::LangList*>(atom),&msg);
				oscgroup->send(msg);
				break;
		}
	}
	
	void OSCOutObject::listToMessage(LangList* value, ofxOscMessage* msgPtr) 
	{	
		for(LangAtomIter atom = value->begin(); atom != value->end(); ++atom) {
			itemToMessage((*atom), msgPtr);
		}
	}
	
	void OSCOutObject::itemToMessage(LangAtom* atom, ofxOscMessage* msgPtr)
	{
		switch(atom->getType())	{
			case LANG_NULL:
				break;
			case LANG_BANG:
				msgPtr->addIntArg(1);
				break;
			case LANG_INT:
				msgPtr->addIntArg(parseInt(atom));
				break;
			case LANG_FLOAT:
				msgPtr->addFloatArg(parseFloat(atom));
				break;
			case LANG_STRING:
				msgPtr->addStringArg(parseString(atom));
				break;
			case LANG_LIST:
				listToMessage(dynamic_cast<Objsc::LangList*>(atom),msgPtr);
				break;
			case LANG_VAR:
				varToMessage(dynamic_cast<Objsc::LangVar*>(atom), msgPtr);
				break;
		}
	}
	
	void OSCOutObject::varToMessage(LangVar* atom, ofxOscMessage* msgPtr)
	{
		switch(atom->getValueType())	{
			case LANG_NULL:
				break;
			case LANG_BANG:
				msgPtr->addIntArg(1);
				break;
			case LANG_INT:
				msgPtr->addIntArg(getVarInt(atom));
				break;
			case LANG_FLOAT:
				msgPtr->addFloatArg(getVarFloat(atom));
				break;
			case LANG_STRING:
				msgPtr->addStringArg(getVarString(atom));
				break;
			case LANG_LIST:
				listToMessage(dynamic_cast<Objsc::LangList*>(atom),msgPtr);
				break;
			case LANG_VAR:
				// ignore
				break;
		}
	}
	
	void OSCOutObject::activate()
	{
		
	}
	
	
	/////////////////////////////////////
	// OSC IN OBJECT
	/////////////////////////////////////
	//~ bool OSCInObject::parseArgs(Objsc::LangList* args)
	//~ {
		//~ if(args->size() < min_args)	{
			//~ return false;
		//~ } else {
			//~ oscgroup = Objsc::OSCComms::Current()->startListen(getItemAsString(args, 0), getItemAsInt(args, 1));
			//~ displaystr = Objsc::OSCOutObject::langname + " " + getItemAsString(args, 0) + " " + getItemAsInt(args, 1);
			//~ return true;
		//~ }
	//~ }
	
	bool OSCInObject::parseArgs(Objsc::LangList* args)
	{
		if(args->size() < min_args)	{
			return false;
		} else {
			//setLabel(getItemAsString(args, 0));
			return true;
		}
	}
	
	/*
	void OSCInObject::setLabel(string nlabel) 
	{ 
		label.assign(nlabel);
		displaystr = getLangName() + " " + label;
	}
	*/
	
	void OSCInObject::receive(int inlet, Objsc::LangAtom* atom)
	{
		send(0, atom);
	}
	
	
	/////////////////////////////////////
	// SEND OBJECT
	/////////////////////////////////////
	SendObject::SendObject() : label(), recname()
	{
		setInlets(1);
		setOutlets(0);
		min_args=1;
	}
	
	SendObject::~SendObject()
	{
		
	}
	
	bool SendObject::parseArgs(Objsc::LangList* args)
	{
		if(args->size() < min_args)	{
			return false;
		} else {
			setLabel(getItemAsString(args, 0));
			return true;
		}
	}
	
	void SendObject::setLabel(string nlabel) 
	{ 
		label.assign(nlabel);
		recname.assign(makeReceiverName(label));
		displaystr = getLangName() + " " + label;
	}
	
	void SendObject::receive(int inlet, Objsc::LangAtom* atom)
	{
		Objsc::App::Current()->getScore()->sendToCurrentZoneMap(recname, Objsc::LangAtomPtr(cloneAtom(atom)));
	}
	
	
	/////////////////////////////////////
	// RECEIVE OBJECT
	/////////////////////////////////////
	ReceiveObject::ReceiveObject() : label()
	{
		setInlets(0);
		setOutlets(1);
		min_args=1;
	}
	
	ReceiveObject::~ReceiveObject()
	{
		
	}
	
	bool ReceiveObject::parseArgs(Objsc::LangList* args)
	{
		if(args->size() < min_args)	{
			return false;
		} else {
			setLabel(getItemAsString(args, 0));
			return true;
		}
	}
	
	void ReceiveObject::setLabel(string nlabel) 
	{ 
		label.assign(nlabel);
		displaystr = getLangName() + " " + label;
	}
	
	void ReceiveObject::receive(int inlet, Objsc::LangAtom* atom)
	{
		send(0, atom);
	}
	
	/////////////////////////////////////
	// RECEIVE OBJECT
	/////////////////////////////////////
	ControlKeyObject::ControlKeyObject() : label()
	{
		setInlets(0);
		setOutlets(3);
		min_args=0;
	}
	
	ControlKeyObject::~ControlKeyObject()
	{
		
	}
	
	bool ControlKeyObject::parseArgs(Objsc::LangList* args)
	{
		if(args->size() < min_args)	{
			return false;
		} else {
			setLabel(getItemAsString(args, 0));
			return true;
		}
	}
	
	void ControlKeyObject::setLabel(string nlabel) 
	{ 
		label.assign(nlabel);
		displaystr = getLangName() + " " + label;
	}
	
	void ControlKeyObject::receive(int inlet, Objsc::LangAtom* atom)
	{
		send(1, atom);
		if(label == parseString(atom))	{
			send(0, new Objsc::LangBang());
		} else {
			send(2, new Objsc::LangBang());
		}
	}
	
}

