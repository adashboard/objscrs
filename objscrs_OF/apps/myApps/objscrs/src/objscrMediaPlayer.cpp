#include "objscrMediaPlayer.hpp"

// objscrs
#include "objscrGUIRoot.hpp"

// openframeworks
#include "ofMain.h"

namespace Objsc
{
	
	
	////////////////////////////////////////
	// MEDIA PLAYER
	////////////////////////////////////////
	MediaPlayer::MediaPlayer() : medialayers(), medialabels()
	{
		
	}
	
	MediaPlayer::~MediaPlayer() 
	{
		medialayers.clear();
		medialabels.clear();
	}
	
	void MediaPlayer::update()
	{
		for(Objsc::MediaViewLayerMap::iterator layer = medialayers.begin(); layer != medialayers.end(); ++layer)	{
			//printf("MediaPlayer::update A\n");
			layer->second->update();
			//printf("MediaPlayer::update B\n");
		}
	}
	
	void MediaPlayer::draw()
	{
		for(Objsc::MediaViewLayerMap::iterator layer = medialayers.begin(); layer != medialayers.end(); ++layer)	{
			layer->second->draw();
		}
	}
	
	// media
	Objsc::MediaViewPtr MediaPlayer::loadImage(const string& filename, const string& label)
	{
		if(!filename.empty())	{
			Objsc::MediaViewPtr media = Objsc::ImageView::create(filename);
			if(label.empty())	{
				if(medialabels.count(filename) > 0)	{
					medialabels[filename] = media;
				} else {
					medialabels.insert(MediaViewLabelEntry(filename, media));
				}
			} else {
				if(medialabels.count(label) > 0)	{
					medialabels[label] = media;
				} else {
					medialabels.insert(MediaViewLabelEntry(label, media));
				}
			}
			return media;
		}
		return Objsc::MediaViewPtr();
	}
	
	Objsc::MediaViewPtr MediaPlayer::loadVideo(const string& filename, const string& label)
	{
		if(!filename.empty())	{
			Objsc::MediaViewPtr media = Objsc::VideoView::create(filename);
			if(label.empty())	{
				if(medialabels.count(filename) > 0)	{
					medialabels[filename] = media;
				} else {
					medialabels.insert(MediaViewLabelEntry(filename, media));
				}
			} else {
				if(medialabels.count(label) > 0)	{
					medialabels[label] = media;
				} else {
					medialabels.insert(MediaViewLabelEntry(label, media));
				}
			}
			return media;
		}
		return Objsc::MediaViewPtr();
	}
	
	
	Objsc::MediaViewPtr MediaPlayer::createText(const string& label)
	{
		//printf("MediaPlayer::createText %s\n", label.c_str());
		Objsc::MediaViewPtr media = Objsc::TextView::create();
		//printf("MediaPlayer::createText A\n");
		if(medialabels.count(label) > 0)	{
			//printf("MediaPlayer::createText B1\n");
			medialabels[label] = media;
		} else {
			//printf("MediaPlayer::createText B2\n");
			medialabels.insert(MediaViewLabelEntry(label, media));
		}
		//printf("MediaPlayer::createText DONE\n");
		return media;
	}
	
	
	
	const bool MediaPlayer::mediaLoaded(const string& label) const
	{
		return (medialabels.count(label) > 0);
	}
	
	Objsc::MediaViewPtr MediaPlayer::getMedia(const string& label)
	{
		if(medialabels.count(label) > 0)	{
			return medialabels[label];
		}
		return Objsc::MediaViewPtr();
	}
	
	
	void MediaPlayer::remove(const string& label)
	{
		if(medialabels.count(label) > 0)	{
			if(medialayers.count(medialabels[label]->getLayer()) > 0)	{
				if(medialayers[medialabels[label]->getLayer()] == medialabels[label])	{
					medialayers.erase(medialabels[label]->getLayer());
				}
			}
			medialabels.erase(label);
		}
		//printf("MediaPlayer::remove %d\n", medialabels.count(label));
	}
	
	void MediaPlayer::clearAll()
	{
		for(Objsc::MediaViewLabelMap::iterator media = medialabels.begin(); media != medialabels.end(); ++media)	{
			media->second->hide();
			media->second->stop();
		}
		medialayers.clear();
		medialabels.clear();
	}
	
	// display
	void MediaPlayer::setLayer(const string& label, int layer)
	{
		if(medialabels.count(label) > 0)	{
			medialabels[label]->setLayer(layer);
		}
	}
	
	void MediaPlayer::show(const string& label, int layer)
	{
		//printf("MediaPlayer::show %d\n", medialabels.count(label));
		if(medialabels.count(label) > 0)	{
			if(layer != 0)	{
				medialabels[label]->setLayer(layer);
			}
			if(medialayers.count(medialabels[label]->getLayer()) > 0)	{
				medialayers[medialabels[label]->getLayer()] = medialabels[label];
			} else {
				medialayers.insert(MediaViewLayerEntry(medialabels[label]->getLayer(), medialabels[label]));
			}
			medialabels[label]->show();
		}
	}
	
	void MediaPlayer::hide(const string& label)
	{
		if(medialabels.count(label) > 0)	{
			medialabels[label]->hide();
		}
	}
}

