#ifndef  OBJSCR_STAGE_H
#define OBJSCR_STAGE_H

// objscrs
#include "singleton.h"
#include "objscrGeom.hpp"
#include "objscrBody.hpp"
#include "objscrFrames.hpp"
#include "objscrShapeFilter.hpp"
#include "objscrBodyFilter.hpp"
#include "objscrZoneMap.hpp"

// openframeworks
#include "ofxOpenCv.h"

// shared_ptr
#include <tr1/memory>

#include <string>
#include <list>
#include <map>
#include <vector>
#include <exception>
using std::string;
using std::list;
using std::map;
using std::vector;
using std::exception;

//#include <math.h>

// DEFINES
#ifndef MAX
	#define MAX(x,y) (((x) > (y)) ? (x) : (y))
#endif
#ifndef MIN
	#define MIN(x,y) (((x) < (y)) ? (x) : (y))
#endif





namespace Objsc
{
	
	typedef vector < unsigned int > StageNumVec;
	
	
	
	class Stage : public Singleton<Stage>
	{
		private:
			Objsc::Rect				screenRect;
			Objsc::Rect				stageRect;
			Objsc::ShapeFilterPtr		shapeFilter;
			Objsc::BodyFilterPtr			bodyFilter;
			Objsc::ZoneMapSet			zonemaps;
			Objsc::StageNumVec		stageNumbers;
			bool					active;
			bool					preview;
			
		public:
			Stage();
			~Stage() {}
				
			void update(int time, int timeframe);
			void draw();
				
			void setScreenCoords(int x, int y, int width, int height) {screenRect.set(x,y,width,height);}
			void screenToStage(float x, float y, float* dx, float* dy)
			{
				*dx = (x-screenRect.getX())/screenRect.getWidth();
				*dy = (y-screenRect.getY())/screenRect.getHeight();
			}
			void stageToScreen(float x, float y, int* dx, int* dy)
			{
				*dx = MAX(0,toInt(screenRect.getX()+(screenRect.getWidth()*x)));
				*dy = MAX(0,toInt(screenRect.getY()+(screenRect.getHeight()*y)));
			}
			bool withinScreen(float x, float y) {return screenRect.containsPoint(x,y);}
			bool withinBounds(float x, float y) {return stageRect.containsPoint(x,y);}
			
			// frames
			void setFrameSource(Objsc::FrameSourcePtr sourcePtr);
			void start();
			void stop();
			bool isActive() const { return active; }
			void previewOn() { preview=true; }
			void previewOff() { preview=false; }
			
			// zones
			void addZoneMap(Objsc::ZoneMapPtr zonemap);
			void setZoneMap(Objsc::ZoneMapPtr zonemap);
			void removeZoneMap(Objsc::ZoneMapPtr zonemap);
			void clearZoneMaps();
			void doLoadActions();
			
			// bodies
			void updateBodies();
			
			static std::tr1::shared_ptr<Objsc::Stage> create()
			{
				return std::tr1::shared_ptr<Objsc::Stage>(new Stage());
			}
			
			static const string cmdlabel;
			
			// shapes
			void setShapesMinRadius(int value) { shapeFilter->setMinRadius(value); }
			void setShapesMaxRadius(int value) { shapeFilter->setMaxRadius(value); }
			void setShapesThreshold(int value) { shapeFilter->setThreshold(value); }
			
			// stage numbers
			unsigned int requestStageNumber();
			void releaseStageNumber(unsigned int value);
	};
	
	typedef std::tr1::shared_ptr<Objsc::Stage> StagePtr;
	typedef map< string, Objsc::StagePtr > StageMap;
	typedef StageMap::value_type StageEntry;
}

#endif // OBJSCR_STAGE_H
