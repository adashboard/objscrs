#include "objscrLangViews.hpp"


// objscrs
#include "objscrGUIStyles.hpp"
#include "objscrGUIRoot.hpp"
#include "objscrLangEditor.hpp"

// STL
#include <algorithm>
#include <functional>

#include <math.h>

namespace Objsc
{
	
	////////////////////////////////////////
	// DECLARATIONS
	////////////////////////////////////////
	const string Objsc::LangViewBase::stylename = "langobject";
	const string Objsc::LangPatchCanvas::stylename = "langpatch";
	const string Objsc::LangCordView::stylename = "langcord";
	
	////////////////////////////////////////
	// FUNCTIONS
	////////////////////////////////////////
	struct cordConnects : std::binary_function< Objsc::LangCordViewPtr, Objsc::LangViewPtr, bool >	{
		bool operator() (const Objsc::LangCordViewPtr& ptr, const Objsc::LangViewPtr& objview) const	{
			return ptr->connectsWith(objview);
		}
	};
	
	////////////////////////////////////////
	// SOCKET VIEW
	////////////////////////////////////////
	SocketView::SocketView(socket_type ntype) : type(ntype)
	{
		Objsc::StylePalette::Current()->applyStyle(Objsc::LangViewBase::stylename, this);
		setBounds(0,0, Objsc::LANGVIEW_SOCKETWIDTH, Objsc::LANGVIEW_SOCKETHEIGHT);
	}
	
	void SocketView::setAction(Objsc::LangViewWeakPtr view, int socketnum)
	{
		switch(type)	{
			case Objsc::LANG_INLET:
				action = Objsc::CommandDispatcher::createCommand("_langeditor_", "end_cord");
				break;
			case Objsc::LANG_OUTLET:
				action = Objsc::CommandDispatcher::createCommand("_langeditor_", "start_cord");
				break;
		}
		if(action)	{
			Objsc::PacketPtr data = action->getData();
			data->addArg(socketnum);
			data->addArg(view);
		}
	}
	
	void SocketView::draw()
	{
		if (visible)	{
			ofSetColor(fgColour.r, fgColour.g, fgColour.b);
			if(hasFocus)	{
				ofRect(bounds.getX(), bounds.getY(), bounds.getWidth(), bounds.getHeight());
			} else {
				outlineBox(bounds);
			}
		}
	}
	
	void SocketView::mousePressed(int x, int y, int button)
	{
		consumed = false;
		if(withinBounds(x,y))	{
			consumed = true;
			Objsc::CommandDispatcher::Current()->doCommand(action);
		}
	}
	
	
	////////////////////////////////////////
	// LANG VIEW
	////////////////////////////////////////
	void LangView::mapToStage()
	{
		if(object)	{
			object->setLocation((bounds.getX()-canvasrect.getX())/canvasrect.getWidth(), (bounds.getY()-canvasrect.getY())/canvasrect.getHeight());
		}
	}
	
	void LangView::mapToScreen()
	{
		if(object)	{
			_setLocation(canvasrect.getX()+(object->getX()*canvasrect.getWidth()), canvasrect.getY()+(object->getY()*canvasrect.getHeight()));
		}
	}
	
	void LangView::_setLocation(float x, float y)
	{
		bounds.setLocation(x,y);
		inrect.setLocation(bounds.getX()+Objsc::LANGVIEW_MARGIN, bounds.getY()+Objsc::LANGVIEW_MARGIN);
	}
	
	void LangView::_setBounds(float x, float y, float width, float height)
	{
		bounds.set(x,y,width,height);
		inrect.set(bounds.getX()+Objsc::LANGVIEW_MARGIN, bounds.getY()+Objsc::LANGVIEW_MARGIN, bounds.getWidth()-Objsc::LANGVIEW_MARGIN*2, bounds.getHeight()-Objsc::LANGVIEW_MARGIN*2);
	}
	
	void LangView::link(int outlet, std::tr1::shared_ptr<Objsc::LangView> toView, int inlet)
	{
		if(object && toView->hasObject())	{
			object->link(outlet, toView->getObject(), inlet);
		}
	}
	
	void LangView::setLocation(float x, float y)
	{
		_setLocation(x,y);
		mapToStage();
	}
	
	void LangView::setBounds(float x, float y, float width, float height)
	{
		_setBounds(x,y,width,height);
		mapToStage();
	}
	
	void LangView::openMenu(int x, int y)
	{
		if(!editmenu)	{
			editmenu = Objsc::ContextMenu::create();
			Objsc::CommandPtr actionPatch = Objsc::CommandDispatcher::createCommand(Objsc::LangPatchEditor::cmdlabel  , "lobj_edit");
			actionPatch->getData()->addArg(Objsc::LangViewPtr(this));
			editmenu->addItem("edit", actionPatch);
			Objsc::CommandPtr actionDelete = Objsc::CommandDispatcher::createCommand(Objsc::LangPatchEditor::cmdlabel  , "lobj_delete");
			actionDelete->getData()->addArg(Objsc::LangViewPtr(this));
			editmenu->addItem("delete", actionDelete);
		}
		if(hasFocus)
			looseFocus();
		Objsc::GUIRoot::Current()->openRootMenu(editmenu, x, y);
	}
	
	////////////////////////////////////////
	// OBJECT VIEW
	////////////////////////////////////////
	LangObjectView::LangObjectView() : dodrag(false), scrX(0), scrY(0), actionParse(), inlets(), outlets()
	{
		Objsc::StylePalette::Current()->applyStyle(Objsc::LangViewBase::stylename, this);
		Objsc::StylePalette::Current()->applyStyle(Objsc::LangViewBase::stylename, &text);
		setBounds(0,0, Objsc::LANGVIEW_DFTWIDTH, Objsc::LANGVIEW_DFTHEIGHT);
		text.sizeWithText(true);
		text.setTextColour(255,0,0);
		actionParse = Objsc::CommandDispatcher::createCommand(Objsc::LangPatchEditor::cmdlabel  , "lobj_parse");
	}
	
	LangObjectView::~LangObjectView()
	{
		inlets.clear();
		outlets.clear();
	}
	
	
	void LangObjectView::setObject(Objsc::LangObjectPtr objectPtr)
	{
		object=objectPtr;
		updateText();
	}
	
	void LangObjectView::reset()
	{
		bounds.setWidth(Objsc::LANGVIEW_DFTWIDTH);
		text.clearText();
		text.setTextColour(255,0,0);
		text.editOn();
	}
	
	void LangObjectView::setText(const string& str) 
	{ 
		text.setText(str);
		fitToText();
	}
	
	void LangObjectView::updateText()
	{
		reset();
		if(object)	{
			text.setText(object->getDisplayString());
			fitToText();
			text.setTextColour(0,0,0);
			text.editOff();
		}
	}
	
	void LangObjectView::fitToText()
	{
		if(text.getWidth()+Objsc::LANGVIEW_RIGHTMARGIN > bounds.getWidth())	{
			bounds.setWidth(text.getWidth()+Objsc::LANGVIEW_RIGHTMARGIN);
			placeSockets();
		} else if(text.getWidth()+Objsc::LANGVIEW_RIGHTMARGIN < Objsc::LANGVIEW_DFTWIDTH) {
			bounds.setWidth(Objsc::LANGVIEW_DFTWIDTH);
			placeSockets();
		}
	}
	
	void LangObjectView::initSockets(std::tr1::weak_ptr<Objsc::LangView> weakPtr)
	{
		if(object)	{
			for(int i = 0; i < object->getNumInlets(); i++)	{
				inlets.push_back(Objsc::SocketView::createInlet());
				inlets.back()->setAction(weakPtr, i);
			}
			for(int i = 0; i < object->getNumOutlets(); i++)	{
				outlets.push_back(Objsc::SocketView::createOutlet());
				outlets.back()->setAction(weakPtr, i);
			}
			placeSockets();
		}
	}
	
	void LangObjectView::placeSockets()
	{
		float inwidth = 0.0f;
		float outwidth = 0.0f;
		float sktwidth = 0.0f;
		
		if(!inlets.empty())	{
			inwidth = (inlets.size()*Objsc::LANGVIEW_SOCKETWIDTH) + ((inlets.size()-1)*2.0f);
		}
		if(!outlets.empty())	{
			outwidth = (outlets.size()*Objsc::LANGVIEW_SOCKETWIDTH) + ((outlets.size()-1)*2.0f);
		}
		sktwidth = outwidth>inwidth?outwidth:inwidth;
		if(sktwidth > bounds.getWidth())	{
			bounds.setWidth(sktwidth);
		} 
		
		float inspace = 0.0f;
		float outspace = 0.0f;
		if(inlets.size() == 1)	{
			inlets.back()->setLocation(bounds.getX(),bounds.getY());
		} else if(inlets.size() > 1)	{
			float inspace = (bounds.getWidth()-(inlets.size()*Objsc::LANGVIEW_SOCKETWIDTH))/(inlets.size()-1);
			int i = 0;
			for(Objsc::SocketViewVector::iterator skt = inlets.begin(); skt != inlets.end(); ++skt)	{
				(*skt)->setLocation(bounds.getX()+(i*(Objsc::LANGVIEW_SOCKETWIDTH+inspace)),bounds.getY());
				++i;
			}
		}
		if(outlets.size() == 1)	{
			outlets.back()->setLocation(bounds.getX(),bounds.getBottom()-Objsc::LANGVIEW_SOCKETHEIGHT);
		} else if(outlets.size() > 1)	{
			float inspace = (bounds.getWidth()-(outlets.size()*Objsc::LANGVIEW_SOCKETWIDTH))/(outlets.size()-1);
			int i = 0;
			for(Objsc::SocketViewVector::iterator skt = outlets.begin(); skt != outlets.end(); ++skt)	{
				(*skt)->setLocation(bounds.getX()+(i*(Objsc::LANGVIEW_SOCKETWIDTH+inspace)),bounds.getBottom()-Objsc::LANGVIEW_SOCKETHEIGHT);
				++i;
			}
		}

	}
	
	void LangObjectView::initActions(std::tr1::weak_ptr<Objsc::LangView> weakPtr)
	{
		actionParse->getData()->addArg(weakPtr);
	}
	
	void LangObjectView::update()
	{
		
	}
	
	void LangObjectView::draw()
	{
		if (visible)	{
			ofSetColor(bgColour.r, bgColour.g, bgColour.b, bgColour.a);
			ofRect(bounds.getX(), bounds.getY(), bounds.getWidth(), bounds.getHeight());
			text.draw();
			ofSetColor(fgColour.r, fgColour.g, fgColour.b, fgColour.a);
			outlineBox(bounds);
			for(SocketViewVector::iterator in = inlets.begin(); in != inlets.end(); ++in)	{
				(*in)->draw();
			}
			for(SocketViewVector::iterator out = outlets.begin(); out != outlets.end(); ++out)	{
				(*out)->draw();
			}
		}
	}
	
	void LangObjectView::_setLocation(float x, float y)
	{
		bounds.setLocation(x,y);
		inrect.setLocation(bounds.getX()+Objsc::LANGVIEW_MARGIN, bounds.getY()+Objsc::LANGVIEW_MARGIN);
		text.setLocation(x,y);
		placeSockets();
	}
	
	void LangObjectView::_setBounds(float x, float y, float width, float height)
	{
		bounds.set(x,y,width,height);
		inrect.set(bounds.getX()+Objsc::LANGVIEW_MARGIN, bounds.getY()+Objsc::LANGVIEW_MARGIN, bounds.getWidth()-Objsc::LANGVIEW_MARGIN*2, bounds.getHeight()-Objsc::LANGVIEW_MARGIN*2);
		text.setBounds(x,y,width,height);
		placeSockets();
	}
	
	
	void LangObjectView::keyPressed(int key)
	{
		consumed = false;
		if(hasFocus) 	{
			if(key == OF_KEY_RETURN)	{
				actionParse->getData()->setKwArg("str", text.getText());
				Objsc::CommandDispatcher::Current()->doCommand(actionParse);
			} else {
				text.keyPressed(key);
				fitToText();
			}
			consumed = true;
		}
	}
	
	void LangObjectView::keyReleased(int key)
	{
		consumed = false;
		if(hasFocus) 	{
			text.keyReleased(key);
		}
	}
	
	void LangObjectView::mouseMoved(int x, int y )
	{
		if(withinBounds(x,y) && visible)	{
			if(!hasFocus && displayState!=Objsc::DISABLED) {
				gainFocus();
			}
			text.mouseMoved(x,y);
		} else {
			if(hasFocus && displayState!=Objsc::DISABLED) {
				looseFocus();
				if(dodrag)
					dodrag=false;
			}
		}
		for(SocketViewVector::iterator in = inlets.begin(); in != inlets.end(); ++in)	{
			(*in)->mouseMoved(x,y);
		}
		for(SocketViewVector::iterator out = outlets.begin(); out != outlets.end(); ++out)	{
			(*out)->mouseMoved(x,y);
		}
	}
	
	void LangObjectView::mouseDragged(int x, int y, int button)
	{
		consumed = false;
		float dX = x-scrX;
		float dY = y-scrY;
		if(dodrag)	{
			bounds.moveCentre(dX,dY);
			inrect.moveCentre(dX,dY);
			text.setLocation(bounds.getX(), bounds.getY());
			for(SocketViewVector::iterator in = inlets.begin(); in != inlets.end(); ++in)	{
				(*in)->moveCentre(dX,dY);
			}
			for(SocketViewVector::iterator out = outlets.begin(); out != outlets.end(); ++out)	{
				(*out)->moveCentre(dX,dY);
			}
			mapToStage();
			consumed = true;
		}
		scrX = x;
		scrY = y;
	}
	
	void LangObjectView::mousePressed(int x, int y, int button)
	{
		consumed = false;
		if(withinBounds(x,y))	{
			scrX = x;
			scrY = y;
			consumed = true;
			switch(button)	{
				case 0:
					for(SocketViewVector::iterator in = inlets.begin(); in != inlets.end(); ++in)	{
						(*in)->mousePressed(x,y,button);
						if((*in)->eventConsumed())	{
							return;
						}
					}
					for(SocketViewVector::iterator out = outlets.begin(); out != outlets.end(); ++out)	{
						(*out)->mousePressed(x,y,button);
						if((*out)->eventConsumed())	{
							return;
						}
					}
					if(onInside(x,y))	{
						text.mousePressed(x,y,button);
					} else if(onMargin(x,y))	{
						dodrag = true;
					}
					break;
				case 1:
					break;
				case 2:
					openMenu(x,y);
					break;
			}
			
		}
	}
	
	void LangObjectView::mouseReleased(int x, int y, int button)
	{
		if(dodrag) {
			dodrag=false;
		}
	}
	
	void LangObjectView::windowResized(int w, int h)
	{
		
	}
	
	////////////////////////////////////////
	// BANG VIEW
	////////////////////////////////////////
	LangBangView::LangBangView() : inlet(Objsc::LANG_INLET), outlet(Objsc::LANG_OUTLET), dodrag(false), scrX(0), scrY(0), showbang(false)
	{
		Objsc::StylePalette::Current()->applyStyle(Objsc::LangViewBase::stylename, this);
		setBounds(0,0, Objsc::LANGVIEW_DFTHEIGHT, Objsc::LANGVIEW_DFTHEIGHT);
	}
	
	LangBangView::~LangBangView() 
	{
		//~ if(object)
			//~ object->removeObserver();
	}
	
	void LangBangView::setObject(Objsc::LangObjectPtr objectPtr)
	{
		object=objectPtr;
		//object->setObserver(); // cast to weak pointer
	}
	
	
	void LangBangView::initSockets(std::tr1::weak_ptr<Objsc::LangView> weakPtr)
	{
		inlet.setAction(weakPtr, 0);
		outlet.setAction(weakPtr, 0);
	}
	
	void LangBangView::update()
	{
		
	}
	
	void LangBangView::draw()
	{
		if (visible)	{
			ofSetColor(bgColour.r, bgColour.g, bgColour.b, bgColour.a);
			ofRect(bounds.getX(), bounds.getY(), bounds.getWidth(), bounds.getHeight());
			if(showbang)	{
				ofSetColor(fgColour.r, fgColour.g, fgColour.b, fgColour.a);
				ofCircle(bounds.getCentreX(), bounds.getCentreY(), Objsc::LANGVIEW_DFTHEIGHT/2.0f);
				showbang=false;
			} else if(hasFocus)	{
				ofSetColor(hiColour.r, hiColour.g, hiColour.b, hiColour.a);
				ofCircle(bounds.getCentreX(), bounds.getCentreY(), Objsc::LANGVIEW_DFTHEIGHT/2.0f);
			}
			ofSetColor(fgColour.r, fgColour.g, fgColour.b);
			ofNoFill();
			ofCircle(bounds.getCentreX(), bounds.getCentreY(), Objsc::LANGVIEW_DFTHEIGHT/2.0f);
			ofFill();
			outlineBox(bounds);
			inlet.draw();
			outlet.draw();
		}
	}
	
	void LangBangView::_setLocation(float x, float y)
	{
		bounds.setLocation(x,y);
		inrect.setLocation(bounds.getX()+Objsc::LANGVIEW_MARGIN, bounds.getY()+Objsc::LANGVIEW_MARGIN);
		inlet.setLocation(x,y);
		outlet.setLocation(x,bounds.getBottom()-Objsc::LANGVIEW_SOCKETHEIGHT);
	}
	
	void LangBangView::_setBounds(float x, float y, float width, float height)
	{
		bounds.set(x,y,width,height);
		inrect.set(bounds.getX()+Objsc::LANGVIEW_MARGIN, bounds.getY()+Objsc::LANGVIEW_MARGIN, bounds.getWidth()-Objsc::LANGVIEW_MARGIN*2, bounds.getHeight()-Objsc::LANGVIEW_MARGIN*2);
		inlet.setLocation(x,y);
		outlet.setLocation(x,bounds.getBottom()-Objsc::LANGVIEW_SOCKETHEIGHT);
	}
	
	void LangBangView::keyPressed(int key)
	{
		consumed = false;
	}
	
	void LangBangView::keyReleased(int key)
	{
		consumed = false;
	}
	
	void LangBangView::mouseMoved(int x, int y )
	{
		if(withinBounds(x,y) && visible)	{
			if(!hasFocus && displayState!=Objsc::DISABLED) {
				if(onInside(x,y))	{
					gainFocus();
				}
			}
		} else {
			if(hasFocus && displayState!=Objsc::DISABLED) {
				looseFocus();
				if(dodrag)
					dodrag=false;
			}
		}
		inlet.mouseMoved(x,y);
		outlet.mouseMoved(x,y);
	}
	
	void LangBangView::mouseDragged(int x, int y, int button)
	{
		consumed = false;
		float dX = x-scrX;
		float dY = y-scrY;
		if(dodrag)	{
			bounds.moveCentre(dX,dY);
			inrect.moveCentre(dX,dY);
			inlet.setLocation(bounds.getX(), bounds.getY());
			outlet.setLocation(bounds.getX(), bounds.getBottom()-Objsc::LANGVIEW_SOCKETHEIGHT);
			mapToStage();
			consumed = true;
		}
		scrX = x;
		scrY = y;
	}
	
	void LangBangView::mousePressed(int x, int y, int button)
	{
		consumed = false;
		if(withinBounds(x,y))	{
			scrX = x;
			scrY = y;
			consumed = true;
			switch(button)	{
				case 0:
					inlet.mousePressed(x,y,button);
					if(inlet.eventConsumed())	{
						return;
					}
					outlet.mousePressed(x,y,button);
					if(outlet.eventConsumed())	{
						return;
					}
					if(onInside(x,y))	{
						if(object)
							object->activate();
					} else if(onMargin(x,y))	{
						dodrag = true;
					}
					break;
				case 1:
					break;
				case 2:
					openMenu(x,y);
					break;
			}
		}
	}
	
	void LangBangView::mouseReleased(int x, int y, int button)
	{
		if(dodrag) {
			dodrag=false;
		}
	}
	
	void LangBangView::windowResized(int w, int h)
	{
		
	}
	
	
	////////////////////////////////////////
	// TOGGLE VIEW
	////////////////////////////////////////
	LangToggleView::LangToggleView() : inlet(Objsc::LANG_INLET), outlet(Objsc::LANG_OUTLET), dodrag(false), scrX(0), scrY(0), drawon(false)
	{
		Objsc::StylePalette::Current()->applyStyle(Objsc::LangViewBase::stylename, this);
		setBounds(0,0, Objsc::LANGVIEW_DFTHEIGHT, Objsc::LANGVIEW_DFTHEIGHT);
	}
	
	LangToggleView::~LangToggleView()
	{
		//if(object)
			//object->removeObserver();
	}
	
	void LangToggleView::setObject(Objsc::LangObjectPtr objectPtr)
	{
		object=objectPtr;
		if(object)
			drawon = std::tr1::static_pointer_cast<Objsc::ToggleObject>(object)->isOn();
	}
	
	
	void LangToggleView::initSockets(std::tr1::weak_ptr<Objsc::LangView> weakPtr)
	{
		inlet.setAction(weakPtr, 0);
		outlet.setAction(weakPtr, 0);
	}
	
	void LangToggleView::update()
	{
		
	}
	
	void LangToggleView::objActivated() 
	{
		//printf("LangToggleView::objActivated\n");
		if(object)
			drawon = std::tr1::static_pointer_cast<Objsc::ToggleObject>(object)->isOn();
	}
	
	void LangToggleView::draw()
	{
		if (visible)	{
			ofSetColor(bgColour.r, bgColour.g, bgColour.b, bgColour.a);
			ofRect(bounds.getX(), bounds.getY(), bounds.getWidth(), bounds.getHeight());
			ofSetColor(fgColour.r, fgColour.g, fgColour.b);
			if(drawon)	{
				crossXLines(bounds);
			}
			outlineBox(bounds);
			inlet.draw();
			outlet.draw();
		}
	}
	
	void LangToggleView::_setLocation(float x, float y)
	{
		bounds.setLocation(x,y);
		inrect.setLocation(bounds.getX()+Objsc::LANGVIEW_MARGIN, bounds.getY()+Objsc::LANGVIEW_MARGIN);
		inlet.setLocation(x,y);
		outlet.setLocation(x,bounds.getBottom()-Objsc::LANGVIEW_SOCKETHEIGHT);
	}
	
	void LangToggleView::_setBounds(float x, float y, float width, float height)
	{
		bounds.set(x,y,width,height);
		inrect.set(bounds.getX()+Objsc::LANGVIEW_MARGIN, bounds.getY()+Objsc::LANGVIEW_MARGIN, bounds.getWidth()-Objsc::LANGVIEW_MARGIN*2, bounds.getHeight()-Objsc::LANGVIEW_MARGIN*2);
		inlet.setLocation(x,y);
		outlet.setLocation(x,bounds.getBottom()-Objsc::LANGVIEW_SOCKETHEIGHT);
	}
	
	void LangToggleView::keyPressed(int key)
	{
		consumed = false;
	}
	
	void LangToggleView::keyReleased(int key)
	{
		consumed = false;
	}
	
	void LangToggleView::mouseMoved(int x, int y )
	{
		if(withinBounds(x,y) && visible)	{
			if(!hasFocus && displayState!=Objsc::DISABLED) {
				if(onInside(x,y))	{
					gainFocus();
				}
			}
		} else {
			if(hasFocus && displayState!=Objsc::DISABLED) {
				looseFocus();
				if(dodrag)
					dodrag=false;
			}
		}
		inlet.mouseMoved(x,y);
		outlet.mouseMoved(x,y);
	}
	
	void LangToggleView::mouseDragged(int x, int y, int button)
	{
		consumed = false;
		float dX = x-scrX;
		float dY = y-scrY;
		if(dodrag)	{
			bounds.moveCentre(dX,dY);
			inrect.moveCentre(dX,dY);
			inlet.setLocation(bounds.getX(), bounds.getY());
			outlet.setLocation(bounds.getX(), bounds.getBottom()-Objsc::LANGVIEW_SOCKETHEIGHT);
			mapToStage();
			consumed = true;
		}
		scrX = x;
		scrY = y;
	}
	
	void LangToggleView::mousePressed(int x, int y, int button)
	{
		consumed = false;
		if(withinBounds(x,y))	{
			scrX = x;
			scrY = y;
			consumed = true;
			switch(button)	{
				case 0:
					inlet.mousePressed(x,y,button);
					if(inlet.eventConsumed())	{
						return;
					}
					outlet.mousePressed(x,y,button);
					if(outlet.eventConsumed())	{
						return;
					}
					if(onInside(x,y))	{
						if(object)
							object->activate();
					} else if(onMargin(x,y))	{
						dodrag = true;
					}
					break;
				case 1:
					break;
				case 2:
					openMenu(x,y);
					break;
			}
		}
	}
	
	void LangToggleView::mouseReleased(int x, int y, int button)
	{
		if(dodrag) {
			dodrag=false;
		}
	}
	
	void LangToggleView::windowResized(int w, int h)
	{
		
	}
	
	////////////////////////////////////////
	// MESSAGE VIEW
	////////////////////////////////////////
	LangMessageView::LangMessageView() : inlet(Objsc::LANG_INLET), outlet(Objsc::LANG_OUTLET), dodrag(false), scrX(0), scrY(0), actionParse()
	{
		Objsc::StylePalette::Current()->applyStyle(Objsc::LangViewBase::stylename, this);
		Objsc::StylePalette::Current()->applyStyle(Objsc::LangViewBase::stylename, &text);
		setBounds(0,0, Objsc::LANGVIEW_DFTWIDTH, Objsc::LANGVIEW_DFTHEIGHT);
		text.sizeWithText(true);
		startEdit();
		actionParse = Objsc::CommandDispatcher::createCommand(Objsc::LangPatchEditor::cmdlabel  , "lobj_parsemsg");
	}
	
	void LangMessageView::setObject(Objsc::LangObjectPtr objectPtr) 
	{
		object=objectPtr;
		updateText();
	}
	
	void LangMessageView::initSockets(std::tr1::weak_ptr<Objsc::LangView> weakPtr)
	{
		inlet.setAction(weakPtr, 0);
		outlet.setAction(weakPtr, 0);
	}
	
	void LangMessageView::initActions(std::tr1::weak_ptr<Objsc::LangView> weakPtr)
	{
		actionParse->getData()->addArg(weakPtr);
	}
	
	void LangMessageView::update()
	{
		
	}
	
	void LangMessageView::draw()
	{
		if (visible)	{
			ofSetColor(bgColour.r, bgColour.g, bgColour.b, bgColour.a);
			ofRect(bounds.getX(), bounds.getY(), bounds.getWidth(), bounds.getHeight());
			ofSetColor(fgColour.r, fgColour.g, fgColour.b);
			text.draw();
			ofSetColor(fgColour.r, fgColour.g, fgColour.b, fgColour.a);
			ofLine(bounds.getX(), bounds.getY(), bounds.getRight(), bounds.getY());
			ofLine(bounds.getX(), bounds.getBottom(), bounds.getRight(), bounds.getBottom());
			ofLine(bounds.getX(), bounds.getY(), bounds.getX(), bounds.getBottom());
			ofLine(bounds.getRight(), bounds.getY(), bounds.getRight()-Objsc::LANGVIEW_BEVEL, bounds.getY()+Objsc::LANGVIEW_BEVEL);
			ofLine(bounds.getRight()-Objsc::LANGVIEW_BEVEL, bounds.getY()+Objsc::LANGVIEW_BEVEL, bounds.getRight()-Objsc::LANGVIEW_BEVEL, bounds.getBottom()-Objsc::LANGVIEW_BEVEL);
			ofLine(bounds.getRight()-Objsc::LANGVIEW_BEVEL, bounds.getBottom()-Objsc::LANGVIEW_BEVEL, bounds.getRight(), bounds.getBottom());
			inlet.draw();
			outlet.draw();
		}
	}
	
	void LangMessageView::_setLocation(float x, float y)
	{
		bounds.setLocation(x,y);
		inrect.setLocation(bounds.getX()+Objsc::LANGVIEW_MARGIN, bounds.getY()+Objsc::LANGVIEW_MARGIN);
		text.setLocation(x,y);
		inlet.setLocation(x,y);
		outlet.setLocation(x,bounds.getBottom()-Objsc::LANGVIEW_SOCKETHEIGHT);
	}
	
	void LangMessageView::_setBounds(float x, float y, float width, float height)
	{
		bounds.set(x,y,width,height);
		inrect.set(bounds.getX()+Objsc::LANGVIEW_MARGIN, bounds.getY()+Objsc::LANGVIEW_MARGIN, bounds.getWidth()-Objsc::LANGVIEW_MARGIN*2, bounds.getHeight()-Objsc::LANGVIEW_MARGIN*2);
		text.setBounds(x,y,width-Objsc::LANGVIEW_BEVEL,height);
		inlet.setLocation(x,y);
		outlet.setLocation(x,bounds.getBottom()-Objsc::LANGVIEW_SOCKETHEIGHT);
	}
	
	void LangMessageView::fitToText()
	{
		if(text.getWidth()+Objsc::LANGVIEW_RIGHTMARGIN > bounds.getWidth())	{
			bounds.setWidth(text.getWidth()+Objsc::LANGVIEW_RIGHTMARGIN+Objsc::LANGVIEW_MARGIN);
		}
	}
	
	void LangMessageView::updateText()
	{
		text.setText(object->getDisplayString());
		fitToText();
		if(text.getText().empty())	{
			startEdit();
		} else {
			endEdit();
		}
	}
	
	void LangMessageView::startEdit()
	{
		text.editOn();
		text.setTextColour(255,0,0);
	}
	
	void LangMessageView::endEdit()
	{
		text.editOff();
		text.setTextColour(0,0,0);
	}
	
	void LangMessageView::keyPressed(int key)
	{
		consumed = false;
		if(hasFocus) 	{
			if(key == OF_KEY_RETURN)	{
				object->setDisplayString(text.getText());
				updateText();
				if(object) {
					object->activate();
				}
			} else {
				text.keyPressed(key);
				fitToText();
			}
			consumed = true;
		}
	}
	
	void LangMessageView::keyReleased(int key)
	{
		if(hasFocus) 	{
			text.keyReleased(key);
		}
	}
	
	void LangMessageView::mouseMoved(int x, int y )
	{
		if(withinBounds(x,y) && visible)	{
			if(!hasFocus && displayState!=Objsc::DISABLED) {
				gainFocus();
			}
		} else {
			if(hasFocus && displayState!=Objsc::DISABLED) {
				looseFocus();
				if(dodrag)
					dodrag=false;
			}
		}
		text.mouseMoved(x,y);
		inlet.mouseMoved(x,y);
		outlet.mouseMoved(x,y);
	}
	
	void LangMessageView::mouseDragged(int x, int y, int button)
	{
		consumed = false;
		float dX = x-scrX;
		float dY = y-scrY;
		if(dodrag)	{
			bounds.moveCentre(dX,dY);
			inrect.moveCentre(dX,dY);
			text.setLocation(bounds.getX(), bounds.getY());
			inlet.setLocation(bounds.getX(), bounds.getY());
			outlet.setLocation(bounds.getX(), bounds.getBottom()-Objsc::LANGVIEW_SOCKETHEIGHT);
			mapToStage();
			consumed = true;
		}
		scrX = x;
		scrY = y;
	}
	
	void LangMessageView::mousePressed(int x, int y, int button)
	{
		consumed = false;
		if(withinBounds(x,y))	{
			consumed = true;
			scrX = x;
			scrY = y;
			switch(button)	{
				case 0:
					inlet.mousePressed(x,y,button);
					if(inlet.eventConsumed())	{
						return;
					}
					outlet.mousePressed(x,y,button);
					if(outlet.eventConsumed())	{
						return;
					}
					if(onInside(x,y))	{
						if(text.isEditable())	{
							text.mousePressed(x,y,button);
						} else if(object) {
							object->activate();
						}
					} else if(onMargin(x,y))	{
						dodrag = true;
					}
					break;
				case 1:
					break;
				case 2:
					openMenu(x,y);
					break;
			}
		}
	}
	
	void LangMessageView::mouseReleased(int x, int y, int button)
	{
		if(dodrag) {
			dodrag=false;
		}
	}
	
	
	////////////////////////////////////////
	// NUMBER VIEW
	////////////////////////////////////////
	LangNumberView::LangNumberView() : inlet(Objsc::LANG_INLET), outlet(Objsc::LANG_OUTLET), dodrag(false), scrX(0), scrY(0), actionParse()
	{
		Objsc::StylePalette::Current()->applyStyle(Objsc::LangViewBase::stylename, this);
		Objsc::StylePalette::Current()->applyStyle(Objsc::LangViewBase::stylename, &text);
		setBounds(0,0, Objsc::LANGVIEW_DFTWIDTH, Objsc::LANGVIEW_DFTHEIGHT);
		text.sizeWithText(true);
		actionParse = Objsc::CommandDispatcher::createCommand(Objsc::LangPatchEditor::cmdlabel  , "lobj_parsenum");
	}
	
	LangNumberView::~LangNumberView()
	{
		//~ if(object)
			//~ object->removeObserver();
	}
	
	void LangNumberView::setObject(Objsc::LangObjectPtr objectPtr) 
	{
		object=objectPtr;
		updateText();
	}
	
	void LangNumberView::initSockets(std::tr1::weak_ptr<Objsc::LangView> weakPtr)
	{
		inlet.setAction(weakPtr, 0);
		outlet.setAction(weakPtr, 0);
	}
	
	void LangNumberView::initActions(std::tr1::weak_ptr<Objsc::LangView> weakPtr)
	{
		actionParse->getData()->addArg(weakPtr);
	}
	
	void LangNumberView::update()
	{
		
	}
	
	void LangNumberView::draw()
	{
		if (visible)	{
			ofSetColor(bgColour.r, bgColour.g, bgColour.b, bgColour.a);
			ofRect(bounds.getX(), bounds.getY(), bounds.getWidth(), bounds.getHeight());
			ofSetColor(fgColour.r, fgColour.g, fgColour.b);
			text.draw();
			ofSetColor(fgColour.r, fgColour.g, fgColour.b, fgColour.a);
			ofLine(bounds.getX(), bounds.getY(), bounds.getRight()-Objsc::LANGVIEW_BEVEL, bounds.getY());
			ofLine(bounds.getX(), bounds.getBottom(), bounds.getRight(), bounds.getBottom());
			ofLine(bounds.getX(), bounds.getY(), bounds.getX(), bounds.getBottom());
			ofLine(bounds.getRight()-Objsc::LANGVIEW_BEVEL, bounds.getY(), bounds.getRight(), bounds.getY()+Objsc::LANGVIEW_BEVEL);
			ofLine(bounds.getRight(), bounds.getY()+Objsc::LANGVIEW_BEVEL, bounds.getRight(), bounds.getBottom());
			inlet.draw();
			outlet.draw();
		}
	}
	
	void LangNumberView::_setLocation(float x, float y)
	{
		bounds.setLocation(x,y);
		inrect.setLocation(bounds.getX()+Objsc::LANGVIEW_MARGIN, bounds.getY()+Objsc::LANGVIEW_MARGIN);
		text.setLocation(x,y);
		inlet.setLocation(x,y);
		outlet.setLocation(x,bounds.getBottom()-Objsc::LANGVIEW_SOCKETHEIGHT);
	}
	
	void LangNumberView::_setBounds(float x, float y, float width, float height)
	{
		bounds.set(x,y,width,height);
		inrect.set(bounds.getX()+Objsc::LANGVIEW_MARGIN, bounds.getY()+Objsc::LANGVIEW_MARGIN, bounds.getWidth()-Objsc::LANGVIEW_MARGIN*2, bounds.getHeight()-Objsc::LANGVIEW_MARGIN*2);
		text.setBounds(x,y,width-Objsc::LANGVIEW_BEVEL,height);
		inlet.setLocation(x,y);
		outlet.setLocation(x,bounds.getBottom()-Objsc::LANGVIEW_SOCKETHEIGHT);
	}
	
	void LangNumberView::fitToText()
	{
		if(text.getWidth()+Objsc::LANGVIEW_RIGHTMARGIN > bounds.getWidth())	{
			bounds.setWidth(text.getWidth()+Objsc::LANGVIEW_RIGHTMARGIN+Objsc::LANGVIEW_MARGIN);
		}
	}
	
	void LangNumberView::updateText()
	{
		text.setText(object->getDisplayString());
		fitToText();
	}
	
	void LangNumberView::objActivated() 
	{
		updateText();
	}
	
	void LangNumberView::keyPressed(int key)
	{
		if(hasFocus) 	{
			if(key == OF_KEY_RETURN)	{
				object->setDisplayString(text.getText());
				object->activate();
			} else {
				text.keyPressed(key);
				fitToText();
			}
		}
	}
	
	void LangNumberView::keyReleased(int key)
	{
		if(hasFocus) 	{
			text.keyReleased(key);
		}
	}
	
	void LangNumberView::mouseMoved(int x, int y )
	{
		if(withinBounds(x,y) && visible)	{
			if(!hasFocus && displayState!=Objsc::DISABLED) {
				gainFocus();
			}
		} else {
			if(hasFocus && displayState!=Objsc::DISABLED) {
				looseFocus();
				if(dodrag)
					dodrag=false;
			}
		}
		text.mouseMoved(x,y);
		inlet.mouseMoved(x,y);
		outlet.mouseMoved(x,y);
	}
	
	void LangNumberView::mouseDragged(int x, int y, int button)
	{
		consumed = false;
		float dX = x-scrX;
		float dY = y-scrY;
		if(dodrag)	{
			bounds.moveCentre(dX,dY);
			inrect.moveCentre(dX,dY);
			text.setLocation(bounds.getX(), bounds.getY());
			inlet.setLocation(bounds.getX(), bounds.getY());
			outlet.setLocation(bounds.getX(), bounds.getBottom()-Objsc::LANGVIEW_SOCKETHEIGHT);
			mapToStage();
			consumed = true;
		}
		scrX = x;
		scrY = y;
	}
	
	void LangNumberView::mousePressed(int x, int y, int button)
	{
		consumed = false;
		if(withinBounds(x,y))	{
			consumed = true;
			scrX = x;
			scrY = y;
			switch(button)	{
				case 0:
					inlet.mousePressed(x,y,button);
					if(inlet.eventConsumed())	{
						return;
					}
					outlet.mousePressed(x,y,button);
					if(outlet.eventConsumed())	{
						return;
					}
					if(onInside(x,y))	{
						if(text.isEditable())	{
							text.mousePressed(x,y,button);
						} else if(object) {
							object->activate();
						}
					} else if(onMargin(x,y))	{
						dodrag = true;
					}
					break;
				case 1:
					break;
				case 2:
					openMenu(x,y);
					break;
			}
		}
	}
	
	void LangNumberView::mouseReleased(int x, int y, int button)
	{
		if(dodrag) {
			dodrag=false;
		}
	}
	
	////////////////////////////////////////
	// CORD VIEW
	////////////////////////////////////////
	LangCordView::LangCordView() : outletView(), inletView(), outlet(), inlet(), endX(), endY(), editing(false)
	{
		Objsc::StylePalette::Current()->applyStyle(Objsc::LangCordView::stylename, this);
	}
	
	
	void LangCordView::initActions(std::tr1::weak_ptr<Objsc::LangCordView> weakPtr)
	{
		actionDelete = Objsc::CommandDispatcher::createCommand(Objsc::LangPatchEditor::cmdlabel  , "cord_delete");
		actionDelete->getData()->addArg(weakPtr);
	}
	
	
	void LangCordView::start(Objsc::LangViewPtr viewPtr, int socket) 
	{
		outletView=viewPtr;
		outlet=socket;
		endX = outletView->getOutletX(outlet);
		endY = outletView->getOutletY(outlet);
		editing = true;
		visible = true;
	}
	void LangCordView::connect(Objsc::LangViewPtr viewPtr, int socket, bool linkobject) 
	{
		inletView=viewPtr;
		inlet=socket;
		endX = inletView->getInletX(inlet);
		endY = inletView->getInletY(inlet);
		editing = false;
		if(linkobject)
			outletView->link(outlet, inletView, inlet);
	}
	
	void LangCordView::unlink()
	{
		if(outletView && inletView)	{
			outletView->getObject()->unlink(outlet, inletView->getObject(), inlet);
			Objsc::CommandDispatcher::Current()->doCommand(actionDelete);
		}
	}
				
	void LangCordView::update()
	{
		
	}
	
	void LangCordView::draw()
	{
		if (visible)	{
			if(hasFocus) {
				ofSetColor(hiColour.r, hiColour.g, hiColour.b);
			} else {
				ofSetColor(fgColour.r, fgColour.g, fgColour.b);
			}
			if(editing)	{
				ofLine(outletView->getOutletX(outlet), outletView->getOutletY(outlet), endX, endY);
			} else {
				ofLine(outletView->getOutletX(outlet), outletView->getOutletY(outlet), inletView->getInletX(inlet), inletView->getInletY(inlet));
			}
		}
	}
	
	void LangCordView::keyPressed(int key)
	{
		consumed = false;
		if(!editing && hasFocus)	{
			if(key == OF_KEY_BACKSPACE)	{
				unlink();
				consumed = true;
			}
		}
	}
	
	void LangCordView::keyReleased(int key)
	{
		
	}
	
	bool LangCordView::withinBounds(float x, float y)
	{
		if(editing)	{
			return true;
		} else {
			if(x < outletView->getOutletX(outlet) &&  x < inletView->getInletX(inlet))
				return false;
			if(x > outletView->getOutletX(outlet) &&  x > inletView->getInletX(inlet))
				return false;
			if(y < outletView->getOutletY(outlet) &&  y < inletView->getInletY(inlet))
				return false;
			if(y > outletView->getOutletY(outlet) &&  y > inletView->getInletY(inlet))
				return false;
			return fabs((outletView->getOutletX(outlet)*(inletView->getInletY(inlet)-y))+(inletView->getInletX(inlet)*(y-outletView->getOutletY(outlet)))+(x*(outletView->getOutletY(outlet)-inletView->getInletY(inlet)))) < 160.0f;
		}
	}
	
	void LangCordView::mouseMoved(int x, int y )
	{
		if(editing)	{
			endX = x;
			endY = y;
		} else {
			if(withinBounds(x,y) && visible)	{
				if(!hasFocus) {
					gainFocus();
				}
			} else {
				if(hasFocus) {
					looseFocus();
				}
			}
		}
	}
	
	void LangCordView::mouseDragged(int x, int y, int button)
	{
		
	}
	
	void LangCordView::mousePressed(int x, int y, int button)
	{
		consumed = false;
		if(!editing && withinBounds(x,y))	{
			unlink();
			consumed = true;
		}
	}
	
	void LangCordView::mouseReleased(int x, int y, int button)
	{
		
	}
	
	void LangCordView::windowResized(int w, int h)
	{
		
	}
	
	
	////////////////////////////////////////
	// PATCH CANVAS
	////////////////////////////////////////
	LangPatchCanvas::LangPatchCanvas() : patch(), objviews(), cord(), cordviews()//, namefield()
	{
		Objsc::StylePalette::Current()->applyStyle(Objsc::LangPatchCanvas::stylename, this);
		//Objsc::StylePalette::Current()->applyStyle(Objsc::LangPatchCanvas::stylename, &//namefield);
		//~ //namefield.sizeWithText(true);
		//~ //namefield.clearText();
		//~ //namefield.drawBackground(false);
	}
	
	void LangPatchCanvas::setPatch(Objsc::LangPatchPtr patchPtr) 
	{
		if(patch)	{
			clear();
		}
		patch=patchPtr;
		//~ //namefield.setText(patch->getName());
		//~ //namefield.setLocation(bounds.getRight()-//namefield.getWidth()-Objsc::WINDOWMARGIN_WIDTH, bounds.getY());
	}
			
	void LangPatchCanvas::update()
	{
		
	}
	
	void LangPatchCanvas::draw()
	{
		//ofSetColor(bgColour.r, bgColour.g, bgColour.b, 255);
		ofSetColor(bgColour.r, bgColour.g, bgColour.b, 120);
		ofRect(bounds.getX(), bounds.getY(), bounds.getWidth(), bounds.getHeight());
		//namefield.draw();
		for(LangCordViewVector::iterator crd = cordviews.begin(); crd != cordviews.end(); ++crd)	{
			(*crd)->draw();
		}
		for(LangViewVector::iterator obj = objviews.begin(); obj != objviews.end(); ++obj)	{
			(*obj)->draw();
		}
		if(cord)	{
			cord->draw();
		}
	}
	
	void LangPatchCanvas::setLocation(float x, float y)
	{
		bounds.setLocation(x,y);
		//namefield.setLocation(bounds.getRight()-//namefield.getWidth()-Objsc::WINDOWMARGIN_WIDTH, bounds.getY());
	}
	
	void LangPatchCanvas::setDimensions(float width, float height)
	{
		bounds.setDimensions(width,height);
		//namefield.setLocation(bounds.getRight()-//namefield.getWidth()-Objsc::WINDOWMARGIN_WIDTH, bounds.getY());
	}
	
	void LangPatchCanvas::setBounds(float x, float y, float width, float height)
	{
		bounds.set(x,y,width,height);
		//namefield.setLocation(bounds.getRight()-//namefield.getWidth()-Objsc::WINDOWMARGIN_WIDTH, bounds.getY());
	}
	
	void LangPatchCanvas::keyPressed(int key)
	{
		for(LangViewVector::iterator obj = objviews.begin(); obj != objviews.end(); ++obj)	{
			(*obj)->keyPressed(key);
			if((*obj)->eventConsumed())	{
				consumed = true;
				return;
			}
		}
		for(LangCordViewVector::iterator crd = cordviews.begin(); crd != cordviews.end(); ++crd)	{
			(*crd)->keyPressed(key);
			if((*crd)->eventConsumed())	{
				consumed = true;
				return;
			}
		}
	}
	
	void LangPatchCanvas::keyReleased(int key)
	{
		for(LangViewVector::iterator obj = objviews.begin(); obj != objviews.end(); ++obj)	{
			(*obj)->keyReleased(key);
		}
	}
	
	void LangPatchCanvas::mouseMoved(int x, int y )
	{
		for(LangCordViewVector::iterator crd = cordviews.begin(); crd != cordviews.end(); ++crd)	{
			(*crd)->mouseMoved(x,y);
		}
		for(LangViewVector::iterator obj = objviews.begin(); obj != objviews.end(); ++obj)	{
			(*obj)->mouseMoved(x,y);
		}
		if(cord)	{
			cord->mouseMoved(x,y);
		}
	}
	
	void LangPatchCanvas::mouseDragged(int x, int y, int button)
	{
		consumed = false;
		for(LangViewVector::iterator obj = objviews.begin(); obj != objviews.end(); ++obj)	{
			(*obj)->mouseDragged(x,y,button);
			if((*obj)->eventConsumed())	{
				consumed = true;
				return;
			}
		}
	}
	
	void LangPatchCanvas::mousePressed(int x, int y, int button)
	{
		consumed = false;
		for(LangViewVector::iterator obj = objviews.begin(); obj != objviews.end(); ++obj)	{
			(*obj)->mousePressed(x,y,button);
			if((*obj)->eventConsumed())	{
				consumed = true;
				return;
			}
		}
		for(LangCordViewVector::iterator crd = cordviews.begin(); crd != cordviews.end(); ++crd)	{
			(*crd)->mousePressed(x,y,button);
			if((*crd)->eventConsumed())	{
				consumed = true;
				return;
			}
		}
		if(cord && !consumed)	{
			consumed = true;
			dropCord();
		}
	}
	
	void LangPatchCanvas::mouseReleased(int x, int y, int button)
	{
		for(LangCordViewVector::iterator crd = cordviews.begin(); crd != cordviews.end(); ++crd)	{
			(*crd)->mouseReleased(x,y,button);
		}
		for(LangViewVector::iterator obj = objviews.begin(); obj != objviews.end(); ++obj)	{
			(*obj)->mouseReleased(x,y,button);
		}
		if(cord)	{
			cord->mouseReleased(x,y,button);
		}
	}
	
	void LangPatchCanvas::windowResized(int w, int h)
	{
		
	}
	
	void LangPatchCanvas::addObjectView(Objsc::LangViewPtr view)
	{
		view->setCanvasRect(bounds);
		view->mapToScreen();
		view->initSockets(std::tr1::weak_ptr<Objsc::LangView>(view));
		objviews.push_back(view);
	}
	
	void LangPatchCanvas::linkAllViews()
	{
		for(LangViewVector::iterator view = objviews.begin(); view != objviews.end(); ++view)	{
			if((*view)->getObject()->getNumOutlets()  > 0)	{
				linkFromView((*view), false);
			}
		}
	}
	
	void LangPatchCanvas::linkFromView(Objsc::LangViewPtr viewFrom, bool linkobject)
	{
		const Objsc::LangCordMap outlets = viewFrom->getObject()->getOutlets();
		for(Objsc::LangCordMap::const_iterator link = outlets.begin(); link != outlets.end(); ++link)	{
			const Objsc::LangAddressVector addrs = link->second->getAddresses();
			for(Objsc::LangAddressVector::const_iterator addr = addrs.begin(); addr != addrs.end(); ++addr)	{
				linkViews(viewFrom, link->first, viewForObject((*addr).first), (*addr).second, linkobject);
			}
		}
	}
	
	void LangPatchCanvas::linkViews(Objsc::LangViewPtr viewFrom, int outlet, Objsc::LangViewPtr viewTo, int inlet, bool linkobject)
	{
		if(viewFrom && viewTo)	{
			cordviews.push_back(Objsc::LangCordView::create());
			cordviews.back()->start(viewFrom,outlet);
			cordviews.back()->connect(viewTo,inlet,linkobject);
		}
	}
	
	Objsc::LangViewPtr& LangPatchCanvas::viewForObject(Objsc::LangObjectPtr objPtr)
	{
		for(LangViewVector::iterator view = objviews.begin(); view != objviews.end(); ++view)	{
			if((*view)->getObject() == objPtr)
				return (*view);
		}
	}
	
	// lang
	void LangPatchCanvas::createObject(float x, float y)
	{
		Objsc::LangObjectViewPtr view = Objsc::LangObjectView::create();
		view->setCanvasRect(bounds);
		view->setLocation(x,y);
		objviews.push_back(std::tr1::static_pointer_cast<Objsc::LangView>(view));
	}
	
	void LangPatchCanvas::createMessage(float x, float y)
	{
		Objsc::LangMessageViewPtr view = Objsc::LangMessageView::create();
		view->setObject(patch->createObject("message"));
		view->setCanvasRect(bounds);
		view->setLocation(x,y);
		objviews.push_back(std::tr1::static_pointer_cast<Objsc::LangView>(view));
	}
	
	void LangPatchCanvas::createNumber(float x, float y)
	{
		Objsc::LangNumberViewPtr view = Objsc::LangNumberView::create();
		Objsc::LangObjectPtr obj = patch->createObject("number");
		view->setObject(obj);
		view->setCanvasRect(bounds);
		view->setLocation(x,y);
		obj->setObserver(std::tr1::static_pointer_cast<Objsc::LangObserver>(view));
		objviews.push_back(std::tr1::static_pointer_cast<Objsc::LangView>(view));
	}
	
	void LangPatchCanvas::createBang(float x, float y)
	{
		Objsc::LangBangViewPtr view = Objsc::LangBangView::create();
		Objsc::LangObjectPtr obj = patch->createObject("bang");
		view->setObject(obj);
		view->setCanvasRect(bounds);
		view->setLocation(x,y);
		obj->setObserver(std::tr1::static_pointer_cast<Objsc::LangObserver>(view));
		objviews.push_back(std::tr1::static_pointer_cast<Objsc::LangView>(view));
	}
	
	void LangPatchCanvas::createToggle(float x, float y)
	{
		Objsc::LangToggleViewPtr view = Objsc::LangToggleView::create();
		Objsc::LangObjectPtr obj = patch->createObject("toggle");
		view->setObject(obj);
		view->setCanvasRect(bounds);
		view->setLocation(x,y);
		obj->setObserver(std::tr1::static_pointer_cast<Objsc::LangObserver>(view));
		objviews.push_back(std::tr1::static_pointer_cast<Objsc::LangView>(view));
	}
	
	void LangPatchCanvas::startCord(Objsc::LangViewPtr view, int socket)
	{
		cord = Objsc::LangCordView::create();
		cord->start(view,socket);
	}
	
	void LangPatchCanvas::endCord(Objsc::LangViewPtr view, int socket)
	{
		if(cord)	{
			cord->connect(view,socket);
			cordviews.push_back(cord);
			cord.reset();
		}
	}
	
	void LangPatchCanvas::dropCord()
	{
		cord.reset();
	}
	
	void LangPatchCanvas::clear() 
	{ 
		if(cord)	{
			cord.reset();
		}
		cordviews.clear(); // MIGHT LEAK!
		objviews.clear(); // MIGHT LEAK!
	}
	
	void LangPatchCanvas::removeObject(Objsc::LangViewPtr objview)
	{
		cordviews.erase(remove_if(cordviews.begin(), cordviews.end(), bind2nd(cordConnects(), objview)), cordviews.end());
		objviews.erase(find(objviews.begin(), objviews.end(), objview));
		if(objview->hasObject())	{
			patch->removeObject(objview->getObject());
		}
	}
	
	
	void LangPatchCanvas::removeCord(Objsc::LangCordViewPtr cordview)
	{
		cordviews.erase(find(cordviews.begin(), cordviews.end(),cordview));
	}
	
	
	// parsing
	void LangPatchCanvas::parseStringToObject(Objsc::LangViewPtr objview, const string& str)
	{
		Objsc::LangObjectPtr objPtr = patch->parseStringToObject(str);
		if(objPtr)	{
			patch->addObject(objPtr);
			objview->setObject(objPtr);
			objview->mapToStage();
			objview->initSockets(std::tr1::weak_ptr<Objsc::LangView>(objview));
		} else {
			objview->reset();
		}
	}
}

