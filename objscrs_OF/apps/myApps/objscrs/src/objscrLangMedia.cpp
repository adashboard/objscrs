 #include "objscrLangMedia.hpp"

#include <stdlib.h>

// objscrs
#include "objscrFiles.hpp"
#include "objscrDataTypes.hpp"
#include "objscrMediaPlayer.hpp"
#include "objscrSoundMixer.hpp"
#include "objscrSettings.hpp"
#include "objscrGUIRoot.hpp"

namespace Objsc
{
	
	////////////////////////////////////////
	// DECLARATIONS
	////////////////////////////////////////
	const string Objsc::SoundObject::langname = "snd";
	const string Objsc::VideoObject::langname = "vid";
	const string Objsc::ImageObject::langname = "img";
	const string Objsc::AnimObject::langname = "anim";
	const string Objsc::TextObject::langname = "txt";
	const string Objsc::GrabObject::langname = "grab";
	
	/////////////////////////////////////
	// FUNCTIONS
	/////////////////////////////////////
	void initLangMedia()
	{
		Objsc::LangObjectFactory* factory = Objsc::LangObjectFactory::Current();
		//factory->addCreator(Objsc::SoundObject::langname, new Objsc::SoundObjectCreator());
		factory->addCreator(Objsc::VideoObject::langname, new Objsc::VideoObjectCreator());
		factory->addCreator(Objsc::ImageObject::langname, new Objsc::ImageObjectCreator());
		factory->addCreator(Objsc::AnimObject::langname, new Objsc::AnimObjectCreator());
		factory->addCreator(Objsc::TextObject::langname, new Objsc::TextObjectCreator());
		factory->addCreator(Objsc::GrabObject::langname, new Objsc::GrabObjectCreator());
	}
	
	
	
	/////////////////////////////////////
	// MEDIA OBJECT
	/////////////////////////////////////
	bool LangMediaObject::parseArgs(Objsc::LangList* args)
	{
		if(args->size() < min_args)	{
			return false;
		} else {
			setLabel(getItemAsString(args, 0));
			return true;
		}
	}
	
	
	/////////////////////////////////////
	// GRAB OBJECT
	/////////////////////////////////////
	GrabObject::GrabObject() : 
		label(),
		img(),
		alpha(255)
	{
		setInlets(2);
		setOutlets(1);
	}
	
	bool GrabObject::parseArgs(Objsc::LangList* args)
	{
		if(args->size() < min_args)	{
			return false;
		} else {
			setLabel(getItemAsString(args, 0));
			return true;
		}
	}
	
	void GrabObject::setLabel(string nlabel) 
	{ 
		label.assign(nlabel);
		displaystr = Objsc::GrabObject::langname + " " + label;
		//printf("ImageObject::setLabel <%s>\n", label.c_str());
	}
	
	void GrabObject::activate()
	{
		grab();
		send(0, new Objsc::LangBang());
	}
	
	void GrabObject::setAlpha(int value)
	{
		if(value > 99)	{
			alpha = 255;
		} else if(value < 1)	{
			alpha = 0;
		} else {
			alpha = (unsigned char)(255*(float(value)/100.0));
		}
		//printf("GrabObject::setAlpha <%u>\n", alpha);
	}
	
	void GrabObject::grab()
	{
		if(!frameSource)	{
			frameSource = Objsc::GUIRoot::Current()->getFrameSourceEditor()->getFrameSource();
			img.allocate(frameSource->getWidth(),frameSource->getHeight(), OF_IMAGE_COLOR_ALPHA);
		} else {
		//if(frameSource->hasNewFrame())	{
			
			//int w=Objsc::Settings::Current()->getDisplayWidth();
			//int h=Objsc::Settings::Current()->getDisplayHeight();
			
			int w=frameSource->getWidth();
			int h=frameSource->getHeight();
			//img.grabScreen(0,0,w,h);
			unsigned char *srcPixels = frameSource->getFrame();
			
			//printf("GrabObject::grab %d %d\n", frameSource->getWidth(),frameSource->getHeight());
			
			unsigned char *dstPixels = img.getPixels();
			
			int span3, span4;
			for (int i = 0; i < h; i++) {
				for (int j = 0; j < w; j++) {
					span3 = (i*w*3)+(j*3);
					//stride3 = j*3;
					span4 = (i*w*4)+(j*4);
					//stride4 = j*4;
					dstPixels[span4] = srcPixels[span3];
					dstPixels[span4+1] = srcPixels[span3+1];
					dstPixels[span4+2] = srcPixels[span3+2];
					dstPixels[span4+3]=alpha;
					/*
					dstPixels[(i*w*4) + j*4] = srcPixels[(i*w*3) + j*3];
					dstPixels[(i*w*4) + (j*4)+1] = srcPixels[(i*w*3) + (j*3)+1];
					dstPixels[(i*w*4) + (j*4)+2] = srcPixels[(i*w*3) + (j*3)+2];
					dstPixels[(i*w*4) + (j*4)+3]=alpha;
					*/
				}
			}
			
			//printf("GrabObject::grab array done\n");
			
			//img.setFromPixels(dstPixels,frameSource->getWidth(),frameSource->getHeight(), OF_IMAGE_COLOR_ALPHA, true);
			
			img.update();
			if(label.empty())	{
				img.saveImage(Objsc::ProjectFiles::Current()->makeTimestampedImagePath("objscrs"));
			} else {
				img.saveImage(Objsc::ProjectFiles::Current()->makeTimestampedImagePath(label));
			}
			
		}
	}
	
	void GrabObject::receive(int inlet, Objsc::LangAtom* atom)
	{
		switch(atom->getType())	{
			case LANG_NULL:
				break;
			case LANG_BANG:
				if(inlet == 0)	{
					activate();
				}
				break;
			case LANG_INT:
				switch(inlet) {
					case 0:
						if(parseBool(atom))	{
							activate();
						} 
						break;
					case 1:
						setAlpha(parseInt(atom, 100)); 
						break;
				}
				break;
			case LANG_FLOAT:
				switch(inlet) {
					case 0:
						if(parseBool(atom))	{
							activate();
						}
						break;
					case 1:
						setAlpha(parseInt(atom, 100)); 
						break;
				}
				break;
			case LANG_STRING:
				break;
			case LANG_VAR:
				break;
			case LANG_LIST:
				switch(inlet) {
					case 0:
						receiveList(0, dynamic_cast<Objsc::LangList*>(atom));
						break;
				}
				break;
		}
	}
	
	void GrabObject::receiveList(int inlet, LangList* value)
	{
		
		string cmd = parseString(value->idx(0), "");
		if(cmd.empty()) {
			int num = parseInt(value->idx(0), -1);
			if(num > -1)	{
				setAlpha(num);
			}
		} else {
			if(cmd == "grab")	{
				activate();
			} else if(cmd == "prefix")	{
				setLabel(parseString(value->idx(1), ""));
			} else if(cmd == "alpha")	{
				if(value->size() > 1)	{
					setAlpha(parseInt(value->idx(1), 100));
				}
			} 
		}
	}
	
	/////////////////////////////////////
	// IMAGE OBJECT
	/////////////////////////////////////
	ImageObject::~ImageObject()
	{
		
	}
	
	void ImageObject::setLabel(string nlabel) 
	{ 
		label.assign(nlabel);
		displaystr = Objsc::ImageObject::langname + " " + label;
		//printf("ImageObject::setLabel <%s>\n", label.c_str());
	}
	
	void ImageObject::receive(int inlet, Objsc::LangAtom* atom)
	{
		switch(atom->getType())	{
			case LANG_NULL:
				break;
			case LANG_BANG:
				if(inlet == 0)	{
					activate();
				}
				break;
			case LANG_INT:
				switch(inlet) {
					case 0:
						if(parseBool(atom))	{
							showMedia();
						} else {
							hideMedia();
						}
						break;
					case 1:
						locXMedia(parseFloat(atom, 0.0f));
						break;
					case 2:
						locYMedia(parseFloat(atom, 0.0f));
						break;
					case 3:
						widthMedia(parseFloat(atom, -1.0f));
						break;
					case 4:
						heightMedia(parseFloat(atom, -1.0f));
						break;
				}
				break;
			case LANG_FLOAT:
				switch(inlet) {
					case 0:
						if(parseBool(atom))	{
							showMedia();
						} else {
							hideMedia();
						}
						break;
					case 1:
						locXMedia(parseFloat(atom, 0.0f));
						break;
					case 2:
						locYMedia(parseFloat(atom, 0.0f));
						break;
					case 3:
						widthMedia(parseFloat(atom, -1.0f));
						break;
					case 4:
						heightMedia(parseFloat(atom, -1.0f));
						break;
				}
				break;
			case LANG_STRING:
				break;
			case LANG_VAR:
				break;
			case LANG_LIST:
				switch(inlet) {
					case 0:
						receiveList(0, dynamic_cast<Objsc::LangList*>(atom));
						break;
					case 1:
						locXMedia(parseFloat(dynamic_cast<Objsc::LangList*>(atom)->idx(0), 0.0f));
						break;
					case 2:
						locYMedia(parseFloat(dynamic_cast<Objsc::LangList*>(atom)->idx(0), 0.0f));
						break;
					case 3:
						widthMedia(parseFloat(dynamic_cast<Objsc::LangList*>(atom)->idx(0), -1.0f));
						break;
					case 4:
						heightMedia(parseFloat(dynamic_cast<Objsc::LangList*>(atom)->idx(0), -1.0f));
						break;
				}
				break;
		}
	}
	
	void ImageObject::receiveList(int inlet, LangList* value)
	{
		
		string cmd = parseString(value->idx(0), "");
		if(cmd.empty()) {
			if(parseInt(value->idx(0), -1) == 1)	{
				showMedia();
			} else if(parseInt(value->idx(0), -1) == 0)	{
				hideMedia();
			}
		} else {
			if(cmd == "load")	{
				if(value->size() == 2)	{
					loadMedia(parseString(value->idx(1), ""),0,false);
				} else if(value->size() == 3)	{
					loadMedia(parseString(value->idx(1), ""),parseInt(value->idx(2), 0),false);
				} if(value->size() > 3)	{
					loadMedia(parseString(value->idx(1), ""),parseInt(value->idx(2), 0),parseBool(value->idx(3), false));
				}
			} else if(cmd == "show")	{
				if(value->size() == 2)	{
					showMedia(parseInt(value->idx(1), 0));
				} else {
					showMedia();
				}
			} else if(cmd == "hide")	{
				hideMedia();
			} else if(cmd == "fullscreen")	{
				fullscreenMedia();
			} else if(cmd == "centre")	{
				centreMedia();
			} else if(cmd == "size")	{
				if(value->size() > 2)	{
					sizeMedia(parseFloat(value->idx(1), -1), parseFloat(value->idx(2), -1));
				}
			} else if(cmd == "loc")	{
				if(value->size() > 2)	{
					locMedia(parseFloat(value->idx(1), 0), parseFloat(value->idx(2), 0));
				}
			} else if(cmd == "geom")	{
				if(value->size() > 4)	{
					geomMedia(parseFloat(value->idx(1), 0), parseFloat(value->idx(2), 0),parseFloat(value->idx(3), -1), parseFloat(value->idx(4), -1));
				}
			} else if(cmd == "layer")	{
				if(value->size() > 1)	{
					setLayerMedia(parseInt(value->idx(1), 0));
				}
			} else if(cmd == "remove")	{
				removeMedia();
			}
		}
	}
	
	void ImageObject::loadMedia(const string& filename, int layer, bool show)
	{
		if(!filename.empty() && !label.empty())	{
			Objsc::MediaPlayer::Current()->loadImage(filename, label);
			if(layer != 0)	{
				if(show)	{
					Objsc::MediaPlayer::Current()->show(label, layer);
				} else {
					Objsc::MediaPlayer::Current()->setLayer(label, layer);
				}
			}
		}
	}
	
	void ImageObject::removeMedia()
	{
		Objsc::MediaPlayer::Current()->remove(label);
	}
	
	void ImageObject::showMedia(int layer)
	{
		if(layer != 0)	{
			Objsc::MediaPlayer::Current()->show(label, layer);
		} else {
			Objsc::MediaPlayer::Current()->show(label);
		}
		send(1, new Objsc::LangInt(1));
	}
	
	void ImageObject::hideMedia()
	{
		Objsc::MediaPlayer::Current()->hide(label);
		send(1, new Objsc::LangInt(0));
	}
	
	void ImageObject::fullscreenMedia()
	{
		Objsc::MediaViewPtr media = Objsc::MediaPlayer::Current()->getMedia(label);
		if(media)	{
			media->fullscreen();
		}
	}
	
	void ImageObject::centreMedia()
	{
		Objsc::MediaViewPtr media = Objsc::MediaPlayer::Current()->getMedia(label);
		if(media)	{
			media->fullscreen();
		}
	}
	
	void ImageObject::sizeMedia(float width, float height)
	{
		if(width < 0 || height < 0)
			return;
		Objsc::MediaViewPtr media = Objsc::MediaPlayer::Current()->getMedia(label);
		if(media)	{
			media->setDimensions(width,height);
		}
	}
	
	void ImageObject::widthMedia(float width)
	{
		Objsc::MediaViewPtr media = Objsc::MediaPlayer::Current()->getMedia(label);
		if(media)	{
			media->setWidth(width);
		}
	}
	
	void ImageObject::heightMedia(float height)
	{
		Objsc::MediaViewPtr media = Objsc::MediaPlayer::Current()->getMedia(label);
		if(media)	{
			media->setHeight(height);
		}
	}
	
	void ImageObject::locXMedia(float x)
	{
		Objsc::MediaViewPtr media = Objsc::MediaPlayer::Current()->getMedia(label);
		if(media)	{
			media->setCentreX(x);
		}
	}
	
	void ImageObject::locYMedia(float y)
	{
		Objsc::MediaViewPtr media = Objsc::MediaPlayer::Current()->getMedia(label);
		if(media)	{
			media->setCentreY(y);
		}
	}
	
	void ImageObject::locMedia(float x, float y)
	{
		Objsc::MediaViewPtr media = Objsc::MediaPlayer::Current()->getMedia(label);
		if(media)	{
			media->setLocation(x,y);
		}
	}
	
	void ImageObject::geomMedia(float x, float y,float width, float height)
	{
		if(width < 0 || height < 0)
			return;
		Objsc::MediaViewPtr media = Objsc::MediaPlayer::Current()->getMedia(label);
		if(media)	{
			media->setBounds(x,y,width,height);
		}
	}
	
	void ImageObject::setLayerMedia(int layer)
	{
		Objsc::MediaPlayer::Current()->setLayer(label, layer);
	}
	
	void ImageObject::activate()
	{
		showMedia();
	}
	
	
	/////////////////////////////////////
	// ANIM OBJECT
	/////////////////////////////////////
	AnimObject::~AnimObject()
	{
		
	}
	
	void AnimObject::setLabel(string nlabel) 
	{ 
		label.assign(nlabel);
		displaystr = Objsc::AnimObject::langname + " " + label;
	}
	
	int AnimObject::loadAnimFiles(const string& sBasename)
	{
		//int found;
		basename = sBasename;
		frames.clear();
		currentFrame = 0;
		finder.open(Objsc::ProjectFiles::Current()->makeImagePath());
		if(finder.empty())	{
			return 0;
		}
		const Objsc::StringVector items  = finder.getFilenames();
		for(Objsc::StringVector::const_iterator item = items.begin(); item != items.end(); ++item)	{
			if(int((*item).find(basename)) == 0)	{
				frames.push_back((*item));
			}
		}
		if (frames.empty())	{
			return 0;
		} else {
			sort(frames.begin(),frames.end());
			return int(frames.size());
		}
	}
	
	int AnimObject::reloadAnimFiles()
	{
		if(basename.empty())	{
			return 0;
		}
		frames.clear();
		finder.open(Objsc::ProjectFiles::Current()->makeImagePath());
		if(finder.empty())	{
			return 0;
		}
		const Objsc::StringVector items  = finder.getFilenames();
		for(Objsc::StringVector::const_iterator item = items.begin(); item != items.end(); ++item)	{
			if(int((*item).find(basename)) == 0)	{
				frames.push_back((*item));
			}
		}
		if (frames.empty())	{
			return 0;
		} else {
			sort(frames.begin(),frames.end());
			return int(frames.size());
		}
	}
	
	void AnimObject::nextFrame()
	{
		if(++currentFrame >= totalFrames)	{
			currentFrame = 0;
		}
		//std::cout << "nextFrame " << totalFrames << " " << currentFrame << std::endl; 
		Objsc::MediaPlayer::Current()->loadImage(frames[currentFrame], label);
		send(0, new Objsc::LangInt(currentFrame));
	}
	
	void AnimObject::prevFrame()
	{
		if(--currentFrame < 0)	{
			currentFrame = totalFrames-1;
		}
		//std::cout << "nextFrame " << totalFrames << " " << currentFrame << std::endl; 
		Objsc::MediaPlayer::Current()->loadImage(frames[currentFrame], label);
		send(0, new Objsc::LangInt(currentFrame));
	}
	
	void AnimObject::firstFrame()
	{
		if(totalFrames ==0)
			return;
		currentFrame = 0;
		Objsc::MediaPlayer::Current()->loadImage(frames[currentFrame], label);
		send(0, new Objsc::LangInt(currentFrame));
	}
	
	void AnimObject::lastFrame()
	{
		if(totalFrames ==0)
			return;
		currentFrame = totalFrames-1;
		Objsc::MediaPlayer::Current()->loadImage(frames[currentFrame], label);
		send(0, new Objsc::LangInt(currentFrame));
	}
	
	void AnimObject::gotoFrame(int frameNum)
	{
		if(totalFrames ==0)
			return;
		if(frameNum > -1 or frameNum < totalFrames)	{
			currentFrame = frameNum;
			Objsc::MediaPlayer::Current()->loadImage(frames[currentFrame], label);
			send(0, new Objsc::LangInt(currentFrame));
		}
	}
	
	void AnimObject::loadMedia(const string& filename, int layer, bool show)
	{
		if(!filename.empty() && !label.empty())	{
			totalFrames = loadAnimFiles(filename);
			if(totalFrames > 0)	{
				Objsc::MediaPlayer::Current()->loadImage(frames[currentFrame], label);
				lyr = layer;
				if(layer != 0)	{
					if(show)	{
						Objsc::MediaPlayer::Current()->show(label, layer);
					} else {
						Objsc::MediaPlayer::Current()->setLayer(label, layer);
					}
				}
			}
		}
	}
	
	void AnimObject::receiveList(int inlet, LangList* value)
	{
		
		string cmd = parseString(value->idx(0), "");
		if(cmd.empty()) {
			if(parseInt(value->idx(0), -1) == 1)	{
				showMedia();
			} else if(parseInt(value->idx(0), -1) == 0)	{
				hideMedia();
			}
		} else {
			if(cmd == "load")	{
				if(value->size() == 2)	{
					loadMedia(parseString(value->idx(1), ""),0,false);
				} else if(value->size() == 3)	{
					loadMedia(parseString(value->idx(1), ""),parseInt(value->idx(2), 0),false);
				} if(value->size() > 3)	{
					loadMedia(parseString(value->idx(1), ""),parseInt(value->idx(2), 0),parseBool(value->idx(3), false));
				}
				send(2, new Objsc::LangInt(totalFrames));
			} else if(cmd == "show")	{
				if(value->size() == 2)	{
					showMedia(parseInt(value->idx(1), 0));
				} else {
					showMedia();
				}
			} else if(cmd == "hide")	{
				hideMedia();
			} else if(cmd == "fullscreen")	{
				fullscreenMedia();
				fs = true;
			} else if(cmd == "centre")	{
				centreMedia();
			} else if(cmd == "size")	{
				if(value->size() > 2)	{
					sizeMedia(parseFloat(value->idx(1), -1), parseFloat(value->idx(2), -1));
				}
			} else if(cmd == "loc")	{
				if(value->size() > 2)	{
					locMedia(parseFloat(value->idx(1), 0), parseFloat(value->idx(2), 0));
				}
			} else if(cmd == "geom")	{
				if(value->size() > 4)	{
					geomMedia(parseFloat(value->idx(1), 0), parseFloat(value->idx(2), 0),parseFloat(value->idx(3), -1), parseFloat(value->idx(4), -1));
				}
			} else if(cmd == "layer")	{
				if(value->size() > 1)	{
					setLayerMedia(parseInt(value->idx(1), 0));
					lyr = parseInt(value->idx(1), 0);
				}
			} else if(cmd == "remove")	{
				removeMedia();
			} else if(cmd == "reset")	{
				removeMedia();
			} else if(cmd == "reload")	{
				totalFrames = reloadAnimFiles();
				send(2, new Objsc::LangInt(totalFrames));
			} else if(cmd == "next")	{
				nextFrame();
				showMedia(lyr);
				if (fs)	{
					fullscreenMedia();
				}
			} else if(cmd == "prev")	{
				prevFrame();
				showMedia(lyr);
				if (fs)	{
					fullscreenMedia();
				}
			} else if(cmd == "go")	{
				if(value->size() == 2)	{
					gotoFrame(parseInt(value->idx(1), -1));
				}
				showMedia(lyr);
				if (fs)	{
					fullscreenMedia();
				}
			} else if(cmd == "first")	{
				firstFrame();
				showMedia(lyr);
				if (fs)	{
					fullscreenMedia();
				}
			} else if(cmd == "last")	{
				lastFrame();
				showMedia(lyr);
				if (fs)	{
					fullscreenMedia();
				}
			}
		}
	}
	
	void AnimObject::activate()
	{
		nextFrame();
		showMedia(lyr);
		if (fs)	{
			fullscreenMedia();
		}
	}
	
	/////////////////////////////////////
	// VIDEO OBJECT
	/////////////////////////////////////
	VideoObject::~VideoObject()
	{
		
	}
	
	void VideoObject::update(int timeElapsed)
	{
		if(media)	{
			//printf("VideoObject::update B %d\n", playing);
			if(playing) {
				//media->update();
				timeplayed += timeElapsed*0.001;
				//printf("timeplayed %f, duration: %f\n", timeplayed,duration);
				if(timeplayed >= duration)
					mediaEnded();
				//~ if(media->isFinished())
					//~ mediaEnded();
			}
		}
	}
	
	void VideoObject::mediaEnded()
	{
		stopMedia();
		send(0, new Objsc::LangBang());
	}
	
	void VideoObject::setLabel(string nlabel) 
	{ 
		label.assign(nlabel);
		displaystr = Objsc::VideoObject::langname + " " + label;
		//printf("VideoObject::setLabel <%s>\n", label.c_str());
	}
	
	void VideoObject::receive(int inlet, Objsc::LangAtom* atom)
	{
		switch(atom->getType())	{
			case LANG_NULL:
				break;
			case LANG_BANG:
				if(inlet == 0)	{
					showMedia();
					playMedia();
				}
				break;
			case LANG_INT:
				switch(inlet) {
					case 0:
						if(parseBool(atom))	{
							showMedia();
							playMedia();
						} else {
							stopMedia();
							hideMedia();
						}
						break;
					case 1:
						locXMedia(parseFloat(atom, 0.0f));
						break;
					case 2:
						locYMedia(parseFloat(atom, 0.0f));
						break;
					case 3:
						widthMedia(parseFloat(atom, -1.0f));
						break;
					case 4:
						heightMedia(parseFloat(atom, -1.0f));
						break;
				}
				break;
			case LANG_FLOAT:
				switch(inlet) {
					case 0:
						if(parseBool(atom))	{
							showMedia();
							playMedia();
						} else {
							stopMedia();
							hideMedia();
						}
						break;
					case 1:
						locXMedia(parseFloat(atom, 0.0f));
						break;
					case 2:
						locYMedia(parseFloat(atom, 0.0f));
						break;
					case 3:
						widthMedia(parseFloat(atom, -1.0f));
						break;
					case 4:
						heightMedia(parseFloat(atom, -1.0f));
						break;
					case 5:
						setSpeed(parseFloat(atom, 1.0f));
						break;
					case 6:
						//heightMedia(parseFloat(atom, -1.0f));
						break;
				}
				break;
			case LANG_STRING:
				break;
			case LANG_VAR:
				break;
			case LANG_LIST:
				switch(inlet) {
					case 0:
						receiveList(0, dynamic_cast<Objsc::LangList*>(atom));
						break;
					case 1:
						locXMedia(parseFloat(dynamic_cast<Objsc::LangList*>(atom)->idx(0), 0.0f));
						break;
					case 2:
						locYMedia(parseFloat(dynamic_cast<Objsc::LangList*>(atom)->idx(0), 0.0f));
						break;
					case 3:
						widthMedia(parseFloat(dynamic_cast<Objsc::LangList*>(atom)->idx(0), -1.0f));
						break;
					case 4:
						heightMedia(parseFloat(dynamic_cast<Objsc::LangList*>(atom)->idx(0), -1.0f));
						break;
				}
				break;
		}
	}
	
	void VideoObject::receiveList(int inlet, LangList* value)
	{
		
		string cmd = parseString(value->idx(0), "");
		if(cmd.empty()) {
			if(parseInt(value->idx(0), -1) == 1)	{
				showMedia();
			} else if(parseInt(value->idx(0), -1) == 0)	{
				hideMedia();
			}
		} else {
			if(cmd == "load")	{
				if(value->size() == 2)	{
					loadMedia(parseString(value->idx(1), ""),0,false);
				} else if(value->size() == 3)	{
					loadMedia(parseString(value->idx(1), ""),parseInt(value->idx(2), 0),false);
				} else if(value->size() == 4)	{
					loadMedia(parseString(value->idx(1), ""),parseInt(value->idx(2), 0),parseBool(value->idx(3), false));
					if(parseBool(value->idx(3), false))	{
						playMedia();
					}
				} else if(value->size() == 5)	{
					loadMedia(parseString(value->idx(1), ""),parseInt(value->idx(2), 0),parseBool(value->idx(3), false));
					if(parseBool(value->idx(3), false))	{
						playMedia(parseFloat(value->idx(4), 0.0f));
					}
				} else if(value->size() > 5)	{
					loadMedia(parseString(value->idx(1), ""),parseInt(value->idx(2), 0),parseBool(value->idx(3), false));
					if(parseBool(value->idx(3), false))	{
						playMedia(parseFloat(value->idx(4), 0.0f),parseFloat(value->idx(5), 0.0f));
					}
				}
			} else if(cmd == "show")	{
				if(value->size() == 2)	{
					showMedia(parseInt(value->idx(1), 0));
					playMedia();
				} else {
					playMedia();
					showMedia();
				}
			} else if(cmd == "hide")	{
				stopMedia();
				playing = false;
				timeplayed = 0.0f;
				hideMedia();
				//printf("============= HIDE %d\n", playing);
			} else if(cmd == "play")	{
				if(value->size() > 2)	{
					playMedia(parseFloat(value->idx(1), 0.0f),parseFloat(value->idx(2), 0.0f));
				} else if(value->size() == 2)	{
					playMedia(parseFloat(value->idx(1), 0.0f));
				} else {
					playMedia();
				}
			} else if(cmd == "stop")	{
				stopMedia();
			} else if(cmd == "fullscreen")	{
				fullscreenMedia();
			} else if(cmd == "centre")	{
				centreMedia();
			} else if(cmd == "size")	{
				if(value->size() > 2)	{
					sizeMedia(parseFloat(value->idx(1), -1), parseFloat(value->idx(2), -1));
				}
			} else if(cmd == "loc")	{
				if(value->size() > 2)	{
					locMedia(parseFloat(value->idx(1), 0), parseFloat(value->idx(2), 0));
				}
			} else if(cmd == "geom")	{
				if(value->size() > 4)	{
					geomMedia(parseFloat(value->idx(1), 0), parseFloat(value->idx(2), 0),parseFloat(value->idx(3), -1), parseFloat(value->idx(4), -1));
				}
			} else if(cmd == "layer")	{
				if(value->size() > 1)	{
					setLayerMedia(parseInt(value->idx(1), 0));
				}
			} else if(cmd == "speed")	{
				if(value->size() == 2)	{
					setSpeed(parseFloat(value->idx(1), 1.0f));
				}
			} else if(cmd == "vol")	{
				if(value->size() == 2)	{
					setVolume(parseFloat(value->idx(1), 0.0f));
				}
			} else if(cmd == "remove")	{
				removeMedia();
			}
		}
	}
	
	void VideoObject::loadMedia(const string& filename, int layer, bool show)
	{
		if(!filename.empty() && !label.empty())	{
			//media = Objsc::MediaPlayer::Current()->loadVideo(filename, label);
			media = std::tr1::static_pointer_cast<Objsc::VideoView>(Objsc::MediaPlayer::Current()->loadVideo(filename, label));
			if(layer != 0)	{
				if(show)	{
					Objsc::MediaPlayer::Current()->show(label, layer);
				} else {
					Objsc::MediaPlayer::Current()->setLayer(label, layer);
				}
			}
			duration = media->getDuration();
		}
	}
	
	void VideoObject::playMedia(float vol, float speed)
	{
		//media = Objsc::MediaPlayer::Current()->getMedia(label);
		printf("VideoObject::playMedia vol: %f speed: %f\n", vol, speed);
		if(media)	{
			printf("VideoObject::playMedia B\n");
			media->start();
			printf("VideoObject::playMedia C\n");
			if(vol !=0.0f)
				media->setVolume(vol);
			//~ if(speed !=0.0f)
				//~ media->setSpeed(speed);
			playing = true;
			timeplayed = 0.0f;
			send(1, new Objsc::LangInt(1));
		}
		printf("VideoObject::playMedia DONE\n");
	}
	
	void VideoObject::stopMedia()
	{
		//Objsc::MediaViewPtr media = Objsc::MediaPlayer::Current()->getMedia(label);
		if(media)	{
			media->stop();
			playing = false;
			timeplayed = 0.0f;
		}
		send(1, new Objsc::LangInt(0));
	}
	
	void VideoObject::hideMedia()
	{
		if(media)	{
			media->hide();
			media->stop();
			playing = false;
			timeplayed = 0.0f;
		}
	}
	
	void VideoObject::setVolume(float vol)
	{
		//Objsc::MediaViewPtr media = Objsc::MediaPlayer::Current()->getMedia(label);
		if(media)	{
			media->setVolume(vol);
		}
	}
	
	void VideoObject::setSpeed(float speed)
	{
		//Objsc::MediaViewPtr media = Objsc::MediaPlayer::Current()->getMedia(label);
		if(media)	{
			media->setSpeed(speed);
		}
	}
	
	void VideoObject::activate()
	{
		playMedia();
	}
	
	
	/////////////////////////////////////
	// TEXT OBJECT
	/////////////////////////////////////
	TextObject::~TextObject()
	{
		
	}
	
	void TextObject::setLabel(string nlabel) 
	{ 
		label.assign(nlabel);
		displaystr = Objsc::TextObject::langname + " " + label;
		Objsc::MediaPlayer::Current()->createText(label);
	}
	
	void TextObject::setTextColour(int r, int g, int b)
	{
		Objsc::MediaViewPtr media = Objsc::MediaPlayer::Current()->getMedia(label);
		if(media)	{
			media->setTextColour(r,g,b);
		}
	}
	
	void TextObject::setBackColour(int r, int g, int b)
	{
		Objsc::MediaViewPtr media = Objsc::MediaPlayer::Current()->getMedia(label);
		if(media)	{
			media->setBackColour(r,g,b);
		}
	}
	
	void TextObject::receive(int inlet, Objsc::LangAtom* atom)
	{
		switch(atom->getType())	{
			case LANG_NULL:
				break;
			case LANG_BANG:
				if(inlet == 0)	{
					showMedia();
				}
				break;
			case LANG_INT:
				switch(inlet) {
					case 0:
						if(parseBool(atom))	{
							showMedia();
						} else {
							hideMedia();
						}
						break;
					case 1:
						locXMedia(parseFloat(atom, 0.0f));
						break;
					case 2:
						locYMedia(parseFloat(atom, 0.0f));
						break;
					case 3:
						setText(intToString(parseInt(atom, 0.0f)));
						break;
				}
				break;
			case LANG_FLOAT:
				switch(inlet) {
					case 0:
						if(parseBool(atom))	{
							showMedia();
						} else {
							hideMedia();
						}
						break;
					case 1:
						locXMedia(parseFloat(atom, 0.0f));
						break;
					case 2:
						locYMedia(parseFloat(atom, 0.0f));
						break;
					case 3:
						setText(floatToString(parseFloat(atom, 0.0f)));
						break;
				}
				break;
			case LANG_STRING:
				switch(inlet) {
					case 3:
						setText(parseString(atom, ""));
						break;
				}
				break;
			case LANG_VAR:
				break;
			case LANG_LIST:
				switch(inlet) {
					case 0:
						receiveList(0, dynamic_cast<Objsc::LangList*>(atom));
						break;
					case 1:
						locXMedia(parseFloat(dynamic_cast<Objsc::LangList*>(atom)->idx(0), 0.0f));
						break;
					case 2:
						locYMedia(parseFloat(dynamic_cast<Objsc::LangList*>(atom)->idx(0), 0.0f));
						break;
					case 3:
						setText(dynamic_cast<Objsc::LangList*>(atom));
						break;
				}
				break;
		}
	}
	
	void TextObject::setText(LangList* value)
	{
		Objsc::MediaViewPtr media = Objsc::MediaPlayer::Current()->getMedia(label);
		if(media)	{
			media->setText(listToString(value));
			activate();
		}
	}
	
	void TextObject::setText(const string& value)
	{
		Objsc::MediaViewPtr media = Objsc::MediaPlayer::Current()->getMedia(label);
		if(media)	{
			media->setText(value);
			activate();
		}
	}
	
	void TextObject::clearText()
	{
		Objsc::MediaViewPtr media = Objsc::MediaPlayer::Current()->getMedia(label);
		if(media)	{
			media->clearText();
			activate();
		}
	}
	
	void TextObject::receiveList(int inlet, LangList* value)
	{
		
		string cmd = parseString(value->idx(0), "");
		if(cmd.empty()) {
			if(parseInt(value->idx(0), -1) == 1)	{
				showMedia();
			} else if(parseInt(value->idx(0), -1) == 0)	{
				hideMedia();
			}
		} else {
			if(cmd == "clear")	{
				clearText();
			} else if(cmd == "show")	{
				showMedia();
			} else if(cmd == "hide")	{
				hideMedia();
				//printf("============= HIDE %d\n", playing);
			} else if(cmd == "centre")	{
				centreMedia();
			//~ } else if(cmd == "size")	{
				//~ if(value->size() > 2)	{
					//~ sizeMedia(parseFloat(value->idx(1), -1), parseFloat(value->idx(2), -1));
				//~ }
			} else if(cmd == "loc")	{
				if(value->size() > 2)	{
					locMedia(parseFloat(value->idx(1), 0), parseFloat(value->idx(2), 0));
				}
			//~ } else if(cmd == "geom")	{
				//~ if(value->size() > 4)	{
					//~ geomMedia(parseFloat(value->idx(1), 0), parseFloat(value->idx(2), 0),parseFloat(value->idx(3), -1), parseFloat(value->idx(4), -1));
				//~ }
			} else if(cmd == "layer")	{
				if(value->size() > 1)	{
					setLayerMedia(parseInt(value->idx(1), 0));
				}
			} else if(cmd == "remove")	{
				removeMedia();
			} else if(cmd == "rgb")	{
				if(value->size() > 3)	{
					setTextColour(parseInt(value->idx(1), 255), parseInt(value->idx(2), 255), parseInt(value->idx(3), 255));
				}
			} else if(cmd == "bg")	{
				if(value->size() > 3)	{
					setBackColour(parseInt(value->idx(1), 0), parseInt(value->idx(2), 0), parseInt(value->idx(3), 0));
				}
			} else if(cmd == "bg_on")	{
				Objsc::MediaViewPtr media = Objsc::MediaPlayer::Current()->getMedia(label);
				if(media)	{
					media->showBG();
				}
			} else if(cmd == "bg_off")	{
				Objsc::MediaViewPtr media = Objsc::MediaPlayer::Current()->getMedia(label);
				if(media)	{
					media->hideBG();
				}
			} else if(cmd == "size")	{
				if(value->size() > 1)	{
					Objsc::MediaViewPtr media = Objsc::MediaPlayer::Current()->getMedia(label);
					if(media)	{
						media->setTextSize(parseInt(value->idx(1), 24));
					}
				}
			}
		}
	}
	
	
	void TextObject::activate()
	{
		send(0, new Objsc::LangBang());
	}
	
	
	void TextObject::update(int timeElapsed)
	{
		
	}
	
	void TextObject::locXMedia(float x)
	{
		Objsc::MediaViewPtr media = Objsc::MediaPlayer::Current()->getMedia(label);
		if(media)	{
			media->setX(x);
		}
	}
	
	void TextObject::locYMedia(float y)
	{
		Objsc::MediaViewPtr media = Objsc::MediaPlayer::Current()->getMedia(label);
		if(media)	{
			media->setY(y);
		}
	}
	
	/////////////////////////////////////
	// SOUND OBJECT
	/////////////////////////////////////
	SoundObject::SoundObject() :
				label(),
				sound()
	{
		setInlets(4);
		setOutlets(3);
		min_args = 1;
	}
	
	SoundObject::~SoundObject()
	{
		
	}
	
	
	void SoundObject::receive(int inlet, Objsc::LangAtom* atom)
	{
		switch(atom->getType())	{
			case LANG_BANG:
				if(inlet == 0)	{
					activate();
				}
				break;
			case LANG_INT:
				switch(inlet)	{
					case 0:
						if(parseBool(atom))	{
							start();
						} else {
							stop();
						}
						break;
				}
				break;
			case LANG_FLOAT:
				switch(inlet)	{
					case 0:
						if(parseBool(atom))	{
							start();
						} else {
							stop();
						}
						break;
					case 1:
						sound->setVolume(parseFloat(atom));
						break;
					case 2:
						sound->setSpeed(parseFloat(atom));
						break;
					case 3:
						sound->setPan(parseFloat(atom));
						break;
				}
				break;
			case LANG_LIST:
				receiveList(inlet, dynamic_cast<Objsc::LangList*>(atom));
				break;
		}
	}
	
	void SoundObject::receiveList(int inlet, LangList* value)
	{
		if(inlet == 0) {
			string cmd = parseString(value->idx(0), "");
			if(cmd.empty())	{
				parseInt(value->idx(0), 0)>0?start():stop();
			} else {
				if(cmd == "start")	{
					start();
				} else if(cmd == "stop")	{
					stop();
				} else if(cmd == "play")	{
					start();
				} else if(cmd == "amp")	{
					getAmp();
				} else if(cmd == "load")	{
					if(value->size() > 1)	{
						sound->loadSound(parseString(value->idx(1), ""));
					}
				} else if(cmd == "loops")	{
					if(value->size() > 1)	{
						sound->setLoops(parseInt(value->idx(1), 1));
					}
				} else if(cmd == "vol")	{
					if(value->size() > 1)	{
						sound->setVolume(parseFloat(value->idx(1), 0.0f));
					}
				} else if(cmd == "speed")	{
					if(value->size() > 1)	{
						sound->setSpeed(parseFloat(value->idx(1), 1.0f));
					}
				} else if(cmd == "pan")	{
					if(value->size() > 1)	{
						sound->setPan(parseFloat(value->idx(1), 0.5f));
					}
				} 
			}
		} else if(inlet == 1)	{
			sound->setVolume(parseFloat(value->idx(0), 0.0f));
		} else if(inlet == 2)	{
			sound->setSpeed(parseFloat(value->idx(0), 1.0f));
		} else if(inlet == 3)	{
			sound->setPan(parseFloat(value->idx(0), 0.5f));
		}
	}
	
	
	void SoundObject::start()
	{
		sound->start();
	}
	
	void SoundObject::stop()
	{
		sound->stop();
		//send(2, new Objsc::LangInt(0));
	}
	
	void SoundObject::activate()
	{
		start();
	}
	
	void SoundObject::getAmp()
	{
		send(0, new Objsc::LangFloat(sound->getAmp()));
	}
	
	bool SoundObject::parseArgs(Objsc::LangList* args)
	{
		if(args->size() < min_args)	{
			return false;
		} else {
			//setLabel(getItemAsString(args, 1));
			setLabel(getItemAsString(args, 0));
			return true;
		}
	}
	
	void SoundObject::setLabel(string nlabel) 
	{ 
		label.assign(nlabel);
		displaystr.assign(getLangName() + " " + label);
		sound = Objsc::SoundMixer::Current()->createSound(label, getLangName());
		sound->addListener(SoundListenerPtr(this));
	}
			
	
	void SoundObject::receiveSoundEvent(SoundEvent& event)
	{
		//printf("SoundObject::receiveSoundEvent %d\n", event.type);
		switch(event.type)	{
			case SNDSTART:
				send(1, new Objsc::LangBang());
				send(2, new Objsc::LangInt(1));
				break;
			case SNDENDED:
				send(2, new Objsc::LangInt(0));
				break;
			case SNDNEWLOOP:
				send(1, new Objsc::LangBang());
				break;
		}
	}
	
}

