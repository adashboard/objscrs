#include "objscrZoneViews.hpp"


// objscrs
#include "objscrGUIStyles.hpp"
#include "objscrStage.hpp"
#include "objscrGUIRoot.hpp"
#include "objscrZoneMapEditor.hpp"

// STL
#include <algorithm>


namespace Objsc
{
	
	////////////////////////////////////////
	// DECLARATIONS
	////////////////////////////////////////
	const string Objsc::ZoneView::stylename = "zoneviews";
	const string Objsc::CircleZoneView::stylename = "circlezoneviews";
	
	void initZoneViews()
	{
		Objsc::ZoneViewFactory* factory = Objsc::ZoneViewFactory::Current();
		factory->addCreator(Objsc::BasicZone::zonetype, new Objsc::BasicZoneViewCreator());
		factory->addCreator(Objsc::CircleZone::zonetype, new Objsc::CircleZoneViewCreator());
	}
	
	
	////////////////////////////////////////
	// ZONE VIEW
	////////////////////////////////////////
	ZoneView::ZoneView() : 
		zone(), 
		dodrag(false), 
		scrX(0), 
		scrY(0), 
		dragdirection(DRAG_NONE),
		dragnorth(),
		dragnortheast(),
		drageast(),
		dragsoutheast(),
		dragsouth(),
		dragsouthwest(),
		dragwest(),
		dragnorthwest(),
		namefield(),
		editmenu(),
		renaming(false)
	{
		Objsc::StylePalette::Current()->applyStyle(Objsc::ZoneView::stylename, this);
		Objsc::StylePalette::Current()->applyStyle(Objsc::ZoneView::stylename, &namefield);
		namefield.clearText();
		namefield.drawBackground(false);
		dragnorth.setDimensions(Objsc::HANDLESIZE,Objsc::HANDLESIZE);
		dragnortheast.setDimensions(Objsc::HANDLESIZE,Objsc::HANDLESIZE);
		drageast.setDimensions(Objsc::HANDLESIZE,Objsc::HANDLESIZE);
		dragsoutheast.setDimensions(Objsc::HANDLESIZE,Objsc::HANDLESIZE);
		dragsouth.setDimensions(Objsc::HANDLESIZE,Objsc::HANDLESIZE);
		dragsouthwest.setDimensions(Objsc::HANDLESIZE,Objsc::HANDLESIZE);
		dragwest.setDimensions(Objsc::HANDLESIZE,Objsc::HANDLESIZE);
		dragnorthwest.setDimensions(Objsc::HANDLESIZE,Objsc::HANDLESIZE);
		resetHandles();
	}
	
	void ZoneView::setZone(Objsc::ZonePtr zonePtr) 
	{
		zone=zonePtr;
		namefield.setText(zone->getName());
		
		mapToScreen();
	}
	
	void ZoneView::mapToScreen()
	{
		if(zone)	{
			int x=0;
			int y=0;
			int w=0;
			int h=0;
			const Objsc::Rect zrect = zone->getBounds();
			Objsc::Stage::Current()->stageToScreen(zrect.getX(), zrect.getY(), &x,&y);
			Objsc::Stage::Current()->stageToScreen(zrect.getWidth(), zrect.getHeight(),&w,&h);
			bounds.set(x,y,w,h);
			namefield.setLocation(bounds.getX(), bounds.getY());
			resetHandles();
		}
	}
	
	
	void ZoneView::mapToStage()
	{
		if(zone)	{
			float x=0.0f;
			float y=0.0f;
			float w=0.0f;
			float h=0.0f;
			Objsc::Stage::Current()->screenToStage(bounds.getX(), bounds.getY(), &x,&y);
			Objsc::Stage::Current()->screenToStage(bounds.getWidth(), bounds.getHeight(),&w,&h);
			zone->setBounds(x,y,w,h);
		}
	}
	
	void ZoneView::startRename()
	{
		renaming = true;
	}
	
	
	void ZoneView::endRename()
	{
		renaming = false;
	}
	
	
	void ZoneView::setLocation(float x, float y)
	{
		bounds.setLocation(x,y);
		boundsResized();
	}
	
	void ZoneView::setDimensions(float width, float height)
	{
		
	}
	
	void ZoneView::setBounds(float x, float y, float width, float height)
	{
		
	}
	
	void ZoneView::boundsResized()
	{
		mapToStage();
		resetHandles();
		namefield.setLocation(bounds.getX(), bounds.getY());	
	}
	
	void ZoneView::resetHandles()
	{
		dragnorth.setLocation(bounds.getCentreX()-(Objsc::HANDLESIZE/2),bounds.getY());
		dragnortheast.setLocation(bounds.getRight()-dragnortheast.getWidth(),bounds.getY());
		drageast.setLocation(bounds.getRight()-drageast.getWidth(),bounds.getCentreY()-(Objsc::HANDLESIZE/2));
		dragsoutheast.setLocation(bounds.getRight()-dragnortheast.getWidth(),bounds.getBottom()-dragsoutheast.getHeight());
		dragsouth.setLocation(bounds.getCentreX()-(Objsc::HANDLESIZE/2),bounds.getBottom()-dragsouth.getHeight());
		dragsouthwest.setLocation(bounds.getX(),bounds.getBottom()-dragsouthwest.getHeight());
		dragwest.setLocation(bounds.getX(),bounds.getCentreY()-(Objsc::HANDLESIZE/2));
		dragnorthwest.setLocation(bounds.getX(),bounds.getY());
	}
	
	void ZoneView::update()
	{
		
	}
	
	void ZoneView::draw()
	{
		if (visible)	{
			if(hasFocus)	{
				ofSetColor(bgColour.r, bgColour.g, bgColour.b, 30);
				ofRect(bounds.getX(), bounds.getY(), bounds.getWidth(), bounds.getHeight());
				ofSetColor(bgColour.r, bgColour.g, bgColour.b);
				ofNoFill();
				ofRect(dragnorth.getX(), dragnorth.getY(),Objsc::HANDLESIZE,Objsc::HANDLESIZE);
				ofRect(dragnortheast.getX(), dragnortheast.getY(),Objsc::HANDLESIZE,Objsc::HANDLESIZE);
				ofRect(drageast.getX(), drageast.getY(),Objsc::HANDLESIZE,Objsc::HANDLESIZE);
				ofRect(dragsoutheast.getX(), dragsoutheast.getY(),Objsc::HANDLESIZE,Objsc::HANDLESIZE);
				ofRect(dragsouth.getX(), dragsouth.getY(),Objsc::HANDLESIZE,Objsc::HANDLESIZE);
				ofRect(dragsouthwest.getX(), dragsouthwest.getY(),Objsc::HANDLESIZE,Objsc::HANDLESIZE);
				ofRect(dragwest.getX(), dragwest.getY(),Objsc::HANDLESIZE,Objsc::HANDLESIZE);
				ofRect(dragnorthwest.getX(), dragnorthwest.getY(),Objsc::HANDLESIZE,Objsc::HANDLESIZE);
				ofFill();
				switch(dragdirection) {
					case DRAG_NORTH:
						ofRect(dragnorth.getX(), dragnorth.getY(),Objsc::HANDLESIZE,Objsc::HANDLESIZE);
						break;
					case DRAG_NORTHEAST:
						ofRect(dragnortheast.getX(), dragnortheast.getY(),Objsc::HANDLESIZE,Objsc::HANDLESIZE);
						break;
					case DRAG_EAST:
						ofRect(drageast.getX(), drageast.getY(),Objsc::HANDLESIZE,Objsc::HANDLESIZE);
						break;
					case DRAG_SOUTHEAST:
						ofRect(dragsoutheast.getX(), dragsoutheast.getY(),Objsc::HANDLESIZE,Objsc::HANDLESIZE);
						break;
					case DRAG_SOUTH:
						ofRect(dragsouth.getX(), dragsouth.getY(),Objsc::HANDLESIZE,Objsc::HANDLESIZE);
						break;
					case DRAG_SOUTHWEST:
						ofRect(dragsouthwest.getX(), dragsouthwest.getY(),Objsc::HANDLESIZE,Objsc::HANDLESIZE);
						break;
					case DRAG_WEST:
						ofRect(dragwest.getX(), dragwest.getY(),Objsc::HANDLESIZE,Objsc::HANDLESIZE);
						break;
					case DRAG_NORTHWEST:
						ofRect(dragnorthwest.getX(), dragnorthwest.getY(),Objsc::HANDLESIZE,Objsc::HANDLESIZE);
						break;
				}
			}
			namefield.draw();
			ofSetColor(bgColour.r, bgColour.g, bgColour.b);
			outlineBox(bounds);
		}
	}
	
	
	void ZoneView::keyPressed(int key)
	{
		if(renaming)	{
			namefield.keyPressed(key);
		}
	}
	
	void ZoneView::keyReleased(int key)
	{
		if(renaming)	{
			namefield.keyReleased(key);
		}
	}
	
	void ZoneView::mouseMoved(int x, int y )
	{
		if(withinBounds(x,y))	{
			if(!hasFocus) {
				gainFocus();
			}
			if(dragnorth.containsPoint(x,y))	{
				dragdirection=DRAG_NORTH;
			} else if(dragnortheast.containsPoint(x,y))	{
				dragdirection=DRAG_NORTHEAST;
			} else if(drageast.containsPoint(x,y))	{
				dragdirection=DRAG_EAST;
			} else if(dragsoutheast.containsPoint(x,y))	{
				dragdirection=DRAG_SOUTHEAST;
			} else if(dragsouth.containsPoint(x,y))	{
				dragdirection=DRAG_SOUTH;
			} else if(dragsouthwest.containsPoint(x,y))	{
				dragdirection=DRAG_SOUTHWEST;
			} else if(dragwest.containsPoint(x,y))	{
				dragdirection=DRAG_WEST;
			} else if(dragnorthwest.containsPoint(x,y))	{
				dragdirection=DRAG_NORTHWEST;
			} else {
				dragdirection=DRAG_NONE;
				if(renaming)	{
					namefield.mouseMoved(x,y);
				}		
			}	
		} else {
			if(hasFocus) {
				looseFocus();
				if(dodrag)
					dodrag=false;
				dragdirection=DRAG_NONE;
			}
		}
	}
	
	void ZoneView::mouseDragged(int x, int y, int button)
	{
		consumed = false;
		float dX = x-scrX;
		float dY = y-scrY;
		if(dodrag)	{
			bounds.moveCentre(dX,dY);
			boundsResized();
			consumed = true;
		} else {
			switch(dragdirection) {
				case DRAG_NORTH:
					bounds.stretchSides(dY,0.0f,0.0f,0.0f);
					boundsResized();
					consumed = true;
					break;
				case DRAG_NORTHEAST:
					bounds.stretchSides(dY,dX,0.0f,0.0f);
					boundsResized();
					consumed = true;
					break;
				case DRAG_EAST:
					bounds.stretchSides(0.0f,dX,0.0f,0.0f);
					boundsResized();
					consumed = true;
					break;
				case DRAG_SOUTHEAST:
					bounds.stretchSides(0.0f,dX,dY,0.0f);
					boundsResized();
					consumed = true;
					break;
				case DRAG_SOUTH:
					bounds.stretchSides(0.0f,0.0f,dY,0.0f);
					boundsResized();
					consumed = true;
					break;
				case DRAG_SOUTHWEST:
					bounds.stretchSides(0.0f,0.0f,dY,dX);
					boundsResized();
					consumed = true;
					break;
				case DRAG_WEST:
					bounds.stretchSides(0.0f,0.0f,0.0f,dX);
					boundsResized();
					consumed = true;
					break;
				case DRAG_NORTHWEST:
					bounds.stretchSides(dY,0.0f,0.0f,dX);
					boundsResized();
					consumed = true;
					break;
			}
		}
		scrX = x;
		scrY = y;
	}
	
	void ZoneView::mousePressed(int x, int y, int button)
	{
		consumed = false;
		if(withinBounds(x,y))	{
			scrX = x;
			scrY = y;
			switch(button)	{
				case 0:
					if(dragnorth.containsPoint(x,y))	{
						dragdirection=DRAG_NORTH;
					} else if(dragnortheast.containsPoint(x,y))	{
						dragdirection=DRAG_NORTHEAST;
					} else if(drageast.containsPoint(x,y))	{
						dragdirection=DRAG_EAST;
					} else if(dragsoutheast.containsPoint(x,y))	{
						dragdirection=DRAG_SOUTHEAST;
					} else if(dragsouth.containsPoint(x,y))	{
						dragdirection=DRAG_SOUTH;
					} else if(dragsouthwest.containsPoint(x,y))	{
						dragdirection=DRAG_SOUTHWEST;
					} else if(dragwest.containsPoint(x,y))	{
						dragdirection=DRAG_WEST;
					} else if(dragnorthwest.containsPoint(x,y))	{
						dragdirection=DRAG_NORTHWEST;
					} else {
						dodrag = true;
						dragdirection=DRAG_NONE;
					}
					consumed = true;
					break;
				case 1:
					break;
				case 2:
					consumed = true;
					openMenu(x, y);
					break;
			}
		}
	}
	
	void ZoneView::mouseReleased(int x, int y, int button)
	{
		if(dodrag) {
			dodrag=false;
		}
		if(dragdirection!=DRAG_NONE) {
			dragdirection=DRAG_NONE;
		}
	}
	
	void ZoneView::windowResized(int w, int h)
	{
		
	}
	
	void ZoneView::openMenu(int x, int y)
	{
		if(!editmenu)	{
			editmenu = Objsc::ContextMenu::create();
			Objsc::CommandPtr actionPatch = Objsc::CommandDispatcher::createCommand(Objsc::ZoneMapEditor::cmdlabel , "zone_patch");
			actionPatch->getData()->addArg(Objsc::ZoneViewPtr(this));
			editmenu->addItem("patch", actionPatch);
			/*Objsc::CommandPtr actionRename = Objsc::CommandDispatcher::createCommand(Objsc::ZoneMapEditor::cmdlabel , "zone_rename");
			actionRename->getData()->addArg(Objsc::ZoneViewPtr(this));
			editmenu->addItem("rename", actionRename);*/
			Objsc::CommandPtr actionDelete = Objsc::CommandDispatcher::createCommand(Objsc::ZoneMapEditor::cmdlabel , "zone_delete");
			actionDelete->getData()->addArg(Objsc::ZoneViewPtr(this));
			editmenu->addItem("delete", actionDelete);
		}
		if(hasFocus) {
			looseFocus();
		}
		Objsc::GUIRoot::Current()->openRootMenu(editmenu, x, y);
	}
	
	
	////////////////////////////////////////
	// CIRCLE ZONE VIEW
	////////////////////////////////////////
	CircleZoneView::CircleZoneView()
	{
		Objsc::StylePalette::Current()->applyStyle(Objsc::CircleZoneView::stylename, this);
		Objsc::StylePalette::Current()->applyStyle(Objsc::CircleZoneView::stylename, &namefield);
	}
	
	void CircleZoneView::mapToScreen()
	{
		if(zone)	{
			int x=0;
			int y=0;
			int w=0;
			int h=0;
			const Objsc::Rect zrect = zone->getBounds();
			Objsc::Stage::Current()->stageToScreen(zrect.getX(), zrect.getY(), &x,&y);
			Objsc::Stage::Current()->stageToScreen(zrect.getWidth(), zrect.getHeight(),&w,&h);
			bounds.set(x,y,w,h);
			circle.set(bounds.getCentreX(),bounds.getCentreY(),w/2.0f);
			namefield.setLocation(bounds.getX(), bounds.getY());
			resetHandles();
		}
	}
	
	void CircleZoneView::mapToStage()
	{
		if(zone)	{
			float x=0.0f;
			float y=0.0f;
			float w=0.0f;
			float h=0.0f;
			Objsc::Stage::Current()->screenToStage(bounds.getX(), bounds.getY(), &x,&y);
			Objsc::Stage::Current()->screenToStage(bounds.getWidth(), bounds.getHeight(),&w,&h);
			zone->setBounds(x,y,w,h);
			mapToScreen();
		}
	}
	
	
	void CircleZoneView::draw()
	{
		if (visible)	{
			if(hasFocus)	{
				ofFill();
				ofSetColor(bgColour.r, bgColour.g, bgColour.b, 30);
				ofEllipse(bounds.getCentreX(), bounds.getCentreY(), bounds.getWidth(), bounds.getHeight());
				ofSetColor(bgColour.r, bgColour.g, bgColour.b);
				ofNoFill();
				ofRect(dragnorth.getX(), dragnorth.getY(),Objsc::HANDLESIZE,Objsc::HANDLESIZE);
				ofRect(dragnortheast.getX(), dragnortheast.getY(),Objsc::HANDLESIZE,Objsc::HANDLESIZE);
				ofRect(drageast.getX(), drageast.getY(),Objsc::HANDLESIZE,Objsc::HANDLESIZE);
				ofRect(dragsoutheast.getX(), dragsoutheast.getY(),Objsc::HANDLESIZE,Objsc::HANDLESIZE);
				ofRect(dragsouth.getX(), dragsouth.getY(),Objsc::HANDLESIZE,Objsc::HANDLESIZE);
				ofRect(dragsouthwest.getX(), dragsouthwest.getY(),Objsc::HANDLESIZE,Objsc::HANDLESIZE);
				ofRect(dragwest.getX(), dragwest.getY(),Objsc::HANDLESIZE,Objsc::HANDLESIZE);
				ofRect(dragnorthwest.getX(), dragnorthwest.getY(),Objsc::HANDLESIZE,Objsc::HANDLESIZE);
				ofFill();
				switch(dragdirection) {
					case DRAG_NORTH:
						ofRect(dragnorth.getX(), dragnorth.getY(),Objsc::HANDLESIZE,Objsc::HANDLESIZE);
						break;
					case DRAG_NORTHEAST:
						ofRect(dragnortheast.getX(), dragnortheast.getY(),Objsc::HANDLESIZE,Objsc::HANDLESIZE);
						break;
					case DRAG_EAST:
						ofRect(drageast.getX(), drageast.getY(),Objsc::HANDLESIZE,Objsc::HANDLESIZE);
						break;
					case DRAG_SOUTHEAST:
						ofRect(dragsoutheast.getX(), dragsoutheast.getY(),Objsc::HANDLESIZE,Objsc::HANDLESIZE);
						break;
					case DRAG_SOUTH:
						ofRect(dragsouth.getX(), dragsouth.getY(),Objsc::HANDLESIZE,Objsc::HANDLESIZE);
						break;
					case DRAG_SOUTHWEST:
						ofRect(dragsouthwest.getX(), dragsouthwest.getY(),Objsc::HANDLESIZE,Objsc::HANDLESIZE);
						break;
					case DRAG_WEST:
						ofRect(dragwest.getX(), dragwest.getY(),Objsc::HANDLESIZE,Objsc::HANDLESIZE);
						break;
					case DRAG_NORTHWEST:
						ofRect(dragnorthwest.getX(), dragnorthwest.getY(),Objsc::HANDLESIZE,Objsc::HANDLESIZE);
						break;
				}
			}
			namefield.draw();
			ofSetColor(bgColour.r, bgColour.g, bgColour.b);
			ofNoFill();
			ofEllipse(bounds.getCentreX(), bounds.getCentreY(), bounds.getWidth(), bounds.getHeight());
			ofCircle(bounds.getCentreX(), bounds.getCentreY(), 2.0f);
			ofFill();
		}
	}
	
	
	////////////////////////////////////////
	// ZONE MAP
	////////////////////////////////////////
	ZoneMapView::ZoneMapView() : zonemap(), zoneviews() //, namefield()
	{
		Objsc::StylePalette::Current()->applyStyle(Objsc::ZoneView::stylename, this);
	}
	
	void ZoneMapView::setZoneMap(Objsc::ZoneMapPtr zonemapPtr) 
	{
		zonemap=zonemapPtr;
		clearViews();
		if(zonemap->hasZones())	{
			const Objsc::ZonePtrList zones = zonemap->getZones();
			for(Objsc::ZonePtrList::const_iterator zone = zones.begin(); zone != zones.end(); ++zone)	{ 
				zoneviews.push_back(Objsc::ZoneViewFactory::Current()->create((*zone)->getZoneType()));
				zoneviews.back()->setZone((*zone));
			}
		}
	}
	
	void ZoneMapView::update()
	{
		
	}
	
	void ZoneMapView::draw()
	{
		if (visible)	{
			for(ZoneViewVec::iterator view = zoneviews.begin(); view != zoneviews.end(); ++view)	{
				(*view)->draw();
			}
		}
	}
	
	void ZoneMapView::setLocation(float x, float y)
	{
		bounds.setLocation(x,y);
	}
	
	void ZoneMapView::setDimensions(float width, float height)
	{
		bounds.setDimensions(width,height);
	}
	
	void ZoneMapView::setBounds(float x, float y, float width, float height)
	{
		bounds.set(x,y,width,height);
	}
	
	
	
	void ZoneMapView::keyPressed(int key)
	{
		
	}
	
	void ZoneMapView::keyReleased(int key)
	{
		
	}
	
	void ZoneMapView::mouseMoved(int x, int y )
	{
		for(ZoneViewVec::iterator view = zoneviews.begin(); view != zoneviews.end(); ++view)	{
			(*view)->mouseMoved(x,y);
		}
	}
	
	void ZoneMapView::mouseDragged(int x, int y, int button)
	{
		consumed = false;
		if(withinBounds(x,y))	{
			for(ZoneViewVec::iterator view = zoneviews.begin(); view != zoneviews.end(); ++view)	{
				(*view)->mouseDragged(x,y,button);
				if((*view)->eventConsumed())	{
					consumed = true;
					return;
				}
			}
		}
	}
	
	void ZoneMapView::mousePressed(int x, int y, int button)
	{
		consumed = false;
		if(withinBounds(x,y))	{
			for(ZoneViewVec::iterator view = zoneviews.begin(); view != zoneviews.end(); ++view)	{
				(*view)->mousePressed(x,y,button);
				if((*view)->eventConsumed())	{
					consumed = true;
					return;
				}
			}
		}
	}
	
	void ZoneMapView::mouseReleased(int x, int y, int button)
	{
		for(ZoneViewVec::iterator view = zoneviews.begin(); view != zoneviews.end(); ++view)	{
			(*view)->mouseReleased(x,y,button);
		}
	}
	
	void ZoneMapView::windowResized(int w, int h)
	{
		
	}
	
	void ZoneMapView::createZone(float scrX, float scrY, const string& zonetype)
	{
		float zX = float(0.0f);
		float zY = float(0.0f);
		Objsc::Stage::Current()->screenToStage(scrX, scrY, &zX, &zY);
		Objsc::ZonePtr zone = zonemap->createZone(zonetype);
		zone->setLocation(zX,zY);
		zoneviews.push_back(Objsc::ZoneViewFactory::Current()->create(zonetype));
		zoneviews.back()->setZone(zone);
	}
	
	void ZoneMapView::removeZone(Objsc::ZoneViewPtr zoneview)
	{
		zonemap->removeZone(zoneview->getZone());
		zoneviews.erase(find(zoneviews.begin(),zoneviews.end(), zoneview));
	}
	
	void ZoneMapView::clearViews()
	{
		zoneviews.clear();
	}
	
	
}

