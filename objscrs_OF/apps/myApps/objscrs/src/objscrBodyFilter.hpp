#ifndef  OBJSCR_BODYFILTER_H
#define OBJSCR_BODYFILTER_H


// objscrs
#include "objscrShapeFilter.hpp"
#include "objscrBody.hpp"

// STL
#include <vector>
using std::vector;

// shared_ptr
#include <tr1/memory>

/*
- reads shapes
- compares against bodies
- creates body where necessary
- updates existign bodies
*/

namespace Objsc
{
	
	typedef vector < int > ShapeNumVec;
	
	////////////////////////////////////////
	// BODY FILTER
	////////////////////////////////////////
	class BodyFilter
	{
		protected:
			Objsc::ShapeFilterPtr 	shapes;
			Objsc::BodyPtrList		bodies;
			Objsc::ShapeNumVec	matchedShapes;
		
			void process();
			void createBody(const ofxCvBlob& shape);
			void cull();
			void prune();
		
		public:
			BodyFilter() : shapes(), 
					bodies(), 
					matchedShapes(){}
			virtual ~BodyFilter() {}
				
			void setShapeFilter(Objsc::ShapeFilterPtr shapeFilterPtr) {shapes=shapeFilterPtr;}
			void update();
			
			bool hasBodies() const { return !bodies.empty();}
			const Objsc::BodyPtrIter& beginBodies() { return bodies.begin(); }
			const Objsc::BodyPtrIter& endBodies() { return bodies.end(); }
			const Objsc::BodyPtrList& getBodies() const { return bodies; }
			
			void clear();
			
			static std::tr1::shared_ptr<Objsc::BodyFilter> create()
			{
				return std::tr1::shared_ptr<Objsc::BodyFilter>(new BodyFilter());
			}
			
			
	};
	
	typedef std::tr1::shared_ptr<Objsc::BodyFilter> BodyFilterPtr;
}

#endif // OBJSCR_BODYFILTER_H
