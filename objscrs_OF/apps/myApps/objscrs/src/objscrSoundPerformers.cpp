#include "objscrSoundPerformers.hpp"

// OF
#include "ofMain.h"


namespace Objsc
{
	
	/////////////////////////////////////
	// DECLARATIONS
	/////////////////////////////////////
	const string Objsc::SingleNote::soundname = "snd";
	const string Objsc::RandomNote::soundname = "rndsnd";
	const string Objsc::RandomSequence::soundname = "rndseq";
	const string Objsc::Cloud::soundname = "cloudsnd";
	const string Objsc::Sequencer::soundname = "seqsnd";
	
	
	/////////////////////////////////////
	// FUNCTIONS
	/////////////////////////////////////
	void initSoundPerformers()
	{
		Objsc::SoundPerformerFactory* factory = Objsc::SoundPerformerFactory::Current();
		factory->addCreator(Objsc::SingleNote::soundname, new Objsc::SingleNoteCreator());
		factory->addCreator(Objsc::RandomNote::soundname, new Objsc::RandomNoteCreator());
		factory->addCreator(Objsc::RandomSequence::soundname, new Objsc::RandomSequenceCreator());
		factory->addCreator(Objsc::Cloud::soundname, new Objsc::CloudCreator());
		factory->addCreator(Objsc::Sequencer::soundname, new Objsc::SequencerCreator());
	}
	
	
	////////////////////////////////////////
	// SOUND PERFORMER
	////////////////////////////////////////
	SoundPerformer::SoundPerformer() :
		active(false),
		listeners(),
		speed(1.0f),
		vol(0.0f),
		panL(0.5f),
		panR(0.5f),
		loops(1),
		loopcount(0)
	{
		
	}
	SoundPerformer::~SoundPerformer()
	{
		listeners.clear();
	}
	void SoundPerformer::addListener(Objsc::SoundListenerPtr listener)
	{
		listeners.push_back(listener);
	}
	
	void SoundPerformer::updateListeners(SoundEvent& event)
	{
		for(Objsc::SoundListenerVec::iterator lstnr = listeners.begin(); lstnr != listeners.end(); ++lstnr)	{
			(*lstnr)->receiveSoundEvent(event);
		}
	}
	
	////////////////////////////////////////
	// SINGLE NOTE
	////////////////////////////////////////
	SingleNote::SingleNote() :
		sound()
	{
		
	}
	
	SingleNote::~SingleNote()
	{
		sound.reset();
	}

	void SingleNote::receiveSoundEvent(SoundEvent& event)
	{
		//printf("SingleNote::receiveSoundEvent %d\n", event.type);
		switch(event.type)	{
			case SNDSTART:
				break;
			case SNDENDED:
				stop();
				break;
			case SNDNEWLOOP:
				break;
		}
		updateListeners(event);
	}

	void SingleNote::start()
	{
		active=true; 
		sound->play();
	}
	
	void SingleNote::update(int timeframe)
	{
		if(active && sound)	{
			sound->update();
		}
	}
	
	void SingleNote::loadSound(const string& filename)
	{
		printf("SingleNote::loadSound <%s> A\n", filename.c_str());
		if(!filename.empty())	{
			//printf("SingleNote::loadSound <%s> B\n", filename.c_str());
			sound = Objsc::SoundPlayer::create();
			sound->loadSound(filename);
			sound->setListener(SoundListenerPtr(this));
			sound->setVolume(0.5f);
			sound->setPan(0.5f);
		}
	}
	
	void SingleNote::setSpeed(float value)
	{
		speed=value;
		if(sound)
			sound->setSpeed(speed);
	}
	void SingleNote::setPan(float value)
	{
		panL = 1.0-value;
		panR = value;
		if(sound)
			sound->setPan(speed);
	}
	void SingleNote::setVolume(float value)
	{
		vol=value;
		if(sound)
			sound->setVolume(vol);
	}
	void SingleNote::setLoops(int value)
	{
		loops = value;
		if(sound)
			sound->setLoops(loops);
	}
	
	////////////////////////////////////////
	// RANDOM NOTE
	////////////////////////////////////////
	RandomNote::RandomNote() :
		sounds(),
		sound()
	{
		
	}
	
	RandomNote::~RandomNote()
	{
		sound.reset();
		sounds.clear();
	}

	void RandomNote::receiveSoundEvent(SoundEvent& event)
	{
		//printf("RandomNote::receiveSoundEvent %d\n", event.type);
		switch(event.type)	{
			case SNDSTART:
				break;
			case SNDENDED:
				stop();
				break;
			case SNDNEWLOOP:
				break;
		}
		updateListeners(event);
	}

	void RandomNote::start()
	{
		if(!sounds.empty())	{
			if(sound)	{
				sound->cut();
			}
			active=true; 
			sound = sounds[(int)ofRandom(0, sounds.size())];
			sound->setVolume(vol);
			sound->setPan(panR);
			sound->setLoops(loops);
			sound->play();
		}
	}
	
	void RandomNote::update(int timeframe)
	{
		//printf("RandomNote::update A\n");
		if(active && sound)	{
			//printf("RandomNote::update B\n");
			sound->update();
		}
	}
	
	void RandomNote::loadSound(const string& filename)
	{
		//printf("RandomNote::loadSound <%s> A\n", filename.c_str());
		if(!filename.empty())	{
			//printf("RandomNote::loadSound <%s> B\n", filename.c_str());
			sounds.push_back(Objsc::SoundPlayer::create());
			sounds.back()->loadSound(filename);
			sounds.back()->setListener(SoundListenerPtr(this));
			sounds.back()->setVolume(0.5f);
			sounds.back()->setPan(0.5f);
		}
	}
	
	
	void RandomNote::setSpeed(float value)
	{
		speed=value;
		//~ if(sound)
			//~ sound->setSpeed(speed);
		for(Objsc::SoundPlayerVec::iterator snd = sounds.begin(); snd != sounds.end(); ++snd)	{
			(*snd)->setSpeed(speed);
		}
	}
	void RandomNote::setPan(float value)
	{
		panL = 1.0-value;
		panR = value;
		//~ if(sound)
			//~ sound->setPan(speed);
		for(Objsc::SoundPlayerVec::iterator snd = sounds.begin(); snd != sounds.end(); ++snd)	{
			(*snd)->setPan(value);
		}
	}
	void RandomNote::setVolume(float value)
	{
		vol=value;
		//~ if(sound)
			//~ sound->setVolume(vol);
		for(Objsc::SoundPlayerVec::iterator snd = sounds.begin(); snd != sounds.end(); ++snd)	{
			(*snd)->setVolume(vol);
		}
	}
	void RandomNote::setLoops(int value)
	{
		loops = value;
		//~ if(sound)
			//~ sound->setLoops(loops);
		for(Objsc::SoundPlayerVec::iterator snd = sounds.begin(); snd != sounds.end(); ++snd)	{
			(*snd)->setLoops(loops);
		}
	}

	////////////////////////////////////////
	// RANDOM SEQUENCE
	////////////////////////////////////////
	RandomSequence::RandomSequence() :
		sounds(),
		sound(),
		seq(),
		count(0)
	{
		
	}
	
	RandomSequence::~RandomSequence()
	{
		sound.reset();
		sounds.clear();
		seq.clear();
	}

	void RandomSequence::receiveSoundEvent(SoundEvent& event)
	{
		//printf("RandomSequence::receiveSoundEvent %d\n", event.type);
		switch(event.type)	{
			case SNDSTART:
				break;
			case SNDENDED:
				start();
				break;
			case SNDNEWLOOP:
				break;
		}
	}

	void RandomSequence::start()
	{
		if(!sounds.empty())	{
			//printf("RandomSequence::start %d\n", count);
			//~ if(sound)	{
				//~ sound->cut();
			//~ }
			active=true; 
			sound = sounds[seq[count++]];
			sound->play();
			if(count == seq.size())	{
				shuffle();
			}
		} else {
			stop();
		}
	}
	
	void RandomSequence::update(int timeframe)
	{
		if(active && sound)	{
			sound->update();
		}
	}
	
	void RandomSequence::loadSound(const string& filename)
	{
		//printf("RandomSequence::loadSound <%s> A\n", filename.c_str());
		if(!filename.empty())	{
			//printf("RandomSequence::loadSound <%s> B\n", filename.c_str());
			sounds.push_back(Objsc::SoundPlayer::create());
			sounds.back()->loadSound(filename);
			sounds.back()->setListener(SoundListenerPtr(this));
			sounds.back()->setVolume(0.5f);
			sounds.back()->setPan(0.5f);
			refill();
		}
	}
	
	void RandomSequence::shuffle()
	{
		random_shuffle(seq.begin(), seq.end());
		count = 0;
	}
	
	void RandomSequence::refill()
	{
		seq.clear();
		for(int i = 0; i < sounds.size(); i++)	{
			seq.push_back(i);
		}
		shuffle();
	}
	
	void RandomSequence::setSpeed(float value)
	{
		speed=value;
		//~ if(sound)
			//~ sound->setSpeed(speed);
		for(Objsc::SoundPlayerVec::iterator snd = sounds.begin(); snd != sounds.end(); ++snd)	{
			(*snd)->setSpeed(speed);
		}
	}
	void RandomSequence::setPan(float value)
	{
		panL = 1.0-value;
		panR = value;
		//~ if(sound)
			//~ sound->setPan(speed);
		for(Objsc::SoundPlayerVec::iterator snd = sounds.begin(); snd != sounds.end(); ++snd)	{
			(*snd)->setPan(value);
		}
	}
	void RandomSequence::setVolume(float value)
	{
		vol=value;
		//~ if(sound)
			//~ sound->setVolume(vol);
		for(Objsc::SoundPlayerVec::iterator snd = sounds.begin(); snd != sounds.end(); ++snd)	{
			(*snd)->setVolume(vol);
		}
	}
	void RandomSequence::setLoops(int value)
	{
		loops = value;
		//~ if(sound)
			//~ sound->setLoops(loops);
		for(Objsc::SoundPlayerVec::iterator snd = sounds.begin(); snd != sounds.end(); ++snd)	{
			(*snd)->setLoops(loops);
		}
	}
	
	////////////////////////////////////////
	// CLOUD
	////////////////////////////////////////
	Cloud::Cloud() :
		sounds(),
		sampleL(0.0f),
		sampleR(0.0f),
		density(0),
		variance(0.0f),
		nextsoundtime(0)
	{
		
	}
	
	Cloud::~Cloud()
	{
		sounds.clear();
	}
	
	void Cloud::receiveSoundEvent(SoundEvent event)
	{
		//printf("Cloud::receiveSoundEvent %d\n", event.type);
		switch(event.type)	{
			case SNDSTART:
				break;
			case SNDENDED:
				stop();
				break;
			case SNDNEWLOOP:
				break;
		}
	}

	void Cloud::start()
	{
		//~ if(!sounds.empty())	{
			//~ if(sound)	{
				//~ sound->cut();
			//~ }
			//~ active=true; 
			//~ sound = sounds[(int)ofRandom(0, sounds.size())];
			//~ sound->play();
		//~ }
	}
	
	void Cloud::update(int timeframe)
	{
		for(Objsc::SoundPlayerVec::iterator snd = sounds.begin(); snd != sounds.end(); ++snd)	{
			(*snd)->update();
		}
	}
	
	void Cloud::loadSound(const string& filename)
	{
		//printf("Cloud::loadSound <%s> A\n", filename.c_str());
		if(!filename.empty())	{
			//printf("Cloud::loadSound <%s> B\n", filename.c_str());
			sounds.push_back(Objsc::SoundPlayer::create());
			sounds.back()->loadSound(filename);
			sounds.back()->setListener(SoundListenerPtr(this));
			sounds.back()->setVolume(0.5f);
			sounds.back()->setPan(0.5f);
		}
	}
	
	float Cloud::tickL() 
	{
		sampleL = 0.0f;
		for(Objsc::SoundPlayerVec::iterator snd = sounds.begin(); snd != sounds.end(); ++snd)	{
			sampleL += (*snd)->tickL();
		}
		return sampleL;
	}
	
	float Cloud::tickR()
	{
		sampleR = 0.0f;
		for(Objsc::SoundPlayerVec::iterator snd = sounds.begin(); snd != sounds.end(); ++snd)	{
			sampleR += (*snd)->tickR();
		}
		return sampleR;
	}
	
	////////////////////////////////////////
	// SEQUENCER
	//////////////////////////////////////// 
	Sequencer::Sequencer() :
		sounds(),
		sound()
	{
		
	}
	
	Sequencer::~Sequencer()
	{
		sound.reset();
		sounds.clear();
	}

	void Sequencer::receiveSoundEvent(SoundEvent event)
	{
		//printf("Sequencer::receiveSoundEvent %d\n", event.type);
		switch(event.type)	{
			case SNDSTART:
				break;
			case SNDENDED:
				stop();
				break;
			case SNDNEWLOOP:
				break;
		}
	}

	void Sequencer::start()
	{
		if(!sounds.empty())	{
			if(sound)	{
				sound->cut();
			}
			active=true; 
			sound = sounds[(int)ofRandom(0, sounds.size())];
			sound->play();
		}
	}
	
	void Sequencer::update(int timeframe)
	{
		if(active && sound)	{
			sound->update();
		}
	}
	
	void Sequencer::loadSound(const string& filename)
	{
		//printf("Sequencer::loadSound <%s> A\n", filename.c_str());
		if(!filename.empty())	{
			//printf("Sequencer::loadSound <%s> B\n", filename.c_str());
			sounds.push_back(Objsc::SoundPlayer::create());
			sounds.back()->loadSound(filename);
			sounds.back()->setListener(SoundListenerPtr(this));
			sounds.back()->setVolume(0.5f);
			sounds.back()->setPan(0.5f);
		}
	}


	
}

