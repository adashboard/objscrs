#ifndef  OBJSCR_LANGMEDIA_H
#define OBJSCR_LANGMEDIA_H

// objscrs
#include "singleton.h"
#include "objscrLangAtoms.hpp"
#include "objscrLangObjects.hpp"
#include "objscrConstants.hpp"
#include "objscrMediaViews.hpp"
#include "objscrSoundPerformers.hpp"
#include "objscrFiles.hpp"
#include "objscrFrames.hpp"

// openframeworks
#include "ofMain.h"


// STL
#include <string>
using std::string;


// shared_ptr
#include <tr1/memory>


	
namespace Objsc
{
	
	/////////////////////////////////////
	// FUNCTIONS
	/////////////////////////////////////
	void initLangMedia();
	
	/////////////////////////////////////
	// MEDIA OBJECT
	/////////////////////////////////////
	class LangMediaObject : public LangObject
	{
		protected:
			string			label;
			
		public:
			LangMediaObject() : label() {}
			virtual ~LangMediaObject() {}
			bool parseArgs(Objsc::LangList* args);
			const string& getLabel() const { return label; }
	};
	
	typedef std::tr1::shared_ptr<Objsc::LangMediaObject> LangMediaObjectPtr;
	
	/////////////////////////////////////
	// GRAB OBJECT
	/////////////////////////////////////
	class GrabObject : public LangObject
	{
		protected:
			string				label;
			ofImage 				img;
			unsigned char		alpha;
			//unsigned char 		*dstPixels;
			Objsc::FrameSourcePtr	frameSource;
			
			void grab();
			void receiveList(int inlet, LangList* value);
			void setAlpha(int value);
			
		public:
			GrabObject();
			~GrabObject() {}
			bool parseArgs(Objsc::LangList* args);
			const string& getLabel() const { return label; }
			
			static const string langname;
			const string& getLangName() const { return Objsc::GrabObject::langname; }
			
			const string& getDisplayString()
			{
				if(displaystr.empty())	{
					return Objsc::GrabObject::langname;
				}
				return displaystr;
			}
			
			void setLabel(string nlabel);
			
			void receive(int inlet, Objsc::LangAtom* atom);
			void activate();
	};
	
	/////////////////////////////////////
	// IMAGE OBJECT
	/////////////////////////////////////
	class ImageObject : public LangMediaObject
	{
		protected:
			virtual void loadMedia(const string& filename, int layer, bool show);
			void removeMedia();
			void showMedia(int layer=0);
			virtual void hideMedia();
			void fullscreenMedia();
			void centreMedia();
			void sizeMedia(float width, float height);
			void widthMedia(float width);
			void heightMedia(float height);
			void locMedia(float x, float y);
			virtual void locXMedia(float x);
			virtual void locYMedia(float y);
			void geomMedia(float x, float y,float width, float height);
			void setLayerMedia(int layer);
		
			virtual void receiveList(int inlet, LangList* value);
			
		public:
			ImageObject()
			{
				setInlets(5);
				setOutlets(2);
			}
			~ImageObject();
				
			static const string langname;
			virtual const string& getLangName() const { return Objsc::ImageObject::langname; }
			
			virtual const string& getDisplayString()
			{
				if(displaystr.empty())	{
					return Objsc::ImageObject::langname;
				}
				return displaystr;
			}
			
			virtual void setLabel(string nlabel);
			
			virtual void receive(int inlet, Objsc::LangAtom* atom);
			virtual void activate();
	};
	
	/////////////////////////////////////
	// ANIM OBJECT
	/////////////////////////////////////
	class AnimObject : public ImageObject
	{
		private:
			Objsc::FileResource		finder;
			Objsc::StringVector		frames;
			string				basename;
			int 					totalFrames;
			int 					currentFrame;
			bool				fs;
			float				x;
			float				y;
			int					lyr;
		
		protected:
			void nextFrame();
			void prevFrame();
			void firstFrame();
			void lastFrame();
			void gotoFrame(int frameNum);
			int loadAnimFiles(const string& sBasename);
			int reloadAnimFiles();
			void loadMedia(const string& filename, int layer, bool show);
			void receiveList(int inlet, LangList* value);
		
		public:
			AnimObject() :
				finder(),
				frames(),
				basename(),
				totalFrames(0),
				currentFrame(0),
				fs(false),
				x(0.0f),
				y(0.0f),
				lyr(0)
			{
				setInlets(5);
				setOutlets(3);
				finder.includeFileType("png");
				finder.includeFileType("jpg");
				finder.includeFileType("jpeg");
			}
			~AnimObject();
				
			static const string langname;
			const string& getLangName() const { return Objsc::AnimObject::langname; }
			
			const string& getDisplayString()
			{
				if(displaystr.empty())	{
					return Objsc::AnimObject::langname;
				}
				return displaystr;
			}
			
			void setLabel(string nlabel);
			
			void activate();
	};
	
	/////////////////////////////////////
	// VIDEO OBJECT
	/////////////////////////////////////
	class VideoObject : public ImageObject
	{
		protected:
			Objsc::VideoViewPtr		media;
			bool				playing;
			float				duration;
			float 				timeplayed;
		
			
			void loadMedia(const string& filename, int layer, bool show);
			
			void playMedia(float vol=0.0f, float speed=0.0f);
			void stopMedia();
			void hideMedia();
			void mediaEnded();
			void setVolume(float vol);
			void setSpeed(float speed);
			void setPan(float pan);
			
			void receiveList(int inlet, LangList* value);
			
		public:
			VideoObject() :
				media(),
				playing(false),
				duration(0.0f),
				timeplayed(0.0f)
			{
				setInlets(6);
				setOutlets(2);
			}
			~VideoObject();
			
			void update(int timeElapsed);
				
			static const string langname;
			virtual const string& getLangName() const { return Objsc::VideoObject::langname; }
			
			const string& getDisplayString()
			{
				if(displaystr.empty())	{
					return Objsc::VideoObject::langname;
				}
				return displaystr;
			}
			
			void setLabel(string nlabel);
			
			void receive(int inlet, Objsc::LangAtom* atom);
			void activate();
	};
	
	/////////////////////////////////////
	// TEXT OBJECT
	/////////////////////////////////////
	class TextObject : public ImageObject
	{
		protected:
			void receiveList(int inlet, LangList* value);
			void setText(LangList* value);
			void setText(const string& value);
			void clearText();
			void locXMedia(float x);
			void locYMedia(float y);
		
			void setTextColour(int r, int g, int b);
			void setBackColour(int r, int g, int b);
			
		public:
			TextObject()
			{
				setInlets(4);
				setOutlets(2);
			}
			~TextObject();
			
			void update(int timeElapsed);
				
			static const string langname;
			virtual const string& getLangName() const { return Objsc::TextObject::langname; }
			
			const string& getDisplayString()
			{
				if(displaystr.empty())	{
					return Objsc::TextObject::langname;
				}
				return displaystr;
			}
			
			void setLabel(string nlabel);
			
			void receive(int inlet, Objsc::LangAtom* atom);
			void activate();
	};
	
	/////////////////////////////////////
	// SOUND OBJECT
	/////////////////////////////////////
	class SoundObject : public LangObject, public SoundListener
	{
		protected:
			string					label;
			Objsc::SoundPerformerPtr	sound;
			
			void receiveList(int inlet, LangList* value);
			void getAmp();
			
		public:
			SoundObject();
			~SoundObject();
			
			static const string langname;
			const string& getLangName() const { return Objsc::SoundObject::langname; }
			
			bool parseArgs(Objsc::LangList* args);
			void setLabel(string nlabel);
			const string& getLabel() const { return label; }
			
			void receive(int inlet, Objsc::LangAtom* atom);
			
			void activate();
			
			void start();
			void stop();
			
			void receiveSoundEvent(SoundEvent& event);
	};
	
	
	////////////////////////////////////////
	// MEDIA OBJECT CREATORS
	////////////////////////////////////////
	class SoundObjectCreator : public LangObjectCreator
	{
		public:
			Objsc::LangObjectPtr create() const { return Objsc::LangObjectPtr(new Objsc::SoundObject());}
	};
	
	class VideoObjectCreator : public LangObjectCreator
	{
		public:
			Objsc::LangObjectPtr create() const { return Objsc::LangObjectPtr(new Objsc::VideoObject());}
	};
	
	class ImageObjectCreator : public LangObjectCreator
	{
		public:
			Objsc::LangObjectPtr create() const { return Objsc::LangObjectPtr(new Objsc::ImageObject());}
	};
	
	class AnimObjectCreator : public LangObjectCreator
	{
		public:
			Objsc::LangObjectPtr create() const { return Objsc::LangObjectPtr(new Objsc::AnimObject());}
	};
	
	class TextObjectCreator : public LangObjectCreator
	{
		public:
			Objsc::LangObjectPtr create() const { return Objsc::LangObjectPtr(new Objsc::TextObject());}
	};
	
	class GrabObjectCreator : public LangObjectCreator
	{
		public:
			Objsc::LangObjectPtr create() const { return Objsc::LangObjectPtr(new Objsc::GrabObject());}
	};
	
}

#endif // OBJSCR_LANGMEDIA_H


