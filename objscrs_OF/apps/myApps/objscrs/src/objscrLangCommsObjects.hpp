#ifndef  OBJSCR_LANGCOMMSOBJECTS_H
#define OBJSCR_LANGCOMMSOBJECTS_H

// objscrs
#include "singleton.h"
#include "objscrLangAtoms.hpp"
#include "objscrLangObjects.hpp"
#include "objscrConstants.hpp"
#include "objscrOSC.hpp"

// openframeworks
#include "ofxOsc.h"


// STL
#include <string>
using std::string;


// shared_ptr
#include <tr1/memory>


	
namespace Objsc
{
	
	/////////////////////////////////////
	// FUNCTIONS
	/////////////////////////////////////
	void initLangCommsObjects();
	
	
	/////////////////////////////////////
	// OSC OUT OBJECT
	/////////////////////////////////////
	class OSCOutObject : public LangObject
	{
		protected:
			Objsc::OSCGroupPtr 	oscgroup;
			string 				label;
		
			void listToMessage(LangList* value, ofxOscMessage* msgPtr);
			void itemToMessage(LangAtom* atom, ofxOscMessage* msgPtr);
			void varToMessage(LangVar* atom, ofxOscMessage* msgPtr);
			
		public:
			OSCOutObject() : oscgroup()
			{
				setInlets(1);
				setOutlets(0);
				min_args = 1;
			}
			~OSCOutObject() {}
				
			static const string langname;
			const string& getLangName() const { return Objsc::OSCOutObject::langname; }
			void setLabel(string nlabel);
			const string& getLabel() const { return label; }
			
			const string& getDisplayString()
			{
				if(displaystr.empty())	{
					return Objsc::OSCOutObject::langname;
				}
				return displaystr;
			}
			
			bool parseArgs(Objsc::LangList* args);
			void setGroup(const string& groupname);
			
			void receive(int inlet, Objsc::LangAtom* atom);
			void activate();
	};
	
	/////////////////////////////////////
	// OSC IN OBJECT
	/////////////////////////////////////
	class OSCInObject : public LangObject
	{
		public:
			OSCInObject()
			{
				setInlets(0);
				setOutlets(1);
			}
			~OSCInObject() {}
				
			static const string langname;
			const string& getLangName() const { return Objsc::OSCInObject::langname; }
			
			const string& getDisplayString()
			{
				if(displaystr.empty())	{
					return Objsc::OSCInObject::langname;
				}
				return displaystr;
			}
			
			bool parseArgs(Objsc::LangList* args);
			
			void receive(int inlet, Objsc::LangAtom* atom);
			//void activate();
			
			bool hasReceiverName() { return true; }
			const string& getReceiverName() const { return Objsc::OSCInObject::langname; }
	};
	
	
	/////////////////////////////////////
	// SEND OBJECT
	/////////////////////////////////////
	class SendObject : public LangObject
	{
		protected:
			string 			label;
			string			recname;
			
		public:
			SendObject();
			~SendObject();
			
			static const string langname;
			const string& getLangName() const { return Objsc::SendObject::langname; }
			void setLabel(string nlabel);
			const string& getLabel() const { return label; }
			
			bool parseArgs(Objsc::LangList* args);
			
			void receive(int inlet, Objsc::LangAtom* atom);
	};
	
	
	/////////////////////////////////////
	// RECEIVE OBJECT
	/////////////////////////////////////
	class ReceiveObject : public LangObject
	{
		protected:
			string 			label;
			
			
		public:
			ReceiveObject();
			~ReceiveObject();
			
			static const string langname;
			const string& getLangName() const { return Objsc::ReceiveObject::langname; }
			void setLabel(string nlabel);
			const string& getLabel() const { return label; }
			
			bool parseArgs(Objsc::LangList* args);
			
			void receive(int inlet, Objsc::LangAtom* atom);
			
			bool hasReceiverName() { return true; }
			const string& getReceiverName() const { return label; }
	};
	
	/////////////////////////////////////
	// CONTROLKEY OBJECT
	/////////////////////////////////////
	class ControlKeyObject : public LangObject
	{
		protected:
			string 			label;
			
		public:
			ControlKeyObject();
			~ControlKeyObject();
			
			static const string langname;
			const string& getLangName() const { return Objsc::ControlKeyObject::langname; }
			void setLabel(string nlabel);
			const string& getLabel() const { return label; }
			
			bool parseArgs(Objsc::LangList* args);
			
			void receive(int inlet, Objsc::LangAtom* atom);
			
			bool hasReceiverName() { return true; }
			const string& getReceiverName() const { return getLangName(); }
	};
	
	
	////////////////////////////////////////
	// COMMS OBJECT CREATORS
	////////////////////////////////////////
	class OSCOutObjectCreator : public LangObjectCreator
	{
		public:
			Objsc::LangObjectPtr create() const { return Objsc::LangObjectPtr(new Objsc::OSCOutObject());}
	};
	
	class OSCInObjectCreator : public LangObjectCreator
	{
		public:
			Objsc::LangObjectPtr create() const { return Objsc::LangObjectPtr(new Objsc::OSCInObject());}
	};
	
	class SendObjectCreator : public LangObjectCreator
	{
		public:
			Objsc::LangObjectPtr create() const { return Objsc::LangObjectPtr(new Objsc::SendObject());}
	};
	
	class ReceiveObjectCreator : public LangObjectCreator
	{
		public:
			Objsc::LangObjectPtr create() const { return Objsc::LangObjectPtr(new Objsc::ReceiveObject());}
	};
	
	class ControlKeyObjectCreator : public LangObjectCreator
	{
		public:
			Objsc::LangObjectPtr create() const { return Objsc::LangObjectPtr(new Objsc::ControlKeyObject());}
	};

	
}

#endif // OBJSCR_LANGCOMMSOBJECTS_H


