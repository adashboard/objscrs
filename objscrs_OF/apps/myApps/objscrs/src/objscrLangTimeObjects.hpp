#ifndef  OBJSCR_LANGTIMEOBJECTS_H
#define OBJSCR_LANGTIMEOBJECTS_H

// Objsc
#include "objscrLangAtoms.hpp"
#include "objscrLangObjects.hpp"

// STL
#include <string>
using std::string;


// shared_ptr
#include <tr1/memory>


	
namespace Objsc
{
	
	/////////////////////////////////////
	// FUNCTIONS
	/////////////////////////////////////
	void initLangTimeObjects();
	
	
	////////////////////////////////////////
	// METRO OBJECT
	////////////////////////////////////////
	class MetroObject : public LangObject
	{
		protected:
			int			interval;
			int 			count;
			bool		active;
			
			void receiveList(int inlet, LangList* value);
			
		public:
			MetroObject();
			~MetroObject();
			
			static const string langname;
			const string& getLangName() const { return Objsc::MetroObject::langname; }
			
			bool parseArgs(Objsc::LangList* args);
			
			void receive(int inlet, Objsc::LangAtom* atom);
			virtual void activate();
			void update(int timeframe);
			
			void setInterval(int value);
			void start();
			void stop() { active=false; }
			
			const bool hasArgs() const { return true;}
			string getArgString() { return intToString(interval); }
	};
	
	////////////////////////////////////////
	// TIMEOUT OBJECT
	////////////////////////////////////////
	class TimeObject : public MetroObject
	{
		public:
			TimeObject();
			~TimeObject();
			
			static const string langname;
			const string& getLangName() const { return Objsc::TimeObject::langname; }
			
			void activate();
	};
	
	////////////////////////////////////////
	// DURATION OBJECT
	////////////////////////////////////////
	class DurationObject : public LangObject
	{
		protected:
			int 			count;
			bool		active;
			
			void receiveList(int inlet, LangList* value);
			
		public:
			DurationObject();
			~DurationObject();
			
			static const string langname;
			const string& getLangName() const { return Objsc::DurationObject::langname; }
			
			void receive(int inlet, Objsc::LangAtom* atom);
			void activate();
			void update(int timeframe);
			
			void start();
			void stop() { active=false; }
	};
	
	////////////////////////////////////////
	// LINE OBJECT
	////////////////////////////////////////
	class LineObject : public LangObject
	{
		protected:
			float		current;
			float 		begin;
			float 		end;
			float 		step;
			int			interval;
			int			tframe;
			int 			count;
			bool		active;
			
			void receiveList(int inlet, LangList* value);
			
		public:
			LineObject();
			~LineObject();
			
			static const string langname;
			const string& getLangName() const { return Objsc::LineObject::langname; }
			
			bool parseArgs(Objsc::LangList* args);
			
			void receive(int inlet, Objsc::LangAtom* atom);
			virtual void activate();
			void update(int timeframe);
			
			void setInterval(int value);
			void setBeginValue(int value);
			void setEndValue(int value);
			void reset();
			void start();
			void stop() { active=false; }
			
			const bool hasArgs() const { return true;}
			string getArgString() { return intToString(interval); }
	};
	
	////////////////////////////////////////
	// CREATORS
	////////////////////////////////////////
	class MetroObjectCreator : public LangObjectCreator
	{
		public:
			Objsc::LangObjectPtr create() const { return Objsc::LangObjectPtr(new Objsc::MetroObject());}
	};
	
	class TimeObjectCreator : public LangObjectCreator
	{
		public:
			Objsc::LangObjectPtr create() const { return Objsc::LangObjectPtr(new Objsc::TimeObject());}
	};
	
	class DurationObjectCreator : public LangObjectCreator
	{
		public:
			Objsc::LangObjectPtr create() const { return Objsc::LangObjectPtr(new Objsc::DurationObject());}
	};
	
	class LineObjectCreator : public LangObjectCreator
	{
		public:
			Objsc::LangObjectPtr create() const { return Objsc::LangObjectPtr(new Objsc::LineObject());}
	};
};


#endif

