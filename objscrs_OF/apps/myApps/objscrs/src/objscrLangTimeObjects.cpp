#include "objscrLangTimeObjects.hpp"

#include "objscrMath.hpp"

#ifndef MAX
	#define MAX(x,y) (((x) > (y)) ? (x) : (y))
#endif

namespace Objsc
{
	
	////////////////////////////////////////
	// DECLARATIONS
	////////////////////////////////////////
	const string Objsc::MetroObject::langname = "metro";
	const string Objsc::TimeObject::langname = "timeout";
	const string Objsc::DurationObject::langname = "duration";
	const string Objsc::LineObject::langname = "line";
	
	
	
	/////////////////////////////////////
	// FUNCTIONS
	/////////////////////////////////////
	void initLangTimeObjects()
	{
		Objsc::LangObjectFactory* factory = Objsc::LangObjectFactory::Current();
		factory->addCreator(Objsc::MetroObject::langname, new Objsc::MetroObjectCreator());
		factory->addCreator(Objsc::TimeObject::langname, new Objsc::TimeObjectCreator());
		factory->addCreator(Objsc::DurationObject::langname, new Objsc::DurationObjectCreator());
		factory->addCreator(Objsc::LineObject::langname, new Objsc::LineObjectCreator());
	}
	
	
	////////////////////////////////////////
	// METRO OBJECT
	////////////////////////////////////////
	MetroObject::MetroObject() :
		interval(200),
		count(0),
		active(false)
	{
		setInlets(2);
		setOutlets(1);
	}
	
	MetroObject::~MetroObject()
	{
		
	}
	
	bool MetroObject::parseArgs(Objsc::LangList* args)
	{
		//~ if(args->size() > 1)	{
			//~ setInterval(getItemAsInt(args, 1));
		if(!args->empty())	{
			setInterval(getItemAsInt(args, 0));
			char cstr[64];
			sprintf(cstr, "%s %d", getLangName().c_str(), interval);
			displaystr.assign(cstr);
		}
		return true;
	}
	
	void MetroObject::receive(int inlet, Objsc::LangAtom* atom)
	{
		switch(atom->getType())	{
			case LANG_BANG:
				break;
			case LANG_INT:
				switch(inlet)	{
					case 0:
						if(parseBool(atom))	{
							start();
						} else {
							stop();
						}
						break;
					case 1:
						setInterval(parseInt(atom));
						break;
				}
				break;
			case LANG_FLOAT:
				switch(inlet)	{
					case 0:
						if(parseBool(atom))	{
							start();
						} else {
							stop();
						}
						break;
					case 1:
						setInterval(parseFloat(atom));
						break;
				}
				break;
			case LANG_LIST:
				receiveList(inlet, dynamic_cast<Objsc::LangList*>(atom));
				break;
		}
	}
	
	void MetroObject::receiveList(int inlet, LangList* value)
	{
		if(inlet == 0) {
			string cmd = parseString(value->idx(0), "");
			if(cmd.empty())	{
				parseInt(value->idx(0), 0)>0?start():stop();
			} else {
				if(cmd == "start")	{
					start();
				} else if(cmd == "stop")	{
					stop();
				}
			}
		} else if(inlet == 1) {
			setInterval(parseInt(value->idx(0), 200));
		}
	}
	
	void MetroObject::start() 
	{ 
		active=true;
		count=0;
	}
	
	void MetroObject::setInterval(int value) 
	{ 
		interval=MAX(0, value); 
	}
	
	void MetroObject::update(int timeframe)
	{
		if(active)	{
			count += timeframe;
			if(count >= interval)
				activate();
		}
	}
	
	void MetroObject::activate()
	{
		count=0;
		send(0, new Objsc::LangBang());
	}
	
	
	////////////////////////////////////////
	// TIMEOUT OBJECT
	////////////////////////////////////////
	TimeObject::TimeObject()
	{
		setInlets(2);
		setOutlets(1);
	}
	
	TimeObject::~TimeObject()
	{
		
	}
	
	void TimeObject::activate()
	{
		count=0;
		stop();
		send(0, new Objsc::LangBang());
	}
	
	////////////////////////////////////////
	// DURATION OBJECT
	////////////////////////////////////////
	DurationObject::DurationObject() :
		count(0),
		active(false)
	{
		setInlets(2);
		setOutlets(1);
	}
	
	DurationObject::~DurationObject()
	{
		
	}
	
	void DurationObject::receive(int inlet, Objsc::LangAtom* atom)
	{
		switch(atom->getType())	{
			case LANG_BANG:
				switch(inlet)	{
					case 0:
						activate();
						count=0;
						break;
					case 1:
						activate();
						break;
				}
				break;
			case LANG_INT:
				switch(inlet)	{
					case 0:
						if(parseBool(atom))	{
							start();
						} else {
							stop();
						}
						break;
				}
				break;
			case LANG_FLOAT:
				switch(inlet)	{
					case 0:
						if(parseBool(atom))	{
							start();
						} else {
							stop();
						}
						break;
				}
				break;
			case LANG_LIST:
				receiveList(inlet, dynamic_cast<Objsc::LangList*>(atom));
				break;
		}
	}
	
	void DurationObject::receiveList(int inlet, LangList* value)
	{
		if(inlet == 0) {
			string cmd = parseString(value->idx(0), "");
			if(cmd.empty())	{
				parseInt(value->idx(0), 0)>0?start():stop();
			} else {
				if(cmd == "start")	{
					start();
				} else if(cmd == "stop")	{
					stop();
				} else if(cmd == "reset")	{
					count = 0;
				} else if(cmd == "bang")	{
					activate();
					count = 0;
				}
			}
		}
	}
	
	void DurationObject::start() 
	{ 
		active=true;
		count=0;
	}
	
	void DurationObject::update(int timeframe)
	{
		if(active)	{
			count += timeframe;
		}
	}
	
	void DurationObject::activate()
	{
		send(0, new Objsc::LangInt(count));
	}
	
	
	////////////////////////////////////////
	// LINE OBJECT
	////////////////////////////////////////
	LineObject::LineObject() :
		current(0.0f),
		begin(0.0f),
		end(0.0f),
		step(0.0f),
		interval(200),
		tframe(0.0f),
		count(0),
		active(false)
	{
		setInlets(4);
		setOutlets(2);
	}
	
	LineObject::~LineObject()
	{
		
	}
	
	bool LineObject::parseArgs(Objsc::LangList* args)
	{
		//~ if(args->size() > 1)	{
			//~ setInterval(getItemAsInt(args, 1));
		if(!args->empty())	{
			setInterval(getItemAsInt(args, 0));
			char cstr[64];
			sprintf(cstr, "%s %d", getLangName().c_str(), interval);
			displaystr.assign(cstr);
		}
		return true;
	}
	
	void LineObject::receive(int inlet, Objsc::LangAtom* atom)
	{
		switch(atom->getType())	{
			case LANG_BANG:
				break;
			case LANG_INT:
				switch(inlet)	{
					case 0:
						if(parseBool(atom))	{
							start();
						} else {
							stop();
						}
						break;
					case 1:
						setInterval(parseInt(atom));
						break;
				}
				break;
			case LANG_FLOAT:
				switch(inlet)	{
					case 0:
						active=true;
						setEndValue(parseFloat(atom));
						break;
					case 1:
						setInterval(parseFloat(atom));
						break;
					case 2:
						setBeginValue(parseFloat(atom));
						break;
					case 3:
						setEndValue(parseFloat(atom));
						break;
				}
				break;
			case LANG_LIST:
				receiveList(inlet, dynamic_cast<Objsc::LangList*>(atom));
				break;
		}
	}
	
	void LineObject::receiveList(int inlet, LangList* value)
	{
		if(inlet == 0) {
			string cmd = parseString(value->idx(0), "");
			if(cmd.empty())	{
				parseInt(value->idx(0), 0)>0?start():stop();
			} else {
				if(cmd == "start")	{
					start();
				} else if(cmd == "stop")	{
					stop();
				}
			}
		} else if(inlet == 1) {
			setInterval(parseInt(value->idx(0), 200));
		}
	}
	
	void LineObject::start() 
	{ 
		active=true;
		reset();
	}
	
	void LineObject::setInterval(int value) 
	{ 
		interval=MAX(0, value); 
	}
	
	void LineObject::setBeginValue(int value)
	{
		begin = value;
		if(tframe > 0)	{
			step = (end-begin)/(interval/tframe);
		} else {
			step = 0.0f;
		}
	}
	
	void LineObject::setEndValue(int value)
	{
		end = value;
		if(tframe > 0)	{
			if(active)	{
				step = (end-current)/(interval/tframe);
			} else {
				step = (end-begin)/(interval/tframe);
			}
		} else {
			step = 0.0f;
		}
	}
	
	void LineObject::reset()
	{
		current = begin;
		if(tframe > 0)	{
			step = (end-begin)/(interval/tframe);
		} else {
			step = 0.0f;
		}
		count=0;
	}
	
	void LineObject::update(int timeframe)
	{
		tframe = timeframe;
		if(active)	{
			send(0, new Objsc::LangFloat(current));
			current += step;
			if(valueMatches(end,current,step))	{
				activate();
				stop();
				send(0, new Objsc::LangFloat(end));
			}
		}
	}
	
	void LineObject::activate()
	{
		send(1, new Objsc::LangBang());
	}
}

