#ifndef OBJSCR_COLOURS_H
#define OBJSCR_COLOURS_H


namespace Objsc
{
	////////////////////////////////////////
	// DATA TYPES
	////////////////////////////////////////
	struct Colour {
		int r;
		int g;
		int b;
		int a;
	};



	// FUNCTIONS
	int clipColourValue(int value);
	bool isNullColour(Colour col);
	bool isWhiteColour(Colour col);
	bool isGreyColour(Colour col);
	bool coloursMatch(Colour a, Colour b);
	Colour makeNullColour();
	Colour makeWhiteColour();
	Colour makeBlackColour();
	Colour makeColour(int red, int green, int blue);
	Colour makeColour(int red, int green, int blue, int alpha);
	Colour hsi2rgb(float H, float S, float I);
	void rgb2hsi(int r, int g, int b, float* hue, float* saturation, float* intensity);
	Colour makeRandomColour();

	/************************
	COLOURS
	************************/
	const Colour 		SCREENCOLOUR = makeColour(170,170,170); //makeColour(182,169,142); 
	const Colour 		HIBGCOLOUR = makeColour(255,255,255,127); 
	const Colour 		BGCOLOUR = makeColour(51,51,51); // blue- 75,95,130); 
	const Colour 		FGCOLOUR = makeColour(0,0,0,255); 
	const Colour		HICOLOUR = makeColour(255,102,0); 
	const Colour		SELCOLOUR = makeColour(256,128,0); 
	const Colour		DISCOLOUR = makeColour(96,96,96); 
	const Colour		TEXTCOLOUR = makeColour(256,256,256); 
	const Colour		BLACK = makeColour(0,0,0); 
	const Colour		WHITE = makeColour(255,255,255); 
	const Colour		TRANWHITE = makeColour(255,255,255,120); 
	const Colour		RED = makeColour(255,0,0); 
	const Colour		GREEN = makeColour(0,255,0); 
	const Colour		BLUE = makeColour(0,0,255); 
	const Colour		TURQUOISE = makeColour(0,255,255); 
	const Colour		YELLOW = makeColour(255,255,0); 
	const Colour		MAGENTA = makeColour(255,0,255); 
	const Colour		DARKGREY = makeColour(64,64,64);
	const Colour		CHARCOAL = makeColour(32,32,32);
	const Colour		MIDGREY = makeColour(128,128,128);
	const Colour		LIGHTGREY = makeColour(192,192,192);

}

#endif


