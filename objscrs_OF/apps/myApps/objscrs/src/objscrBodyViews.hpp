#ifndef OBJSCR_BODYVIEWS_H
#define OBJSCR_BODYVIEWS_H


// objscrs
#include "objscrConstants.hpp"
#include "objscrGUI.hpp"
#include "objscrGeom.hpp"
#include "objscrCommands.hpp"
#include "objscrBody.hpp"

// STL
#include <string>
#include <vector>
#include <map>
#include <list>
using std::string;
using std::vector;
using std::map;
using std::list;

// shared_ptr
#include <tr1/memory>


namespace Objsc
{
	////////////////////////////////////////
	// BODY VIEW
	////////////////////////////////////////
	class BodyView : public Objsc::Widget
	{
		protected:
			Objsc::BodyPtr 		body;
			ofPoint             		centroid;
			int					scrX;
			int					scrY;
			Objsc::TextField		namefield;
			//Objsc::TextField		stagenumfield;
			Objsc::ContextMenuPtr 	editmenu;
	
			//~ void mapToScreen();
			//~ void mapToStage();
			void openMenu(int x, int y);
		
		public:
			BodyView();
			virtual ~BodyView() {}
			static const string stylename;
			void setBody(Objsc::BodyPtr bodyPtr);
			Objsc::BodyPtr getBody() {return body;}
			const bool matchesBody(Objsc::BodyPtr bodyPtr) const { return body == bodyPtr; }
			// geom
			virtual void setLocation(float x, float y);
			virtual void setDimensions(float width, float height);
			virtual void setBounds(float x, float y, float width, float height);
			// runtime
			void update();
			void draw();
			// events
			void keyPressed(int key);
			void keyReleased(int key);
			void mouseMoved(int x, int y);
			void mouseDragged(int x, int y, int button);
			void mousePressed(int x, int y, int button);
			void mouseReleased(int x, int y, int button);
			void windowResized(int w, int h);
			void boundsResized();
			
			static std::tr1::shared_ptr<Objsc::BodyView> create()
			{
				return std::tr1::shared_ptr<Objsc::BodyView>(new BodyView());
			}
	};
	
	typedef std::tr1::shared_ptr<Objsc::BodyView> BodyViewPtr;
	typedef vector < Objsc::BodyViewPtr > BodyViewVec;
	

	
	
}

#endif // OBJSCR_BODYVIEWS_H
