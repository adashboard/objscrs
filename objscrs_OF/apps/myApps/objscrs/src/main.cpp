#include "ofMain.h"
#include "testApp.h"
#include "ofAppGlutWindow.h"

// objscrs
#include "objscrFiles.hpp"
#include "objscrSettings.hpp"


int main()
{
	// version info
	printf("\nobjscrs version: 0.3.4\n");
	printf("[snapshot: g]\n");
	printf("Glasgow 10-03-2011\n\n");
	
	// read settings file before launching
	Objsc::ProjectFiles::Current()->initPaths();
	Objsc::ProjectFiles::Current()->setCurrentProject("root");
	Objsc::Settings::Current()->load();
	Objsc::Settings::Current()->print();
	
	// run app
	ofAppGlutWindow window;
	ofSetupOpenGL(&window, Objsc::Settings::Current()->getDisplayWidth(),Objsc::Settings::Current()->getDisplayHeight(), OF_WINDOW);
	ofRunApp( new testApp());
}
