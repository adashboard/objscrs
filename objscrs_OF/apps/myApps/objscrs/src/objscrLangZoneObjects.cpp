#include "objscrLangZoneObjects.hpp"


#include <cmath>

//objscrs
#include "objscrGUIRoot.hpp"
#include "objscrShapeFilter.hpp"
#include "objscrArchive.hpp"
#include "objscrApp.hpp"
#include "objscrBody.hpp"


namespace Objsc
{
	
	////////////////////////////////////////
	// DECLARATIONS
	////////////////////////////////////////
	const string Objsc::BodyPosObject::langname = "body_xy";
	const string Objsc::BodyProxObject::langname = "body_prox";
	const string Objsc::BodyAreaObject::langname = "body_area";
	const string Objsc::BodyMoveObject::langname = "body_move";
	const string Objsc::ZoneObject::langname = "zone";
	const string Objsc::ZoneMapObject::langname = "zonemaps";
	const string Objsc::ZoneSpaceObject::langname = "zonespace";
	const string Objsc::TrackingObject::langname = "tracking";
	const string Objsc::ScoreObject::langname = "scores";
	const string Objsc::BodyPrefsObject::langname = "body_prefs";
	
	
	
	
	/////////////////////////////////////
	// FUNCTIONS
	/////////////////////////////////////
	void initLangZoneObjects()
	{
		Objsc::LangObjectFactory* factory = Objsc::LangObjectFactory::Current();
		factory->addCreator(Objsc::BodyPosObject::langname, new Objsc::BodyPosObjectCreator());
		factory->addCreator(Objsc::BodyProxObject::langname, new Objsc::BodyProxObjectCreator());
		factory->addCreator(Objsc::BodyAreaObject::langname, new Objsc::BodyAreaObjectCreator());
		factory->addCreator(Objsc::BodyMoveObject::langname, new Objsc::BodyMoveObjectCreator());
		factory->addCreator(Objsc::ZoneObject::langname, new Objsc::ZoneObjectCreator());
		factory->addCreator("body_enter", new Objsc::NoInletsZoneObjectCreator("body_enter"));
		factory->addCreator("body_exit", new Objsc::NoInletsZoneObjectCreator("body_exit"));
		factory->addCreator("body_within", new Objsc::NoInletsZoneObjectCreator("body_within"));
		factory->addCreator(Objsc::ZoneMapObject::langname, new Objsc::ZoneMapObjectCreator());
		factory->addCreator(Objsc::ZoneSpaceObject::langname, new Objsc::ZoneSpaceObjectCreator());
		factory->addCreator(Objsc::TrackingObject::langname, new Objsc::TrackingObjectCreator());
		factory->addCreator(Objsc::ScoreObject::langname, new Objsc::ScoreObjectCreator());
		factory->addCreator(Objsc::BodyPrefsObject::langname, new Objsc::BodyPrefsObjectCreator());
	}
	
	
	
	/////////////////////////////////////
	// BODY POS OBJECT
	/////////////////////////////////////
	void BodyPosObject::receive(int inlet, Objsc::LangAtom* atom)
	{
		if(inlet > -1 && inlet < num_inlets) 	{
			//std::cout << "handling ..." << std::endl; 
			switch(atom->getType())	{
				case LANG_BANG:
					if(inlet == 0)	{
						activate();
					}
					break;
				case LANG_LIST:
					receiveBody(inlet, dynamic_cast<LangList*>(atom));
					break;
			}
		}
	}
	
	void BodyPosObject::receiveBody(int inlet, Objsc::LangList* nbody)
	{
		id = parseInt(nbody->idx(0));
		x = parseFloat(nbody->idx(1));
		y = parseFloat(nbody->idx(2));
		if(inlet == 0)	{
			activate();
		}
	}
		
	void BodyPosObject::activate()
	{
		send(0, new Objsc::LangInt(id));
		send(1, new Objsc::LangFloat(x));
		send(2, new Objsc::LangFloat(y));
	}
	
	
	/////////////////////////////////////
	// BODY PROX OBJECT
	/////////////////////////////////////
	void BodyProxObject::receive(int inlet, Objsc::LangAtom* atom)
	{
		if(inlet > -1 && inlet < num_inlets) 	{
			//std::cout << "handling ..." << std::endl; 
			switch(atom->getType())	{
				case LANG_BANG:
					if(inlet == 0)	{
						activate();
					}
					break;
				case LANG_LIST:
					receiveBody(inlet, dynamic_cast<LangList*>(atom));
					break;
			}
		}
	}
	
	void BodyProxObject::receiveBody(int inlet, Objsc::LangList* nbody)
	{
		id = parseInt(nbody->idx(0));
		x = parseFloat(nbody->idx(1));
		y = parseFloat(nbody->idx(2));
		//printf("receiveBody %d\n", nbody->size());
		if(nbody->size() >3)	{
			p = parseFloat(nbody->idx(3));
			//printf("prox %f\n", p);
		} else {
			p = 0.0f;
		}
		if(inlet == 0)	{
			activate();
		}
	}
		
	void BodyProxObject::activate()
	{
		send(0, new Objsc::LangInt(id));
		send(1, new Objsc::LangFloat(x));
		send(2, new Objsc::LangFloat(y));
		send(3, new Objsc::LangFloat(p));
	}
	
	/////////////////////////////////////
	// BODY AREA OBJECT
	/////////////////////////////////////
	void BodyAreaObject::receive(int inlet, Objsc::LangAtom* atom)
	{
		if(inlet > -1 && inlet < num_inlets) 	{
			//std::cout << "handling ..." << std::endl; 
			switch(atom->getType())	{
				case LANG_BANG:
					if(inlet == 0)	{
						activate();
					}
					break;
				case LANG_LIST:
					receiveBody(inlet, dynamic_cast<LangList*>(atom));
					break;
			}
		}
	}
	
	void BodyAreaObject::receiveBody(int inlet, Objsc::LangList* nbody)
	{
		id = parseInt(nbody->idx(0));
		shapeArea = parseFloat(nbody->idx(3));
		bodyArea = parseFloat(nbody->idx(4));
		//printf("receiveBody %d\n", nbody->size());
		if(inlet == 0)	{
			activate();
		}
	}
		
	void BodyAreaObject::activate()
	{
		send(0, new Objsc::LangInt(id));
		send(1, new Objsc::LangFloat(shapeArea));
		send(2, new Objsc::LangFloat(bodyArea));
	}
	
	/////////////////////////////////////
	// BODY MOVE OBJECT
	/////////////////////////////////////
	void BodyMoveObject::receive(int inlet, Objsc::LangAtom* atom)
	{
		if(inlet > -1 && inlet < num_inlets) 	{
			//std::cout << "handling ..." << std::endl; 
			switch(atom->getType())	{
				case LANG_BANG:
					if(inlet == 0)	{
						activate();
					}
					break;
				case LANG_LIST:
					receiveBody(inlet, dynamic_cast<LangList*>(atom));
					break;
			}
		}
	}
	
	void BodyMoveObject::receiveBody(int inlet, Objsc::LangList* nbody)
	{
		id = parseInt(nbody->idx(0));
		x = parseFloat(nbody->idx(1));
		y = parseFloat(nbody->idx(2));
		t = ofGetElapsedTimef();
		if (t-pT > 0.0f)	{
			velX = (x-pX)/(t-pT);
			velY = (y-pY)/(t-pT);
			dist = (float)sqrt(pow(x-pX, 2) + pow(y-pY, 2));
			vel = dist/(t-pT);
		} else {
			velX = 0.0f;
			velY = 0.0f;
			dist = 0.0f;
			vel = 0.0f;
		}
		pX = x;
		pY = y;
		pT = t;
		if(inlet == 0)	{
			activate();
		}
	}
		
	void BodyMoveObject::activate()
	{
		send(0, new Objsc::LangInt(id));
		send(1, new Objsc::LangFloat(vel));
		send(2, new Objsc::LangFloat(velX));
		send(3, new Objsc::LangFloat(velY));
		send(4, new Objsc::LangFloat(dist));
	}
	
	
	/////////////////////////////////////
	// ZONE OBJECT
	/////////////////////////////////////
	void ZoneObject::receive(int inlet, Objsc::LangAtom* atom)
	{
		if(inlet > -1 && inlet < num_inlets) 	{
			//std::cout << "handling ..." << std::endl; 
			switch(atom->getType())	{
				case LANG_BANG:
					activate();
					break;
				case LANG_LIST:
					receiveBody(inlet, dynamic_cast<LangList*>(atom));
					break;
			}
		}
	}
	
	void ZoneObject::receiveBody(int inlet, Objsc::LangList* nbody)
	{
		//printf("ZoneObject::receiveBody [%u] %f,%f\n", nbody->getID(), nbody->getX(), nbody->getY());
		body.clear();
		body.add(parseInt(nbody->idx(0)));
		body.add(parseFloat(nbody->idx(1)));
		body.add(parseFloat(nbody->idx(2)));
		body.add(parseFloat(nbody->idx(3)));
		body.add(parseFloat(nbody->idx(4)));
		numbodies = parseInt(nbody->idx(5));
		//printf("receiveBody %d\n", nbody->size());
		if(nbody->size() >6)	{
			body.add(parseFloat(nbody->idx(6)));
		}
		if(inlet == 0)	{
			activate();
		}
	}
		
	void ZoneObject::activate()
	{
		send(0, new Objsc::LangBang());
		send(1, new Objsc::LangInt(numbodies));
		send(2, new Objsc::LangList(body));
	}
	
	/////////////////////////////////////
	// ZONE MAP OBJECT
	/////////////////////////////////////
	void ZoneMapObject::receive(int inlet, Objsc::LangAtom* atom)
	{
		if(inlet > -1 && inlet < num_inlets) 	{
			switch(atom->getType())	{
				case LANG_LIST:
					receiveList(0, dynamic_cast<Objsc::LangList*>(atom));
					break;
			}
		}
	}
	
	void ZoneMapObject::receiveList(int inlet, LangList* value)
	{
		
		string cmd = parseString(value->idx(0), "");
		if(cmd == "show")	{
			showZones();
		} else if(cmd == "hide")	{
			hideZones();
		} else if(cmd == "goto")	{
			gotoMap(parseString(value->idx(1), ""));
		} 
	}
	
	void ZoneMapObject::showZones()
	{
		Objsc::GUIRoot::Current()->showZones();
		send(1, new Objsc::LangInt(1));
	}
	
	void ZoneMapObject::hideZones()
	{
		Objsc::GUIRoot::Current()->hideZones();
		send(1, new Objsc::LangInt(0));
	}
	
	void ZoneMapObject::gotoMap(const string& mapname)
	{
		if(!mapname.empty())	{
			Objsc::CommandPtr cmd = Objsc::CommandDispatcher::createCommand("_zonemapeditor_", "set_map");
			Objsc::PacketPtr data = cmd->getData();
			data->addArg(mapname);
			Objsc::CommandDispatcher::Current()->doCommand(cmd);
			send(0, new Objsc::LangBang());
		}
	}
	
	
	/////////////////////////////////////
	// ZONE SCALE OBJECT
	/////////////////////////////////////
	void ZoneSpaceObject::receive(int inlet, Objsc::LangAtom* atom)
	{
		
	}
	
	void ZoneSpaceObject::receiveList(int inlet, LangList* value)
	{
		
	}
	
	
	/////////////////////////////////////
	// TRACKING OBJECT
	/////////////////////////////////////
	void TrackingObject::receive(int inlet, Objsc::LangAtom* atom)
	{
		if(inlet > -1 && inlet < num_inlets) 	{
			switch(atom->getType())	{
				case LANG_INT:
					switch(inlet) {
						case 0:
							if(parseBool(atom))	{
								startTracking();
							} else {
								stopTracking();
							}
							break;
						case 1:
							Objsc::Stage::Current()->setShapesMinRadius(parseInt(atom, 0));
							break;
						case 2:
							Objsc::Stage::Current()->setShapesMaxRadius(parseInt(atom, 0));
							break;
						case 3:
							Objsc::Stage::Current()->setShapesThreshold(parseInt(atom, 0));
							break;
					}
					break;
				case LANG_FLOAT:
					switch(inlet) {
						case 0:
							if(parseBool(atom))	{
								startTracking();
							} else {
								stopTracking();
							}
							break;
						case 1:
							Objsc::Stage::Current()->setShapesMinRadius(parseInt(atom, 0));
							break;
						case 2:
							Objsc::Stage::Current()->setShapesMaxRadius(parseInt(atom, 0));
							break;
						case 3:
							Objsc::Stage::Current()->setShapesThreshold(parseInt(atom, 0));
							break;
					}
					break;
				case LANG_LIST:
					receiveList(0, dynamic_cast<Objsc::LangList*>(atom));
					break;
			}
		}
	}
	
	void TrackingObject::receiveList(int inlet, LangList* value)
	{
		
		string cmd = parseString(value->idx(0), "");
		if(cmd == "show")	{
			showTracking();
		} else if(cmd == "hide")	{
			hideTracking();
		} else if(cmd == "start")	{
			startTracking();
		} else if(cmd == "stop")	{
			stopTracking();
		} else if(cmd == "ref")	{
			takeRef();
		} else if(cmd == "continuous")	{
			startRef();
		} else if(cmd == "static")	{
			stopRef();
		} else if(cmd == "preview_on")	{
			Objsc::Stage::Current()->previewOn();
		} else if(cmd == "preview_off")	{
			Objsc::Stage::Current()->previewOff();
		} 
	}
	
	void TrackingObject::showTracking()
	{
		Objsc::GUIRoot::Current()->showStage();
		send(3, new Objsc::LangInt(1));
	}
	
	void TrackingObject::hideTracking()
	{
		Objsc::GUIRoot::Current()->hideStage();
		send(3, new Objsc::LangInt(0));
	}
	
	void TrackingObject::startTracking()
	{
		Objsc::Stage::Current()->start();
		send(2, new Objsc::LangInt(1));
	}
	
	void TrackingObject::stopTracking()
	{
		Objsc::Stage::Current()->stop();
		Objsc::GUIRoot::Current()->clearStage();
		send(2, new Objsc::LangInt(0));
	}
	
	void TrackingObject::takeRef()
	{
		Objsc::CommandPtr cmd = Objsc::CommandDispatcher::createCommand(Objsc::ShapeFilter::cmdlabel, "ref_take");
		Objsc::CommandDispatcher::Current()->doCommand(cmd);
		send(0, new Objsc::LangBang());
	}
	
	void TrackingObject::startRef()
	{
		Objsc::CommandPtr cmd = Objsc::CommandDispatcher::createCommand(Objsc::ShapeFilter::cmdlabel, "ref_on");
		Objsc::CommandDispatcher::Current()->doCommand(cmd);
		send(1, new Objsc::LangInt(1));
	}
	
	void TrackingObject::stopRef()
	{
		Objsc::CommandPtr cmd = Objsc::CommandDispatcher::createCommand(Objsc::ShapeFilter::cmdlabel, "ref_off");
		Objsc::CommandDispatcher::Current()->doCommand(cmd);
		send(1, new Objsc::LangInt(0));
	}
	
	/////////////////////////////////////
	// SCORE OBJECT
	/////////////////////////////////////
	void ScoreObject::receive(int inlet, Objsc::LangAtom* atom)
	{
		if(inlet > -1 && inlet < num_inlets) 	{
			switch(atom->getType())	{
				case LANG_LIST:
					receiveList(0, dynamic_cast<Objsc::LangList*>(atom));
					break;
			}
		}
	}
	
	void ScoreObject::receiveList(int inlet, LangList* value)
	{
		string cmd = parseString(value->idx(0), "");
		if(cmd == "load")	{
			loadScore(parseString(value->idx(1), ""));
		} 
	}
	
	void ScoreObject::loadScore(const string& scorename)
	{
		if(!scorename.empty())	{
			//Objsc::Archivist::Current()->open(scorename);
			Objsc::App::Current()->loadScore(scorename);
		}
	}
	
	/////////////////////////////////////
	// BODY PREFS OBJECT
	/////////////////////////////////////
	void BodyPrefsObject::receive(int inlet, Objsc::LangAtom* atom)
	{
		if(inlet > -1 && inlet < num_inlets) 	{
			switch(atom->getType())	{
				case LANG_BANG:
					switch(inlet) {
						case 0:
							Objsc::Body::printThresholds();
							break;
					}
					break;
				case LANG_INT:
					switch(inlet) {
						case 0:
							Objsc::Body::unbornAge = parseInt(atom, 0);
							break;
						case 1:
							Objsc::Body::unbornHits = parseInt(atom, 1);
							break;
						case 2:
							Objsc::Body::unbornLastHit = parseInt(atom, 24);
							break;
						case 3:
							Objsc::Body::becomingAge = parseInt(atom, 0);
							break;
						case 4:
							Objsc::Body::becomingHits = parseInt(atom, 6);
							break;
						case 5:
							Objsc::Body::becomingLastHit = parseInt(atom, 24);
							break;
						case 6:
							Objsc::Body::presentLastHit = parseInt(atom, 24);
							break;
						case 7:
							Objsc::Body::absentLastHitMin = parseInt(atom, 6);
							break;
						case 8:
							Objsc::Body::absentLastHitMax = parseInt(atom, 24);
							break;
					}
					break;
				case LANG_FLOAT:
					switch(inlet) {
						case 0:
							Objsc::Body::unbornAge = parseInt(atom, 0);
							break;
						case 1:
							Objsc::Body::unbornHits = parseInt(atom, 1);
							break;
						case 2:
							Objsc::Body::unbornLastHit = parseInt(atom, 24);
							break;
						case 3:
							Objsc::Body::becomingAge = parseInt(atom, 0);
							break;
						case 4:
							Objsc::Body::becomingHits = parseInt(atom, 6);
							break;
						case 5:
							Objsc::Body::becomingLastHit = parseInt(atom, 24);
							break;
						case 6:
							Objsc::Body::presentLastHit = parseInt(atom, 24);
							break;
						case 7:
							Objsc::Body::absentLastHitMin = parseInt(atom, 6);
							break;
						case 8:
							Objsc::Body::absentLastHitMax = parseInt(atom, 24);
							break;
					}
					break;
				case LANG_LIST:
					receiveList(0, dynamic_cast<Objsc::LangList*>(atom));
					break;
			}
		}
	}
	
	
	
	void BodyPrefsObject::receiveList(int inlet, LangList* value)
	{
		string cmd = parseString(value->idx(0), "");
		if(cmd == "reset")	{
			Objsc::Body::resetThresholds();
			Objsc::Body::printThresholds();
		} else if(cmd == "print")	{
			Objsc::Body::printThresholds();
		}
	}
	
}

