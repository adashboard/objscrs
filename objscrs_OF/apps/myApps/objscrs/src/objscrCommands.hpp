#ifndef  OBJSCR_COMMANDS_H
#define OBJSCR_COMMANDS_H

// objscrs
#include "singleton.h"
#include "objscrPackets.hpp"

// STL
#include <iostream>
#include <string>
#include <vector>
#include <map>
using std::string;
using std::vector;
using std::map;

// shared_ptr
#include <tr1/memory>


namespace Objsc
{
	class Command
	{
		protected:
			string target;
			string name;
			Objsc::PacketPtr data;
		
		public:
			Command(string nname) : target(), name(nname), data(new Objsc::Packet()) {}
			Command(string ntarget, string nname) : target(ntarget), name(nname), data(new Objsc::Packet()) {}
			~Command() {}
			void setTarget(string label) {target.assign(label);}
			const string& getTarget() const {return target;}
			void setName(string label) {name.assign(label);}
			const string& getName() const {return name;}
			Objsc::PacketPtr& getData() { return data;} 
	};
	
	typedef std::tr1::shared_ptr<Objsc::Command> CommandPtr;
	typedef vector < Objsc::CommandPtr > CommandPtrVector;
	
	class CommandReceiver
	{
		public:
			virtual ~CommandReceiver() {}
			virtual void doCommand(Objsc::CommandPtr& cmd) =0;
			virtual void undoCommand(Objsc::CommandPtr& cmd) =0;
	};
	
	typedef std::tr1::shared_ptr<Objsc::CommandReceiver> CommandReceiverPtr;
	typedef map < string, Objsc::CommandReceiverPtr > CommandReceiverMap;
	typedef CommandReceiverMap::value_type CommandReceiverMapEntry;
	
	class CommandDispatcher : public Singleton<CommandDispatcher> 
	{
		private:
			Objsc::CommandReceiverMap receivers;
			Objsc::CommandPtrVector history;
			int historycursor;
			
		public:
			CommandDispatcher() : receivers(), history(), historycursor(0) {}
			~CommandDispatcher() {}
				
			void registerCommandReceiver(string label, CommandReceiver* ptr)	{
				receivers.insert(Objsc::CommandReceiverMapEntry(label, CommandReceiverPtr(ptr)));
			}
			
			void deregisterCommandReceiver(string label, CommandReceiver* ptr)	{
				receivers.erase(label);
			}
			
			static Objsc::CommandPtr createCommand(string target, string name)	{
				return Objsc::CommandPtr(new Objsc::Command(target, name));
			}
			
			void doCommand(Objsc::CommandPtr cmd)	{
				if(receivers.count(cmd->getTarget()) > 0)	{
					receivers[cmd->getTarget()]->doCommand(cmd);
				}
			}
			
			void doCommand(string target, string name)	{
				doCommand(Objsc::CommandPtr(new Objsc::Command(target, name)));
			}
			
			void undoCommand(Objsc::CommandPtr cmd)	{
				if(receivers.count(cmd->getTarget()) > 0)	{
					receivers[cmd->getTarget()]->undoCommand(cmd);
				}
			}
			
			void storeCommand(Objsc::CommandPtr cmd)	{
				history.push_back(cmd);
			}
			
			void undo() {
				
			}
			
			void redo() {
				
			}
	};
	
}

#endif // OBJSCR_COMMANDS_H
