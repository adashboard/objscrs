#include "objscrShapeFilter.hpp"

// objscrs
#include "objscrTimer.hpp"
#include "objscrConstants.hpp"
#include "objscrSettings.hpp"

namespace Objsc
{
	
	 const string Objsc::ShapeFilter::cmdlabel  = "_shapes_";
	
	
	ShapeFilter::ShapeFilter() :
			input(),
			active(false),
			colorImg(),
			grayImage(),
			grayBg(),
			grayDiff(),
			contourFinder(),
			threshold(80),
			inWidth(0),
			inHeight(0),
			continuous(false),
			resize(false),
			minRadius(30),
			maxRadius(2000)
	{
		// commands
		Objsc::CommandDispatcher::Current()->registerCommandReceiver(Objsc::ShapeFilter::cmdlabel , this);
		// settings
		if(Objsc::Settings::Current()->isLoaded())	{
			//printf("ShapeFilter - loading preferences\n");
			minRadius = Objsc::Settings::Current()->getTrackingMinArea();
			maxRadius = Objsc::Settings::Current()->getTrackingMaxArea();
			threshold = Objsc::Settings::Current()->getTrackingThreshold();
		}
	}
	
	void ShapeFilter::setInput(Objsc::FrameSourcePtr inputPtr) 
	{
		input=inputPtr;
		//~ inWidth=input->getWidth();
		//~ inHeight=input->getHeight();
		inWidth=VIDSOURCE_WIDTH;
		inHeight=VIDSOURCE_HEIGHT;
		if(input->getWidth() == PAL_WIDTH)	{
			inputImg.allocate(PAL_WIDTH,PAL_HEIGHT);
			resize = true;
		}
		colorImg.allocate(inWidth,inHeight);
		grayImage.allocate(inWidth,inHeight);
		grayBg.allocate(inWidth,inHeight);
		grayDiff.allocate(inWidth,inHeight);
	}
			
	void ShapeFilter::process()
	{
		//printf("ShapeFilter::process\n");
		if(input->hasNewFrame())	{
			//colorImg.setFromPixels(input->getFrame(),inWidth,inHeight);
			//~ colorImg.setFromPixels(input->getFrame(),720,576);
			//~ colorImg.resize(320,240);
			if(resize)	{
				inputImg.setFromPixels(input->getFrame(),PAL_WIDTH,PAL_HEIGHT);
				colorImg.scaleIntoMe(inputImg, CV_INTER_NN);
			} else {
				colorImg.setFromPixels(input->getFrame(),VIDSOURCE_WIDTH,VIDSOURCE_HEIGHT);
			}
			grayImage = colorImg;
			grayDiff.absDiff(grayBg, grayImage);
			grayDiff.threshold(threshold);

			// find contours which are between the size of 20 pixels and 1/3 the w*h pixels.
			// also, find holes is set to true so we will get interior contours as well....
			// old max radius: (inWidth*inHeight)/3
			contourFinder.findContours(grayDiff, minRadius, maxRadius, 10, false);	// find holes
			if(continuous)
				grayBg = grayImage;
		}
	}
	
	void ShapeFilter::start() 
	{ 
		active=true;
		//~ colorImg.setFromPixels(input->getFrame(),inWidth,inHeight);
		//~ grayBg = colorImg;
		if(resize)	{
			inputImg.setFromPixels(input->getFrame(),PAL_WIDTH,PAL_HEIGHT);
			colorImg.scaleIntoMe(inputImg, CV_INTER_NN);
		} else {
			colorImg.setFromPixels(input->getFrame(),VIDSOURCE_WIDTH,VIDSOURCE_HEIGHT);
		}
		grayImage = colorImg;
		grayBg = grayImage;
	}
	
	const ofxCvBlob& ShapeFilter::getShape(int idx)
	{
		return contourFinder.blobs[idx];
	}
	
	
	void ShapeFilter::draw()
	{
		ofSetColor(0xffffff);
		colorImg.draw(0,0);
		grayDiff.draw(320,0);
		ofFill();
		//~ ofSetColor(0x333333);
		//~ ofRect(0,0,320,240);
		ofSetColor(0xffffff);
		for (int i = 0; i < contourFinder.nBlobs; i++){
			contourFinder.blobs[i].draw(0,0);
		}
		ofFill();
	}
	
	void ShapeFilter::takeRef()
	{
		//printf("ShapeFilter::takeRef\n");
		grayBg = grayImage;
	}
	
	void ShapeFilter::startRef()
	{
		//printf("ShapeFilter::startRef\n");
		continuous=true;
		grayBg = grayImage;
	}
	
	void ShapeFilter::stopRef()
	{
		//printf("ShapeFilter::stopRef\n");
		continuous=false;
	}
	
	// commands
	void ShapeFilter::doCommand(Objsc::CommandPtr& cmd)
	{
		//std::cout << "ShapeFilter::doCommand: " << cmd->getName() << std::endl;
		
		string cmdname = cmd->getName();
		if(cmdname ==  "proc_on")	{
			start();
		} else if(cmdname ==  "proc_off")	{
			stop();
		} else if(cmdname ==  "ref_on")	{
			startRef();
		} else if(cmdname ==  "ref_off")	{
			stopRef();
		} else if(cmdname ==  "ref_take")	{
			takeRef();
		}
	}
	
	void ShapeFilter::undoCommand(Objsc::CommandPtr& cmd)
	{
		std::cout << "ShapeFilter::undoCommand: " << cmd->getName() << std::endl;
	}
}

