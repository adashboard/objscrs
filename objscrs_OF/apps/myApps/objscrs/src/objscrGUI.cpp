#include "objscrGUI.hpp"


// objscrs
#include "objscrImage.hpp"
#include "objscrGUIStyles.hpp"
#include "objscrApp.hpp"

#include <ctype.h>


namespace Objsc
{
	////////////////////////////////////////
	// FUNCTIONS
	////////////////////////////////////////
	void outlineBox(Objsc::Rect& rect)
	{
		ofLine(rect.getX(), rect.getY(), rect.getRight(), rect.getY());
		ofLine(rect.getX(), rect.getBottom(), rect.getRight(), rect.getBottom());
		ofLine(rect.getX(), rect.getY(), rect.getX(), rect.getBottom());
		ofLine(rect.getRight(), rect.getY(), rect.getRight(), rect.getBottom());
	}

	void outlineBox(float x1, float y1, float x2, float y2)
	{
		ofLine(x1,y1,x1,y2);
		ofLine(x2,y1,x2,y2);
		ofLine(x1,y1,x2,y1);
		ofLine(x1,y2,x2,y2);
	}
	
	void crossXLines(Objsc::Rect& rect)
	{
		ofLine(rect.getX(), rect.getY(), rect.getRight(), rect.getBottom());
		ofLine(rect.getRight(), rect.getY(), rect.getX(), rect.getBottom());
	}

	////////////////////////////////////////
	// HORIZONTAL SLIDER
	////////////////////////////////////////
	// display
	void HorizontalSlider::draw()
	{
		if (visible)	{
			ofSetColor(bgColour.r, bgColour.g, bgColour.b, bgColour.a);
			ofRect(bounds.getX(), bounds.getY(), bounds.getWidth(), bounds.getHeight());
			if(showcentre)	{
				ofSetColor(fgColour.r, fgColour.g, fgColour.b, 120);
				ofLine(scrollrect.getCentreX(), scrollrect.getY(), scrollrect.getCentreX(), scrollrect.getBottom());
			}
			if(dodrag)	{
				ofSetColor(hiColour.r, hiColour.g, hiColour.b, hiColour.a);
			} else {
				ofSetColor(fgColour.r, fgColour.g, fgColour.b, fgColour.a);
			}
			ofRect(sliderect.getX(), sliderect.getY(), sliderect.getWidth(), sliderect.getHeight());
			ofSetColor(fgColour.r, fgColour.g, fgColour.b, 120);
			ofLine(sliderect.getCentreX(), sliderect.getY(), sliderect.getCentreX(), sliderect.getBottom());
		}
	}

	// scroll
	void HorizontalSlider::scrollUpdated()
	{
		float fvalue = (sliderect.getCentreX()-scrollrect.getX())/scrollrect.getWidth();
		value=fvalue<1.0f?(fvalue>0.0f?fvalue:0.0f):1.0f;
	}

	// events
	void HorizontalSlider::keyPressed(int key)
	{
		
	}

	void HorizontalSlider::keyReleased(int key)
	{
		
	}

	void HorizontalSlider::mouseMoved(int x, int y )
	{
		if(sliderect.containsPoint(x,y))	{
			dodrag=true;
		} else {
			if(dodrag)
				dodrag=false;
		}
	}

	void HorizontalSlider::mouseDragged(int x, int y, int button)
	{
		if(withinBounds(x,y))	{
			sliderect.setCentreX(MAX(MIN(x,scrollrect.getRight()), scrollrect.getX()));
			scrollUpdated();
		}
	}

	void HorizontalSlider::mousePressed(int x, int y, int button)
	{
		if(withinBounds(x,y))	{
			if(sliderect.containsPoint(x,y))	{
				dodrag=true;
			} else {
				if(dodrag) {
					dodrag=false;
				}
				if(glutGetModifiers() == GLUT_ACTIVE_CTRL || button==Objsc::MOUSERIGHT)	{
					if(showcentre) {
						setValue(0.5f);
					}
				}
			}
		} else {
			if(dodrag)
				dodrag=false;
		}
	}

	void HorizontalSlider::mouseReleased(int x, int y, int button)
	{
		if(dodrag)
			dodrag=false;
	}


	////////////////////////////////////////
	// RANGE SLIDER
	////////////////////////////////////////
	void RangeSlider::draw()
	{
		if (visible)	{
			ofSetColor(bgColour.r, bgColour.g, bgColour.b, bgColour.a);
			ofRect(bounds.getX(), bounds.getY(), bounds.getWidth(), bounds.getHeight());
			if(showcentre)	{
				ofSetColor(fgColour.r, fgColour.g, fgColour.b, 120);
				ofLine(bounds.getCentreX(), bounds.getY(), bounds.getCentreX(), bounds.getBottom());
			}
			if(dodrag)	{
				ofSetColor(hiColour.r, hiColour.g, hiColour.b, hiColour.a);
			} else {
				ofSetColor(fgColour.r, fgColour.g, fgColour.b, fgColour.a);
			}
			ofRect(sliderect.getX(), sliderect.getY(), sliderect.getWidth(), sliderect.getHeight());
			ofSetColor(fgColour.r, fgColour.g, fgColour.b, 120);
			ofLine(sliderect.getX(), sliderect.getY(), sliderect.getX(), sliderect.getBottom());
			ofLine(sliderect.getRight(), sliderect.getY(), sliderect.getRight(), sliderect.getBottom());
		}
	}

	// scroll
	void RangeSlider::scrollUpdated()
	{
		minValue=posToScale(sliderect.getLeft());
		maxValue=posToScale(sliderect.getRight());
	}

	// events
	void RangeSlider::mouseMoved(int x, int y )
	{
		if(bounds.containsPoint(x,y))	{
			dodrag=true;
		} else {
			if(dodrag)
				dodrag=false;
		}
	}

	void RangeSlider::mouseDragged(int x, int y, int button)
	{
		if(withinBounds(x,y))	{
			if(x < sliderect.getCentreX())	{
				sliderect.stretchSides(0.0f, 0.0f, 0.0f, x-sliderect.getX());
			} else if(x > sliderect.getCentreX())	{
				sliderect.stretchSides(0.0f, x-sliderect.getRight(), 0.0f, 0.0f);
			}
			scrollUpdated();
		}
	}

	void RangeSlider::mousePressed(int x, int y, int button)
	{
		if(withinBounds(x,y))	{
			if(sliderect.containsPoint(x,y))	{
				dodrag=true;
			} else {
				if(dodrag) {
					dodrag=false;
				}
				if(glutGetModifiers() == GLUT_ACTIVE_CTRL || button==Objsc::MOUSERIGHT)	{
					if(showcentre) {
						setMinValue(0.0f);
						setMaxValue(1.0f);
					}
				}
			}
		} else {
			if(dodrag)
				dodrag=false;
		}
	}


	////////////////////////////////////////
	// IMAGE
	////////////////////////////////////////
	void Image::paint()
	{
		if(loaded)	{
			colpix = new unsigned char[pWidth*pHeight*4];
			int pos;
			for (int x = 0; x < pWidth; x++){
				for (int y = 0; y < pHeight; y++){
					pos= (y*(pWidth*4)) + (x * 4);
					colpix[pos] =  (unsigned char)(colour.r);
					colpix[pos+1] =  (unsigned char)(colour.g);
					colpix[pos+2] =  (unsigned char)(colour.b);
					colpix[pos+3] =  pixels[y*pWidth+x];
				}
			}
			texture.clear();
			texture.allocate(pWidth, pHeight, GL_RGBA);
			texture.loadData(colpix, pWidth, pHeight, GL_RGBA);
		}
	}

	void Image::load(string imgname, int width, int height)
	{
		name = imgname;
		pWidth = width;
		pHeight = height;
		bounds.setDimensions(pWidth,pHeight);
		pixels = new unsigned char[pWidth*pHeight];
		Objsc::ImageProducer::Current()->fillPixels(name, pixels, pWidth, pHeight);
		loaded=true;
	}

	void Image::resize(int width, int height)
	{
		if(loaded)	{
			pWidth = width;
			pHeight = height;
			bounds.setDimensions(pWidth,pHeight);
			pixels = new unsigned char[pWidth*pHeight];
			Objsc::ImageProducer::Current()->fillPixels(name, pixels, pWidth, pHeight);
			paint();
		} else {
			bounds.setDimensions(pWidth,pHeight);
		}
	}

	// runtime
	void Image::draw()
	{
		if(visible && loaded)	{
			ofSetColor(0xFFFFFF);
			texture.draw(bounds.getX(), bounds.getY(), bounds.getWidth(), bounds.getHeight());
		}
	}


	////////////////////////////////////////
	// GUI CONTAINER
	////////////////////////////////////////
	// events
	void GuiContainer::keyPressed(int key)
	{
		
	}

	void GuiContainer::keyReleased(int key)
	{
		
	}

	void GuiContainer::mouseMoved(int x, int y )
	{
		for (Objsc::GuiBaseVector::iterator child = children.begin(); child != children.end(); ++child)	{
			(*child)->mouseMoved(x,y);
		}
	}

	void GuiContainer::mouseDragged(int x, int y, int button)
	{
		
	}

	void GuiContainer::mousePressed(int x, int y, int button)
	{
		consumed = false;
		for (Objsc::GuiBaseVector::iterator child = children.begin(); child != children.end(); ++child)	{
			(*child)->mousePressed(x,y,button);
			if((*child)->eventConsumed())	{
				consumed = true;
				return;
			}
		}
	}

	void GuiContainer::mouseReleased(int x, int y, int button)
	{
		
	}

	void GuiContainer::windowResized(int w, int h)
	{
		
	}

	////////////////////////////////////////
	// LAYER
	////////////////////////////////////////
	Objsc::ContextMenu* GuiLayer::createContextMenu(float x, float y, Objsc::ContextMenuSource* source)
	{
		Objsc::ContextMenu* menu = new Objsc::ContextMenu();
		//~ menu->setStyleName("default");
		//~ source->buildContextMenu(menu);
		//~ menu->enable();
		//~ menu->setLocation(x,y);
		//~ root = (Objsc::GuiContainer*)menu;
		return menu;
	}

	Objsc::ContextMenu* GuiLayer::createContextMenu(float x, float y, Objsc::ContextMenuSource* source, string info)
	{
		Objsc::ContextMenu* menu = new Objsc::ContextMenu();
		//~ menu->setStyleName("default");
		//~ source->buildContextMenu(menu, info);
		//~ menu->enable();
		//~ menu->setLocation(x,y);
		//~ root = (Objsc::GuiContainer*)menu;
		return menu;
	}
	
	void GuiLayer::setRootWidget(Objsc::GuiBasePtr containerPtr) 
	{ 
		root = containerPtr; 
	}
	
	bool GuiLayer::withinBounds(float x, float y)
	{ 	
		if(root) {
			if(root->isModal())	{
				return true;
			} else {
				return root->withinBounds(x,y); 
			}
		} else {
			return false;
		}
	}
	
	void GuiLayer::update()
	{
		if(active && root)
			root->update();
	}
	void GuiLayer::draw()
	{
		if(active && root) {
			root->draw();
		}
	}

	void GuiLayer::close()
	{
		if(root)
			root->disable();
		disable();
	}

	// events
	void GuiLayer::keyPressed(int key)
	{
		consumed = false;
		if(active && root) {
			root->keyPressed(key);
			consumed = root->eventConsumed();
		}
	}

	void GuiLayer::keyReleased(int key)
	{
		consumed = false;
		if(active && root) {
			root->keyReleased(key);
			consumed = root->eventConsumed();
		}
	}

	void GuiLayer::mouseMoved(int x, int y )
	{
		if(active && root)
			root->mouseMoved(x,y);
	}

	void GuiLayer::mouseDragged(int x, int y, int button)
	{
		consumed = false;
		if(active && root)	{
			if(withinBounds(x,y))	{
				root->mouseDragged(x,y,button);
				consumed = root->eventConsumed();
			}
		}
	}

	void GuiLayer::mousePressed(int x, int y, int button)
	{
		consumed = false;
		if(active && root)	{
			if(withinBounds(x,y))	{
				root->mousePressed(x,y,button);
				consumed = root->eventConsumed();
			} 
		}
	}

	void GuiLayer::mouseReleased(int x, int y, int button)
	{
		consumed = false;
		if(active && root)	{
			root->mouseReleased(x,y,button);
			consumed = root->eventConsumed();
		}
	}

	void GuiLayer::windowResized(int w, int h)
	{
		if(active && root)	{
			root->windowResized(w,h);
		}
	}


	////////////////////////////////////////
	// LABEL
	////////////////////////////////////////
	void Label::setText(char* newtext) 
	{
		text.assign(newtext);
		if((font->stringWidth(text) + Objsc::PADWIDTH*2) > bounds.getWidth())
			bounds.setWidth(font->stringWidth(text) + Objsc::PADWIDTH*2);
		if (bounds.getHeight() < font->getLineHeight()) 
			bounds.setHeight(font->getLineHeight());
	}

	void Label::setText(const string& newtext)
	{
		text = newtext;
		if((font->stringWidth(text) + Objsc::PADWIDTH*2) > bounds.getWidth())
			bounds.setWidth(font->stringWidth(text) + Objsc::PADWIDTH*2);
		if (bounds.getHeight() < font->getLineHeight()) 
			bounds.setHeight(font->getLineHeight());
	}

	void Label::clearText()
	{
		text.clear();
	}
			
	void Label::draw()
	{
		if (visible)	{
			ofSetColor(bgColour.r, bgColour.g, bgColour.b, bgColour.a);
			ofRect(bounds.getX(), bounds.getY(), bounds.getWidth(), bounds.getHeight());
			ofSetColor(textColour.r, textColour.g, textColour.b);
			font->drawString(text, bounds.getX(), bounds.getY() +baseline);
		}
	}

	////////////////////////////////////////
	// TEXT FIELD
	////////////////////////////////////////
	void TextField::_setFont()
	{
		if (font->bLoadedOk)	{
			baseline = font->stringHeight("Aljpy");
			//if (bounds.getHeight() < baseline) 
			bounds.setHeight(baseline+Objsc::PADHEIGHT*2);
		} else {
			printf("ERROR: font not loaded\n");
		}
		if(sizewithtext)	{
			if(text.empty())	{
				if(hasMinSize)	{
					bounds.setWidth(font->stringWidth("A"));
				} else {
					bounds.setWidth(0);
				}
			} else {
				bounds.setWidth(font->stringWidth(text)+5);
			}
		}
	}
	
			
	void TextField::resetCursor()
	{
		cursorRect.setY(bounds.getY()+1);
		cursorRect.setHeight(bounds.getHeight()-2);
		cursorRect.setX(bounds.getX()+font->stringWidth(text));
	};
	
	void TextField::setText(char* newtext) 
	{
		text.assign(newtext);
		if (bounds.getHeight() < font->getLineHeight()) 
			bounds.setHeight(font->getLineHeight());
		cursor=text.size();
		cursorRect.setHeight(bounds.getHeight()-2);
		cursorRect.setX(bounds.getX()+font->stringWidth(text));
		if(sizewithtext)	{
			if(text.empty())	{
				if(hasMinSize)	{
					bounds.setWidth(font->stringWidth("A"));
				} else {
					bounds.setWidth(0);
				}
			} else {
				bounds.setWidth(font->stringWidth(text)+5);
			}
		}
	}

	void TextField::setText(const string& newtext)
	{
		text = newtext;
		if (bounds.getHeight() < font->getLineHeight()) 
			bounds.setHeight(font->getLineHeight());
		cursor=text.size();
		cursorRect.setHeight(bounds.getHeight()-2);
		cursorRect.setX(bounds.getX()+font->stringWidth(text));
		if(sizewithtext)	{
			if(text.empty())	{
				if(hasMinSize)	{
					bounds.setWidth(font->stringWidth("A"));
				} else {
					bounds.setWidth(0);
				}
			} else {
				bounds.setWidth(font->stringWidth(text)+5);
			}
		}
	}

	void TextField::clearText()
	{
		text.clear();
		cursor=0;
		cursorRect.setX(bounds.getX());
		if(sizewithtext)	{
			if(hasMinSize)	{
				bounds.setWidth(font->stringWidth("A"));
			} else {
				bounds.setWidth(0);
			}
		}
	}
			
	void TextField::draw()
	{
		if (visible)	{
			//printf("TextField::draw <%s> %d\n", text.c_str(), drawbg);
			if(drawbg) {
				ofSetColor(bgColour.r, bgColour.g, bgColour.b, bgColour.a);
				ofRect(bounds.getX(), bounds.getY(), bounds.getWidth(), bounds.getHeight());
			}
			ofSetColor(textColour.r, textColour.g, textColour.b);
			font->drawString(text, bounds.getX(), bounds.getY() +baseline);
			if(hasFocus && editable)
				ofRect(cursorRect.getX(), cursorRect.getY(), cursorRect.getWidth(), cursorRect.getHeight());
		}
	}

	// actions
	void TextField::setAction(Objsc::CommandPtr actionPtr)
	{
		action = actionPtr;
	}

	void TextField::activate()
	{
		if(action)	{
			Objsc::CommandDispatcher::Current()->doCommand(action);
		}
	}

	// events
	void TextField::keyPressed(int key)
	{
		if(visible && hasFocus)	{
			if(key == OF_KEY_RETURN)	{
				if(text.size() > 0)	{
					activate();
				}
			} else if(key == OF_KEY_BACKSPACE)	{
				if(cursor > 0) {
					cursor--;
					text.erase(cursor, 1);
				}
			} else if(key == OF_KEY_RIGHT)	{
				if(cursor < text.size()) cursor++;
			} else if(key == OF_KEY_LEFT)	{
				if(cursor > 0) cursor--;
			} else if(key == OF_KEY_UP || key == OF_KEY_DOWN)	{
				//if(cursor > 0) cursor--;
			} else if(isgraph(key)){
				if(maxchars == 0) {
					input[0] = key;
					text.insert(cursor, input, 1);
					cursor++;
				} else if(text.size() < maxchars) {
					input[0] = key;
					text.insert(cursor, input, 1);
					cursor++;
				}
			} else if(isspace(key)){
				if(maxchars == 0) {
					input[0] = key;
					text.insert(cursor, "  ", 1);
					cursor++;
				} else if(text.size() < maxchars) {
					input[0] = key;
					text.insert(cursor, "  ", 1);
					cursor++;
				}
			} else {
				if(maxchars != 0) {
					text.insert(cursor, "??", 1);
					cursor++;
				} else if(text.size() < maxchars) {
					text.insert(cursor, "??", 1);
					cursor++;
				}
			}
			if(cursor>0) 	{
				cursorRect.setX(bounds.getX()+font->stringWidth(text.substr(0,cursor)));
			} else {
				cursorRect.setX(bounds.getX());
			}
			if(sizewithtext)	{
				if(font->stringWidth(text) > bounds.getWidth())	{
					bounds.setWidth(font->stringWidth(text));
				}
			}
		}
	}

	void TextField::mouseMoved(int x, int y)
	{
		if(withinBounds(x,y) && visible)	{
			if(!hasFocus && displayState!=Objsc::DISABLED) {
				gainFocus();
			}
		} else {
			if(hasFocus && displayState!=Objsc::DISABLED) {
				looseFocus();
			}
		}
	}

	void TextField::mousePressed(int x, int y, int button)
	{
		
	}
	
	////////////////////////////////////////
	// NUMBER FIELD
	////////////////////////////////////////
	void NumberField::keyPressed(int key)
	{
		if(visible && hasFocus)	{
			if(key == OF_KEY_RETURN)	{
				if(text.size() > 0)	{
					activate();
				}
			} else if(key == OF_KEY_BACKSPACE)	{
				if(cursor > 0) {
					cursor--;
					text.erase(cursor, 1);
				}
			} else if(key == OF_KEY_RIGHT)	{
				if(cursor < text.size()) cursor++;
			} else if(key == OF_KEY_LEFT)	{
				if(cursor > 0) cursor--;
			} else if(key == OF_KEY_UP || key == OF_KEY_DOWN)	{
				//if(cursor > 0) cursor--;
			} else if(isdigit(key)){
				if(maxchars == 0) {
					input[0] = key;
					text.insert(cursor, input, 1);
					cursor++;
				} else if(text.size() < maxchars) {
					input[0] = key;
					text.insert(cursor, input, 1);
					cursor++;
				}
			}
			if(cursor>0) 	{
				cursorRect.setX(bounds.getX()+font->stringWidth(text.substr(0,cursor)));
			} else {
				cursorRect.setX(bounds.getX());
			}
			if(sizewithtext)	{
				if(font->stringWidth(text) > bounds.getWidth())	{
					bounds.setWidth(font->stringWidth(text));
				}
			}
		}
	}
	
	////////////////////////////////////////
	// TIME DISPLAY
	////////////////////////////////////////
	void TimeDisplay::draw()
	{
		if (visible)	{
			ofSetColor(textColour.r, textColour.g, textColour.b);
			font->drawString(text, bounds.getX(), bounds.getY()+baseline);
		}
	}

	////////////////////////////////////////
	// BUTTON
	////////////////////////////////////////
	void Button::draw()
	{
		if (visible)	{
			if(hasFocus)	{
				ofSetColor(hiColour.r, hiColour.g, hiColour.b, hiColour.a);
			} else {
				ofSetColor(bgColour.r, bgColour.g, bgColour.b, bgColour.a);
			}
			ofRect(bounds.getX(), bounds.getY(), bounds.getWidth(), bounds.getHeight());
			ofSetColor(textColour.r, textColour.g, textColour.b);
			font->drawString(text, bounds.getX()+textX, bounds.getY()+baseline);
		}
	}

	// text
	void Button::setText(char* newtext) 
	{
		text.assign(newtext);
		if((font->stringWidth(text) + Objsc::PADWIDTH*2) > bounds.getWidth())
			bounds.setWidth(font->stringWidth(text) + Objsc::PADWIDTH*2);
		if (bounds.getHeight() < font->getLineHeight()) 
			bounds.setHeight(font->getLineHeight());
		textX = (bounds.getWidth()-(font->stringWidth(text) + Objsc::PADWIDTH))/2;
	}

	void Button::setText(const string& newtext)
	{
		text = newtext;
		if((font->stringWidth(text) + Objsc::PADWIDTH*2) > bounds.getWidth())
			bounds.setWidth(font->stringWidth(text) + Objsc::PADWIDTH*2);
		if (bounds.getHeight() < font->getLineHeight()) 
			bounds.setHeight(font->getLineHeight());
		textX = (bounds.getWidth()-(font->stringWidth(text) + Objsc::PADWIDTH))/2;
	}

	void Button::clearText()
	{
		text.clear();
		textX=0.0f;
	}

	// actions
	void Button::setAction(Objsc::CommandPtr actionPtr)
	{
		action = actionPtr;
	}

	void Button::activate()
	{
		if(action)	{
			Objsc::CommandDispatcher::Current()->doCommand(action);
		}
	}

	// events
	void Button::keyPressed(int key)
	{
		
		
	}

	void Button::keyReleased(int key)
	{
		
	}

	void Button::mouseMoved(int x, int y )
	{
		if(withinBounds(x,y) && visible)	{
			if(!hasFocus && displayState!=Objsc::DISABLED) {
				gainFocus();
			}
		} else {
			if(hasFocus && displayState!=Objsc::DISABLED) {
				looseFocus();
			}
		}
	}

	void Button::mouseDragged(int x, int y, int button)
	{
		
	}

	void Button::mousePressed(int x, int y, int button)
	{
		if(bounds.containsPoint(x,y) && hasFocus)	{
			activate();
		}
	}

	void Button::mouseReleased(int x, int y, int button)
	{
		
	}

	void Button::windowResized(int w, int h)
	{
		
	}

	////////////////////////////////////////
	// HOT SPOT
	////////////////////////////////////////
	void HotSpot::draw()
	{
		if (visible)	{
			ofSetColor(bgColour.r, bgColour.g, bgColour.b, 60);
			ofRect(bounds.getX(), bounds.getY(), bounds.getWidth(), bounds.getHeight());
			ofNoFill();
			ofSetColor(255,255,255,120);
			ofRect(bounds.getX(), bounds.getY(), bounds.getWidth(), bounds.getHeight());
			ofFill();
		}
	}


	// events
	void HotSpot::keyPressed(int key)
	{
		
		
	}

	void HotSpot::keyReleased(int key)
	{
		
	}

	void HotSpot::mouseMoved(int x, int y )
	{
		if(withinBounds(x,y))	{
			if(!hasFocus && displayState!=Objsc::DISABLED) {
				gainFocus();
				show();
				//Objsc::App::Current()->showCursor();
			}
		} else {
			if(hasFocus && displayState!=Objsc::DISABLED) {
				looseFocus();
				hide();
				//Objsc::App::Current()->resetCursor();
			}
		}
	}

	void HotSpot::mouseDragged(int x, int y, int button)
	{
		
	}

	void HotSpot::mousePressed(int x, int y, int button)
	{
		if(bounds.containsPoint(x,y))	{
			activate();
		}
	}

	void HotSpot::mouseReleased(int x, int y, int button)
	{
		if(hasFocus) {
			looseFocus();
			hide();
		}
	}

	void HotSpot::windowResized(int w, int h)
	{
		
	}

	////////////////////////////////////////
	// MENU ITEM
	////////////////////////////////////////
	// children
	Objsc::MenuItem* MenuItem::addItem(string name)
	{
		children.push_back(new Objsc::MenuItem());
		Objsc::MenuItem* menuItem = (Objsc::MenuItem*)children.back();
		menuItem->setParent(this);
		Objsc::StylePalette::Current()->applyStyle(stylename, menuItem);
		menuItem->setText(name);
		float maxwidth=0.0f;
		for (Objsc::GuiBaseVector::iterator child = children.begin(); child != children.end(); ++child)	{
			if((*child)->getWidth()>maxwidth)	{
				maxwidth=(*child)->getWidth();
			}
		}
		for (Objsc::GuiBaseVector::iterator child = children.begin(); child != children.end(); ++child)	{
			(*child)->setWidth(maxwidth);
		}
		return menuItem;
	}

	// geom
	void MenuItem::setLocation(float x, float y) 
	{ 
		bounds.setLocation(x,y);
		childbounds.setLocation(bounds.getRight(),bounds.getY());
		childbounds.setDimensions(0.0f,0.0f);
		if(children.size() > 0)	{
			float yspace = 0.0f;
			x = bounds.getRight()+1;
			for (Objsc::GuiBaseVector::iterator child = children.begin(); child != children.end(); ++child)	{
				(*child)->setLocation(x,y+yspace);
				yspace+=(*child)->getHeight()+1;
				childbounds.growHeight((*child)->getHeight()+1);
				if((*child)->getWidth() > childbounds.getWidth()) {
					childbounds.setWidth((*child)->getWidth());
				}
			}
		}
	}


	bool MenuItem::withinBounds(float x, float y)
	{ 
		if(hasFocus) {
			if(bounds.containsPoint(x,y) || childbounds.containsPoint(x,y))	{
				return true;
			} else {
				for (Objsc::GuiBaseVector::iterator child = children.begin(); child != children.end(); ++child)	{
					if((*child)->withinBounds(x,y))	{
						return true;
					}
				}
				return false;
			}
		} else {
			return bounds.containsPoint(x,y);
		}
	}

	void MenuItem::draw()
	{
		if (visible)	{
			if(hasFocus)	{
				ofSetColor(hiColour.r, hiColour.g, hiColour.b, hiColour.a);
			} else {
				ofSetColor(bgColour.r, bgColour.g, bgColour.b, bgColour.a);
			}
			ofRect(bounds.getX(), bounds.getY(), bounds.getWidth(), bounds.getHeight());
			ofSetColor(textColour.r, textColour.g, textColour.b);
			font->drawString(text, bounds.getX(), bounds.getY()+baseline);
			if(hasFocus && children.size() > 0)	{
				for (Objsc::GuiBaseVector::iterator child = children.begin(); child != children.end(); ++child)	{
					(*child)->draw();
				}
			}
		}
	}


	// events
	void MenuItem::keyPressed(int key)
	{
		
		
	}

	void MenuItem::keyReleased(int key)
	{
		
	}

	void MenuItem::mouseMoved(int x, int y )
	{
		if(withinBounds(x,y) && visible)	{
			if(!hasFocus && displayState!=Objsc::DISABLED) {
				gainFocus();
			}
			for (Objsc::GuiBaseVector::iterator child = children.begin(); child != children.end(); ++child)	{
				(*child)->mouseMoved(x,y);
			}
		} else {
			if(hasFocus && displayState!=Objsc::DISABLED) {
				looseFocus();
			}
		}
	}

	void MenuItem::mouseDragged(int x, int y, int button)
	{
		
	}

	void MenuItem::mousePressed(int x, int y, int button)
	{
		consumed = false;
		if(bounds.containsPoint(x,y) && hasFocus)	{
			consumed = true;
			activate();
			return;
		}
		if(children.size() > 0 && hasFocus)	{
			for (Objsc::GuiBaseVector::iterator child = children.begin(); child != children.end(); ++child)	{
				(*child)->mousePressed(x,y,button);
				if((*child)->eventConsumed())	{
					consumed = true;
					return;
				}
			}
		}
	}

	void MenuItem::mouseReleased(int x, int y, int button)
	{
		
	}

	void MenuItem::windowResized(int w, int h)
	{
		
	}


	////////////////////////////////////////
	// GRAPHIC MENU ITEM
	////////////////////////////////////////
	void GraphicMenuItem::setIcon(string imgname)
	{
		icon.load(imgname, bounds.getWidth(), bounds.getHeight());
		icon.setColour(Objsc::WHITE);
	}

	Objsc::GraphicMenuItem* GraphicMenuItem::addItem(string name, string imgname)
	{
		children.push_back(new Objsc::GraphicMenuItem());
		Objsc::GraphicMenuItem* menuItem = (Objsc::GraphicMenuItem*)children.back();
		menuItem->setParent(this);
		Objsc::StylePalette::Current()->applyStyle(stylename, menuItem);
		menuItem->setText(name);
		menuItem->setIcon(imgname);
		float maxheight=0.0f;
		for (Objsc::GuiBaseVector::iterator child = children.begin(); child != children.end(); ++child)	{
			if((*child)->getHeight()>maxheight)	{
				maxheight=(*child)->getHeight();
			}
		}
		for (Objsc::GuiBaseVector::iterator child = children.begin(); child != children.end(); ++child)	{
			(*child)->setHeight(maxheight);
		}
		return menuItem;
	}

	// geom
	void GraphicMenuItem::setLocation(float x, float y) 
	{ 
		bounds.setLocation(x,y);
		icon.setCentre(bounds.getCentreX(),bounds.getCentreY());
		childbounds.setLocation(bounds.getRight(),bounds.getY());
		childbounds.setDimensions(0.0f,0.0f);
		if(children.size() > 0)	{
			float yspace = 0.0f;
			x = bounds.getRight()+1;
			for (Objsc::GuiBaseVector::iterator child = children.begin(); child != children.end(); ++child)	{
				(*child)->setLocation(x,y+yspace);
				yspace+=(*child)->getHeight()+1;
				childbounds.growHeight((*child)->getHeight()+1);
				if((*child)->getWidth() > childbounds.getWidth()) {
					childbounds.setWidth((*child)->getWidth());
				}
			}
		}
	}


	void GraphicMenuItem::draw()
	{
		if (visible)	{
			if(hasFocus)	{
				ofSetColor(hiColour.r, hiColour.g, hiColour.b, hiColour.a);
			} else {
				ofSetColor(bgColour.r, bgColour.g, bgColour.b, bgColour.a);
			}
			ofRect(bounds.getX(), bounds.getY(), bounds.getWidth(), bounds.getHeight());
			icon.draw();
			if(hasFocus && children.size() > 0)	{
				for (Objsc::GuiBaseVector::iterator child = children.begin(); child != children.end(); ++child)	{
					(*child)->draw();
				}
			}
		}
	}

	////////////////////////////////////////
	// CONTEXT MENU
	////////////////////////////////////////
	// items
	Objsc::MenuItem* ContextMenu::addItem(const string& name)
	{
		children.push_back(new Objsc::MenuItem());
		Objsc::MenuItem* menuItem = (Objsc::MenuItem*)children.back();
		menuItem->setParent(this);
		Objsc::StylePalette::Current()->applyStyle(stylename, menuItem);
		menuItem->setText(name);
		bounds.growHeight(menuItem->getHeight()+1);
		if(menuItem->getWidth() > bounds.getWidth())	{
			bounds.setWidth(menuItem->getWidth());
			for (Objsc::GuiBaseVector::iterator child = children.begin(); child != children.end(); ++child)	{
				(*child)->setWidth(bounds.getWidth());
			}
		} else {
			menuItem->setWidth(bounds.getWidth());
		}
		return menuItem;
	}
			
	// geom
	void ContextMenu::setLocation(float x, float y) 
	{ 
		if(y+bounds.getHeight()>ofGetHeight())	{
			y=ofGetHeight()-bounds.getHeight();
		}
		if(x+bounds.getWidth()>ofGetWidth())	{
			x=ofGetWidth()-bounds.getWidth();
		}
		bounds.setLocation(x,y);
		float yspace = 0;
		for (Objsc::GuiBaseVector::iterator child = children.begin(); child != children.end(); ++child)	{
			(*child)->setLocation(x,y+yspace);
			yspace+=(*child)->getHeight()+1;
		}
	}



	bool ContextMenu::withinBounds(float x, float y)
	{ 
		if(bounds.containsPoint(x,y)) {
			return true;
		} else {
			for (Objsc::GuiBaseVector::iterator child = children.begin(); child != children.end(); ++child)	{
				if((*child)->withinBounds(x,y))
					return true;
			}
		}
		return false;
	}
	
	void ContextMenu::reset()
	{
		for (Objsc::GuiBaseVector::iterator child = children.begin(); child != children.end(); ++child)	{
			(*child)->looseFocus();
		}
	}

	////////////////////////////////////////
	// GRAPHIC MENU
	////////////////////////////////////////
	Objsc::GraphicMenuItem* GraphicMenu::addItem(string name, string imgname)
	{
		children.push_back(new Objsc::GraphicMenuItem());
		Objsc::GraphicMenuItem* menuItem = (Objsc::GraphicMenuItem*)children.back();
		menuItem->setParent(this);
		Objsc::StylePalette::Current()->applyStyle(stylename, menuItem);
		menuItem->setText(name);
		menuItem->setDimensions(24,24);
		menuItem->setIcon(imgname);
		bounds.growWidth(menuItem->getWidth()+1);
		if(menuItem->getHeight() > bounds.getHeight())	{
			bounds.setHeight(menuItem->getHeight());
			for (Objsc::GuiBaseVector::iterator child = children.begin(); child != children.end(); ++child)	{
				(*child)->setHeight(bounds.getHeight());
			}
		} else {
			menuItem->setHeight(bounds.getHeight());
		}
		return menuItem;
	}
			
	// geom
	void GraphicMenu::setLocation(float x, float y) 
	{ 
		if(y+bounds.getHeight()>ofGetHeight())	{
			y=ofGetHeight()-bounds.getHeight();
		}
		if(x+bounds.getWidth()>ofGetWidth())	{
			x=ofGetWidth()-bounds.getWidth();
		}
		bounds.setLocation(x,y);
		float xspace = 0;
		for (Objsc::GuiBaseVector::iterator child = children.begin(); child != children.end(); ++child)	{
			(*child)->setLocation(x+xspace,y);
			xspace+=(*child)->getWidth()+1;
		}
	}


	////////////////////////////////////////
	// LIST MENU
	////////////////////////////////////////
	// items
	Objsc::MenuItem* ListMenu::addItem(string name)
	{
		children.push_back(new Objsc::MenuItem());
		Objsc::MenuItem* menuItem = (Objsc::MenuItem*)children.back();
		menuItem->setParent(this);
		Objsc::StylePalette::Current()->applyStyle(stylename, menuItem);
		menuItem->setText(name);
		menuItem->setWidth(childbounds.getWidth());
		return menuItem;
	}
	
	const string& ListMenu::getSelectedName() const
	{
		if(selected > -1 && selected < children.size())	{
			return children[selected]->getText();
		} else {
			return "";
		}
	}
	
	// actions
	void ListMenu::setAction(Objsc::CommandPtr actionPtr)
	{
		action = actionPtr;
	}

	void ListMenu::activate()
	{
		if(action)	{
			Objsc::CommandDispatcher::Current()->doCommand(action);
		}
	}
	
	// display
	void ListMenu::setStyleName(string sname)
	{
		stylename=sname;
	}

	void ListMenu::setToTop()
	{
		topitem=0;
		refreshDisplay();
	}

	void ListMenu::scrollUp()
	{
		topitem = MAX(0, --topitem);
		refreshDisplay();
	}

	void ListMenu::scrollDown()
	{
		topitem = MIN(children.size(), ++topitem);
		refreshDisplay();
	}

	void ListMenu::refreshDisplay()
	{
		displayitems=0;
		selected = -1;
		int count=0;
		float listheight=0.0f;
		float displayheight=0.0f;
		for (Objsc::GuiBaseVector::iterator child = children.begin(); child != children.end(); ++child)	{
			if(count < topitem) {
				(*child)->hide();
			} else {
				listheight += (*child)->getHeight();
				if(listheight <= bounds.getHeight())	{
					displayheight=listheight;
					(*child)->show();
					(*child)->setLocation(bounds.getX(),bounds.getY()+listheight-(*child)->getHeight());
					displayitems++;
				} else {
					(*child)->hide();
				}
			}
			count++;
		}
	}

	void ListMenu::shrinkWrap(float minHeight)
	{
		displayitems=0;
		int count=0;
		float listheight=0.0f;
		float displayheight=0.0f;
		for (Objsc::GuiBaseVector::iterator child = children.begin(); child != children.end(); ++child)	{
			if(count < topitem) {
				(*child)->hide();
			} else {
				listheight += (*child)->getHeight();
				if(listheight <= bounds.getHeight())	{
					displayheight=listheight;
					(*child)->show();
					(*child)->setLocation(bounds.getX(),bounds.getY()+listheight-(*child)->getHeight());
					displayitems++;
				} else {
					(*child)->hide();
				}
			}
			count++;
		}
		setDimensions(bounds.getWidth(), MAX(displayheight,minHeight));
	}

	void ListMenu::draw()
	{
		if(visible) {
			ofSetColor(bgColour.r, bgColour.g, bgColour.b, bgColour.a);
			ofRect(childbounds.getX(), childbounds.getY(), childbounds.getWidth(), childbounds.getHeight());
			for (Objsc::GuiBaseVector::iterator child = children.begin(); child != children.end(); ++child)	{
				(*child)->draw();
			}
			ofSetColor(bgColour.r, bgColour.g, bgColour.b, bgColour.a);
			ofRect(scrollrect.getX(), scrollrect.getY(), scrollrect.getWidth(), scrollrect.getHeight());
			ofSetColor(hiColour.r, hiColour.g, hiColour.b, hiColour.a);
			ofRect(sliderect.getX(), sliderect.getY(), sliderect.getWidth(), sliderect.getHeight());
		}
	}


	void ListMenu::scrollUpdated()
	{
		float perc = (sliderect.getCentreY()-scrollrect.getY())/(scrollrect.getHeight()-Objsc::SCROLLHANDLEHEIGHT);
		unsigned int index = (int)(children.size() * perc);
		if(index != topitem)	{
			topitem = MIN(MAX(0, index), children.size()-1);
			refreshDisplay();
		}
	}

	// events
	void ListMenu::keyPressed(int key)
	{
		
	}

	void ListMenu::keyReleased(int key)
	{
		
	}

	void ListMenu::mouseMoved(int x, int y )
	{
		if(childbounds.containsPoint(x,y))	{
			for (Objsc::GuiBaseVector::iterator child = children.begin(); child != children.end(); ++child)	{
				(*child)->mouseMoved(x,y);
			}
		}
	}

	void ListMenu::mouseDragged(int x, int y, int button)
	{
		if(doscroll && scrollrect.containsPoint(x,y))	{
			sliderect.setCentreY(MAX(MIN(y,scrollrect.getBottom()-(Objsc::SCROLLHANDLEHEIGHT/2)), scrollrect.getY()+(Objsc::SCROLLHANDLEHEIGHT/2)));
			scrollUpdated();
		}
	}

	void ListMenu::mousePressed(int x, int y, int button)
	{
		consumed = false;
		selected = -1;
		int count = 0;
		if(childbounds.containsPoint(x,y))	{
			//printf("ListMenu::mousePressed\n");
			for (Objsc::GuiBaseVector::iterator child = children.begin(); child != children.end(); ++child)	{
				(*child)->mousePressed(x,y,button);
				if((*child)->eventConsumed())	{
					consumed = true;
					selected = count;
					//printf("ListMenu::mousePressed selected: %d\n", selected);
					activate();
					return;
				}
				++count;
			}
		} else if(scrollrect.containsPoint(x,y))	{
			doscroll=true;
			consumed = true;
			sliderect.setCentreY(MAX(MIN(y,scrollrect.getBottom()-(Objsc::SCROLLHANDLEHEIGHT/2)), scrollrect.getY()+(Objsc::SCROLLHANDLEHEIGHT/2)));
			scrollUpdated();
		}
	}

	void ListMenu::mouseReleased(int x, int y, int button)
	{
		if(doscroll)	{
			doscroll=false;
		}
	}

	void ListMenu::windowResized(int w, int h)
	{
		
	}

}


