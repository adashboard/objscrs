#ifndef OBJSCR_MEDIAVIEWS_H
#define OBJSCR_MEDIAVIEWS_H


// objscrs
#include "objscrConstants.hpp"
#include "objscrGUI.hpp"
#include "objscrGeom.hpp"
#include "objscrCommands.hpp"
#include "objscrSound.hpp"

// openframeworks
#include "ofMain.h"

// STL
#include <string>
#include <vector>
#include <map>
#include <list>
using std::string;
using std::vector;
using std::map;
using std::list;

// shared_ptr
#include <tr1/memory>


namespace Objsc
{
	
	////////////////////////////////////////
	// MEDIA VIEW
	////////////////////////////////////////
	class MediaView : public Objsc::GuiBase
	{
		protected:
			bool		loaded;
		
			
			virtual void sizeToBounds() {}
			
		public:
			MediaView() : loaded(false){}
			virtual ~MediaView() {}
				
			virtual void update() {}
			virtual void draw() {}
			virtual void show() {}
			virtual void hide() {}
			virtual void showBG() {}
			virtual void hideBG() {}
				
			virtual bool isFinished() {return false;}
			
			void setLocation(float x, float y);
			void setDimensions(float width, float height);
			void setBounds(float x, float y, float width, float height);
			virtual void setX(float x);
			virtual void setY(float y);
			void setCentreX(float x);
			void setCentreY(float y);
			void setWidth(float width);
			void setHeight(float height);
			virtual void sizeToMedia() {}
			virtual void fullscreen();
			void centre();
			virtual void setLayer(int nlayer) {}
			virtual const int getLayer() const {return 0;}
			
			virtual void setText(const string& newtext) { }
			virtual void clearText() { }
			virtual void setTextSize(int size) {}
			virtual void setTextColour(int r, int g, int b) {}
			virtual void setBackColour(int r, int g, int b) {}
			
			virtual void load(const string& filename, bool fitmedia=true) {}	
			virtual void setVolume(float value) {}
			virtual const float getVolume() { return 0.0f;}
			virtual void setSpeed(float value) {}
			virtual const float getSpeed() { return 1.0f;}
			virtual void setPan(float value) {}
			virtual void setLoops(int value) {}
			virtual const int getLoops() const {return 0;}
			virtual void start() {}
			virtual void stop() {}
			virtual const float getDuration() { return 0.0f; }
	};
	
	typedef std::tr1::shared_ptr<Objsc::MediaView> MediaViewPtr;
	typedef map < int, Objsc::MediaViewPtr > MediaViewLayerMap;
	typedef MediaViewLayerMap::value_type MediaViewLayerEntry;
	typedef map < string, Objsc::MediaViewPtr > MediaViewLabelMap;
	typedef MediaViewLabelMap::value_type MediaViewLabelEntry;
	
	
	////////////////////////////////////////
	// IMAGE VIEW
	////////////////////////////////////////
	class ImageView : public Objsc::MediaView
	{
		protected:
			ofImage			media;
			int				layer;
			
			//void sizeToBounds();
				
		public:
			ImageView();
			~ImageView();
				
			void draw();
			void load(const string& filename, bool fitmedia=true);
			void sizeToMedia();
			void show() {visible = true;}
			void hide() {visible = false;}
		
			static Objsc::MediaViewPtr create()
			{
				return Objsc::MediaViewPtr(new ImageView());
			}
			
			static Objsc::MediaViewPtr create(const string& filename, bool fitmedia=true)
			{
				Objsc::MediaViewPtr ptr(new ImageView());
				ptr->load(filename,fitmedia);
				return ptr;
			}
			
			void setLayer(int nlayer) {layer=nlayer;}
			const int getLayer() const {return layer;}
	};
	
	
	////////////////////////////////////////
	// TEXT VIEW
	////////////////////////////////////////
	class TextView : public Objsc::MediaView
	{
		protected:
			Objsc::TextField	widget;
			int				layer;
			ofTrueTypeFont 	font;
			bool			visible;
			
			//void sizeToBounds();
				
		public:
			TextView();
			~TextView();
				
			void draw();
			void sizeToMedia();
			void show() {visible = true;}
			void hide() {visible = false;}
			void showBG();
			void hideBG();
				
			void setText(const string& newtext);
			void loadText(const string& filename);
			void clearText() { widget.clearText(); }
			void setTextSize(int size);
			void setTextColour(int r, int g, int b) { widget.setTextColour(r,g,b); }
			void setBackColour(int r, int g, int b) { widget.setBackColour(r,g,b); }
		
			static Objsc::MediaViewPtr create()
			{
				return Objsc::MediaViewPtr(new TextView());
			}
			
			void setLayer(int nlayer) {layer=nlayer;}
			const int getLayer() const {return layer;}
			
			void setX(float x);
			void setY(float y);
	};
	
	
	////////////////////////////////////////
	// VIDEO VIEW
	////////////////////////////////////////
	class VideoView : public Objsc::MediaView
	{
		protected:
			ofVideoPlayer		media;
			bool			playing;
			int				loops;
			int				layer;
				
		public:
			VideoView();
			~VideoView();
				
			void update();
			void draw();
		
			//bool isFinished() {if(loaded){return media.getPosition()==1.0f;}}
			bool isFinished();
		
			const float getDuration() {return loaded?media.getDuration():0.0f; }
		
			void show() 
			{
				visible = true;
			}
			void hide() 
			{
				visible = false;
				stop();
			}
			void sizeToMedia();
			
			void load(const string& filename, bool fitmedia=true);
			void setVolume(float value);
			void setSpeed(float value);
			const float getSpeed();
			void setLoops(int value) { loops=value;}
			const int getLoops() const {return loops;}
			void start();
			void stop();
			
			void setLayer(int nlayer) {layer=nlayer;}
			const int getLayer() const {return layer;}
			
			static Objsc::MediaViewPtr create()
			{
				return Objsc::MediaViewPtr(new VideoView());
			}
			
			static Objsc::MediaViewPtr create(const string& filename, bool fitmedia=true)
			{
				Objsc::MediaViewPtr ptr(new VideoView());
				ptr->load(filename,fitmedia);
				return ptr;
			}
	};
	
	typedef std::tr1::shared_ptr<Objsc::VideoView> VideoViewPtr;
	
}

#endif  //OBJSCR_MEDIAVIEWS_H

