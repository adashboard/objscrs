#include "objscrLangObjects.hpp"


#include <stdlib.h>

// for printscreen
#include "objscrGUIRoot.hpp"

// STL
#include <algorithm>
#include <functional>


namespace Objsc
{
	
	////////////////////////////////////////
	// DECLARATIONS
	////////////////////////////////////////
	const string Objsc::LangObject::langname = "_undefined_";
	const string Objsc::BangObject::langname = "bang";
	const string Objsc::LoadBangObject::langname = "loadbang";
	const string Objsc::PatchOnObject::langname ="patchon";
	const string Objsc::PatchOffObject::langname ="patchoff";
	const string Objsc::ToggleObject::langname = "toggle";
	const string Objsc::MessageObject::langname = "message";
	const string Objsc::PrintObject::langname = "print";
	const string Objsc::PrintScreenObject::langname = "printscr";
	const string Objsc::NumberObject::langname = "number";
	const string Objsc::SelectObject::langname = "select";
	const string Objsc::TriggerObject::langname = "trigger";
	const string Objsc::UnpackObject::langname = "unpack";
	const string Objsc::PackObject::langname = "pack";
	const string Objsc::GateObject::langname = "gate";
	
	
	/////////////////////////////////////
	// FUNCTIONS
	/////////////////////////////////////
	void initLangObjects()
	{
		Objsc::LangObjectFactory* factory = Objsc::LangObjectFactory::Current();
		factory->addCreator(Objsc::BangObject::langname, new Objsc::BangObjectCreator());
		factory->addCreator(Objsc::LoadBangObject::langname, new Objsc::LoadBangObjectCreator());
		factory->addCreator(Objsc::PatchOnObject::langname, new Objsc::PatchOnObjectCreator());
		factory->addCreator(Objsc::PatchOffObject::langname, new Objsc::PatchOffObjectCreator());
		factory->addCreator(Objsc::ToggleObject::langname, new Objsc::ToggleObjectCreator());
		factory->addCreator(Objsc::MessageObject::langname, new Objsc::MessageObjectCreator());
		factory->addCreator(Objsc::PrintObject::langname, new Objsc::PrintObjectCreator());
		factory->addCreator(Objsc::PrintScreenObject::langname, new Objsc::PrintScreenObjectCreator());
		factory->addCreator(Objsc::NumberObject::langname, new Objsc::NumberObjectCreator());
		factory->addCreator("float", new Objsc::NumberObjectCreator("float"));
		factory->addCreator(Objsc::SelectObject::langname, new Objsc::SelectObjectCreator());
		factory->addCreator("sel", new Objsc::SelectObjectCreator("sel"));
		factory->addCreator(Objsc::TriggerObject::langname, new Objsc::TriggerObjectCreator());
		factory->addCreator("t", new Objsc::TriggerObjectCreator("t"));
		factory->addCreator(Objsc::UnpackObject::langname, new Objsc::UnpackObjectCreator());
		factory->addCreator(Objsc::PackObject::langname, new Objsc::PackObjectCreator());
		factory->addCreator(Objsc::GateObject::langname, new Objsc::GateObjectCreator());
	}
	
	string makeReceiverName(const string& langname)
	{
		char cstr[256];
		sprintf(cstr, "_%s_", langname.c_str());
		return string(cstr);
	}
	

	struct objectIsLinked : std::binary_function< Objsc::LangAddress, Objsc::LangObjectPtr, bool >	
	{
		bool operator() (const Objsc::LangAddress& addr, const Objsc::LangObjectPtr& obj) const	{
			return (addr.first == obj);
		}
	};
	
	
	float strToFloat(const string& str)
	{
		return atof(str.c_str());
	}
	
	
	/////////////////////////////////////
	// LANG CORD
	/////////////////////////////////////
	void LangCord::send(LangAtom* atom)
	{
		for(Objsc::LangAddressVector::iterator obj = objects.begin(); obj != objects.end(); ++obj)	{
			obj->first.get()->receive(obj->second, atom);
		}
	}
	
	void LangCord::unlinkAll(Objsc::LangObjectPtr objectPtr)
	{
		objects.erase(find_if(objects.begin(), objects.end(), bind2nd(Objsc::objectIsLinked(), objectPtr)));
	}
	
	bool LangCord::connectsTo(Objsc::LangObjectPtr objectPtr)
	{
		for(Objsc::LangAddressVector::iterator obj = objects.begin(); obj != objects.end(); ++obj)	{
			if(obj->first == objectPtr)	{
				return true;
			} 
		}
		return false;
	}
	
	/////////////////////////////////////
	// LANG RECEIVER
	/////////////////////////////////////
	void LangReceiver::add(LangObjectPtr objectPtr)
	{
		//printf("LangReceiver::add <%s>\n", objectPtr->getLangName().c_str());
		objects.push_back(objectPtr);
	}
	
	void LangReceiver::remove(LangObjectPtr objectPtr)
	{
		//printf("LangReceiver::remove <%s>\n", objectPtr->getLangName().c_str());
		objects.erase(find(objects.begin(), objects.end(), objectPtr));
	}
			
	void LangReceiver::receive(LangAtom* atom)
	{
		//printf("LangReceiver::receive A %d\n", atom->getType());
		for(Objsc::LangObjectList::iterator obj = objects.begin(); obj != objects.end(); ++obj)	{
			//printf("LangReceiver::receive B <%s>\n", (*obj)->getLangName().c_str());
			(*obj)->receive(0, atom);
		}
	}
	
	/////////////////////////////////////
	// LANG OBJECT
	/////////////////////////////////////
	LangObject::LangObject() : 
		num_inlets(), 
		num_outlets(), 
		outlets(), 
		x(0.0f), 
		y(0.0f), 
		show_inlets(true), 
		show_outlets(true), 
		displaystr(), 
		min_args(0), 
		max_args(0) 
	{}
			
	void LangObject::setOutlets(int size) 
	{ 
		num_outlets = size;
		outlets.clear();
		for(int i = 0; i < num_outlets; i++)	{
			outlets.insert(LangCordEntry(i, new LangCord()));
		}
	}
	
	const int LangObject::definedOutlets() const
	{
		if(!show_outlets)	{
			return 0;
		} else {
			int sum = 0;
			for(LangCordMap::const_iterator out = outlets.begin(); out != outlets.end(); ++out) {
				sum += out->second->numAddresses();
			}
			return sum;
		}
	}
	
	
	void LangObject::link(int outlet, LangObjectPtr objectPtr, int inlet)
	{
		if(outlet > -1 && outlet < num_outlets) {
			outlets[outlet]->link(objectPtr, inlet);
		}
	}
	
	void LangObject::unlink(int outlet, LangObjectPtr objectPtr, int inlet)
	{
		if(outlets.count(outlet) > 0)	{
			outlets[outlet]->unlink(objectPtr, inlet);
		}
	}
			
	void LangObject::unlinkAll(Objsc::LangObjectPtr objectPtr)
	{
		for(LangCordMap::iterator out = outlets.begin(); out != outlets.end(); ++out) {
			if(out->second->connectsTo(objectPtr))	{
				out->second->unlinkAll(objectPtr);
			} 
		}
	}
	
	bool LangObject::connectsTo(Objsc::LangObjectPtr objectPtr)
	{
		for(LangCordMap::iterator out = outlets.begin(); out != outlets.end(); ++out) {
			if(out->second->connectsTo(objectPtr))	{
				return true;
			}
		}
		return false;
	}
	
	void LangObject::clearOutlet(int outlet)
	{
		if(outlets.count(outlet) > 0)	{
			outlets[outlet]->clear();
		} 
	}
	
	void LangObject::clearOutlets()
	{
		for(LangCordMap::iterator out = outlets.begin(); out != outlets.end(); ++out) {
			out->second->clear();
		}
		outlets.clear();
	}
	
	void LangObject::setLocation(float nx, float ny)
	{
		x = nx;
		y = ny;
	}
	
	const string& LangObject::getDisplayString()
	{
		if(displaystr.empty())	{
			return getLangName();
		}
		return displaystr;
	}
	
	void LangObject::receive(int inlet, Objsc::LangAtom* atom)
	{
		if(inlet > -1 && inlet < num_inlets) 	{
			switch(atom->getType())	{
				case LANG_NULL:
					break;
				case LANG_BANG:
					receiveBang(inlet);
					break;
				case LANG_INT:
					receiveInt(inlet, parseInt(atom));
					break;
				case LANG_FLOAT:
					receiveFloat(inlet, parseFloat(atom));
					break;
				case LANG_STRING:
					receiveString(inlet, parseString(atom));
					break;
				case LANG_LIST:
					receiveList(inlet, dynamic_cast<Objsc::LangList*>(atom));
					break;
				case LANG_VAR:
					break;
			}
		}
	}
	
	void LangObject::send(int index, LangAtom* atom)
	{
		if(outlets.count(index) > 0)	{
			outlets[index]->send(atom);
		} 
	}
	
	void LangObject::parseValue(LangAtom* atom)
	{
		switch(atom->getType())	{
			case LANG_NULL:
				break;
			case LANG_BANG:
				break;
			case LANG_INT:
				//std::cout << parseInt(atom) << std::endl;
				send(2, new Objsc::LangInt(parseInt(atom)));
				break;
			case LANG_FLOAT:
				//std::cout << parseFloat(atom) << std::endl; 
				send(2, new Objsc::LangFloat(parseFloat(atom)));
				break;
			case LANG_STRING:
				//std::cout << parseString(atom) << std::endl;
				send(2, new Objsc::LangString(parseString(atom)));
				break;
			case LANG_LIST:
				break;
			case LANG_VAR:
				break;
		}
	}
	
	void LangObject::receiveBang(int inlet) 
	{ 
		//std::cout << "bang!" << std::endl; 
		send(0, new Objsc::LangBang());
	}
	void LangObject::receiveInt(int inlet, int value) 
	{
		//std::cout << "int: " << value << std::endl;
		send(1, new Objsc::LangInt(value));
	}
	void LangObject::receiveFloat(int inlet, float value) 
	{
		//std::cout << "float: " << value << std::endl; 
		send(1, new Objsc::LangFloat(value));
	}
	void LangObject::receiveString(int inlet, string value) 
	{
		//std::cout << "string: " << value << std::endl;
		send(1, new Objsc::LangString(value));
	}
	void LangObject::receiveList(int inlet, LangList* value) 
	{	
		//std::cout << "list: " << std::endl; 
		for(LangAtomIter atom = value->begin(); atom != value->end(); ++atom) {
			parseValue((*atom)); 
		}
	}
	
	
	/////////////////////////////////////
	// TOGGLE OBJECT
	/////////////////////////////////////
	void ToggleObject::receive(int inlet, Objsc::LangAtom* atom)
	{
		switch(atom->getType())	{
			case LANG_NULL:
				break;
			case LANG_BANG:
				activate();
				break;
			case LANG_INT:
				if(parseInt(atom, 0)==0)	{
					turnOff();
				} else {
					turnOn();
				}
				break;
			case LANG_FLOAT:
				if(parseInt(atom, 0)==0)	{
					turnOff();
				} else {
					turnOn();
				}
				break;
			case LANG_STRING:
				break;
			case LANG_VAR:
				break;
			case LANG_LIST:
				receiveList(0, dynamic_cast<Objsc::LangList*>(atom));
				break;
		}
	}
	
	void ToggleObject::receiveList(int inlet, LangList* value)
	{
		if(parseInt(value->idx(0), 0) == 0)	{
			turnOff();
		} else {
			turnOn();
		}
	}
	
	void ToggleObject::turnOn()
	{
		on = true;
		if(observer)	{
			observer->objActivated();
		}
		send(0, new Objsc::LangInt(1));
	}
	
	void ToggleObject::turnOff()
	{
		on = false;
		if(observer)
			observer->objActivated();
		send(0, new Objsc::LangInt(0));
	}
	
	void ToggleObject::setObserver(Objsc::LangObserverPtr obsPtr) 
	{
		observer=obsPtr;
	}
	void ToggleObject::removeObserver() 
	{
		observer.reset();
	}
	
	/////////////////////////////////////
	// MESSAGE OBJECT
	/////////////////////////////////////
	void MessageObject::setDisplayString(const string& str)
	{
		//printf("MessageObject::setDisplayString\n");
		Objsc::tokenizeString(str, message);
		displaystr = listToString(&message);
	}
	
	void MessageObject::receive(int inlet, Objsc::LangAtom* atom)
	{
		switch(atom->getType())	{
			case LANG_NULL:
				break;
			case LANG_BANG:
				if(inlet == 0)
					activate();
				break;
			case LANG_INT:
				message.setVar(1, Objsc::LangAtomPtr(atom));
				if(inlet == 0)
					activate();
				break;
			case LANG_FLOAT:
				message.setVar(1, Objsc::LangAtomPtr(atom));
				if(inlet == 0)
					activate();
				break;
			case LANG_STRING:
				message.setVar(1, Objsc::LangAtomPtr(atom));
				if(inlet == 0)
					activate();
				break;
			case LANG_VAR:
				message.setVar(1, Objsc::LangAtomPtr(atom));
				if(inlet == 0)
					activate();
				break;
			case LANG_LIST:
				//printf("MessageObject::receive LANG_LIST A\n");
				int limit = dynamic_cast<Objsc::LangList*>(atom)->size();
				for(int i = 0; i < limit; ++i)	{
					message.setVar(i+1, dynamic_cast<Objsc::LangList*>(atom)->get(i));
				}
				//printf("MessageObject::receive LANG_LIST DONE\n");
				if(inlet == 0)
					activate();
				break;
		}
	}
	
	void MessageObject::activate() 
	{
		//printf("activate [[%s]]\n", listToString(&message).c_str());
		if(message.size() > 1)	{
			send(0, &message);
		} else if(message.size() == 1)	{
			switch(message.getItemType(0))	{
				case LANG_NULL:
					break;
				case LANG_BANG:
					send(0, new Objsc::LangBang());
					break;
				case LANG_INT:
					send(0, new Objsc::LangInt(getItemAsInt(&message, 0)));
					break;
				case LANG_FLOAT:
					send(0, new Objsc::LangFloat(getItemAsFloat(&message, 0)));
					break;
				case LANG_STRING:
					//send(0, new Objsc::LangString(getFirstAsString(&message))); // NOTE: breaks a lot of existing code
					send(0, &message);
					break;
				case LANG_VAR:
					send(0, &message); // NOTE: change to send out value
				case LANG_LIST:
					send(0, &message);
					break;
			}
		}
	}
	
	
	/////////////////////////////////////
	// NUMBER OBJECT
	/////////////////////////////////////
	void NumberObject::setDisplayString(const string& str)
	{
		setValue(strToFloat(str));
	}
	
	void NumberObject::receive(int inlet, Objsc::LangAtom* atom)
	{
		switch(atom->getType())	{
			case LANG_NULL:
				break;
			case LANG_BANG:
				activate();
				break;
			case LANG_INT:
				setValue(parseFloat(atom, 0.0f));
				if(inlet == 0)
					activate();
				break;
			case LANG_FLOAT:
				setValue(parseFloat(atom));
				if(inlet == 0)
					activate();
				break;
			case LANG_STRING:
				break;
			case LANG_VAR:
				break;
			case LANG_LIST:
				receiveList(inlet, dynamic_cast<Objsc::LangList*>(atom));
				break;
		}
	}
	
	void NumberObject::receiveList(int inlet, LangList* value)
	{
		setValue(parseFloat(value->idx(0), 0));
		if(inlet == 0)
			activate();
	}
	
	void NumberObject::activate() 
	{
		if(observer)
			observer->objActivated();
		send(0, new Objsc::LangFloat(value));
	}
	
	void NumberObject::setValue(float nvalue) 
	{ 
		value=nvalue;
		char cstr[32];
		sprintf(cstr,"%.3f", value);
		if(label.empty())	{
			displaystr.assign(cstr);
		} else {
			displaystr = label + " " + string(cstr);
		}
		if(observer)
			observer->objActivated();
	}
	
	
	/////////////////////////////////////
	// SELECT OBJECT
	/////////////////////////////////////
	bool SelectObject::parseArgs(Objsc::LangList* args)
	{
		if(args->size() < min_args)	{
			return false;
		} else {
			//keys.copy(args, 1);
			keys.copy(args);
			if(label.empty())	{
				displaystr = Objsc::SelectObject::langname + " " + listToString(&keys);
			} else {
				displaystr.assign(label.c_str());
				displaystr.append(" ");
				displaystr.append(listToString(&keys));
			}
			setOutlets(keys.size()+1);
			return true;
		}
	}
	
	
	void SelectObject::receive(int inlet, Objsc::LangAtom* atom)
	{
		switch(atom->getType())	{
			case LANG_NULL:
				break;
			case LANG_BANG:
				break;
			case LANG_INT:
				select(atom);
				break;
			case LANG_FLOAT:
				select(atom);
				break;
			case LANG_STRING:
				select(atom);
				break;
			case LANG_VAR:
				break;
			case LANG_LIST:
				//printf("SelectObject::receive LIST\n");
				select(dynamic_cast<Objsc::LangList*>(atom)->idx(0));
				break;
		}
	}
	
	void SelectObject::select(Objsc::LangAtom* atom)
	{
		int index = keys.find(atom);
		if(index == -1)	{
			send(num_outlets-1, atom);
		} else {
			send(index, new Objsc::LangBang());
		}
	}
	
	/////////////////////////////////////
	// TRIGGER OBJECT
	/////////////////////////////////////
	bool TriggerObject::parseArgs(Objsc::LangList* args)
	{
		//printf("TriggerObject::parseArgs START %d\n", args->size());
		if(args->size() < min_args)	{
			return false;
		} else {
			//keys.copy(args, 1);
			keys.copy(args);
			if(label.empty())	{
				//printf("TriggerObject::parseArgs <%s> A\n", displaystr.c_str());
				displaystr.assign(getLangName().c_str());
				displaystr.append(" ");
				displaystr.append(listToString(&keys));
			} else {
				displaystr.assign(label.c_str());
				displaystr.append(" ");
				displaystr.append(listToString(&keys));
			}
			//printf("TriggerObject::parseArgs <%s>\n", displaystr.c_str());
			setOutlets(keys.size());
			//printf("TriggerObject::parseArgs DONE %d\n", keys.size());
			return true;
		}
	}
	
	void TriggerObject::receive(int inlet, Objsc::LangAtom* atom)
	{
		switch(atom->getType())	{
			case LANG_NULL:
				break;
			case LANG_BANG:
				trigger(atom);
				break;
			case LANG_INT:
				trigger(atom);
				break;
			case LANG_FLOAT:
				trigger(atom);
				break;
			case LANG_STRING:
				trigger(atom);
				break;
			case LANG_VAR:
				break;
			case LANG_LIST:
				trigger(dynamic_cast<Objsc::LangList*>(atom)->idx(0));
				break;
		}
	}
	
	void TriggerObject::trigger(Objsc::LangAtom* atom)
	{
		for(int i = num_outlets-1; i > -1; --i)	{
			if(atom->getType() == keys.idx(i)->getType())	{
				send(i, atom);
			} else {
				send(i, keys.idx(i));
			}
		}
	}
	
	/////////////////////////////////////
	// UNPACK OBJECT
	/////////////////////////////////////
	bool UnpackObject::parseArgs(Objsc::LangList* args)
	{
		//printf("UnpackObject::parseArgs START %d\n", args->size());
		if(args->size() < min_args)	{
			return false;
		} else {
			//keys.copy(args, 1);
			keys.copy(args);
			if(label.empty())	{
				//printf("UnpackObject::parseArgs <%s> A\n", displaystr.c_str());
				displaystr.assign(getLangName().c_str());
				displaystr.append(" ");
				displaystr.append(listToString(&keys));
			} else {
				displaystr.assign(label.c_str());
				displaystr.append(" ");
				displaystr.append(listToString(&keys));
			}
			//printf("UnpackObject::parseArgs <%s>\n", displaystr.c_str());
			setOutlets(keys.size());
			//printf("UnpackObject::parseArgs DONE %d\n", keys.size());
			return true;
		}
	}
	
	void UnpackObject::receive(int inlet, Objsc::LangAtom* atom)
	{
		switch(atom->getType())	{
			case LANG_NULL:
				break;
			case LANG_BANG:
				break;
			case LANG_INT:
				break;
			case LANG_FLOAT:
				break;
			case LANG_STRING:
				break;
			case LANG_VAR:
				break;
			case LANG_LIST:
				receiveList(inlet, dynamic_cast<Objsc::LangList*>(atom));
				break;
		}
	}
	
	void UnpackObject::receiveList(int inlet, Objsc::LangList* atom) 
	{	
		if(atom->size() >= num_outlets)	{
			for(int i = num_outlets-1; i > -1; --i)	{
				//printf("types: %d %d\n", atom->idx(i)->getType(), keys.idx(i)->getType());
				if(atom->idx(i)->getType() == keys.idx(i)->getType())	{
					send(i, atom->idx(i));
				} else {
					send(i, keys.idx(i));
				}
			}
		}
	}
	
	
	/////////////////////////////////////
	// PACK OBJECT
	/////////////////////////////////////
	bool PackObject::parseArgs(Objsc::LangList* args)
	{
		//printf("PackObject::parseArgs START %d\n", args->size());
		if(args->size() < min_args)	{
			return false;
		} else {
			//keys.copy(args, 1);
			keys.copy(args);
			input.copy(args);
			if(label.empty())	{
				//printf("PackObject::parseArgs <%s> A\n", displaystr.c_str());
				displaystr.assign(getLangName().c_str());
				displaystr.append(" ");
				displaystr.append(listToString(&keys));
			} else {
				displaystr.assign(label.c_str());
				displaystr.append(" ");
				displaystr.append(listToString(&keys));
			}
			//printf("PackObject::parseArgs <%s>\n", displaystr.c_str());
			setInlets(keys.size());
			//printf("PackObject::parseArgs DONE %d\n", keys.size());
			return true;
		}
	}
	
	void PackObject::receive(int inlet, Objsc::LangAtom* atom)
	{
		switch(atom->getType())	{
			case LANG_NULL:
				break;
			case LANG_BANG:
				input.set(inlet, atom);
				if(inlet == 0)	{
					activate();
				}
				break;
			case LANG_INT:
				input.set(inlet, atom);
				if(inlet == 0)	{
					activate();
				}
				break;
			case LANG_FLOAT:
				input.set(inlet, atom);
				if(inlet == 0)	{
					activate();
				}
				break;
			case LANG_STRING:
				input.set(inlet, atom);
				if(inlet == 0)	{
					activate();
				}
				break;
			case LANG_VAR:
				break;
			case LANG_LIST:
				receiveList(inlet, dynamic_cast<Objsc::LangList*>(atom));
				if(inlet == 0)	{
					activate();
				}
				break;
		}
	}
	
	
	void PackObject::activate()
	{	
		for(int i = 0; i < num_inlets; ++i)	{
			//printf("types: %d %d\n", atom->idx(i)->getType(), keys.idx(i)->getType());
			if(input.idx(i)->getType() != keys.idx(i)->getType())	{
				input.set(i, keys.idx(i));
			} 
		}
		send(0, &input);
		input.copy(&keys);
	}
	
	void PackObject::receiveList(int inlet, Objsc::LangList* atom) 
	{	
		if(inlet > -1 && inlet < num_inlets)	{
			input.set(inlet, atom->idx(0));
		}
	}
	
	
	/////////////////////////////////////
	// PRINT OBJECT
	/////////////////////////////////////
	bool PrintObject::parseArgs(Objsc::LangList* args)
	{
		if(args->size() < min_args)	{
			return false;
		} else {
			//setLabel(getItemAsString(args, 1));
			setLabel(getItemAsString(args, 0));
			return true;
		}
	}
	
	void PrintObject::setLabel(string nlabel) 
	{ 
		label.assign(nlabel);
		displaystr = Objsc::PrintObject::langname + " " + label;
	}
			
	const string& PrintObject::getDisplayString()
	{
		if(displaystr.empty())	{
			return Objsc::PrintObject::langname;
		}
		return displaystr;
	}
	
	void PrintObject::receive(int inlet, Objsc::LangAtom* atom) 
	{
		switch(atom->getType())	{
			case LANG_NULL:
				buffer.assign(Objsc::LANG_NULLSTRING);
				break;
			case LANG_BANG:
				buffer.assign(Objsc::LANG_BANGSTRING);
				activate();
				break;
			case LANG_INT:
				buffer.assign(intToString(parseInt(atom)));
				activate();
				break;
			case LANG_FLOAT:
				buffer.assign(floatToString(parseFloat(atom)));
				activate();
				break;
			case LANG_STRING:
				buffer.assign(parseString(atom));
				activate();
				break;
			case LANG_VAR:
				buffer.assign(varToString(dynamic_cast<Objsc::LangVar*>(atom)));
				activate();
				break;
			case LANG_LIST:
				receiveList(dynamic_cast<Objsc::LangList*>(atom));
				activate();
				break;
		}
	}
	
	void PrintObject::activate() 
	{
		if(label.empty())	{
			std::cout << buffer << std::endl; 
		} else {
			std::cout << label << " " << buffer << std::endl;
		}
		buffer.clear();
	}
	
	/////////////////////////////////////
	// PRINT SCREEN OBJECT
	/////////////////////////////////////
	void PrintScreenObject::setLabel(string nlabel) 
	{ 
		label.assign(nlabel);
		displaystr = Objsc::PrintScreenObject::langname + " " + label;
	}
			
	const string& PrintScreenObject::getDisplayString()
	{
		if(displaystr.empty())	{
			return Objsc::PrintScreenObject::langname;
		}
		return displaystr;
	}
	
	void PrintScreenObject::activate() 
	{
		if(label.empty())	{
			Objsc::GUIRoot::Current()->printToScreen(buffer);
		} else {
			Objsc::GUIRoot::Current()->printToScreen(label + " " + buffer);
		}
		buffer.clear();
	}
	
	
	/////////////////////////////////////
	// GATE OBJECT
	/////////////////////////////////////
	void GateObject::receive(int inlet, Objsc::LangAtom* atom) 
	{
		if(inlet == 0)	{
			if(active)	{
				send(0, atom);
			}
		} else if(inlet == 1)	{
			switch(atom->getType())	{
				case LANG_BANG:
					break;
				case LANG_INT:
					if(parseBool(atom))	{
						open();
					} else {
						close();
					}
					break;
				case LANG_FLOAT:
					if(parseBool(atom))	{
						open();
					} else {
						close();
					}
					break;
				case LANG_LIST:
					receiveList(inlet, dynamic_cast<Objsc::LangList*>(atom));
					break;
			}
		}
	}
	
	void GateObject::receiveList(int inlet, LangList* value)
	{
		if(inlet == 1) {
			string cmd = parseString(value->idx(0), "");
			if(cmd.empty())	{
				parseInt(value->idx(0), 0)>0?open():close();
			} else {
				if(cmd == "open")	{
					open();
				} else if(cmd == "close")	{
					close();
				}
			}
		} 
	}
}



