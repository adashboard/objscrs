#ifndef  OBJSCR_VIDEO_H
#define OBJSCR_VIDEO_H

// objscrs
#include "singleton.h"
#include "objscrFrames.hpp"
#include "objscrDataTypes.hpp"

// STL
#include <string>
#include <list>
#include <map>
#include <exception>
using std::string;
using std::list;
using std::map;
using std::exception;

// shared_ptr
#include <tr1/memory>

namespace Objsc
{
	
	////////////////////////////////////////
	// VIDEO DEVICE
	////////////////////////////////////////
	class VideoSource : public FrameSource
	{
		public:
			VideoSource(){}
			virtual ~VideoSource() {}
				
			virtual void buildProfile() {}
			virtual const string getIdentifier() const =0;
			virtual const string getVenderName() const =0;
			virtual const string getModelName() const =0;
			virtual const string getDevice() const =0;
			virtual StringVector getFormatIdentifiers() =0;
			virtual StringVector getFormatSizes(int index) =0;
			virtual void setFormat(int formatNum, int sizeNum) =0;
			
			virtual void setFrameSize(int width, int height) =0;
			virtual void start() =0;
			virtual void stop() =0;
	};
	
	typedef std::tr1::shared_ptr<Objsc::VideoSource> VideoSourcePtr;
	
	typedef list< VideoSource* > VideoSourceList;
	
	
	////////////////////////////////////////
	// VIDEO MANAGER
	////////////////////////////////////////
	class VideoManager
	{
		public:
			virtual ~VideoManager() {}
				
			virtual void scanDevices() =0;
			virtual bool devicesFound() =0;
			virtual StringVector getDeviceIdentifiers() =0;
			virtual VideoSource* getDevice(int index) =0;
	};
	
	typedef std::tr1::shared_ptr<Objsc::VideoManager> VideoManagerPtr;
	
	////////////////////////////////////////
	// VIDEO MANAGER CREATORS
	////////////////////////////////////////
	class VideoManagerCreator
	{
		public:
			virtual ~VideoManagerCreator() {}
			virtual VideoManager* create() const =0;
	};
	
	////////////////////////////////////////
	// VIDEO MANAGER FACTORY
	////////////////////////////////////////
	typedef map < string, VideoManagerCreator* > VideoManagerCreatorMap;
	typedef VideoManagerCreatorMap::value_type VideoManagerCreatorEntry;

	class VideoManagerFactory : public Singleton<VideoManagerFactory>
	{
		private:
			VideoManagerCreatorMap creators;
		
		public:
			VideoManagerFactory() : creators() {}
			~VideoManagerFactory() { creators.clear(); }
			
			VideoManagerPtr create(string drivername)
			{
				VideoManagerCreatorMap::iterator iter = creators.find(drivername);
				if(iter == creators.end())
					throw std::exception();
				return VideoManagerPtr((*iter).second->create());
			}
			
			void addCreator(string drivername, VideoManagerCreator* creatorPtr)
			{
				creators.insert(VideoManagerCreatorEntry(drivername, creatorPtr));
			}
			
	};
			
}

#endif // OBJSCR_VIDEO_H