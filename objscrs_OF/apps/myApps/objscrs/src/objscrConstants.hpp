#ifndef  OBJSCR_CONSTANTS_H
#define OBJSCR_CONSTANTS_H


#include <string>
#include <limits>

#define RINT(x) ( (x)-floor(x)>0.5? ceil(x) : floor(x) )

#define RAD2DEG 57.295779513082323

namespace Objsc
{
	
	const int 			VIDSOURCE_WIDTH = 320;
	const int 			VIDSOURCE_HEIGHT = 240;
	const int 			PAL_WIDTH = 720;
	const int 			PAL_HEIGHT = 576;
	
	const std::string 		NULL_STRING = "";
	
	
	// OSC
	const std::string 		OSC_MAINLISTENER = "_main_";
	const int 			OSC_MAINPORT = 9898;
	
	// numbers
	const double  		MAXDOUBLE = (std::numeric_limits<double>::max)();
	const double  		MINDOUBLE = (std::numeric_limits<double>::min)();
	const float  			MAXFLOAT = (std::numeric_limits<float>::max)();
	const float  			MINFLOAT = (std::numeric_limits<float>::min)();
	
	// zones
	const float 			ZONE_DFTWIDTH = 0.1f;
	const float 			ZONE_DFTHEIGHT = 0.1f;
	
	
	// GUI
	//const int COLOURBINS = 24;
	const int LARGEBTNWIDTH = 64;
	const int TOOLBUTTONWIDTH = 18;
	const int TOOLBUTTONHEIGHT = 18;
	const int TOOLBUTTONMARGIN = 10;

	const unsigned int ID_UNDEFINED = 0;
	const unsigned int INVALID_NODE_INDEX = 0;
	const int MOUSELEFT = 0;
	const int MOUSECENTRE = 1;
	const int MOUSERIGHT = 2;
	
	const int SCRWIDTH = 640;
	const int SCRHEIGHT = 480;
	const int FONTSIZE = 16;
	const int PADWIDTH = 2;
	const int PADHEIGHT = 4;
	const float HANDLESIZE = 8.0f;
	const float SCROLLHANDLEWIDTH = 20.0f;
	const float SCROLLHANDLEHEIGHT = 30.0f;
	const float TIMELINEHANDLEHEIGHT = 20.0f;
	const float TIMELINESCROLLHEIGHT = 21.0f;
	
	const float WINDOWMARGIN_WIDTH = 10.0f;
	const float WINDOWMARGIN_HEIGHT = 10.0f;
	
	// markers
	const float MARKERWIDTH = 32.0f; //40.0f;
	const float MARKERHEIGHT = 32.0f; //40.0f;
	const float MARKERICONWIDTH = 32.0f;
	const float MARKERICONHEIGHT = 32.0f;
	
	enum gui_drag
	{
		DRAG_NONE,
		DRAG_NORTH,
		DRAG_NORTHEAST,
		DRAG_EAST,
		DRAG_SOUTHEAST,
		DRAG_SOUTH,
		DRAG_SOUTHWEST,
		DRAG_WEST,
		DRAG_NORTHWEST,
		DRAG_STARTHANDLE,
		DRAG_ENDHANDLE,
		DRAG_WHOLE
	};

	enum gui_scrolls {
		SCROLL_NONE,
		SCROLL_UP,
		SCROLL_DOWN
	};
	
	enum displaystates {ENABLED, DISABLED, HIGHLIGHT, SELECTED};
	enum selectionmodes {SELECTONE, SELECTMANY, SELECTNONE};
	
}

#endif // OBJSCR_CONSTANTS_H



