#include "testApp.h"

# define OBJSCRS_SOUND_ON // comment out to disable sound


void testApp::setup()
{	 
	ofBackground(100,100,100);
	app = Objsc::App::Current();
	//guiroot = Objsc::GUIRoot::Current();
	app->init();
	guiroot = Objsc::GUIRoot::Current();
	//app->newScore();
	guiroot->run();
	// audio
	#ifdef  OBJSCRS_SOUND_ON
	//ofSoundStreamSetup(2,2,this, 44100,256, 4);
	//audioout = Objsc::SoundMixer::Current();
	//sndon = true;
	#endif 
	sndon = false;
	startTime();
	ofSetFrameRate(12);
}

void testApp::startTime()
{
	timenow = 0;
	timepassed = ofGetElapsedTimeMillis();
	timeframe = 0;
}

void testApp::update()
{	
	timenow = ofGetElapsedTimeMillis();
	timeframe=timenow-timepassed;
	app->update(timenow,timeframe);
	guiroot->update();
	if(sndon){
		//audioout->update(timeframe);
	}
	timepassed=ofGetElapsedTimeMillis();
}


void testApp::draw()
{
	guiroot->draw();
}



void testApp::keyPressed(int key)
{ 
	guiroot->keyPressed(key);
}



void testApp::keyReleased(int key)
{ 
	guiroot->keyReleased(key);
}


void testApp::mouseMoved(int x, int y )
{
	guiroot->mouseMoved(x,y);
}


void testApp::mouseDragged(int x, int y, int button)
{
	guiroot->mouseDragged(x,y,button);
}


void testApp::mousePressed(int x, int y, int button)
{
	guiroot->mousePressed(x,y,button);
}


void testApp::mouseReleased(int x, int y, int button)
{
	guiroot->mouseReleased(x,y,button);
}


void testApp::windowResized(int w, int h)
{
	guiroot->windowResized(w,h);
}

/*
void testApp::audioRequested(float * output, int bufferSize, int nChannels)
{
	if (sndon){
		//audioout->audioRequested(output, bufferSize, nChannels);
	}
}
*/