#ifndef  OBJSCR_SETTINGS_H
#define OBJSCR_SETTINGS_H

// objscrs
#include "singleton.h"

// xml
#include "tinyxml.h"

namespace Objsc
{
	class Settings : public Singleton<Settings>
	{
		private:
			bool	loaded;
			// settings
			int		cameraID;
			bool	cameraOn;
			int		scrWidth;
			int		scrHeight;
			int 		trackingMinArea;
			int 		trackingMaxArea;
			int 		trackingThreshold;
			//~ int		unbornAge;
			//~ int		unbornHits;
			//~ int		unbornLastHit;
			//~ int		becomingAge;
			//~ int		becomingHits;
			//~ int		becomingLastHit;
			//~ int		presentLastHit;
			//~ int		absentLastHitMax;
			//~ int		absentLastHitMin;
		
			// methods
			void decodeCamera(TiXmlElement *parentNode);
			void decodeDimensions(TiXmlElement *parentNode);
			void decodeBodyPrefs(TiXmlElement *parentNode);
			void decodeTracking(TiXmlElement *parentNode);
			
		public:
			Settings();
			~Settings() {}
				
			bool load();
			void print();
				
			const bool isLoaded() const { return loaded; }
			const int getCameraID() const { return cameraID; }
			const bool getCameraOn() const { return cameraOn; }
			const int getDisplayWidth() const { return scrWidth; }
			const int getDisplayHeight() const { return scrHeight; }
			const int getTrackingMinArea() const { return trackingMinArea; }
			const int getTrackingMaxArea() const { return trackingMaxArea; }
			const int getTrackingThreshold() const { return trackingThreshold; }
	};
}

#endif // OBJSCR_SETTINGS_H
