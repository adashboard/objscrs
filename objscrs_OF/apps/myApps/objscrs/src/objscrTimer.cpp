#include "objscrTimer.hpp"

// STL
#include <iostream>

namespace Objsc
{
	////////////////////////////////////////
	// FUNCTIONS
	////////////////////////////////////////
	bool actionHasExpired(Objsc::TimedActionPtr& action) { return action->isExpired(); }
	
	////////////////////////////////////////
	// TIMED ACTION
	////////////////////////////////////////
	void TimedAction::update(int timeframe)
	{
		if(active)	{
			timecount+=timeframe;
			if(timecount >= timeout)	{
				fire();
			}
		}
	}
	
	void TimedAction::expire()
	{
		timecount=0;
		repeatcount=0;
		active=false;
	}
	
	void TimedAction::fire()
	{
		timecount=0;
		if(cmd)	{
			Objsc::CommandDispatcher::Current()->doCommand(cmd);
		}
		if(repeats > 0)	{
			if(++repeatcount>=repeats)	{
				expire();
			}
		}
	}
	
	////////////////////////////////////////
	// TIMER
	////////////////////////////////////////
	void Timer::update(int timeframe)
	{
		for (TimedActionList::iterator it = actions.begin(); it != actions.end(); ++it)	{
			(*it)->update(timeframe);
		}
		actions.remove_if(&Objsc::actionHasExpired);
	}
	
}

