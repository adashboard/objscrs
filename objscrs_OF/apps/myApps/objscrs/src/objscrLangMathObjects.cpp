#include "objscrLangMathObjects.hpp"


#include <math.h>

// OF
#include "ofMain.h"


#ifndef RINT
	#define RINT(x) ( (x)-floor(x)>0.5? ceil(x) : floor(x) )
#endif

namespace Objsc
{
	
	////////////////////////////////////////
	// DECLARATIONS
	////////////////////////////////////////
	const string Objsc::EqualsObject::langname = "==";
	const string Objsc::GreaterThanObject::langname = ">";
	const string Objsc::LessThanObject::langname = "<";
	const string Objsc::AddObject::langname = "+";
	const string Objsc::MinusObject::langname = "-";
	const string Objsc::MultiplyObject::langname = "*";
	const string Objsc::DivideObject::langname = "/";
	const string Objsc::ModuloObject::langname = "%";
	const string Objsc::InvertObject::langname = "inv";
	const string Objsc::CeilingObject::langname = "ceil";
	const string Objsc::FloorObject::langname = "floor";
	const string Objsc::AverageObject::langname = "avg";
	const string Objsc::RandomObject::langname = "rnd";
	const string Objsc::MaxObject::langname = "max";
	const string Objsc::MinObject::langname = "min";
	const string Objsc::ChangeObject::langname = "change";
	const string Objsc::IntObject::langname = "int";
	
	
	/////////////////////////////////////
	// FUNCTIONS
	/////////////////////////////////////
	void initLangMathObjects()
	{
		Objsc::LangObjectFactory* factory = Objsc::LangObjectFactory::Current();
		factory->addCreator(Objsc::EqualsObject::langname, new Objsc::EqualsObjectCreator());
		factory->addCreator(Objsc::GreaterThanObject::langname, new Objsc::GreaterThanObjectCreator());
		factory->addCreator(Objsc::LessThanObject::langname, new Objsc::LessThanObjectCreator());
		factory->addCreator(Objsc::AddObject::langname, new Objsc::AddObjectCreator());
		factory->addCreator(Objsc::MinusObject::langname, new Objsc::MinusObjectCreator());
		factory->addCreator(Objsc::MultiplyObject::langname, new Objsc::MultiplyObjectCreator());
		factory->addCreator(Objsc::DivideObject::langname, new Objsc::DivideObjectCreator());
		factory->addCreator(Objsc::ModuloObject::langname, new Objsc::ModuloObjectCreator());
		factory->addCreator(Objsc::InvertObject::langname, new Objsc::InvertObjectCreator());
		factory->addCreator(Objsc::CeilingObject::langname, new Objsc::CeilingObjectCreator());
		factory->addCreator(Objsc::FloorObject::langname, new Objsc::FloorObjectCreator());
		factory->addCreator(Objsc::AverageObject::langname, new Objsc::AverageObjectCreator());
		factory->addCreator(Objsc::RandomObject::langname, new Objsc::RandomObjectCreator());
		factory->addCreator(Objsc::MaxObject::langname, new Objsc::MaxObjectCreator());
		factory->addCreator(Objsc::MinObject::langname, new Objsc::MinObjectCreator());
		factory->addCreator(Objsc::ChangeObject::langname, new Objsc::ChangeObjectCreator());
		factory->addCreator(Objsc::IntObject::langname, new Objsc::IntObjectCreator());
	}


	/////////////////////////////////////
	// MATH OBJECT
	/////////////////////////////////////
	MathObject::MathObject() : value(0.0f), label() {}
		
	bool MathObject::parseArgs(Objsc::LangList* args)
	{
		if(args->size() < min_args)	{
			return false;
		} else {
			//setValue(getItemAsFloat(args, 1));
			setValue(getItemAsFloat(args, 0));
			return true;
		}
	}
	
	void MathObject::setLabel(string nlabel)
	{
		setValue((float)atof(nlabel.c_str()));
	}
	
	void MathObject::setValue(float nvalue) 
	{ 
		value = nvalue;
		char cstr[64];
		sprintf(cstr, "%s %.3f", getLangName().c_str(), value);
		displaystr.assign(cstr);
		sprintf(cstr, "%.3f", value);
		label.assign(cstr);
	}
	
	void MathObject::receive(int inlet, Objsc::LangAtom* atom)
	{
		switch(atom->getType())	{
			case LANG_NULL:
				//send(1, new Objsc::LangBang());
				break;
			case LANG_BANG:
				activate();
				break;
			case LANG_INT:
				switch(inlet)	{
					case 0:
						eval(parseInt(atom));
						break;
					case 1:
						setValue(parseInt(atom));
						break;
				}
				break;
			case LANG_FLOAT:
				switch(inlet)	{
					case 0:
						eval(parseFloat(atom));
						break;
					case 1:
						setValue(parseFloat(atom));
						break;
				}
				break;
			case LANG_STRING:
				//send(1, new Objsc::LangBang());
				break;
			case LANG_VAR:
				//send(1, new Objsc::LangBang());
				break;
			case LANG_LIST:
				receiveList(inlet, dynamic_cast<Objsc::LangList*>(atom));
				break;
		}
	}
	
	void MathObject::receiveList(int inlet, LangList* value)
	{
		switch(inlet)	{
			case 0:
				eval(parseFloat(value->idx(0), 0.0f));
				break;
			case 1:
				setValue(parseFloat(value->idx(0), 0.0f));
				break;
		}
	}
	
	
	/////////////////////////////////////
	// EQUALS OBJECT
	/////////////////////////////////////
	void EqualsObject::eval(float input)
	{
		if(input == value)	{
			send(0, new Objsc::LangBang());
		} else {
			send(1, new Objsc::LangBang());
		}
	}
	
	
	/////////////////////////////////////
	// CHANGE OBJECT
	/////////////////////////////////////
	void ChangeObject::eval(float input)
	{
		if(input != value)	{
			value = input;
			send(0, new Objsc::LangFloat(value));
			send(1, new Objsc::LangBang());
		} 
	}
	
	/////////////////////////////////////
	// GREATER THAN OBJECT
	/////////////////////////////////////
	void GreaterThanObject::eval(float input)
	{
		if(input > value)	{
			send(0, new Objsc::LangBang());
		} else {
			send(1, new Objsc::LangBang());
		}
	}
	
	
	/////////////////////////////////////
	// LESS THAN OBJECT
	/////////////////////////////////////
	void LessThanObject::eval(float input)
	{
		if(input < value)	{
			send(0, new Objsc::LangBang());
		} else {
			send(1, new Objsc::LangBang());
		}
	}
	
	
	/////////////////////////////////////
	// ADD OBJECT
	/////////////////////////////////////
	void AddObject::eval(float input)
	{
		send(0, new Objsc::LangFloat(input+value));
	}
	
	
	/////////////////////////////////////
	// MINUS OBJECT
	/////////////////////////////////////
	void MinusObject::eval(float input)
	{
		send(0, new Objsc::LangFloat(input-value));
	}
	
	
	
	/////////////////////////////////////
	// MULTIPLY OBJECT
	/////////////////////////////////////
	void MultiplyObject::eval(float input)
	{
		send(0, new Objsc::LangFloat(input*value));
	}
	
	/////////////////////////////////////
	// DIVIDE OBJECT
	/////////////////////////////////////
	void DivideObject::eval(float input)
	{
		if(value != 0.0f)	{
			send(0, new Objsc::LangFloat(input/value));
		} else {
			send(0, new Objsc::LangFloat(0.0f));
		}
	}
	
	/////////////////////////////////////
	// MODULO OBJECT
	/////////////////////////////////////
	void ModuloObject::eval(float input)
	{
		send(0, new Objsc::LangFloat(fmod(input,value)));
	}
	
	
	/////////////////////////////////////
	// MAX OBJECT
	/////////////////////////////////////
	void MaxObject::eval(float input)
	{
		limit = MAX(input,value);
		send(1, new Objsc::LangFloat(limit));
	}
	
	void MaxObject::receive(int inlet, Objsc::LangAtom* atom)
	{
		switch(atom->getType())	{
			case LANG_NULL:
				//send(1, new Objsc::LangBang());
				break;
			case LANG_BANG:
				activate();
				break;
			case LANG_INT:
				switch(inlet)	{
					case 0:
						eval(parseInt(atom));
						activate();
						break;
					case 1:
						eval(parseInt(atom));
						break;
				}
				break;
			case LANG_FLOAT:
				switch(inlet)	{
					case 0:
						eval(parseFloat(atom));
						activate();
						break;
					case 1:
						eval(parseFloat(atom));
						break;
				}
				break;
			case LANG_STRING:
				//send(1, new Objsc::LangBang());
				break;
			case LANG_VAR:
				//send(1, new Objsc::LangBang());
				break;
			case LANG_LIST:
				receiveList(inlet, dynamic_cast<Objsc::LangList*>(atom));
				break;
		}
	}
	
	void MaxObject::receiveList(int inlet, LangList* value)
	{
		string cmd = parseString(value->idx(0), "");
		if(cmd == "reset" && inlet==0)	{
			reset();
		} else if(cmd == "")	{
			switch(inlet)	{
				case 0:
					eval(parseFloat(value->idx(0), 0.0f));
					activate();
					break;
				case 1:
					eval(parseFloat(value->idx(0), 0.0f));
					break;
			}
		}
	}
	
	void MaxObject::activate()
	{
		send(0, new Objsc::LangFloat(limit));
	}
	
	void MaxObject::reset()
	{
		limit=value;
	}
	
	/////////////////////////////////////
	// MIN OBJECT
	/////////////////////////////////////
	void MinObject::eval(float input)
	{
		limit = MIN(input,value);
		send(1, new Objsc::LangFloat(limit));
	}
	
	void MinObject::receive(int inlet, Objsc::LangAtom* atom)
	{
		switch(atom->getType())	{
			case LANG_NULL:
				//send(1, new Objsc::LangBang());
				break;
			case LANG_BANG:
				activate();
				break;
			case LANG_INT:
				switch(inlet)	{
					case 0:
						eval(parseInt(atom));
						activate();
						break;
					case 1:
						eval(parseInt(atom));
						break;
				}
				break;
			case LANG_FLOAT:
				switch(inlet)	{
					case 0:
						eval(parseFloat(atom));
						activate();
						break;
					case 1:
						eval(parseFloat(atom));
						break;
				}
				break;
			case LANG_STRING:
				//send(1, new Objsc::LangBang());
				break;
			case LANG_VAR:
				//send(1, new Objsc::LangBang());
				break;
			case LANG_LIST:
				receiveList(inlet, dynamic_cast<Objsc::LangList*>(atom));
				break;
		}
	}
	
	void MinObject::receiveList(int inlet, LangList* value)
	{
		string cmd = parseString(value->idx(0), "");
		if(cmd == "reset" && inlet==0)	{
			reset();
		} else if(cmd == "")	{
			switch(inlet)	{
				case 0:
					eval(parseFloat(value->idx(0), 0.0f));
					activate();
					break;
				case 1:
					eval(parseFloat(value->idx(0), 0.0f));
					break;
			}
		}
	}
	
	void MinObject::activate()
	{
		send(0, new Objsc::LangFloat(limit));
	}
	
	void MinObject::reset()
	{
		limit=value;
	}
	
	/////////////////////////////////////
	// INVERT OBJECT
	/////////////////////////////////////
	void InvertObject::eval(float input)
	{
		send(0, new Objsc::LangFloat(1.0f-input));
	}
	
	/////////////////////////////////////
	// CEILING OBJECT
	/////////////////////////////////////
	void CeilingObject::eval(float input)
	{
		send(0, new Objsc::LangFloat(input<value?input:value));
	}
	
	/////////////////////////////////////
	// FLOOR OBJECT
	/////////////////////////////////////
	void FloorObject::eval(float input)
	{
		send(0, new Objsc::LangFloat(input>value?input:value));
	}
	
	
	/////////////////////////////////////
	// AVERAGE OBJECT
	/////////////////////////////////////
	AverageObject::AverageObject() : 
			sum(0.0f),
			avg(0.0f),
			count(0.0f)
	{
		setInlets(2);
		setOutlets(3);
	}
	
	void AverageObject::receive(int inlet, Objsc::LangAtom* atom)
	{
		switch(atom->getType())	{
			case LANG_NULL:
				break;
			case LANG_BANG:
				if(inlet == 0)	{
					activate();
				}
				break;
			case LANG_INT:
				if(inlet == 1)	{
					add(parseInt(atom));
				}
				break;
			case LANG_FLOAT:
				if(inlet == 1)	{
					add(parseFloat(atom));
				}
				break;
			case LANG_STRING:
				break;
			case LANG_VAR:
				break;
			case LANG_LIST:
				if(inlet == 0)	{
					receiveList(0, dynamic_cast<Objsc::LangList*>(atom));
				}
				break;
		}
	}
	
	void AverageObject::receiveList(int inlet, LangList* value)
	{
		string cmd = parseString(value->idx(0), "");
		if(cmd == "reset")	{
			reset();
		} else if(cmd == "count")	{
			send(1, new Objsc::LangFloat(count));
		} else if(cmd == "sum")	{
			send(1, new Objsc::LangFloat(sum));
		}
	}
	
	void AverageObject::activate()
	{
		send(0, new Objsc::LangFloat(avg));
	}
	
	void AverageObject::reset()
	{
		sum = 0.0f;
		count = 0.0f;
		avg = 0.0f;
		send(2, new Objsc::LangFloat(avg));
	}
	
	void AverageObject::add(float value)
	{
		sum += value;
		++count;
		avg = sum/count;
		send(2, new Objsc::LangFloat(avg));
	}
	
	/////////////////////////////////////
	// RANDOM OBJECT
	/////////////////////////////////////
	RandomObject::RandomObject() : 
			lower(0.0f),
			upper(1.0f)
	{
		setInlets(2);
		setOutlets(1);
	}
	
	void RandomObject::receive(int inlet, Objsc::LangAtom* atom)
	{
		switch(atom->getType())	{
			case LANG_NULL:
				break;
			case LANG_BANG:
				if(inlet == 0)	{
					activate();
				}
				break;
			case LANG_INT:
				if(inlet == 0)	{
					lower = 0.0f;
					upper = (float)parseInt(atom);
					activate();
				} else if(inlet == 1)	{
					lower = 0.0f;
					upper = (float)parseInt(atom);
				} 
				break;
			case LANG_FLOAT:
				if(inlet == 0)	{
					lower = 0.0f;
					upper = parseFloat(atom);
					activate();
				}else if(inlet == 1)	{
					lower = 0.0f;
					upper = parseFloat(atom);
				}
				break;
			case LANG_STRING:
				break;
			case LANG_VAR:
				break;
			case LANG_LIST:
				receiveList(inlet, dynamic_cast<Objsc::LangList*>(atom));
				break;
		}
	}
	
	void RandomObject::receiveList(int inlet, LangList* value)
	{
		if(value->size() == 1)	{
			lower = 0.0f;
			upper = parseFloat(value->idx(0), 1.0f);
		} else if(value->size() > 1)	{
			lower = parseFloat(value->idx(0), 0.0f);
			upper = parseFloat(value->idx(1), 1.0f);
		}
		if(inlet == 0)	{
			activate();
		}
	}
	
	void RandomObject::activate()
	{
		send(0, new Objsc::LangFloat(ofRandom(lower, upper)));
	}
	
	/////////////////////////////////////
	// INT OBJECT
	/////////////////////////////////////
	void IntObject::eval(float input)
	{
		intvalue = RINT(input);
		send(0, new Objsc::LangInt(intvalue));
	}
	
	void IntObject::activate()
	{
		send(0, new Objsc::LangInt(intvalue));
	}
	
	void IntObject::setValue(float nvalue) 
	{ 
		value = intvalue = RINT(nvalue);
		char cstr[64];
		sprintf(cstr, "%s %.3f", getLangName().c_str(), value);
		displaystr.assign(cstr);
		sprintf(cstr, "%.3f", value);
		label.assign(cstr);
	}
}

