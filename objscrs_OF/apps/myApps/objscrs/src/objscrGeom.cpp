#include "objscrGeom.hpp"


////////////////////////////////
// FUNCTIONS
////////////////////////////////
float distance(float x1, float y1, float x2, float y2) {return sqrt(pow(x2-x1,2)+pow(y2-y1,2));}
float screenToStage(float scrPos, float scrDim) { return scrPos/scrDim; }
float stageToScreen(float stagePos, float scrDim) { return scrDim*stagePos; }
